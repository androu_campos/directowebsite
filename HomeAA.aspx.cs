using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;

public partial class HomeAA : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Redirect("http://drpmecca.directo.com/MeccaDirecto");
        try
        {

            img1.Attributes.Add("onmouseover", "showImage('" + img1.ClientID + "','1');");
            img1.Attributes.Add("onmouseout", "showImage('" + img1.ClientID + "','2');");
            img2.Attributes.Add("onmouseover", "showImage('" + img2.ClientID + "','3');");
            img2.Attributes.Add("onmouseout", "showImage('" + img2.ClientID + "','4');");
            ImageButton1.Attributes.Add("onmouseover", "showImage('" + ImageButton1.ClientID + "','5');");
            ImageButton1.Attributes.Add("onmouseout", "showImage('" + ImageButton1.ClientID + "','6');");
            ImageButton2.Attributes.Add("onmouseover", "showImage('" + ImageButton2.ClientID + "','7');");
            ImageButton2.Attributes.Add("onmouseout", "showImage('" + ImageButton2.ClientID + "','8');");
            ImageButton3.Attributes.Add("onmouseover", "showImage('" + ImageButton3.ClientID + "','9');");
            ImageButton3.Attributes.Add("onmouseout", "showImage('" + ImageButton3.ClientID + "','10');");
            
            string conn = ConfigurationManager.ConnectionStrings["MECCA2ConnectionString"].ConnectionString;

            #region "Summary"]
            string date_1 = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            string cmdLatest = "SELECT top 1 [Id], [Date], [Calls], [CMinutes], [VMinutes], [Profit], [TotalSales], [ASR], [CACD], [VACD], [Attempts], [ABR] " +
                             "  FROM [MECCA2].[dbo].[SummaryAA] " +
                              "WHERE [DATE] = '" + date_1 + "' ";

            SqlDataAdapter adaptLatest = new SqlDataAdapter(cmdLatest, conn);
            DataTable tblLatest = new DataTable();

            try
            {
                adaptLatest.Fill(tblLatest);
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "');</script>");
            }
                        

            float AttemptsY = Convert.ToInt32(tblLatest.Rows[0]["Attempts"].ToString());
            double MinutesY = Convert.ToDouble(tblLatest.Rows[0]["CMinutes"].ToString());
            int CallsY = Convert.ToInt32(tblLatest.Rows[0]["Calls"].ToString());
            double ProfitY = Convert.ToDouble(tblLatest.Rows[0]["Profit"].ToString());
            double TotSalesY = Convert.ToDouble(tblLatest.Rows[0]["TotalSales"].ToString());
            int ASRY = Convert.ToInt32(tblLatest.Rows[0]["ASR"].ToString());
            int ABRY = Convert.ToInt32(tblLatest.Rows[0]["ABR"].ToString());
            double ACDY = Convert.ToDouble(tblLatest.Rows[0]["CACD"].ToString());

            this.lblAttemptsY.Text = String.Format("{0:N0}", AttemptsY);
            this.lblMinutesY.Text = String.Format("{0:N0}", MinutesY);
            this.lblCallsY.Text = String.Format("{0:N0}", CallsY);
            this.lblProfitY.Text = String.Format("{0:N0}", ProfitY);
            this.lblTotSalesY.Text = String.Format("{0:N0}", TotSalesY);
            this.lblASRY.Text = String.Format("{0:N2}", ASRY) + "%";
            this.lblABRY.Text = String.Format("{0:N2}", ABRY) + "%";
            this.lblACDY.Text = String.Format("{0:N2}", ACDY) + "%";
            
            
            //For Change Summary Table Data
            string date_8 = DateTime.Now.AddDays(-8).ToString("yyyy-MM-dd");
            string commandChange = "SELECT top 1 [Id], [Date], [Calls], [CMinutes], [VMinutes], [Profit], [TotalSales], [ASR], [CACD], [VACD], [Attempts], [ABR] " +
                             "  FROM [MECCA2].[dbo].[SummaryAA] " +
                              "WHERE [DATE] = '" + date_8 + "' ";
            SqlDataAdapter adaptChange = new SqlDataAdapter(commandChange, conn);
            DataTable tblChange = new DataTable();
            adaptChange.Fill(tblChange);
            
            int Attempts8 = Convert.ToInt32(tblChange.Rows[0]["Attempts"].ToString());
            double Minutes8 = Convert.ToDouble(tblChange.Rows[0]["CMinutes"].ToString());
            int Calls8 = Convert.ToInt32(tblChange.Rows[0]["Calls"].ToString());
            double Profit8 = Convert.ToDouble(tblChange.Rows[0]["Profit"].ToString());
            double TotSales8 = Convert.ToDouble(tblChange.Rows[0]["TotalSales"].ToString());
            int ASR8 = Convert.ToInt32(tblChange.Rows[0]["ASR"].ToString());
            int ABR8 = Convert.ToInt32(tblChange.Rows[0]["ABR"].ToString());
            double ACD8 = Convert.ToDouble(tblChange.Rows[0]["CACD"].ToString());

            float AttemptsC = Math.Abs(Attempts8 - AttemptsY);
            double MinutesC = Math.Abs(Minutes8 - MinutesY);
            float CallsC = Math.Abs(Calls8 - CallsY);
            double ProfitC = Math.Abs(Profit8 - ProfitY);
            double TotSalesC = Math.Abs(TotSales8 - TotSalesY);
            float ASRC = Math.Abs(ASR8 - ASRY);
            float ABRC = Math.Abs(ABR8 - ABRY);
            double ACDC = Math.Abs(ACD8 - ACDY);

            this.lblAttemptsC.Text = String.Format("{0:N0}", AttemptsC);
            this.lblMinutesC.Text = String.Format("{0:N2}", MinutesC);
            this.lblCallsC.Text = String.Format("{0:N0}", CallsC);
            this.lblProfitC.Text = String.Format("{0:N0}", ProfitC);
            this.lblTotSalesC.Text = String.Format("{0:N0}", TotSalesC);
            this.lblASRC.Text = String.Format("{0:N2}", ASRC);
            this.lblABRC.Text = String.Format("{0:N2}", ABRC);
            this.lblACDC.Text = String.Format("{0:N2}", ACDC);

            //Setting images and colors
            if (AttemptsY < Attempts8)
            {
                Image10.ImageUrl = "./Images/roja.gif";
                lblAttemptsC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image10.ImageUrl = "./Images/verde.gif";
                lblAttemptsC.ForeColor = System.Drawing.Color.Green;
            }

            if (MinutesY < Minutes8)
            {
                Image4.ImageUrl = "./Images/roja.gif";
                lblMinutesC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image4.ImageUrl = "./Images/verde.gif";
                lblMinutesC.ForeColor = System.Drawing.Color.Green;
            }

            if (CallsY < Calls8)                     //CALLS
            {
                Image5.ImageUrl = "./Images/roja.gif";
                lblCallsC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image5.ImageUrl = "./Images/verde.gif";
                lblCallsC.ForeColor = System.Drawing.Color.Green;
            }

            if (ProfitY < Profit8)                      //PROFIT
            {
                Image6.ImageUrl = "./Images/roja.gif";
                lblProfitC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image6.ImageUrl = "./Images/verde.gif";
                lblProfitC.ForeColor = System.Drawing.Color.Green;
            }

            if (TotSalesY < TotSales8)                 //TOTAL SALES
            {
                Image13.ImageUrl = "./Images/roja.gif";
                lblTotSalesC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image13.ImageUrl = "./Images/verde.gif";
                lblTotSalesC.ForeColor = System.Drawing.Color.Green;
            }

            if (ASRY < ASR8)                            //ASR
            {
                Image7.ImageUrl = "./Images/roja.gif";
                lblASRC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image7.ImageUrl = "./Images/verde.gif";
                lblASRC.ForeColor = System.Drawing.Color.Green;
            }

            if (ABRY < ABR8)                     //ABR
            {
                Image15.ImageUrl = "./Images/roja.gif";
                lblABRC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image15.ImageUrl = "./Images/verde.gif";
                lblABRC.ForeColor = System.Drawing.Color.Green;
            }

            if (ACDY < ACD8)                           //ACD
            {
                Image8.ImageUrl = "./Images/roja.gif";
                lblACDC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image8.ImageUrl = "./Images/verde.gif";
                lblACDC.ForeColor = System.Drawing.Color.Green;
            }

            #endregion

            #region "Top15"

            //string date_1 = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            string cmdTop15 = "SELECT TOP (15) ROUND(SUM(CustMinutes), 2) AS Minutes, Country, " +
                                     "ROUND(SUM(TotalPrice - TotalCost), 4) AS Profit, " +
                                     "mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR, " +
                                     "mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR, " +
                                     "mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD " +
                              "  FROM MECCA2.dbo.trafficlog T " +
                              "  WHERE(BillingDate BETWEEN '" + date_1 + "' AND '" + date_1 + "') " +
                                    " AND T.CUSTOMER IN('ALLACCESS-CCPH06') " +
                                    " AND T.CustMinutes > 0 " +
                              "  GROUP BY Country, BillingDate " +
                              "  ORDER BY Profit DESC;";
            SqlDataAdapter adaptTop15 = new SqlDataAdapter(cmdTop15, conn);
            DataTable tblTop15 = new DataTable();
            adaptTop15.Fill(tblTop15);

            gdvCountry.DataSource = tblTop15;
            gdvCountry.DataBind();

            #endregion

            #region "Customer"

            //string date_1 = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            string cmdCustomer = "SELECT ROUND(SUM(CustMinutes), 2)  AS Minutes," +
                                       " Customer, " +
                                       " ROUND(SUM(TotalPrice), 4) AS Sales, " +
                                       " ROUND(SUM(TotalPrice - TotalCost), 4) AS Profit, " +
                                       " mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR, " +
                                       " mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR, " +
                                       " mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD " +
                                "   FROM mecca2.dbo.trafficlog T " +
                                "  WHERE BillingDate = '" + date_1 + "' " +
                                "    AND T.CUSTOMER IN('ALLACCESS-CCPH06') " +
                                "    AND CustMinutes > 0 " +
                                "  GROUP BY Customer, BillingDate " +
                                "  ORDER BY Profit DESC; ";
            SqlDataAdapter adaptCustomer = new SqlDataAdapter(cmdCustomer, conn);
            DataTable tblCustomer = new DataTable();
            adaptCustomer.Fill(tblCustomer);

            gdvCustomers.DataSource = tblCustomer;
            gdvCustomers.DataBind();

            #endregion  

            #region "Vendor"

            //string date_1 = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            string cmdVendor = "SELECT ROUND(SUM(VendorMinutes), 2) AS Minutes, " +
                                    " Vendor, ROUND(SUM(TotalCost), 4) AS Cost, " +
                                    " ROUND(SUM(TotalPrice - TotalCost), 4) AS Profit, " +
                                    " mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR, " +
                                    " mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR, " +
                                    " mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD " +
                             "   FROM MECCA2.dbo.trafficlog T " +
                             "  WHERE BillingDate = '" + date_1 + "' " +
                             "    AND T.CUSTOMER IN('ALLACCESS-CCPH06') " +
                             "    AND T.VENDOR <> 'VOICE-DETECTION' " +
                             "  GROUP BY Vendor, BillingDate ORDER BY Cost DESC; ";
            SqlDataAdapter adaptVendor = new SqlDataAdapter(cmdVendor, conn);
            DataTable tblVendor = new DataTable();
            adaptVendor.Fill(tblVendor);

            gdVendors.DataSource = tblVendor;
            gdVendors.DataBind();

            #endregion

            #region "Regions"

            //string date_1 = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            string cmdRegion = "Select top 15 case when country = 'MEXICO' then country+' - '+[Type]+' - '+ltrim(right(region,len(region)-charindex('-',region))) " +
	                                 " else country + ' - ' +[Type] + ' - ' + region end 'Region', " +
	                                 " ROUND(SUM(CustMinutes), 2) 'Minutes', " +
	                                 " ROUND(SUM(TotalPrice - TotalCost), 4) AS Profit, " +
                                     " mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR, " +
	                                 " mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR, " +
                                     " mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD " +
                               "  From MECCA2.dbo.trafficlog T " +
                               "  Where billingdate = '" + date_1 + "' " +
                               "    And customer IN('ALLACCESS-CCPH06') " +
                               "    And custminutes > 0 " +
                               "  Group by case when country = 'MEXICO' then country+' - ' +[Type] + ' - ' + ltrim(right(region, len(region) - charindex('-', region))) " +
                               "	    else country + ' - ' +[Type] + ' - ' + region end " +
                               "  Order by ROUND(SUM(TotalPrice -TotalCost), 4) desc;";
            SqlDataAdapter adaptRegion = new SqlDataAdapter(cmdRegion, conn);
            DataTable tblRegion = new DataTable();
            adaptRegion.Fill(tblRegion);

            gdvRegions.DataSource = tblRegion;
            gdvRegions.DataBind();

            #endregion

            if ((Session["Rol"].ToString() == "NOC" || Session["Rol"].ToString() == "NOC National") && Session["Rol"].ToString() != "Comercial" && Session["Rol"].ToString() != string.Empty)
            {
                lblProfitY.Visible = false;
                lblProfitC.Visible = false;
                Image6.Visible = false;
                Label5.Visible = false;

                Label11.Visible = false;
                lblTotSalesY.Visible = false;
                lblTotSalesC.Visible = false;
                Image13.Visible = false;
            }
            
            if (Session["DirectoUser"].ToString() == "djassan" || Session["DirectoUser"].ToString() == "hvalle")
            {
                hypNational.Visible = true;
            }
            else
            {
                hypNational.Visible = false;
            }
        }
        catch (Exception ex)
        {
            string message = ex.Message.ToString();
            Response.Write("<script>alert('" + message + "- " + ex.InnerException + "');</script>");
        }

    }

    protected void buttoncchome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeCC.aspx");
    }

    protected void buttonaahome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeAA.aspx");
    }

    protected void buttonicshome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeR.aspx");
    }

    protected void buttonhome_click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }

    protected void buttonsmshome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeSMS.aspx");
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            string messagerror = ex.Message.ToString();
        }

        return resultsDataSet;
    }
   
}
