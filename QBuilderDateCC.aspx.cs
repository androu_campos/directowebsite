using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RKLib.ExportData;
using System.Globalization;

public partial class QBuilderDateCC : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        SqlConnection SqlConn1 = new SqlConnection();
        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand SqlCommand1 = new SqlCommand();

        string user = string.Empty;
        string rol = string.Empty;
        string agency = string.Empty;

        user = Session["DirectoUser"].ToString();


        if (!Page.IsPostBack)
        {
            txtFromV.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            txtToV.Text = DateTime.Now.AddDays(-1).ToShortDateString();


            if (user == "santander" || user == "bento" || user == "contacto" || user == "eci" || user == "ergon"
                || user == "megacal" || user == "mkt911" || user == "sto" || user == "targetone" || user == "vcip"
                || user == "santanadmoncall" || user == "santanderdomer" || user == "domercent" || user == "megacall-invex")
            {
                if (user == "santander")
                {
                    labelAgency.Visible = true;
                    ddlAgency.Visible = true;
                    lblCC.Visible = true;
                    ddlCC.Visible = true;
                }
                else
                {
                    ddlCC.Items.Clear();

                    if (user == "santanderdomer")
                    {
                        SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-" + user + "%' AND Type = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
                    }
                    else if (user == "santanderdomer")
                    {
                        SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-MED-%' AND Type = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
                    }
                    else
                    {
                        SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE (PROVIDERNAME LIKE 'CC-" + user + "SAN%' or PROVIDERNAME LIKE 'CC-" + user + "%' ) AND Type = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
                    }
                    SqlCommand1.Connection = SqlConn1;
                    SqlConn1.Open();
                    SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
                    while (myReader1.Read())
                    {
                        ddlCC.Items.Add(myReader1.GetValue(0).ToString());
                    }
                    myReader1.Close();
                    SqlConn1.Close();

                    lblCC.Visible = true;
                    ddlCC.Visible = true;
                    ddlCC.Enabled = true;
                }

            }
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        string Query, muser, mrol;
        muser = Session["DirectoUser"].ToString();
        mrol = Session["Rol"].ToString();

        string sql = string.Empty;
        string user = Session["DirectoUser"].ToString();
        string from = txtFromV.Text;
        string to = txtToV.Text;
        string ag = ddlAgency.SelectedValue.ToString();
        string cc = ddlCC.SelectedValue.ToString();
        SqlCommand sqlSpQuery = new SqlCommand();

        DateTime dfrom = DateTime.Parse(from, CultureInfo.InvariantCulture);
        DateTime dto = DateTime.Parse(to, CultureInfo.InvariantCulture);

        TimeSpan ts = dfrom - dto;

        if (user == "santander" || user == "bento" || user == "contacto" || user == "eci" || user == "ergon"
            || user == "megacal" || user == "mkt911" || user == "sto" || user == "targetone" || user == "vcip"
            || user == "santanadmoncall" || user == "domercent" || user == "santanderdomer" || user == "domer"
            || user == "megacall-invex")
        {
            if (user == "santander")
            {
                if (ag == "ALL" && cc == "ALL")
                {
                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, Country, Region, "
                         + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                         + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed' "
                        //+ "from talktel.dbo.trafficlog a join pricing.dbo.[VW_TARGETONE_MX] b on a.custprefix = b.CompleteCode "
                         + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and (customer like '%SANTAN%' OR CUSTOMER LIKE '%SAN%') AND CUSTOMER NOT LIKE '%CORTIZO%'"
                         + "group by billingdate, Customer, Country, Region, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[MX Price] "
                         + "UNION "
                         + "SELECT '** ALL **','********','********','********', "
                         + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                         + "cast(b.[MX Price] as varchar(50)) 'Price', "
                         + "sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
                        //+ "from talktel.dbo.trafficlog a join pricing.dbo.VW_TARGETONE_MX b on a.custprefix = b.CompleteCode "
                         + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and (customer like '%SANTAN%' OR CUSTOMER LIKE '%SAN%') AND CUSTOMER NOT LIKE '%CORTIZO%' "
                         + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }
                else if (ag != "ALL" && cc == "**ALL**")
                {

                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, "
                       + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                       + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed'  "
                       + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like 'CC-" + ag + "SAN%' "
                       + "group by billingdate, Customer, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[Price] "
                       + "UNION "
                       + "SELECT '** ALL **','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
                       + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like 'CC-" + ag + "SAN%' "
                       + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }
                else if (ag != "ALL" && cc != "ALL")
                {
                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, "
                       + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                       + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed' "
                       + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like '" + cc + "' "
                       + "group by billingdate, Customer, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[Price] "
                       + "UNION "
                       + "SELECT '** ALL **','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
                       + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like '" + cc + "' "
                       + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }
            }
            else if (user == "vcip")
            {
                if (cc == "**ALL**")
                {
                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, "
                         + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                         + "cast(b.[MX Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[MX Price]),4) 'Billed' "
                         + "from talktel.dbo.trafficlog a join pricing.dbo.VW_VCIP_MX b on a.custprefix = b.CompleteCode "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and (customer like 'CC-" + user + "SAN%' or customer like 'CC-" + user + "-SAN%') "
                         + "group by billingdate, Customer, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[MX Price] "
                         + "UNION "
                         + "SELECT '** ALL **','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[MX Price]) 'Billed' "
                         + "from talktel.dbo.trafficlog a join pricing.dbo.VW_VCIP_MX b on a.custprefix = b.CompleteCode "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and (customer like 'CC-" + user + "SAN%' or customer like 'CC-" + user + "-SAN%') "
                         + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }

                else
                {
                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, "
                       + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                       + "cast(b.[MX Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[MX Price]),4) 'Billed' "
                       + "from talktel.dbo.trafficlog a join pricing.dbo.VW_VCIP_MX b on a.custprefix = b.CompleteCode "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like '" + cc + "' "
                       + "group by billingdate, Customer, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[MX Price] "
                       + " UNION "
                       + "SELECT '** ALL **','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[MX Price]) 'Billed' "
                       + "from talktel.dbo.trafficlog a join pricing.dbo.VW_VCIP_MX b on a.custprefix = b.CompleteCode "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like '" + cc + "' "
                       + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }
            }
            else if (user == "santanderdomer")
            {
                if (cc == "**ALL**")
                {
                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, "
                         + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                         + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed' "
                         + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and customer like 'CC-" + user + "%' "
                         + "group by billingdate, Customer, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[Price] "
                         + "UNION "
                         + "SELECT '** ALL **','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
                         + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and customer like 'CC-" + user + "%' "
                         + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }

                else
                {
                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, "
                       + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                       + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed' "
                       + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like '" + cc + "' "
                       + "group by billingdate, Customer, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[MX Price] "
                       + " UNION "
                       + "SELECT '** ALL **','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
                       + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like '" + cc + "' "
                       + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }
            }
            else if (user == "santanadmoncall" || user == "domercent" || user == "megacall-invex")
            {
                if (cc == "**ALL**")
                {
                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, "
                         + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                         + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed' "
                         + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and customer like 'CC-" + user + "%' "
                         + "group by billingdate, Customer, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[Price] "
                         + " UNION "
                         + "SELECT '** ALL **','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
                         + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and customer like 'CC-" + user + "%' "
                         + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }
                else
                {
                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, "
                       + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                       + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed' "
                       + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like '" + cc + "' "
                       + "group by billingdate, Customer, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[Price] "
                       + " UNION "
                       + "SELECT '** ALL **','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
                       + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like '" + cc + "' "
                       + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }
            }
            else
            {
                if (cc == "**ALL**" && user == "megacal")
                {
                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, "
                         + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                         + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed'  "
                         + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and (customer like 'CC-" + user + "SAN%' or customer like 'CC-" + user + "L-INVEX%' )"
                         + "group by billingdate, Customer, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[Price] "
                         + "having sum(custminutes) > 0 "
                         + "UNION "
                         + "SELECT '** ALL **','********', "
                         + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                         + "cast(b.[Price] as varchar(50)) 'Price', "
                         + "sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
                         + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and (customer like 'CC-" + user + "SAN%' or customer like 'CC-" + user + "L-INVEX%' )"
                         + "group by billingdate, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[Price] "
                         + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }
                else if (cc == "**ALL**")
                {
                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, "
                         + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                         + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed'  "
                         + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and customer like 'CC-" + user + "SAN%' "
                         + "group by billingdate, Customer, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[Price] "
                         + "having sum(custminutes) > 0 "
                         + "UNION "
                         + "SELECT '** ALL **','********', "
                         + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                         + "cast(b.[Price] as varchar(50)) 'Price', "
                         + "sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
                         + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                         + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                         + "and customer like 'CC-" + user + "SAN%' "
                         + "group by billingdate, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[Price] "
                         + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }
                else
                {
                    sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, "
                       + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                       + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed'  "
                       + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like '" + cc + "' "
                       + "group by billingdate, Customer, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[Price] "
                       + " UNION "
                       + "SELECT '** ALL **','********', "
                       + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                       + "cast(b.[Price] as varchar(50)) 'Price', "
                       + "sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
                       + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                       + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                       + "and customer like '" + cc + "' "
                       + "group by billingdate, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END, b.[Price] "
                       + "order by billingdate, Customer, round(sum(custminutes),4) desc ";
                }
            }
        }
        else if (user.ToString().Contains("santanagn") || user.ToString().Contains("targetonesant")
            || user.ToString().Contains("megacalsantan") || user.ToString().Contains("mkt911santan")
            || user.ToString().Contains("bentosan") || user.ToString().Contains("ecisant")
            || user.ToString().Contains("t1mayor") || user.ToString().Contains("santanadmoncall")
            || user.ToString().Contains("domercent") || user.ToString().Contains("contactosantagn")
            || user.ToString().Contains("konecta-sant"))
        {
            sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Country, Region, Lmc,"
                    + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                    + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed'  "
                    + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                    + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                    + "and customer like '" + user + "' "
                    + "group by billingdate, Country,Region, LMC, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END , b.[Price] "
                    + "having sum(custminutes) > 0 "
                    + " UNION "
                    + " SELECT '** ALL **','********','********','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
                    + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                    + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                    + "and customer like '" + user + "' "
                    + "order by billingdate, round(sum(custminutes),4) desc ";
            // bloquear las fechas para el tema de refacturacion de Domer - 17 al 23 de Agosto 2016
            //if (user.ToString().Contains("domercen") && (from <= "2016-08-18" || to <= "2016-08-23"))
            //{
            //    sql = "SELECT 'Nothing Found'";
            //}
        }
        else if (user.ToString().Contains("vcipsant") || user.ToString().Contains("vcip-sant"))
        {
            sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Country, Region, Lmc,"
            + "case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
            + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.[Price]),4) 'Billed' "
            + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
            + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
            + "and customer like '" + user + "' "
            + "group by billingdate, Country,Region, LMC, case when Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END , b.[MX Price] "
            + "having sum(custminutes) > 0 "
            + " UNION "
            + " SELECT '** ALL **','********','********','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.[Price]) 'Billed' "
            + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
            + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
            + "and customer like '" + user + "' "
            + "order by billingdate, round(sum(custminutes),4) desc ";
        }
        else if (user.ToString().Contains("cc-lider"))
        {
            sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Country, Region, Lmc,"
                     + "case when a.Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                     + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(billablecalls * b.[Price]),4) 'Billed' "
                    + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                     + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                     + "and customer like '" + user + "' "
                     + "group by billingdate, Country,Region, LMC, case when a.Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END , b.[Price] "
                     + "having sum(custminutes) > 0 "
                     + " UNION "
                     + " SELECT '** ALL **','********','********','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(billablecalls * b.[Price]) 'Billed' "
                    + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                     + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                     + "and customer like '" + user + "' "
                     + "order by billingdate, round(sum(custminutes),4) desc ";
        }
        else if (user.ToString().Contains("cc-med"))
        {
            sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, Country, Region, Lmc,"
                     + "case when a.Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                     + "cast(b.[Price] as varchar(50)) 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(billablecalls * b.[Price]),4) 'Billed' "
                    + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                     + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                     + "and customer like '" + user + "-%' "
                     + "group by billingdate, Customer,Country,Region, LMC, case when a.Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END , b.[Price] "
                     + "having sum(custminutes) > 0 "
                     + " UNION "
                     + " SELECT '** ALL **','********','********','********','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(billablecalls * b.[Price]) 'Billed' "
                    + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                     + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                     + "and customer like '" + user + "-%' "
                     + "order by billingdate, round(sum(custminutes),4) desc ";
        }
        else if (user.ToString().Contains("cc-broxel"))
        {
            sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Customer, case when Vendor = 'CC-BROXEL-DIGITEX' then 'DIGITEX' when Vendor = 'TLKTMX-BROXEL' then 'BROXEL' else Vendor end as Vendor, Country, Region, Lmc,"
                    + "case when a.Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END AS Type, "
                    + "'0.0000' as 'Price', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(billablecalls * 0.0000),4) 'Billed' "
                   + "FROM TALKTEL.dbo.trafficlog a "
                    + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                    + "and customer like '" + user + "%' "
                    + "group by billingdate, Customer,case when Vendor = 'CC-BROXEL-DIGITEX' then 'DIGITEX' when Vendor = 'TLKTMX-BROXEL' then 'BROXEL' else Vendor end, Country,Region, LMC, case when a.Type IN ('CPP','MOBILE') THEN 'CPP' ELSE 'FIXED' END  "
                    + "having sum(custminutes) > 0 "
                    + " UNION "
                    + " SELECT '** ALL **','********','********','********','********','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(billablecalls * 0.0000) 'Billed' "
                   + "FROM TALKTEL.dbo.trafficlog a "
                    + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                    + "and customer like '" + user + "%' "
                    + "order by billingdate, round(sum(custminutes),4) desc ";
        }
        else
        {
            sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, Country, Region, Lmc, Type, cast(b.Price as varchar(50)) 'Price', custprefix 'Prefix', sum(billablecalls) 'Calls', round(sum(custminutes),4) 'Minutes', ROUND(SUM(custminutes * b.price),4) 'Billed' "
                + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                + "and customer = '" + user + "' "
                + "group by billingdate, country, region, lmc, Type, b.price, custprefix "
                + "having sum(custminutes) > 0 "
                + " UNION "
                + " SELECT '** ALL **','********','********','********','********','********','********',sum(billablecalls) 'Calls', sum(custminutes) 'Minutes', sum(custminutes * b.price) 'Billed' "
                + "FROM TALKTEL.dbo.trafficlog a JOIN TALKTEL.[dbo].[Custbreakout] b on custname = customer and  a.custprefix = b.Prefix "
                + "where billingdate between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                + "and customer = '" + user + "' "
                + "order by billingdate, country, region, round(sum(custminutes),4) desc ";
        }

        sqlSpQuery.CommandType = System.Data.CommandType.Text;
        sqlSpQuery.CommandText = sql;

        DataSet ResultSet = null;

        if (ts.TotalDays >= -31)
        {
            ResultSet = Util.RunQuery(sqlSpQuery);

            //Response.Write(sqlSpQuery.CommandText.ToString());
            //Response.End();
            Session["qry"] = sqlSpQuery.CommandText;
            Session["qbuildercc"] = ResultSet;

            int jr = ResultSet.Tables[0].Rows.Count;

            if (jr <= 0)
            {
                btnExcel.Visible = false;
            }
            else
            {
                this.gdvData.AutoGenerateColumns = false;
                this.gdvData.DataSource = ResultSet;
                this.gdvData.Columns.Clear();
                for (int c = 0; c < ResultSet.Tables[0].Columns.Count; c++)
                {
                    string columnName = ResultSet.Tables[0].Columns[c].ColumnName.ToString();
                    BoundField columna = new BoundField();
                    columna.HtmlEncode = false;
                    if (columnName == "Billed")
                    {
                        columna.DataFormatString = "${0:f2}";
                        columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        columna.ItemStyle.Width = 75;
                        columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    }
                    else if (columnName == "Minutes")
                    {
                        columna.DataFormatString = "{0:0,0}";
                        columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        columna.ItemStyle.Width = 75;
                        columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    }
                    else if (columnName == "Calls")
                    {
                        columna.DataFormatString = "{0:0,0}";
                        columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        columna.ItemStyle.Width = 100;
                        columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    }

                    columna.DataField = columnName;
                    columna.HeaderText = columnName;

                    gdvData.Columns.Add(columna);
                    gdvData.DataBind();
                }
            }

            //gdvData.DataSource = ResultSet;
            //gdvData.DataBind();
            gdvData.Visible = true;
            btnExcel.Visible = true;
        }
        else
        {
            SqlCommand sqlSpQuery2 = new SqlCommand();
            sqlSpQuery2.CommandType = CommandType.Text;

            string query = sqlSpQuery.CommandText.ToString();
            query = query.Replace("'", "-");

            sqlSpQuery2.CommandText = "INSERT INTO DirectoReports.dbo.QueryLog VALUES (getdate(),'" + query + "','" + muser + "','" + mrol + "')";
            Util.RunQuery2(sqlSpQuery2);

            Label2.Text = "Please contact your Sales Agent for reports with more than 30 days.";
            Label2.Visible = true;

        }

        
    }

    protected void gdvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvData.PageIndex = e.NewPageIndex;
        DataSet ds = (DataSet)Session["qbuildercc"];
        gdvData.DataSource = ds;
        gdvData.DataBind();
    }

    protected void exportData(object sender, EventArgs e)
    {
        try
        {

            DataTable dtEmployee = ((DataSet)Session["qbuildercc"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "BilledTraffic" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }

    protected void ddlAgency_SelectedIndexChanged(object sender, EventArgs e)
    {
        string user = string.Empty;
        string rol = string.Empty;
        string agency = string.Empty;

        user = Session["DirectoUser"].ToString();

        SqlConnection SqlConn1 = new SqlConnection();
        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand SqlCommand1 = new SqlCommand();


        if (ddlAgency.SelectedValue.ToString() == "ALL")
        {
            ddlCC.Enabled = false;
            ddlCC.Items.Clear();
            SqlCommand1.CommandText = "SELECT '**ALL**'";
        }
        else
        {
            ddlCC.Enabled = true;
            agency = ddlAgency.SelectedValue.ToString();
            ddlCC.Items.Clear();

            if (agency == "VCIP")
            {
                SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE (PROVIDERNAME LIKE 'CC-" + agency + "SAN%' or PROVIDERNAME LIKE 'CC-" + agency + "-SAN%') AND Type = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
            }
            else if (agency == "santanadmoncall")
            {
                SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-" + agency + "%' AND Type = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
            }
            else
            {
                SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-" + agency + "SAN%' AND Type = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
            }
        }



        SqlCommand1.Connection = SqlConn1;
        SqlConn1.Open();
        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
        while (myReader1.Read())
        {
            ddlCC.Items.Add(myReader1.GetValue(0).ToString());
        }
        myReader1.Close();
        SqlConn1.Close();
    }
}
