using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;
using System.Data.SqlClient;

public partial class GraphGrid : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Session["DirectoUser"] as string))//Usuario Logeado            
        {
            String query = null;            
            DataTable tbl;


            

            query = "SELECT SUM(Live_Calls) as 'Live_Calls', Time FROM[CDRDB].[dbo].[CDRN] where Time >= DATEADD(DAY,0,left(getdate(),11)) and AdjacencyName = 'C_TKM_Core' group by Time order by Time "; 
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];         
            makeChart1(tbl, "TKM1 161 - 191");
            tbl.Dispose();


            query = "SELECT SUM(Live_Calls) as 'Live_Calls', Time FROM[CDRDB].[dbo].[CDRN] where Time >= DATEADD(DAY,0,left(getdate(),11)) and AdjacencyName = 'C_TKM2_Core' group by Time order by Time ";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
            makeChart2(tbl, "TKM2 162 -14");
            tbl.Dispose();

            query = "SELECT SUM(Live_Calls) as 'Live_Calls', Time FROM[CDRDB].[dbo].[CDRN] where Time >= DATEADD(DAY,0,left(getdate(),11)) and AdjacencyName = 'C_TKM3_Core' group by Time order by Time ";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
            makeChart3(tbl, "TKM3 163 - 245");
            tbl.Dispose();

            query = "SELECT SUM(Live_Calls) as 'Live_Calls', Time FROM[CDRDB].[dbo].[CDRN] where Time >= DATEADD(DAY,0,left(getdate(),11)) and AdjacencyName = 'C_TKM4_Core' group by Time order by Time ";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
            makeChart4(tbl, "TKM4 164 - 190");
            tbl.Dispose();

            query = "SELECT SUM(Live_Calls) as 'Live_Calls', Time FROM[CDRDB].[dbo].[CDRN] where Time >= DATEADD(DAY,0,left(getdate(),11)) and AdjacencyName = 'C_TKM5_Core' group by Time order by Time ";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
            makeChart5(tbl, "TKM5 165 132");
            tbl.Dispose();

            query = "SELECT SUM(Live_Calls) as 'Live_Calls', Time FROM[CDRDB].[dbo].[CDRN] where Time >= DATEADD(DAY,0,left(getdate(),11)) and AdjacencyName = 'C_TKM6_Core' group by Time order by Time ";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
            makeChart6(tbl, "TKM6 166 - 18");
            tbl.Dispose();

            query = "SELECT SUM(Live_Calls) as 'Live_Calls', Time FROM[CDRDB].[dbo].[CDRN] where Time >= DATEADD(DAY,0,left(getdate(),11)) and AdjacencyName = 'C_TKM7_Core' group by Time order by Time ";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
            makeChart7(tbl, "C_TKM7_Core");
            tbl.Dispose();




        }
        else
        {
            Response.Redirect("~/SignIn.aspx");
        }

    }

    private void makeChart1(DataTable tbl, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0 )
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));                
                i++;
            }

            Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;
            Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;


            Chart1.Series["Ports"].ToolTip = "#VALX{t}\nPorts = #VALY{0,0}";
           
            Chart1.Titles["Title1"].Text = tITLE;
            
        }        
        else
        {
            this.Chart1.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";
        }

    }

    private void makeChart2(DataTable tbl, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                Chart2.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                i++;
            }

            Chart2.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;
            Chart2.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;


            Chart2.Series["Ports"].ToolTip = "#VALX{t}\nPorts = #VALY{0,0}";

            Chart2.Titles["Title1"].Text = tITLE;

        }
        else
        {
            this.Chart2.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";
        }

    }

    private void makeChart3(DataTable tbl, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                Chart3.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                i++;
            }

            Chart3.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;
            Chart3.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;


            Chart3.Series["Ports"].ToolTip = "#VALX{t}\nPorts = #VALY{0,0}";

            Chart3.Titles["Title1"].Text = tITLE;

        }
        else
        {
            this.Chart3.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";
        }

    }

    private void makeChart4(DataTable tbl, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                Chart4.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                i++;
            }

            Chart4.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;
            Chart4.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;


            Chart4.Series["Ports"].ToolTip = "#VALX{t}\nPorts = #VALY{0,0}";

            Chart4.Titles["Title1"].Text = tITLE;

        }
        else
        {
            this.Chart4.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";
        }

    }

    private void makeChart5(DataTable tbl, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                Chart5.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                i++;
            }

            Chart5.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;
            Chart5.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;


            Chart5.Series["Ports"].ToolTip = "#VALX{t}\nPorts = #VALY{0,0}";

            Chart5.Titles["Title1"].Text = tITLE;

        }
        else
        {
            this.Chart5.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";
        }

    }

    private void makeChart6(DataTable tbl, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                Chart6.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                i++;
            }

            Chart6.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;
            Chart6.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;


            Chart6.Series["Ports"].ToolTip = "#VALX{t}\nPorts = #VALY{0,0}";

            Chart6.Titles["Title1"].Text = tITLE;

        }
        else
        {
            this.Chart6.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";
        }

    }

    private void makeChart7(DataTable tbl, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                Chart7.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                i++;
            }

            Chart7.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;
            Chart7.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;


            Chart7.Series["Ports"].ToolTip = "#VALX{t}\nPorts = #VALY{0,0}";

            Chart7.Titles["Title1"].Text = tITLE;

        }
        else
        {
            this.Chart7.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";
        }

    }
    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
