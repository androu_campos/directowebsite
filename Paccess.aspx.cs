using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class Paccess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string provider = Session["DirectoUser"].ToString();
       
        
        if(Session["DirectoUser"].ToString() == "JollyC")
            provider = "ALLACCESSPH-SCRATCH' OR Customer = 'ALLACCESSPH-PINLESS";

        if (Session["DirectoUser"].ToString() == "ALL-ACCESS-PHIL")
        {
            //Session["DirectoUser"] = "ALL-ACCESS-PHIL' OR Customer ='ALLACCESSPH-SCRATCH' OR Customer = 'ALLACCESSPH-PINLESS";
            provider = "ALL-ACCESS-PHIL' OR Customer ='ALLACCESSPH-SCRATCH' OR Customer = 'ALLACCESSPH-PINLESS' OR Customer = 'ALLACCESS-UNLIMITED";
        }

        if (Session["DirectoUser"].ToString() == "lpicache")
        {
            //provider = "PHIP-CALLCENTER";
            provider = "ALLACCESSPH-SCRATCH' OR Customer = 'ALLACCESSPH-PINLESS";
            Session["hometype"] = ""; 
        }


        if (Session["DirectoUser"].ToString() == "Andres")
            provider = "ATSI";

        string customer = provider;
        string vendor = provider;

        vendor = vendor.Replace("Customer", "Vendor");


        if (Session["DirectoUser"].ToString() == "ALL-ACCESS-PHIL")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandText = "select 'Termination' as Direction, Vendor, TerminationIP as IP, Region,sum(Calls) as TotalCalls, round(Sum(Minutes),2) as TotalMinutes ,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetInc where Vendor = '" + vendor + "' group by Vendor, TerminationIP, Region union select 'Termination' as Direction, Vendor, OriginationIP as IP, Region,sum(Attempts)AS Attempts, sum(Calls) as TotalCalls,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], round(Sum(Minutes),2) as TotalMinutes, mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetInc where Customer = '" + customer + "' group by Vendor, OriginationIP, Region order by Region";
            sqlQry.CommandType = CommandType.Text;
            DataSet myDataset = Util.RunQuery(sqlQry);
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            Session["RcntProv"] = myDataset;

            sqlQry.CommandText = "select 'Termination' as Direction, Vendor, TerminationIP as IP, Region, sum(Calls) as TotalCalls, round(Sum(Minutes),2) as TotalMinutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetAll where Vendor = '" + vendor + "' group by Vendor, TerminationIP, Region union select 'Termination' as Direction, Vendor, OriginationIP as IP, Region,sum(Attempts)AS Attempts, sum(Calls) as TotalCalls,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], round(Sum(Minutes),2) as TotalMinutes, mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetAll where Customer = '" + customer + "' group by Vendor, OriginationIP, Region";
            myDataset = RunQuery(sqlQry);
            GridView2.DataSource = myDataset.Tables[0];
            GridView2.DataBind();
            Session["TodayProv"] = myDataset;
        }
        else if (Session["DirectoUser"].ToString() == "csolares")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandText = "select Customer, Vendor, Country, Region, Type, Class,sum(Calls) as TotalCalls, round(Sum(Minutes),2) as TotalMinutes ,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetInc where Country in ('GUATEMALA','EL SALVADOR') group by Customer, Vendor, Country, Region, Type, Class union select '*****' Customer, '*****' Vendor, '*****' Country, '*****' Region, '*****' Type, '*****' Class, sum(Attempts)AS Attempts, sum(Calls) as TotalCalls,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], round(Sum(Minutes),2) as TotalMinutes, mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetInc where COUNTRY IN ('GUATEMALA','EL SALVADOR') ORDER by Region";
            sqlQry.CommandType = CommandType.Text;
            DataSet myDataset = Util.RunQuery(sqlQry);
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            Session["RcntProv"] = myDataset;

            sqlQry.CommandText = "select Customer, Vendor, Country, Region, Type, Class,sum(Calls) as TotalCalls, round(Sum(Minutes),2) as TotalMinutes ,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetAll where Country in ('GUATEMALA','EL SALVADOR') group by Customer, Vendor, Country, Region, Type, Class union select '*****' Customer, '*****' Vendor, '*****' Country, '*****' Region, '*****' Type, '*****' Class, sum(Attempts)AS Attempts, sum(Calls) as TotalCalls,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], round(Sum(Minutes),2) as TotalMinutes, mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetAll where COUNTRY IN ('GUATEMALA','EL SALVADOR') ORDER by Region";
            myDataset = RunQuery(sqlQry);
            GridView2.DataSource = myDataset.Tables[0];
            GridView2.DataBind();
            Session["TodayProv"] = myDataset;
        }
        else if (Session["DirectoUser"].ToString() == "VERSITEL")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandText = "select 'Origination' as Direction, OriginationIP as IP, Region,sum(Calls) as TotalCalls,round(Sum(Minutes),2) as TotalMinutes ,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetInc where Customer = '" + customer + "' group by OriginationIP, Region union select '**ALL**' as Direction, '' as IP, '' as Region,sum(Calls) as TotalCalls,round(Sum(Minutes),2) as TotalMinutes,sum(Attempts)AS Attempts, sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetInc where Customer = '" + customer + "' order by TotalMinutes desc";
            sqlQry.CommandType = CommandType.Text;
            DataSet myDataset = Util.RunQuery(sqlQry);
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            Session["RcntProv"] = myDataset;

            sqlQry.CommandText = "select 'Origination' as Direction, OriginationIP as IP, Region,sum(Calls) as TotalCalls,round(Sum(Minutes),2) as TotalMinutes ,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetAll where Customer = '" + customer + "' group by OriginationIP, Region union select '**ALL**' as Direction, '' as IP, '' as Region,sum(Calls) as TotalCalls,round(Sum(Minutes),2) as TotalMinutes,sum(Attempts)AS Attempts, sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetAll where Customer = '" + customer + "' order by TotalMinutes desc";
            myDataset = RunQuery(sqlQry);
            GridView2.DataSource = myDataset.Tables[0];
            GridView2.DataBind();
            Session["TodayProv"] = myDataset;
        }
        else if (Session["DirectoUser"].ToString() == "divulge-telecom" || Session["DirectoUser"].ToString() == "adiptel") // Customer
        {

            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandText = "select 'Origination' as Direction, OriginationIP as IP, Country, Region,sum(Attempts)AS Attempts, sum(Calls) as TotalCalls,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], round(Sum(Minutes),2) as TotalMinutes, mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetInc where Customer = '" + customer + "' group by OriginationIP, Country, Region order by Region";
            sqlQry.CommandType = CommandType.Text;
            DataSet myDataset = Util.RunQuery(sqlQry);
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            Session["RcntProv"] = myDataset;

            sqlQry.CommandText = "select 'Origination' as Direction, OriginationIP as IP, Country, Region,sum(Attempts)AS Attempts, sum(Calls) as TotalCalls,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], round(Sum(Minutes),2) as TotalMinutes, mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetAll where Customer = '" + customer + "' group by OriginationIP, Country, Region";
            myDataset = RunQuery(sqlQry);
            GridView2.DataSource = myDataset.Tables[0];
            GridView2.DataBind();
            Session["TodayProv"] = myDataset;
        }
        else if (Session["DirectoUser"].ToString() == "ranaya-minute-serv")
        {

            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandText = "select 'Origination' as Direction, TerminationIP as IP, Country, Region, sum(Calls) as TotalCalls, round(Sum(Minutes),2) as TotalMinutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetInc where Vendor = '" + vendor + "' group by TerminationIP, Country, Region ";
            sqlQry.CommandType = CommandType.Text;
            DataSet myDataset = Util.RunQuery(sqlQry);
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            Session["RcntProv"] = myDataset;

            sqlQry.CommandText = "select 'Origination' as Direction, TerminationIP as IP, Country, Region, sum(Calls) as TotalCalls, round(Sum(Minutes),2) as TotalMinutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetAll where Vendor = '" + vendor + "' group by TerminationIP, Country, Region ";
            myDataset = RunQuery(sqlQry);
            GridView2.DataSource = myDataset.Tables[0];
            GridView2.DataBind();
            Session["TodayProv"] = myDataset;
        }
        else
        {

            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandText = "select 'Origination' as Direction, TerminationIP as IP, Region,sum(Calls) as TotalCalls, round(Sum(Minutes),2) as TotalMinutes ,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetInc where Vendor = '" + vendor + "' group by TerminationIP, Region union select 'Origination' as Direction, OriginationIP as IP, Region,sum(Attempts)AS Attempts, sum(Calls) as TotalCalls,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], round(Sum(Minutes),2) as TotalMinutes, mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetInc where Customer = '" + customer + "' group by OriginationIP, Region order by Region";
            sqlQry.CommandType = CommandType.Text;
            DataSet myDataset = Util.RunQuery(sqlQry);
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            Session["RcntProv"] = myDataset;

            sqlQry.CommandText = "select 'Origination' as Direction, TerminationIP as IP, Region, sum(Calls) as TotalCalls, round(Sum(Minutes),2) as TotalMinutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetAll where Vendor = '" + vendor + "' group by TerminationIP, Region union select 'Origination' as Direction, OriginationIP as IP, Region,sum(Attempts)AS Attempts, sum(Calls) as TotalCalls,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls], round(Sum(Minutes),2) as TotalMinutes, mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR from mecca2.dbo.OpSheetAll where Customer = '" + customer + "' group by OriginationIP, Region";
            myDataset = RunQuery(sqlQry);
            GridView2.DataSource = myDataset.Tables[0];
            GridView2.DataBind();
            Session["TodayProv"] = myDataset;
        }

    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        GridView2.DataBind();
    }
    protected void cmdExport1_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["RcntProv"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "Recent" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string error = ex.Message;
        }
    }
    protected void cmdExport2_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["TodayProv"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "Today" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string error = ex.Message;
        }
    }
}
