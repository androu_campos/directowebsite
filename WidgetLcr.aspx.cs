using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class WidgetLcr : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        giveAccess();
        
        if (!Page.IsPostBack)
        {
            txtReceived.Text = System.DateTime.Now.AddDays(-1).ToShortDateString();
        }
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        if (Session["DirectoUser"].ToString() == "Murray" || Session["DirectoUser"].ToString() == "xavier" || Session["DirectoUser"].ToString() == "yurema")
        {
            int i = e.NewEditIndex;
        }
        else
        {
            e.Cancel = true;            
        }
        
        
    }
    protected void GridView1_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        
        
        
        

    }
    protected void cmdInsert_Click(object sender, EventArgs e)
    {
        MECCA2TableAdapters.WidgetEntryAdp adp = new MECCA2TableAdapters.WidgetEntryAdp();
        adp.Insert(dpdCountryLcr.SelectedValue, dpdOwner.SelectedItem.ToString(), dpdStatus.SelectedItem.ToString(),Convert.ToInt32(dpdPriority.SelectedValue), Session["DirectoUser"].ToString(),Convert.ToDateTime(txtReceived.Text + " " + DateTime.Now.ToShortTimeString() ),null,dpdCtrys.SelectedValue.ToString(),rdbTypeOf.SelectedValue.ToString(),txtComments.Text);
        GridView1.DataBind();
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {


        

    }
    

    protected void giveAccess()
    {
        if (Session["DirectoUser"].ToString() == "Murray" || Session["DirectoUser"].ToString() == "yurema" || Session["DirectoUser"].ToString() == "xavier" || Session["DirectoUser"].ToString() == "oscar" || Session["DirectoUser"].ToString() == "angel" || Session["DirectoUser"].ToString() == "michelle" || Session["DirectoUser"].ToString() == "vicente")
        {
            cmdInsert.Enabled = true;
        }
        else
        {
            cmdInsert.Enabled = false;
        }
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {

    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

      
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (Session["DirectoUser"].ToString() == "Murray" || Session["DirectoUser"].ToString() == "xavier" || Session["DirectoUser"].ToString() == "yurema")
        {
            int i = e.RowIndex;
        }
        else
        {
            e.Cancel = true;
        }
    }
}
