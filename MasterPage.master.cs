using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    void Page_PreInit(object sender, EventArgs e)
    {
        Master.EnableTheming = true;
        Page.EnableTheming = true;
        Page.Theme = "Theme1";
        Page.StyleSheetTheme = "Theme1";

        SessionUsuario SesUs = new SessionUsuario();

        if (!SesUs.EsSessionValida())
            Response.Redirect("~/SignIn.aspx");

        if (!string.IsNullOrEmpty(Session["DirectoUser"] as string))//Usuario Logeado            
        {
            Session["DirectoUser"] = Request.Cookies["userMecca"].Value.ToString();

            if (Session["DirectoUser"].ToString() != "jim.baumhart")
            {

            }
            else if (Session["DirectoUser"].ToString() == "jim.baumhart")
            {
                Response.Redirect("~/ContactUs.aspx");
            }
        }
        else
        {
            Response.Redirect("~/SignIn.aspx");
        }

        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UserAgent.IndexOf("Chrome") > 0)
        {
            if (Request.Browser.Adapters.Count > 0)
            {
                Request.Browser.Adapters.Clear();
                Response.Redirect(Page.Request.Url.AbsoluteUri);
            }
        }

        try
        {

            MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adpBR = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
            MyDataSetTableAdapters.Billing_StatusDataTable tblBR = adpBR.GetData();

            string hometype = "";

            if (tblBR.Rows.Count > 0)//Billing running
            {
                lblBillingRunning.Visible = true;

            }
            else
            {
                lblBillingRunning.Visible = false;
            }
            //if (Session["Rol"].ToString() != string.Empty || (Session["Rol"].ToString() == "Customer" && Session["DirectoUser"].ToString() == "lpicache"))//Usuario Logeado            
            //if (Session["Rol"].ToString() != string.Empty && Session["Rol"].ToString() != "Customer" && Session["Rol"].ToString() != "PHAccess")//Usuario Logeado            
            //{


            //    if (Request.Cookies["Rol"] != null)
            //    {
            //        Session["Rol"] = Request.Cookies["Rol"].Value.ToString();
            //        Session["DirectoUser"] = Request.Cookies["userMecca"].Value.ToString();
            //    }

            //    lblUserName.Text = Session["DirectoUser"].ToString();
            //    string urlrequested = Request.RawUrl.ToString();
            //    int urlLength = urlrequested.Length;
            //    int index = urlrequested.LastIndexOf("/");
            //    int sufix = urlrequested.LastIndexOf("aspx");
            //    string url = urlrequested.Substring(index, sufix - index + 4);

            //    url = "~" + url;

            //    MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpS = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
            //    MyDataSetTableAdapters.MainNodesDataTable tblS = adpS.GetDataByUrlnRol(url, Session["Rol"].ToString());

            //    MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpS1 = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
            //    MyDataSetTableAdapters.MainNodesDataTable tblS1 = adpS1.GetDataByURLnRol2(url, Session["Rol"].ToString());


            //    if (tblS.Count == 0 && Session["Rol"].ToString() != "Admins" && tblS1.Count == 0)//Verifica si el usuario tiene acceso a esta pagina
            //    {
            //        Response.Redirect("ContactUs.aspx", false);
            //    }
            //}
            //else
            //{
            //    Response.Redirect("SignIn.aspx", false);
            //}

        }

        catch (Exception ex)
        {
            string errormessage = ex.Message.ToString();

        }

    }
}