<%@ Page Language="C#" MasterPageFile="~/Master.master" StylesheetTheme="Theme1" AutoEventWireup="true" CodeFile="RatesAssign.aspx.cs" Inherits="RatesAssign" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table style="position:absolute; top:220px; left:230px;">
        <tr>
            <td style="width: 100px" valign="top" align="left">
            </td>
            <td style="width: 105px" valign="top">
                <asp:Label ID="Label2" runat="server" Text="Rates Variations" CssClass="labelBlue8" Visible="False"></asp:Label></td>
            <td style="width: 105px" valign="top">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 100px" valign="top" align="left">
    <asp:GridView ID="gvStatistics" runat="server" Font-Names="Arial" Font-Size="8pt">
    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
            </td>
            <td style="width: 105px" valign="top" align="left">
    <asp:GridView ID="gvVariations" runat="server" Font-Names="Arial" Font-Size="8pt">
    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
            </td>
            <td align="left" style="width: 105px" valign="top">
                <table>
                    <tr>
                        <td style="width: 100px">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/RatesUpload.aspx" Width="87px">Upload Rates</asp:HyperLink></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/RateSchedule.aspx" Width="87px">Rate Schedule</asp:HyperLink></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px" valign="top">
                <asp:Button ID="btnCheck" runat="server" OnClick="btnCheck_Click" Text="Check Schedules" CssClass="boton" Width="89px" /></td>
            <td align="left" style="width: 105px" valign="top">
            </td>
            <td align="left" style="width: 105px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 169px;" valign="top">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8"></asp:Label>
                <asp:GridView ID="gvAlready" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                    DataSourceID="SqlDsratesAlready" Visible="False" Font-Names="Arial" Font-Size="8pt">
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" DeleteText="Deleted" />
                        
                        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                            SortExpression="ID" />
                        <asp:BoundField DataField="ProviderName" HeaderText="ProviderName" SortExpression="ProviderName" />
                        <asp:BoundField DataField="Rates" HeaderText="Rates" SortExpression="Rates" />
                        <asp:BoundField DataField="Applyday" HeaderText="Applyday" SortExpression="Applyday" />
                        <asp:BoundField DataField="Uploaded" HeaderText="Uploaded" SortExpression="Uploaded" />
                    </Columns>
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td style="width: 105px; height: 169px;" valign="top">                
                <table style="width: 247px; height: 50px">
                    <tr>
                        <td style="width: 113px" align="left">
                </td>
                        <td align="left" style="width: 57px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 113px" align="left" valign="top">
                <asp:TextBox ID="txtApply" runat="server" Visible="False" CssClass="labelSteelBlue" Width="81px" OnTextChanged="txtApply_TextChanged"></asp:TextBox>
                            &nbsp; &nbsp;
                        </td>
                        <td align="left" style="width: 57px" valign="top">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" Visible="False" /></td>
                        <td style="width: 100px" align="left" valign="top">
                <asp:Button ID="btnApply" runat="server" OnClick="btnApply_Click"
                    Text="Apply Day" Visible="False" CssClass="boton" /></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 113px" valign="top">
                            <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="Invalid Date"
                                Visible="False"></asp:Label></td>
                        <td align="left" style="width: 57px" valign="top">
                        </td>
                        <td align="left" style="width: 100px" valign="top">
                        </td>
                    </tr>
                </table>
                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="Image1"
                    TargetControlID="txtApply">
                </cc1:CalendarExtender>
            </td>
            <td style="width: 105px; height: 169px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" valign="top">
                &nbsp;<asp:ScriptManager id="ScriptManager1" runat="server"></asp:ScriptManager>
                </td>
            <td style="width: 105px" valign="top">
                </td>
            <td style="width: 105px" valign="top">
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="SqlDsratesAlready" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBLocalConnectionString %>"
        DeleteCommand="sp_RateDeleteSchedule" ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>"
        SelectCommand="SELECT [ID]&#13;&#10;      ,[ProviderName]&#13;&#10;      ,[Rates]&#13;&#10;      ,[Applyday]&#13;&#10;      ,[Uploaded]&#13;&#10;  FROM [CDRDB].[dbo].[RateSchedules]&#13;&#10;Order by providername" DeleteCommandType="StoredProcedure">
        <DeleteParameters>
            <asp:ControlParameter ControlID="gvAlready" Name="ID" PropertyName="SelectedValue"
                Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="ty" SessionField="ww" Type="String" />
        </DeleteParameters>
    </asp:SqlDataSource>
                <asp:Label ID="Label1" runat="server" Text="Rate Assign" CssClass="labelTurkH"></asp:Label>



</asp:Content>

