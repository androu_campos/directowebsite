using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class DidDailyTraffic : System.Web.UI.Page
{
    

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void gvDailyReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDailyReport.PageIndex = e.NewPageIndex;
        gvDailyReport.DataSource = Session["DidDailyTraffic"];
        gvDailyReport.DataBind();
    }

    protected void cmdShow_Click(object sender, EventArgs e)
    {
        string sql = string.Empty;

        sql = "Select '**ALL**' BillingDate ,'**ALL**' Customer,'**ALL**' Vendor,'**ALL**' Country,'**ALL**' Region, '**ALL**' State,sum(Custminutes) as [Minutes], "
            + "sum(BillableCalls) as [AnsweredCalls], sum(tCalls) as [Attempts],sum(RA) as [Rejected] "
            + "from ics..TrafficLog where Billingdate between '" + TextBox1.Text + " 00:00:00' and  '" + TextBox2.Text + " 23:59:59' and region <> 'UNKNOWN' "
            + "UNION "
            + "Select CONVERT(varchar,BillingDate,103) BillingDate, Customer, Vendor, Country,   substring(Region,0,charindex('-',Region)-1) Region, substring(Region,charindex('-',Region)+2,len(Region)) State,sum(Custminutes) as [Minutes], "
            + "sum(BillableCalls) as [AnsweredCalls], sum(tCalls) as [Attempts],sum(RA) as [Rejected] "
            + "from ics..TrafficLog where Billingdate between '" + TextBox1.Text + " 00:00:00' and  '" + TextBox2.Text + " 23:59:59' and region <> 'UNKNOWN' group by CONVERT(varchar,BillingDate,103), Customer,Vendor, Country,  substring(Region,0,charindex('-',Region)-1),substring(Region,charindex('-',Region)+2,len(Region)) ";

        SqlCommand sqlC = new SqlCommand();
        sqlC.CommandText = sql;

        DataSet myDataset = RunQuery(sqlC);

        Session["DidDailyTraffic"] = myDataset;

        if (myDataset.Tables[0].Rows.Count > 0)
        {
            gvDailyReport.DataSource = myDataset.Tables[0];
            gvDailyReport.DataBind();

            cmdExcelExport.Visible = true;
            //cmdExportC.Visible = true;
            //Label4.Visible = false;
        }
        else
        {
            cmdExcelExport.Visible = false;
        //    //cmdExportC.Visible = false;
        //    //Label4.Visible = true;
        }

    }

    protected void cmdExport(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["DidDailyTraffic"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "DidDailyTraffic" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["ICSConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

        return resultsDataSet;
    }


}
