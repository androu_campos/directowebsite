<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PushListEdit.aspx.cs" Inherits="PushListEdit" Title="Untitled Page" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblHeader" runat="server" CssClass="labelBlue8" Text="Push List Edit" Font-Bold="True" Width="100%"></asp:Label></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td colspan="2" height="25">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:GridView ID="GridView1" runat="server" AllowSorting="True" CssClass="dropdown" DataKeyNames="Id"
                    DataSourceID="SqlDataSource2" OnRowDeleting="GridView1_RowDeleting" Width="100%" AutoGenerateColumns="False" AllowPaging="True" PageSize="25">
                    <HeaderStyle CssClass="titleOrangegrid" />
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                        <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True"
                            SortExpression="Id" />
                        <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
                        <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                        <asp:BoundField DataField="Average_Cost" HeaderText="Average_Cost" SortExpression="Average_Cost" />
                        <asp:BoundField DataField="Expected_ASR" HeaderText="Expected_ASR" SortExpression="Expected_ASR" />
                        <asp:BoundField DataField="Yesterday_ACD" HeaderText="Yesterday_ACD" SortExpression="Yesterday_ACD" />
                        <asp:BoundField DataField="Avail_Capacity" HeaderText="Avail_Capacity" SortExpression="Avail_Capacity" />
                        <asp:BoundField DataField="Service_Level" HeaderText="Service_Level" SortExpression="Service_Level" />
                        <asp:BoundField DataField="Comments" HeaderText="Comments" SortExpression="Comments" />
                        <asp:BoundField DataField="Vendor_Billing" HeaderText="Vendor_Billing" SortExpression="Vendor_Billing" />
                        <asp:BoundField DataField="Date_Updated" HeaderText="Date_Updated" SortExpression="Date_Updated" />
                        <asp:BoundField DataField="Minimum" HeaderText="Minimum" SortExpression="Minimum" />
                    </Columns>
                </asp:GridView>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 44px;">
            </td>
            <td style="width: 100px; height: 44px;">
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" 
                SelectCommand="SELECT idEntry, countryLcr, owner, status, priority, Date_received, Date_Completed, RequestedBy, TypeOf, Comments, Minimum FROM WidgetEntry" UpdateCommand="UPDATE WidgetEntry SET countryLcr = @countryLcr, owner = @owner, status = @status, priority = @priority, Date_received = @Date_received, Date_Completed = @Date_Completed, RequestedBy = @RequestedBy, Comments = @Comments, Minimum=@Minimum WHERE (idEntry = @idEntry)" DeleteCommand="DELETE FROM WidgetEntry WHERE (idEntry = @idEntry)">
                    <UpdateParameters>
                        <asp:Parameter Name="countryLcr" />
                        <asp:Parameter Name="owner" />
                        <asp:Parameter Name="status" />
                        <asp:Parameter Name="priority" />
                        <asp:Parameter Name="Date_received" />
                        <asp:Parameter Name="Date_Completed" />
                        <asp:Parameter Name="RequestedBy" />
                        <asp:Parameter Name="idEntry" />
                        <asp:Parameter Name="Comments" />
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="idEntry" />
                    </DeleteParameters>
                 
                </asp:SqlDataSource> 
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                    DeleteCommand="DELETE FROM Push_List WHERE (Id = @Id)" SelectCommand="SELECT Id, Country, City, Average_Cost, Expected_ASR, Yesterday_ACD, Avail_Capacity, Service_Level, Comments, Vendor_Billing, Date_Updated, Minimum FROM Push_List"
                    UpdateCommand="UPDATE Push_List SET  Country=@Country, City=@City, Average_Cost=@Average_Cost, Expected_ASR=@Expected_ASR, Yesterday_ACD=@Yesterday_ACD, Avail_Capacity=@Avail_Capacity, Service_Level=@Service_Level, Comments=@Comments, Vendor_Billing=@Vendor_Billing, Date_Updated = @Date_Updated, Minimum=@Minimum WHERE (Id = @Id)">
                    <DeleteParameters>
                        <asp:Parameter Name="Id" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Id" />
                        <asp:Parameter Name="Country" />
                        <asp:Parameter Name="City" />
                        <asp:Parameter Name="Average_Cost" />
                        <asp:Parameter Name="Expected_ASR" />
                        <asp:Parameter Name="Yesterday_ACD" />
                        <asp:Parameter Name="Avail_Capacity" />
                        <asp:Parameter Name="Service_Level" />
                        <asp:Parameter Name="Comments" />
                        <asp:Parameter Name="Vendor_Billing" />
                        <asp:Parameter Name="Date_Updated" />
                        <asp:Parameter Name="Minimum" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </td>
            <td style="width: 100px; height: 44px;">
            </td>
        </tr>
    </table>

</asp:Content>

