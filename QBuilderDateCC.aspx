<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="QBuilderDateCC.aspx.cs"
    Inherits="QBuilderDateCC" Title="DIRECTO - Connections Worldwide" Theme="Theme1"
    StylesheetTheme="Theme1" EnableTheming="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblTitle" runat="server" CssClass="labelTurkH" Text="QUERY BUILDER BY DATE"
        Font-Bold="True">
    </asp:Label>
    <br>
    <asp:Label ID="Label2" runat="server" Font-Bold="False" Font-Names="Arial" ForeColor="#004A8F"
        Text="" Width="450px" Font-Size="8pt"></asp:Label>
    <table style="position: absolute; top: 220px; left: 228px;">
        <tr>
            <td style="width: 1088px; height: 155px;" valign="top">
                <table>
                    <tr>
                        <td style="width: 100px" valign="top" align="left" bgcolor="#d5e3f0">
                            <asp:Label ID="LabelS1" runat="server" Font-Bold="False" Text="" CssClass="labelBlue8"></asp:Label>
                        </td>
                        <td style="width: 300px" valign="top" align="left" bgcolor="#d5e3f0">
                            <asp:Label ID="Label1" runat="server" Font-Bold="False" Font-Names="Arial" ForeColor="#004A8F"
                                Text="Choose Range Of Days" Width="282px" Font-Size="8pt"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="labelFrom" runat="server" Text="From:" Font-Bold="False" CssClass="labelSteelBlue" />
                        </td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:TextBox ID="txtFromV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                            &nbsp;&nbsp;<asp:Image ID="imgFrom" runat="server" ImageUrl="~/Images/calendar1.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="labelTo" runat="server" Text="To:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label>
                        </td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:TextBox ID="txtToV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                            &nbsp;&nbsp;<asp:Image ID="imgTo" runat="server" ImageUrl="~/Images/calendar1.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="labelAgency" runat="server" Text="Agency:" Font-Bold="False" CssClass="labelSteelBlue"
                                Visible="false"></asp:Label>
                        </td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:DropDownList ID="ddlAgency" runat="server" Width="190px" Font-Names="Arial"
                                CssClass="labelSteelBlue" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlAgency_SelectedIndexChanged">
                                <asp:ListItem Text="**ALL**" Value="ALL"></asp:ListItem>
                                <asp:ListItem Text="BENTO" Value="BENTO"></asp:ListItem>
                                <asp:ListItem Text="CONTACTO" Value="CONTACTO"></asp:ListItem>
                                <asp:ListItem Text="ECI" Value="ECI"></asp:ListItem>
                                <asp:ListItem Text="ERGON" Value="ERGON"></asp:ListItem>
                                <asp:ListItem Text="MEGACALL" Value="MEGACAL"></asp:ListItem>
                                <asp:ListItem Text="MKT911" Value="MKT911"></asp:ListItem>
                                <asp:ListItem Text="STO" Value="STO"></asp:ListItem>
                                <asp:ListItem Text="TARGETONE" Value="TARGETONE"></asp:ListItem>
                                <asp:ListItem Text="VCIP" Value="VCIP"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="lblCC" runat="server" Text="CallCenter:" Font-Bold="False" CssClass="labelSteelBlue"
                                Visible="false"></asp:Label>
                        </td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:DropDownList ID="ddlCC" runat="server" Width="190px" Font-Names="Arial" CssClass="labelSteelBlue"
                                Visible="false" Enabled="false">
                                <asp:ListItem Text="**ALL**" Value="ALL"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <br />
                <table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="SubmitClick" Font-Names="Arial"
                                CssClass="boton" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 1088px">
                <asp:GridView ID="gdvData" runat="server" Visible="false" Font-Names="Arial" Font-Size="8pt"
                    AllowPaging="true" PageSize="20" OnPageIndexChanging="gdvData_PageIndexChanging">
                    <HeaderStyle CssClass="titleOrangegrid" Height="40px" Width="150px" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <AlternatingRowStyle BackColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="width: 1088px">
                <asp:Button ID="btnExcel" runat="server" Text="EXCEL" OnClick="exportData" Font-Names="Arial"
                    CssClass="boton" Visible="false" />
            </td>
        </tr>
    </table>
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromV"
        PopupButtonID="imgFrom" Format="M/d/yyyy">
    </cc1:CalendarExtender>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToV"
        PopupButtonID="imgTo" Format="M/d/yyyy">
    </cc1:CalendarExtender>
</asp:Content>
