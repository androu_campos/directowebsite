using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Invoice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string customer = Request.QueryString[0].ToString();

        string to = Request.QueryString[1].ToString();
        to = to.Replace("_", "/");

        string startDate = "2007/10/22";

        DataTable tblHeader = Util.RunQueryByStmntatCDRDB("Select * from Invoice where MeccaName ='" + customer + "'").Tables[0];
        int tip = Convert.ToInt32(tblHeader.Rows[0]["Tip"].ToString());
        if (tip == 1)
        {
            startDate = DateTime.Now.AddDays(-7).ToShortDateString();
        }
        else if (tip == 2)
        {
            startDate = DateTime.Now.AddDays(-15).ToShortDateString();
        }
        else
        {
            startDate = DateTime.Now.AddDays(-30).ToShortDateString();
        }
//GRID 1
        DataTable tbl_1 = Util.RunQueryByStmnt("select LEFT(Billingdate,11) as [Date],sum(Calls) as [Completed Calls], Sum(CustMinutes) as [Billed Minutes], sum(TotalSales) as Amount from Mecca2.dbo.SalesLog where Customer like '" + customer + "'  and BillingDate >= '" + startDate + "'  and BillingDate <= '" + to + "' group by Billingdate, Customer order by Billingdate").Tables[0];
        gdv1.DataSource = tbl_1;
        gdv1.DataBind();
//GRID 2
        DataTable tbl_2 = Util.RunQueryByStmnt("select sum(Calls) as [Completed Calls], Sum(CustMinutes) as [Billed Minutes], Price, sum(TotalSales) as Amount from Mecca2.dbo.SalesLog where Customer like '" +  customer + "'  and BillingDate >= '" + startDate + "' and BillingDate <= '" + to + "' group by Customer, Price having sum(CustMinutes)>0").Tables[0];
        gdv2.DataSource = tbl_2;
        gdv2.DataBind();
//GRID 3
        DataTable tbl_3 = Util.RunQueryByStmnt("select sum(Calls) as [Total Calls], Sum(CustMinutes) as [Total Minutes], '******' as Price, sum(TotalSales) as [Total Amount] from Mecca2.dbo.SalesLog where Customer like '" + customer + "'  and BillingDate >= '" + startDate + "' and BillingDate <= '" + to + "' group by Customer order by sum(TotalSales)").Tables[0];
        gdv3.DataSource = tbl_3;
        gdv3.DataBind();

//Invoice Summary
        lblBillDate.Text = "Bill Date: " + DateTime.Now.ToShortDateString();

        string amoundDue = tbl_3.Rows[0][3].ToString();
        int f = amoundDue.IndexOf(".");
        lblAmoundDue.Text = "Amound Due: $" + amoundDue.Substring(0, f + 4);
        lblAmoundDue.ForeColor = System.Drawing.Color.Red;

//Billing Period
        lblBillingPeriod.Text = "Billing Period: " + startDate + " - " + to;
        DateTime due = Convert.ToDateTime(to);
        lblDueDate.Text = "Due Date: " + due.AddDays(Convert.ToInt32(tblHeader.Rows[0]["DueDate"])).ToShortDateString();


//
        lblCurrentCharges.Text = "Current Charges: " + amoundDue.Substring(0, f + 4);
        lblTotDue.Text = "Total Due: " + amoundDue.Substring(0, f + 4);

        lblInvoice.Text = Request.QueryString[2].ToString();
//        
        lblCustomerH.Text = tblHeader.Rows[0]["MeccaName"].ToString();
        lblAddressH.Text = tblHeader.Rows[0]["Address"].ToString() + "," + tblHeader.Rows[0]["ZipCode"].ToString() + "," + tblHeader.Rows[0]["ST"].ToString();
        lblPhoneH.Text = tblHeader.Rows[0]["Phone"].ToString();


    }
}
