using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class qbuilderbydate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string cmd = string.Empty;
        string hometype = string.Empty;

        if (!Page.IsPostBack)
        {
            txtFrom.Value = DateTime.Today.AddDays(-1).ToShortDateString();
            txtTo.Value = DateTime.Today.AddDays(-1).ToShortDateString();


            txtToV.Text = txtTo.Value.ToString();
            txtFromV.Text = txtFrom.Value.ToString();
            SqlConnection SqlConn = new SqlConnection();
            SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";            
            SqlCommand SqlCommand7 = new SqlCommand("SELECT DISTINCT Class FROM Regions WHERE Class is not NULL UNION SELECT '** NONE **' AS Class FROM Regions UNION SELECT '**ALL**' AS Class FROM Regions ORDER BY Class", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader7 = SqlCommand7.ExecuteReader();
            while (myReader7.Read())
            {
                dpdClass.Items.Add(myReader7.GetValue(0).ToString());
            }
            myReader7.Close();
            SqlConn.Close();

            SqlConnection SqlConn1 = new SqlConnection();
            SqlCommand SqlCommand1;
            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";

            if (Session["hometype"] != string.Empty)
            {
                hometype = Session["hometype"].ToString();

                if (hometype == "R")
                    SqlCommand1 = new SqlCommand("SELECT DISTINCT ProviderName AS Customer from ProviderIP WHERE Type like 'C' AND Providername like 'ICS%' and providername not in (select providername from ICS.dbo.DIDProviders) UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT '** NONE **' from ProviderIP as ProviderName UNION SELECT 'ICS-FIRSTU2' UNION SELECT 'ICS-FIRSTU5' order by providername ", SqlConn1);

            }
            else
            {
                cmd = "SELECT DISTINCT PROVIDERNAME AS Customer INTO #CustomerList FROM CDRDB.DBO.PROVIDERIP WHERE [TYPE] = 'C' \n ";
                cmd += "INSERT INTO #CustomerList SELECT '**ALL**' \n ";
                cmd += "INSERT INTO #CustomerList SELECT '** NONE **' \n ";
                cmd += "UPDATE #CustomerList SET Customer = 'ICS' WHERE Customer like 'ICS%' \n ";
                cmd += "UPDATE #CustomerList SET Customer = 'TALKTEL' WHERE Customer IN (SELECT CUSTOMER FROM CDRDB.DBO.CUSTBROKERREL WHERE CUSTBROKER = 'CallCenters' AND CUSTOMER NOT LIKE 'CCPH%' AND CUSTOMER NOT LIKE 'CC-PHIP%' ) \n ";
                cmd += "SELECT DISTINCT Customer FROM #CustomerList order by Customer \n ";


                //SqlCommand1 = new SqlCommand("SELECT DISTINCT (CASE WHEN ProviderName LIKE 'ICS%' THEN 'ICS' ELSE ProviderName END) ProviderName from ProviderIP WHERE Type like 'C' UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT '** NONE **' from ProviderIP as ProviderName", SqlConn1);                
            }

            SqlCommand1 = new SqlCommand(cmd, SqlConn1);
            DataSet dataSet = RunQuery(SqlCommand1);

            DataTable dataTable = dataSet.Tables[0];

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                ddlCust.Items.Add(dataTable.Rows[i][0].ToString());
            }

        }

    }

    protected void ddlCust_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlCust.SelectedItem.ToString() == "**ALL**") hfCust.Value = "%";
        else hfCust.Value = ddlCust.SelectedItem.ToString();

    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlVendor.SelectedItem.ToString() == "**ALL**") hfVendor.Value = "%";
        else hfVendor.Value = ddlVendor.SelectedItem.ToString();

    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {

        string Coun;

        if (ddlCountry.SelectedItem.ToString() == "**ALL**") Coun = "%";
        else Coun = ddlCountry.SelectedItem.ToString();
        hfCountry.Value = Coun;
        ddlRegion.Enabled = true;
        ddlRegion.Items.Clear();
        sqldsRegion.SelectCommand = "SELECT  '**ALL**' as Region FROM CDRDB..Regions UNION SELECT DISTINCT Region FROM CDRDB..Regions UNION SELECT '** NONE **' as Region FROM CDRDB..Regions WHERE Country like '" + Coun + "' ORDER BY Region";
        ddlLmc.Enabled = true;
        ddlLmc.Items.Clear();
        sqldsLmc.SelectCommand = "SELECT  '**ALL**' as Lmc FROM CDRDB..Regions UNION SELECT DISTINCT Lmc FROM CDRDB..Regions UNION SELECT '** NONE **' as Lmc FROM CDRDB..Regions WHERE Country like '" + Coun + "' ORDER BY Lmc";
        
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {                        
        /**/       
            ckboxl.ClearSelection();
            string businessunit = "", custunits = "";

            //CUSTOMER
            if (ddlCust.SelectedValue.ToString() != "** NONE **")
            {
                ckboxl.Items[0].Selected = true;
                ckboxl.Items[14].Selected = true;//CMinutes
                ckboxl.Items[20].Selected = false;//CACD
              
            }
            else
            {
                ckboxl.Items[0].Selected = false;
                ckboxl.Items[14].Selected = false;//CMinutes
                ckboxl.Items[20].Selected = true;//CACD
            }
            //VENDOR
            if (ddlVendor.SelectedValue.ToString() != "** NONE **")
            {
                ckboxl.Items[1].Selected = true;
                ckboxl.Items[15].Selected = true;//VMinutes
                ckboxl.Items[20].Selected = true;//ACD
            }
            else
            {
                ckboxl.Items[1].Selected = false;
                ckboxl.Items[15].Selected = true;//VMinutes
                ckboxl.Items[20].Selected = true;//VACD
            }

            //COUNTRY
            if (ddlCountry.SelectedValue.ToString() != "** NONE **")
            {
                ckboxl.Items[4].Selected = true;
            }
            else
            {
                ckboxl.Items[4].Selected = false;
            }

            //REGION
            if (ddlRegion.SelectedValue.ToString() != "** NONE **" && ddlRegion.SelectedValue.ToString() != "")
            {
                ckboxl.Items[5].Selected = true;
            }
            //else if (ddlRegion.Enabled == false)
            //{
            //    ckboxl.Items[5].Selected = false;
            //}
            else
            {
                ckboxl.Items[5].Selected = false;
            }
            if (ddlLmc.SelectedValue.ToString() != "** NONE **" && ddlLmc.SelectedValue.ToString() != "")
            {
                ckboxl.Items[10].Selected = true;
            }
            else
            {
                ckboxl.Items[10].Selected = false;
            }
            


        
        ckboxl.Items[13].Selected = true;//TotCalls                                
        ckboxl.Items[19].Selected = true;//ASR
        ckboxl.Items[21].Selected = true;//ABR
        ckboxl.Items[22].Selected = true;//ATTEMPTS
        ckboxl.Items[23].Selected = true;//RA


                     
        /**/
        string SelectString, WhereString, FromString, FromSBro = "", GroupByString, UnionString, QQ, sOrder, BrokerWhere;
        string sF, sW, sS, sSt, sG, sSUM = "";
        bool bCV = false, bVB = false;

        //checando valores no asignados
        if (ddlRegion.Enabled == false)
        {
            hfRegion.Value = "%";
        }
        else
        {
            if (ddlRegion.SelectedValue.ToString() == "**ALL**") hfRegion.Value = "%";
            else if (ddlRegion.SelectedValue.ToString() == "** NONE **") hfRegion.Value = "%";
            else hfRegion.Value = ddlRegion.SelectedItem.ToString();
        }

        if (ddlLmc.Enabled == false)
        {
            hfLmc.Value = "%";
        }
        else
        {
            if (ddlLmc.SelectedValue.ToString() == "**ALL**") hfLmc.Value = "%";
            else if (ddlLmc.SelectedValue.ToString() == "** NONE **") hfLmc.Value = "%";
            else hfLmc.Value = ddlLmc.SelectedItem.ToString();
        }
           

        if (ddlCountry.SelectedItem.ToString() == "**ALL**") hfCountry.Value = "%";
        else if (ddlCountry.SelectedItem.ToString() == "** NONE **") hfCountry.Value = "%";
        else hfCountry.Value = ddlCountry.SelectedItem.ToString();

        if (hfCust.Value.ToString() == "") hfCust.Value = "%";
        if (hfVendor.Value.ToString() == "") hfVendor.Value = "%";
        if (hfCustBroker.Value.ToString() == "") hfCustBroker.Value = "%";
        if (hfVendorBroker.Value.ToString() == "") hfVendorBroker.Value = "%";
        //FIN checando valores no asignados

        SelectString = "";
        BrokerWhere = "";
        FromString = " FM2TL";

        WhereString = "";

        GroupByString = "";
        UnionString = "";

        //WHERE Builder

        string from = txtFromV.Text;
        string to = txtToV.Text;
        from = from.Replace("/", "|");
        to = to.Replace("/", "|");


        WhereString = WhereString + "'" + from + "' AND '" + to + "'";
        if (hfCust.Value.ToString() != "%")
        {
            businessunit = hfCust.Value.ToString();
            custunits = getCustomer(businessunit);

            if (custunits != "'-'")
                WhereString = WhereString + " AND T.Customer in ( " + custunits + ") ";            
            else
                WhereString = WhereString + " AND T.Customer LIKE '" + hfCust.Value.ToString() + "'";
        }
        if (hfVendor.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Vendor LIKE '" + hfVendor.Value.ToString() + "'";
        }
        if (hfCountry.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Country LIKE '" + hfCountry.Value.ToString() + "'";
        }
        if (hfRegion.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Region LIKE '" + hfRegion.Value.ToString() + "'";
        }
        if (hfLmc.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Lmc LIKE '" + hfLmc.Value.ToString() + "'";
        }
        if (hfCustBroker.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND B.CustBroker LIKE '" + hfCustBroker.Value.ToString() + "'";
            FromSBro = FromSBro + " LJM2CB";
            bCV = true;
        }
        if (hfVendorBroker.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND V.VendorBroker LIKE '" + hfVendorBroker.Value.ToString() + "'";
            FromSBro = FromSBro + " LJM2VB";
            bVB = true;
        }

        
        if (dpdType.SelectedValue.ToString() != "** NONE **" && dpdType.SelectedValue.ToString() != "**ALL**")
        {
            WhereString = WhereString + " AND R.Type LIKE '" + dpdType.SelectedValue.ToString() + "'";
            //FromSBro = FromSBro + " LJM2R";
        
        }


        if (dpdClass.SelectedValue.ToString() != "** NONE **" && dpdClass.SelectedValue.ToString() != "**ALL**")
        {
            WhereString = WhereString + " AND R.Class LIKE '" + dpdClass.SelectedValue.ToString() + "'";
        }
        //WhereString = WhereString + BrokerWhere;
        //FIN WHERE Builder

        if (chkBilling.Items[0].Selected == true)//GROUP BY DATE
        {
            SelectString = SelectString + ",CONVERT(VARCHAR(11) ,T.Billingdate,101) as BillingDate";
            GroupByString = GroupByString + ",T.BillingDate";
            UnionString = UnionString + ",'** ALL **' AS BillingDate";
        }
        if (ckboxl.Items[0].Selected == true)
        {
            if (custunits != "'-'" && hfCust.Value.ToString() != "%")
            {
                SelectString = SelectString + ",'" + businessunit + "' AS Customer";
            }
            else
            {
                if (hfCust.Value.ToString() == "%")
                {
                    SelectString = SelectString + ",CASE WHEN T.Customer like 'ICS%' THEN 'ICS' WHEN (T.Customer like 'CC-%' OR T.Customer like 'CCP-%') THEN 'TALKTEL' ELSE T.Customer END as Customer";
                    GroupByString = GroupByString + ",CASE WHEN T.Customer like 'ICS%' THEN 'ICS' WHEN (T.Customer like 'CC-%' OR T.Customer like 'CCP-%') THEN 'TALKTEL' ELSE T.Customer END";
                }
                else
                {
                    SelectString = SelectString + ",T.Customer";
                    GroupByString = GroupByString + ",T.Customer";
                }
            }

            UnionString = UnionString + ",'****' AS Customer";
            //SelectString = SelectString + ",T.Customer";
            //GroupByString = GroupByString + ",T.Customer";
            //UnionString = UnionString + ",'****' AS Customer";
        }
        if (ckboxl.Items[1].Selected == true)
        {
            SelectString = SelectString + ",T.Vendor";
            GroupByString = GroupByString + ",T.Vendor";
            UnionString = UnionString + ",'****' AS Vendor";
        }
        if (ckboxl.Items[2].Selected == true)
        {
            SelectString = SelectString + ",CustBroker";
            GroupByString = GroupByString + ",B.CustBroker";
            UnionString = UnionString + ",'****' aS CustBroker";
            if (bCV == false) FromSBro = FromSBro + " LJM2CB";
        }
        if (ckboxl.Items[3].Selected == true)
        {
            SelectString = SelectString + ",VendorBroker";
            GroupByString = GroupByString + ",V.VendorBroker";
            UnionString = UnionString + ",'****' as VendorBroker";
            if (bVB == false) FromSBro = FromSBro + " LJM2VB";
        }
        if (ckboxl.Items[4].Selected == true)
        {
            SelectString = SelectString + ",T.Country";
            GroupByString = GroupByString + ",T.Country";
            UnionString = UnionString + ",'****' as Country";
        }
        if (ckboxl.Items[5].Selected == true)
        {
            SelectString = SelectString + ",T.Region";
            GroupByString = GroupByString + ",T.Region";
            UnionString = UnionString + ",'****' as Region";
        }
        if (ckboxl.Items[6].Selected == true)
        {
            SelectString = SelectString + ",T.RegionPrefix";
            GroupByString = GroupByString + ",T.RegionPrefix";
            UnionString = UnionString + ",'****' as RegionPrefix";
        }
        if (ckboxl.Items[7].Selected == true)
        {
            SelectString = SelectString + ",T.CustPrefix";
            GroupByString = GroupByString + ",T.CustPrefix";
            UnionString = UnionString + ",'****'";
        }
        if (ckboxl.Items[8].Selected == true)
        {
            SelectString = SelectString + ",T.VendorPrefix";
            GroupByString = GroupByString + ",T.VendorPrefix";
            UnionString = UnionString + ",'****' as VendorPrefix";
        }
        int icount = 0;
        if (dpdType.SelectedValue.ToString() != "** NONE **")//TYPE
        {
            SelectString = SelectString + ",R.Type";
            GroupByString = GroupByString + ",R.Type";
            UnionString = UnionString + ",'****' as [Type]";
            if (FromString.Contains("LJM2R") == false)
                FromString = FromString + " LJM2R";
            icount = icount + 1;
        }


        //CLASS
        if (dpdClass.SelectedValue.ToString() != "** NONE **")
        {
            SelectString = SelectString + ",R.Class";
            GroupByString = GroupByString + ",R.Class";
            UnionString = UnionString + ",'****' as Class";
            if (icount > 0)
            {
            }
            else
            {
                if (FromString.Contains("LJM2R") == false)
                    FromString = FromString + " LJM2R";
            }                        
        }

                
        //


        if (ckboxl.Items[9].Selected == true)
        {
            SelectString = SelectString + ",R.Type";
            GroupByString = GroupByString + ",R.Type";
            UnionString = UnionString + ",'****' as [Type]";
            if(FromString.Contains("LJM2R") == false)
                FromString = FromString + " LJM2R";
            

        }
        if (ckboxl.Items[10].Selected == true)
        {
            SelectString = SelectString + ",R.LMC";
            GroupByString = GroupByString + ",R.LMC";
            UnionString = UnionString + ",'****' as LMC";

            if (ckboxl.Items[9].Selected == false)
            {
                if (FromString.Contains("LJM2R") == false)
                    FromString = FromString + " LJM2R";
            }
        }
        if (ckboxl.Items[11].Selected == true)
        {
            SelectString = SelectString + ",T.Cost";
            GroupByString = GroupByString + ",T.Cost";
            UnionString = UnionString + ",NULL as Cost";
        }
        if (ckboxl.Items[12].Selected == true)
        {
            SelectString = SelectString + ",T.Price";
            GroupByString = GroupByString + ",T.Price";
            UnionString = UnionString + ",NULL AS Price";
        }


        //Arriba con group
        //Sin group abajo
        
        if (ckboxl.Items[14].Selected == true)
        {
            sSUM = sSUM + ",ROUND(SUM(T.CustMinutes),4) as CMinutes";
        }
        if (ckboxl.Items[15].Selected == true)
        {
            sSUM = sSUM + ",ROUND(SUM(T.VendorMinutes),4) as VMinutes";
        }
        if (ckboxl.Items[16].Selected == true)
        {
            sSUM = sSUM + ",ROUND(SUM(T.TotalCost),4) as TotalCost";
        }
        if (ckboxl.Items[17].Selected == true)
        {
            sSUM = sSUM + ",ROUND(SUM(T.TotalPrice),4) as TotalSales";
        }
        if (ckboxl.Items[18].Selected == true)
        {
            sSUM = sSUM + ",ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit";
        }

        if (ckboxl.Items[22].Selected == true)
        {
            sSUM = sSUM + ",SUM(T.tCalls) as Attempts";
        }
        if (ckboxl.Items[13].Selected == true)
        {
            sSUM = sSUM + ",SUM(T.BillableCalls) as AnsweredCalls";
        }

        if (ckboxl.Items[23].Selected == true)
        {
            sSUM = sSUM + ",SUM(T.RA) as [Rejected Calls]";
        }
 
        if (ckboxl.Items[19].Selected == true)
        {
            sSUM = sSUM + ",mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR";
        }
        if (ckboxl.Items[21].Selected == true)
        {

            sSUM = sSUM + ",mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR";
        }
        if (ckboxl.Items[20].Selected == true)
        {
            sSUM = sSUM + ",mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD";
        }

        

        sS = "";
        sOrder = "";
        if (SelectString.Length > 3)
        {
            sS = SelectString.Substring(1);
            sOrder = " ORDER BY " + sS.Replace("CONVERT(VARCHAR(11) ,T.Billingdate,101) as", " ");

            if (custunits != "'-'" && hfCust.Value.ToString() != "%")
            {
                sOrder = sOrder.Replace("'" + businessunit + "' AS Customer,", " ");
                sOrder = sOrder.Replace(",'" + businessunit + "' AS Customer", " ");
            }
            if (hfCust.Value.ToString() == "%")
            {
                sOrder = sOrder.Replace("CASE WHEN T.Customer like 'ICS%' THEN 'ICS' WHEN (T.Customer like 'CC-%' OR T.Customer like 'CCP-%') THEN 'TALKTEL' ELSE T.Customer END as Customer", " Customer");
                sOrder = sOrder.Replace(",CASE WHEN T.Customer like 'ICS%' THEN 'ICS' WHEN (T.Customer like 'CC-%' OR T.Customer like 'CCP-%') THEN 'TALKTEL' ELSE T.Customer END as Customer", "Customer");
            }

            //sS = SelectString.Substring(1);
            //sOrder = " ORDER BY " + sS.Replace("CONVERT(VARCHAR(11) ,T.Billingdate,101) as", " ");
        }
        else
        {
            if (sSUM.Length > 3)
            {
                sSUM = sSUM.Substring(1);
            }
        }

        //string sHaving = " HAVING SUM(CustMinutes) > 0 ";
        string sHaving = " ";
        sG = string.Empty;

        sF = FromString + FromSBro;
        sW = WhereString;
        if (GroupByString.Length > 3)
        {
            sG = " GROUP BY " + GroupByString.Substring(1);
        }
        sSt = "";
        if (UnionString.Length > 1)
        {
            sSt = UnionString.Substring(1);
        }

        sW = sW.Replace("|", "/");
        sF = sF.Replace("FM2TL", "FROM MECCA2..trafficlog T");
        sF = sF.Replace("LJM2CB", "LEFT JOIN MECCA2..CBroker B ON T.Customer = B.Customer");
        sF = sF.Replace("LJM2VB", "LEFT JOIN MECCA2..VBroker V ON T.Vendor = V.Vendor");
        sF = sF.Replace("LJM2R", "LEFT JOIN MECCA2.dbo.Regions R ON T.RegionPrefix = R.Code");

        QQ = "sS=" + sS + "&sF=" + sF + "&sW=" + sW + "&sG=" + sG + "&sSt=" + sSt + "&sSUM=" + sSUM;

        string Query = " SELECT " + sS + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT " + sSt + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + sHaving + sOrder;
        //Query = Query.Replace("SELECT ,", "SELECT ");
        Session["SalesLogS"] = Query;

        string dfrom = from;
        string dto = to;

        dfrom = from.Replace("|", "/");
        dto = to.Replace("|", "/");

        Session["dfrom"] = dfrom.ToString();
        Session["dto"] = dto.ToString();

        //Response.Cookies["MSLH"].Value = Query;  
        if ((sS.Length + sSUM.Length) > 6)
        {
            Response.Redirect("~/MSLH.aspx", false);
        }

    }
    protected void rdbtnlType_SelectedIndexChanged(object sender, EventArgs e)
    {
    
     //***

    }
    protected void MECCA_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("http://mecca3.computer-tel.com/MECCA/");
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect("MasterSalesLogH.aspx");
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    protected void ddlCustBroker_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCustBroker.SelectedItem.ToString() == "**ALL**") hfCustBroker.Value = "%";
        else hfCustBroker.Value = ddlCustBroker.SelectedItem.ToString();

    }
    protected void ddlVendorBroker_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlVendorBroker.SelectedItem.ToString() == "**ALL**") hfVendorBroker.Value = "%";
        else hfVendorBroker.Value = ddlVendorBroker.SelectedItem.ToString();

    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }

    protected string getCustomer(string customer)
    {
        string cust = "";
        String connectionString = ConfigurationManager.ConnectionStrings["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter;
        SqlCommand sQuery = new SqlCommand();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@businessunit", SqlDbType.VarChar);
        param[0].Value = customer;
        sQuery.Parameters.AddRange(param);

        DataSet ResultSet = new DataSet();

        sQuery.CommandType = System.Data.CommandType.StoredProcedure;
        sQuery.CommandText = "sp_getbusinessuc";
        sQuery.Connection = DBConnection;
        dbAdapter = new SqlDataAdapter(sQuery);

        dbAdapter.Fill(ResultSet);

        int index = 0;



        if (ResultSet.Tables.Count > 0)
        {
            while (index < ResultSet.Tables[0].Rows.Count)
            {
                cust += "'" + ResultSet.Tables[0].Rows[index][0].ToString() + "',";
                index++;
            }

            cust = cust.Substring(0, cust.Length - 1);
        }

        return cust;
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }


}
