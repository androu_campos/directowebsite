using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;


public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Redirect("http://drpmecca.directo.com/MeccaDirecto");
        try
        {

            //There are unrated minutes?
            MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter adpUN = new MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter();
            MyDataSetTableAdapters.unratedMinutes_DataTable tblUN = adpUN.GetData();

            if (tblUN.Rows.Count > 0)
            {
                HyperLink1.Enabled = true;
                HyperLink1.Text = "There are unrated minutes in MECCA!";
                HyperLink1.ForeColor = System.Drawing.Color.Red;
                HyperLink1.NavigateUrl = "~/unrated.aspx";
            }
            else
            {
                HyperLink1.Visible = false;
                HyperLink1.Text = "All minutes are rated in MECCA!";
            }

            if (Session["Rol"].ToString() == "Country Managers" && Session["DirectoUser"].ToString() != "vicente" && Session["DirectoUser"].ToString() != "mmurray" && Session["DirectoUser"].ToString() != "mmurray" && Session["DirectoUser"].ToString() != "vicente")
            {
                Response.Redirect("CtryHome.aspx", false);
            }

            //img1.Attributes.Add("onmouseover", "showImage('" + img1.ClientID + "','1');");
            //img1.Attributes.Add("onmouseout", "showImage('" + img1.ClientID + "','2');");
            img2.Attributes.Add("onmouseover", "showImage('" + img2.ClientID + "','3');");
            img2.Attributes.Add("onmouseout", "showImage('" + img2.ClientID + "','4');");
            ImageButton1.Attributes.Add("onmouseover", "showImage('" + ImageButton1.ClientID + "','5');");
            ImageButton1.Attributes.Add("onmouseout", "showImage('" + ImageButton1.ClientID + "','6');");
            ImageButton2.Attributes.Add("onmouseover", "showImage('" + ImageButton2.ClientID + "','7');");
            ImageButton2.Attributes.Add("onmouseout", "showImage('" + ImageButton2.ClientID + "','8');");
            ImageButton3.Attributes.Add("onmouseover", "showImage('" + ImageButton3.ClientID + "','9');");
            ImageButton3.Attributes.Add("onmouseout", "showImage('" + ImageButton3.ClientID + "','10');");


            MECCA2TableAdapters.Special_ConsiderationsAdp adpSc = new MECCA2TableAdapters.Special_ConsiderationsAdp();
            MECCA2.Special_ConsiderationsDataTable tblSc = adpSc.GetData();
            StringBuilder comments = new StringBuilder();
           
                //Label12.Text = tblSc.Rows[1][2].ToString();
                //Label13.Text = tblSc.Rows[1][3].ToString();
                //Label14.Text = tblSc.Rows[1][4].ToString();                           
                //Label8.Text = tblSc.Rows[0][2].ToString();
                //Label9.Text = tblSc.Rows[0][3].ToString();
                //Label10.Text = tblSc.Rows[0][4].ToString();
                
           
           
            MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adp = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
            MyDataSetTableAdapters.Billing_StatusDataTable tbl = adp.GetData();

            SummaryTableAdapters.QrysAdp adpL = new SummaryTableAdapters.QrysAdp();
            int lastId = Convert.ToInt32( adpL.GetLastIdqry());
            int lastId1=0, lastId2=0;
            /**///TODAY
            SummaryTableAdapters.SummaryAdp adpD = new SummaryTableAdapters.SummaryAdp();
            Summary.SummaryDataTable tblD = adpD.GetDataByDate(Convert.ToDateTime( DateTime.Now.ToString() ));//Today
            if (tblD.Rows.Count > 0)
            {
                lastId1 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            else
            {
                tblD = adpD.GetDataByDate(Convert.ToDateTime( DateTime.Now.AddDays(-1).ToShortDateString() ));//Today
                lastId1 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            /**/

            /**///7 DAY BEFORE
            
            tblD = adpD.GetDataByDate(DateTime.Now.AddDays(-7));//Today
            if (tblD.Rows.Count > 0)
            {
                lastId2 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            else
            {
                tblD = adpD.GetDataByDate( Convert.ToDateTime( DateTime.Now.AddDays(-8).ToShortDateString() ));//Today
                lastId2 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            /**/
//            lastId2 = 885;

            if (tbl.Rows.Count > 0)//Billing running
            {
                
                lblBillingRunning.Visible = true;
                lblBillingRunning.Text = "Billing Running !!!";
                lblBillingRunning.ForeColor = System.Drawing.Color.Red;

                HyperLink1.Visible = false;
                HyperLink2.Visible = false;

                string yesterday = System.DateTime.Now.AddDays(-1).ToShortDateString();
                string ayesterday = System.DateTime.Now.AddDays(-2).ToShortDateString();
                //  Get Dates of Yesterday//Today

                //SummaryTableAdapters.SummaryAdp adpS = new SummaryTableAdapters.SummaryAdp();
                SummaryTableAdapters.Summary_Adp adpS = new SummaryTableAdapters.Summary_Adp();
                Summary.Summary_DataTable tblS = adpS.GetDataById(lastId1);
                
                //Summary.SummaryDataTable tblS = adpS.GetDataById(lastId1);

                string CMinutes = tblS.Rows[0]["CMinutes"].ToString();
                
                int isFloatCminutes = Convert.ToInt32(CMinutes.IndexOf("."));
                
                if (isFloatCminutes < 0)
                {
                    lblMinutesY.Text = Util.getNumberFormat(CMinutes, 0);
                }
                else
                {
                    CMinutes = Util.getNumberFormat(CMinutes, 1);
                    int index = CMinutes.IndexOf(".");

                    CMinutes = CMinutes.Substring(0, index);
                    
                    lblMinutesY.Text = CMinutes;
                    lblMinutesY.Text = Util.getNumberFormat(CMinutes, 1);
                }


                string Attempts = tblS.Rows[0]["Attempts"].ToString();
                lblAttemptsY.Text = Util.getNumberFormat(Attempts, 0);
                
                string Calls =  tblS.Rows[0]["Calls"].ToString();
                lblCallsY.Text = Util.getNumberFormat(Calls,0);

                string Profit = tblS.Rows[0]["Profit"].ToString();
                lblProfitY.Text = Util.getNumberFormat(Profit,2);

                lblTotSalesY.Text = Util.getNumberFormat(tblS.Rows[0]["TotalSales"].ToString(), 2);
                 


                lblASRY.Text = tblS.Rows[0]["ASR"].ToString() + ".00%";
                lblABRY.Text = tblS.Rows[0]["ABR"].ToString() + ".00%";
                lblACDY.Text = tblS.Rows[0]["CACD"].ToString();


                float minutesY = (float)Convert.ToSingle(tblS.Rows[0]["CMinutes"].ToString());
                int callsY = Convert.ToInt32(tblS.Rows[0]["Calls"].ToString());
                float profitY = (float)Convert.ToSingle(tblS.Rows[0]["Profit"].ToString());
                int asrY = Convert.ToInt32(tblS.Rows[0]["ASR"].ToString());
                int abrY = Convert.ToInt32(tblS.Rows[0]["ABR"].ToString());
                float acdY = (float)Convert.ToSingle(tblS.Rows[0]["CACD"].ToString());
                float totSalesY = (float)Convert.ToSingle(tblS.Rows[0]["TotalSales"].ToString());
                int attemptsY = Convert.ToInt32(tblS.Rows[0]["Attempts"].ToString());

                //Get Dates of PRE-Yesterday
//*               Summary.SummaryDataTable tblA = adpS.GetDataById(lastId2);
                Summary.Summary_DataTable tblA = adpS.GetDataById(lastId2);

                float minutesA = (float)Convert.ToSingle(tblA.Rows[0]["CMinutes"].ToString());
                int callsA = Convert.ToInt32(tblA.Rows[0]["Calls"].ToString());
                float profitA = (float)Convert.ToSingle(tblA.Rows[0]["Profit"].ToString());
                int asrA = Convert.ToInt32(tblA.Rows[0]["ASR"].ToString());
                int abrA = Convert.ToInt32(tblA.Rows[0]["ABR"].ToString());
                float acdA = (float)Convert.ToSingle(tblA.Rows[0]["CACD"].ToString());
                float totSalesA = (float)Convert.ToSingle(tblA.Rows[0]["TotalSales"].ToString());
                int attemptsA = Convert.ToInt32(tblA.Rows[0]["Attempts"].ToString());
                //column change
                //
                string MinutesC = Convert.ToString(minutesY - minutesA).Replace("-", "");
                int isfloat = Convert.ToInt32(MinutesC.IndexOf("."));
                if (isfloat < 0)
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 0);
                }
                else
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 1);
                }

                lblMinutesC.Text = lblMinutesC.Text.Replace("-", "");
                lblCallsC.Text = Util.getNumberFormat(Convert.ToString((callsA - callsY)).Replace("-", ""), 0);
                lblAttemptsC.Text = Util.getNumberFormat(Convert.ToString((attemptsA - attemptsY)).Replace("-", ""), 0);
                lblProfitC.Text = Util.getNumberFormat(Convert.ToString((profitA - profitY)).Replace("-", ""), 2);
                lblASRC.Text = Convert.ToString((asrA - asrY)).Replace("-", "") + ".00%";
                lblABRC.Text = Convert.ToString((abrA - abrY)).Replace("-", "") + ".00%";
                lblACDC.Text = lblACDC.Text = Convert.ToString((acdA - acdY)).Replace("-", "");

                string sales = Convert.ToString(totSalesY - totSalesA).Replace("-", "");
                isfloat = Convert.ToInt32(sales.IndexOf("."));
                if (isfloat < 0)
                {
                    lblTotSalesC.Text = Util.getNumberFormat(Convert.ToString((totSalesA - totSalesY)).Replace("-", ""), 0);
                }
                else
                {
                    lblTotSalesC.Text = Util.getNumberFormat(Convert.ToString((totSalesA - totSalesY)).Replace("-", ""), 2);
                }


                // end column change

                if (minutesY < minutesA)                //MINUTOS
                {
                    Image4.ImageUrl = "./Images/roja.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image4.ImageUrl = "./Images/verde.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Green;
                }

                if (callsY < callsA)                     //CALLS
                {
                    Image5.ImageUrl = "./Images/roja.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Red;
                }

                else
                {
                    Image5.ImageUrl = "./Images/verde.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Green;
                }
                if (profitY < profitA)                      //PROFIT
                {
                    Image6.ImageUrl = "./Images/roja.gif";
                    lblProfitC.ForeColor = System.Drawing.Color.Red;

                }
                else
                {
                    Image6.ImageUrl = "./Images/verde.gif";
                    lblProfitC.ForeColor = System.Drawing.Color.Green;

                }
                if (asrY < asrA)                            //ASR
                {
                    Image7.ImageUrl = "./Images/roja.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image7.ImageUrl = "./Images/verde.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Green;
                }

                if (acdY < acdA)                           //ACD
                {
                    Image8.ImageUrl = "./Images/roja.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image8.ImageUrl = "./Images/verde.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Green;
                }

                if (totSalesY < totSalesA)                 //TOTAL SALES
                {
                    Image13.ImageUrl = "./Images/roja.gif";
                    lblTotSalesC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image13.ImageUrl = "./Images/verde.gif";
                    lblTotSalesC.ForeColor = System.Drawing.Color.Green;
                }

                if (attemptsY < attemptsA)                     //Attempts
                {
                    Image10.ImageUrl = "./Images/roja.gif";
                    lblAttemptsC.ForeColor = System.Drawing.Color.Red;
                }

                else
                {
                    Image10.ImageUrl = "./Images/verde.gif";
                    lblAttemptsC.ForeColor = System.Drawing.Color.Green;
                }

                if (abrY < abrA)                     //ABR
                {
                    Image15.ImageUrl = "./Images/roja.gif";
                    lblABRC.ForeColor = System.Drawing.Color.Red;
                }

                else
                {
                    Image15.ImageUrl = "./Images/verde.gif";
                    lblABRC.ForeColor = System.Drawing.Color.Green;
                }

                lblCusN.Visible = true;
                lblVenN.Visible = true;
                lblCouN.Visible = true;

                //**//
                if (Session["Rol"].ToString() == "NOC" || Session["Rol"].ToString() == "NOC National")
                {

                    lblHeaderProfit1.Visible = false;
                    lblHeaderProfit2.Visible = false;
                    lblHeaderProfit3.Visible = false;


                    TableItemStyle estilo = new TableItemStyle();
                    estilo.CssClass = "white";

                    gdvCountry.Columns[2].Visible = false;
                    lblHeaderProfit1.Text = "Minutes";
                    lblHeaderProfit1.Visible = true;
                    Label7.Visible = false;

                    gdVendors.Columns[2].Visible = false;
                    lblHeaderProfit2.Text = "Minutes";
                    lblHeaderProfit2.Visible = true;
                    Label2.Visible = false;

                    gdvCustomers.Columns[2].Visible = false;
                    lblHeaderProfit3.Text = "Minutes";
                    lblHeaderProfit3.Visible = true;

                    Label4.Visible = false;
                    lblProfitY.Visible = false;
                    lblProfitC.Visible = false;
                    Image6.Visible = false;
                    Label5.Visible = false;

                    Label11.Visible = false;
                    lblTotSalesY.Visible = false;
                    lblTotSalesC.Visible = false;
                    Image13.Visible = false;
                }

            }//FIN DEL BILLING RUNNING
            else
            {
                lblBillingRunning.Visible = true;
                lblBillingRunning.Text = "Billing NOT Running";
                lblBillingRunning.ForeColor = System.Drawing.Color.White;

                if (Session["DirectoUser"].ToString() == "hvalle" || Session["DirectoUser"].ToString() == "jai")
                {
                    Label16.Visible = false;
                    imgCtryMan5.Visible = true;
                    pnlCtryMan.Visible = true;    /****************/

                    //REPORT CTRY MANAGERS
                    RunCtryCmmsn("sp_countryCommissionsH");

                }
                else
                {
                    Label16.Visible = false;
                    imgCtryMan5.Visible = false;
                    pnlCtryMan.Visible = false;
                }

                /*                                         MECCA  SUMMARY                                       */
                lblCusN.Visible = false;
                lblVenN.Visible = false;
                lblCouN.Visible = false;
                string yesterday = System.DateTime.Now.AddDays(-1).ToShortDateString();
                string ayesterday = System.DateTime.Now.AddDays(-2).ToShortDateString();                               
                //  Get Dates of Yesterday
                SummaryTableAdapters.Summary_Adp adpS = new SummaryTableAdapters.Summary_Adp();
                Summary.Summary_DataTable tblS = adpS.GetDataById(lastId1);

                string CMinutes = tblS.Rows[0]["CMinutes"].ToString();
                int isFloatCminutes = Convert.ToInt32(CMinutes.IndexOf("."));
                if (isFloatCminutes < 0)
                {
                    lblMinutesY.Text = Util.getNumberFormat(CMinutes, 0);
                }
                else
                {   
                    CMinutes = Util.getNumberFormat(CMinutes, 1);
                    int index = CMinutes.IndexOf(".");

                    CMinutes = CMinutes.Substring(0, index);
                    lblMinutesY.Text = CMinutes;
                    //lblMinutesY.Text = Util.getNumberFormat(CMinutes, 1);
                }
                
                string Calls = tblS.Rows[0]["Calls"].ToString();
                string Attempts = tblS.Rows[0]["Attempts"].ToString();
                lblCallsY.Text = Util.getNumberFormat(Calls, 0);
                lblAttemptsY.Text = Util.getNumberFormat(Attempts, 0);
                string Profit = tblS.Rows[0]["Profit"].ToString();
                lblProfitY.Text = Util.getNumberFormat(Profit, 2);
                lblASRY.Text = tblS.Rows[0]["ASR"].ToString() + ".00%";
                lblABRY.Text = tblS.Rows[0]["ABR"].ToString() + ".00%";
                lblACDY.Text = tblS.Rows[0]["CACD"].ToString();

                lblTotSalesY.Text = Util.getNumberFormat(tblS.Rows[0]["TotalSales"].ToString(), 2);
                

                float minutesY = (float)Convert.ToSingle(tblS.Rows[0]["CMinutes"].ToString());
                int callsY = Convert.ToInt32(tblS.Rows[0]["Calls"].ToString());
                int attemptsY = Convert.ToInt32(tblS.Rows[0]["Attempts"].ToString());
                float profitY = (float)Convert.ToSingle(tblS.Rows[0]["Profit"].ToString());
                int asrY = Convert.ToInt32(tblS.Rows[0]["ASR"].ToString());
                int abrY = Convert.ToInt32(tblS.Rows[0]["ABR"].ToString());
                float acdY = (float)Convert.ToSingle(tblS.Rows[0]["CACD"].ToString());
                float totSalesY = (float)Convert.ToSingle(tblS.Rows[0]["TotalSales"].ToString());
                //Get Dates of PRE-Yesterday


//                lastId2 = 885;
                
                //***                Summary.SummaryDataTable tblA = adpS.GetDataById(lastId2);
                Summary.Summary_DataTable tblA = adpS.GetDataById(lastId2);

                float minutesA = (float)Convert.ToSingle(tblA.Rows[0]["CMinutes"].ToString());
                int callsA = Convert.ToInt32(tblA.Rows[0]["Calls"].ToString());
                int attemptsA = Convert.ToInt32(tblA.Rows[0]["Attempts"].ToString());
                float profitA = (float)Convert.ToSingle(tblA.Rows[0]["Profit"].ToString());
                int asrA = Convert.ToInt32(tblA.Rows[0]["ASR"].ToString());
                int abrA = Convert.ToInt32(tblA.Rows[0]["ABR"].ToString());
                float acdA = (float)Convert.ToSingle(tblA.Rows[0]["CACD"].ToString());
                float totSalesA = (float)Convert.ToSingle(tblA.Rows[0]["TotalSales"].ToString());
                //column change
                string MinutesC = Convert.ToString(minutesY - minutesA).Replace("-", "");
                int isfloat = Convert.ToInt32(MinutesC.IndexOf("."));
                if (isfloat < 0)
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 0);
                }
                else
                {
                    //MinutesC = Util.getNumberFormat(MinutesC, 0);
                    //MinutesC = MinutesC.Substring(0, MinutesC.Length - 3);
                    //lblMinutesC.Text = CMinutes;
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 1);
                }

                lblMinutesC.Text = lblMinutesC.Text.Replace("-", "");
                lblCallsC.Text = Util.getNumberFormat(Convert.ToString((callsA - callsY)).Replace("-", ""),0);
                lblAttemptsC.Text = Util.getNumberFormat(Convert.ToString((attemptsA - attemptsY)).Replace("-", ""), 0);
                lblProfitC.Text = Util.getNumberFormat(Convert.ToString((profitA - profitY)).Replace("-", ""),2);
                lblASRC.Text = Convert.ToString((asrA - asrY)).Replace("-", "") + ".00%";
                lblABRC.Text = Convert.ToString((abrA - abrY)).Replace("-", "") + ".00%";
                lblACDC.Text = lblACDC.Text = Convert.ToString((acdA - acdY)).Replace("-", "");

                string sales = Convert.ToString(totSalesY - totSalesA).Replace("-", "");
                isfloat = Convert.ToInt32(sales.IndexOf("."));
                if (isfloat < 0)
                {
                    lblTotSalesC.Text = Util.getNumberFormat(Convert.ToString((totSalesA - totSalesY)).Replace("-", ""), 0);
                }
                else
                {
                    lblTotSalesC.Text = Util.getNumberFormat(Convert.ToString((totSalesA - totSalesY)).Replace("-", ""), 2);
                }
                // end column change

                if (minutesY < minutesA)                //MINUTOS
                {
                    Image4.ImageUrl = "./Images/roja.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image4.ImageUrl = "./Images/verde.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Green;
                }

                if (callsY < callsA)                     //LLAMADAS
                {
                    Image5.ImageUrl = "./Images/roja.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image5.ImageUrl = "./Images/verde.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Green;
                }
                if (profitY < profitA)                      //PROFIT
                {
                    Image6.ImageUrl = "./Images/roja.gif";
                    lblProfitC.ForeColor = System.Drawing.Color.Red;

                }
                else
                {
                    Image6.ImageUrl = "./Images/verde.gif";
                    lblProfitC.ForeColor = System.Drawing.Color.Green;

                }
                if (asrY < asrA)                            //ASR
                {
                    Image7.ImageUrl = "./Images/roja.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image7.ImageUrl = "./Images/verde.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Green;
                }

                if (acdY < acdA)                           //ACD
                {
                    Image8.ImageUrl = "./Images/roja.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image8.ImageUrl = "./Images/verde.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Green;
                }

                if (attemptsY < attemptsA)                     //Attempts
                {
                    Image10.ImageUrl = "./Images/roja.gif";
                    lblAttemptsC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image10.ImageUrl = "./Images/verde.gif";
                    lblAttemptsC.ForeColor = System.Drawing.Color.Green;
                }

                if (totSalesY < totSalesA)                 //TOTAL SALES
                {
                    Image13.ImageUrl = "./Images/roja.gif";
                    lblTotSalesC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image13.ImageUrl = "./Images/verde.gif";
                    lblTotSalesC.ForeColor = System.Drawing.Color.Green;
                }

                if (abrY < abrA)                     //ABR
                {
                    Image15.ImageUrl = "./Images/roja.gif";
                    lblABRC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image15.ImageUrl = "./Images/verde.gif";
                    lblABRC.ForeColor = System.Drawing.Color.Green;
                }

                //lblBillingRunning.Visible = true;
                //lblBillingRunning.Text = "Billing not Runing";


                MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter adpU2 = new MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter();
                MyDataSetTableAdapters.UnknownIDs2DataTable tblU2 = adpU2.GetData();

                if (tblU2.Rows.Count > 0)
                {
                    HyperLink2.Text = "There are unmatched IDs in MECCA!";
                    HyperLink2.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    HyperLink2.Text = "All IDs are matched";
                    HyperLink2.Visible = false;
                }


                
                string yest = DateTime.Today.AddDays(-1).ToShortDateString();
                MyDataSetTableAdaptersTableAdapters.trafficTopAdp adpT = new MyDataSetTableAdaptersTableAdapters.trafficTopAdp();
                gdvCustomers.DataSource = adpT.GetData();                
                gdvCustomers.DataBind();

                MyDataSetTableAdaptersTableAdapters.trafficVtopAdp adpV = new MyDataSetTableAdaptersTableAdapters.trafficVtopAdp();
                gdVendors.DataSource = adpV.GetData();
                gdVendors.DataBind();

                //MyDataSetTableAdaptersTableAdapters.trafficTopCAdp adpC = new MyDataSetTableAdaptersTableAdapters.trafficTopCAdp();
                

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM MECCA2.DBO.TOP15COUNTRY";
                cmd.CommandType = CommandType.Text;
                DataSet ds = RunQuery(cmd);
                gdvCountry.DataSource = ds;
                gdvCountry.DataBind();                

                MyDataSetTableAdaptersTableAdapters.UnknownIDs1TableAdapter adpUn = new MyDataSetTableAdaptersTableAdapters.UnknownIDs1TableAdapter();
                MyDataSetTableAdapters.UnknownIDs1DataTable tblUn = adpUn.GetData();

            }


            if ((Session["Rol"].ToString() == "NOC" || Session["Rol"].ToString() == "NOC National") && Session["Rol"].ToString() != "Comercial" && Session["Rol"].ToString() != string.Empty)
            {

                lblHeaderProfit1.Visible = false;
                lblHeaderProfit2.Visible = false;
                lblHeaderProfit3.Visible = false;


                TableItemStyle estilo = new TableItemStyle();
                estilo.CssClass = "white";

                gdvCountry.Columns[2].Visible = false;
                lblHeaderProfit1.Text = "Minutes";
                lblHeaderProfit1.Visible = true;
                Label7.Visible = false;

                gdVendors.Columns[2].Visible = false;
                lblHeaderProfit2.Text = "Minutes";
                lblHeaderProfit2.Visible = true;
                Label2.Visible = false;

                gdvCustomers.Columns[2].Visible = false;
                lblHeaderProfit3.Text = "Minutes";
                lblHeaderProfit3.Visible = true;

                Label4.Visible = false;


                lblProfitY.Visible = false;
                lblProfitC.Visible = false;
                Image6.Visible = false;
                Label5.Visible = false;

                Label11.Visible = false;
                lblTotSalesY.Visible = false;
                lblTotSalesC.Visible = false;
                Image13.Visible = false;

            }
            else
            {

            }
            //BOARDS ROUTING AND DEPLOYMENT
            //R
            MECCA2TableAdapters.MonitoringAdp adpBoards = new MECCA2TableAdapters.MonitoringAdp();          
            
            gdvRouting.DataSource = adpBoards.GetTop5Routing();
            gdvRouting.DataBind();
            //D
            //gdvDeployment.DataSource = adpBoards.GetTop5Deployment();
            //gdvDeployment.DataBind();

            //
            if (Session["DirectoUser"].ToString() == "djassan" || Session["DirectoUser"].ToString() == "hvalle")
            {
                hypNational.Visible = true;
            }
            else
            {
                hypNational.Visible = false;
            }

    }
       catch(Exception ex)
        {
        string message = ex.Message.ToString();
        }

 }
    private void RunCtryCmmsn(string storeProcedure)
    {
        try
        {
            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = System.Data.CommandType.StoredProcedure;
            sqlSpQuery.CommandText = storeProcedure;
            sqlSpQuery.Parameters.Add("@fromP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();
            sqlSpQuery.Parameters.Add("@toP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();
            sqlSpQuery.CommandTimeout = 160;
            DataSet ResultSetC;
            //ResultSetC = Util.RunQuery(sqlSpQuery);
            ResultSetC = Util.RunQueryByStmnt("select isnull(minutes,0) Minutes,isnull(answeredcalls,0) AnsweredCalls,isnull(grossprofit,0) GrossProfit from WidgetHuguette where Id=1");
            DataTable tbl = (DataTable)ResultSetC.Tables[0].Copy();//yesterday

            for (int k = 1; k < ResultSetC.Tables.Count; k++)
            {
                tbl.LoadDataRow(ResultSetC.Tables[k].Rows[0].ItemArray, false);
            }
            
            SqlCommand sqlSpQuery2 = new SqlCommand();
            sqlSpQuery2.CommandType = System.Data.CommandType.StoredProcedure;
            sqlSpQuery2.CommandText = storeProcedure;
            sqlSpQuery2.Parameters.Add("@fromP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-7).ToShortDateString();
            sqlSpQuery2.Parameters.Add("@toP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-7).ToShortDateString();
            sqlSpQuery2.CommandTimeout = 160;
            DataSet ResultSet2;
            //ResultSet2 = RunQuery(sqlSpQuery2);
            ResultSet2 = Util.RunQueryByStmnt("select isnull(minutes,0) Minutes,isnull(answeredcalls,0) AnsweredCalls,isnull(grossprofit,0) GrossProfit from WidgetHuguette where Id=2");
            DataTable tbl2 = (DataTable)ResultSet2.Tables[0].Copy();//1 month ago

            for (int k = 1; k < ResultSetC.Tables.Count; k++)
            {
               tbl2.LoadDataRow(ResultSet2.Tables[k].Rows[0].ItemArray, false);
            }

            for (int z = 0; z < 8; z++)
            {
                
                decimal gP = Convert.ToDecimal( tbl.Rows[z]["GrossProfit"].ToString() );
                decimal gp2 = Convert.ToDecimal( tbl2.Rows[z]["GrossProfit"].ToString() );

                if (z == 0)//top Andre
                {
                    lblTCall1.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin1.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross1.Text = tbl.Rows[z]["GrossProfit"].ToString();
                    if (gP > gp2)
                    {
                        imgStatus1.ImageUrl = "./Images/verde.gif";
                    }
                    else
                    {
                        imgStatus1.ImageUrl = "./Images/roja.gif";
                    }
                }
                else if (z == 1)//Hamit
                {
                    lblTCall2.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin2.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross2.Text = tbl.Rows[z]["GrossProfit"].ToString();
                    if (gP > gp2)
                    {
                        imgStatus2.ImageUrl = "./Images/verde.gif";
                    }
                    else
                    {
                        imgStatus2.ImageUrl = "./Images/roja.gif";
                    }
                }
                else if (z == 2)//Iliana
                {
                    lblTCall3.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin3.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross3.Text = tbl.Rows[z]["GrossProfit"].ToString();
                    
                    if (gP > gp2)
                    {
                        imgStatus3.ImageUrl = "./Images/verde.gif";
                    }
                    else
                    {
                        imgStatus3.ImageUrl = "./Images/roja.gif";
                    }
                }
                else if (z == 3)//Jaime
                {
                    lblTCall4.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin4.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross4.Text = tbl.Rows[z]["GrossProfit"].ToString();

                    if (gP > gp2)
                    {
                        imgStatus4.ImageUrl = "./Images/verde.gif";
                    }
                    else
                    {
                        imgStatus4.ImageUrl = "./Images/roja.gif";
                    }
                }
                else if (z == 4)//Jake
                {
                    if (tbl.Rows[z]["AnsweredCalls"] == System.DBNull.Value)
                    {
                        lblTCall5.Text = "0";
                        lblTMin5.Text = "0";
                        lblTgross5.Text = "0";
                        imgStatus5.ImageUrl = "./Images/roja.gif";
                    }
                    else
                    {


                        lblTCall5.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                        lblTMin5.Text = tbl.Rows[z]["Minutes"].ToString();
                        lblTgross5.Text = tbl.Rows[z]["GrossProfit"].ToString();
                        if (gP > gp2)
                        {
                            imgStatus5.ImageUrl = "./Images/verde.gif";
                        }
                        else
                        {
                            imgStatus5.ImageUrl = "./Images/roja.gif";
                        }
                    }
                                        
                }
                else if (z == 5)//Martha
                {
                    lblTCall6.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin6.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross6.Text = tbl.Rows[z]["GrossProfit"].ToString();
                    
                    if (gP > gp2)
                    {
                        imgStatus6.ImageUrl = "./Images/verde.gif";
                    }
                    else
                    {
                        imgStatus6.ImageUrl = "./Images/roja.gif";
                    }
                }
                else if (z == 6)//Michelle
                {
                    lblTCall7.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin7.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross7.Text = tbl.Rows[z]["GrossProfit"].ToString();

                    if (gP > gp2)
                    {
                        imgStatus7.ImageUrl = "./Images/verde.gif";
                    }
                    else
                    {
                        imgStatus7.ImageUrl = "./Images/roja.gif";
                    }
                }
                else if (z == 7)//Roberto
                {
                    lblTCall8.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin8.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross8.Text = tbl.Rows[z]["GrossProfit"].ToString();

                    if (gP > gp2)
                    {
                        imgStatus8.ImageUrl = "./Images/verde.gif";
                    }
                    else
                    {
                        imgStatus8.ImageUrl = "./Images/roja.gif";
                    }
                }
            }

        }
        catch (Exception ex)
        {
            string errormessage = ex.Message.ToString();
        }
    }

    protected void buttoncchome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeCC.aspx");
    }

    protected void buttonaahome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeAA.aspx");
    }

    protected void buttonicshome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeR.aspx");
    }   
    
    protected void buttonsmshome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeSMS.aspx");
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            string messagerror = ex.Message.ToString();
        }

        return resultsDataSet;
    }
    }
    

