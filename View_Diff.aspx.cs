using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;


public partial class View_Diff : System.Web.UI.Page
{

    private static ArrayList theTables = new ArrayList(6);
    private static int ii = 0;
    private bool dat = false;
    private bool newComment = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Page.IsPostBack)
        //{
        //Response.Redirect("NotAvailable.aspx", false);



        showInfo(sender, e, null);






        //    ii = i;
        //    //VERIFICO SI YA HAY ALGUN COMENTARIO != null
        //    string qryC = "SELECT * from CDRDB..Diff_Comments where date_Diff = '" + txtDate.Text + " 00:00:00.000'";
        //    DataSet dtsComments = Util.RunQueryByStmntatCDRDB(qryC);

        //    if (dtsComments.Tables[0].Rows.Count > 0)
        //    {
        //        if (dtsComments.Tables[0].Rows[0][1].ToString() != string.Empty)
        //        {
        //            cmdAddComment.Visible = false;
        //            cmdEdit.Visible = true;
        //            string commy = dtsComments.Tables[0].Rows[0][1].ToString();
        //            commy = commy.Replace("<table width=\\\"400px\\\"><tr><td> ", "");
        //            commy = commy.Replace(" </td></tr><tr><td> ", "\r\n");
        //            commy = commy.Replace(" </td></tr></table>", "\r\n");

        //            txtComments.Text = commy;

        //            commy = "<table aling=\"left\" width=\"400px\"><tr><td>Comments: " + dtsComments.Tables[0].Rows[0][1].ToString();
        //            commy = commy.Replace("\r\n", " </td></tr><tr><td> ");
        //            lbl_Comments.Text = "" + commy + " </td></tr></table>";

                    

        //        }
        //        else
        //        {
        //            cmdAddComment.Visible = true;
        //            cmdEdit.Visible = false;
        //            lbl_Comments.Text = "There are no comments for " + txtDate.Text;
        //        }
        //        lblLink.Visible = true;
        //        string date = txtDate.Text + ".xls";
        //        date = date.Replace("/", "-");
        //        string strMachineName = Request.ServerVariables["SERVER_NAME"];
        //        lblLink.Text = "<A href=http://" + strMachineName + "/Diffs_M3_M4/" + "report_" + date + ">Download Report</a>";
        //    }
        //    else
        //    {
        //        cmdEdit.Visible = false;
        //        cmdAddComment.Visible = false;
        //        lblLink.Visible = false;
        //    }


        //    if (Session["DirectoUser"].ToString() == "elidio" || Session["DirectoUser"].ToString() == "jonatan" || Session["DirectoUser"].ToString() == "alfonso" || Session["DirectoUser"].ToString() == "Osolis" || Session["DirectoUser"].ToString() == "Fernando")
        //    {
        //        pnlComments.Visible = true;
        //    }
        //    else
        //    {
        //        pnlComments.Visible = false;
        //    }

        //}

    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {

        lblChanges.Visible = true;

        string year = "";
        string month = "";
        string day = "";
        string date = "";
        DateTime dt = new DateTime();
        dt = System.Convert.ToDateTime(txtDate.Text);
        year = dt.Year.ToString().Substring(3);
        month = dt.Month.ToString();
        day = dt.Day.ToString();

        if (month.Length == 1)
            month = "0" + month;

        if (day.Length == 1)
            day = "0" + day;

        date = year + "/" + month + "/" + day;

        showInfo(sender, e, date);
        //lblChanges.Text = idP.Value.ToString();
        //lblChanges.Text = txtDate.Text;


        //*************CARGA ID DE PROCESO PARA REPORTE DEL DIA SELECCIONADO****************************************
        //sql = "SELECT ID_Process FROM CDRDB..Diff_Summary_Log WHERE Proc_date = SUBSTRING(CONVERT(varchar(10),"+dt+",111),4,7)";
        //DataSet dts = (DataSet)Util.RunQueryByStmntatCDRDB(sql);



        //
        //string sql = "SELECT * FROM CDRDB..Diff_1 WHERE [Date]>'" + txtDate.Text + " 00:00:00.000' and [Date] < '" + txtDate.Text + " 23:59:59.000'";
        //DataSet dts1 = (DataSet)Util.RunQueryByStmntatCDRDB(sql);
        //if (dts1.Tables[0].Rows.Count > 0)
        //{
        //    gdv1.DataSource = dts1.Tables[0];
        //    gdv1.DataBind();
        //    theTables.Add(dts1.Tables[0]);
        //    i++;
        //}
        //else
        //{
        //    txtComments.Text = string.Empty;
        //    gdv1.DataBind();
        //}
        ////
        //sql = "SELECT * FROM CDRDB..Diff_2 WHERE [Date_C]>'" + txtDate.Text + " 00:00:00.000' AND [Date_C]<'" + txtDate.Text + " 23:59:59.000'";
        //DataSet dts2 = (DataSet)Util.RunQueryByStmntatCDRDB(sql);
        //if (dts2.Tables[0].Rows.Count > 0)
        //{
        //    gdv2.DataSource = dts2.Tables[0];
        //    gdv2.DataBind();
        //    theTables.Add(dts2.Tables[0]);
        //    i++;
        //}
        //else
        //{
        //    gdv2.DataBind();
        //}
        ////

        //DataSet dts3 = (DataSet)Util.RunQueryByStmntatCDRDB("SELECT * FROM CDRDB..Diff_3 WHERE [Date_C]>'" + txtDate.Text + " 00:00:00.000' AND [Date_C]<'" + txtDate.Text + " 23:59:59.000'");
        //if (dts3.Tables[0].Rows.Count > 0)
        //{
        //    gdv3.DataSource = dts3.Tables[0];
        //    gdv3.DataBind();
        //    theTables.Add(dts3.Tables[0]);
        //    i++;
        //    Label4.Text = "CALLS WITH DIFFERENCE > 1 MIN";
        //}
        //else
        //{
        //    gdv3.DataBind();
        //    Label4.Text = "CALLS WITH DIFFERENCE > 1 MIN .......nothing found!!";
        //}
        ////

        //DataSet dts4 = (DataSet)Util.RunQueryByStmntatCDRDB("SELECT * FROM CDRDB..Diff_4 WHERE [Date_C]>'" + txtDate.Text + " 00:00:00.000' AND [Date_C]<'" + txtDate.Text + " 23:59:59.000'");
        //if (dts4.Tables[0].Rows.Count > 0)
        //{
        //    gdv4.DataSource = dts4.Tables[0];
        //    gdv4.DataBind();
        //    theTables.Add(dts4.Tables[0]);
        //    i++;
        //}
        //else
        //{
        //    gdv4.DataBind();
        //}
        ////
        //sql = "SELECT * FROM CDRDB..cdrs_Diff_5 WHERE [stop]>'" + txtDate.Text + " 00:00:00.000' AND [stop]<'" + txtDate.Text + " 23:59:59.000'";
        //DataSet dts5 = (DataSet)Util.RunQueryByStmntatCDRDB(sql);
        //if (dts5.Tables[0].Rows.Count > 0)
        //{
        //    gdv5.DataSource = dts5.Tables[0];
        //    gdv5.DataBind();
        //    theTables.Add(dts5.Tables[0]);
        //    i++;
        //}
        //else
        //{
        //    gdv5.DataBind();
        //}
        ////
        //sql = "SELECT * FROM CDRDB..Diff_6 WHERE [Date_C]>'" + txtDate.Text + " 00:00:00.000' AND [Date_C]<'" + txtDate.Text + " 23:59:59.000'";
        //DataSet dts6 = (DataSet)Util.RunQueryByStmntatCDRDB(sql);
        //if (dts6.Tables[0].Rows.Count > 0)
        //{
        //    gdv6.DataSource = dts6.Tables[0];
        //    gdv6.DataBind();
        //    theTables.Add(dts6.Tables[0]);
        //    i++;
        //}
        //else
        //{
        //    gdv6.DataBind();
        //}
        //ii = i;
        ////VERIFICO SI YA HAY ALGUN COMENTARIO != null
        //string qryC = "SELECT * from CDRDB..Diff_Comments where date_Diff = '" + txtDate.Text + " 00:00:00.000'";
        //DataSet dtsComments = Util.RunQueryByStmntatCDRDB(qryC);

        //if (dtsComments.Tables[0].Rows.Count > 0)
        //{
        //    if (dtsComments.Tables[0].Rows[0][1].ToString() != string.Empty)
        //    {
        //        cmdAddComment.Visible = false;
        //        cmdEdit.Visible = true;
        //        txtComments.Text = dtsComments.Tables[0].Rows[0][1].ToString();

        //        string commy = "<table aling=\"left\" width=\"400px\"><tr><td>Comments: " + dtsComments.Tables[0].Rows[0][1].ToString();
        //        commy = commy.Replace("\r\n", " </td></tr><tr><td> ");
        //        lbl_Comments.Text = commy + " </td></tr></table>";                
                
        //    }
        //    else
        //    {
        //        cmdAddComment.Visible = true;
        //        cmdEdit.Visible = false;
        //        lbl_Comments.Text = "There are no comments for " + txtDate.Text;
        //        txtComments.Text = string.Empty;
        //    }
        //    lblLink.Visible = true;
        //    string date = txtDate.Text + ".xls";
        //    date = date.Replace("/", "-");
        //    string strMachineName = Request.ServerVariables["SERVER_NAME"];
        //    lblLink.Text = "<A href=http://" + strMachineName + "/Diffs_M3_M4/" + "report_" + date + ">Download Report</a>";

        //}
        //else
        //{
        //    cmdEdit.Visible = false;
        //    cmdAddComment.Visible = false;
        //    lblLink.Visible = false;
        //}

        //if (Session["DirectoUser"].ToString() == "elidio" || Session["DirectoUser"].ToString() == "jonatan" || Session["DirectoUser"].ToString() == "alfonso" || Session["DirectoUser"].ToString() == "Osolis" || Session["DirectoUser"].ToString() == "Fernando")
        //{
        //    pnlComments.Visible = true;
        //}
        //else
        //{
        //    pnlComments.Visible = false;
        //}


    }

    protected void showInfo(object sender, EventArgs e, string date)
    {

        ArrayList myList = new ArrayList(6);
        int i = 0;
        string idProcess = "";
        string sql;
        string displayComment = "";
        lblChanges.Text = "";
        lblChanges.Visible=true;

        date = txtDate.Text;

        //*************CARGA ID DE PROCESO PARA REPORTE DEL DIA SELECCIONADO****************************************

        if (!Page.IsPostBack)
        {
            sql = "SELECT ID_Process FROM CDRDB..Diff_Summary_Log WHERE Proc_date = SUBSTRING(CONVERT(varchar(10),dateadd(day,-1,getDate()),111),4,7)";
        }
        //if (date == "")
        //{
        //   sql = "SELECT ID_Process FROM CDRDB..Diff_Summary_Log WHERE Proc_date = SUBSTRING(CONVERT(varchar(10),dateadd(day,-1,getDate()),111),4,7)";
        //}
        else
        {
            date = txtDate.Text;


            if ( date == "")
            {
                sql = "SELECT ID_Process FROM CDRDB..Diff_Summary_Log WHERE Proc_date = SUBSTRING(CONVERT(varchar(10),dateadd(day,-1,getDate()),111),4,7)";
            }
            else{
                DateTime dt = System.DateTime.Parse(date);
                string year = dt.Year.ToString();
                year = year.Substring(3);
                string month=dt.Month.ToString();
                string day = dt.Day.ToString();

                if (month.Length == 1)
                    month = "0"+month;
                
                if (day.Length == 1)
                    day = "0"+day;


                string newDate = year + "/" + month + "/" + day;


                sql = "SELECT ID_Process FROM CDRDB..Diff_Summary_Log WHERE Proc_date = '" + newDate + "'";
            
            }
            


        }

        DataSet dts = (DataSet)Util.RunQueryByStmntatCDRDB(sql);

        if (dts.Tables[0].Rows.Count > 0)
        {
            idProcess=dts.Tables[0].Rows[0][0].ToString();
            idP.Value = idProcess;
            Label8.Visible = false;
            cmdReport.Enabled = true;
            sql = "SELECT Tag, Attemps, Completed_calls, Minutes FROM CDRDB..Diff_Summary WHERE Process_ID = " + idProcess;
            DataSet dts1 = (DataSet)Util.RunQueryByStmntatCDRDB(sql);


            if (dts1.Tables[0].Rows.Count > 0)
            {
                gdv1.DataSource = dts1.Tables[0];
                gdv1.DataBind();
                theTables.Add(dts1.Tables[0]);
                i++;
            }
            else
            {
                txtComments.Text = string.Empty;
                gdv1.DataBind();
            }
            

            //*************BUSCA COMENTARIO**************************************************************************
            sql = "SELECT Date_Insert, Username, Comment FROM CDRDB..Diff_Comments WHERE Process_ID = " + idProcess;
            DataSet dts6 = (DataSet)Util.RunQueryByStmntatCDRDB(sql);
            if (dts6.Tables[0].Rows.Count > 0)
            {
                lbl_Comments.Text = "Comments";
                for (int j = 0; j < dts6.Tables[0].Rows.Count; j++)
                {
                    displayComment = displayComment +
                                    "Date insert: " + dts6.Tables[0].Rows[j][0].ToString() + "\n" +
                                    "UserName: " + dts6.Tables[0].Rows[j][1].ToString() + "\n" +
                                    "Comment: " + dts6.Tables[0].Rows[j][2].ToString() + "\n" +
                                    "---------------------------------------\n\n";

                }

                TextBox1.Text = displayComment.ToString();
            }
            else
            {
                lbl_Comments.Text = "No Comments...";
                TextBox1.Text= "";
            }
        }
        else
        {
            lblChanges.Visible = true;
            lblChanges.Text = "No rows for date - "+date.ToString();          

        }
    }

    protected void cmdAddComment_Click(object sender, EventArgs e)
    {
        string sql = "";
        string comment = txtComments.Text;
        string user = Session["DirectoUser"].ToString();
        string idProcess = "";
        string insertdate = "";
        string showdate = "";

        idProcess = idP.Value.ToString();
        insertdate = DateTime.Now.ToString();

        sql = "SELECT Proc_date FROM CDRDB..Diff_Summary_Log WHERE ID_PROCESS = " + idProcess;
        DataSet dst = (DataSet)Util.RunQueryByStmntatCDRDB(sql);

        showdate=dst.Tables[0].Rows[0][0].ToString();


        sql = "INSERT INTO CDRDB..Diff_Comments VALUES ('" + idProcess + "','" + comment + "','" + user + "','" + insertdate + "')";
        Util.RunQryAtCDRDB_void(sql);

        txtComments.Text = "";
        lblChanges.Text = "Comment added ";
        showInfo(sender, e, showdate);
        
    }

    protected void gdv1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdv1.PageIndex = e.NewPageIndex;
        gdv1.DataSource = theTables[0];
        gdv1.DataBind();
    }

    protected void cmdExport_Click(object sender, EventArgs e)        
    {

        int maxProcess = 0;
        string fileName = "";


        string sql = "select max(ID_Process)from CDRDB..Diff_Summary_Log";
        DataSet dts = (DataSet)Util.RunQueryByStmntatCDRDB(sql);
        maxProcess = (int)dts.Tables[0].Rows[0][0];
        dts.Dispose();



        if (maxProcess == int.Parse(idP.Value))
        {
            Response.Redirect("http://mecca.directo.com/Diffs_M3_M4/excelFile.zip");
        }
        else
        {
            sql = "select File_Name from CDRDB..FileLog_Diff where ID_Process = "+idP.Value;
            DataSet dts1 = (DataSet)Util.RunQueryByStmntatCDRDB(sql);
            fileName = (String) dts1.Tables[0].Rows[0][0];
            Response.Redirect("http://mecca.directo.com/Diffs_M3_M4/"+fileName);
            dts1.Dispose();
        }
    }
}
