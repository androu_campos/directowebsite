using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;

public partial class DTMonitorData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder q = new StringBuilder();
        q.Append( "SELECT Customer,sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,CAST(AVG(PDD) as int) AS PDD from OpSheetInc WHERE Customer like 'DATA' GROUP BY Customer UNION SELECT '**ALL**' as Customer,sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,CAST(AVG(PDD) as int) AS PDD  from OpSheetInc WHERE Customer like 'DATA'  ORDER BY TotalMinutes DESC");

        //Recent

        DataSet mydataSet1 = RunQuery(q, "MECCA2ConnectionString");
        gdvRecent.DataSource = mydataSet1.Tables[0];
        gdvRecent.DataBind();

        //Todays
        q.Replace("OpSheetInc", "OpSheetAll");

        DataSet myDataSet2 = RunQuery(q, "MECCA2ConnectionString");
        gdvToday.DataSource = myDataSet2.Tables[0];
        gdvToday.DataBind();

        //Yesterdays
        q.Replace("OpSheetAll", "OpSheetY");

        DataSet myDataSet3 = RunQuery(q, "MECCA2ConnectionString");
        gdvYesterday.DataSource = myDataSet3.Tables[0];
        gdvYesterday.DataBind();

        StringBuilder query = new StringBuilder();
        query.Append("select crdate as Modified from [mecca2].[dbo].sysobjects  where type = 'U' and name like 'OpSheetY'");
        DataSet ResultSet1;
        ResultSet1 = RunQuery(query, "MECCA2ConnectionString");
        lblYesterday.Text = "Last Modified:" + ResultSet1.Tables[0].Rows[0][0].ToString();
        //lblRefresh RECENT
        StringBuilder recentupdate = new StringBuilder();
        recentupdate.Append("select crdate as Modified from [mecca2].[dbo].sysobjects  where type = 'U' and name like 'OpSheetInc'");
        DataSet ResultSet2;
        ResultSet2 = RunQuery(recentupdate, "MECCA2ConnectionString");
        lblRecent.Text = "Last Modified:" + ResultSet2.Tables[0].Rows[0][0].ToString();
        lblRecent.Visible = true;
        //lblRefresh TODAY
        StringBuilder todayupdate = new StringBuilder();
        todayupdate.Append("select crdate as Modified from [mecca2].[dbo].sysobjects  where type = 'U' and name like 'OpSheetAll'");
        DataSet ResultSet3;
        ResultSet3 = RunQuery(todayupdate, "MECCA2ConnectionString");
        lblToday.Text = "Last Modified:" + ResultSet3.Tables[0].Rows[0][0].ToString();
        lblToday.Visible = true;

        lblBilling.Visible = false;
    }

    private System.Data.DataSet RunQuery(StringBuilder qry, string StringConnection)
    {

        System.Data.SqlClient.SqlCommand sqlQuery = new System.Data.SqlClient.SqlCommand();
        sqlQuery.CommandText = qry.ToString();


        string connectionString = ConfigurationManager.ConnectionStrings
        [StringConnection].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
