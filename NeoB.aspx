<%@ Page Language="C#" MasterPageFile="~/Master.master" StylesheetTheme="Theme1" AutoEventWireup="true" CodeFile="NeoB.aspx.cs" Inherits="NeoB" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Label ID="Label2" runat="server" Text="Neo" CssClass="labelTurkH"></asp:Label>
    
    <table align="left" bgcolor="black" height="100%" width="100%" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td bgcolor="black" style="width: 100px">
<asp:Panel ID="Panel1" runat="server" BackColor="Black" BorderColor="Transparent"
        Font-Bold="True" Font-Overline="False" ForeColor="White" GroupingText="Parameters"
        Height="100px" Width="811px" Direction="LeftToRight">
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp;<table>
            <tr>
                <td style="width: 128px">
                    Attempts &gt;</td>
                <td style="width: 127px">
                    <asp:TextBox ID="txtCallsMin" runat="server" Width="60px"></asp:TextBox></td>
                <td style="width: 75px">
                    Class</td>
                <td style="width: 100px">
                    <asp:DropDownList ID="ddlClass" runat="server">
                        <asp:ListItem Value="** ALL **">** ALL **</asp:ListItem>
                        <asp:ListItem Value="ON-NET">ON-NET</asp:ListItem>
                        <asp:ListItem>OFF-NET</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 62px">
                    Type</td>
                <td style="width: 100px">
                    <asp:DropDownList ID="ddlType" runat="server">
                        <asp:ListItem Selected="True" Value="** ALL **">** ALL **</asp:ListItem>
                        <asp:ListItem>CPP</asp:ListItem>
                        <asp:ListItem>FIXED</asp:ListItem>
                        <asp:ListItem>MPP</asp:ListItem>
                        <asp:ListItem>MOBILE</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 100px">
                    Report</td>
                <td style="width: 100px">
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem>** ALL **</asp:ListItem>
                        <asp:ListItem>MEXICO</asp:ListItem>
                        <asp:ListItem>ROW</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 100px">
                    <asp:Button ID="btnApply" runat="server" OnClick="btnApply_Click" Text="Apply Parameters" /></td>
            </tr>
            <tr>
                <td style="width: 128px">
                    Current Values</td>
                <td style="width: 127px">
                    <asp:Label ID="lbCall" runat="server"></asp:Label>&nbsp; Attempts</td>
                <td style="width: 75px">
                    Class</td>
                <td style="width: 100px">
                    <asp:Label ID="lbClass" runat="server"></asp:Label></td>
                <td style="width: 62px">
                    Type</td>
                <td style="width: 100px">
                    <asp:Label ID="lbType" runat="server"></asp:Label></td>
                <td style="width: 100px">
                    Report</td>
                <td style="width: 100px">
                    <asp:Label ID="Label1" runat="server"></asp:Label></td>
                <td style="width: 100px">
                    <asp:Button ID="btnShowGV"
            runat="server" OnClick="btnShowGV_Click" Text="Show Data Details" /></td>
            </tr>
        </table>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp;</asp:Panel>
            </td>
        </tr>
        <tr>
            <td bgcolor="black" style="width: 100px">
    <table width="100%">
        <tr>
            <td align="left" style="width: 100px" valign="top" bgcolor="black">
    <asp:TreeView ID="tvNEO" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="Large"
        ForeColor="LimeGreen" MaxDataBindDepth="10" OnTreeNodePopulate="tvNEO_TreeNodePopulate" EnableClientScript="False" NodeIndent="115" OnTreeNodeExpanded="tvNEO_TreeNodePopulate" PopulateNodesFromClient="False" BackColor="Black">
        <Nodes>
            <asp:TreeNode Expanded="False" PopulateOnDemand="True" SelectAction="SelectExpand"
                Text="Recent Traffic" Value="OpSheetInc"></asp:TreeNode>
            <asp:TreeNode Expanded="False" PopulateOnDemand="True" SelectAction="SelectExpand"
                Text="Today Traffic" Value="OpSheetAll"></asp:TreeNode>            
        </Nodes>
        <ParentNodeStyle BackColor="Black" />
        <HoverNodeStyle BackColor="Black" />
        <SelectedNodeStyle BackColor="Black" />
        <RootNodeStyle BackColor="Black" />
        <LeafNodeStyle BackColor="Black" ForeColor="Black" />
        <NodeStyle BackColor="Black" />
    </asp:TreeView>
            </td>
            <td align="center" style="width: 100px" valign="middle">
    <asp:GridView ID="gvNeo" runat="server" CellPadding="3" ForeColor="Black" GridLines="Vertical"
        Style="left: 256px; top: 466px" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" Visible="False">
        <FooterStyle BackColor="#CCCCCC" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="#CCCCCC" />
    </asp:GridView>
            </td>
        </tr>
    </table>
            </td>
        </tr>
    </table>
    
</asp:Content>

