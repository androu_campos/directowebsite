using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Master : System.Web.UI.MasterPage
{
    void Page_PreInit(object sender, EventArgs e)
    {
        Master.EnableTheming = true;
        Page.EnableTheming = true;
        Page.Theme = "Theme1";
        Page.StyleSheetTheme = "Theme1";

        //if (Page.Request.ServerVariables["http_user_agent"].ToLower().Contains("safari"))
        //{
        //    Page.ClientTarget = "uplevel";
        //}

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UserAgent.IndexOf("Chrome") > 0)
        {
            if (Request.Browser.Adapters.Count > 0)
            {
                Request.Browser.Adapters.Clear();
                Response.Redirect(Page.Request.Url.AbsoluteUri);
            }
        }

        try
        {

            MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adpBR = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
            MyDataSetTableAdapters.Billing_StatusDataTable tblBR = adpBR.GetData();

            string hometype = "";

            if (tblBR.Rows.Count > 0)//Billing running
            {
                lblBillingRunning.Visible = true;

            }
            else
            {
                lblBillingRunning.Visible = false;
            }
            //if (Session["Rol"].ToString() != string.Empty || (Session["Rol"].ToString() == "Customer" && Session["DirectoUser"].ToString() == "lpicache"))//Usuario Logeado            
            if (Session["Rol"].ToString() != string.Empty && Session["Rol"].ToString() != "Customer" && Session["Rol"].ToString() != "PHAccess")//Usuario Logeado            
            {

                if (Request.Cookies["Rol"] != null)
                {
                    Session["Rol"] = Request.Cookies["Rol"].Value.ToString();
                    Session["DirectoUser"] = Request.Cookies["userMecca"].Value.ToString();
                }


                lblUserName.Text = Session["DirectoUser"].ToString();
                string urlrequested = Request.RawUrl.ToString();
                int urlLength = urlrequested.Length;
                int index = urlrequested.LastIndexOf("/");
                int sufix = urlrequested.LastIndexOf("aspx");
                string url = urlrequested.Substring(index, sufix - index + 4);

                if (url == "/HomeW.aspx")
                {
                    hometype = "W";
                    Menu1.Items.Add(new MenuItem("Sales", "Sales"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Wholesale Master Sales Log ", "MasterSalesLog", "./Pag/imas/int/f-azul.gif", "~/MasterSalesLog.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Builder ", "QueryBuilder", "./Pag/imas/int/f-azul.gif", "~/Qbuilder.aspx"));
                    //Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Monitor ", "QueryMonitor", "./Pag/imas/int/f-azul.gif", "~/Monitor.aspx"));
                    Session["hometype"] = hometype;

                }
                else if (url == "/HomeR.aspx")
                {
                    hometype = "R";

                    Menu1.Items.Clear();

                    Menu1.Items.Add(new MenuItem("Sales Tools", "Sales"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" ICS Master Sales Log (Outbound)", "ICSMasterSalesLog", "./Pag/imas/int/f-azul.gif", "~/ICSMasterSalesLog.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Monitor (Outbound)", "ICSMonitor", "./Pag/imas/int/f-azul.gif", "~/ICSMonitor.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Builder (Outbound)", "QueryBuilder", "./Pag/imas/int/f-azul.gif", "~/Qbuilder.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Builder (Inbound)", "ICSQueryBuilder", "./Pag/imas/int/f-azul.gif", "~/ICSQbuilder.aspx"));

                    Menu1.Items.Add(new MenuItem("Reports", "Reports"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" CallMex Report", "CallMexReport", "./Pag/imas/int/f-azul.gif", "~/CallMexReport.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Recent customer traffic", "RecentCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer&Table=OpSheetInc"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Recent vendor traffic", "RecentVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Vendor&Table=OpSheetInc"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Today's customer traffic", "TodayCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Today's vendor traffic", "TodayVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Vendor"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Yesterday's customer traffic", "YesterdayCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer&Table=OpSheetY"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Yesterday's vendor traffic", "YesterdayVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Vendor&Table=OpSheetY"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" ICS Report", "ICSReport", "./Pag/imas/int/f-azul.gif", "~/ICSReport.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" ICS Report Cards", "ICSReportFive", "./Pag/imas/int/f-azul.gif", "~/ICSReportFive.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" ICS Report Cards Guatemala", "ICSGuatemala", "./Pag/imas/int/f-azul.gif", "~/ICSGuatemala.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" ICS Report Cards El Salvador", "ICSSalvador", "./Pag/imas/int/f-azul.gif", "~/ICSSalvador.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" ICS Traffic Log", "ICSHistory", "./Pag/imas/int/f-azul.gif", "~/ICSHistory.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" DID Traffic Log", "DidDailyTraffic", "./Pag/imas/int/f-azul.gif", "~/DidDailyTraffic.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" DID Daily Minutes", "DidCustStTraffic", "./Pag/imas/int/f-azul.gif", "~/DidCustState.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" DID Monitor", "DIDMonitor", "./Pag/imas/int/f-azul.gif", "~/DidMonitor.aspx"));

                    Menu1.Items.Add(new MenuItem("Graphs", "Graphs"));
                    Menu1.Items[2].ChildItems.Add(new MenuItem(" ICS DID Ports Graphs", "ICSGraphs", "./Pag/imas/int/f-azul.gif", "~/ICSGraphs.aspx"));
                    Menu1.Items[2].ChildItems.Add(new MenuItem(" ICS Active Ports Graphs", "ICSPortsGraph", "./Pag/imas/int/f-azul.gif", "~/ICSPortsGraph.aspx"));
                    Menu1.Items[2].ChildItems.Add(new MenuItem(" ICS Platform Ports Graphs", "ICSFrameworks", "./Pag/imas/int/f-azul.gif", "~/ICSFrameworks.aspx"));
                    Menu1.Items[2].ChildItems.Add(new MenuItem(" ICS First Use Ports Graphs", "ICSUse", "./Pag/imas/int/f-azul.gif", "~/ICSFUse.aspx"));

                    Menu1.Items.Add(new MenuItem("DID Tools", "DID Tools"));
                    Menu1.Items[3].ChildItems.Add(new MenuItem(" DID List", "DIDGuide", "./Pag/imas/int/f-azul.gif", "~/DIDGuide.aspx"));
                    Menu1.Items[3].ChildItems.Add(new MenuItem(" Add DID", "AddDID", "./Pag/imas/int/f-azul.gif", "~/AddDid.aspx"));

                    Session["hometype"] = hometype;

                    HyperLink1.NavigateUrl = "~/HomeR.aspx";
                }
                else if (url == "/HomeCC.aspx")
                {
                    hometype = "CC";

                    Menu1.Items.Clear();

                    Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Master Sales Log", "CCMasterSalesLog", "./Pag/imas/int/f-azul.gif", "~/MasterSalesLogCC.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Monitor ", "CCMonitor", "./Pag/imas/int/f-azul.gif", "~/Monitor.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Builder ", "CCQBuilder", "./Pag/imas/int/f-azul.gif", "~/QBuilderCC.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Customer / Vendor Rates", "Customer/VendorRates", "./Pag/imas/int/f-azul.gif", "~/RateViewer.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advanced Query Monitor Hour", "CCMonitorHour", "./Pag/imas/int/f-azul.gif", "~/MonitorHourCC.aspx"));

                    if (Session["DirectoUser"].ToString() == "arturo.marquez" || Session["DirectoUser"].ToString() == "rodrigo.andrade")
                    {
                        Menu1.Items[0].ChildItems.Add(new MenuItem(" Prepaid CallCenters", "PrepaidCallCenters", "./Pag/imas/int/f-azul.gif", "~/PrepaidCC.aspx"));
                    }

                    Menu1.Items.Add(new MenuItem("Graphs", "Graphs"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Per day/time for Provider", "Per day/time for Provider", "./Pag/imas/int/f-azul.gif", "~/PortsGraphCC.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Per day/time for Provider Comparative", "Per day/time for Provider Comparative", "./Pag/imas/int/f-azul.gif", "~/PortsGraphComparativeCC.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer", "GraphPanelCustomer", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer 2", "GraphPanelCustomer2", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC2.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer 3", "GraphPanelCustomer3", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC3.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Vendor", "GraphPanelVendor", "./Pag/imas/int/f-azul.gif", "~/GraphGridVCC.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Cust Last 7Days", "GraphPanelCustomer", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC7.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Vend Last 7Days", "GraphPanelVendor", "./Pag/imas/int/f-azul.gif", "~/GraphGridVCC7.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel CallCenters Grid ", "GraphPanelCCGrid", "./Pag/imas/int/f-azul.gif", "~/GraphGridGCC.aspx"));

                    Session["hometype"] = hometype;

                    HyperLink1.NavigateUrl = "~/HomeCC.aspx";
                }
                else if (url == "/HomeAA.aspx")
                {
                    hometype = "AA";

                    Menu1.Items.Clear();

                    Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Master Sales Log", "AAMasterSalesLog", "./Pag/imas/int/f-azul.gif", "~/MasterSalesLogAA.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Monitor ", "AAMonitor", "./Pag/imas/int/f-azul.gif", "~/MonitorAA.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Builder ", "AAQBuilder", "./Pag/imas/int/f-azul.gif", "~/QBuilderAA.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Customer / Vendor Rates", "Customer/VendorRates", "./Pag/imas/int/f-azul.gif", "~/RateViewerAA.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advanced Query Monitor Hour", "AAMonitorHour", "./Pag/imas/int/f-azul.gif", "~/MonitorHourAA.aspx"));
                    
                    Menu1.Items.Add(new MenuItem("Graphs", "Graphs"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Per day/time for Provider", "Per day/time for Provider", "./Pag/imas/int/f-azul.gif", "~/PortsGraphAA.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Per day/time for Provider Comparative", "Per day/time for Provider Comparative", "./Pag/imas/int/f-azul.gif", "~/PortsGraphComparativeAA.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer", "GraphPanelCustomer", "./Pag/imas/int/f-azul.gif", "~/GraphGridAA.aspx"));
                    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer 2", "GraphPanelCustomer2", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC2.aspx"));
                    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer 3", "GraphPanelCustomer3", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC3.aspx"));
                    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Vendor", "GraphPanelVendor", "./Pag/imas/int/f-azul.gif", "~/GraphGridVCC.aspx"));
                    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Cust Last 7Days", "GraphPanelCustomer", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC7.aspx"));
                    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Vend Last 7Days", "GraphPanelVendor", "./Pag/imas/int/f-azul.gif", "~/GraphGridVCC7.aspx"));
                    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel CallCenters Grid ", "GraphPanelCCGrid", "./Pag/imas/int/f-azul.gif", "~/GraphGridGCC.aspx"));

                    Session["hometype"] = hometype;

                    HyperLink1.NavigateUrl = "~/HomeAA.aspx";
                }
                else if (url == "/HomeCCPH.aspx")
                {
                    hometype = "CCPH";

                    Menu1.Items.Clear();

                    Menu1.Items.Add(new MenuItem("CDRS", "CDRS"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" CDRS Viewer", "CDRSViewerCCPH", "./Pag/imas/int/f-azul.gif", "~/CdrsViewerCCPH.aspx"));


                    Session["hometype"] = hometype;

                    HyperLink1.NavigateUrl = "~/HomeCCPH.aspx";
                }
                else if(url == "/HomeSMS.aspx")
                {
                    hometype = "SMS";
                    Menu1.Items.Clear();

                    Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" DailyReport", "DailyReport", "./Pag/imas/int/f-azul.gif", "~/SMSDailyReport.aspx"));


                    Session["hometype"] = hometype;

                    HyperLink1.NavigateUrl = "~/HomeSMS.aspx";

                }
                else if (url == "/Home.aspx" || url == "/HomeJun.aspx" || url == "/HomeCC.aspx" || url == "/HomeCCCust.aspx" || url == "/HomeCCPH.aspx")
                {
                    Session["hometype"] = "";
                }
                else if (url == "/HomeCM.aspx")
                {
                    HyperLink1.NavigateUrl = "~/HomeCM.aspx";
                    Menu1.Items.Clear();
                    Menu1.Items.Add(new MenuItem("Sales", "Sales"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Master Sales Log ROW", "MasterSalesLog", "./Pag/imas/int/f-azul.gif", "~/MasterSalesLogRow.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Active Ports Graph", "ActivePorstGraph", "./Pag/imas/int/f-azul.gif", "~/PortsGraph.aspx"));

                    Session["hometype"] = "";
                }

                else if (Session["hometype"].ToString() == "W")
                {
                    Menu1.Items.Add(new MenuItem("Sales", "Sales"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Wholesale Master Sales Log ", "MasterSalesLog", "./Pag/imas/int/f-azul.gif", "~/MasterSalesLog.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Builder ", "QueryBuilder", "./Pag/imas/int/f-azul.gif", "~/Qbuilder.aspx"));
                    //Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Monitor ", "QueryMonitor", "./Pag/imas/int/f-azul.gif", "~/Monitor.aspx"));
                }
                else if (Session["hometype"].ToString() == "R")
                {
                    Menu1.Items.Clear();
                    Menu1.Items.Add(new MenuItem("Sales Tools", "Sales"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" ICS Master Sales Log (Outbound)", "ICSMasterSalesLog", "./Pag/imas/int/f-azul.gif", "~/ICSMasterSalesLog.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Monitor (Outbound)", "ICSMonitor", "./Pag/imas/int/f-azul.gif", "~/ICSMonitor.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Builder (Outbound)", "QueryBuilder", "./Pag/imas/int/f-azul.gif", "~/Qbuilder.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Builder (Inbound)", "ICSQueryBuilder", "./Pag/imas/int/f-azul.gif", "~/ICSQbuilder.aspx"));

                    Menu1.Items.Add(new MenuItem("Reports", "Reports"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" CallMex Report", "CallMexReport", "./Pag/imas/int/f-azul.gif", "~/CallMexReport.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Recent customer traffic", "RecentCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer&Table=OpSheetInc"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Recent vendor traffic", "RecentVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Vendor&Table=OpSheetInc"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Today's customer traffic", "TodayCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Today's vendor traffic", "TodayVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Vendor"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Yesterday's customer traffic", "YesterdayCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer&Table=OpSheetY"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Yesterday's vendor traffic", "YesterdayVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Vendor&Table=OpSheetY"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" ICS Report", "ICSReport", "./Pag/imas/int/f-azul.gif", "~/ICSReport.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" ICS Report Cards", "ICSReportFive", "./Pag/imas/int/f-azul.gif", "~/ICSReportFive.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" ICS Report Cards Guatemala", "ICSGuatemala", "./Pag/imas/int/f-azul.gif", "~/ICSGuatemala.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" ICS Report Cards El Salvador", "ICSSalvador", "./Pag/imas/int/f-azul.gif", "~/ICSSalvador.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" ICS Traffic Log", "ICSHistory", "./Pag/imas/int/f-azul.gif", "~/ICSHistory.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" DID Traffic Log", "DidDailyTraffic", "./Pag/imas/int/f-azul.gif", "~/DidDailyTraffic.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" DID Daily Minutes", "DidCustStTraffic", "./Pag/imas/int/f-azul.gif", "~/DidCustState.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" DID Monitor", "DIDMonitor", "./Pag/imas/int/f-azul.gif", "~/DidMonitor.aspx"));

                    Menu1.Items.Add(new MenuItem("Graphs", "Graphs"));
                    Menu1.Items[2].ChildItems.Add(new MenuItem(" ICS DID Ports Graphs", "ICSGraphs", "./Pag/imas/int/f-azul.gif", "~/ICSGraphs.aspx"));
                    Menu1.Items[2].ChildItems.Add(new MenuItem(" ICS Active Ports Graphs", "ICSPortsGraph", "./Pag/imas/int/f-azul.gif", "~/ICSPortsGraph.aspx"));
                    Menu1.Items[2].ChildItems.Add(new MenuItem(" ICS Platform Ports Graphs", "ICSFrameworks", "./Pag/imas/int/f-azul.gif", "~/ICSFrameworks.aspx"));
                    Menu1.Items[2].ChildItems.Add(new MenuItem(" ICS First Use Ports Graphs", "ICSUse", "./Pag/imas/int/f-azul.gif", "~/ICSFUse.aspx"));

                    Menu1.Items.Add(new MenuItem("DID Tools", "DID Tools"));
                    Menu1.Items[3].ChildItems.Add(new MenuItem(" DID List", "DIDGuide", "./Pag/imas/int/f-azul.gif", "~/DIDGuide.aspx"));
                    Menu1.Items[3].ChildItems.Add(new MenuItem(" Add DID", "AddDID", "./Pag/imas/int/f-azul.gif", "~/AddDid.aspx"));


                    HyperLink1.NavigateUrl = "~/HomeR.aspx";

                }

                else if (Session["hometype"].ToString() == "CC")
                {


                    Menu1.Items.Clear();

                    Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Master Sales Log", "CCMasterSalesLog", "./Pag/imas/int/f-azul.gif", "~/MasterSalesLogCC.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Monitor ", "CCMonitor", "./Pag/imas/int/f-azul.gif", "~/Monitor.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Builder ", "CCQBuilder", "./Pag/imas/int/f-azul.gif", "~/QBuilderCC.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Customer / Vendor Rates", "Customer/VendorRates", "./Pag/imas/int/f-azul.gif", "~/RateViewer.aspx"));

                    if (Session["DirectoUser"].ToString() == "arturo.marquez" || Session["DirectoUser"].ToString() == "rodrigo.andrade")
                    {
                        Menu1.Items[0].ChildItems.Add(new MenuItem(" Prepaid CallCenters", "PrepaidCallCenters", "./Pag/imas/int/f-azul.gif", "~/PrepaidCC.aspx"));
                    }
                    Menu1.Items.Add(new MenuItem("Graphs", "Graphs"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Per day/time for Provider", "Per day/time for Provider", "./Pag/imas/int/f-azul.gif", "~/PortsGraphCC.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer", "GraphPanelCustomer", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer 2", "GraphPanelCustomer2", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC2.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer 3", "GraphPanelCustomer3", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC3.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Vendor", "GraphPanelVendor", "./Pag/imas/int/f-azul.gif", "~/GraphGridVCC.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Cust Last 7Days", "GraphPanelCustomer", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC7.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Vend Last 7Days", "GraphPanelVendor", "./Pag/imas/int/f-azul.gif", "~/GraphGridVCC7.aspx"));


                    HyperLink1.NavigateUrl = "~/HomeCC.aspx";
                }
                else if (Session["hometype"].ToString() == "AA")
                {


                    Menu1.Items.Clear();

                    Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Master Sales Log", "CCMasterSalesLog", "./Pag/imas/int/f-azul.gif", "~/MasterSalesLogAA.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Monitor ", "CCMonitor", "./Pag/imas/int/f-azul.gif", "~/MonitorAA.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advance Query Builder ", "CCQBuilder", "./Pag/imas/int/f-azul.gif", "~/QBuilderAA.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Customer / Vendor Rates", "Customer/VendorRates", "./Pag/imas/int/f-azul.gif", "~/RateViewerAA.aspx"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Advanced Query Monitor Hour", "CCMonitorHour", "./Pag/imas/int/f-azul.gif", "~/MonitorHourAA.aspx"));

                    if (Session["DirectoUser"].ToString() == "arturo.marquez" || Session["DirectoUser"].ToString() == "rodrigo.andrade")
                    {
                        Menu1.Items[0].ChildItems.Add(new MenuItem(" Prepaid CallCenters", "PrepaidCallCenters", "./Pag/imas/int/f-azul.gif", "~/PrepaidCC.aspx"));
                    }
                    Menu1.Items.Add(new MenuItem("Graphs", "Graphs"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Per day/time for Provider", "Per day/time for Provider", "./Pag/imas/int/f-azul.gif", "~/PortsGraphAA.aspx"));
                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer", "GraphPanelCustomer", "./Pag/imas/int/f-azul.gif", "~/GraphGridAA.aspx"));
                    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer 2", "GraphPanelCustomer2", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC2.aspx"));
                    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Customer 3", "GraphPanelCustomer3", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC3.aspx"));
                    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Vendor", "GraphPanelVendor", "./Pag/imas/int/f-azul.gif", "~/GraphGridVCC.aspx"));
                    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Cust Last 7Days", "GraphPanelCustomer", "./Pag/imas/int/f-azul.gif", "~/GraphGridCC7.aspx"));
                    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Graph Panel Vend Last 7Days", "GraphPanelVendor", "./Pag/imas/int/f-azul.gif", "~/GraphGridVCC7.aspx"));
                    
                    HyperLink1.NavigateUrl = "~/HomeAA.aspx";
                }
                else if(Session["hometype"].ToString() == "SMS")
                {
                    
                    Menu1.Items.Clear();

                    Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" DailyReport", "DailyReport", "./Pag/imas/int/f-azul.gif", "~/SMSDailyReport.aspx"));

                    HyperLink1.NavigateUrl = "~/HomeSMS.aspx";
                }

                else if (Session["hometype"].ToString() == "CCPH")
                {

                    Menu1.Items.Clear();

                    Menu1.Items.Add(new MenuItem(" CDRS", "CDRS"));
                    Menu1.Items[0].ChildItems.Add(new MenuItem(" CDRS Viewer", "CDRSViewer", "./Pag/imas/int/f-azul.gif", "~/CdrsViewerCCPH.aspx"));
                    HyperLink1.NavigateUrl = "~/HomeCCPH.aspx";
                }
                //else if (Session["DirectoUser"].ToString() == "jim.baumhart")
                //{
                //    HyperLink1.DataBind();
                //    HyperLink1.NavigateUrl = "~/HomeCM.aspx";

                //    Session["hometype"] = "";
                //}


                url = "~" + url;


                MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpS = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                MyDataSetTableAdapters.MainNodesDataTable tblS = adpS.GetDataByUrlnRol(url, Session["Rol"].ToString());

                MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpS1 = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                MyDataSetTableAdapters.MainNodesDataTable tblS1 = adpS1.GetDataByURLnRol2(url, Session["Rol"].ToString());



                if (tblS.Count == 0 && Session["Rol"].ToString() != "Admins" && tblS1.Count == 0)//Verifica si el usuario tiene acceso a esta pagina
                {
                    Response.Redirect("ContactUs.aspx", false);
                }
                else
                {

                    //User, Request, IPAddress and DateTime.

                    Util.RunQryAtCDRDB_void("INSERT INTO [MECCA2].[dbo].[UserSniper] VALUES ('" + Session["DirectoUser"].ToString() + "','" + Request.ServerVariables["REMOTE_ADDR"] + "','" + url + "',GETDATE())");

                    if (!Page.IsPostBack)//Construye el Treeview                    
                    {
                        if (Session["hometype"].ToString() == "")
                        {
                            //obtiene el rol del usuario
                            string rol = Session["Rol"].ToString();
                            //Make tree new                 
                            //query qe regresa los nodos principales
                            MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter adpN = new MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter();
                            MyDataSetTableAdapters.Roles_Nodes1DataTable tblN = adpN.GetDataByRole(rol, rol);

                            makeMenu(tblN);//llena el menu control

                            if (Session["DirectoUser"].ToString() == "hvalle")
                            {
                                Menu1.Items.Add(new MenuItem("Today", "TodayTraffic"));
                                Menu1.Items[4].ChildItems.Add(new MenuItem(" Today's customer traffic", "TodayCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer"));
                                Menu1.Items[4].ChildItems.Add(new MenuItem(" Today's vendor traffic", "TodayVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Vendor"));

                                Menu1.Items.Add(new MenuItem("Recent", "RecenTraffic"));
                                Menu1.Items[5].ChildItems.Add(new MenuItem(" Recent customer traffic", "RecentCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer&Table=OpSheetInc"));
                                Menu1.Items[5].ChildItems.Add(new MenuItem(" Recent vendor traffic", "RecentVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Vendor&Table=OpSheetInc"));

                                Menu1.Items.Add(new MenuItem("Yesterday", "YesterTraffic"));
                                Menu1.Items[6].ChildItems.Add(new MenuItem(" Yesterday's customer traffic", "YesterdayCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer&Table=OpSheetY"));
                                Menu1.Items[6].ChildItems.Add(new MenuItem(" Yesterday's vendor traffic", "YesterdayVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer&Table=OpSheetY"));
                            }
                            //end Make                    

                            if (Session["DirectoUser"].ToString() == "djassan" || Session["DirectoUser"].ToString() == "hvalle" || Session["DirectoUser"].ToString() == "fguelfi" || Session["DirectoUser"].ToString() == "scullen" || Session["DirectoUser"].ToString() == "hariel" || Session["DirectoUser"].ToString() == "msanvicente")
                            {
                                Menu1.Items[0].ChildItems.Add(new MenuItem(" Live Traffic Watch", "Live Traffic Watch", "./Pag/imas/int/f-azul.gif", "~/Live.aspx"));

                            }
                            if (Session["DirectoUser"].ToString() == "avillareal")
                            {
                                Menu1.Items[4].ChildItems.Add(new MenuItem(" Master Sales Log", "Master Sales Log", "./Pag/imas/int/f-azul.gif", "~/MasterSalesLog.aspx"));
                            }
                            if (Session["DirectoUser"].ToString() == "jmanatlao")
                            {
                                Menu1.Items.Clear();
                                Menu1.Items.Add(new MenuItem("Traffic", "Traffic"));
                                Menu1.Items[0].ChildItems.Add(new MenuItem(" Live traffic watch (Summary)", "Live", "./Pag/imas/int/f-azul.gif", "~/live.aspx"));

                                Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                                Menu1.Items[1].ChildItems.Add(new MenuItem(" Advance Query Monitor", "Monitor", "./Pag/imas/int/f-azul.gif", "~/Monitor.aspx"));
                                Menu1.Items[1].ChildItems.Add(new MenuItem(" Advanced Query Monitor Hour", "MonitorHour", "./Pag/imas/int/f-azul.gif", "~/MonitorHour.aspx"));

                                Menu1.Items.Add(new MenuItem("Graphs", "Graphs"));
                                Menu1.Items[2].ChildItems.Add(new MenuItem(" Adctive Ports Graph", "ActivePorstGraph", "./Pag/imas/int/f-azul.gif", "~/PortsGraphJun.aspx"));

                                HyperLink1.NavigateUrl = "~/HomeJun.aspx";

                            }
                            if (Session["DirectoUser"].ToString() == "csph")
                            {
                                Menu1.Items.Clear();

                                Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                                Menu1.Items[0].ChildItems.Add(new MenuItem(" Advanced QMonitor AllAccess", "Advanced QMonitor AllAccess", "./Pag/imas/int/f-azul.gif", "~/MonitorAllAccessUnl.aspx"));


                                HyperLink1.NavigateUrl = "~/HomeJun.aspx";

                            }
                            if (Session["DirectoUser"].ToString() == "amarquez" || Session["DirectoUser"].ToString() == "mmerlos")
                            {
                                Menu1.Items[0].ChildItems.Add(new MenuItem(" SWAP", "SWAP", "./Pag/imas/int/f-azul.gif", "~/SWAP.aspx"));
                            }
                            if (Session["DirectoUser"].ToString() == "david@directo.com" || Session["DirectoUser"].ToString() == "fcarbia" || Session["DirectoUser"].ToString() == "rlopez" || Session["DirectoUser"].ToString() == "cfuentes" || Session["DirectoUser"].ToString() == "avillareal")
                            {
                                Menu1.Items[3].ChildItems.Add(new MenuItem(" Graph Panel Santander", "Graph Panel Santander", "./Pag/imas/int/f-azul.gif", "~/GraphGridSant.aspx"));
                                Menu1.Items[3].ChildItems.Add(new MenuItem(" Graph Panel TELMEX ", "Graph Panel TELMEX", "./Pag/imas/int/f-azul.gif", "~/GraphGridTMX.aspx"));
                            }

                            if (rol == "CallCenter")
                            {
                                HyperLink1.NavigateUrl = "~/HomeCC.aspx";
                            }
                            if (rol == "CallCenterPH")
                            {
                                Menu1.Items.Clear();

                                Menu1.Items.Add(new MenuItem("CDRS", "CDRS"));
                                Menu1.Items[0].ChildItems.Add(new MenuItem(" CDRS Viewer", "CDRSViewerCCPH", "./Pag/imas/int/f-azul.gif", "~/CdrsViewerCCPH.aspx"));

                                HyperLink1.NavigateUrl = "~/HomeCCPH.aspx";
                            }
                            if (rol == "CallCenterCust")
                            {
                                HyperLink1.NavigateUrl = "~/HomeCCCust.aspx";

                                if (Session["DirectoUser"].ToString() == "dir-tip-18")
                                {
                                    Menu1.Items.Clear();
                                    Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Traffic Stats", "Stats", "./Pag/imas/int/f-azul.gif", "~/TrafficCC.aspx"));

                                    Menu1.Items.Add(new MenuItem("Graphs", "Graphs"));
                                    Menu1.Items[1].ChildItems.Add(new MenuItem(" Active Ports Graph", "ActivePorstGraph", "./Pag/imas/int/f-azul.gif", "~/PortsGraphCCCust.aspx"));

                                }
                                else if (Session["DirectoUser"].ToString() == "cc-eci-hispano")
                                {
                                    Menu1.Items.Clear();
                                    Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Traffic Stats", "TrafficStats", "./Pag/imas/int/f-azul.gif", "~/TrafficCC.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Active Ports Graph", "ActivePortsGraph", "./Pag/imas/int/f-azul.gif", "~/PortsGraphCCCust.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Active Calls Graph", "ActiveCallsGraph", "./Pag/imas/int/f-azul.gif", "~/CallsGraphCCCust.aspx"));
                                }
                                else if (Session["DirectoUser"].ToString() == "cc-bconnectsamarilla")
                                {
                                    Menu1.Items.Clear();
                                    Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Traffic Stats", "TrafficStats", "./Pag/imas/int/f-azul.gif", "~/TrafficCC.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Active Ports Graph", "ActivePortsGraph", "./Pag/imas/int/f-azul.gif", "~/PortsGraphCCCust.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Active Calls Graph", "ActiveCallsGraph", "./Pag/imas/int/f-azul.gif", "~/CallsGraphCCCust.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" CDR", "CDR", "./Pag/imas/int/f-azul.gif", "~/CDRsDownloaderCC.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Billed Traffic", "BilledTraffic", "./Pag/imas/int/f-azul.gif", "~/QBuilderDateCC.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" BlackList", "BlackList", "./Pag/imas/int/f-azul.gif", "~/blacklist.aspx"));
                                }
                                else if (Session["DirectoUser"].ToString() == "santander-demo")
                                {
                                    Menu1.Items.Clear();
                                    Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Traffic Stats", "TrafficStats", "./Pag/imas/int/f-azul.gif", "~/TrafficCC.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Active Ports Graph", "ActivePortsGraph", "./Pag/imas/int/f-azul.gif", "~/PortsGraphCCCust.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Active Calls Graph", "ActiveCallsGraph", "./Pag/imas/int/f-azul.gif", "~/CallsGraphCCCust.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" CDR", "CDR", "./Pag/imas/int/f-azul.gif", "~/CDRsDownloaderCC.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Billed Traffic", "BilledTraffic", "./Pag/imas/int/f-azul.gif", "~/QBuilderDateCC.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Black List Dinamico", "BlackListDinamico", "./Pag/imas/int/f-azul.gif", "~/BlackListDinamico.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Black List Reus", "BlackListReus", "./Pag/imas/int/f-azul.gif", "~/BlackListReus.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Black List Santander", "BlackListSantander", "./Pag/imas/int/f-azul.gif", "~/BlackListSant.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Qualification", "Qualification", "./Pag/imas/int/f-azul.gif", "~/Qualification.aspx"));
                                }
                                else
                                {
                                    Menu1.Items.Clear();
                                    Menu1.Items.Add(new MenuItem("Tools", "Tools"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Traffic Stats", "TrafficStats", "./Pag/imas/int/f-azul.gif", "~/TrafficCC.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Traffic Stats Hour", "TrafficStatsHour", "./Pag/imas/int/f-azul.gif", "~/TrafficCCHour.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Active Ports Graph", "ActivePortsGraph", "./Pag/imas/int/f-azul.gif", "~/PortsGraphCCCust.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Active Calls Graph", "ActiveCallsGraph", "./Pag/imas/int/f-azul.gif", "~/CallsGraphCCCust.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" CDR", "CDR", "./Pag/imas/int/f-azul.gif", "~/CDRsDownloaderCC.aspx"));
                                    Menu1.Items[0].ChildItems.Add(new MenuItem(" Billed Traffic", "BilledTraffic", "./Pag/imas/int/f-azul.gif", "~/QBuilderDateCC.aspx"));

                                }
                            }
                            else if (Session["DirectoUser"].ToString() == "arturo.marquez" || Session["DirectoUser"].ToString() == "yurema.villa" && Session["DirectoUser"].ToString() != "sharon.murphy")
                            {
                                Menu1.Items[2].ChildItems.Add(new MenuItem(" Last Week LCR Volumes", "Last Week LCR Volumes", "./Pag/imas/int/f-azul.gif", "~/LWLCR.aspx"));
                            }
                            else if (Session["DirectoUser"].ToString() == "arturo.marquez")
                            {
                                Menu1.Items[0].ChildItems.Add(new MenuItem(" Upload", "Upload", "./Pag/imas/int/f-azul.gif", "~/CopyUpload.aspx"));
                                Menu1.Items[0].ChildItems.Add(new MenuItem(" Bilateral Agreement", "Bilat", "./Pag/imas/int/f-azul.gif", "~/Bilateral.aspx"));
                            }
                            else if (Session["DirectoUser"].ToString() == "graphsant")
                            {
                                Menu1.Items.Clear();
                                Menu1.Items.Add(new MenuItem("Graphs", "Graphs"));
                                Menu1.Items[0].ChildItems.Add(new MenuItem(" Graph Panel Santander", "GraphPanelSantander", "./Pag/imas/int/f-azul.gif", "~/GraphGridSant.aspx"));
                            }
                            //else if (Session["DirectoUser"].ToString() == "jim.baumhart")
                            //{
                            //    HyperLink1.DataBind();
                            //    HyperLink1.NavigateUrl = "~/HomeCM.aspx";
                            //}
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("SignIn.aspx", false);
            }
        }

        catch (Exception ex)
        {
            string errormessage = ex.Message.ToString();

        }
    }

    protected void lnkCloseSesion_Click(object sender, EventArgs e)
    {

        if (Session["RememberMe"].ToString() == "ON" || Request.Cookies["userMecca"] != null)
        {//clear cookies
            Response.Cookies["userMecca"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["passMecca"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["link"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["Rol"].Expires = DateTime.Now.AddDays(-1);
            Util.RunQryAtCDRDB_void("UPDATE [CDRDB].[dbo].[login] SET [Active] = '0' WHERE username ='" + Session["DirectoUser"].ToString() + "'");
            Session["RememberMe"] = string.Empty;
            Session["DirectoUser"] = string.Empty;
            Response.Redirect("~/SignIn.aspx");
        }
        else
        {
            Util.RunQryAtCDRDB_void("UPDATE [CDRDB].[dbo].[login] SET [Active] = '0' WHERE username ='" + Session["DirectoUser"].ToString() + "'");
            Session["Rol"] = string.Empty;
            Session["DirectoUser"] = string.Empty;
            Response.Redirect("~/SignIn.aspx");
        }
    }

    private void makeMenu(DataTable tblN)
    {
        int jN = tblN.Rows.Count;
        string[] selected = new string[jN];
        for (int i = 0; i < jN; i++)
        {
            string rolName = tblN.Rows[i]["NodeName"].ToString();
            //Nodos primarios   nivel 1                        
            Menu1.Items.Add(new MenuItem(rolName, i.ToString()));
            int idNodeFhater = Convert.ToInt32(tblN.Rows[i]["Id"]);
            Menu1.Items[i].Selectable = false;

            //Llena los nodos secundarios  nivel 2
            MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpBN = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
            MyDataSetTableAdapters.MainNodesDataTable tblBN = adpBN.GetDataByFatherId(idNodeFhater);
            int rbN = tblBN.Rows.Count;
            for (int n = 0; n < rbN; n++)
            {
                string nameNode = tblBN.Rows[n]["Name"].ToString();
                string urlPath = tblBN.Rows[n]["Url"].ToString();

                if (i == 0 && n == 6 && Session["DirectoUser"].ToString() != "yurema.villa" && Session["Rol"].ToString() == "Comercial" && Session["DirectoUser"].ToString() != "manuel.moreno" && Session["DirectoUser"].ToString() != "jorge.ibarra")
                {

                }
                else if (i == 0 && n == 11 && Session["DirectoUser"].ToString() != "jorge.ibarra" && Session["DirectoUser"].ToString() != "yurema.villa" && Session["Rol"].ToString() == "Comercial")
                {
                    Menu1.Items[i].ChildItems.RemoveAt(9);
                }
                else
                {
                    Menu1.Items[i].ChildItems.Add(new MenuItem(nameNode, nameNode, "./Pag/imas/int/f-azul.gif", urlPath));
                }

            }
        }
        //if (Session["DirectoUser"].ToString() == "jim.baumhart")
        //{
        //    Menu1.Items.Clear();
        //    Menu1.Items.Add(new MenuItem("Sales", "Sales"));
        //    Menu1.Items[0].ChildItems.Add(new MenuItem(" Master Sales Log ROW", "MasterSalesLog", "./Pag/imas/int/f-azul.gif", "~/MasterSalesLogROW.aspx"));
        //    Menu1.Items[0].ChildItems.Add(new MenuItem(" Master Sales Log MX", "MasterSalesLogMX", "./Pag/imas/int/f-azul.gif", "~/MasterSalesLogM.aspx"));
        //    Menu1.Items[0].ChildItems.Add(new MenuItem(" Active Ports Graph", "ActivePorstGraph", "./Pag/imas/int/f-azul.gif", "~/PortsGraph.aspx"));
        //    Menu1.Items.Add(new MenuItem("Tools", "Tools"));//agregar CM a las direciones para que sean las del Country Manager, en este caso Jim
        //    Menu1.Items[1].ChildItems.Add(new MenuItem(" Advance Query Builder", "AdvanceQueryMonitorHour", "./Pag/imas/int/f-azul.gif", "~/QBuilderCM.aspx"));
        //    Menu1.Items[1].ChildItems.Add(new MenuItem(" Advance Query Monitor", "AdvanceQueryMonitor", "./Pag/imas/int/f-azul.gif", "~/MonitorCM.aspx"));
        //    Menu1.Items[1].ChildItems.Add(new MenuItem(" Advance Query Monitor Hour", "AdvanceQueryMonitorHour", "./Pag/imas/int/f-azul.gif", "~/MonitorHourCM.aspx"));
        //    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Advance Query Builder", "AdvanceQueryMonitorHour", "./Pag/imas/int/f-azul.gif", "~/UnderConstruction.aspx"));
        //    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Advance Query Monitor", "AdvanceQueryMonitor", "./Pag/imas/int/f-azul.gif", "~/UnderConstruction.aspx"));
        //    //Menu1.Items[1].ChildItems.Add(new MenuItem(" Advance Query Monitor Hour", "AdvanceQueryMonitorHour", "./Pag/imas/int/f-azul.gif", "~/UnderConstruction.aspx"));
        //    Menu1.Items.Add(new MenuItem("LCR", "LCR"));
        //    Menu1.Items[2].ChildItems.Add(new MenuItem(" LCR Explorer", "LCRExplorer", "./Pag/imas/int/f-azul.gif", "~/lcr.aspx"));
        //    //Menu1.Items[2].ChildItems.Add(new MenuItem(" LCR Explorer", "LCRExplorer", "./Pag/imas/int/f-azul.gif", "~/UnderConstruction.aspx"));

        //    HyperLink1.DataBind();
        //    HyperLink1.NavigateUrl = "~/HomeCM.aspx";

        //    Session["hometype"] = "";
        //}
    }

}