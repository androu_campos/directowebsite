using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterIphone : System.Web.UI.MasterPage
{
    void Page_PreInit(object sender, EventArgs e)
    {
        Master.EnableTheming = true;
        Page.EnableTheming = true;
        Page.Theme = "Theme1";
        Page.StyleSheetTheme = "Theme1";

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {


            MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adpBR = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
            MyDataSetTableAdapters.Billing_StatusDataTable tblBR = adpBR.GetData();
            if (tblBR.Rows.Count > 0)//Billing running
            {
                lblBillingRunning.Visible = true;

            }
            else
            {
                lblBillingRunning.Visible = false;
            }

            if (Session["Rol"].ToString() != string.Empty && Session["Rol"].ToString() != "Customer")//Usuario Logeado            
            {


                if (Request.Cookies["Rol"] != null)
                {
                    Session["Rol"] = Request.Cookies["Rol"].Value.ToString();
                    Session["DirectoUser"] = Request.Cookies["userMecca"].Value.ToString();
                }


                lblUserName.Text = Session["DirectoUser"].ToString();
                string urlrequested = Request.RawUrl.ToString();
                int urlLength = urlrequested.Length;
                int index = urlrequested.LastIndexOf("/");
                int sufix = urlrequested.LastIndexOf("aspx");
                string url = urlrequested.Substring(index, sufix - index + 4);
                url = "~" + url;


                MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpS = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                MyDataSetTableAdapters.MainNodesDataTable tblS = adpS.GetDataByUrlnRol(url, Session["Rol"].ToString());

                if (tblS.Count == 0 && Session["Rol"].ToString() != "Admins")//Verifica si el usuario tiene acceso a esta pagina
                {
                    Response.Redirect("ContactUs.aspx", false);
                }
                else
                {
                    MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adp = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
                    MyDataSetTableAdapters.Billing_StatusDataTable tbl = adp.GetData();

                    if (!Page.IsPostBack)//Construye el Treeview
                    {
                        //obtiene el rol del usuario
                        string rol = Session["Rol"].ToString();
                        //Make tree new                 
                        //query qe regresa los nodos principales
                        MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter adpN = new MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter();
                        MyDataSetTableAdapters.Roles_Nodes1DataTable tblN = adpN.GetDataByRole(rol, rol);
                        int jN = tblN.Count;
                        string[] selected = new string[jN];
                        for (int i = 0; i < jN; i++)
                        {
                            string rolName = tblN.Rows[i]["NodeName"].ToString();
                            //Nodos primarios   nivel 1                        
                            Menu1.Items.Add(new MenuItem(rolName, i.ToString()));
                            int idNodeFhater = Convert.ToInt32(tblN.Rows[i]["Id"]);
                            Menu1.Items[i].Selectable = false;

                            //Llenar los nodos secundarios  nivel 2
                            MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpBN = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                            MyDataSetTableAdapters.MainNodesDataTable tblBN = adpBN.GetDataByFatherId(idNodeFhater);
                            int rbN = tblBN.Rows.Count;
                            for (int n = 0; n < rbN; n++)
                            {
                                string nameNode = tblBN.Rows[n]["Name"].ToString();
                                string urlPath = tblBN.Rows[n]["Url"].ToString();

                                if (i == 0 && n == 6 && Session["DirectoUser"].ToString() != "yurema" && Session["Rol"].ToString() == "Comercial" && Session["DirectoUser"].ToString() != "daniel" && Session["DirectoUser"].ToString() != "iliana")
                                {

                                }
                                else
                                {
                                    Menu1.Items[i].ChildItems.Add(new MenuItem(nameNode, nameNode, "./Pag/imas/int/f-azul.gif", urlPath));
                                }

                                if (i == 0 && n == 11 && Session["DirectoUser"].ToString() != "daniel" && Session["DirectoUser"].ToString() != "yurema" && Session["DirectoUser"].ToString() != "iliana" && Session["Rol"].ToString() == "Comercial")
                                {
                                    Menu1.Items[i].ChildItems.RemoveAt(9);
                                }


                            }



                        }
                        if (Session["DirectoUser"].ToString() == "huguette")
                        {
                            Menu1.Items.Add(new MenuItem("Today", "TodayTraffic"));
                            Menu1.Items[4].ChildItems.Add(new MenuItem(" Today's customer traffic", "TodayCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer"));
                            Menu1.Items[4].ChildItems.Add(new MenuItem(" Today's vendor traffic", "TodayVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Vendor"));

                            Menu1.Items.Add(new MenuItem("Recent", "RecenTraffic"));
                            Menu1.Items[5].ChildItems.Add(new MenuItem(" Recent customer traffic", "RecentCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer&Table=OpSheetInc"));
                            Menu1.Items[5].ChildItems.Add(new MenuItem(" Recent vendor traffic", "RecentVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Vendor&Table=OpSheetInc"));

                            Menu1.Items.Add(new MenuItem("Yesterday", "YesterTraffic"));
                            Menu1.Items[6].ChildItems.Add(new MenuItem(" Yesterday's customer traffic", "YesterdayCustomer", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer&Table=OpSheetY"));
                            Menu1.Items[6].ChildItems.Add(new MenuItem(" Yesterday's vendor traffic", "YesterdayVendor", "./Pag/imas/int/f-azul.gif", "~/RTraffic.aspx?Type=Customer&Table=OpSheetY"));
                        }
                        //end Make                    

                        if (Session["DirectoUser"].ToString() == "david" || Session["DirectoUser"].ToString() == "huguette" || Session["DirectoUser"].ToString() == "felipe" || Session["DirectoUser"].ToString() == "santiago" || Session["DirectoUser"].ToString() == "vicente" || Session["DirectoUser"].ToString() == "hagi" || Session["DirectoUser"].ToString() == "rmanja")
                        {
                            Menu1.Items[3].ChildItems.Add(new MenuItem(" Live Calls graph", "Live Calls graph", "./Pag/imas/int/f-azul.gif", "~/TestGraph.aspx"));
                            Menu1.Items[0].ChildItems.Add(new MenuItem(" Live Traffic Watch", "Live Traffic Watch", "./Pag/imas/int/f-azul.gif", "~/Live.aspx"));
                        }


                    }
                }

            }
            else
            {

                Response.Redirect("SignIn.aspx", false);
            }



        }


        catch (Exception ex)
        {
            string errormessage = ex.Message.ToString();

        }


    }

    protected void lnkCloseSesion_Click(object sender, EventArgs e)
    {

        if (Session["RememberMe"].ToString() == "ON" || Request.Cookies["userMecca"] != null)
        {//clear cookies
            Response.Cookies["userMecca"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["passMecca"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["link"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["Rol"].Expires = DateTime.Now.AddDays(-1);
            Session["RememberMe"] = string.Empty;
            Response.Redirect("~/SignIn.aspx");
        }
        else

            Session["Rol"] = string.Empty;
        Session["DirectoUser"] = string.Empty;
        Response.Redirect("~/SignIn.aspx");
    }
}
