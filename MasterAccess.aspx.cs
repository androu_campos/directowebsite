using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterAccess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (!Page.IsPostBack)
            {
                txtFrom.Text = System.DateTime.Now.AddDays(-1).ToShortDateString();
                txtTo.Text = System.DateTime.Now.AddDays(-1).ToShortDateString();
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();

        }
    }
    protected void dpdCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        string Coun = dpdCountry.SelectedValue.ToString();
        
        dpdRegion.Enabled = true;
        SqldsRegion.SelectCommand = "SELECT  '**ALL**' as Region UNION SELECT DISTINCT Region FROM CDRDB..Regions WHERE Country like '" + Coun + "' ORDER BY Region";
    }

    protected void cmdReport_Click(object sender, EventArgs e)
    {

        try
        {
            //build query
            string to, from, country, region, simm, gateway = string.Empty, WhereString = string.Empty, VBroker, FromSBro = string.Empty, SelectString = string.Empty, GroupByString = string.Empty, UnionString = string.Empty, sSUM = string.Empty, FromString;
            to = txtFrom.Text;
            from = txtFrom.Text;
            bool bVB = false;

            if (dpdCountry.SelectedItem.ToString() == "** ALL **") country = "%";
            else country = dpdCountry.SelectedItem.ToString();
            if (dpdRegion.SelectedItem.ToString() == "** ALL **") region = "%";
            else region = dpdRegion.SelectedItem.ToString();
            if (dpdSIM.SelectedItem.ToString() == "** ALL **") simm = "%";
            else simm = dpdSIM.SelectedItem.ToString();
            if (dpdVBroker.SelectedItem.ToString() == "** ALL **") VBroker = "%";
            else VBroker = dpdVBroker.SelectedItem.ToString();
            if (dplGateway.SelectedItem.ToString() == "** ALL **") gateway = "%";
            else gateway = dplGateway.SelectedItem.ToString();

            FromString = " FROM QTrafficLog T ";
            //WHERE Builder

            WhereString = WhereString + "'" + from + "' AND '" + to + "'";
            if (country != "%")
            {
                WhereString = WhereString + " AND T.Country LIKE '" + country + "'";
            }
            if (region != "%")
            {
                WhereString = WhereString + " AND T.region LIKE '" + region + "'";
            }
            if (simm != "%")
            {
                WhereString = WhereString + " AND T.SIM LIKE '" + simm + "'";
            }
            if (gateway != "%")
            {
                WhereString = WhereString + " AND T.Gateway LIKE '" + gateway + "'";
            }
             if (VBroker != "%")
             {
                 WhereString = WhereString + " AND B.Broker LIKE '" + VBroker + "'";
                 FromSBro = FromSBro + " LEFT JOIN SIMInfo S ON T.SIM = S.SIM";
                 FromSBro = FromSBro + " LEFT JOIN Broker B ON S.IDBroker = B.IDBroker";
                 bVB = true;
             }
            


            //WhereString = WhereString + BrokerWhere;
            //FIN WHERE Builder     

             if (ckboxl.Items[0].Selected == true)//GROUP BY DATE
             {
                 SelectString = SelectString + ",CONVERT(VARCHAR(11) ,T.Billingdate,101) as BillingDate";
                 GroupByString = GroupByString + ",T.BillingDate";
                 UnionString = UnionString + ",'** ALL **' AS BillingDate";
             }
             if (ckboxl.Items[1].Selected == true)
             {
                 SelectString = SelectString + ",T.Gateway";
                 GroupByString = GroupByString + ",T.Gateway";
                 UnionString = UnionString + ",'****' AS Gateway";
             }
             if (ckboxl.Items[2].Selected == true)
             {
                 SelectString = SelectString + ",T.SIM";
                 GroupByString = GroupByString + ",T.SIM";
                 UnionString = UnionString + ",'****' AS SIM";
             }
             
             if (ckboxl.Items[3].Selected == true)
             {
                 SelectString = SelectString + ",T.Country";
                 GroupByString = GroupByString + ",T.Country";
                 UnionString = UnionString + ",'****' as Country";
             }
             if (ckboxl.Items[4].Selected == true)
             {
                 SelectString = SelectString + ",T.Region";
                 GroupByString = GroupByString + ",T.Region";
                 UnionString = UnionString + ",'****' as Region";
             }
             if (ckboxl.Items[5].Selected == true)
             {
                 SelectString = SelectString + ",T.Type";
                 GroupByString = GroupByString + ",T.Type";
                 UnionString = UnionString + ",'****' as [Type]";
             }
             if (ckboxl.Items[6].Selected == true)
             {
                 SelectString = SelectString + ",T.Class";
                 GroupByString = GroupByString + ",T.Class";
                 UnionString = UnionString + ",'****' as [Class]";
             }
             if (ckboxl.Items[7].Selected == true)
             {
                 SelectString = SelectString + ",T.LMC";
                 GroupByString = GroupByString + ",T.LMC";
                 UnionString = UnionString + ",'****' as [LMC]";
             }
             if (ckboxl.Items[8].Selected == true)
             {
                 SelectString = SelectString + ",T.RegionPrefix";
                 GroupByString = GroupByString + ",T.RegionPrefix";
                 UnionString = UnionString + ",'****' as RegionPrefix";
             }
             if (ckboxl.Items[9].Selected == true)
             {
                 SelectString = SelectString + ",T.CustPrefix";
                 GroupByString = GroupByString + ",T.CustPrefix";
                 UnionString = UnionString + ",'****'";
             }             
             
             if (ckboxl.Items[10].Selected == true)
             {
                 SelectString = SelectString + ",T.Cost";
                 GroupByString = GroupByString + ",T.Cost";
                 UnionString = UnionString + ",NULL as Cost";
             }
             if (ckboxl.Items[11].Selected == true)
             {
                 SelectString = SelectString + ",T.Price";
                 GroupByString = GroupByString + ",T.Price";
                 UnionString = UnionString + ",NULL AS Price";
             }
             //Arriba con group
             //Sin group abajo
             if (ckboxl.Items[12].Selected == true)
             {
                 sSUM = sSUM + ",SUM(T.Attempts) as Attempts";
             }
             if (ckboxl.Items[13].Selected == true)
             {
                 sSUM = sSUM + ",SUM(T.BillableCalls) as Calls";
             }
             if (ckboxl.Items[14].Selected == true)
             {
                 sSUM = sSUM + ",ROUND(SUM(T.BillMinutes),4) as Minutes";
             }
             if (ckboxl.Items[15].Selected == true)
             {
                 sSUM = sSUM + ",ROUND(SUM(T.Cost*T.BillMinutes),4) as TotalCost";
             }
             if (ckboxl.Items[16].Selected == true)
             {
                 sSUM = sSUM + ",ROUND(SUM(T.Price*T.BillMinutes),4) as TotalSales";
             }
             if (ckboxl.Items[17].Selected == true)
             {
                 sSUM = sSUM + ",(CASE WHEN SUM(T.Attempts) = 0 THEN 0 ELSE ROUND(CAST (SUM(T.BillableCalls)AS FLOAT) /SUM(T.Attempts)*100,2) END)AS ASR";
             }
             if (ckboxl.Items[18].Selected == true)
             {
                 sSUM = sSUM + ",(CASE WHEN SUM(T.BillableCalls) = 0 THEN 0 ELSE ROUND(SUM(T.BillMinutes)/SUM(T.BillableCalls),2)END) as ACD";
             }
             if (ckboxl.Items[19].Selected == true)
             {
                 sSUM = sSUM + ",ROUND(SUM(T.Price*T.BillMinutes)-SUM(T.Cost*T.BillMinutes),4) as Profit";
             }
             if (ckboxl.Items[20].Selected == true)
             {
                 SelectString = SelectString + ",B.Broker";
                 GroupByString = GroupByString + ",B.Broker";
                 UnionString = UnionString + ",'****' as Broker";
                 if (bVB == false)
                 {
                     FromSBro = FromSBro + " LEFT JOIN SIMInfo S ON T.SIM = S.SIM";
                     FromSBro = FromSBro + " LEFT JOIN Broker B ON S.IDBroker = B.IDBroker";
                 }

             }

             string sOrder, sS,sG,sF,sW,sSt;
             sS = "";
             sOrder = "";
             if (SelectString.Length > 3)
             {
                 sS = SelectString.Substring(1);// quita la coma del principio
                 sOrder = " ORDER BY " + sS.Replace("CONVERT(VARCHAR(11) ,T.Billingdate,101) as", " ");
             }
             else
             {
                 if (sSUM.Length > 3)
                 {
                     sSUM = sSUM.Substring(1);
                 }
             }

             string QQ,sHaving = " HAVING SUM(T.BillMinutes) > 0 ";
             sG = string.Empty;

             sF = FromString + FromSBro;
             sW = WhereString;
             if (GroupByString.Length > 3)
             {
                 sG = " GROUP BY " + GroupByString.Substring(1);
             }
             sSt = "";
             if (UnionString.Length > 1)
             {
                 sSt = UnionString.Substring(1);
             }

             
             QQ = "sS=" + sS + "&sF=" + sF + "&sW=" + sW + "&sG=" + sG + "&sSt=" + sSt + "&sSUM=" + sSUM;

             string Query = " SELECT " + sS + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT " + sSt + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + sHaving + sOrder;

             Session["MasterAccess"] = Query;

             if ((sS.Length + sSUM.Length) > 6)
             {
                 Response.Redirect("~/MasterResults.aspx", false);
             }
        
             

        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect("MasterAccess.aspx");
    }
}
