using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;

public partial class GraphGridV : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        String query = null;
        DataTable tbl1;

        if (!IsPostBack)
        {
            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in (select ip from cdrdb.dbo.providerip where providername like 'TLK%' AND TYPE = 'V' UNION SELECT 'DGTK' AS IP ) ";
            query += "group by Time ORDER BY Time ";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart1(tbl1, "TLK ALL");

            tbl1.Dispose();
        }
        Chart1.Visible = true;
        Chart2.Visible = false;
        Chart3.Visible = false;
        Chart4.Visible = false;
        Chart5.Visible = false;
        Chart6.Visible = false;
        Chart7.Visible = false;
        Chart8.Visible = false;
        Chart9.Visible = false;
        Chart10.Visible = false;
        Chart11.Visible = false;
        Chart12.Visible = false;

    }

    private void makeChart1(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {
                pdx = Chart1.Series["Ports ALL"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports ALL"].Points[pdx].MapAreaAttributes = script;
            }

            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports ALL"].Points.FindMaxValue("Y");

            string from;
            string to;

            from = DateTime.Now.AddDays(-1).ToShortDateString().ToString();
            to = DateTime.Now.AddDays(0).ToShortDateString().ToString();

            from = from + " 00:00:00 AM";
            to = to + " 23:59:59 PM";

            //this.Chart1.Series["MaxPorts"].Points.AddXY(Convert.ToDateTime(to), 3976);
            //this.Chart1.Series["MaxPorts"].Points.AddXY(Convert.ToDateTime(from), 3976);

            this.Chart1.Titles.Add(tITLE);
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;
            

            this.Chart1.Visible = true;

            lblGrid.Text = "Ports ALL";

        }
        else
        {
            this.Chart1.Visible = false;
            Session["tblDetail1"] = tbl;
            Session["tblDetail2"] = null;
            lblGrid.Visible = false;
        }

    }

    private void makeChart2(DataTable tbl1, DataTable tbl2, string tITLE)
    {
        if (tbl1.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;
            string script2 = string.Empty;
            
            this.Chart2.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl1.Rows)
            {
                pdx = Chart2.Series["Ports TLCL"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart2.Series["Ports TLCL"].Points[pdx].MapAreaAttributes = script;
            }

            foreach (DataRow myRow in tbl2.Rows)
            {
                pdx = Chart2.Series["Ports TMEX"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script2 = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script2 = script.Replace(" ", "");
                this.Chart2.Series["Ports TMEX"].Points[pdx].MapAreaAttributes = script2;
            }


            this.Chart2.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart2.Series["Ports TLCL"].Points.FindMaxValue("Y");
            DataPoint maxpointY2 = this.Chart2.Series["Ports TMEX"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart2.Titles.Add(tITLE);
            this.Chart2.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl1;
            Session["tblDetail2"] = tbl2;

            lblGrid.Text = "Ports TELCEL";
            lblGrid2.Text = "Ports TELMEX";
            this.Chart2.Visible = true;
        }
        else
        {
            Session["tblDetail1"] = tbl1;
            Session["tblDetail2"] = tbl2;
            lblGrid.Visible = false;
            lblGrid2.Visible = false;
        }

    }

    private void makeChart3(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart3.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;
                     
            foreach (DataRow myRow in tbl.Rows)
            {
                pdx = Chart3.Series["Ports TLC"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart3.Series["Ports TLC"].Points[pdx].MapAreaAttributes = script;
            }

            this.Chart3.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart3.Series["Ports TLC"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart3.Titles.Add(tITLE);
            this.Chart3.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart3.Visible = true;
            lblGrid.Text = "Ports TELCEL";
        }
        else
        {
            this.Chart3.Visible = false;
            Session["tblDetail1"] = tbl;
            lblGrid.Visible = false;
        }

    }

    private void makeChart4(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart4.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart4.Series["Ports TCDF"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart4.Series["Ports TCDF"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart4.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart4.Series["Ports TCDF"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart4.Titles.Add(tITLE);
            this.Chart4.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart4.Visible = true;
            lblGrid.Text = "Ports TELCEL DF";
        }
        else
        {
            this.Chart4.Visible = false;
            Session["tblDetail1"] = tbl;
            lblGrid.Visible = false;
        }

    }

    private void makeChart5(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart5.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart5.Series["Ports TCNL"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart5.Series["Ports TCNL"].Points[pdx].MapAreaAttributes = script;

            }

            this.Chart5.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart5.Series["Ports TCNL"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart5.Titles.Add(tITLE);

            string from;
            string to;

            from = DateTime.Now.AddDays(-1).ToShortDateString().ToString();
            to = DateTime.Now.AddDays(0).ToShortDateString().ToString();

            from = from + " 00:00:00 AM";
            to = to + " 23:59:59 PM";

            this.Chart5.Series["MaxPorts"].Points.AddXY(Convert.ToDateTime(to), 2728);
            this.Chart5.Series["MaxPorts"].Points.AddXY(Convert.ToDateTime(from), 2728);

            this.Chart5.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart5.Visible = true;

            lblGrid.Text = "Ports TELCEL NAL";
        }
        else
        {
            this.Chart5.Visible = false;
            Session["tblDetail1"] = tbl;
            lblGrid.Visible = false;
        }

    }

    private void makeChart6(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart6.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart6.Series["Ports TMX"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart6.Series["Ports TMX"].Points[pdx].MapAreaAttributes = script;
            }

            this.Chart6.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart6.Series["Ports TMX"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart6.Titles.Add(tITLE);
            this.Chart6.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart6.Visible = true;
            lblGrid.Text = "Ports TELMEX";
        }
        else
        {
            this.Chart6.Visible = false;
            Session["tblDetail1"] = tbl;
            lblGrid.Visible = false;
        }
    }

    private void makeChart7(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart7.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart7.Series["Ports DF"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart7.Series["Ports DF"].Points[pdx].MapAreaAttributes = script;
            }

            this.Chart7.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart7.Series["Ports DF"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart7.Titles.Add(tITLE);
            this.Chart7.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart7.Visible = true;
            lblGrid.Text = "Ports DF";
        }
        else
        {
            this.Chart7.Visible = false;
            Session["tblDetail1"] = tbl;
            lblGrid.Visible = false;
        }
    }

    private void makeChart8(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart8.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart8.Series["Ports TNAL"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart8.Series["Ports TNAL"].Points[pdx].MapAreaAttributes = script;
            }

            this.Chart8.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart8.Series["Ports TNAL"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart8.Titles.Add(tITLE);
            this.Chart8.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart8.Visible = true;
            lblGrid.Text = "Ports TELCEL NAL";

        }
        else
        {
            this.Chart8.Visible = false;
            Session["tblDetail1"] = tbl;
            lblGrid.Visible = false;
        }
    }

    private void makeChart9(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart9.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart9.Series["Ports TUNL"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart9.Series["Ports TUNL"].Points[pdx].MapAreaAttributes = script;
            }

            this.Chart9.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart9.Series["Ports TUNL"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart9.Titles.Add(tITLE);
            this.Chart9.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart9.Visible = true;

            lblGrid.Text = "Ports TELCEL NAL UNLI";
        }
        else
        {
            this.Chart9.Visible = false;
            Session["tblDetail1"] = tbl;

            lblGrid.Visible = false;
        }
    }

    private void makeChart10(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart9.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart10.Series["Ports DF"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart10.Series["Ports DF"].Points[pdx].MapAreaAttributes = script;
            }

            this.Chart10.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart10.Series["Ports DF"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart10.Titles.Add(tITLE);
            this.Chart10.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart10.Visible = true;

            lblGrid.Text = "Ports DF";
        }
        else
        {
            this.Chart10.Visible = false;
            Session["tblDetail1"] = tbl;
            lblGrid.Visible = false;
        }
    }

    private void makeChart11(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart11.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart11.Series["Ports NAL"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart11.Series["Ports NAL"].Points[pdx].MapAreaAttributes = script;
            }

            this.Chart11.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart11.Series["Ports NAL"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart11.Titles.Add(tITLE);
            this.Chart11.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart11.Visible = true;

            lblGrid.Text = "Ports NAL";
            gdvDetail2.Visible = false;
            gdvDetail2.DataSource = null;
            gdvDetail2.DataBind();
        }
        else
        {
            this.Chart11.Visible = false;
            Session["tblDetail1"] = tbl;
            lblGrid.Visible = false;
        }
    }

    private void makeChart12(DataTable tbl1, DataTable tbl2, string tITLE)
    {
        if (tbl1.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart12.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl1.Rows)
            {
                pdx = Chart12.Series["Ports DF"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart12.Series["Ports DF"].Points[pdx].MapAreaAttributes = script;
            }

            foreach (DataRow myRow in tbl2.Rows)
            {
                pdx = Chart12.Series["Ports NAL"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart12.Series["Ports NAL"].Points[pdx].MapAreaAttributes = script;
            }


            this.Chart12.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart12.Series["Ports DF"].Points.FindMaxValue("Y");
            DataPoint maxpointY2 = this.Chart12.Series["Ports NAL"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart12.Titles.Add(tITLE);
            this.Chart12.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl1;
            Session["tblDetail2"] = tbl2;

            this.Chart12.Visible = true;

            lblGrid.Text = "Ports DF";
            lblGrid2.Text = "Ports NAL";
        }
        else
        {
            this.Chart12.Visible = false;
            Session["tblDetail1"] = tbl1;
            Session["tblDetail2"] = tbl2;
            lblGrid.Visible = false;
            lblGrid2.Visible = false;
        }

    }

    protected void ddlRoute_SelectedIndexChanged(object sender, EventArgs e)
    {
        String query = null;
        DataTable tbl1;
        DataTable tbl2;

        if (ddlRoute.SelectedValue.ToString() == "ALL")
        {
            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in (select IP from cdrdb.dbo.providerip where providername like 'TLK%' AND TYPE = 'V') ";
            query += "group by Time ORDER BY Time ";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart1(tbl1, "TLK ALL");

            tbl1.Dispose();

            Chart1.Visible = true;
            Chart2.Visible = false;
            Chart3.Visible = false;
            Chart4.Visible = false;
            Chart5.Visible = false;
            Chart6.Visible = false;
            Chart7.Visible = false;
            Chart8.Visible = false;
            Chart9.Visible = false;
            Chart10.Visible = false;
            Chart11.Visible = false;
            Chart12.Visible = false;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
        else if (ddlRoute.SelectedValue.ToString() == "TCTT")
        {
            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TCDF','TCNL','TCDF','TROC','TCA2','TCNL','TOCA','TUNL') group by Time ORDER BY Time";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TMEX','TNAL','TMR2') group by Time ORDER BY Time";
            tbl2 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart2(tbl1, tbl2, "TLK-TLC-TMX");

            tbl1.Dispose();
            tbl2.Dispose();

            Chart1.Visible = false;
            Chart2.Visible = true;
            Chart3.Visible = false;
            Chart4.Visible = false;
            Chart5.Visible = false;
            Chart6.Visible = false;
            Chart7.Visible = false;
            Chart8.Visible = false;
            Chart9.Visible = false;
            Chart10.Visible = false;
            Chart11.Visible = false;
            Chart12.Visible = false;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
        else if (ddlRoute.SelectedValue.ToString() == "TCAL")
        {

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TCDF','TCNL','TCDF','TROC','TCA2','TCNL','TOCA','TUNL') group by Time ORDER BY Time";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart3(tbl1, "TLK-TLC");

            tbl1.Dispose();

            Chart1.Visible = false;
            Chart2.Visible = false;
            Chart3.Visible = true;
            Chart4.Visible = false;
            Chart5.Visible = false;
            Chart6.Visible = false;
            Chart7.Visible = false;
            Chart8.Visible = false;
            Chart9.Visible = false;
            Chart10.Visible = false;
            Chart11.Visible = false;
            Chart12.Visible = false;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
        else if (ddlRoute.SelectedValue.ToString() == "TCDF")
        {

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TCDF','TROC','TCA2') group by Time ORDER BY Time";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart4(tbl1, "TLK-TLC-DF");

            tbl1.Dispose();

            Chart1.Visible = false;
            Chart2.Visible = false;
            Chart3.Visible = false;
            Chart4.Visible = true;
            Chart5.Visible = false;
            Chart6.Visible = false;
            Chart7.Visible = false;
            Chart8.Visible = false;
            Chart9.Visible = false;
            Chart10.Visible = false;
            Chart11.Visible = false;
            Chart12.Visible = false;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
        else if (ddlRoute.SelectedValue.ToString() == "TCNL")
        {
            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TCNL','TOCA') group by Time ORDER BY Time";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart5(tbl1, "TLK-TLC-NAL");

            tbl1.Dispose();

            Chart1.Visible = false;
            Chart2.Visible = false;
            Chart3.Visible = false;
            Chart4.Visible = false;
            Chart5.Visible = true;
            Chart6.Visible = false;
            Chart7.Visible = false;
            Chart8.Visible = false;
            Chart9.Visible = false;
            Chart10.Visible = false;
            Chart11.Visible = false;
            Chart12.Visible = false;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
        else if (ddlRoute.SelectedValue.ToString() == "TMAL")
        {
            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TMEX','TNAL','TMR2') group by Time ORDER BY Time";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart6(tbl1, "TLK-TMX-ALL");

            tbl1.Dispose();

            Chart1.Visible = false;
            Chart2.Visible = false;
            Chart3.Visible = false;
            Chart4.Visible = false;
            Chart5.Visible = false;
            Chart6.Visible = true;
            Chart7.Visible = false;
            Chart8.Visible = false;
            Chart9.Visible = false;
            Chart10.Visible = false;
            Chart11.Visible = false;
            Chart12.Visible = false;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
        else if (ddlRoute.SelectedValue.ToString() == "TMEX")
        {

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TMEX') group by Time ORDER BY Time";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart7(tbl1, "TLK-TMX-DF");

            tbl1.Dispose();

            Chart1.Visible = false;
            Chart2.Visible = false;
            Chart3.Visible = false;
            Chart4.Visible = false;
            Chart5.Visible = false;
            Chart6.Visible = false;
            Chart7.Visible = true;
            Chart8.Visible = false;
            Chart9.Visible = false;
            Chart10.Visible = false;
            Chart11.Visible = false;
            Chart12.Visible = false;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
        else if (ddlRoute.SelectedValue.ToString() == "TNAL")
        {

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TNAL') group by Time ORDER BY Time";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart8(tbl1, "TLK-TMX-TNAL");

            tbl1.Dispose();

            Chart1.Visible = false;
            Chart2.Visible = false;
            Chart3.Visible = false;
            Chart4.Visible = false;
            Chart5.Visible = false;
            Chart6.Visible = false;
            Chart7.Visible = false;
            Chart8.Visible = true;
            Chart9.Visible = false;
            Chart10.Visible = false;
            Chart11.Visible = false;
            Chart12.Visible = false;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
        else if (ddlRoute.SelectedValue.ToString() == "TUNL")
        {

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TUNL') group by Time ORDER BY Time";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart9(tbl1, "TLK-TLC-NAL-UNLI");

            tbl1.Dispose();

            Chart1.Visible = false;
            Chart2.Visible = false;
            Chart3.Visible = false;
            Chart4.Visible = false;
            Chart5.Visible = false;
            Chart6.Visible = false;
            Chart7.Visible = false;
            Chart8.Visible = false;
            Chart9.Visible = true;
            Chart10.Visible = false;
            Chart11.Visible = false;
            Chart12.Visible = false;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
        else if (ddlRoute.SelectedValue.ToString() == "TADF")
        {

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TMEX','TCDF') group by Time ORDER BY Time";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart10(tbl1, "TLK-DF");

            tbl1.Dispose();

            Chart1.Visible = false;
            Chart2.Visible = false;
            Chart3.Visible = false;
            Chart4.Visible = false;
            Chart5.Visible = false;
            Chart6.Visible = false;
            Chart7.Visible = false;
            Chart8.Visible = false;
            Chart9.Visible = false;
            Chart10.Visible = true;
            Chart11.Visible = false;
            Chart12.Visible = false;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
        else if (ddlRoute.SelectedValue.ToString() == "TANL")
        {
            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TCNL','TNAL') group by Time ORDER BY Time";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart11(tbl1, "TLK-NAL");

            tbl1.Dispose();

            Chart1.Visible = false;
            Chart2.Visible = false;
            Chart3.Visible = false;
            Chart4.Visible = false;
            Chart5.Visible = false;
            Chart6.Visible = false;
            Chart7.Visible = false;
            Chart8.Visible = false;
            Chart9.Visible = false;
            Chart10.Visible = false;
            Chart11.Visible = true;
            Chart12.Visible = false;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
        else if (ddlRoute.SelectedValue.ToString() == "TCDN")
        {
            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TMEX','TCDF') group by Time ORDER BY Time";
            tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('TCNL','TNAL') group by Time ORDER BY Time";
            tbl2 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart12(tbl1, tbl2, "TLK-DF-NAL");

            tbl1.Dispose();
            tbl2.Dispose();

            Chart1.Visible = false;
            Chart2.Visible = false;
            Chart3.Visible = false;
            Chart4.Visible = false;
            Chart5.Visible = false;
            Chart6.Visible = false;
            Chart7.Visible = false;
            Chart8.Visible = false;
            Chart9.Visible = false;
            Chart10.Visible = false;
            Chart11.Visible = false;
            Chart12.Visible = true;

            cmdDetail.Visible = true;
            cmdHide.Visible = false;
            lblGrid.Visible = false;
            gdvDetail.Visible = false;
            lblGrid2.Visible = false;
            gdvDetail2.Visible = false;
        }
    }

    protected void cmdDetail_Click(object sender, EventArgs e)
    {
        
        if (lblGrid.Text == "Ports ALL")
        {
            gdvDetail.DataSource = ((DataTable)Session["tblDetail1"]);
            gdvDetail.DataBind();

            gdvDetail.Visible = true;
            lblGrid.Visible = true;
            lblGrid.Text = ddlRoute.SelectedItem.ToString();
            gdvDetail2.Visible = false;
            lblGrid2.Visible = false;
            cmdHide.Visible = true;
            cmdDetail.Visible = false;
        }
        else
        {
            gdvDetail.DataSource = ((DataTable)Session["tblDetail1"]);
            gdvDetail.DataBind();

            gdvDetail.Visible = true;
            lblGrid.Visible = true;

            gdvDetail2.DataSource = ((DataTable)Session["tblDetail2"]);
            gdvDetail2.DataBind();

            gdvDetail2.Visible = true;
            lblGrid2.Visible = true;

            cmdHide.Visible = true;
            cmdDetail.Visible = false;
            
        }
    }

    protected void cmdHide_Click(object sender, EventArgs e)
    {
        gdvDetail.Visible = false;
        lblGrid.Visible = false;
        gdvDetail2.Visible = false;
        lblGrid2.Visible = false;
        cmdDetail.Visible = true;
        cmdHide.Visible = false;

        Session["tblDetail1"] = null;
        Session["tblDetail2"] = null;

    }
}


