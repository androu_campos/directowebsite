using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class WidgetEntry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString.Count > 0)
            {
                txtCountry.Text = Request.QueryString["c"].ToString();
                txtRegion.Text = Request.QueryString["r"].ToString();
                txtAvg.Text = Request.QueryString["av"].ToString();
                txtACD.Text = Request.QueryString["ac"].ToString();
                Session["SelRegion"] = 0;
            }
        }
    }
    protected void cmdInsert_Click(object sender, EventArgs e)
    {
        string ctry = txtCountry.Text;
        string region = txtRegion.Text;
        string avg = (txtAvg.Text != " ") ? Convert.ToString(txtAvg.Text) : "";
        string asr = (txtASR.Text != string.Empty) ? Convert.ToString(txtASR.Text) : "";
        string acd = (txtACD.Text != "") ? Convert.ToString(txtACD.Text) : "";
        string availC = (txtAvailC.Text != "") ? Convert.ToString(txtAvailC.Text) : "";
        string serviceL = dpdServiceL.SelectedValue.ToString();
        string comments = txtComments.Text;
        string billIncr = DropDownList2.SelectedValue.ToString();
        string minimum = txtMinimum.Text;

        MECCA2TableAdapters.Push_ListAdp adp = new MECCA2TableAdapters.Push_ListAdp();
        adp.Insert(ctry,region,avg, asr, acd, availC, serviceL, comments, billIncr, DateTime.Now,minimum);
        lblUpdated.Visible = true;
        cmdInsert.Enabled = false;
    }
}
