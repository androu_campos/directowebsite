using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class ReportDiffMecca : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ddlRegion.Enabled = true;

            dsRegion.SelectCommand = "SELECT '%'  as Region UNION select distinct Region from MECCA2.dbo.trafficlog where (vendor ='NATIONAL' OR vendor='NATIONAL-TELULAR') AND VendorMinutes > 1  and billingdate BETWEEN '" + txtFromV.Text + "'  and '" + txtToV.Text + "' order by region";
            
            ddlRegion.DataSourceID = "dsRegion";
            ddlRegion.DataTextField = "Region";
            ddlRegion.DataValueField = "Region";
            btnSubmit.Enabled = true;
            txtFromV.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            txtToV.Text = DateTime.Now.AddDays(-1).ToShortDateString();        
        }
        else
        {
            
        }
    }

    protected void onClick(object sender, EventArgs e)
    {
        string url = "Data Source=172.27.27.30;Initial Catalog=MECCA2; User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlConnection connection = new SqlConnection(url);
        SqlDataSource ds = new SqlDataSource();
        SqlCommand sc = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet dset = new DataSet();


    }
    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        string strCnn = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlConnection cnn = new SqlConnection(strCnn);
        SqlDataSource dsDatos = new SqlDataSource();
        SqlCommand x = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet dsG1 = new DataSet();
        if (cbGroup.Checked == true) x.CommandText = "sp_ReportMpop";
        else x.CommandText = "sp_ReportMpop2";
        x.CommandType = CommandType.StoredProcedure;
        x.Connection = cnn;
        da.SelectCommand = x;

        SqlParameter pinf = new SqlParameter("@inf", SqlDbType.DateTime);
        pinf.Direction = ParameterDirection.Input;
        SqlParameter psup = new SqlParameter("@sup", SqlDbType.DateTime);
        psup.Direction = ParameterDirection.Input;
        SqlParameter preg = new SqlParameter("@Region", SqlDbType.VarChar);
        preg.Direction = ParameterDirection.Input;
        
        pinf.Value = txtFromV.Text;
        psup.Value = txtToV.Text;
        preg.Value = ddlRegion.SelectedValue.ToString();


        x.Parameters.Add(pinf);
        x.Parameters.Add(psup);
        x.Parameters.Add(preg);

        x.CommandTimeout = 600;
        da.Fill(dsG1);
        gvRePOP.DataSource = dsG1;
        Session["RMpop"] = dsG1;
        gvRePOP.DataBind();

        exportMpop.Visible = true;

        Session["MPOPSet"] = dsG1;
    }
    protected void txtToV_TextChanged(object sender, EventArgs e)
    {
       
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        ddlRegion.Enabled = true;
        dsRegion.SelectCommand = "SELECT '%'  as Region UNION select distinct Region from MECCA2.dbo.trafficlog where (vendor ='NATIONAL' OR vendor='NATIONAL-TELULAR') AND VendorMinutes > 1  and billingdate BETWEEN '" + txtFromV.Text + "'  and '" + txtToV.Text + "' order by region";
        btnSubmit.Enabled = true;       
    }

    protected void exportMpop_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["MPOPSet"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = ran + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
            
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

    }


    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void gvRePOP_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataSet myDtst = (DataSet)Session["MPOPSet"];
        gvRePOP.PageIndex = e.NewPageIndex;
        gvRePOP.DataSource = myDtst.Tables[0];
        gvRePOP.DataBind();
    }
}
