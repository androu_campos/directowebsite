<%@ Page Language="C#" StylesheetTheme="Theme1"  MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="NEO.aspx.cs" Inherits="NEO" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <asp:Panel ID="Panel2" runat="server" BackColor="Black" Height="100%" Width="100%">
   
    <asp:Panel ID="Panel1" runat="server" BackColor="Transparent" BorderColor="Transparent"
        Font-Bold="True" Font-Overline="False" ForeColor="White" GroupingText="Parameters"
        Height="100px" Width="811px" Direction="LeftToRight">
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp;<table>
            <tr>
                <td style="width: 128px">
                    Attempts &gt;</td>
                <td style="width: 127px">
                    <asp:TextBox ID="txtCallsMin" runat="server" Width="60px"></asp:TextBox></td>
                <td style="width: 75px">
                    Class</td>
                <td style="width: 100px">
                    <asp:DropDownList ID="ddlClass" runat="server">
                        <asp:ListItem Value="ALL">** ALL **</asp:ListItem>
                        <asp:ListItem Value="ON-NET">ON-NET</asp:ListItem>
                        <asp:ListItem>OFF-NET</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 62px">
                    Type</td>
                <td style="width: 100px">
                    <asp:DropDownList ID="ddlType" runat="server">
                        <asp:ListItem Selected="True" Value="ALL">** ALL **</asp:ListItem>
                        <asp:ListItem>CPP</asp:ListItem>
                        <asp:ListItem>FIXED</asp:ListItem>
                        <asp:ListItem>MPP</asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 100px">
                    <asp:Button ID="btnApply" runat="server" OnClick="btnApply_Click" Text="Apply Parameters" /></td>
            </tr>
            <tr>
                <td style="width: 128px">
                    Current Values</td>
                <td style="width: 127px">
                    <asp:Label ID="lbCall" runat="server"></asp:Label>&nbsp; Attempts</td>
                <td style="width: 75px">
                    Class</td>
                <td style="width: 100px">
                    <asp:Label ID="lbClass" runat="server"></asp:Label></td>
                <td style="width: 62px">
                    Type</td>
                <td style="width: 100px">
                    <asp:Label ID="lbType" runat="server"></asp:Label></td>
                <td style="width: 100px">
                    <asp:Button ID="btnShowGV"
            runat="server" OnClick="btnShowGV_Click" Text="Show Data Details" /></td>
            </tr>
        </table>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp;</asp:Panel>
    <table bgcolor="black">
        <tr>
            <td align="left" style="width: 100px" valign="top">
    <asp:TreeView ID="tvNEO" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="Large"
        ForeColor="LimeGreen" MaxDataBindDepth="10" OnTreeNodePopulate="tvNEO_TreeNodePopulate" EnableClientScript="False" NodeIndent="115" OnTreeNodeExpanded="tvNEO_TreeNodePopulate" PopulateNodesFromClient="False">
        <Nodes>
            <asp:TreeNode Expanded="False" PopulateOnDemand="True" SelectAction="SelectExpand"
                Text="Recent Traffic" Value="OpSheetInc"></asp:TreeNode>
            <asp:TreeNode Expanded="False" PopulateOnDemand="True" SelectAction="SelectExpand"
                Text="Today Traffic" Value="OpSheetAll"></asp:TreeNode>            
        </Nodes>
    </asp:TreeView>
            </td>
            <td align="center" style="width: 100px" valign="middle">
    <asp:GridView ID="gvNeo" runat="server" CellPadding="3" ForeColor="Black" GridLines="Vertical"
        Style="left: 256px; top: 466px" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" Visible="False">
        <FooterStyle BackColor="#CCCCCC" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="#CCCCCC" />
    </asp:GridView>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <br />
    &nbsp;<br />
    <br />


    <asp:Label ID="Label5" runat="server" CssClass="labelTurkH" Text="MECCA NEO"></asp:Label>



</asp:Content>

