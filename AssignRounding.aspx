<%@ Page Language="C#" MasterPageFile="~/Master.master" StylesheetTheme="Theme1"
    AutoEventWireup="true" CodeFile="AssignRounding.aspx.cs" Inherits="AssignRounding"
    Title="DIRECTO - Connections Worldwide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label5" runat="server" CssClass="labelTurkH" Text="MECCA Assign Rounding"></asp:Label>
    <table align="left" id="main" width="100%" style="position: absolute; top: 220px; left: 230px;">
        <tr>
            <td valign="top" align="left">
                <table align="left">
                    <tr>
                        <td align="center" bgcolor="#d5e3f0">
                            <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Step 1 "></asp:Label>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Choose Type of Provider"
                                CssClass="labelBlue8"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButtonList ID="rblProvider" runat="server" RepeatDirection="Horizontal"
                                AutoPostBack="True" OnSelectedIndexChanged="rblProvider_SelectedIndexChanged"
                                Width="300px" CssClass="labelSteelBlue">
                                <asp:ListItem Value="Customer" Selected="True">Customer</asp:ListItem>
                                <asp:ListItem Value="Vendor">Vendor</asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td bgcolor="#d5e3f0">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                            <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Step 2"></asp:Label>
                            &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <asp:Label ID="Label3" runat="server" Font-Bold="False" Text="Choose Provider" CssClass="labelBlue8"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlProvider" runat="server" AutoPostBack="True" DataSourceID="sqlDsProvider"
                                DataTextField="ProviderName" DataValueField="ProviderName" OnSelectedIndexChanged="ddlProvider_SelectedIndexChanged"
                                Width="300px" CssClass="dropdown">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td bgcolor="#d5e3f0">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                            <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Text="Step 3"></asp:Label>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
                            <asp:Label ID="Label2" runat="server" Font-Bold="False" Text="Type of Rounding" CssClass="labelBlue8"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButtonList ID="rblOptions" runat="server" AutoPostBack="True" CssClass="labelSteelBlue"
                                OnSelectedIndexChanged="rblOptions_SelectedIndexChanged" Enabled="False" Width="300px"
                                Font-Names="Arial" Font-Size="8pt">
                                <asp:ListItem Value="601" Text="MEX 60/60 - ROW 1/1"></asp:ListItem>
                                <asp:ListItem Value="1" Text="1/1 ALL"></asp:ListItem>
                                <asp:ListItem Value="6" Text="6/6 ALL"></asp:ListItem>
                                <asp:ListItem Value="306" Text="30/6 ALL"></asp:ListItem>
                                <asp:ListItem Value="60" Text="60/60 ALL"></asp:ListItem>
                                <asp:ListItem Value="0" Text="Personalized" Selected="True"></asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <table align="left">
                    <tr>
                        <td>
                            <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update Data"
                                Enabled="False" CssClass="boton" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                &nbsp;<table align="left">
                    <tr>
                        <td style="width: 100px">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center"
                    Font-Names="Arial" Font-Size="8pt">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Provider" SortExpression="Provider">
                            <ItemTemplate>
                                <asp:Label ID="LProvider" runat="server" Text='<%# Bind("Provider") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Country" SortExpression="Country">
                            <ItemTemplate>
                                <asp:Label ID="LCountry" runat="server" Text='<%# Bind("Country") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rounding" SortExpression="Rounding">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlRounding" runat="server" DataTextField="Rounding" SelectedValue='<%# Bind("Rounding") %>'
                                    Width="100px">
                                    <asp:ListItem Value="1" Text="1/1"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="6/6"></asp:ListItem>
                                    <asp:ListItem Value="306" Text="30/6"></asp:ListItem>
                                    <asp:ListItem Value="60" Text="60/60"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="No Assigned"></asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="1">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" /></td>
        </tr>
        <tr>
            <td align="center" colspan="1">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
        <td colspan="1">
    <asp:SqlDataSource ID="sqlDsProvider" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>" SelectCommand="SELECT DISTINCT ProviderName FROM CDRDB.dbo.ProviderIP UNION SELECT '**NULL** ' AS ProviderName FROM CDRDB.dbo.ProviderIP AS ProviderIP_1 ORDER BY ProviderName">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="salDsData" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>" SelectCommand="&#13;&#10; "
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlProvider" Name="ProviderName" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDsRound" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>" SelectCommand="SELECT '1' as Rounding&#13;&#10;UNION&#13;&#10;SELECT '6' as Rounding&#13;&#10;UNION&#13;&#10;SELECT '306' as Rounding&#13;&#10;UNION&#13;&#10;SELECT '60' as Rounding">
    </asp:SqlDataSource>
        
        </td>        
        </tr>
    </table>
</asp:Content>
