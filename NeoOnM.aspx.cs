using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class NeoOnM : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //PopulateOptions(NewNode);
        if (!Page.IsPostBack)
        {

            tvNEO.Nodes[0].Expanded = true;
            //Session["Load"] = "1";

            //Session["Load"] = "0";
            //string ll = tvNEO.Nodes[0].Value.ToString();
            tvNEO.Nodes[1].Expanded = true;
            //Session["Load"] = "1";

        }

    }

    protected void tvNEO_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        e.Node.ChildNodes.Clear();
        switch (e.Node.Depth)
        {
            case 0:
                PopulateOptions(e.Node);
                break;
            case 1:
                PopulateLevels(e.Node);
                break;
            case 2:
                PopulateOptions(e.Node);
                break;
            case 3:
                PopulateLevels(e.Node);
                break;
            case 4:
                PopulateOptions(e.Node);
                break;
            case 5:
                PopulateLevels(e.Node);
                break;
            case 6:
                PopulateOptions(e.Node);
                break;
            case 7:
                PopulateLevels(e.Node);
                break;
            case 8:
                PopulateOptions(e.Node);
                break;
            case 9:
                PopulateLevels(e.Node);
                break;
            case 10:
                PopulateOptions(e.Node);
                break;
        }

    }

    void PopulateLevels(TreeNode node)
    {

        SeparaPath(node.ValuePath);
        if (Session["Table"].ToString() == "") Session["Table"] = node.Value.ToString();

        //(node.Depth


        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;
        sqlSpQuery.CommandText = "sp_NeoGetVal";

        sqlSpQuery.Parameters.Add("@tab", SqlDbType.VarChar).Value = Session["Table"].ToString();
        sqlSpQuery.Parameters.Add("@op0", SqlDbType.VarChar).Value = Session["op0"].ToString();
        sqlSpQuery.Parameters.Add("@val0", SqlDbType.VarChar).Value = Session["val0"].ToString();
        sqlSpQuery.Parameters.Add("@op1", SqlDbType.VarChar).Value = Session["op1"].ToString();
        sqlSpQuery.Parameters.Add("@val1", SqlDbType.VarChar).Value = Session["val1"].ToString();
        sqlSpQuery.Parameters.Add("@op2", SqlDbType.VarChar).Value = Session["op2"].ToString();
        sqlSpQuery.Parameters.Add("@val2", SqlDbType.VarChar).Value = Session["val2"].ToString();
        sqlSpQuery.Parameters.Add("@op3", SqlDbType.VarChar).Value = Session["op3"].ToString();
        sqlSpQuery.Parameters.Add("@val3", SqlDbType.VarChar).Value = Session["val3"].ToString();
        sqlSpQuery.Parameters.Add("@ti", SqlDbType.VarChar).Value = "MEXON";
        sqlSpQuery.Parameters.Add("@depth", SqlDbType.Int).Value = node.Depth;


        DataSet resultSet;
        resultSet = RunQuery(sqlSpQuery);

        if (resultSet.Tables.Count > 0)
        {
            foreach (DataRow row in resultSet.Tables[0].Rows)
            {


                TreeNode NewNode = new
                    TreeNode(row["Options"].ToString(),
                    row["Options"].ToString());
                NewNode.Checked = false;
                NewNode.Expanded = false;
                NewNode.Selected = false;
                NewNode.PopulateOnDemand = true;
                NewNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                NewNode.ImageUrl = getImageLevel(Convert.ToDouble(row["Grade"].ToString()));
                node.ChildNodes.Add(NewNode);
            }
            if (resultSet.Tables[0].Rows.Count > 0)
            {
                gvNeo.DataSource = resultSet.Tables[0];
                gvNeo.DataBind();
            }
        }
    }
    void PopulateOptions(TreeNode node)
    {
        SeparaPath(node.ValuePath);
        if (Session["Table"].ToString() == "") Session["Table"] = node.Value.ToString();

        //(node.Depth


        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;
        sqlSpQuery.CommandText = "sp_NeoGetOptios";

        sqlSpQuery.Parameters.Add("@tab", SqlDbType.VarChar).Value = Session["Table"].ToString();
        sqlSpQuery.Parameters.Add("@op0", SqlDbType.VarChar).Value = Session["op0"].ToString();
        sqlSpQuery.Parameters.Add("@val0", SqlDbType.VarChar).Value = Session["val0"].ToString();
        sqlSpQuery.Parameters.Add("@op1", SqlDbType.VarChar).Value = Session["op1"].ToString();
        sqlSpQuery.Parameters.Add("@val1", SqlDbType.VarChar).Value = Session["val1"].ToString();
        sqlSpQuery.Parameters.Add("@op2", SqlDbType.VarChar).Value = Session["op2"].ToString();
        sqlSpQuery.Parameters.Add("@val2", SqlDbType.VarChar).Value = Session["val2"].ToString();
        sqlSpQuery.Parameters.Add("@op3", SqlDbType.VarChar).Value = Session["op3"].ToString();
        sqlSpQuery.Parameters.Add("@val3", SqlDbType.VarChar).Value = Session["val3"].ToString();
        sqlSpQuery.Parameters.Add("@ti", SqlDbType.VarChar).Value = "MEXON";
        sqlSpQuery.Parameters.Add("@depth", SqlDbType.Int).Value = node.Depth;


        DataSet resultSet;
        resultSet = RunQuery(sqlSpQuery);

        if (resultSet.Tables.Count > 0)
        {


            foreach (DataRow row in resultSet.Tables[0].Rows)
            {
                //rowOp = getOptionGV(row["NameOption"].ToString(), node.Depth);
                //tabOp.Rows.Add(rowOp.ItemArray);

                TreeNode NewNode = new
                    TreeNode(row["Options"].ToString(),
                    row["Options"].ToString());
                NewNode.Checked = false;
                NewNode.Expanded = false;
                NewNode.Selected = false;
                NewNode.PopulateOnDemand = true;
                NewNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                NewNode.ImageUrl = getImageLevel(Convert.ToDouble(row["Grade"].ToString()));
                node.ChildNodes.Add(NewNode);
            }
            if (resultSet.Tables[0].Rows.Count > 0)
            {
                gvNeo.DataSource = resultSet.Tables[0];
                gvNeo.DataBind();
            }
        }
    }
    string BuildWhere(int iNivel)
    {
        string Where = "";
        switch (iNivel)
        {
            case 1:
                Where = " WHERE ASR <> 0 ";
                break;
            case 3:
                Where = Where + " WHERE " + Session["option0"].ToString() + " LIKE '" + Session["Level1"].ToString() + "'" +
                     " AND ASR <> 0 ";
                break;
            case 5:
                Where = Where + " WHERE " + Session["option0"].ToString() + " LIKE '" + Session["Level1"].ToString() +
                     "' AND " + Session["option1"].ToString() + " LIKE '" + Session["Level2"].ToString() + "'" +
                     " AND ASR <> 0 ";
                break;
            case 7:
                Where = Where + " WHERE " + Session["option0"].ToString() + " LIKE '" + Session["Level1"].ToString() +
                    "' AND " + Session["option1"].ToString() + " LIKE '" + Session["Level2"].ToString() +
                    "' AND " + Session["option2"].ToString() + " LIKE '" + Session["Level3"].ToString() + "'" +
                    " AND ASR <> 0 ";
                break;
            case 9:
                Where = Where + " WHERE " + Session["option0"].ToString() + " LIKE '" + Session["Level1"].ToString() +
                    "' AND " + Session["option1"].ToString() + " LIKE '" + Session["Level2"].ToString() +
                    "' AND " + Session["option2"].ToString() + " LIKE '" + Session["Level3"].ToString() +
                    "' AND " + Session["option3"].ToString() + " LIKE '" + Session["Level4"].ToString() + "'" +
                    " AND ASR <> 0 ";
                break;
        }
        return Where;
    }
    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString =
            ConfigurationManager.ConnectionStrings
            ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
            //labelStatus.Text = "Unable to connect to SQL Server.";
        }
        return resultsDataSet;
    }
    void SeparaPath(string patthh)
    {


        Session["op0"] = "NA";
        Session["val0"] = "NA";
        Session["op1"] = "NA";
        Session["val1"] = "NA";
        Session["op2"] = "NA";
        Session["val2"] = "NA";
        Session["op3"] = "NA";
        Session["val3"] = "NA";
        Session["op4"] = "NA";
        Session["val4"] = "NA";

        int i = 0, indx;
        string[] niv = new string[11];
        string swap;
        while (patthh.Contains("/") == true)
        {
            indx = patthh.IndexOf("/");
            niv[i] = patthh.Substring(0, indx);
            swap = patthh.Substring(indx + 1);
            patthh = swap;
            i++;
        }
        niv[i] = patthh;
        if (niv[0] != null) Session["Table"] = niv[0];
        if (niv[1] != null) Session["op0"] = niv[1];
        if (niv[2] != null) Session["val0"] = niv[2];
        if (niv[3] != null) Session["op1"] = niv[3];
        if (niv[4] != null) Session["val1"] = niv[4];
        if (niv[5] != null) Session["op2"] = niv[5];
        if (niv[6] != null) Session["val2"] = niv[6];
        if (niv[7] != null) Session["op3"] = niv[7];
        if (niv[8] != null) Session["val3"] = niv[8];
        if (niv[9] != null) Session["op4"] = niv[9];
        if (niv[10] != null) Session["val4"] = niv[10];

    }
    string BuildsGrade(string Name)
    {
        //Jugar con la formula de Grade
        string grade;

        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;

        sqlSpQuery.CommandText = "sp_getMaxNeo";

        sqlSpQuery.Parameters.Add("@from", SqlDbType.VarChar).Value = "MECCA2..OpSheet" + Session["Table"].ToString();

        sqlSpQuery.Parameters.Add("@opti", SqlDbType.VarChar).Value = Name;

        DataSet MaxResultSet;
        MaxResultSet = RunQuery(sqlSpQuery);
        DataRow MaxRow = MaxResultSet.Tables[0].Rows[0];

        grade = ",ROUND(CAST(SUM(Calls) as Float)/" + MaxRow["CallsM"].ToString() + " * " + txtCalls.Text + " " +
                "+ SUM(Minutes)/" + MaxRow["MinM"].ToString() + " * " + txtMin.Text + " " +
                "+ ROUND(100*CAST(SUM(CompCalls) AS FLOAT)/SUM(Calls)*" + txtASR.Text + "/" + MaxRow["ASRM"].ToString() + " ,5) " +
                "+ ROUND(CAST(SUM(minutes) as FLOAT)/SUM(CompCalls)*" + txtACD.Text + "/" + MaxRow["ACDM"].ToString() + ",2),3) as Grade";

        return grade;
    }
    private DataRow getOptionGV(string Name, int iNivel)
    {

        string sSelectIn, sSelect, sWhere, sQuery, sFrom, sGrade;


        //Obtiene los MAXIMOS de la opcion Seleccionada--------------------
        sGrade = BuildsGrade(Name);
        //FIN Obtiene MAXIMOS*****************
        //******* FIN Grade
        //SELECT Interno
        sSelectIn = "SELECT " + Name + ", SUM(Calls) as Calls,SUM(CompCalls) as CompCalls,SUM(Minutes) as Minutes" + sGrade;
        //******** FIN SELECT Interno

        //SELECT EXTERNO
        sSelect = "SELECT '" + Name + "' as Options,SUM(Calls) as Calls,SUM(CompCalls) as CompCalls,SUM(Minutes) as Minutes,ROUND(100*CAST(SUM(CompCalls) AS FLOAT)/SUM(Calls),2) as ASR,ROUND(CAST(SUM(minutes) as FLOAT)/SUM(CompCalls),2) as ACD,ROUND(AVG(Grade),2) as Grade FROM (";
        //******** FIN SELECT Externo

        // FROM -----------
        sFrom = " FROM MECCA2..OpSheet" + Session["Table"].ToString();
        // *****FIN  FROM

        // WHERE -----------
        sWhere = BuildWhere(iNivel + 1);
        // *****FIN WHERE 

        //Build Query
        sQuery = sSelect + "SELECT TOP 10 * FROM (" + sSelectIn + sFrom + sWhere + "  GROUP BY " + Name + ") t   WHERE Grade > 0 ORDER BY Grade) f";

        SqlCommand sqlQuery = new SqlCommand();
        sqlQuery.CommandText = sQuery;
        DataSet ResultSett;
        ResultSett = RunQuery(sqlQuery);
        if (ResultSett.Tables.Count > 0)
        {
            return ResultSett.Tables[0].Rows[0];
        }
        else return null;

    }
    string getImageLevel(double ima)
    {
        string imagen = "";
        ima *= 10;
        int i;
        for (i = 1; i <= 10; i++)
        {
            if (ima > i - 1 & ima <= i) imagen = "~/NeoIma/d" + i.ToString() + ".bmp";
        }
        //if (imagen == "") imagen = "~/NeoIma/s1.gif";
        return imagen;
    }

    protected void btnShowGV_Click(object sender, EventArgs e)
    {
        if (gvNeo.Visible == true)
        {
            gvNeo.Visible = false;
            btnShowGV.Text = "Show Data Details";
        }
        else
        {
            gvNeo.Visible = true;
            btnShowGV.Text = "Hide Data Details";
        }
    }
   
}
