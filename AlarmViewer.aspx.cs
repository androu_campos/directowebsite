using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class AlarmViewer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Corre el sp 'sp_getNocAlarms' para obtener las alarmas
        Label2.Text = "Last Update: " +  DateTime.Now.ToString();
        if (!Page.IsPostBack)
        {
            int tabla = 0, i;

            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = CommandType.StoredProcedure;

            sqlSpQuery.CommandText = "sp_getNocAlarms";

            DataSet ResultSet;
            ResultSet = Util.RunQuery(sqlSpQuery);
            tabla = ResultSet.Tables.Count;
            creargrids();
            for (i = 0; i < tabla; i++)
            {
                bindgrids(i, ResultSet.Tables[i]);
            }

        }
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        int tabla = 0, i;

        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;

        sqlSpQuery.CommandText = "sp_getNocAlarms";

        DataSet ResultSet;
        ResultSet = RunQuery(sqlSpQuery);
        tabla = ResultSet.Tables.Count;
        creargrids();
        for (i = 0; i < tabla; i++)
        {
            bindgrids(i, ResultSet.Tables[i]);
        }
    }

    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString =
            ConfigurationManager.ConnectionStrings
            ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
            //labelStatus.Text = "Unable to connect to SQL Server.";
        }
        return resultsDataSet;
    }

    private void bindgrids(int i, DataTable tablita)
    {
        switch (i)
        {
            case 0:
                GridView0.DataSource = tablita;
                GridView0.DataBind();
                break;                      
        }
    }

    private void creargrids()
    {
        GridView0.DataSource = "";
    }

    protected void GridView0_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
