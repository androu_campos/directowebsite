using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class qviewer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {



        string Customer = Request.QueryString[0].ToString();
        string Vendor = Request.QueryString[1].ToString();
        string OriginationIP = Request.QueryString[2].ToString();
        string TerminationIP = Request.QueryString[3].ToString();
        string Country = Request.QueryString[4].ToString();
        string Region = Request.QueryString[5].ToString();
        string Type = Request.QueryString[7].ToString();
        string Class = Request.QueryString[8].ToString();
        int SourceMSW = Convert.ToInt32(Request.QueryString[9]);
        int ReportType = Convert.ToInt32(Request.QueryString[10]);
        string chk= Request.QueryString[6].ToString();

        int indx = 0;
        int flag = 0;
        string qry = "SELECT ";
        //  # = NINGUNO
        //  @ = TODOS

        string[] arreglo = new string[11];
        string[] columns = new string[11];

        if (ReportType == 0)
        { //hacer query sobre la tabla OpSheetInc

            for(int i=0; i<11; i++)//recorre todos los parametros
            {
                                
                string var = Request.QueryString[i].ToString();
                if (var != "**NONE**")//obtiene los parametros distintos de NONE
                {                                        
                    
                    arreglo[indx] = Request.QueryString[i].ToString();
                    columns[indx] = Request.QueryString.GetKey(i);
                    indx = indx + 1;
                                        
                }
                else
                {
                    flag = flag + 1;
                }

            }
                       
            if(flag==11)
            { //no se selecciono nigun valor, todos en **NONE**

               //mandar mensaje de error , "SELECCIONE UNA PARAMETRO PARA EL QUERY"

            }
            else
            {               
                
                for (int j = 0; j < indx; j++ )
                {
                    //forma la sentencia SQL 

                    if (j == indx - 1)
                    {
                        qry = qry + columns[j].ToString();
                    }
                    else
                    {
                        qry = qry + columns[j].ToString() + ",";
                    }


                }                        
                
                qry = qry + " FROM OpsheetInc WHERE ";
                
                for (int k = 0; k < indx; k++ )
                {
                    if (k == indx - 1)
                    {
                    qry = qry + columns[k].ToString() + "='" + arreglo[k].ToString() + "'";//forma la sentencia SQL
                    }
                    else
                    {                     
                     qry = qry + columns[k].ToString() + "='" +  arreglo[k].ToString() + "' and ";//forma la sentencia SQL
                    }
                                      
                 }
                 //la sentencia est� formada correctamente
                 SqlCommand sqlSpQuery = new SqlCommand();
                 sqlSpQuery.CommandType = System.Data.CommandType.Text;
                 sqlSpQuery.CommandText = qry;
                 DataSet ResultSet;
                 ResultSet =  RunQuery(sqlSpQuery);
                 gdvQviewer.DataSource = ResultSet.Tables[0];
                 gdvQviewer.DataBind();


            }

        }
        
        else
        {
            if (ReportType == 1)
            { //hacer query sobre la tabla OpSheetAll

            }
            else
                if (ReportType == 2)
                { //hacer query sobre la tabla OpSheetY

                }

        }


    }


    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
