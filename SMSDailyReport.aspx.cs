using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using System.IO;
using RKLib.ExportData;

public partial class SMSDailyReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Value = DateTime.Today.AddDays(-1).ToShortDateString();
            txtTo.Value = DateTime.Today.AddDays(-1).ToShortDateString();

            txtToV.Text = txtTo.Value.ToString();
            txtFromV.Text = txtFrom.Value.ToString();
            Session["tiempo"] = "0";

            getProviders();
            getCustomers();
        }        
        
    }
    protected void txtFromV_TextChanged(object sender, EventArgs e)
    {
        setDays(Convert.ToDateTime(txtFromV.Text));
    }
    protected void txtToV_TextChanged(object sender, EventArgs e)
    {
        setDays(Convert.ToDateTime(txtToV.Text));
    }
    protected void setDays(DateTime dia)
    {
        DayOfWeek da;

        if (Session["tiempo"].ToString() == "1")
        {
            dia = dia.AddDays(1 - dia.Day);
            txtFromV.Text = dia.ToShortDateString();

            dia = dia.AddMonths(1);
            dia = dia.AddDays(-dia.Day);
            txtToV.Text = dia.ToShortDateString();
        }
        else if (Session["tiempo"].ToString() == "2")
        {
            da = dia.DayOfWeek;
            if (da == DayOfWeek.Monday)
            {
                txtFromV.Text = dia.AddDays(0).ToShortDateString();
                txtToV.Text = dia.AddDays(6).ToShortDateString();
            }
            else if (da == DayOfWeek.Tuesday)
            {
                txtFromV.Text = dia.AddDays(-1).ToShortDateString();
                txtToV.Text = dia.AddDays(5).ToShortDateString();
            }
            else if (da == DayOfWeek.Wednesday)
            {
                txtFromV.Text = dia.AddDays(-2).ToShortDateString();
                txtToV.Text = dia.AddDays(4).ToShortDateString();
            }
            else if (da == DayOfWeek.Thursday)
            {
                txtFromV.Text = dia.AddDays(-3).ToShortDateString();
                txtToV.Text = dia.AddDays(3).ToShortDateString();
            }
            else if (da == DayOfWeek.Friday)
            {
                txtFromV.Text = dia.AddDays(-4).ToShortDateString();
                txtToV.Text = dia.AddDays(2).ToShortDateString();
            }
            else if (da == DayOfWeek.Saturday)
            {
                txtFromV.Text = dia.AddDays(-5).ToShortDateString();
                txtToV.Text = dia.AddDays(1).ToShortDateString();
            }
            else if (da == DayOfWeek.Sunday)
            {
                txtFromV.Text = dia.AddDays(-6).ToShortDateString();
                txtToV.Text = dia.AddDays(0).ToShortDateString();
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        getDailyReport();
        btnExport.Visible = true;
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        DataTable dtEmployee = ((DataSet)Session["ResultSetSMSDailyReport"]).Tables[0].Copy();
        // Export all the details to CSV
        RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
        string filename = "SMSDailyReport" + ".xls";
        objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
    }

    protected void getDailyReport()
    {
        
        string postData = "";
        string year = "";
        string month = "";
        string day = "";
        int aux = 0;

        string from = txtFromV.Text;
        string to = txtToV.Text;
        
        month = from.Substring(0, from.IndexOf("/"));
        day = from.Substring(from.IndexOf("/") + 1, from.LastIndexOf("/")-from.IndexOf("/")-1);
        year = from.Substring(from.LastIndexOf("/")+1,4);

        from = year + "-" + month + "-" + day;

        month = to.Substring(0, to.IndexOf("/"));
        day = to.Substring(to.IndexOf("/") + 1, to.LastIndexOf("/") - to.IndexOf("/") - 1);
        year = to.Substring(to.LastIndexOf("/") + 1, 4);
        to = year + "-" + month + "-" + day;

        postData = HttpUtility.UrlEncode("fecha_inicio") + "="
                + HttpUtility.UrlEncode(from) + "&"
                + HttpUtility.UrlEncode("fecha_fin") + "="
                + HttpUtility.UrlEncode(to);

        if (ddlCustomer.SelectedValue != "** NONE **" && ddlCustomer.SelectedValue != "** ALL **")
        {
            postData += "&" + HttpUtility.UrlEncode("client") + "="
                     + HttpUtility.UrlEncode(ddlCustomer.SelectedValue);
        }

        if (ddlVendor.SelectedValue != "** NONE **" && ddlVendor.SelectedValue != "** ALL **")
        {
            postData += "&" + HttpUtility.UrlEncode("provider") + "="
                     + HttpUtility.UrlEncode(ddlVendor.SelectedValue);
        }

        byte[] data = Encoding.ASCII.GetBytes(postData);


        HttpWebRequest request = WebRequest.Create("http://sms.directo.com:1366/globalResume") as HttpWebRequest;
        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";
        request.ContentLength = data.Length;

        Stream requestStream = request.GetRequestStream();
        requestStream.Write(data, 0, data.Length);
        requestStream.Close();

        HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        //string xml = response.GetResponseStream();

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(response.GetResponseStream());

        //using (DataSet ds = new DataSet())
        //{
        //    ds.ReadXml(xmlReader);
        //    xmlReader.Close();
        //    gdvCustomers.DataSource = ds;
        //    gdvCustomers.DataBind();
        //}

        XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/tabla/fila/columna");

        DataTable table1 = new DataTable("Records");
        table1.Columns.Add("BillDate");
        table1.Columns.Add("Customer");
        table1.Columns.Add("Provider");
        table1.Columns.Add("Operator");
        table1.Columns.Add("Sent").DataType = System.Type.GetType("System.Double");
        table1.Columns.Add("Delivered").DataType = System.Type.GetType("System.Double");
        table1.Columns.Add("Sales").DataType = System.Type.GetType("System.Double");
        table1.Columns.Add("Cost").DataType = System.Type.GetType("System.Double");   
        table1.Columns.Add("Profit").DataType = System.Type.GetType("System.Double");


        List<RowReport> globalResume = new List<RowReport>();

        foreach (XmlNode node in nodes)
        {

            table1.Rows.Add(node.SelectSingleNode("date").InnerText,
                            node.SelectSingleNode("customer").InnerText.ToUpper(),
                            node.SelectSingleNode("provider").InnerText,
                            node.SelectSingleNode("operator").InnerText,
                            node.SelectSingleNode("total_sms").InnerText,
                            node.SelectSingleNode("entregados_sms").InnerText,
                            node.SelectSingleNode("sales").InnerText,
                            node.SelectSingleNode("cost").InnerText,
                            node.SelectSingleNode("profit").InnerText);
        }

        DataSet set = new DataSet();
        set.Tables.Add(table1);

        gdvGlobalResume.DataSource = set.Tables[0];
        gdvGlobalResume.DataBind();
        gdvGlobalResume.Visible = true;

        Session["ResultSetSMSDailyReport"] = set;

    }

    protected void getProviders()
    {
        HttpWebRequest request = WebRequest.Create("http://sms.directo.com:1366/getProvidersXML") as HttpWebRequest;
        HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(response.GetResponseStream());

        XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/providers/provider");

        ddlVendor.Items.Add("** NONE **");
        ddlVendor.Items.Add("** ALL **");        

        foreach (XmlNode node in nodes)
        {
            ddlVendor.Items.Add(node.SelectSingleNode("name").InnerText);            
        }
    }

    protected void getCustomers()
    {
        HttpWebRequest request = WebRequest.Create("http://sms.directo.com:1366/getClientsXML") as HttpWebRequest;
        HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(response.GetResponseStream());

        XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/clients/client");

        ddlCustomer.Items.Add("** NONE **");
        ddlCustomer.Items.Add("** ALL **");

        foreach (XmlNode node in nodes)
        {
            ddlCustomer.Items.Add(node.SelectSingleNode("name").InnerText);
        }
    }

    //protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    gdvGlobalResume.PageIndex = e.NewPageIndex;
    //    gdvGlobalResume.DataBind();
    //}

    class RowReport
    {
        public string customer;
        public string provider;
        public string lmc;
        public string sent;
        public string deliveredSms;
        public string cost;
        public string sales;
        public string profit;
    }

}
