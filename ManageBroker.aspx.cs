using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class ManageSim : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //delete
            AccessDatasetTableAdapters.BrokerTableAdapter adp = new AccessDatasetTableAdapters.BrokerTableAdapter();
            AccessDataset.BrokerDataTable tbl = adp.GetData();
            //delete
            dpdBrokerDelete.Items.Clear();
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                dpdBrokerDelete.Items.Add(tbl.Rows[i]["Broker"].ToString());
            }
            //edit
            dpdBrokerName.Items.Clear();
            for (int j = 0; j < tbl.Rows.Count; j++)
            {
                dpdBrokerName.Items.Add(tbl.Rows[j]["Broker"].ToString());
            }
        }                    
    }
    
    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        AccessDatasetTableAdapters.BrokerTableAdapter adpa = new AccessDatasetTableAdapters.BrokerTableAdapter();
        AccessDataset.BrokerDataTable tbla = adpa.GetDataByBroker(dpdBrokerDelete.SelectedValue.ToString());

        int broker = Convert.ToInt32( tbla.Rows[0]["IDBroker"].ToString());
        AccessDatasetTableAdapters.SIMInfoTableAdp adp = new AccessDatasetTableAdapters.SIMInfoTableAdp();
        AccessDataset.SIMInfoDataTable tbl = adp.GetDataByBroker(broker);
        //
        AccessDatasetTableAdapters.BrokerTableAdapter adpB = new AccessDatasetTableAdapters.BrokerTableAdapter();
        AccessDataset.BrokerDataTable tblB = adpB.GetDataByBroker(dpdBrokerDelete.SelectedValue.ToString());


        if (tbl.Rows.Count > 0)
        { //        Tiene al menos un Sim asignado

            lblDelete.Text = "This Broker can't be deleted";
        }
        else
        { // Borrarlo

            adpB.Delete(Convert.ToInt32(tblB.Rows[0]["IDBroker"].ToString()));
            lblDelete.Text = "Broker deleted";                    
            
        }


    }

    protected void cmdCreate_Click(object sender, EventArgs e)
    {
        try
        {
            AccessDatasetTableAdapters.BrokerTableAdapter adp = new AccessDatasetTableAdapters.BrokerTableAdapter();
            adp.Insert(txtName.Text);
            lblInsert.Text = "Broker created succesfull";
            lblInsert.Visible = true;

            //delete
            AccessDatasetTableAdapters.BrokerTableAdapter adp1 = new AccessDatasetTableAdapters.BrokerTableAdapter();
            AccessDataset.BrokerDataTable tbl = adp1.GetData();
            //delete
            dpdBrokerDelete.Items.Clear();
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                dpdBrokerDelete.Items.Add(tbl.Rows[i]["Broker"].ToString());
            }
            //edit
            dpdBrokerName.Items.Clear();
            for (int j = 0; j < tbl.Rows.Count; j++)
            {
                dpdBrokerName.Items.Add(tbl.Rows[j]["Broker"].ToString());
            }
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            lblInsert.Text = "Error creating Broker";
            lblInsert.Visible = true;
        }
    }
    protected void cmdAssign_Click(object sender, EventArgs e)
    {
        try
        {
            AccessDatasetTableAdapters.PlanBreakOutAdp adpP = new AccessDatasetTableAdapters.PlanBreakOutAdp();
            AccessDataset.PlanBreakOutDataTable tblP = adpP.GetDataByName(dpdPlan.SelectedValue.ToString());
            int id = Convert.ToInt32(tblP.Rows[0]["IDPlan"].ToString());

            AccessDatasetTableAdapters.BrokerTableAdapter adpB = new AccessDatasetTableAdapters.BrokerTableAdapter();
            AccessDataset.BrokerDataTable tblB= adpB.GetDataByBroker(dpdBroker.SelectedValue.ToString());

            AccessDatasetTableAdapters.SIMInfoTableAdp adp = new AccessDatasetTableAdapters.SIMInfoTableAdp();
            adp.Insert(txtSim.Text,id,Convert.ToInt32(tblB.Rows[0]["IDBroker"].ToString()));


            lblAssign.Text = "SIM assigned succesfully";
            lblAssign.Visible = true;
            txtSim.Text = string.Empty;

        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            lblAssign.Text = "SIM can't be assigned";
            lblAssign.Visible = true;
        }

    }
    //edit
    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            AccessDatasetTableAdapters.BrokerTableAdapter adp = new AccessDatasetTableAdapters.BrokerTableAdapter();
            AccessDataset.BrokerDataTable tbl = adp.GetDataByBroker(dpdBrokerName.SelectedValue.ToString());
            int id = Convert.ToInt32(tbl.Rows[0]["IDBroker"].ToString());
            adp.Update(txtNewName.Text, id);
            lblUpdated.Text = "Broker Updated Succesfully";
            lblUpdated.Visible = true;
         //
            txtNewName.Text = string.Empty;


        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            lblUpdated.Text = "Error in Updated Broker";
            lblUpdated.Visible = true;
        }
    }





}
