using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class MasterAResults : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string qry = Session["MasterAccess"].ToString();

        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = System.Data.CommandType.Text;        
        sqlSpQuery.CommandText = qry;
        
        DataSet ResultSet;
        ResultSet = RunQuery(sqlSpQuery);
        Session["ResultSetMasterAcces"] = ResultSet;//for export to excel

        gdvMaster.DataSource = ResultSet.Tables[0];
        gdvMaster.DataBind();



    }
    protected void cmdExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {

            DataTable dtEmployee = ((DataSet)Session["ResultSetMasterAcces"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "MasterSalesLog" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["QUESCOMConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
