using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPh : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Session["DirectoUser"] as string))//Usuario Logeado            
            {
                if (Request.Cookies["Rol"] != null)
                {
                    Session["Rol"] = Request.Cookies["Rol"].Value.ToString();
                    Session["DirectoUser"] = Request.Cookies["userMecca"].Value.ToString();
                }

                lblUserName.Text = Session["DirectoUser"].ToString();
                string urlrequested = Request.RawUrl.ToString();
                int urlLength = urlrequested.Length;
                int index = urlrequested.LastIndexOf("/");
                int sufix = urlrequested.LastIndexOf("aspx");
                string url = urlrequested.Substring(index, sufix - index + 4);

                url = "~" + url;


                MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpS = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                MyDataSetTableAdapters.MainNodesDataTable tblS = adpS.GetDataByUrlnRol(url, Session["Rol"].ToString());

                if (tblS.Count == 0 && Session["Rol"].ToString() != "Admins")//Verifica si el usuario tiene acceso a esta pagina
                {
                    Response.Redirect("ContactUs.aspx", false);
                }
                else
                {
                    Util.RunQryAtCDRDB_void("INSERT INTO [MECCA2].[dbo].[UserSniper] VALUES ('" + Session["DirectoUser"].ToString() + "','" + Request.ServerVariables["REMOTE_ADDR"] + "','" + url + "',GETDATE())");
                    if (!Page.IsPostBack)//Construye el Treeview                    
                    {
                        //obtiene el rol del usuario
                        string rol = Session["Rol"].ToString();
                        //Make tree new                 
                        //query qe regresa los nodos principales
                        MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter adpN = new MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter();
                        MyDataSetTableAdapters.Roles_Nodes1DataTable tblN = adpN.GetDataByRole(rol, rol);

                        makeMenu(tblN);
                    }
                }
            }
            else
            {
                Response.Redirect("SignIn.aspx", false);
            }
        }
        catch (Exception ex)
        {
            string errormessage = ex.Message.ToString();

        }
    }

    protected void lnkCloseSesion_Click(object sender, EventArgs e)
    {

        if (Session["RememberMe"].ToString() == "ON" || Request.Cookies["userMecca"] != null)
        {//clear cookies
            Response.Cookies["userMecca"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["passMecca"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["link"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["Rol"].Expires = DateTime.Now.AddDays(-1);
            Util.RunQryAtCDRDB_void("UPDATE [CDRDB].[dbo].[login] SET [Active] = '0' WHERE username ='" + Session["DirectoUser"].ToString() + "'");
            Session["RememberMe"] = string.Empty;
            Session["DirectoUser"] = string.Empty;
            Response.Redirect("~/SignIn.aspx");
        }
        else
        {
            Util.RunQryAtCDRDB_void("UPDATE [CDRDB].[dbo].[login] SET [Active] = '0' WHERE username ='" + Session["DirectoUser"].ToString() + "'");
            Session["Rol"] = string.Empty;
            Session["DirectoUser"] = string.Empty;
            Response.Redirect("~/SignIn.aspx");
        }
    }

    private void makeMenu(DataTable tblN)
    {
        int jN = tblN.Rows.Count;
        string[] selected = new string[jN];
        for (int i = 0; i < jN; i++)
        {
            string rolName = tblN.Rows[i]["NodeName"].ToString();
            //Nodos primarios   nivel 1                        
            Menu1.Items.Add(new MenuItem(rolName, i.ToString()));
            int idNodeFhater = Convert.ToInt32(tblN.Rows[i]["Id"]);
            Menu1.Items[i].Selectable = false;

            //Llena los nodos secundarios  nivel 2
            MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpBN = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
            MyDataSetTableAdapters.MainNodesDataTable tblBN = adpBN.GetDataByFatherId(idNodeFhater);
            int rbN = tblBN.Rows.Count;
            for (int n = 0; n < rbN; n++)
            {
                string nameNode = tblBN.Rows[n]["Name"].ToString();
                string urlPath = tblBN.Rows[n]["Url"].ToString();

                if (i == 0 && n == 6 && Session["DirectoUser"].ToString() != "yvilla" && Session["Rol"].ToString() == "Comercial" && Session["DirectoUser"].ToString() != "jczapien" && Session["DirectoUser"].ToString() != "jmarrufoc" && Session["DirectoUser"].ToString() != "isalazar")
                {

                }
                else
                {
                    Menu1.Items[i].ChildItems.Add(new MenuItem(nameNode, nameNode, "./Pag/imas/int/f-azul.gif", urlPath));
                }

                if (i == 0 && n == 11 && Session["DirectoUser"].ToString() != "jczapien" && Session["DirectoUser"].ToString() != "jmarrufoc" && Session["DirectoUser"].ToString() != "yvilla" && Session["DirectoUser"].ToString() != "isalazar" && Session["Rol"].ToString() == "Comercial")
                {
                    Menu1.Items[i].ChildItems.RemoveAt(9);
                }

            }

        }
    }
}
