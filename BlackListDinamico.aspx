<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    Theme="Theme1" AutoEventWireup="true" CodeFile="BlackListDinamico.aspx.cs" Inherits="BlackList"
    Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td style="width: 71px" valign="top" align="left">
                <asp:Label ID="labelFrom" runat="server" Text="From:" Font-Bold="False" CssClass="labelBlue8" />
            </td>
            <td style="width: 100px" valign="top" align="left">
                <asp:TextBox ID="txtFromV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"
                    AutoPostBack="True" MaxLength="10"></asp:TextBox></td>
            <td style="width: 27px">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td>
        </tr>
        <tr>
            <td style="width: 71px;">
                <asp:Label ID="labelTo" runat="server" Text="To:" Font-Bold="False" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px; height: 24px;">
                <asp:TextBox ID="txtToV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"
                    AutoPostBack="True" MaxLength="10"></asp:TextBox></td>
            <td style="width: 27px;">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
        </tr>
        <tr>
            <td style="width: 71px;">
                <asp:Label ID="lblsrch" runat="server" CssClass="labelBlue8" Text="Number: "></asp:Label>
            </td>
            <td style="width: 100px; height: 24px;">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="labelSteelBlue" Width="80px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnsearch" runat="server" CssClass="boton" Text="Search" OnClick="btnsearch_Click" />
            </td>
            <td>
                <asp:Button ID="btnExpCSV" runat="server" CssClass="boton" Text="Export to CSV" OnClick="btnExpCSV_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCount" runat="server" CssClass="labelBlue8" Text="Total Records:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCount" runat="server" CssClass="labelSteelBlue" Enabled="false"
                    Width="90px" TextAlignment="Right"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblError" runat="server" CssClass="lblNotFound" Visible="false"></asp:Label>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="width: 103px" valign="top">
                <asp:GridView ID="gvGood" runat="server" Font-Names="Arial" Font-Size="8pt" DataSourceID="sqldsBlackStatus"
                    AutoGenerateColumns="False" AllowPaging="True" PageSize="50" EnableModelValidation="True"
                    Visible="true">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Number" HeaderText="Number" SortExpression="digits" ItemStyle-Wrap="true" />
                        <asp:BoundField DataField="date_start" HeaderText="Date Start" SortExpression="date_start"
                            ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="date_end" HeaderText="Date End" SortExpression="date_end"
                            ItemStyle-Wrap="false" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqldsBlackStatus" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListSantander %>"
                    ProviderName="<%$ ConnectionStrings:BlackListSantander.ProviderName %>" SelectCommand="SELECT right(digits,13) as Number, date_start, date_end FROM lcr where reliability = 0">
                </asp:SqlDataSource>
                <asp:GridView ID="gvSearch" runat="server" Font-Names="Arial" Font-Size="8pt" DataSourceID="sqldsBlackStatusSearch"
                    AutoGenerateColumns="False" AllowPaging="True" PageSize="50" EnableModelValidation="True"
                    Visible="False">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Number" HeaderText="Number" ReadOnly="True" SortExpression="Number"
                            ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="date_start" HeaderText="date_start" SortExpression="date_start"
                            ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="date_end" HeaderText="date_end" SortExpression="date_end"
                            ItemStyle-Wrap="false" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqldsBlackStatusSearch" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListSantander %>"
                    ProviderName="<%$ ConnectionStrings:BlackListSantander.ProviderName %>" SelectCommand="SELECT right(digits,13) as Number, date_start, date_end FROM lcr where reliability = 0"
                    FilterExpression="Number like '%{0}%'">
                    <FilterParameters>
                        <asp:ControlParameter ControlID="txtSearch" Name="Number" PropertyName="Text" />
                    </FilterParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sqldsBlackListCount" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListSantander %>"
                    ProviderName="<%$ ConnectionStrings:BlackListSantander.ProviderName %>" SelectCommand="SELECT count(digits) Count FROM lcr where reliability = 0">
                    <FilterParameters>
                        <asp:ControlParameter ControlID="txtSearch" Name="Number" PropertyName="Text" />
                    </FilterParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtFromV" runat="server"
        PopupButtonID="Image1" Format="dd/MM/yyyy">
    </cc1:CalendarExtender>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToV"
        PopupButtonID="Image2" Format="dd/M/yyyy">
    </cc1:CalendarExtender>
</asp:Content>
