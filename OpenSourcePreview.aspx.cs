using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;

public partial class OpenSourcePreview : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String query = null;
        DataTable tbl;

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor in ('OPEV') and [type] like 'V%'  group by Time ORDER BY Time";
        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartO(tbl, "OPENSOURCE");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor in ('OPEH') and [type] like 'V%'  group by Time ORDER BY Time";
        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartOL50(tbl, "OPENSOURCE-HACD");

        tbl.Dispose();


        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor in ('OPEO') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartPrepaid(tbl, "OPENSOURCE-ORANGE");
        

        //tbl.Dispose();       

        //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        //query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        //query += "[Live_Calls] > 0   AND Cust_Vendor IN ('OPIC', 'OLC2') and [type] like 'V%'  group by Time ORDER BY Time";

        //tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        //makeChartOLC2(tbl, "OUTLANDER-POSTPAID");

        //tbl.Dispose();

        //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        //query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        //query += "[Live_Calls] > 0   AND Cust_Vendor IN ('OICS','OPRE') and [type] like 'V%'  group by Time ORDER BY Time";

        //tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        //makeChartOPRE(tbl, "OUTLANDER-POSPREMIUM");

        Chart3.Visible = false;
        Chart4.Visible = false;


        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor in ('OPSP') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartOLMO(tbl, "OPENSOURCE-SPLIT");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor in ('WANV') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartOLMW(tbl, "WANSQUAD");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor in ('OLMS') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartOLMS(tbl, "OUTLANDER-MOVISTAR-MS");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor in ('OMIV') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartOMIV(tbl, "OUTLANDER-MOVISTAR-ICS");

        tbl.Dispose();



        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor IN ('OICS') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartPostPaidC(tbl, "OUTLANDER-NEW-NET");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor IN ('ONIC') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartONIC(tbl, "OUTLANDER-NEWNET-ICS");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor IN ('OFLW') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartOALP(tbl, "OUTLANDERNEWNET-OFLW");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor IN ('OAIC') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartOAIC(tbl, "OUTLANDER-ALPI-ICS");

        tbl.Dispose();

        //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        //query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        //query += "[Live_Calls] > 0   AND Cust_Vendor IN ('OLFD') and [type] like 'V%'  group by Time ORDER BY Time";

        //tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        //makeChartOLFD(tbl, "OUTLANDER-FIDELITY");

        Chart13.Visible = false;
    }
    private void makeChartO(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart1.Titles.Add(tITLE);
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetailO"] = tbl;

            this.Chart1.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            Session["tblDetailO"] = tbl;
        }

    }
    private void makeChartPostPaidC(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart7.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart7.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart7.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart7.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart7.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart7.Titles.Add(tITLE);
            this.Chart7.RenderType = RenderType.ImageTag;
            Session["tblDetailOCIC"] = tbl;

            this.Chart7.Visible = true;
        }
        else
        {
            this.Chart7.Visible = false;            
            Session["tblDetailOICS"] = tbl;
        }

    }
    private void makeChartOLC2(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart3.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart3.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart3.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart3.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart3.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart3.Titles.Add(tITLE);
            this.Chart3.RenderType = RenderType.ImageTag;
            Session["tblDetailOLC2"] = tbl;

            this.Chart3.Visible = true;
        }
        else
        {
            this.Chart3.Visible = false;
            Session["tblDetailOLC2"] = tbl;
        }

    }
    private void makeChartOPRE(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart4.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart4.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart4.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart4.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart4.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart4.Titles.Add(tITLE);
            this.Chart4.RenderType = RenderType.ImageTag;
            Session["tblDetailOLMS"] = tbl;

            this.Chart4.Visible = true;
        }
        else
        {
            this.Chart4.Visible = false;
            Session["tblDetailOLMS"] = tbl;
        }

    }
    private void makeChartOLMW(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart5.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart5.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart5.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart5.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart5.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart5.Titles.Add(tITLE);
            this.Chart5.RenderType = RenderType.ImageTag;
            Session["tblDetailPostPaidC"] = tbl;

            this.Chart5.Visible = true;
        }
        else
        {
            this.Chart5.Visible = false;
            Session["tblDetailPostPaidC"] = tbl;
        }

    }
    private void makeChartPrepaid(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart6.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart6.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart6.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart6.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart6.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart6.Titles.Add(tITLE);
            this.Chart6.RenderType = RenderType.ImageTag;
            Session["tblDetailPostPaidC"] = tbl;

            this.Chart6.Visible = true;
        }
        else
        {
            this.Chart6.Visible = false;
            Session["tblDetailPostPaidC"] = tbl;
        }
    }
    private void makeChartOLMO(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart2.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart2.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart2.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart2.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart2.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart2.Titles.Add(tITLE);
            this.Chart2.RenderType = RenderType.ImageTag;
            Session["tblDetailPostPaidC"] = tbl;

            this.Chart2.Visible = true;
        }
        else
        {
            this.Chart2.Visible = false;
            Session["tblDetailPostPaidC"] = tbl;
        }

    }
    private void makeChartONIC(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart8.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart8.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart8.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart8.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart8.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart8.Titles.Add(tITLE);
            this.Chart8.RenderType = RenderType.ImageTag;
            Session["tblDetailONIC"] = tbl;

            this.Chart8.Visible = true;
        }
        else
        {
            this.Chart8.Visible = false;
            Session["tblDetailONIC"] = tbl;
        }

    }
    private void makeChartOALP(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart9.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart9.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart9.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart9.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart9.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart9.Titles.Add(tITLE);
            this.Chart9.RenderType = RenderType.ImageTag;
            Session["tblDetailOAIC"] = tbl;

            this.Chart9.Visible = true;
        }
        else
        {
            this.Chart9.Visible = false;
            Session["tblDetailOAIC"] = tbl;
        }

    }
    private void makeChartOAIC(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart10.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart10.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart10.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart10.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart10.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart10.Titles.Add(tITLE);
            this.Chart10.RenderType = RenderType.ImageTag;
            Session["tblDetailOAIC"] = tbl;

            this.Chart10.Visible = true;
        }
        else
        {
            this.Chart10.Visible = false;
            Session["tblDetailOAIC"] = tbl;
        }

    }
    private void makeChartOMIV(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart11.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart11.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart11.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart11.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart11.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart11.Titles.Add(tITLE);
            this.Chart11.RenderType = RenderType.ImageTag;
            Session["tblDetailOAIC"] = tbl;

            this.Chart11.Visible = true;
        }
        else
        {
            this.Chart11.Visible = false;
            Session["tblDetailOMIV"] = tbl;
        }

    }
    private void makeChartOLMS(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart12.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart12.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart12.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart12.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart12.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart12.Titles.Add(tITLE);
            this.Chart12.RenderType = RenderType.ImageTag;
            Session["tblDetailOAIC"] = tbl;

            this.Chart12.Visible = true;
        }
        else
        {
            this.Chart12.Visible = false;
            Session["tblDetailOMIV"] = tbl;
        }

    }
    private void makeChartOLFD(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart13.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart13.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart13.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart13.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart13.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart13.Titles.Add(tITLE);
            this.Chart13.RenderType = RenderType.ImageTag;
            Session["tblDetailOLFD"] = tbl;

            this.Chart13.Visible = true;
        }
        else
        {
            this.Chart13.Visible = false;
            Session["tblDetailOLFD"] = tbl;
        }
    }

    private void makeChartOL50(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart14.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart14.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart14.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart14.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart14.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart14.Titles.Add(tITLE);
            this.Chart14.RenderType = RenderType.ImageTag;
            Session["tblDetailOL50"] = tbl;

            this.Chart14.Visible = true;
        }
        else
        {
            this.Chart14.Visible = false;
            Session["tblDetailOL50"] = tbl;
        }
    }


}
