<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LtwGrouped.aspx.cs" Inherits="LtwGrouped" StylesheetTheme="Theme1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Live Traffic Watch</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td with="150" align="center" valign="top">
                    <table width="100%" border=0>
                        <tr>
                            <td style="width: 100px" height="100%" align="center" bgcolor="#d5e3f0" valign="middle">
                                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Customer"></asp:Label>
                            </td>
                            <td style="width: 30px" height="100%" align="center" bgcolor="#d5e3f0" valign="middle">
                                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Calls"></asp:Label>
                            </td>
                        </tr>
                        <asp:Label ID="lblCustomer" runat="server" Font-Names="Arial" Font-Size="8pt" Text="Label">
                        </asp:Label>
                    </table>
                </td>
                <td align="center" width="5">
                </td>
                <td with="150" align="center" valign="top">
                    <table width="100%" border=0>
                        <tr>
                            <td style="width: 100px" height="100%" align="center" bgcolor="#d5e3f0" valign="middle">
                                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Vendor"></asp:Label>
                            </td>
                            <td style="width: 30px" height="100%" align="center" bgcolor="#d5e3f0" valign="middle">
                                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Calls"></asp:Label>
                            </td>
                        </tr>
                        <asp:Label ID="lblVendor" runat="server" Font-Names="Arial" Font-Size="8pt" Text="Label">
                        </asp:Label>
                    </table>
                </td>
            </tr>
            <tr>
            </tr>
        </table>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <asp:Timer ID="Timer1" runat="server" Interval="600000">
        </asp:Timer>
    
    </div>
    </form>
</body>
</html>
