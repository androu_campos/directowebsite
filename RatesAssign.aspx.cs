using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class RatesAssign : System.Web.UI.Page
{

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //RangeValidator rangevalida = new RangeValidator();
        //rangevalida.ControlToValidate = "txtApply";
        //rangevalida.MinimumValue = DateTime.Now.ToShortDateString();
        //rangevalida.MaximumValue = DateTime.Now.AddYears(10).ToShortDateString();
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        //PopulateOptions(NewNode);
        if (!Page.IsPostBack)
        {

            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = CommandType.StoredProcedure;
            sqlSpQuery.CommandText = "sp_RateStatistics";
            string dd = Session["ww"].ToString();
            sqlSpQuery.Parameters.Add("@ty", SqlDbType.VarChar).Value = Session["ww"].ToString();
            DataSet resultSet;
            resultSet = RunQuery(sqlSpQuery);

            if (resultSet.Tables.Count > 0)
            {
                if (resultSet.Tables[1].Rows.Count > 0)
                {
                    gvStatistics.DataSource = resultSet.Tables[1];
                    gvStatistics.DataBind();
                }

                if (resultSet.Tables[2].Rows.Count > 0)
                {
                    gvVariations.DataSource = resultSet.Tables[2];
                    gvVariations.DataBind();
                }
            }

        }
    }

    
    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString = "Data Source=localhost;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        sqlQuery.CommandTimeout = 666;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
       catch
        {
            //labelStatus.Text = "Unable to connect to SQL Server.";
        }
        return resultsDataSet;
    }


    protected void btnCheck_Click(object sender, EventArgs e)
    {
        RatesDetect();

        gvAlready.Visible = true;
        btnApply.Visible = true;
        txtApply.Visible = true;
        Image1.Visible = true;
    }
    protected void btnApply_Click(object sender, EventArgs e)
    {

        if (Convert.ToDateTime(txtApply.Text) < DateTime.Now.Date.AddDays(-1))
        {
            Label4.Visible = true;   

        }
        else
        {
            Label4.Visible = false;
            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = CommandType.StoredProcedure;
            sqlSpQuery.CommandText = "sp_RateInsertShedule";
            string dd = Session["ww"].ToString();
            
            sqlSpQuery.Parameters.Add("@ty", SqlDbType.VarChar).Value = Session["ww"].ToString();
            sqlSpQuery.Parameters.Add("@ApplyDay", SqlDbType.SmallDateTime).Value = txtApply.Text;

            DataSet resultSet;
            resultSet = RunQuery(sqlSpQuery);
            RatesDetect();
            gvAlready.Visible = true;
            btnApply.Enabled = false;
            btnCheck.Enabled = false;
        }


    }


    private void RatesDetect()
    {
        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;
        sqlSpQuery.CommandText = "sp_RateDetectShedule";
        string dd = Session["ww"].ToString();
        sqlSpQuery.Parameters.Add("@ty", SqlDbType.VarChar).Value = Session["ww"].ToString();

        DataSet resultSet;
        resultSet = RunQuery(sqlSpQuery);

        SqlDsratesAlready.SelectCommand = "SELECT [ID],[ProviderName],[Rates],[Applyday],[Uploaded] FROM [CDRDB].[dbo].[RateSchedules] Order by providername";

    }

    protected void txtApply_TextChanged(object sender, EventArgs e)
    {
        //if (Convert.ToDateTime(txtApply.Text) < DateTime.Now)
        //    Response.Write("No");
        //else
        //    Response.Write("Si");
    }
}
