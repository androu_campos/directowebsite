<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CallsGraphCCCustS.aspx.cs"
    Inherits="PortsGraphCCCust" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
    
    function dispData(date,calls,graph)
    {
    
    if (document.getElementById)
	{
									        
    var a = date;
    var i=0;
    var b;
    var c;
    if(a.length > 3)
    {
      
      i = a.lastIndexOf("/");
      b = a.substring(i+5);
      a = a.substring(0,i+5);
      
      c = a + " " + b;
      
      document.getElementById('<%=lblMaxPoint.ClientID%>').innerText = "DateTime: " + c + " , Calls: " + calls;
      
    }
    
     
	}

	
    }    
    
    </script>

    <table>
        <tr>
            <td style="width: 904px; height: 12px;" valign="top">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Active Calls Graph"
                    Width="125px"></asp:Label></td>
        </tr>
        <tr>
            <td valign="top" height="30" style="border-right: 0px solid; border-top: 0px solid;
                border-left: 0px solid; border-bottom: 0px solid; width: 904px;">
                <table style="height: 29px">
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 61px; height: 24px;">
                            <asp:Label ID="lblAg" runat="server" CssClass="labelBlue8" Text="Agency"></asp:Label></td>
                        <td style="width: 117px; height: 24px;">
                            <asp:DropDownList ID="dpdAgen" runat="server" CssClass="dropdown" AutoPostBack="true" OnSelectedIndexChanged="dpdAgen_SelectedIndexChanged">
                                <asp:ListItem Text="**ALL**" Value="**ALL**"></asp:ListItem>
                                <asp:ListItem Text="BENTO" Value="BENTO"></asp:ListItem>
                                <asp:ListItem Text="CONTACTO" Value="CONTACTO"></asp:ListItem>
                                <asp:ListItem Text="ECI" Value="ECI"></asp:ListItem>
                                <asp:ListItem Text="ERGON" Value="ERGON"></asp:ListItem>
                                <asp:ListItem Text="MEGACAL" Value="MEGACAL"></asp:ListItem>
                                <asp:ListItem Text="MKT911" Value="MKT911"></asp:ListItem>
                                <asp:ListItem Text="STO" Value="STO"></asp:ListItem>
                                <asp:ListItem Text="TARGETONE" Value="TARGETONE"></asp:ListItem>
                                <asp:ListItem Text="VCIP" Value="VCIP"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td align="center" bgcolor="#d5e3f0" width="61" style="height: 24px">
                            <asp:Label ID="lblCC" runat="server" CssClass="labelBlue8" Text="CallCenter:"></asp:Label></td>
                        <td style="width: 117px; height: 24px;">
                            <asp:DropDownList ID="dpdCC" runat="server" CssClass="dropdown" Enabled="false">
                                <asp:ListItem Text="**ALL**" Value="**ALL**"></asp:ListItem>
                            </asp:DropDownList> 
                        </td>
                        <td style="width: 117px; height: 24px;" align="center" bgcolor="#d5e3f0">
                            <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="From:"></asp:Label></td>
                        <td style="width: 460px; height: 24px;">
                            <asp:TextBox ID="txtFrom" runat="server" CssClass="labelSteelBlue" Width="80px"></asp:TextBox>
                            <asp:Image ID="imgFrom" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                        <td style="width: 163px; height: 24px;" align="center" bgcolor="#d5e3f0">
                            <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="To:"></asp:Label></td>
                        <td style="width: 460px; height: 24px;">
                            <asp:TextBox ID="txtTo" runat="server" CssClass="labelSteelBlue" Width="80px"></asp:TextBox>
                            <asp:Image ID="imgTo" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                        <td align="right" style="width: 108px; height: 24px;">
                            <asp:Button ID="cmdReport" runat="server" CssClass="boton" OnClick="cmdReport_Click"
                                Text="View Graph" /></td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 321px">
                            <asp:Label ID="lblWarning" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                ForeColor="Red" Text="Nothing found!!" Visible="False" Width="92px"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 904px" align="left" valign="top">
                <DCWC:Chart ID="Chart1" runat="server" Width="603px" Height="340px" Palette="Dundas"
                    BackColor="#D3DFF0" ImageType="Png" ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid"
                    BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                    <Legends>
                        <DCWC:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent"
                            EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False"
                            Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Calls" ChartType="Area"
                            Color="165, 255, 69, 0">
                        </DCWC:Series>
                        <DCWC:Series BorderStyle="Dot" ChartType="Line" Color="Red" Name="MaxCalls" XValueType="DateTime">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent" ShadowColor="Transparent"
                            BackGradientType="TopBottom" Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
                            Name="Title1" Color="26, 59, 105">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
            <td style="width: 100px" align="left" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 904px">
                <asp:Label ID="lblMaxPoint" runat="server" CssClass="labelBlue8" Width="317px"></asp:Label><br />
                <%--<table style="width: 178px; height: 68px;">
                    <tr>
                        <td align="right" style="width: 100px">
                
                        </td>
                    </tr>
                </table>--%>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 904px" align="left" valign="top">
                <table>
                    <tr>
                        <td style="width: 179px">
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 179px" valign="top">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button ID="cmdDetail" runat="server" CssClass="boton" OnClick="cmdDetail_Click"
                                        Text="View detail" Visible="False" /><asp:Button ID="cmdHide" runat="server" CssClass="boton"
                                            OnClick="cmdHide_Click" Text="Hide detail" Visible="False" /><br />
                                    <asp:Label ID="lblGrid" runat="server" Text="Label" Visible="False" CssClass="labelBlue8"></asp:Label>
                                    <br />
                                    <asp:GridView ID="gdvDetail" runat="server" Font-Size="8pt" Font-Names="Arial" Visible="False"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:BoundField DataField="Time" HeaderText="Time">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Live_Calls" HeaderText="Live_Calls">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle CssClass="titleOrangegrid"></HeaderStyle>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="cmdDetail" EventName="Click"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="cmdHide" EventName="Click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td align="left" style="width: 100px" valign="top">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 100px">
                                                <asp:Label ID="Label5" runat="server" Text="Loading...Please wait" ForeColor="LimeGreen"
                                                    Width="105px" CssClass="labelBlue8"></asp:Label></td>
                                            <td style="width: 100px">
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Loading.gif"></asp:Image></td>
                                        </tr>
                                    </table>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 179px">
                            &nbsp;</td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 904px">
                <asp:SqlDataSource ID="sqldsCust" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                    SelectCommand="SELECT '**ALL**' AS Customer &#13;&#10;FROM CDRDB.dbo.ProviderIP &#13;&#10;UNION &#13;&#10;SELECT DISTINCT ProviderName AS Customer &#13;&#10;FROM CDRDB.dbo.ProviderIP AS ProviderIP_2 &#13;&#10;WHERE (Type = 'V') UNION select Customer from cdrdb.dbo.CustBrokerRel where custbroker = 'CallCenters'"
                    ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgFrom"
                    TargetControlID="txtFrom">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="imgTo"
                    TargetControlID="txtTo">
                </cc1:CalendarExtender>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 106px; height: 44px;" valign="top">
                <asp:Timer ID="Timer1" runat="server" Interval="300000">
                </asp:Timer>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
