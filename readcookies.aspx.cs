using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class readcookies : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected  void ReadClicked(Object Sender, EventArgs e)
{
//Get the cookie name the user entered
String strCookieName = NameField.Text;

//Grab the cookie
HttpCookie cookie = Request.Cookies[strCookieName];

 //Check to make sure the cookie exists
 if (null == cookie) {
 Response.Write("Cookie not found. <br><hr>");
 }
 else {
 //Write the cookie value
 String strCookieValue = cookie.Value.ToString();
 Response.Write("The " + strCookieName + " cookie contains: <b>"
 + strCookieValue + "</b><br><hr>");
 }
}
}
