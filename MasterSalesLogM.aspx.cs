using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class MasterTest : System.Web.UI.Page
{

    int[] indexs = new int[25];

    protected void Page_Load(object sender, EventArgs e)
    {
        string hometype = "";
        string cmd = string.Empty;

        string user = "";

        user = Session["DirectoUser"].ToString();

        if (!Page.IsPostBack)
        {

            fillDDLCustBroker(user);
            fillDDLVendorBroker(user);

            fillDDLCustomer(user);
            fillDDLVendor(user);

            fillDDLCountry(user);


            txtFrom.Value = DateTime.Today.AddDays(-1).ToShortDateString();
            txtTo.Value = DateTime.Today.AddDays(-1).ToShortDateString();

            txtToV.Text = txtTo.Value.ToString();
            txtFromV.Text = txtFrom.Value.ToString();


            rdbtnlType.SelectedIndex = Convert.ToInt32(Session["ReportChoosed"]);//7;
            fillRadioButton(Convert.ToInt32(Session["ReportChoosed"]));
            Session["tiempo"] = "0";

            if (hometype != "")
            {
                hometype = Session["hometype"].ToString();

                if (hometype == "W")
                    Label3.Text = "MECCA WHOLESALE MASTER SALES LOG";

                else if (hometype == "R")
                    Label3.Text = "MECCA RETAIL MASTER SALES LOG";


            }
            else
            {
                Label3.Text = "MECCA TOTAL MASTER SALES LOG";
            }

        }
        else
        {
            Session["ReportChoosed"] = rdbtnlType.SelectedIndex;
            //fillRadioButton(Convert.ToInt32(Session["ReportChoosed"]));


            Session["CountrySelected"] = ddlCountry.SelectedValue.ToString();

            Session["regionSelectedMstr"] = ddlRegion.SelectedValue.ToString();

            int selectIndx = ddlRegion.SelectedIndex;


            //string country = ddlCountry.SelectedValue.ToString();
            //country = country.Replace("**ALL**", "%");
            //DataSet dtst = Util.RunQueryByStmntatCDRDB("Select distinct region from regions where country = '" + country + "' UNION SELECT '**ALL**' AS REGION FROM REGIONS ORDER BY REGION ");
            //DataTable tbl = dtst.Tables[0];
            //foreach (DataRow row in tbl.Rows)
            //{
            //    ddlRegion.Items.Add(row[0].ToString());
            //}
            //ddlRegion.Enabled = true;
            //ddlRegion.SelectedIndex = selectIndx;
        }
    }

    private void fillDDLCustBroker(string user)
    {
        SqlConnection SqlConn1;
        SqlCommand SqlCommand1;

        SqlConn1 = new SqlConnection();
        SqlCommand1 = new SqlCommand();

        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";

        if (user != "jim.baumhart")
        {
            ddlCustBroker.Items.Clear();

            SqlCommand1 = new SqlCommand("SELECT '**ALL**' as CustBroker UNION SELECT DISTINCT CustBroker FROM MECCA2..CBroker", SqlConn1);
        }
        else
        {
            ddlCustBroker.Items.Clear();

            //SqlCommand1 = new SqlCommand("SELECT '**ALL**' as CustBroker UNION SELECT DISTINCT CustBroker FROM MECCA2..CBroker", SqlConn1);
            SqlCommand1 = new SqlCommand("SELECT '*NONE*' UNION SELECT '**ALL**' as CustBroker union SELECT 'Jim'", SqlConn1);
        }

        SqlConn1.Open();
        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
        while (myReader1.Read())
        {
            ddlCustBroker.Items.Add(myReader1.GetValue(0).ToString());
            ddlCustBroker.SelectedIndex = 1;
        }

        myReader1.Close();
        SqlConn1.Close();
    }

    private void fillDDLVendorBroker(string user)
    {
        SqlConnection SqlConn1;
        SqlCommand SqlCommand1;

        SqlConn1 = new SqlConnection();
        SqlCommand1 = new SqlCommand();

        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";

        if (user != "jim.baumhart")
        {
            ddlVendorBroker.Items.Clear();

            SqlCommand1 = new SqlCommand("SELECT '**ALL**' as VendorBroker UNION SELECT DISTINCT VendorBroker FROM MECCA2..VBroker", SqlConn1);
        }
        else
        {
            ddlVendorBroker.Items.Clear();

            //SqlCommand1 = new SqlCommand("SELECT '*NONE*' UNION SELECT '**ALL**' as VendorBroker UNION SELECT DISTINCT VendorBroker FROM MECCA2..VBroker", SqlConn1);
            SqlCommand1 = new SqlCommand("SELECT '*NONE*' UNION SELECT '**ALL**' as VendorBroker union SELECT 'Jim'", SqlConn1);
        }

        SqlConn1.Open();
        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
        while (myReader1.Read())
        {
            ddlVendorBroker.Items.Add(myReader1.GetValue(0).ToString());
            ddlVendorBroker.SelectedIndex = 1;
        }

        myReader1.Close();
        SqlConn1.Close();
    }

    private void fillDDLCustomer(string user)
    {
        SqlConnection SqlConn1;
        SqlCommand SqlCommand1;

        SqlConn1 = new SqlConnection();
        SqlCommand1 = new SqlCommand();

        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";

        if (user != "jim.baumhart")
        {
            ddlCust.Items.Clear();

            SqlCommand1 = new SqlCommand("SELECT DISTINCT ProviderName from CDRDB.dbo.providerip WHERE Type like 'C' UNION SELECT '**ALL**' from CDRDB.dbo.providerip", SqlConn1);
        }
        else
        {
            ddlCust.Items.Clear();

            //SqlCommand1 = new SqlCommand("SELECT DISTINCT ProviderName from CDRDB.dbo.providerip_active WHERE Type like 'C' AND Active = 1 UNION SELECT '**ALL**' from CDRDB.dbo.providerip_active", SqlConn1);
            SqlCommand1 = new SqlCommand("SELECT DISTINCT ProviderName from CDRDB.dbo.providerip join cdrdb.dbo.custbrokerrel on customer = providername WHERE Type like 'C' UNION SELECT '**ALL**' from CDRDB.dbo.providerip", SqlConn1);
        }

        SqlConn1.Open();
        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
        while (myReader1.Read())
        {
            ddlCust.Items.Add(myReader1.GetValue(0).ToString());
        }

        myReader1.Close();
        SqlConn1.Close();
    }

    private void fillDDLVendor(string user)
    {

        SqlConnection SqlConn1;
        SqlCommand SqlCommand1;

        SqlConn1 = new SqlConnection();
        SqlCommand1 = new SqlCommand();

        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";

        if (user != "jim.baumhart")
        {
            ddlVendor.Items.Clear();

            SqlCommand1 = new SqlCommand("SELECT DISTINCT ProviderName from CDRDB.dbo.providerip_active WHERE Type like 'V' AND Active = 1 UNION SELECT '**ALL**' from CDRDB.dbo.providerip_active", SqlConn1);
        }
        else
        {   
            ddlVendor.Items.Clear();

            //SqlCommand1 = new SqlCommand("SELECT DISTINCT ProviderName from CDRDB.dbo.providerip_active WHERE Type like 'V' AND Active = 1 UNION SELECT '**ALL**' from CDRDB.dbo.providerip_active", SqlConn1);
            SqlCommand1 = new SqlCommand("SELECT DISTINCT ProviderName from CDRDB.dbo.providerip_active join cdrdb.dbo.vendorbrokerrel on vendor = providername WHERE Type like 'V' AND Active = 1 and vendorbroker = 'Jim' UNION SELECT '**ALL**' from CDRDB.dbo.providerip_active", SqlConn1);
        }

        SqlConn1.Open();
        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
        while (myReader1.Read())
        {

            ddlVendor.Items.Add(myReader1.GetValue(0).ToString());
        }


        myReader1.Close();
        SqlConn1.Close();
    }

    private void fillDDLCountry(string user)
    {

        SqlConnection SqlConn1;
        SqlCommand SqlCommand1;

        SqlConn1 = new SqlConnection();
        SqlCommand1 = new SqlCommand();

        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";

        if (user != "jim.baumhart")
        {
            ddlCountry.Items.Clear();

            SqlCommand1 = new SqlCommand("SELECT '**ALL**' as Country UNION SELECT DISTINCT Country FROM CDRDB..Regions ORDER BY Country", SqlConn1);
        }
        else
        {
            ddlCountry.Items.Clear();

            SqlCommand1 = new SqlCommand("SELECT 'MEXICO'", SqlConn1);
            ddlCountry.Enabled = false;
        }

        SqlConn1.Open();
        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
        while (myReader1.Read())
        {

            ddlCountry.Items.Add(myReader1.GetValue(0).ToString());
        }


        myReader1.Close();
        SqlConn1.Close();
    }

    protected void ddlCust_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlCust.SelectedItem.ToString() == "**ALL**") hfCust.Value = "%";
        else hfCust.Value = ddlCust.SelectedItem.ToString();

    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {


    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlRegion.Items.Clear();
        string country = ddlCountry.SelectedValue.ToString();
        country = country.Replace("**ALL**", "%");
        DataSet dtst = null;

        if (country == "COTE D''IVOIRE")
        {
            dtst = Util.RunQueryByStmntatCDRDB("Select distinct region from regions where country = 'COTE D''''IVOIRE' UNION SELECT '**ALL**' AS REGION FROM REGIONS ORDER BY REGION ");
        }
        else
        {
            dtst = Util.RunQueryByStmntatCDRDB("Select distinct region from regions where country = '" + country + "' UNION SELECT '**ALL**' AS REGION FROM REGIONS ORDER BY REGION ");
        }
        DataTable tbl = dtst.Tables[0];
        foreach (DataRow row in tbl.Rows)
        {
            ddlRegion.Items.Add(row[0].ToString());
        }
        ddlRegion.Enabled = true;
        //ddlRegion.SelectedIndex = selectIndx;


    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string SelectString, WhereString, FromString, FromSBro = "", GroupByString, UnionString, BrokerWhere, QQ, sOrder;
        string sF, sW, sS, sSt, sG, sSUM = "";
        string hometype = "", sourcetable = "", routetype = "";
        string businessunit = "", custunits = "";

        string user;
 
        user = Session["DirectoUser"].ToString();

        bool bCV = false, bVB = false;

        if (Session["hometype"] != "")
        {
            hometype = Session["hometype"].ToString();

            if (hometype == "W")
                sourcetable = "FROM MECCA2..trafficlogsegments T";

            if (hometype == "W")
            {
                routetype = " routetype = 'W' ";
                Label3.Text = "MECCA WHOLESALE MASTER SALES LOG";
            }
            else if (hometype == "R")
            {
                routetype = " routetype = 'R' ";
                Label3.Text = "MECCA ICS MASTER SALES LOG";
            }

        }
        else
        {
            sourcetable = "FROM MECCA2..trafficlog T";
            Label3.Text = "MECCA TOTAL MASTER SALES LOG";
        }

        if (Session["regionSelectedMstr"].ToString() == "**ALL**") hfRegion.Value = "%";
        else if (Session["regionSelectedMstr"].ToString() == "** NONE **") hfRegion.Value = "%";
        else if (Session["regionSelectedMstr"].ToString() == "") hfRegion.Value = "%";
        else hfRegion.Value = Session["regionSelectedMstr"].ToString();

        if (ddlCountry.SelectedItem.ToString() == "**ALL**") hfCountry.Value = "%";
        //if (hfCust.Value.ToString() == "") hfCust.Value = "%";

        if (ddlCust.SelectedItem.ToString() == "**ALL**")
        { hfCust.Value = "%"; }
        else { hfCust.Value = ddlCust.SelectedItem.ToString(); }

        //if (hfVendor.Value.ToString() == "") hfVendor.Value = "%";

        if (ddlVendor.SelectedItem.ToString() == "**ALL**") hfVendor.Value = "%";
        else if (ddlVendor.SelectedItem.ToString() == "** NONE **") hfVendor.Value = "%";
        else hfVendor.Value = ddlVendor.SelectedItem.ToString();

        if (ddlCustBroker.SelectedItem.ToString() == "**ALL**") hfCustBroker.Value = "%";
        else hfCustBroker.Value = ddlCustBroker.SelectedItem.ToString();

        //if (hfCustBroker.Value.ToString() == "") hfCustBroker.Value = "%";
        if (ddlVendorBroker.SelectedItem.ToString() == "**ALL**") hfVendorBroker.Value = "%";
        else hfVendorBroker.Value = ddlVendorBroker.SelectedItem.ToString();

        //if (hfVendorBroker.Value.ToString() == "") hfVendorBroker.Value = "%";

        //FIN checando valores no asignados

        SelectString = "";
        BrokerWhere = "";
        FromString = " FM2TL";

        WhereString = "";

        GroupByString = "";
        UnionString = "";

        //WHERE Builder

        string from = txtFromV.Text;
        string to = txtToV.Text;
        from = from.Replace("/", "|");
        to = to.Replace("/", "|");


        WhereString = WhereString + "'" + from + "' AND '" + to + "'";
        if (hfCust.Value.ToString() != "%")
        {
            businessunit = hfCust.Value.ToString();
            custunits = getCustomer(businessunit);

            if (businessunit == "ICS")
            {
                WhereString = WhereString + " AND T.Customer LIKE 'ICS%'";
            }
            else if (businessunit == "TALKTEL")
            {
                WhereString = WhereString + " AND T.Customer in ( " + custunits + ") ";
            }
            else
                WhereString = WhereString + " AND T.Customer LIKE '" + hfCust.Value.ToString() + "'";
        }

        if (hfVendor.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Vendor LIKE '" + hfVendor.Value.ToString() + "'";
        }
        if (hfCountry.Value.ToString() != "%")
        {
            if (ddlCountry.SelectedValue.ToString() == "COTE D''IVOIRE")
            {
                WhereString = WhereString + " AND T.Country LIKE 'COTE D''''IVOIRE'";
            }
            else
            {
                WhereString = WhereString + " AND T.Country LIKE '" + ddlCountry.SelectedValue.ToString() + "'";
            }
        }
        if (hfRegion.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Region LIKE '" + hfRegion.Value.ToString() + "'";
        }
        //LMC
        if (dpdLMC.SelectedValue.ToString() != "**Select LMC**")
        {
            WhereString = WhereString + " AND T.LMC ='" + dpdLMC.SelectedValue.ToString() + "'";
        }
        //TYPE
        if (dpdType.SelectedValue.ToString() != "**Select Type**")
        {
            WhereString = WhereString + " AND T.Type ='" + dpdType.SelectedValue.ToString() + "'";
        }
        //CLASS 
        if (dpdClass.SelectedValue.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Class LIKE '" + dpdClass.SelectedValue.ToString() + "'";
        }

        if (hfCustBroker.Value.ToString() != "%")
        {
            //WhereString = WhereString + " AND B.CustBroker LIKE '" + hfCustBroker.Value.ToString() + "'";
            FromSBro = FromSBro + " LJM2CB";
            bCV = true;
        }
        if (hfVendorBroker.Value.ToString() != "%")
        {
            //WhereString = WhereString + " AND V.VendorBroker LIKE '" + hfVendorBroker.Value.ToString() + "'";
            FromSBro = FromSBro + " LJM2VB";
            bVB = true;
        }

        //FIN WHERE Builder


        if (chkBilling.Items[0].Selected == true)//GROUP BY DATE
        {
            SelectString = SelectString + ",CONVERT(VARCHAR(11) ,T.Billingdate, " + cmbDateFormat.SelectedValue.ToString() + ") as BillingDate";
            GroupByString = GroupByString + ",T.BillingDate";
            UnionString = UnionString + ",'** ALL **' AS BillingDate";
        }

        if (ckboxl.Items[0].Selected == true)
        //if (indexs[0] == 1)
        {
            if (custunits != "'-'" && hfCust.Value.ToString() != "%")
            {
                SelectString = SelectString + ",'" + businessunit + "' AS Customer";
            }
            else
            {
                SelectString = SelectString + ",T.Customer";
                GroupByString = GroupByString + ",T.Customer";
            }

            UnionString = UnionString + ",'****' AS Customer";
        }
        if (ckboxl.Items[1].Selected == true)
        //if (indexs[1] == 1)
        {
            SelectString = SelectString + ",T.Vendor";
            GroupByString = GroupByString + ",T.Vendor";
            UnionString = UnionString + ",'****' AS Vendor";
        }
        if (ckboxl.Items[2].Selected == true)
        //if (indexs[2] == 1)
        {
            SelectString = SelectString + ",CustBroker";
            GroupByString = GroupByString + ",B.CustBroker";
            UnionString = UnionString + ",'****' as CustBroker";
            if (bCV == false) FromSBro = FromSBro + " LJM2CB";
        }
        if (ckboxl.Items[3].Selected == true)
        //if (indexs[3] == 1)
        {
            SelectString = SelectString + ",VendorBroker";
            GroupByString = GroupByString + ",V.VendorBroker";
            UnionString = UnionString + ",'****' as VendorBroker";
            if (bVB == false) FromSBro = FromSBro + " LJM2VB";
        }
        if (ckboxl.Items[4].Selected == true)
        //if (indexs[4] == 1)
        {
            SelectString = SelectString + ",T.Country";
            GroupByString = GroupByString + ",T.Country";
            UnionString = UnionString + ",'****' as Country";
        }
        if (ckboxl.Items[5].Selected == true)
        //if (indexs[5] == 1)
        {
            SelectString = SelectString + ",T.Region";
            GroupByString = GroupByString + ",T.Region";
            UnionString = UnionString + ",'****' as Region";
        }
        if (ckboxl.Items[6].Selected == true)
        //if (indexs[6] == 1)
        {
            SelectString = SelectString + ",T.RegionPrefix";
            GroupByString = GroupByString + ",T.RegionPrefix";
            UnionString = UnionString + ",'****' as RegionPrefix";
        }
        if (ckboxl.Items[7].Selected == true)
        //if (indexs[7] == 1)
        {
            SelectString = SelectString + ",T.CustPrefix";
            GroupByString = GroupByString + ",T.CustPrefix";
            UnionString = UnionString + ",'****'";
        }
        if (ckboxl.Items[8].Selected == true)
        //if (indexs[8] == 1)
        {
            SelectString = SelectString + ",T.VendorPrefix";
            GroupByString = GroupByString + ",T.VendorPrefix";
            UnionString = UnionString + ",'****' as VendorPrefix";
        }
        if (ckboxl.Items[9].Selected == true || dpdType.SelectedValue.ToString() != "**Select Type**")
        //if (indexs[9] == 1 || dpdType.SelectedValue.ToString() != "**Select Type**")
        {
            SelectString = SelectString + ",T.Type";
            GroupByString = GroupByString + ",T.Type";
            UnionString = UnionString + ",'****' as [Type]";
            //FromString = FromString + " LJM2R";
        }
        if (ckboxl.Items[10].Selected == true || dpdLMC.SelectedValue.ToString() != "**Select LMC**")
        //if (indexs[10] == 1 || dpdLMC.SelectedValue.ToString() != "**Select LMC**")
        {
            SelectString = SelectString + ",T.LMC";
            GroupByString = GroupByString + ",T.LMC";
            UnionString = UnionString + ",'****' as LMC";

            //if (ckboxl.Items[9].Selected == false && dpdType.SelectedValue.ToString() == "**Select Type**")
            //if (indexs[9] == 0)
            //{
            //  FromString = FromString + " LJM2R";
            //}
        }
        //CLASS                 
        //11 es class
        if (ckboxl.Items[11].Selected == true || dpdClass.SelectedValue.ToString() != "%")
        //if (indexs[11] == 1 || dpdClass.SelectedValue.ToString() != "%")
        {
            SelectString = SelectString + ",T.Class";
            GroupByString = GroupByString + ",T.Class";
            UnionString = UnionString + ",'****' as Class";
            //if (ckboxl.Items[10].Selected == false && ckboxl.Items[9].Selected == false && dpdType.SelectedValue.ToString() == "**Select Type**" && dpdLMC.SelectedValue.ToString() == "**Select LMC**")
            //    //if (indexs[10] != 1 && indexs[9] != 1)
            //{
            //    FromString = FromString + " LJM2R";
            //}

        }

        if (ckboxl.Items[12].Selected == true)
        //if (indexs[12] == 1)
        {
            if (user != "jim.baumhart")
            {
                SelectString = SelectString + ",T.Cost";
                GroupByString = GroupByString + ",T.Cost";
                UnionString = UnionString + ",NULL as Cost";
            }
            else
            {
                SelectString = SelectString + ",CASE WHEN T.Vendor LIKE 'TLK%' THEN 0.0008 WHEN T.Vendor LIKE 'CTR%' THEN 0.0008 ELSE T.Cost END Cost";
                GroupByString = GroupByString + ",CASE WHEN T.Vendor LIKE 'TLK%' THEN 0.0008 WHEN T.Vendor LIKE 'CTR%' THEN 0.0008 ELSE T.Cost END";
                UnionString = UnionString + ",NULL as Cost";
            }
        }
        if (ckboxl.Items[13].Selected == true)
        //if (indexs[13] == 1)
        {
            SelectString = SelectString + ",T.Price";
            GroupByString = GroupByString + ",T.Price";
            UnionString = UnionString + ",NULL AS Price";
        }
        //Arriba con group
        //Sin group abajo

        if (ckboxl.Items[14].Selected == true)
        //if (indexs[14] == 1)
        {
            sSUM = sSUM + ",ROUND(SUM(T.CustMinutes),4) as CMinutes";
        }
        if (ckboxl.Items[15].Selected == true)
        //if (indexs[15] == 1)
        {
            sSUM = sSUM + ",ROUND(SUM(T.VendorMinutes),4) as VMinutes";
        }
        if (ckboxl.Items[16].Selected == true)
        //if (indexs[16] == 1)
        {
            //sSUM = sSUM + ",ROUND(SUM(CASE WHEN T.Customer = 'STI-800' AND (T.Vendor like 'CC-%' OR T.Vendor = 'TP-BDAY-IN') then T.TotalPrice else T.TotalCost END),4) as TotalCost";
            if (user != "jim.baumhart")
            {
                sSUM = sSUM + ",ROUND(SUM(T.TotalCost),4) as TotalCost";
            }
            else
            {
                sSUM = sSUM + ",ROUND(SUM(CASE WHEN T.VENDOR LIKE 'TLK%' THEN (T.CustMinutes*0.0008) WHEN T.VENDOR LIKE 'CTR%' THEN (T.CustMinutes*0.0008) else T.TotalCost END) ,4) AS TotalCost";
                GroupByString = GroupByString + ",T.Vendor";
            }
        }
        if (ckboxl.Items[17].Selected == true)
        //if (indexs[17] == 1)
        {
            //sSUM = sSUM + ",ROUND(SUM(CASE WHEN T.Customer = 'SI-800' AND (T.Vendor like 'CC-%' OR T.Vendor = 'TP-BDAY-IN') then T.TotalCost WHEN (T.Customer like 'CC-%' or T.Customer like 'CCP-%'or T.Customer like 'VINTALK-OUT') Then (T.TotalCost / 0.9) else T.TotalPrice END),4) as TotalSales";
            sSUM = sSUM + ",ROUND(SUM(T.TotalPrice),4) as TotalSales";
        }
        if (ckboxl.Items[18].Selected == true)
        //if (indexs[18] == 1)
        {
            if (user != "jim.baumhart")
            {
                sSUM = sSUM + ",ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit ";
            }
            else
            {
                sSUM = sSUM + ",ROUND(SUM(CASE WHEN T.VENDOR LIKE 'TLK%' THEN T.TotalPrice-(T.CustMinutes*0.0008) WHEN T.VENDOR LIKE 'CTR%' THEN (T.TOTALPRICE-(T.CustMinutes*0.0008)) else (T.TotalPrice-T.TotalCost) END),4) AS Profit ";
                //GroupByString = GroupByString + ",T.Vendor";
            }
        }

        if (ckboxl.Items[19].Selected == true)
        //if (indexs[19] == 1)
        {

            sSUM = sSUM + ",SUM(T.tCalls) as Attempts";
        }

        if (ckboxl.Items[20].Selected == true)
        //if (indexs[20] == 1)
        {
            sSUM = sSUM + ",SUM(T.BillableCalls) as AnsweredCalls";
        }

        if (ckboxl.Items[21].Selected == true)
        //if (indexs[21] == 1)
        {
            //ASR (answer seizure ratio) = Answered calls / (all failed calls � calls rejected with 34) 
            sSUM = sSUM + ",SUM(T.RA) as [Rejected Calls]";
        }

        if (ckboxl.Items[22].Selected == true)
        //if (indexs[22] == 1)
        {
            //ASR (answer seizure ratio) = Answered calls / (all failed calls � calls rejected with 34) 
            sSUM = sSUM + ",mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR";
        }

        if (ckboxl.Items[23].Selected == true)
        //if (indexs[23] == 1)
        {
            //ABR (answer to bid ration, or peg count) = Answered calls / (all failed calls)

            sSUM = sSUM + ",mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR";
        }

        if (ckboxl.Items[24].Selected == true)
        //if (indexs[24] == 1)
        {
            sSUM = sSUM + ",mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD";
        }


        sS = "";
        sOrder = "";
        if (SelectString.Length > 3)
        {
            sS = SelectString.Substring(1);
            sOrder = " ORDER BY " + sS.Replace("CONVERT(VARCHAR(11) ,T.Billingdate, " + cmbDateFormat.SelectedValue.ToString() + ") as", " ");

            if (custunits != "'-'" && hfCust.Value.ToString() != "%")
            {
                sOrder = sOrder.Replace("'" + businessunit + "' AS Customer,", " ");
                sOrder = sOrder.Replace(",'" + businessunit + "' AS Customer", " ");
            }
            else// (hfCust.Value.ToString() == "%")
            {
                //sOrder = sOrder.Replace("CASE WHEN T.Customer Like 'ICS%' THEN 'ICS' ELSE T.Customer END AS [Customer]", "CASE WHEN T.Customer Like 'ICS%' THEN 'ICS' ELSE T.Customer END");

                sOrder = sOrder.Replace("CASE WHEN T.Customer like 'ICS%' THEN 'ICS' WHEN (T.Customer like 'CC-%' OR T.Customer like 'CCP-%' OR T.Customer like 'VINTALK-OUT') THEN 'TALKTEL' ELSE T.Customer END as Customer", " Customer");
                sOrder = sOrder.Replace("T.Cost END Cost", "T.Cost END");
            }
        }
        else
        {
            if (sSUM.Length > 3)
            {
                sSUM = sSUM.Substring(1);
            }
        }

        string sHaving = " HAVING SUM(CustMinutes) > 0 ";
        sG = string.Empty;

        sF = FromString + FromSBro;
        sW = WhereString;
        if (GroupByString.Length > 3)
        {
            sG = " GROUP BY " + GroupByString.Substring(1);
        }
        sSt = "";
        if (UnionString.Length > 1)
        {
            sSt = UnionString.Substring(1);
        }

        sW = sW.Replace("|", "/");
        sF = sF.Replace("FM2TL", "FROM MECCA2..trafficlog T");

        if (hfCustBroker.Value.ToString() == "*NONE*" || hfCustBroker.Value.ToString() != "%")
        {
            sF = sF.Replace("LJM2CB", " JOIN MECCA2..CBroker B ON T.Customer = B.Customer AND B.CustBroker = 'Jim' ");
        }
        else if (hfCustBroker.Value.ToString() == "%")
        {
            sF = sF.Replace("LJM2CB", " LEFT JOIN MECCA2..CBroker B ON T.Customer = B.Customer ");
        }



        if (hfVendorBroker.Value.ToString() == "*NONE*" || hfVendorBroker.Value.ToString() != "%")
        {
            sF = sF.Replace("LJM2VB", " JOIN MECCA2..VBroker V ON T.Vendor = V.Vendor AND V.VendorBroker = 'Jim' ");
        }
        else if (hfVendorBroker.Value.ToString() == "%")
        {
            sF = sF.Replace("LJM2VB", " LEFT JOIN MECCA2..VBroker V ON T.Vendor = V.Vendor ");
        }

        
        sF = sF.Replace("LJM2R", "LEFT JOIN MECCA2.dbo.Regions R ON T.RegionPrefix = R.Code");

        QQ = "sS=" + sS + "&sF=" + sF + "&sW=" + sW + "&sG=" + sG + "&sSt=" + sSt + "&sSUM=" + sSUM;

        string Query = " SELECT " + sS + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT " + sSt + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + sHaving + sOrder;



        if (hometype == "")
        {
            if (chkControlRoutes.Checked == true)
            {
                if (user != "jim.baumhart")
                {
                    Query = " SELECT " + sS + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT " + sSt + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + sHaving + sOrder;
                }
                else
                {
                    Query = " SELECT " + sS + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + " " + sG + sHaving + sOrder;
                }
            }
            else
            {
                if (user != "jim.baumhart")
                {
                    Query = " SELECT " + sS + sSUM + sF + " WHERE T.Customer NOT IN (SELECT CUSTOMER FROM mecca2.dbo.ControlCustomer) AND BillingDate BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT " + sSt + sSUM + sF + " WHERE T.Customer NOT IN (SELECT CUSTOMER FROM mecca2.dbo.ControlCustomer) AND BillingDate BETWEEN " + sW + sHaving + sOrder;
                }
                else
                {
                    Query = " SELECT " + sS + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT " + sSt + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + sHaving + sOrder;;
                }
            }
        }
        else
            Query = " SELECT " + sS + sSUM + sF + " WHERE " + routetype + " and BillingDate BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT " + sSt + sSUM + sF + " WHERE " + routetype + " and BillingDate BETWEEN " + sW + sHaving + sOrder;

        Session["SalesLogS"] = Query + " option (maxdop 1)";

        string dfrom = from;
        string dto = to;

        dfrom = from.Replace("|", "/");
        dto = to.Replace("|", "/");

        Session["dfrom"] = dfrom.ToString();
        Session["dto"] = dto.ToString();

        //
        Session["ReportChoosed"] = rdbtnlType.SelectedIndex;

        //Response.Write(Session["SalesLogS"]);
        //Response.End();

        if ((sS.Length + sSUM.Length) > 6)
        {
            if (chkControlRoutes.Checked == true)
            {
                Response.Redirect("~/MSLH.aspx?control=1", false);
            }
            else
            {
                Response.Redirect("~/MSLH.aspx?control=0", false);
            }

        }

    }

    public void fillRadioButton(int index)
    {        //Session["ReportChoosed"] = index;

        // "0">Customer "1">Vendor "2">CustBroker "3">VendorBroker "4">Country "5">Region "6">RegionPrefix
        // "7">CustPrefix "8">VendorPrefix "9">Type "10">LMC "11">Class "12">Buy "13">Sell "14">Minutes (Customer)
        // "15">Minutes (Vendor) "16">TotalCost "17">TotalSales "18">Profit "19">Attempts "20">Answered Calls
        // "21">Rejected calls "22">ASR "23">ABR "24">ACD

        if (index == 0)//by region
        {
            checkList();

            this.ckboxl.ClearSelection();

            this.ckboxl.Items[0].Selected = true;//Cust

            this.ckboxl.Items[1].Selected = true;//Vendor

            this.ckboxl.Items[4].Selected = true;//Country

            this.ckboxl.Items[5].Selected = true;//Region            

            this.ckboxl.Items[12].Selected = true;//Sell           

            this.ckboxl.Items[13].Selected = true;//Buy

            this.ckboxl.Items[15].Selected = true;//VMinutes

            this.ckboxl.Items[18].Selected = true;//Profit

            this.ckboxl.Items[22].Selected = true; //ASR

            this.ckboxl.Items[24].Selected = true; //ACD
        }
    }
    public void checkList()
    {
        for (int i = 0; i <= 24; i++)
        {
            if (this.ckboxl.Items[i].Selected == true)
            {
                indexs[i] = 1;
            }
            else
            {
                indexs[i] = 0;
            }
        }
    }
    protected void rdbtnlType_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (rdbtnlType.SelectedIndex == 0)//by region
        {
            ckboxl.ClearSelection();
            this.ckboxl.Items[0].Selected = true;//Cust

            this.ckboxl.Items[1].Selected = true;//Vendor

            this.ckboxl.Items[4].Selected = true;//Country

            this.ckboxl.Items[5].Selected = true;//Region            

            this.ckboxl.Items[12].Selected = true;//Sell           

            this.ckboxl.Items[13].Selected = true;//Buy

            this.ckboxl.Items[15].Selected = true;//VMinutes

            this.ckboxl.Items[18].Selected = true;//Profit

            this.ckboxl.Items[22].Selected = true; //ASR

            this.ckboxl.Items[24].Selected = true; //ACD

        }
        else //Personalizado
        {
            ckboxl.ClearSelection();
            ckboxl.Items[18].Selected = true;//Profit
            ckboxl.Enabled = true;
        }

    }
    protected void MECCA_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("http://mecca3.computer-tel.com/MECCA/");
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect("MasterSalesLog.aspx");
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    protected void ddlCustBroker_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlCustBroker.SelectedItem.ToString() == "**ALL**") hfCustBroker.Value = "%";
        //else hfCustBroker.Value = ddlCustBroker.SelectedItem.ToString();

        if (ddlCustBroker.SelectedItem.ToString() == "**ALL**")
            ddlVendorBroker.SelectedIndex = 2;
        else if (ddlCustBroker.SelectedItem.ToString() == "*NONE*")
            ddlVendorBroker.SelectedIndex = 1;
        else
            ddlVendorBroker.SelectedIndex = 0;
    }
    protected void ddlVendorBroker_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlVendorBroker.SelectedItem.ToString() == "**ALL**") hfVendorBroker.Value = "%";
        //else hfVendorBroker.Value = ddlVendorBroker.SelectedItem.ToString();

        if (ddlVendorBroker.SelectedItem.ToString() == "**ALL**")
            ddlCustBroker.SelectedIndex = 2;
        else if (ddlVendorBroker.SelectedItem.ToString() == "*NONE*")
            ddlCustBroker.SelectedIndex = 1;
        else
            ddlCustBroker.SelectedIndex = 0;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void cbxLtiempo_SelectedIndexChanged(object sender, EventArgs e)
    {
        DateTime hoy;
        hoy = DateTime.Today.AddDays(-1);
        Session["tiempo"] = cbxLtiempo.SelectedIndex.ToString();
        chkBilling.Items[0].Selected = false;
        if (cbxLtiempo.SelectedIndex == 0)
        {
            txtFromV.Text = DateTime.Today.AddDays(-1).ToShortDateString();
            txtToV.Text = DateTime.Today.AddDays(-1).ToShortDateString();
        }
        else if (cbxLtiempo.SelectedIndex == 1)
        {
            setDays(Convert.ToDateTime(txtFromV.Text));
        }
        else if (cbxLtiempo.SelectedIndex == 2)
        {
            setDays(hoy);
        }
    }
    protected void txtFromV_TextChanged(object sender, EventArgs e)
    {
        setDays(Convert.ToDateTime(txtFromV.Text));
    }
    protected void txtToV_TextChanged(object sender, EventArgs e)
    {
        setDays(Convert.ToDateTime(txtToV.Text));
    }
    protected void setDays(DateTime dia)
    {
        DayOfWeek da;

        if (Session["tiempo"].ToString() == "1")
        {
            dia = dia.AddDays(1 - dia.Day);
            txtFromV.Text = dia.ToShortDateString();

            dia = dia.AddMonths(1);
            dia = dia.AddDays(-dia.Day);
            txtToV.Text = dia.ToShortDateString();
        }
        else if (Session["tiempo"].ToString() == "2")
        {
            da = dia.DayOfWeek;
            if (da == DayOfWeek.Monday)
            {
                txtFromV.Text = dia.AddDays(0).ToShortDateString();
                txtToV.Text = dia.AddDays(6).ToShortDateString();
            }
            else if (da == DayOfWeek.Tuesday)
            {
                txtFromV.Text = dia.AddDays(-1).ToShortDateString();
                txtToV.Text = dia.AddDays(5).ToShortDateString();
            }
            else if (da == DayOfWeek.Wednesday)
            {
                txtFromV.Text = dia.AddDays(-2).ToShortDateString();
                txtToV.Text = dia.AddDays(4).ToShortDateString();
            }
            else if (da == DayOfWeek.Thursday)
            {
                txtFromV.Text = dia.AddDays(-3).ToShortDateString();
                txtToV.Text = dia.AddDays(3).ToShortDateString();
            }
            else if (da == DayOfWeek.Friday)
            {
                txtFromV.Text = dia.AddDays(-4).ToShortDateString();
                txtToV.Text = dia.AddDays(2).ToShortDateString();
            }
            else if (da == DayOfWeek.Saturday)
            {
                txtFromV.Text = dia.AddDays(-5).ToShortDateString();
                txtToV.Text = dia.AddDays(1).ToShortDateString();
            }
            else if (da == DayOfWeek.Sunday)
            {
                txtFromV.Text = dia.AddDays(-6).ToShortDateString();
                txtToV.Text = dia.AddDays(0).ToShortDateString();
            }
        }


    }

    protected string getCustomer(string customer)
    {
        string cust = "";
        String connectionString = ConfigurationManager.ConnectionStrings["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter;
        SqlCommand sQuery = new SqlCommand();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@businessunit", SqlDbType.VarChar);
        param[0].Value = customer;
        sQuery.Parameters.AddRange(param);

        DataSet ResultSet = new DataSet();

        sQuery.CommandType = System.Data.CommandType.StoredProcedure;
        sQuery.CommandText = "sp_getbusinessuc";
        sQuery.Connection = DBConnection;
        dbAdapter = new SqlDataAdapter(sQuery);

        dbAdapter.Fill(ResultSet);

        int index = 0;



        if (ResultSet.Tables.Count > 0)
        {
            while (index < ResultSet.Tables[0].Rows.Count)
            {
                cust += "'" + ResultSet.Tables[0].Rows[index][0].ToString() + "',";
                index++;
            }

            cust = cust.Substring(0, cust.Length - 1);
        }

        return cust;
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

}
