using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;
public partial class DidCustStatePh : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TextBox1.Text = DateTime.Today.AddDays(-1).ToShortDateString();
            TextBox2.Text = DateTime.Today.AddDays(-1).ToShortDateString();
        }
    }

    protected void gvDailyReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDailyReport.PageIndex = e.NewPageIndex;
        gvDailyReport.DataSource = Session["DidCustStTraffic"];
        gvDailyReport.DataBind();
    }

    protected void cmdShow_Click(object sender, EventArgs e)
    {
        string sql = string.Empty;

        sql = "Select '**ALL**' BillingDate ,'**ALL**' Customer,'**ALL**' Country,'**ALL**' Region, '**ALL**' State,'**ALL**' Did,'**ALL**' Framework,sum(minutes) as [Minutes], "
            + "sum(BillableCalls) as [AnsweredCalls], sum(Attempts) as [Attempts],sum(Rejected) as [Rejected] "
            + "from ics..TrafficLogExt where Billingdate between '" + TextBox1.Text + " 00:00:00' and  '" + TextBox2.Text + " 23:59:59' and region <> 'UNKNOWN' "
            + "UNION "
            + "Select CONVERT(varchar,te.BillingDate,103) BillingDate, te.Customer, te.Country,   substring(te.Region,0,charindex('-',te.Region)-1) Region, te.State, te.Did, sd.Framework, sum(te.minutes) as [Minutes], "
            + "sum(te.BillableCalls) as [AnsweredCalls], sum(te.Attempts) as [Attempts],sum(te.Rejected) as [Rejected] "
            + "from ics..TrafficLogExt te left join ics..sourceDID sd on te.did = sd.did where te.Billingdate between '" + TextBox1.Text + " 00:00:00' and  '" + TextBox2.Text + " 23:59:59' and te.region <> 'UNKNOWN' group by CONVERT(varchar,te.BillingDate,103), te.Customer, te.Country,  substring(te.Region,0,charindex('-',te.Region)-1),te.State, te.Did, sd.Framework ORDER BY BillingDate";

        SqlCommand sqlC = new SqlCommand();
        sqlC.CommandText = sql;

        DataSet myDataset = RunQuery(sqlC);

        Session["DidCustStTraffic"] = myDataset;

        if (myDataset.Tables[0].Rows.Count > 0)
        {
            gvDailyReport.DataSource = myDataset.Tables[0];
            gvDailyReport.DataBind();

            cmdExcelExport.Visible = true;
            //cmdExportC.Visible = true;
            //Label4.Visible = false;
        }
        else
        {
            cmdExcelExport.Visible = false;
            //    //cmdExportC.Visible = false;
            //    //Label4.Visible = true;
        }

    }

    protected void cmdExport(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["DidCustStTraffic"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "DidCustStTraffic" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["ICSConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

        return resultsDataSet;
    }
}
