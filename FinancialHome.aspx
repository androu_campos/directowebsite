<%@ Page Language="C#" MasterPageFile="~/Master.master" StylesheetTheme="Theme1" AutoEventWireup="true" CodeFile="FinancialHome.aspx.cs" Inherits="FinancialHome" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table align="left" id="tblMain" cellpadding="6"  style=" height:666PX; position:absolute; top:140px; left:220px;">
        <tr>
            <td rowspan="4" style="width: 100px; height: 655px;" valign="top">
                <img border="0" src="Images/meccasummary.gif" width="264" height="24">
                <table border="0" width="264" id="tblSummary" cellspacing="0" cellpadding="0">
                    <tr height="18px">
                        <td width="72" height="20">
                            </td>
                        <td width="71" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">Latest</font></b>
                        </td>
                        <td width="113" colspan="2" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">Change</font></b></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72" align="left">
                            <font color="#004D88" style="font-size: 8pt; margin-left: 10px; margin-top: 5px;
                                margin-bottom: 5px; color: #004d88; font-family: Arial">Minutes</font>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">
                                <asp:Label ID="lblMinutesY" runat="server"></asp:Label>
                            </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblMinutesC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;</td>
                        <td width="19" align="center">
                            <asp:Image ID="Image4" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">Calls</font>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">
                                <asp:Label ID="lblCallsY" runat="server"></asp:Label>
                            </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#FF3300" style="font-size: 8pt; color: red; font-family: Arial">
                                    <asp:Label ID="lblCallsC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;</td>
                        <td width="19" align="center">
                            <asp:Image ID="Image5" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Profit"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblProfitY" runat="server"></asp:Label></font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblProfitC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;</td>
                        <td width="19" align="center">
                            <asp:Image ID="Image6" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">ASR</font>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblASRY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblASRC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;</td>
                        <td width="19" align="center">
                            <asp:Image ID="Image7" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">ACD</font>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblACDY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblACDC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;</td>
                        <td width="19" align="center">
                            <asp:Image ID="Image8" runat="server" /></td>
                    </tr>
                </table>
                <img border="0" src="Images/meccastatus.gif" width="264" height="24"><br />
                <br />
                <ul style="color: #004D88" type="square">
                    <li style="text-align: left;">
                        <asp:Label ID="lblBillingRunning" runat="server" CssClass="labelSteelBlue" Text="Billing Running"
                            Visible="False" Font-Bold="False" Font-Size="8pt" Font-Names="Arial" ForeColor="#666666"
                            Width="193px"></asp:Label>
                    </li>
                    <li style="text-align: left;">
                        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="labelSteelBlue" NavigateUrl="~/unrated.aspx"
                            Font-Size="8pt" Font-Names="Arial" ForeColor="#666666" Width="193px">[HyperLink1]</asp:HyperLink>
                    </li>
                    <li style="text-align: left;">
                        <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Arial" Font-Size="8pt"
                            ForeColor="#666666" NavigateUrl="~/Unknown.aspx" Width="193px">HyperLink</asp:HyperLink></li></ul>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td rowspan="4" style="width: 100px; height: 655px;" valign="top">
                <table border="0" width="264" id="table13" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top">
                            <table border="0" width="264" id="table15" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" bgcolor="#d5e3f0" colspan="3" height="20">
                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/top.gif" /></td>
                                </tr>
                                <tr>
                                    <td width="248" colspan="3" align="left" height="24" valign="bottom"><table style="width: 101%">
                                        <tr>
                                            <td style="width: 33%" valign="bottom">
                                                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Customers"></asp:Label></td>
                                            <td style="width: 37%" align="right" valign="bottom">
                                                <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Minutes"></asp:Label></td>
                                            <td align="right" valign="bottom" width="34%">
                                                <asp:Label ID="lblHeaderProfit1" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Profit"></asp:Label></td>
                                        </tr>
                                    </table>
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td id="IMG" width="248" colspan="3" align="center" valign="top">
                                        <asp:Label ID="lblCusN" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5">
                                        <asp:GridView RowStyle-Height="18px" ID="gdvCustomers" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Customer" HtmlEncode="False" InsertVisible="False" ShowHeader="False">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="220px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Minutes" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0.00}"
                                                    ItemStyle-Width="199px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:C}"
                                                    ItemStyle-Width="199px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                            <RowStyle Height="18px" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td width="282" colspan="3">
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td width="282" colspan="3" align="left" height="24">
                                        <table width="100%" align="center">
                                            <tr>
                                                <td style="width: 33%" valign="bottom">
                                                    <asp:Label ID="lblHeaderVendors" runat="server" Text="Vendors" CssClass="labelBlue8" Font-Bold="True"></asp:Label></td>
                                                <td align="right" valign="bottom" width="33%">
                                                    <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Minutes"></asp:Label><td valign="bottom" width="34%">
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                    <asp:Label ID="lblHeaderProfit2" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Profit"></asp:Label></tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="282" colspan="3" align="center">
                                        <img border="0" src="Images/line.gif" width="264" height="1"><br />
                                        <asp:Label ID="lblVenN" runat="server" ForeColor="Red" Text="Not available"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5">
                                        <asp:GridView ID="gdVendors" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Vendor">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="220px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Minutes" DataFormatString="{0:0,0.00}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="199px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" DataFormatString="{0:C}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="199px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td width="282" colspan="3">
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td width="282" colspan="3" align="left" height="24">
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 33%" valign="bottom">
                                                    <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Countries"></asp:Label></td>
                                                <td style="width: 33%" align="right" valign="bottom">
                                                    <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Minutes"></asp:Label></td>
                                                <td style="width: 34%" valign="bottom">
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                    <asp:Label ID="lblHeaderProfit3" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Profit"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="282" colspan="3" align="center">
                                        <img border="0" src="Images/line.gif" width="264" height="1"><br />
                                        <asp:Label ID="lblCouN" runat="server" ForeColor="Red" Text="Not available"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5">
                                        <asp:GridView ID="gdvCountry" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Country" ItemStyle-Width="199px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Minutes" DataFormatString="{0:0,0.00}" ItemStyle-Width="220px"
                                                    HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" DataFormatString="{0:C}" ItemStyle-Width="199px"
                                                    HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
            <td rowspan="4" style="width: 26px; height: 655px;" valign="top">
                <table border="0" width="264" id="table14" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <p style="margin-bottom: 2px">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/whatsnew.gif" />
                    </tr>
                    <tr>
                        <td bgcolor="#F7F7F7" align="left">
                            <ul style="color: #F6832D" type="square">
                                <li>
                                    <p style="margin-left: 20px; margin-top: 10px; margin-bottom: 10px">
                                        <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">Export
                                            to Excel Button</font>
                                </li>
                                <li>
                                    <p style="margin-left: 20px; margin-top: 10px; margin-bottom: 10px">
                                        <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">Navigation
                                            Bar</font>
                                </li>
                                <li>
                                    <p style="margin-left: 20px; margin-top: 10px; margin-bottom: 10px">
                                        <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">ASR
                                            and ACD in Masters Sales Log</font>
                                </li>
                                <li>
                                    <p style="margin-left: 20px; margin-top: 10px; margin-bottom: 10px">
                                        <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">Grouping
                                            Dates!</font>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 27px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#f7f7f7">
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <img border="0" height="24" src="Images/tutorial.gif" width="264" /><br />
                            &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            <asp:HyperLink ID="HyperLink3" runat="server" Font-Names="Arial" Font-Size="8pt"
                                Font-Underline="True" ForeColor="#666666" NavigateUrl="~/New Master Sales Log.ppt">How to use new Master Sales Log</asp:HyperLink></td>
                    </tr>
                    <tr>
                        <td>
                            <p align="center">
                                &nbsp;<p>
                                    &nbsp;</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="margin-bottom: 2px">
                                <img border="0" src="Images/support.gif" width="264" height="24">
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#F7FAFD">
                            <p style="margin-left: 20px; margin-top: 10px; margin-bottom: 10px;" align="left">
                                <a href="mailto:support@computer-tel.com" style="font-size: 8pt; color: black;
                                    font-family: Arial">rafael@computer-tel.com</a><br>
                                <br>
                                <font color="#004D88"><b style="font-size: 8pt; color: #004d88; font-family: Arial">
                                    Rafael MSN
                                    <br>
                                </b></font><a href="mailto:rmanja@hotmail.com" style="font-size: 8pt; color: black;
                                    font-family: Arial">rmanja@hotmail.com</a><br>
                                <br>
                                <font color="#004D88"><b style="font-size: 8pt; color: #004d88; font-family: Arial">
                                    Octavio MSN<br>
                                </b></font><a href="mailto:xduvox@hotmail.com" style="font-size: 8pt; color: black;
                                    font-family: Arial">xduvox@hotmail.com</a><br>
                                <br>
                                <font color="#004D88"><b style="font-size: 8pt; color: #004d88; font-family: Arial">
                                    Fernando MSN<br>
                                </b></font><a href="mailto:satriodie@hotmail.com" style="font-size: 8pt; color: black;
                                    font-family: Arial">satriodie@hotmail.com </a>
                                <br>
                                &nbsp;</td>
                    </tr>
                   
                </table>
            </td>
        </tr>
          
    </table>


    <table visible="false" width="100%" style="position: absolute; top: 750px; left: 230px;">
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="right" colspan="3">
                            <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        
    </table>
    

</asp:Content>

