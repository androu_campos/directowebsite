<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PrefixMonitor.aspx.cs" Inherits="PrefixMonitor" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <table>
        <tr>
            <td style="width: 100px; height: 25px">
                <asp:Label ID="Label1" runat="server" Text="Prefix :" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px; height: 25px">
                Activate option</td>
            <td style="width: 100px; height: 25px">
                <asp:TextBox ID="txtPrefix" runat="server" CssClass="dropdown"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label2" runat="server" Text="Attempts  >" CssClass="labelBlue8" Width="53px"></asp:Label></td>
            <td style="width: 100px">
                <asp:CheckBox ID="chAttempts" runat="server" CssClass="labelBlue8" /></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtAttempts" runat="server" CssClass="dropdown"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label3" runat="server" Text="ASR <   " CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:CheckBox ID="chASR" runat="server" CssClass="labelBlue8" /></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtASR" runat="server" CssClass="dropdown"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px; height: 22px">
                <asp:Label ID="Label5" runat="server" Text="ABR <" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px; height: 22px">
                <asp:CheckBox ID="chABR" runat="server" CssClass="labelBlue8" /></td>
            <td style="width: 100px; height: 22px">
                <asp:TextBox ID="txtABR" runat="server" CssClass="dropdown"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label4" runat="server" Text="ACD <" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:CheckBox ID="chACD" runat="server" CssClass="labelBlue8" /></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtACD" runat="server" CssClass="dropdown"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:Button ID="btnViewReport" runat="server" OnClick="btnViewReport_Click" Text="View Report" CssClass="boton" /></td>
        </tr>
     <tr>
         <td style="width: 100px">
         </td>
         <td style="width: 100px">
         </td>
         <td style="width: 100px">
             <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="Nothing Found"
                 Visible="False"></asp:Label></td>
     </tr>
    </table>
    <asp:ScriptManager id="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <asp:GridView ID="gvData" runat="server" Font-Size="8pt" Font-Names="Arial">
    
    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:Timer id="Timer1" runat="server" Interval="900000">
    </asp:Timer>
    <br />
    &nbsp;
</asp:Content>

