using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;

public partial class NPLookup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
        }
        else
        {
            try
            {
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
                SqlCommand command = null;
                string sql = string.Empty;
                SqlDataReader readerCustomers = null;
                SqlDataReader readerVendors = null;
                DataSet dsPendingReport = null;
                SqlDataAdapter dbAdapter = null;

                sql = "select case when providername like 'ICS%' then 'ICS' else providername end from cdrdb.dbo.providerip where [type] = 'C' and providername not in (select customer from cdrdb.dbo.custbrokerrelcc) ";
                sql += "union ";
                sql += "select 'TALKTEL' ";
                sql += "union ";
                sql += "select '**ALL**' ";
                

                connection.Open();
                command = new SqlCommand(sql, connection);
                readerCustomers = command.ExecuteReader();

                while (readerCustomers.Read())
                {
                    dpdCustomer.Items.Add(readerCustomers.GetValue(0).ToString());
                }

                readerCustomers.Close();

                sql = "SELECT DISTINCT ProviderName from ProviderIP WHERE Type like 'V' UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT 'NATIONAL ALL' from ProviderIP as ProviderName ";

                command = new SqlCommand(sql, connection);
                readerVendors = command.ExecuteReader();

                while (readerVendors.Read())
                {
                    dpdVendor.Items.Add(readerVendors.GetValue(0).ToString());
                }

                readerVendors.Close();
                

                dpdTrafficSource.Items.Add("Todays Traffic");
                dpdTrafficSource.Items.Add("Yesterdays Traffic");

                sql = "select ReportDate, Customer, Vendor, ReportType, Status from CDRDB.dbo.np_lookup_reports where Pendient = 1 ";
                command = new SqlCommand(sql, connection);
                command.CommandType = CommandType.Text;
                dsPendingReport = new DataSet();
                dbAdapter = new SqlDataAdapter();
                dbAdapter.SelectCommand = command;

                dbAdapter.Fill(dsPendingReport);
                gvPendingReport.DataSource = dsPendingReport;
                gvPendingReport.DataBind();

                gvPendingReport.Visible = true;

                connection.Close();
            }
            catch (Exception ex)
            {
                string errormessage = ex.Message.ToString();
            }
        }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        string customer = string.Empty;
        string vendor = string.Empty;
        string source = string.Empty;
        string sql = string.Empty;

        string flag = string.Empty;

        customer = dpdCustomer.SelectedValue;
        vendor = dpdVendor.SelectedValue;
        source = dpdTrafficSource.SelectedValue;

        if (customer == "**ALL**" && vendor == "**ALL**")
        {
            Label4.Text = "You cannot choose ALL customers and ALL vendors...";
            Label4.Visible = true;
        }
        else
        {


            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
            connection.Open();

            SqlCommand command = new SqlCommand("sp_np_lookup_isavailable", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlParameter isAvailable = command.Parameters.Add("@isAvailable", SqlDbType.Int);
            isAvailable.Direction = ParameterDirection.Output;

            command.ExecuteNonQuery();


            flag = command.Parameters["@isAvailable"].Value.ToString();

            if (flag == "0")
            {
                sql = "insert into cdrdb.dbo.np_lookup_reports (username, reportdate, customer, vendor, reporttype,pendient,status) values (''," + "getdate(),'" + customer +
                    "','" + vendor + "','" + source + "',1,'Pending')";

                command = new SqlCommand(sql, connection);
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                Label4.Text = "Report will be delivered soon";
                Label4.Visible = true;
            }
            else
            {
                Label4.Text = "Another report is running, please try again later.";
                Label4.Visible = true;
            }

            connection.Close();
        }
    }
}
