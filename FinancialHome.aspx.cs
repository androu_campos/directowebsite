using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class FinancialHome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {


            //if (Session["Rol"].ToString() == "Financial")
            //{
            //    this.Response.Redirect("F"
            //}






            MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adp = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
            MyDataSetTableAdapters.Billing_StatusDataTable tbl = adp.GetData();

            SummaryTableAdapters.QrysAdp adpL = new SummaryTableAdapters.QrysAdp();
            int lastId = Convert.ToInt32(adpL.GetLastIdqry());
            int lastId1 = 0, lastId2 = 0;
            /**/
            //TODAY
            SummaryTableAdapters.SummaryAdp adpD = new SummaryTableAdapters.SummaryAdp();
            Summary.SummaryDataTable tblD = adpD.GetDataByDate(Convert.ToDateTime(DateTime.Now.ToString()));//Today
            if (tblD.Rows.Count > 0)
            {
                lastId1 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            else
            {
                tblD = adpD.GetDataByDate(Convert.ToDateTime(DateTime.Now.AddDays(-1).ToShortDateString()));//Today
                lastId1 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            /**/

            /**/
            //7 DAY BEFORE

            tblD = adpD.GetDataByDate(DateTime.Now.AddDays(-7));//Today
            if (tblD.Rows.Count > 0)
            {
                lastId2 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            else
            {
                tblD = adpD.GetDataByDate(Convert.ToDateTime(DateTime.Now.AddDays(-8).ToShortDateString()));//Today
                lastId2 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            /**/


            if (tbl.Rows.Count > 0)//Billing running
            {
                lblBillingRunning.Visible = true;
                lblBillingRunning.Text = "Billing Running !!!";
                lblBillingRunning.ForeColor = System.Drawing.Color.Red;

                HyperLink1.Visible = false;
                HyperLink2.Visible = false;

                string yesterday = System.DateTime.Now.AddDays(-1).ToShortDateString();
                string ayesterday = System.DateTime.Now.AddDays(-2).ToShortDateString();
                //  Get Dates of Yesterday//Today
                SummaryTableAdapters.SummaryAdp adpS = new SummaryTableAdapters.SummaryAdp();
                Summary.SummaryDataTable tblS = adpS.GetDataById(lastId1);

                string CMinutes = tblS.Rows[0]["CMinutes"].ToString();
                lblMinutesY.Text = Util.getNumberFormat(CMinutes, 1);


                string Calls = tblS.Rows[0]["Calls"].ToString();
                lblCallsY.Text = Util.getNumberFormat(Calls, 0);

                string Profit = tblS.Rows[0]["Profit"].ToString();
                lblProfitY.Text = Util.getNumberFormat(Profit, 1);


                lblASRY.Text = tblS.Rows[0]["ASR"].ToString() + "%";
                lblACDY.Text = tblS.Rows[0]["CACD"].ToString();

                float minutesY = (float)Convert.ToSingle(tblS.Rows[0]["CMinutes"].ToString());
                int callsY = Convert.ToInt32(tblS.Rows[0]["Calls"].ToString());
                float profitY = (float)Convert.ToSingle(tblS.Rows[0]["Profit"].ToString());
                int asrY = Convert.ToInt32(tblS.Rows[0]["ASR"].ToString());
                float acdY = (float)Convert.ToSingle(tblS.Rows[0]["CACD"].ToString());

                //Get Dates of PRE-Yesterday
                Summary.SummaryDataTable tblA = adpS.GetDataById(lastId2);


                float minutesA = (float)Convert.ToSingle(tblA.Rows[0]["CMinutes"].ToString());
                int callsA = Convert.ToInt32(tblA.Rows[0]["Calls"].ToString());
                float profitA = (float)Convert.ToSingle(tblA.Rows[0]["Profit"].ToString());
                int asrA = Convert.ToInt32(tblA.Rows[0]["ASR"].ToString());
                float acdA = (float)Convert.ToSingle(tblA.Rows[0]["CACD"].ToString());

                //column change


                string MinutesC = Convert.ToString(minutesY - minutesA).Replace("-", "");
                int isfloat = Convert.ToInt32(MinutesC.IndexOf("."));
                if (isfloat < 0)
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 0);
                }
                else
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 1);
                }
                lblMinutesC.Text = Util.getNumberFormat(MinutesC, 1);
                lblCallsC.Text = Util.getNumberFormat((Convert.ToString(callsY - callsA).Replace("-", "")), 0);
                lblProfitC.Text = Util.getNumberFormat(Convert.ToString(profitY - profitA).Replace("-", ""), 1);
                lblASRC.Text = Convert.ToString(((asrA - asrY) * 100) / asrY).Replace("-", "") + "%";
                lblACDC.Text = lblACDC.Text = Convert.ToString((acdY - acdA)).Replace("-", "");

                // en column change

                if (minutesY < minutesA)                //MINUTOS
                {
                    Image4.ImageUrl = "./Images/roja.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image4.ImageUrl = "./Images/verde.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Green;
                }

                if (callsY < callsA)                     //LLAMADAS
                {
                    Image5.ImageUrl = "./Images/roja.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image5.ImageUrl = "./Images/verde.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Green;
                }
                if (profitY < profitA)                      //PROFIT
                {
                    Image6.ImageUrl = "./Images/roja.gif";
                    lblProfitC.ForeColor = System.Drawing.Color.Red;

                }
                else
                {
                    Image6.ImageUrl = "./Images/verde.gif";
                    lblProfitC.ForeColor = System.Drawing.Color.Green;

                }
                if (asrY < asrA)                            //ASR
                {
                    Image7.ImageUrl = "./Images/roja.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image7.ImageUrl = "./Images/verde.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Green;
                }

                if (acdY < acdA)
                {
                    Image8.ImageUrl = "./Images/roja.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image8.ImageUrl = "./Images/verde.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Green;
                }

                lblCusN.Visible = true;
                lblVenN.Visible = true;
                lblCouN.Visible = true;

                //**//
                if (Session["Rol"].ToString() == "NOC" || Session["Rol"].ToString() == "NOC National")
                {

                    lblHeaderProfit1.Visible = false;
                    lblHeaderProfit2.Visible = false;
                    lblHeaderProfit3.Visible = false;


                    TableItemStyle estilo = new TableItemStyle();
                    estilo.CssClass = "white";

                    gdvCountry.Columns[2].Visible = false;
                    lblHeaderProfit1.Text = "Minutes";
                    lblHeaderProfit1.Visible = true;
                    Label7.Visible = false;

                    gdVendors.Columns[2].Visible = false;
                    lblHeaderProfit2.Text = "Minutes";
                    lblHeaderProfit2.Visible = true;
                    Label2.Visible = false;

                    gdvCustomers.Columns[2].Visible = false;
                    lblHeaderProfit3.Text = "Minutes";
                    lblHeaderProfit3.Visible = true;

                    Label4.Visible = false;
                    lblProfitY.Visible = false;
                    lblProfitC.Visible = false;
                    Image6.Visible = false;
                    Label5.Visible = false;
                }

            }//FIN DEL BILLING RUNNING
            else
            {

                /*                                         MECCA  SUMMARY                                       */
                lblCusN.Visible = false;
                lblVenN.Visible = false;
                lblCouN.Visible = false;
                string yesterday = System.DateTime.Now.AddDays(-1).ToShortDateString();
                string ayesterday = System.DateTime.Now.AddDays(-2).ToShortDateString();



                //  Get Dates of Yesterday
                SummaryTableAdapters.SummaryAdp adpS = new SummaryTableAdapters.SummaryAdp();
                Summary.SummaryDataTable tblS = adpS.GetDataById(lastId1);


                string CMinutes = tblS.Rows[0]["CMinutes"].ToString();
                lblMinutesY.Text = Util.getNumberFormat(CMinutes, 1);
                string Calls = tblS.Rows[0]["Calls"].ToString();
                lblCallsY.Text = Util.getNumberFormat(Calls, 0);
                string Profit = tblS.Rows[0]["Profit"].ToString();
                lblProfitY.Text = Util.getNumberFormat(Profit, 1);
                lblASRY.Text = tblS.Rows[0]["ASR"].ToString() + "%";
                lblACDY.Text = tblS.Rows[0]["CACD"].ToString();


                float minutesY = (float)Convert.ToSingle(tblS.Rows[0]["CMinutes"].ToString());
                int callsY = Convert.ToInt32(tblS.Rows[0]["Calls"].ToString());
                float profitY = (float)Convert.ToSingle(tblS.Rows[0]["Profit"].ToString());
                int asrY = Convert.ToInt32(tblS.Rows[0]["ASR"].ToString());
                float acdY = (float)Convert.ToSingle(tblS.Rows[0]["CACD"].ToString());

                //Get Dates of PRE-Yesterday
                Summary.SummaryDataTable tblA = adpS.GetDataById(lastId2);


                float minutesA = (float)Convert.ToSingle(tblA.Rows[0]["CMinutes"].ToString());
                int callsA = Convert.ToInt32(tblA.Rows[0]["Calls"].ToString());
                float profitA = (float)Convert.ToSingle(tblA.Rows[0]["Profit"].ToString());
                int asrA = Convert.ToInt32(tblA.Rows[0]["ASR"].ToString());
                float acdA = (float)Convert.ToSingle(tblA.Rows[0]["CACD"].ToString());



                //column change
                string MinutesC = Convert.ToString(minutesY - minutesA).Replace("-", "");
                int isfloat = Convert.ToInt32(MinutesC.IndexOf("."));
                if (isfloat < 0)
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 0);
                }
                else
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 1);
                }



                lblCallsC.Text = Util.getNumberFormat((Convert.ToString(callsY - callsA).Replace("-", "")), 0);
                lblProfitC.Text = Util.getNumberFormat(Convert.ToString(profitY - profitA).Replace("-", ""), 1);
                lblASRC.Text = Convert.ToString(((asrA - asrY) * 100) / asrY).Replace("-", "") + "%";
                lblACDC.Text = Convert.ToString((acdY - acdA)).Replace("-", "");



                // en column change

                if (minutesY < minutesA)                //MINUTOS
                {
                    Image4.ImageUrl = "./Images/roja.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image4.ImageUrl = "./Images/verde.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Green;
                }

                if (callsY < callsA)                     //LLAMADAS
                {
                    Image5.ImageUrl = "./Images/roja.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image5.ImageUrl = "./Images/verde.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Green;
                }
                if (profitY < profitA)                      //PROFIT
                {
                    Image6.ImageUrl = "./Images/roja.gif";
                    lblProfitC.ForeColor = System.Drawing.Color.Red;

                }
                else
                {
                    Image6.ImageUrl = "./Images/verde.gif";
                    lblProfitC.ForeColor = System.Drawing.Color.Green;

                }
                if (asrY < asrA)                            //ASR
                {
                    Image7.ImageUrl = "./Images/roja.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image7.ImageUrl = "./Images/verde.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Green;
                }

                if (acdY < acdA)
                {
                    Image8.ImageUrl = "./Images/roja.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image8.ImageUrl = "./Images/verde.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Green;
                }

                lblBillingRunning.Visible = true;
                lblBillingRunning.Text = "Billing not Runing";

                MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter adpU = new MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter();
                MyDataSetTableAdapters.unratedMinutes_DataTable tblU = adpU.GetData();
                if (tblU.Rows.Count > 0)//There are unrated minutes?
                {
                    HyperLink1.Text = "There are unrated minutes in MECCA!";
                    HyperLink1.ForeColor = System.Drawing.Color.Red;

                }
                else
                {
                    HyperLink1.Text = "All minutes are rated in MECCA!";
                }


                MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter adpUN = new MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter();
                MyDataSetTableAdapters.unratedMinutes_DataTable tblUN = adpUN.GetData();
                if (tblUN.Rows.Count > 0)//There are unrated minutes?
                {
                    HyperLink1.Text = "There are unrated minutes in MECCA!";
                    HyperLink1.ForeColor = System.Drawing.Color.Red;

                }
                else
                {
                    HyperLink1.Text = "All minutes are rated in MECCA!";
                }


                MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter adpU2 = new MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter();
                MyDataSetTableAdapters.UnknownIDs2DataTable tblU2 = adpU2.GetData();

                if (tblU2.Rows.Count > 0)
                {
                    HyperLink2.Text = "There are unmatched IDs in MECCA!";
                    HyperLink2.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    HyperLink2.Text = "All IDs are matched";
                }



                string yest = DateTime.Today.AddDays(-1).ToShortDateString();
                MyDataSetTableAdaptersTableAdapters.trafficTopAdp adpT = new MyDataSetTableAdaptersTableAdapters.trafficTopAdp();
                gdvCustomers.DataSource = adpT.GetData(Convert.ToDateTime(yest));
                gdvCustomers.DataBind();

                MyDataSetTableAdaptersTableAdapters.trafficVtopAdp adpV = new MyDataSetTableAdaptersTableAdapters.trafficVtopAdp();
                gdVendors.DataSource = adpV.GetData(Convert.ToDateTime(yest));
                gdVendors.DataBind();

                MyDataSetTableAdaptersTableAdapters.trafficTopCAdp adpC = new MyDataSetTableAdaptersTableAdapters.trafficTopCAdp();
                gdvCountry.DataSource = adpC.GetData(Convert.ToDateTime(yest));
                gdvCountry.DataBind();

                MyDataSetTableAdaptersTableAdapters.UnknownIDs1TableAdapter adpUn = new MyDataSetTableAdaptersTableAdapters.UnknownIDs1TableAdapter();
                MyDataSetTableAdapters.UnknownIDs1DataTable tblUn = adpUn.GetData();

            }


            if (Session["Rol"].ToString() == "NOC" || Session["Rol"].ToString() == "NOC National")
            {

                lblHeaderProfit1.Visible = false;
                lblHeaderProfit2.Visible = false;
                lblHeaderProfit3.Visible = false;


                TableItemStyle estilo = new TableItemStyle();
                estilo.CssClass = "white";

                gdvCountry.Columns[2].Visible = false;
                lblHeaderProfit1.Text = "Minutes";
                lblHeaderProfit1.Visible = true;
                Label7.Visible = false;

                gdVendors.Columns[2].Visible = false;
                lblHeaderProfit2.Text = "Minutes";
                lblHeaderProfit2.Visible = true;
                Label2.Visible = false;

                gdvCustomers.Columns[2].Visible = false;
                lblHeaderProfit3.Text = "Minutes";
                lblHeaderProfit3.Visible = true;

                Label4.Visible = false;


                lblProfitY.Visible = false;
                lblProfitC.Visible = false;
                Image6.Visible = false;
                Label5.Visible = false;

            }


        }
        catch (Exception ex)
        {
            string message = ex.Message.ToString();
        }

    }
}
