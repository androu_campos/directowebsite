using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using RKLib.ExportData;


public partial class Default7 : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
        //Muestra los datos del Recent,Today y Yesterday segun los parametros de la URL enviados por Monitor.aspx
    {
        try
        {
            string customer = Request.QueryString[0];
            string vendor = Request.QueryString[1];
            string originationIP = Request.QueryString[2];
            string terminationIP = Request.QueryString[3];
            string country = Request.QueryString[4];
            string region = Request.QueryString[5];
            string type = Request.QueryString[6];
            string clas = Request.QueryString[7];
            string lmc = Request.QueryString[8];
            string source = Request.QueryString[9];
            string gateway = Request.QueryString[10];
            string dateTo = Request.QueryString[11];
            string dateFrom = Request.QueryString[12];
            string aux = string.Empty;

            //
            //
            StringBuilder select = new StringBuilder();
            select.Append("SELECT ");

            StringBuilder where = new StringBuilder();
            where.Append("WHERE ");

            string whereR = "WHERE ";

            string whereT = "WHERE";

            StringBuilder group = new StringBuilder();
            group.Append("GROUP BY ");

            StringBuilder union = new StringBuilder();
            union.Append(" UNION SELECT ");

            StringBuilder qry = new StringBuilder();
            StringBuilder order = new StringBuilder();
            order.Append(" Order by ");
            int ord = 0;

            select = select.Append("CONVERT(VARCHAR(11) ,Billingdate, 101) as BillingDate, ");
            union.Append("'** ALL **' as BillingDate,");
            group.Append("BillingDate,");

            //CUSTOMER
            if (customer != "** NONE **")
            {   
                    select = select.Append("Customer,");

                    where.Append("Customer like 'ICS-UNLIMITED-5%' AND ");
                    
                    group.Append("Customer,");
                    union.Append("'**ALL**' as Customer,");
                    ord = ord + 1;
                

            }
            //VENDOR
            if (vendor != "** NONE **")
            {
                select = select.Append("Vendor,");

                if(vendor == "NATIONAL ALL")
                    where = where.Append("Vendor in ('NATIONAL','NATIONAL-ISA', 'NATIONAL-OFF','NATIONAL-TLL','NATIONAL-NEXT','NATIONAL-TELULAR') AND ");
                else
                    where = where.Append("Vendor like '" + vendor + "' AND ");
                group.Append("Vendor,");
                union.Append("'**ALL**' as Vendor,");                
            }
            //OriginationIP
            if (originationIP != "** NONE **")
            {
                select = select.Append("OriginationIP,");
                where = where.Append("OriginationIP like '" + originationIP + "' AND ");
                group.Append("OriginationIP,");
                union.Append("'' as OriginationIP,");
            }
            //TerminationIP
            if (terminationIP != "** NONE **")
            {
                select = select.Append("TerminationIP,");
                where = where.Append("TerminationIP like '" + terminationIP + "' AND ");
                group.Append("TerminationIP,");
                union.Append("'' as TerminationIP,");

            }
            //Country
            if (country != "** NONE **")
            {
                select = select.Append("Country,");
                where = where.Append("Country like '" + country + "' AND ");
                group.Append("Country,");
                union.Append("'' as Country,");
            }
            //Region
            if (region != "** NONE **")
            {
                select = select.Append("Region,");
                where = where.Append("Region like '" + region + "' AND ");
                group.Append("Region,");
                union.Append("'' as Region,");
            }
            //Type
            if (type != "** NONE **")
            {
                select = select.Append("Type,");
                where = where.Append("Type like '" + type + "' AND ");
                group.Append("Type,");
                union.Append("'' as Type,");
            }
            //Class
            if (clas != "** NONE **")
            {
                select = select.Append("Class,");
                where = where.Append("Class like '" + clas + "' AND ");
                group.Append("Class,");
                union.Append("'' as Class,");

            }
            //LMC
            if (lmc != "** NONE **" && lmc != "**ALL**")
            {
                select = select.Append("LMC,");
                //select = select.Append("'" + lmc + "' as LMC, ");
                //where = where.Append("LMC in (SELECT LMC FROM MECCA2..LMC WHERE ENTERPRISE = '" + lmc + "') AND ");
                where = where.Append("LMC like '" + lmc + "' AND ");
                group.Append("LMC,");
                union.Append("'' as LMC,");

            }

           
            
            //Source MSW
            if (source != "** NONE **")
            {
                string tmp = string.Empty;
                tmp = where.ToString();
                select = select.Append("Source,");

                whereR = where.ToString() + "Source like '" + source + "CTMP%' AND ";
                whereT = where.ToString() + "Source like '" + source + "CTMP%' AND ";
                
                where = where.Append("Source like '" + source + "' AND ");
                                                                
                group.Append("Source,");
                union.Append("'' as Source,");
            }
            //Gateway
            if (gateway != "** NONE **")
            {
                select = select.Append("VendorID,");
                where = where.Append("VendorID like '" + gateway + "' AND ");
                group.Append("VendorID,");
                union.Append("'' as VendorID,");
            }


            if (lmc == "**ALL**")
            {
                select = select.Append("LMC,");
                group.Append("LMC,");
                union.Append("'' as LMC,");
            }

            

            select = select.Append("DialedNumber, ");
            union.Append("'' as DialedNumber,");
            group.Append("DialedNumber,");

            select = select.Append("Anumber, ");
            union.Append("'' as Anumber,");
            group.Append("Anumber,");

            select = select.Replace("**ALL**", "%");
            where = where.Replace("**ALL**", "%");
            string selectS = select.ToString();
            string whereW = where.ToString();
            string groupG = group.ToString();
            //string orderO = order.ToString();
            string unionU = union.ToString();


            int lst = whereW.LastIndexOf("AND");//where
            whereW = whereW.Remove(lst, whereW.Length - lst);

            

            if (groupG.Length < 10)
                groupG = " ";
            else
            {
                lst = groupG.LastIndexOf(",");//group
                groupG = groupG.Remove(lst, groupG.Length - lst);
            }

            lst = unionU.LastIndexOf(",");//union
            unionU = unionU.Remove(lst, unionU.Length - lst);

            select.Replace(select.ToString(), selectS);
            where.Replace(where.ToString(), whereW);
            group.Replace(group.ToString(), groupG);
            union.Replace(union.ToString(), unionU);

            select.Append("sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,CAST(AVG(PDD) as int) AS PDD ");
            union.Append(",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,CAST(AVG(PDD) as int) AS PDD ");

            //Recent
            whereR = whereR.Replace("**ALL**CTMP%", "%CTMP");
            whereR = whereR.Replace("**ALL**", "%");
            lst = whereR.LastIndexOf("AND");

            if (lst < 0)
                whereR = where.ToString();
            else
                whereR = whereR.Remove(lst, whereR.Length - lst);

            if(Session["hometype"].ToString() == "W")
            {
                whereR += " AND Routetype = 'W' ";
            }
            else if (Session["hometype"].ToString() == "R")
            {
                whereR += " AND Routetype = 'R' ";
            }

            if (dateTo.Length < 10)
            {
                if (dateTo.Length == 9)
                {
                    char[] vectorchar = dateTo.ToCharArray();

                    if (vectorchar[1].ToString() == "_")
                    {
                        dateTo = "0" + dateTo;
                        aux = dateTo;
                    }
                    if (vectorchar[2].ToString() == "_")
                    {
                        for (int i = 0; i < dateTo.Length; i++)
                        {
                            if (i == 3)
                            {
                                aux += "0";
                            }
                            aux += vectorchar[i];
                        }

                    }

                    dateTo = aux.Substring(6, 4);
                    dateTo += aux.Substring(0, 2);
                    dateTo += aux.Substring(3, 2);
                }
                else if (dateTo.Length == 8)
                {
                    dateTo = "0" + dateTo;
                    aux = dateTo;
                    dateTo = aux.Substring(5, 4);
                    dateTo += aux.Substring(0, 2);
                    dateTo += "0" + aux.Substring(3, 1);
                }

            }
            else
            {
                dateTo = dateTo.Substring(6, 4) + dateTo.Substring(0, 2) + dateTo.Substring(3, 2);
            }
            if (dateFrom.Length < 10)
            {
                if (dateFrom.Length == 9)
                {
                    char[] vectorchar = dateFrom.ToCharArray();

                    if (vectorchar[1].ToString() == "_")
                    {
                        dateFrom = "0" + dateFrom;
                        aux = dateFrom;
                    }
                    if (vectorchar[2].ToString() == "_")
                    {
                        for (int i = 0; i < dateFrom.Length; i++)
                        {
                            if (i == 3)
                            {
                                aux += "0";
                            }
                            aux += vectorchar[i];
                        }

                    }
                    dateFrom = aux.Substring(6, 4);
                    dateFrom += aux.Substring(0, 2);
                    dateFrom += aux.Substring(3, 2);
                }
                else if (dateFrom.Length == 8)
                {
                    dateFrom = "0" + dateFrom;
                    aux = dateFrom;
                    dateFrom = aux.Substring(5, 4);
                    dateFrom += aux.Substring(0, 2);
                    dateFrom += "0" + aux.Substring(3, 1);
                }

            }
            else
            {
                dateFrom = dateFrom.Substring(6, 4) + dateFrom.Substring(0, 2) + dateFrom.Substring(3, 2);
            }

            
            

            whereR += " AND BillingDate BETWEEN '" + dateFrom +"' AND '" + dateTo + "' ";


            qry.Append(select.ToString() + " from OpSheetYH_Top3 " + whereR + group.ToString() + union + " from OpSheetYH_Top3 " + whereR.ToString() + " ORDER BY TotalMinutes DESC");

            DataSet mydataSet1 = RunQuery(qry, "MECCA2ConnectionString");
            gdvHistory.DataSource = mydataSet1.Tables[0];
            gdvHistory.DataBind();
            Session["AMTop3History"] = mydataSet1;

            lblHeader.Text = customer;
            lblBilling.Visible = false;
        }
        catch (Exception ex)
        {
            lblBilling.Visible = true;
            string error = ex.Message.ToString();

        }
    }

    protected void exportHistory(object sender, EventArgs e)
    {
        try
        {

            DataTable dtEmployee = ((DataSet)Session["AMTop3History"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "UnlimitedHistory" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }

    private System.Data.DataSet RunQuery(StringBuilder qry, string StringConnection)
    {

        System.Data.SqlClient.SqlCommand sqlQuery = new System.Data.SqlClient.SqlCommand();
        sqlQuery.CommandText = qry.ToString();


        string connectionString = ConfigurationManager.ConnectionStrings
        [StringConnection].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

    protected void gdvHistroy_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvHistory.PageIndex = e.NewPageIndex;
        DataSet ds = (DataSet)Session["AMTop3History"];
        gdvHistory.DataSource = ds;
        gdvHistory.DataBind();
    }

    

}
