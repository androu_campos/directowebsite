<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="RunBilling.aspx.cs" Inherits="RunBilling" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <br />
    <br />
    <asp:Button ID="btnRunBilling" runat="server" CssClass="boton" OnClick="btnRunBilling_Click"
        Text="Run    Billing" Width="830px" /><br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>" SelectCommand="SELECT flag as Time FROM BillSchedule">
    </asp:SqlDataSource>
    <br />
    <br />
    <br />
    <br />
    &nbsp;<asp:GridView ID="gvGood" runat="server" Font-Names="Arial" Font-Size="8pt"
        Width="282px">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Font-Size="8pt"
            Height="40px" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>

