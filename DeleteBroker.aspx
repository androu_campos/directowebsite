<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="DeleteBroker.aspx.cs" Inherits="DeleteBroker" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label1" runat="server" Text="Delete Broker"></asp:Label></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label2" runat="server" Text="Select Broker:"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdBroker" runat="server" CssClass="dropdown" DataTextField="Broker">
                </asp:DropDownList></td>
            <td style="width: 100px">
                <asp:Button ID="cmdDelete" runat="server" CssClass="boton" OnClick="cmdDelete_Click"
                    Text="Delete" /></td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:Label ID="lblDelete" runat="server" Text="Label"></asp:Label></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
                    SelectCommand="SELECT DISTINCT Broker FROM Broker&#13;&#10;UNION &#13;&#10;SELECT '**NULL** ' as Broker FROM Broker">
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

