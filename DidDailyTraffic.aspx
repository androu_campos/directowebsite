<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="DidDailyTraffic.aspx.cs" Inherits="DidDailyTraffic" Title="DIRECTO - Connections Worldwide" Theme="Theme1" EnableTheming="true" StylesheetTheme="Theme1"%>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager id="ScriptManager1" runat="server"></asp:ScriptManager>
     
    <table>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Font-Bold="True" Font-Size="10pt" Text="DID Traffic Log"></asp:Label>
            </td>            
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="from:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" CssClass="labelSteelBlue"></asp:TextBox>
            </td>
            <td>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="to:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" CssClass="labelSteelBlue"></asp:TextBox>
            </td>
            <td>
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" />
            </td>
        </tr>   
            
    </table>
    <br />
    <table>
        <tr>
            <td>
                 <asp:Button ID="cmdShow" runat="server" CssClass="boton" OnClick="cmdShow_Click" Text="View report" />
            </td>
            <td>
                <asp:Button ID="cmdExcelExport" runat="server" CssClass="boton" OnClick="cmdExport" Text="Export to Excel" Visible="False" Width="91px" />
            </td>
        </tr> 
    </table>
    <br />
    <table>
        <tr>
            <td>
                <asp:GridView ID="gvDailyReport" runat="server" AllowPaging="True" Font-Names="Arial" Font-Size="8pt" OnPageIndexChanging="gvDailyReport_PageIndexChanging" PageSize="30" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" HorizontalAlign="Center" />
                    <AlternatingRowStyle BackColor="White" />
                    
                    <Columns>
                    
                        <asp:BoundField DataField="BillingDate" HeaderText="BillingDate" HtmlEncode="False"  >
                        <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    
                        <asp:BoundField DataField="Customer" HeaderText="Customer" HtmlEncode="False"  >
                        <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="Vendor" HeaderText="Vendor" HtmlEncode="False"  >
                        <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                        
                        <asp:BoundField DataField="Country" HeaderText="Country">
                        <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="Region" HeaderText="Region">
                        <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Center" Width="100px" />
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField> 
                        
                        <asp:BoundField DataField="State" HeaderText="Sate">
                        <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Center" Width="100px" />
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField> 
                        
                        <asp:BoundField DataField="Minutes" HeaderText="Minutes" DataFormatString="{0:f2}">
                        <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Center" Width="100px" />
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="AnsweredCalls" HeaderText="AnsweredCalls" >
                        <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Center" Width="100px" />
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="Attempts" HeaderText="Attempts" >
                        <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Center" Width="100px" />
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                                               
                        
                        <asp:BoundField DataField="Rejected" HeaderText="RejectedCalls" >
                        <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Center" Width="100px" />
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>

    <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="TextBox1" runat="server" PopupButtonID="Image1">
    </cc1:CalendarExtender>

    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TextBox2" PopupButtonID="Image2">
    </cc1:CalendarExtender>
</asp:Content>

