<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MonitorMobile.aspx.cs" Inherits="MonitorMobile" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <TABLE align=left>
            <TBODY>
                <TR>
                    <TD vAlign=top align=left>
                        <asp:Label id="Label1" runat="server" Text="Advanced Query Monitor Mobile" CssClass="labelTurk"></asp:Label>
                    </TD>
                </TR>
                <TR>
                    <TD vAlign=top align=left>
                        &nbsp;
                        <TABLE id="tblQuery">
                            <TBODY>
                                <TR>
                                    <TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label id="Label5" runat="server" Text="Customer" CssClass="labelBlue8"></asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                        <asp:DropDownList id="dpdCustomer" runat="server" CssClass="dropdown"></asp:DropDownList>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                    </TD>
                                </TR>
                                <TR>
                                    <TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label id="Label6" runat="server" Text="Vendor:" CssClass="labelBlue8"></asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                        <asp:DropDownList id="dpdVendor" runat="server" CssClass="dropdown"></asp:DropDownList>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                    </TD>
                                </TR>
                                <%--<TR>
                                    <TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label id="Label7" runat="server" Text="OriginationIP" CssClass="labelBlue8"></asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                        <asp:DropDownList id="dpdOriginationIp" runat="server" CssClass="dropdown"></asp:DropDownList>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                    </TD>
                                </TR>
                                <TR>
                                    <TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label id="Label8" runat="server" Text="TerminationIP" CssClass="labelBlue8"></asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                        <asp:DropDownList id="dpdTermination" runat="server" CssClass="dropdown"></asp:DropDownList>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                    </TD>
                                </TR>--%>
                                <TR>
                                    <TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label id="Label9" runat="server" Text="Country" CssClass="labelBlue8"></asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                        <asp:DropDownList id="dpdCountry" runat="server" CssClass="dropdown"></asp:DropDownList>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                    </TD>
                                </TR>
                                <TR>
                                    <TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label id="Label10" runat="server" Text="Region" CssClass="labelBlue8"></asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                        <asp:DropDownList id="dpdRegion" runat="server" CssClass="dropdown"></asp:DropDownList>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                    </TD>
                                </TR>
                                <TR>
                                    <TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label id="Label11" runat="server" Text="Type" CssClass="labelBlue8"></asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                        <asp:DropDownList id="dpdType" runat="server" CssClass="dropdown"></asp:DropDownList>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                    </TD>
                                </TR>
                                <TR>
                                    <TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label id="Label12" runat="server" Text="Class" CssClass="labelBlue8"></asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                        <asp:DropDownList id="dpdClass" runat="server" CssClass="dropdown"></asp:DropDownList>
                                    </TD>
                                    <TD style="WIDTH: 100px">
                                    </TD>
                                </TR>
                                    <tr>
        <td align="center" bgcolor="#d5e3f0" style="width: 100px">
            <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="LMC"></asp:Label></td>
        <td style="width: 100px">
            <asp:DropDownList ID="dpdLMC" runat="server" CssClass="dropdown">
            </asp:DropDownList></td>
        <td style="width: 100px">
        </td>
    </tr>
    <TR>
        <TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
            <asp:Label id="Label13" runat="server" Text="Graph" CssClass="labelBlue8"></asp:Label>
        </TD>
        <TD style="WIDTH: 100px">
            <asp:CheckBoxList ID="ckboxl" runat="server" AccesKey="myCheckBox" CssClass="labelSteelBlue"
                Font-Bold="False" Width="100px">
                <asp:ListItem Selected="True" Value="0">Customer</asp:ListItem>
                <asp:ListItem Value="1">Vendor</asp:ListItem>
            </asp:CheckBoxList>    
        </TD>
        <TD style="WIDTH: 100px">
        </TD>
    </TR>
    <%--<tr>
        <td align="center" bgcolor="#d5e3f0" style="width: 100px; height: 22px;">
            <asp:Label ID="Label14" runat="server" CssClass="labelBlue8" Text="Gateway"></asp:Label></td>
        <td style="width: 100px; height: 22px;">
            <asp:DropDownList ID="dpdGateway" runat="server" CssClass="dropdown">
            </asp:DropDownList></td>
        <td style="width: 100px; height: 22px;">
        </td>
    </tr>--%>
    <TR><TD style="WIDTH: 100px"><asp:Button id="cmdSearch" onclick="cmdSearch_Click" runat="server" Text="View report" CssClass="boton"></asp:Button></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR></TBODY></TABLE> </TD></TR></TBODY></TABLE>
    
    </div>
    </form>
</body>
</html>
