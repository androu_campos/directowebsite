using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;
using System.Text;


public partial class AACustCdrs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Value = DateTime.Today.AddDays(-1).ToShortDateString();
            txtTo.Value = DateTime.Today.AddDays(-1).ToShortDateString();


            txtToV.Text = txtTo.Value.ToString();
            txtFromV.Text = txtFrom.Value.ToString();

            //SqlConnection SqlConn = new SqlConnection();
            //SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";
            //SqlCommand SqlCommand7 = new SqlCommand("SELECT DISTINCT Class FROM Regions WHERE Class is not NULL UNION SELECT '** NONE **' AS Class FROM Regions UNION SELECT '**ALL**' AS Class FROM Regions ORDER BY Class", SqlConn);
            //SqlConn.Open();
            //SqlDataReader myReader7 = SqlCommand7.ExecuteReader();
            //while (myReader7.Read())
            //{
            //    dpdClass.Items.Add(myReader7.GetValue(0).ToString());
            //}
            //myReader7.Close();
            //SqlConn.Close();

            //TextBox1.Text = "0";
            //TextBox2.Text = "0";

        }

    }

    protected void ddlCust_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlCust.SelectedItem.ToString() == "**ALL**") hfCust.Value = "%";
        else hfCust.Value = ddlCust.SelectedItem.ToString();

    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlVendor.SelectedItem.ToString() == "**ALL**") hfVendor.Value = "%";
        else hfVendor.Value = ddlVendor.SelectedItem.ToString();

    }
    //protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //    string Coun;

    //    if (ddlCountry.SelectedItem.ToString() == "**ALL**") Coun = "%";
    //    else Coun = ddlCountry.SelectedItem.ToString();
    //    hfCountry.Value = Coun;
    //    ddlRegion.Enabled = true;
    //    ddlRegion.Items.Clear();
    //    sqldsRegion.SelectCommand = "SELECT  '**ALL**' as Region FROM CDRDB..Regions UNION SELECT DISTINCT Region FROM CDRDB..Regions UNION SELECT '** NONE **' as Region FROM CDRDB..Regions WHERE Country like '" + Coun + "' ORDER BY Region";

    //}
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        ///**/
        string SelectString, WhereString, FromString, FromSBro = "", GroupByString, UnionString, QQ, sOrder, BrokerWhere;
        string sF, sW, sS, sSt, sG, sSUM = "";
        bool bCV = false, bVB = false;

        string sql = "SELECT [OriginationID],[m_sIPAddress],[m_sRemoteIPAddress],[m_sConnectionId],[m_sCallingStationId],[m_sCalledStationID] " +
                     ",[stop],[start],[lDuration],[TerminationID],[VendorID],[PDD],[original_isdn_cause_code],[m_sCallingStationAux] " +
                     ",[m_sCallingStationSend] FROM [ALLACCESS].[dbo].[cdrs] ";

        string where = "";

        if (hfCust.Value.ToString() == "") hfCust.Value = "%";
        if (hfVendor.Value.ToString() == "") hfVendor.Value = "%";

        string from = txtFromV.Text;
        string to = txtToV.Text;

        where = where + " '" + from + " 00:00' AND '" + to + " 23:59' ";

        if (hfCust.Value.ToString() != "%")
        {
            where = where + " AND M_SREMOTEIPADDRESS IN (SELECT IP FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE '" + hfCust.Value.ToString() + "' AND TYPE = 'C') ";
        }
        if (hfVendor.Value.ToString() != "%")
        {
            where = where + " AND VENDORID IN (SELECT IP FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE '" + hfVendor.Value.ToString() + "' AND TYPE = 'V') ";
        }
        if (txtNumber.Text != "")
        {
            where = where + " AND (M_SCALLEDSTATIONID LIKE '%" + txtNumber.Text + "%' OR M_SCALLINGSTATIONID LIKE '%" + txtNumber.Text + "%') ";
        }









        //checando valores no asignados
        //if (ddlRegion.Enabled == false)
        //{
        //    hfRegion.Value = "%";
        //}
        //else
        //{
        //    if (ddlRegion.SelectedValue.ToString() == "**ALL**") hfRegion.Value = "%";
        //    else if (ddlRegion.SelectedValue.ToString() == "** NONE **") hfRegion.Value = "%";
        //    else hfRegion.Value = ddlRegion.SelectedItem.ToString();
        //}

        //if (ddlCountry.SelectedItem.ToString() == "**ALL**") hfCountry.Value = "%";
        //else if (ddlCountry.SelectedItem.ToString() == "** NONE **") hfCountry.Value = "%";
        //else hfCountry.Value = ddlCountry.SelectedItem.ToString();

        //if (hfCust.Value.ToString() == "") hfCust.Value = "%";
        //if (hfVendor.Value.ToString() == "") hfVendor.Value = "%";

        ////FIN checando valores no asignados

        //SelectString = "";
        //BrokerWhere = "";
        //FromString = " FM2TL";

        //WhereString = "";

        //GroupByString = "";
        //UnionString = "";

        ////WHERE Builder  
        //string from = txtFromV.Text;
        //string to = txtToV.Text;


        //WhereString = WhereString + "'" + from + "' AND '" + to + "'";
        //if (hfCust.Value.ToString() != "%")
        //{
        //    WhereString = WhereString + " AND T.Customer LIKE '" + hfCust.Value.ToString() + "'";
        //}
        //if (hfVendor.Value.ToString() != "%")
        //{
        //    WhereString = WhereString + " AND T.Vendor LIKE '" + hfVendor.Value.ToString() + "'";
        //}
        //if (txtNumber.Text != "")
        //{
        //    WhereString = WhereString + " AND T.";
        //}
        //if (hfCountry.Value.ToString() != "%")
        //{
        //    WhereString = WhereString + " AND T.Country LIKE '" + hfCountry.Value.ToString() + "'";
        //}
        //if (hfRegion.Value.ToString() != "%")
        //{
        //    WhereString = WhereString + " AND T.Region LIKE '" + hfRegion.Value.ToString() + "'";
        //}

        //if (dpdType.SelectedValue.ToString() != "** NONE **" && dpdType.SelectedValue.ToString() != "**ALL**")
        //{
        //    WhereString = WhereString + " AND T.Type LIKE '" + dpdType.SelectedValue.ToString() + "'";

        //}

        //if (TextBox3.Text != "")
        //{
        //    WhereString = WhereString + " AND T.DialedNumber LIKE '%" + TextBox3.Text + "%'";
        //}

        ////FIN WHERE Builder


        //    SelectString = SelectString + ",CONVERT(VARCHAR(11) ,T.Billingdate,101) as BillingDate";
        //    GroupByString = GroupByString + ",T.BillingDate";
        //    UnionString = UnionString + ",'** ALL **' AS BillingDate";

        //if (ddlCust.SelectedItem.ToString() != "** NONE **")
        //{
        //    SelectString = SelectString + ",T.Customer";
        //    GroupByString = GroupByString + ",T.Customer";
        //    UnionString = UnionString + ",'****' AS Customer";
        //}



        //if (ddlVendor.SelectedItem.ToString() != "** NONE **")
        //{
        //    SelectString = SelectString + ",T.Vendor";
        //    GroupByString = GroupByString + ",T.Vendor";
        //    UnionString = UnionString + ",'****' AS Vendor";
        //}

        //if (standcause.Checked == true)
        //{
        //    SelectString = SelectString + ",CAST(ISNULL(T.IsdnCause,'0') AS varchar) as IsdnStandard";
        //    GroupByString = GroupByString + ",CAST(ISNULL(T.IsdnCause,'0') AS varchar)";
        //    UnionString = UnionString + ",'****' AS IsdnCause";
        //}
        //else
        //{
        //    SelectString = SelectString + ",CAST(ISNULL(T.IsdnOriginal,'0') AS varchar) as IsdnOriginal";
        //    GroupByString = GroupByString + ",CAST(ISNULL(T.IsdnOriginal,'0') AS varchar)";
        //    UnionString = UnionString + ",'****' AS IsdnOriginal";
        //}

        //SelectString = SelectString + ",B.causedesc as [Description]";
        //GroupByString = GroupByString + ",B.causedesc";
        //UnionString = UnionString + ",'****' AS [Description]";



        //if (ddlCountry.SelectedItem.ToString() != "** NONE **")
        //{
        //    SelectString = SelectString + ",T.Country";
        //    GroupByString = GroupByString + ",T.Country";
        //    UnionString = UnionString + ",'****' as Country";

        //    if (ddlRegion.SelectedItem.ToString() != "** NONE **")
        //    {
        //        SelectString = SelectString + ",T.Region";
        //        GroupByString = GroupByString + ",T.Region";
        //        UnionString = UnionString + ",'****' as Region";
        //    }

        //}



        //int icount = 0;




        //if (dpdType.SelectedValue.ToString() != "** NONE **")//TYPE
        //{
        //    SelectString = SelectString + ",T.Type";
        //    GroupByString = GroupByString + ",T.Type";
        //    UnionString = UnionString + ",'****' as [Type]";
        //    //FromString = FromString + " LJM2R";
        //    //icount = icount + 1;
        //}



        ////Arriba con group
        ////Sin group abajo

        //sSUM = sSUM + ",ROUND(SUM(T.Minutes),4) as CMinutes";
        //sSUM = sSUM + ",Count(*) as Calls";






        //sS = "";
        //sOrder = "";
        //if (SelectString.Length > 3)
        //{
        //    sS = SelectString.Substring(1);
        //    sOrder = " ORDER BY " + sS.Replace("CONVERT(VARCHAR(11) ,T.Date,101) as", " ");
        //}
        //else
        //{
        //    if (sSUM.Length > 3)
        //    {
        //        sSUM = sSUM.Substring(1);
        //    }
        //}

        //sS = SelectString.Substring(1);

        //sOrder = " ORDER BY CONVERT(VARCHAR(11) ,T.Date,101), count(*) desc ";

        //string sHaving = "";

        //if (TextBox1.Text != "0")
        //{
        //    sHaving = " HAVING SUM(Minutes) < " + TextBox1.Text + " ";
        //}
        //else if (TextBox2.Text != "0")
        //{
        //    sHaving = " HAVING SUM(Minutes) > " + TextBox2.Text + " ";
        //}
        //else
        //    sHaving = " HAVING SUM(Minutes) > 0 ";

        //sG = string.Empty;

        //sF = FromString + FromSBro;
        //sW = WhereString;
        //if (GroupByString.Length > 3)
        //{
        //    sG = " GROUP BY CONVERT(VARCHAR(11) ,T.Date,101), " + GroupByString.Substring(1);
        //}
        //sSt = "";
        //if (UnionString.Length > 1)
        //{
        //    sSt = UnionString.Substring(1);
        //}


        //sF = sF.Replace("FM2TL", "FROM mecca2..isdnreport T");

        //if (standcause.Checked == true)
        //{
        //    sF = sF.Replace("LOJM2I", "LEFT OUTER JOIN CDRDB..isdn_causes B ON T.IsdnCause = B.idcause");
        //}
        //else
        //{
        //    sF = sF.Replace("LOJM2I", "LEFT OUTER JOIN CDRDB..isdn_causes B ON T.IsdnOriginal = B.idcause");
        //}
        //sF = sF.Replace("LJM2VB", "LEFT JOIN MECCA2..VBroker V ON T.Vendor = V.Vendor");
        //sF = sF.Replace("LJM2R", "LEFT JOIN MECCA2.dbo.Regions R ON T.RegionPrefix = R.Code");

        //QQ = "sS=" + sS + "&sF=" + sF + "&sW=" + sW + "&sG=" + sG + "&sSt=" + sSt + "&sSUM=" + sSUM;

        //string Query = " SELECT CONVERT(VARCHAR(11) ,T.Date,101) as [Date], " + sS + sSUM + sF + " WHERE T.[Date] BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT '******' as Date, " + sSt + sSUM + sF + " WHERE T.[Date] BETWEEN " + sW + sHaving + sOrder;

        string Query = sql + " WHERE [Stop] BETWEEN " + where + " ORDER BY STOP";

        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandTimeout = 350;
        sqlSpQuery.CommandType = System.Data.CommandType.Text;

        sqlSpQuery.CommandText = Query;
        DataSet ResultSet;
        ResultSet = Util.RunQuery(sqlSpQuery);

        Session["AACustCdrs"] = ResultSet;

        if (ResultSet.Tables.Count > 0)
        {
            gdvRecent.DataSource = ResultSet.Tables[0];
            gdvRecent.DataBind();
            gdvRecent.Visible = true;
            Button1.Visible = true;
        }
        else
        {
            gdvRecent.Visible = false;
            Button1.Visible = false;
        }


        ////Query = Query.Replace("SELECT ,", "SELECT ");
        //Session["SalesLogS"] = Query;
        ////Response.Cookies["MSLH"].Value = Query;  
        //if ((sS.Length + sSUM.Length) > 6)
        //{
        //    Response.Redirect("~/MSLH.aspx", false);
        //}

    }
    protected void rdbtnlType_SelectedIndexChanged(object sender, EventArgs e)
    {

        //***

    }
    protected void MECCA_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("http://mecca3.computer-tel.com/MECCA/");
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect("MasterSalesLogH.aspx");
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }

    protected void gdvRecent_Changing(object sender, GridViewPageEventArgs e)
    {
        gdvRecent.PageIndex = e.NewPageIndex;
        gdvRecent.DataBind();
    }

    protected void btnExcel_Click(object sende, EventArgs e)
    {
        StringBuilder str = new StringBuilder();

        try
        {

            DataTable dtEmployee = ((DataSet)Session["AACustCdrs"]).Tables[0].Copy();
            // Export all the details to CSV

            for (int c = 0; c < dtEmployee.Columns.Count; c++)
            {
                string columnName = dtEmployee.Columns[c].ColumnName.ToString();
                str.Append(columnName);
                str.Append(";");
            }

            str.Append("\n\r");

            for (int i = 0; i < dtEmployee.Rows.Count; i++)
            {
                for (int j = 0; j < dtEmployee.Columns.Count; j++)
                {
                    str.Append(dtEmployee.Rows[i][j].ToString());
                    str.Append(";");
                }

                str.Append("\n\r");

            }
            Response.Clear();
            Response.AddHeader("content-disposition",
                     "attachment;filename=AACustCdrs.txt");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.text";

            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite =
                          new HtmlTextWriter(stringWrite);

            Response.Write(str.ToString());
            Response.End();



            //RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            //string filename = "MasterSalesLogTop20" + ".csv";
            //objExport.ExportDetails(dtEmployee, Export.ExportFormat.CSV, filename);



        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
}
