<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" Title="DIRECTO - Connections Worldwide" AutoEventWireup="true" CodeFile="MaxReportLog.aspx.cs" Inherits="MasReportLog"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <asp:Label ID="Label1" runat="server" Text="MAX REPORT HISTORY" CssClass="labelTurkH"></asp:Label>
    
    
     <table width="600px" align="left" style="position: absolute; top: 220px; left: 230px;">
       <tr>
            <td width="210px" valign="top" align="left" style="height: 96px">
            <table>
                    <%--<tr>          
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                     <tr>--%>
                     <tr>           
                        <td style="width: 100px; height: 10px;" align="right" valign="top">
                            <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="from:"></asp:Label>                
                        </td>
                        <td style="width: 100px; height: 10px;" align="left" valign="top">
                            <asp:TextBox ID="TextBox1" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
                        <td style="width: 100px; height: 10px;" valign="top">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                        <td style="width: 100px; height: 10px;" valign="top">
                            <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="to:"></asp:Label></td>
                        <td align="left" style="width: 100px; height: 10px;" valign="top">
                            <asp:TextBox ID="TextBox2" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
                        <td align="left" style="width: 100px; height: 10px;" valign="top">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                        <td align="left" style="width: 100px; height: 10px;" valign="top">
                            <asp:Button ID="cmdShow" runat="server" CssClass="boton" OnClick="cmdShow_Click"
                                Text="View report" /></td>
                    </tr>    
             </table>
             </td>   
        </tr>  
        <tr>
            <td>
                <table>
                    <%--<tr>            
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                    </tr>  --%>
                    <tr>
                        <td>
                            <asp:Button ID="cmdExcel" runat="server" CssClass="boton" OnClick="cmdExport"
                                Text="Export to EXCEL" Width="110px" />
                        </td>
                    </tr>
                    <%--<tr>            
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                        <td style="width: 100px">&nbsp;
                        </td>
                    </tr>  --%>
                </table>
            </td>
        </tr> 
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:GridView ID="gdv1" runat="server" Font-Names="Arial" Font-Size="8pt" AutoGenerateColumns="false" AllowPaging="True"
                                OnPageIndexChanging="gdv1_PageIndexChanging" PageSize="200" >
                                <HeaderStyle CssClass="titleOrangegrid" Height="40px" Width="150px" HorizontalAlign="Center" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#2461BF" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <AlternatingRowStyle BackColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="BDate" HeaderText="BillingDate" >
                                        <ItemStyle Wrap="false" />
                                    </asp:BoundField >
                                    <asp:BoundField DataField="Vendor" HeaderText="Vendor" >
                                        <ItemStyle Wrap="false" />
                                    </asp:BoundField >
                                    <asp:BoundField DataField="Minutes" HeaderText="Minutes" >
                                        <ItemStyle Wrap="false" />
                                    </asp:BoundField >
                                    <asp:BoundField DataField="Profit" HeaderText="Profit" >
                                        <ItemStyle Wrap="false" />
                                    </asp:BoundField >
                                    
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 577px">
                        </td>
                        <td colspan="4" valign="top">
                            <asp:ScriptManager id="ScriptManager1" runat="server">
                            </asp:ScriptManager><br />
                            <cc1:calendarextender id="CalendarExtender1" runat="server" popupbuttonid="Image1"
                                targetcontrolid="TextBox1" Format="MM/dd/yyyy"></cc1:calendarextender>
                            <cc1:calendarextender id="CalendarExtender2" runat="server" popupbuttonid="Image2"
                                targetcontrolid="TextBox2" Format="MM/dd/yyyy"></cc1:calendarextender>                
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>    
                </table>
            </td>
        </tr>         
    </table>
    
    
    
</asp:Content>

