<%@ Page Language="C#"  AutoEventWireup="true" CodeFile="PrintInvoice.aspx.cs"
    Inherits="PrintInvoice" Title="Untitled Page" StylesheetTheme="Theme1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table border="0" width="100%">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label></td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="left">
                <img src="Images/MECCA2.gif" align="left">
            </td>
            <td align="left">
                <b>Computer-Tel, Inc</b><br>
                    2050 Russett Way<br>Carson City, NV 89703
            </td>
            <td align="left">
                <table border="1">
                    <tr>
                        <td>
                            <font size="-1"></font>
                                <table border="0">
                                    <tr>
                                        <th>
                                            Invoice Number:</th>
                                        <th>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            Customer:</td>
                                        <td>
                                            <u>
                                                <asp:Label ID="lblCustomer" runat="server" Text="Label"></asp:Label></u></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Address:</td>
                                        <td>
                                            <u>
                                                <asp:Label ID="lblAddress" runat="server" Text="Label"></asp:Label></u></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <u></u>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Phone:</td>
                                        <td>
                                            <u>
                                                <asp:Label ID="lblPhone" runat="server" Text="Label"></asp:Label></u></td>
                                    </tr>
                                </table>
                                
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <hr>
    <table border="0">
        <tr>
            <td valign="top" nowrap>
                <font size="+1">Invoice Summary</font><br>
                <blockquote>
                    Bill Date:
                    <asp:Label ID="lblBillDate" runat="server" Text="Label"></asp:Label><br>
                    Billing Period: 
                    <asp:Label ID="lblFrom" runat="server" Text="Label"></asp:Label>-
                    <asp:Label ID="lblTo" runat="server" Text="Label"></asp:Label><br>
                    Amound Due: <font color="red">$<asp:Label ID="lblAmound" runat="server" Text="Label"></asp:Label></font><br>
                    Due Date:
                    <asp:Label ID="lblDueDate" runat="server" Text="Label"></asp:Label><br>
                </blockquote>
                <br />
                <hr>
                <strong>Account Status<br />
                </strong>
                <br>
                    <table border="0">
                        <colgroup>
                        <col align="right">
                        </colgroup>
                        <tr>
                            <td align="left">
                                Previous Balance:</td>
                            <td>
                                $</td>
                            <td style="width: 150px">
                                <asp:Label ID="lblPreviousBalance" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left">
                                Payments:</td>
                            <td>
                                $</td>
                            <td style="width: 150px">
                                <asp:Label ID="lblPayments" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left">
                                Adjustments:</td>
                            <td>
                                $</td>
                            <td style="width: 150px">
                                <asp:Label ID="lblAdjustments" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b></b>Past Due Balance</td>
                            <td>
                                $</td>
                            <td style="width: 150px">
                                <asp:Label ID="lblPastDueBalance" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <strong>
                                Late Fees:</strong></td>
                            <td style="font-weight: bold">
                                $</td>
                            <td style="width: 150px">
                                <asp:Label ID="lblLateFees" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left">
                                Current Charges</td>
                            <td>
                                $</td>
                            <td style="width: 150px">
                                <asp:Label ID="lblCurrentCharges" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <b>Total Due</b> </td>
                            <td>
                                $</td>
                            <td style="width: 150px">
                                <asp:Label ID="lblTotalDue" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                    </table>
                <br />
                <hr>
                <b>Bank Wiring Instructions</b><br>
                <blockquote>
                    Computer-Tel, Inc.<br>
                    COMPASS BANK<br>
                    40 NE Loop 410 Suite 100<br>
                    San Antonio, TX 78216<br>
                    ABA: 1 1 3 0 1 0 5 4 7<br>
                    SWIFT: C P A S U S 4 4<br>
                    Account #: 0 6 6 5 2 5 6</blockquote>
                <blockquote>
                    HSBC<br>
                    Doral Office<br>
                    4090 NW 97th Avenue<br>
                    Miami, Florida 33178<br>
                    ABA : 0 2 1 0 0 1 0 8 8<br>
                    SWIFT : M R M D U S 3 3<br>
                    Account : 1 9 3 1 0 4 6 5 2<br>
                    </blockquote>
                <hr>
                <blockquote>
                    </blockquote>
            </td>
            <td valign="top">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                    Width="320px" Font-Names="Arial" Font-Size="9.5pt" >
                    <Columns>
                        <asp:BoundField DataField="Date" HeaderText="Date"  />
                        <asp:BoundField DataField="Completed Calls" HeaderText="Completed Calls"  DataFormatString="{0:0,0}" HtmlEncode="false" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Billed Minutes" HeaderText="Billed Minutes" DataFormatString="{0:0,0}" HtmlEncode="false" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Amount" HeaderText="Amount" DataFormatString="{0:f2}" HtmlEncode="false" ItemStyle-HorizontalAlign="Center" />
                    </Columns>
                </asp:GridView>
                
                <table border="1" width="100%">
                </table>
                <p>
                    </p>
                <br />
                    <table border="1" width="100%">
                    </table>
                <asp:GridView ID="GridView2" runat="server" Width="320px" AutoGenerateColumns="False" Font-Names="Arial" Font-Size="9.5pt">
                    <Columns>
                        <asp:BoundField DataField="Completed Calls" HeaderText="Completed Calls" DataFormatString="{0:0,0}" HtmlEncode="false" />
                        <asp:BoundField DataField="Billed Minutes" HeaderText="Billed Minutes" DataFormatString="{0:0,0}" HtmlEncode="false" />
                        <asp:BoundField DataField="Price" HeaderText="Price" DataFormatString="{0:0.0000}" HtmlEncode="false" />
                        <asp:BoundField DataField="Amount" HeaderText="Amount" DataFormatString="{0:f2}" HtmlEncode="false" />
                    </Columns>
                </asp:GridView>
                    <p></p>
                <br />
                        <table border="1" width="100%">
                        </table>
                
                <asp:GridView ID="GridView3" runat="server" Width="320px" AutoGenerateColumns="False" Font-Names="Arial" Font-Size="9.5pt">
                    <Columns>
                        <asp:BoundField DataField="Total Calls" HeaderText="Total Calls"  DataFormatString="{0:0,0}" HtmlEncode="false" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Total Minutes" HeaderText="Total Minutes" DataFormatString="{0:0,0}" HtmlEncode="false" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Price" HeaderText="Price" DataFormatString="{0:0.0000}" HtmlEncode="false" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Total Amount" HeaderText="Total Amount" DataFormatString="{0:f2}" HtmlEncode="false" ItemStyle-HorizontalAlign="Center" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>

    

