<%@ Page Language="C#" MasterPageFile="~/Master.master" StylesheetTheme="Theme1" AutoEventWireup="true" CodeFile="Comisions.aspx.cs" Inherits="Comisions" Title="DIRECTO - Connections Worldwide" Culture="Auto" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="Label1" runat="server" Text="MECCA COMMISSIONS" CssClass="labelTurkH"></asp:Label> 
    <table style="position:absolute; top:220px; left:230px;" width="100%" >    
    <tr>
    <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager>
    <td style="height: 154px; width: 321px;" valign="top" align="left">
    <table align="left" width="850" >
        <tr>
            <td align="right" width="20%">
                <asp:Label ID="Label2" runat="server" CssClass="labelSteelBlue" Text="Select Country Manager:" Width="123px"></asp:Label></td>
            <td align="right" width="20%">
                <asp:DropDownList ID="dpdManager" runat="server" CssClass="dropdown" DataTextField="Broker" DataValueField="Broker">
                </asp:DropDownList></td>
            <td align="left" width="15%">
            </td>
            <td width="45%">
            </td>
            <td width="45%">
            </td>
        </tr>
        <tr>
            <td align="right" width="20%">
                <asp:Label ID="Label5" runat="server" CssClass="labelSteelBlue" Text="From:"></asp:Label></td>
            <td align="right" width="20%">
                <asp:TextBox ID="txtFrom" runat="server" Width="146px" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td align="left" width="15%">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td width="45%">
                </td>
            <td width="45%">
            </td>
        </tr>
        <tr>
            <td align="right" width="20%">
                <asp:Label ID="Label6" runat="server" CssClass="labelSteelBlue" Text="To:"></asp:Label></td>
            <td align="right" width="20%">
                <asp:TextBox ID="txtTo" runat="server" Width="146px" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td align="left" width="15%">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td width="45%">
                </td>
            <td width="45%">
            </td>
        </tr>
        <tr>
            <td align="right" width="20%">
               
            </td>
            <td align="left" width="20%">
                
            </td>
            <td align="left" width="15%">
            </td>
            <td width="45%">
            </td>
            <td width="45%">
            </td>
        </tr>
        <tr>
            <td align="right" width="20%">
            </td>
            <td align="left" width="20%">
            </td>
            <td align="left" width="15%">
            </td>
            <td width="45%">
             <%--   <asp:UpdateProgress id="UpdateProgress1" runat="server">
                    <progresstemplate>--%>
<asp:Label id="Label3" runat="server" Text="Loading please wait..." CssClass="labelBlue8"></asp:Label> 
<%--
</progresstemplate>
                </asp:UpdateProgress>--%>
            </td>
            <td width="45%">
            </td>
        </tr>
        <tr>
            <td colspan="4" style="height: 144px">
               <%-- <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>--%>
<TABLE><TR><TD width="20%"></TD><TD width="80%">
                
            <asp:Button ID="cmdReport" runat="server" OnClick="cmdReport_Click" Text="View Report" CssClass="boton" /></TD></TR><TR><TD style="WIDTH: 100px"></TD><TD width="80%"></TD></TR><TR><TD style="WIDTH: 100px"></TD><TD width="80%">
         <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Export to EXCEL" CssClass="boton" Visible="False" /></TD></TR><TR><TD style="WIDTH: 100px"></TD><TD width="80%">
                
                <asp:GridView ID="gdvComisions" runat="server" AutoGenerateColumns="False" PageSize="25"
                     Width="850px" AllowPaging="True" OnPageIndexChanging="gdvComisions_PageIndexChanging1">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="9pt" />
                    <AlternatingRowStyle BackColor="White" />
                <Columns>
                        <asp:BoundField DataField="Customer" HeaderText="Customer">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" Width="160px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Vendor" HeaderText="Vendor">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" Width="160px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CustBroker" HeaderText="CustBroker">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="180px" />
                            <HeaderStyle HorizontalAlign="Left" Width="160px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="VendorBroker" HeaderText="VendorBroker">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" Width="160px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="AnsweredCalls" HeaderText="AnsweredCalls" HtmlEncode="False" DataFormatString="{0:0,0}">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" Width="160px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CMinutes" HeaderText="CMinutes" HtmlEncode="False" DataFormatString="{0:0,0}">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="180px"  />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                         <asp:BoundField DataField="VMinutes" HeaderText="VMinutes" HtmlEncode="False" DataFormatString="{0:0,0}">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="180px"  />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="TotalCost" HeaderText="TotalCost"  HtmlEncode="False" DataFormatString="{0,00:C}">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TotalSales" HeaderText="TotalSales"  HtmlEncode="False" DataFormatString="{0,00:C}">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="DirectoProfit" HeaderText="Directo Profit"  HtmlEncode="False" DataFormatString="${0:f2}">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="GrossProfit" HeaderText="Gross Profit"  HtmlEncode="False" DataFormatString="${0:f2}">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Center" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Minutes" HeaderText="TotalMinutes" HtmlEncode="False" DataFormatString="{0:0,0}">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView></TD></TR></TABLE>
<%--</contenttemplate>
                </asp:UpdatePanel>--%><br />
                <br />
            </td>
            <td colspan="1" style="height: 144px" valign="top">
                </td>
        </tr>
        <tr>
            <td colspan="4" width="20%">
            </td>
            <td colspan="1" width="20%">
            </td>
        </tr>
        
    </table>
    </td>
    </tr>
    
    <tr>
    <td style="width: 321px; height: 21px;">
     
    </td>
    </tr>
    
   
    <tr>
    <td align="left" style="width: 321px">
    <table  >
        <tr>
        <td>
        </td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;</td>
        </tr>
        
        <tr>
        <td height="100">
        
        </td>
        </tr>
         <tr>
            <td width="100%" >
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left" style="height: 42px">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                </td>
        </tr>
        <tr>
            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="Image1"
                TargetControlID="txtTo">
            </cc1:CalendarExtender>
            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="Image2"
                TargetControlID="txtFrom">
            </cc1:CalendarExtender>
            <asp:SqlDataSource ID="sqlBroker" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                SelectCommand="SELECT CustBroker AS Broker FROM CBroker UNION SELECT VendorBroker AS Broker FROM VBroker WHERE (VendorBroker <> 'N/A')">
            </asp:SqlDataSource>
        </tr>
    </table>
    
    </td>
    </tr>
    </table>
    
    
  
 

</asp:Content>

