using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AllAccessReport : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = "";
        DataSet ds = null;



        sql = "Select ISNULL([Type],'TBD') as Type, CUSTOMER,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
        + " SUM(Calls)) as ACD "
        + " from [MECCA2].[dbo].OpSheetAll "
        + " where Customer like 'ALLACCESS%' "
        + " group by ISNULL([Type],'TBD'), CUSTOMER "
        + " UNION "
        + " SELECT '** ALL **','*****',sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + " sum(Attempts) as Attempts, "
        + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
        + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
        + " from [MECCA2].[dbo].OpSheetAll "
        + " where Customer like 'ALLACCESS%' "
        + " order by  TotalMinutes DESC ";
            



       


        ds = Util.RunQueryByStmnt(sql);
        gdvToday.DataSource = ds.Tables[0];
        gdvToday.DataBind();


        sql = "Select ISNULL([Type],'TBD') as Type, CUSTOMER,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
        + " SUM(Calls)) as ACD "
        + " from [MECCA2].[dbo].OpSheetInc "
        + " where Customer like 'ALLACCESS%' "
        + " group by ISNULL([Type],'TBD'), CUSTOMER "
        + " UNION "
        + " SELECT '** ALL **','*****',sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + " sum(Attempts) as Attempts, "
        + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
        + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
        + " from [MECCA2].[dbo].OpSheetInc "
        + " where Customer like 'ALLACCESS%' "
        + " order by  TotalMinutes DESC ";
        

        ds = Util.RunQueryByStmnt(sql);
        gdvRecent.DataSource = ds.Tables[0];
        gdvRecent.DataBind();


        sql = "Select ISNULL([Type],'TBD') as Type, CUSTOMER,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
        + " SUM(Calls)) as ACD "
        + " from [MECCA2].[dbo].OpSheetY "
        + " where Customer like 'ALLACCESS%' "
        + " group by ISNULL([Type],'TBD'), CUSTOMER "
        + " UNION "
        + " SELECT '** ALL **','*****',sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + " sum(Attempts) as Attempts, "
        + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
        + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
        + " from [MECCA2].[dbo].OpSheetY "
        + " where Customer like 'ALLACCESS%' "
        + " order by  TotalMinutes DESC ";
        

        ds = Util.RunQueryByStmnt(sql);
        gdvYesterday.DataSource = ds.Tables[0];
        gdvYesterday.DataBind();

        sql = " Select CAST(datepart(hour,stop)as varchar) as [Hour], ISNULL([Type],'TBD') as Type, "
        + "Customer, "
        + "sum(ISNULL(CustBillMinutes,0)) as TotalMinutes, count(*) as Attempts, sum(CASE WHEN duration > 0 then 1 else 0 end) as AnsweredCalls, "
        + "sum(RA) as Rejected, mecca2.dbo.fGetASR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end), SUM(RA)) as ASR, "
        + "mecca2.dbo.fGetABR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ABR, "
        + "mecca2.dbo.fGetACD(SUM(CustBillMinutes), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ACD "
        + "from [mecca2].[dbo].Repsheet0_ "
        + "where Customer like 'ALLACCESS%' and datepart(hour,stop) > 18 "
        + "group by datepart(hour,stop), ISNULL([Type],'TBD'), "
        + "Customer "        
        + "UNION "
        + "Select '******' as [Hour], "
        + "'******' as Type, "
        + "'******' [Customer Group], "        
        + "sum(ISNULL(CustBillMinutes,0)) as TotalMinutes, "
        + "count(*) as Attempts, "
        + "sum(CASE WHEN duration > 0 then 1 else 0 end) as AnsweredCalls, "
        + "sum(RA) as Rejected, "
        + "mecca2.dbo.fGetASR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end), SUM(RA)) as ASR, "
        + "mecca2.dbo.fGetABR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ABR, "
        + "mecca2.dbo.fGetACD(SUM(CustBillMinutes), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ACD "
        + "from [mecca2].[dbo].Repsheet0_ "
        + "where Customer like 'ALLACCESS%' "
        + "and datepart(hour,stop) > 18 "
        + "order by [Hour], TotalMinutes ";

        ds = Util.RunQueryByStmnt(sql);
        gdvYesterdayHour.DataSource = ds.Tables[0];
        gdvYesterdayHour.DataBind();

    }
    
}
