using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RKLib.ExportData;
using System.Diagnostics;
using System.Reflection;
using Excel;
using System.IO;
using System.Text;
using System.Data.SqlClient;


public partial class ViewDifferences : System.Web.UI.Page
{
    private static ArrayList theTables = new ArrayList(6);
    private static int ii = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDate.Text = DateTime.Now.AddDays(-1).ToShortDateString();
        }

        
    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {

        ArrayList myList = new ArrayList(6);
        int i = 0;

        //
        string sql = "SELECT * FROM CDRDB..Diff_1 WHERE [Date]>'" + txtDate.Text + " 00:00:00.000' and [Date] < '" + txtDate.Text + " 23:59:59.000'";
        DataSet dts1 = (DataSet)Util.RunQueryByStmntatCDRDB(sql);
        if (dts1.Tables[0].Rows.Count > 0)
        {
            gdv1.DataSource = dts1.Tables[0];
            gdv1.DataBind();
            theTables.Add(dts1.Tables[0]);
            i++;
        }
        else
        {
            txtComments.Text = string.Empty;
            gdv1.DataBind();
        }
        //
        sql = "SELECT * FROM CDRDB..Diff_2 WHERE [Date_C]>'" + txtDate.Text + " 00:00:00.000' AND [Date_C]<'" + txtDate.Text + " 23:59:59.000'";
        DataSet dts2 = (DataSet)Util.RunQueryByStmntatCDRDB(sql);
        if (dts2.Tables[0].Rows.Count > 0)
        {
            gdv2.DataSource = dts2.Tables[0];
            gdv2.DataBind();
            theTables.Add(dts2.Tables[0]);
            i++;
        }
        else
        {
            gdv2.DataBind();
        }
        //

        DataSet dts3 = (DataSet)Util.RunQueryByStmntatCDRDB("SELECT * FROM CDRDB..Diff_3 WHERE [Date_C]>'" + txtDate.Text + " 00:00:00.000' AND [Date_C]<'" + txtDate.Text + " 23:59:59.000'");
        if (dts3.Tables[0].Rows.Count > 0)
        {
            gdv3.DataSource = dts3.Tables[0];
            gdv3.DataBind();
            theTables.Add(dts3.Tables[0]);
            i++;
        }
        else
        {
            gdv3.DataBind();
        }
        //

        DataSet dts4 = (DataSet)Util.RunQueryByStmntatCDRDB("SELECT * FROM CDRDB..Diff_4 WHERE [Date_C]>'" + txtDate.Text + " 00:00:00.000' AND [Date_C]<'" + txtDate.Text + " 23:59:59.000'");
        if (dts4.Tables[0].Rows.Count > 0)
        {
            gdv4.DataSource = dts4.Tables[0];
            gdv4.DataBind();
            theTables.Add(dts4.Tables[0]);
            i++;
        }
        else
        {
            gdv4.DataBind();
        }
        //
        sql = "SELECT * FROM CDRDB..cdrs_Diff_5 WHERE [stop]>'" + txtDate.Text + " 00:00:00.000' AND [stop]<'" + txtDate.Text + " 23:59:59.000'";
        DataSet dts5 = (DataSet)Util.RunQueryByStmntatCDRDB(sql);
        if (dts5.Tables[0].Rows.Count > 0)
        {
            gdv5.DataSource = dts5.Tables[0];
            gdv5.DataBind();
            theTables.Add(dts5.Tables[0]);
            i++;
        }
        else
        {
            gdv5.DataBind();
        }
        //
        sql = "SELECT * FROM CDRDB..Diff_6 WHERE [Date_C]>'" + txtDate.Text + " 00:00:00.000' AND [Date_C]<'" + txtDate.Text + " 23:59:59.000'";
        DataSet dts6 = (DataSet)Util.RunQueryByStmntatCDRDB(sql);
        if (dts6.Tables[0].Rows.Count > 0)
        {
            gdv6.DataSource = dts6.Tables[0];
            gdv6.DataBind();
            theTables.Add(dts6.Tables[0]);
            i++;
        }
        else
        {
            gdv6.DataBind();
        }
        ii = i;
        //VERIFICO SI YA HAY ALGUN COMENTARIO != null
        string qryC = "SELECT * from CDRDB..Diff_Comments where date_Diff = '" + txtDate.Text + " 00:00:00.000'";
        DataSet dtsComments = Util.RunQueryByStmntatCDRDB(qryC);

        if (dtsComments.Tables[0].Rows.Count > 0)
        {
            if (dtsComments.Tables[0].Rows[0][1].ToString() != string.Empty)
            {
                cmdAddComment.Visible = false;
                cmdEdit.Visible = true;
                txtComments.Text = dtsComments.Tables[0].Rows[0][1].ToString();
            }
            else
            {
                cmdAddComment.Visible = true;
                cmdEdit.Visible = false;
            }
            lblLink.Visible = true;
            string date = txtDate.Text + ".xls";
            date = date.Replace("/", "-");
            string strMachineName = Request.ServerVariables["SERVER_NAME"];
            lblLink.Text = "<A href=http://" + strMachineName + "/Diffs_M3_M4/" + "report_" + date + ">Download Report</a>";

        }
        else
        {
            cmdEdit.Visible = false;
            cmdAddComment.Visible = false;
            lblLink.Visible = false;
        }
        
      
    }

    protected void cmdAddComment_Click(object sender, EventArgs e)
    {

        if (ii > 0)
        {
            string comment = txtComments.Text;
            string user = Session["DirectoUser"].ToString();
//OBTIENE EL ID DEL COMENTARIO A ACTUALIZAR
            cdrdbDatasetTableAdapters.Diff_Comments1_TableAdapter adpCom1 = new cdrdbDatasetTableAdapters.Diff_Comments1_TableAdapter();
            cdrdbDataset.Diff_Comments1DataTable tblComments = adpCom1.GetData(Convert.ToDateTime(txtDate.Text));
            int iD = Convert.ToInt32(tblComments.Rows[0][0]);
//INSERTA EL NUEVO COMENTARIO AL LOG
            cdrdbDatasetTableAdapters.Log_CommentsAdp adpLog = new cdrdbDatasetTableAdapters.Log_CommentsAdp();
            adpLog.Insert(iD, user, DateTime.Now, txtComments.Text);
//ACTUALIZA EL COMENTARIO QUE SE DESPLIEGA EN LA PAGINA
            cdrdbDatasetTableAdapters.QueriesTableAdapter adpQ = new cdrdbDatasetTableAdapters.QueriesTableAdapter();
            adpQ.UpdateDiff_Comments(txtComments.Text, iD);            
//            txtComments.Load += new EventHandler(cmdAddComment_Click);
//DESHABILITA EL ADD COMMENT
            cmdAddComment.Visible = false;
            cmdEdit.Visible = true;
        }
        
        
    }
    protected void gdv1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdv1.PageIndex = e.NewPageIndex;
        gdv1.DataSource = theTables[0];
        gdv1.DataBind();
    }
    protected void gdv2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdv2.PageIndex = e.NewPageIndex;
        gdv2.DataSource = theTables[1];
        gdv2.DataBind();
    }
    protected void gdv4_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdv4.PageIndex = e.NewPageIndex;
        gdv4.DataSource = theTables[3];
        gdv4.DataBind();
    }
    protected void gdv3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdv3.PageIndex = e.NewPageIndex;
        gdv3.DataSource = theTables[2];
        gdv3.DataBind();
    }
    protected void gdv6_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdv6.PageIndex = e.NewPageIndex;
        gdv6.DataSource = theTables[5];
        gdv6.DataBind();
    }
    protected void gdv5_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdv5.PageIndex = e.NewPageIndex;
        gdv5.DataSource = theTables[4];
        gdv5.DataBind();
    }
    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        string newComment = txtComments.Text;
//OBTIENE EL ID DEL COMENTARIO A ACTUALIZAR
        cdrdbDatasetTableAdapters.Diff_Comments1_TableAdapter adpCom1 = new cdrdbDatasetTableAdapters.Diff_Comments1_TableAdapter();
        cdrdbDataset.Diff_Comments1DataTable tblComments = adpCom1.GetData(Convert.ToDateTime(txtDate.Text));
        int iD = Convert.ToInt32(tblComments.Rows[0][0]);
//INSERTA EL NUEVO COMENTARIO AL LOG
        cdrdbDatasetTableAdapters.Log_CommentsAdp adpLog = new cdrdbDatasetTableAdapters.Log_CommentsAdp();
        adpLog.Insert(iD, (string)Session["DirectoUser"], DateTime.Now, txtComments.Text);
//ACTUALIZA EL COMENTARIO QUE SE DESPLIEGA EN LA PAGINA
        cdrdbDatasetTableAdapters.QueriesTableAdapter adpQ = new cdrdbDatasetTableAdapters.QueriesTableAdapter();
        adpQ.UpdateDiff_Comments(txtComments.Text, iD);            

    }
    protected void cmdExport_Click(object sender, EventArgs e)
    {
     
    }
    
}
