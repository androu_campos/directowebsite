<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="AddIp.aspx.cs" Inherits="AddIp" Title="DIRECTO - Connections Worldwide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">    
        <table style="position:absolute; top:220px; left:230px;">
        <tr>
            <td align="left" style="width: 185px; height: 32px">
                <asp:Label ID="Label4" runat="server"
                    Text="Add IP" CssClass="labelTurk"></asp:Label></td>
            <td style="width: 156px; height: 32px">
            </td>
        </tr>
        <tr>
            <td style="width: 185px" align="right">
                <asp:Label ID="Label1" runat="server" Text="Provider" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 124px">
                <asp:DropDownList ID="dpdProvider" runat="server" Width="156px" CssClass="dropdown">
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width: 185px" align="right">
                <asp:Label ID="Label3" runat="server" Text="Type:" CssClass="labelBlue8" ></asp:Label></td>
            <td style="width: 124px">
                <asp:DropDownList ID="dpdType" runat="server" Width="156px" CssClass="dropdown">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="V">Vendor</asp:ListItem>
                    <asp:ListItem Value="N">National</asp:ListItem>
                    <asp:ListItem Value="G">Gateway</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width: 185px" align="right">
                <asp:Label ID="Label2" runat="server" Text="IP:" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 124px">
                <asp:TextBox ID="txtIP" runat="server" Width="156px" CssClass="labelSteelBlue"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 185px; height: 23px">
            </td>
            <td style="width: 124px; height: 23px">
                <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text="IP Added succesful !!"
                    Visible="False" Width="160px" CssClass="labelBlue8"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 185px">
            </td>
            <td align="left" style="width: 124px">
                <asp:Button ID="Button1" runat="server" Text="Add IP" OnClick="Button1_Click" CssClass="boton" /></td>
        </tr>
        <tr>
            <td style="width: 185px">
            </td>
            <td align="right" style="width: 124px">
            </td>
        </tr>
        <tr>
            <td style="width: 185px">
                <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/IP.aspx" CssClass="labelBlue8" Width="140px">Show Customer IP Database</asp:LinkButton></td>
            <td align="right" style="width: 124px">
            </td>
        </tr>
        <tr>
            <td style="width: 185px">
                <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/Unknown.aspx" CssClass="labelBlue8">Show Unknown Ips</asp:LinkButton></td>
            <td align="right" style="width: 124px">
            </td>
        </tr>
    </table>
</asp:Content>

