<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="MasterAccess.aspx.cs" Inherits="MasterAccess" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="height: 427px" width="700" align="left">
        <tr>
            <td align="left" valign="top" width="35%">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="Master Sales Log"></asp:Label></td>
            <td align="left" style="width: 35%" valign="top">
            </td>
            <td align="left" valign="top" width="30%">
            </td>
        </tr>
        <tr>
            <td align="left" style="height: 25px" valign="top" width="35%">
            </td>
            <td align="left" style="width: 35%; height: 25px" valign="top">
            </td>
            <td align="left" style="height: 25px" valign="top" width="30%">
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="35%">
    <table align="left" width="100%">
        <tr>
            <td style="height: 12px" bgcolor="#d5e3f0" width="30%">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Step 1"></asp:Label></td>
            <td style="height: 12px" bgcolor="#d5e3f0" width="70%">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Choose Range of Days"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 12px" width="30%">
                <asp:Label ID="Label4" runat="server" CssClass="labelSteelBlue" Text="From:"></asp:Label></td>
            <td style="height: 12px" width="70%">
                <table>
                    <tr>
                        <td style="width: 75px">
                <asp:TextBox ID="txtFrom" runat="server" CssClass="labelSteelBlue" Width="100%"></asp:TextBox></td>
                        <td align="right" style="width: 25px">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 12px" width="30%">
                <asp:Label ID="Label5" runat="server" CssClass="labelSteelBlue" Text="To:"></asp:Label></td>
            <td style="height: 12px" width="70%">
                <table style="width: 111px">
                    <tr>
                        <td style="width: 100px">
                <asp:TextBox ID="txtTo" runat="server" CssClass="labelSteelBlue" Width="100%"></asp:TextBox></td>
                        <td align="right" style="width: 26px">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#d5e3f0" style="height: 12px" width="30%">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Step 2"></asp:Label></td>
            <td bgcolor="#d5e3f0" style="height: 12px" width="70%">
                <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Choose Country and Region"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 12px" width="30%">
                <asp:Label ID="Label8" runat="server" CssClass="labelSteelBlue" Text="Country:"></asp:Label></td>
            <td style="height: 12px" width="70%">
                <asp:DropDownList ID="dpdCountry" runat="server" AutoPostBack="True" CssClass="dropdown"
                    DataSourceID="SqlDataSource1" DataTextField="Country" OnSelectedIndexChanged="dpdCountry_SelectedIndexChanged" Width="100%">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="height: 12px" width="30%">
                <asp:Label ID="Label9" runat="server" CssClass="labelSteelBlue" Text="Region:"></asp:Label></td>
            <td style="height: 12px" width="70%">
                <asp:DropDownList ID="dpdRegion" runat="server" CssClass="dropdown" DataSourceID="SqldsRegion"
                    DataTextField="Region" Width="100%">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="height: 12px" bgcolor="#d5e3f0" width="30%">
                <asp:Label ID="Label14" runat="server" CssClass="labelBlue8" Text="Step 3"></asp:Label></td>
            <td style="height: 12px" bgcolor="#d5e3f0" width="70%">
                <asp:Label ID="Label15" runat="server" CssClass="labelBlue8" Text="Choose Gateway"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 12px" width="30%">
                <asp:Label ID="Label16" runat="server" CssClass="labelSteelBlue" Text="Gateway:"></asp:Label></td>
            <td style="height: 12px" width="70%">
                <asp:DropDownList ID="dplGateway" runat="server" CssClass="dropdown" DataSourceID="SqldsGateway"
                    DataTextField="Gateway" Width="100%" DataValueField="Gateway">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="height: 12px" bgcolor="#d5e3f0" width="30%">
                <asp:Label ID="Label17" runat="server" CssClass="labelBlue8" Text="Step 4"></asp:Label></td>
            <td style="height: 12px" bgcolor="#d5e3f0" width="70%">
                <asp:Label ID="Label18" runat="server" CssClass="labelBlue8" Text="Choose SIM"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 12px" width="30%">
                <asp:Label ID="Label19" runat="server" CssClass="labelSteelBlue" Text="SIM:"></asp:Label></td>
            <td style="height: 12px" width="70%"><asp:DropDownList ID="dpdSIM" runat="server" CssClass="dropdown" DataSourceID="SqlSIMds"
                    DataTextField="SIM" Width="100%">
            </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="height: 12px" width="30%">
                <asp:Label ID="Label20" runat="server" CssClass="labelSteelBlue" Text="Broker:"></asp:Label></td>
            <td style="height: 12px" width="70%"><asp:DropDownList ID="dpdVBroker" runat="server" CssClass="dropdown" DataSourceID="SqldsBroker"
                    DataTextField="Broker" Width="100%" DataValueField="Broker">
            </asp:DropDownList></td>
        </tr>
    </table>
            </td>
            <td align="left" style="width: 35%" valign="top">
                <table width="100%">
                    <tr>
                        <td bgcolor="#d5e3f0" colspan="2">
                            <asp:Label ID="Label12" runat="server" CssClass="labelBlue8" Text="Step 5"></asp:Label></td>
                        <td bgcolor="#d5e3f0" colspan="2">
                            <asp:Label ID="Label13" runat="server" CssClass="labelBlue8" Text="Choose Report"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4" rowspan="2">
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" CssClass="labelSteelBlue">
                                <asp:ListItem Selected="True" Value="0">Personalized</asp:ListItem>
                                <asp:ListItem Value="1">Type1</asp:ListItem>
                                <asp:ListItem Value="2">Type2</asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="30%">
                <table align="left" width="100%">
                    <tr>
                        <td align="left" bgcolor="#d5e3f0" style="width: 100px">
                            <asp:Label ID="Label10" runat="server" CssClass="labelBlue8" Text="Step 6"></asp:Label></td>
                        <td align="left" bgcolor="#d5e3f0" style="width: 100px">
                            <asp:Label ID="Label11" runat="server" CssClass="labelBlue8" Text="Choose Fields"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <asp:CheckBoxList ID="ckboxl" runat="server" CssClass="labelSteelBlue">
                                <asp:ListItem Value="19">BillingDate</asp:ListItem>
                                <asp:ListItem Value="0">Gateway</asp:ListItem>
                                <asp:ListItem Value="1">SIM</asp:ListItem>
                                <asp:ListItem Value="2">Country</asp:ListItem>
                                <asp:ListItem Value="3">Region</asp:ListItem>
                                <asp:ListItem Value="4">Type</asp:ListItem>
                                <asp:ListItem Value="5">Class</asp:ListItem>
                                <asp:ListItem Value="6">LMC</asp:ListItem>
                                <asp:ListItem Value="7">RegionPrefix</asp:ListItem>
                                <asp:ListItem Value="8">CustPrefix</asp:ListItem>
                                <asp:ListItem Value="9">Cost</asp:ListItem>
                                <asp:ListItem Value="10">Price</asp:ListItem>
                                <asp:ListItem Value="12">Attempts</asp:ListItem>
                                <asp:ListItem Value="13">BillableCalls</asp:ListItem>
                                <asp:ListItem Value="11">BillMinutes</asp:ListItem>
                                <asp:ListItem Value="14">TotalCost</asp:ListItem>
                                <asp:ListItem Value="15">TotalSales</asp:ListItem>
                                <asp:ListItem Value="16">ASR</asp:ListItem>
                                <asp:ListItem Value="17">ACD</asp:ListItem>
                                <asp:ListItem Value="18">Profit</asp:ListItem>
                                <asp:ListItem Value="20">Broker</asp:ListItem>
                            </asp:CheckBoxList></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="35%" align="right">
                <asp:Button ID="btnReset" runat="server" CssClass="boton" OnClick="btnReset_Click"
                    Text="Reset" /></td>
            <td style="width: 60px" align="left">
                <asp:Button ID="cmdReport" runat="server" CssClass="boton" OnClick="cmdReport_Click"
                    Text="View report" />
                </td>
            <td width="30%">
            </td>
        </tr>
        <tr>
            <td width="35%">
            </td>
            <td style="width: 60px">
                <cc1:calendarextender id="CalendarExtender2" runat="server" popupbuttonid="Image2"
                    targetcontrolid="txtTo"></cc1:calendarextender>
                <cc1:calendarextender id="CalendarExtender1" runat="server" popupbuttonid="Image1"
                    targetcontrolid="txtFrom"></cc1:calendarextender>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
                    SelectCommand="SELECT '** ALL **' as Country UNION SELECT DISTINCT Country FROM QUESCOM..Regions ORDER BY Country" ProviderName="<%$ ConnectionStrings:QUESCOMConnectionString.ProviderName %>">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqldsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>" SelectCommand="SELECT DISTINCT Region FROM Regions&#13;&#10;UNION SELECT '** ALL **' AS Region FROM Regions&#13;&#10; ORDER BY Region">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlSIMds" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
                    SelectCommand="SELECT '** ALL **' as SIM &#13;&#10;UNION&#13;&#10;SELECT DISTINCT SIM FROM SIMinfo&#13;&#10;&#13;&#10;">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqldsGateway" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:QUESCOMConnectionString.ProviderName %>"
                    SelectCommand="SELECT '** ALL **' AS Gateway&#13;&#10;UNION&#13;&#10;SELECT DISTINCT  Gateway FROM QTrafficlog where BillingDate = DATEADD(&quot;d&quot;,-1,LEFT(GETDATE(),11))">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqldsBroker" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:QUESCOMConnectionString.ProviderName %>"
                    SelectCommand="SELECT '** ALL **' as Broker&#13;&#10;UNION&#13;&#10;SELECT DISTINCT Broker FROM Broker">
                </asp:SqlDataSource>
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td width="30%">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <br />
    &nbsp;
</asp:Content>

