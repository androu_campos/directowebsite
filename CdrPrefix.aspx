<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="CdrPrefix.aspx.cs" Inherits="CdrPrefix" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="position: absolute; top: 220px; left: 231px;">
        <tr>
            <td>
                <cc1:TabContainer ID="TabContainer1" runat="server" Width="350px" Height="260px"
                    ActiveTabIndex="1">
                    <cc1:TabPanel ID="Create" runat="server" HeaderText="CREATE">
                        <ContentTemplate>
                            <table align="left" id="createTable">
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label7" runat="server" CssClass="labelTurk" Text="CREATE"></asp:Label></td>
                                    <td align="left" style="width: 100px">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px" bgcolor="#d5e3f0">
                                        <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Prefix:"></asp:Label></td>
                                    <td style="width: 100px" align="left">
                                        <asp:TextBox ID="txtPrefix" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px" bgcolor="#d5e3f0">
                                        <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="ID:"></asp:Label></td>
                                    <td style="width: 100px" align="left">
                                        <asp:TextBox ID="txtID" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px" bgcolor="#d5e3f0">
                                        <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Type:"></asp:Label></td>
                                    <td style="width: 100px" align="left">
                                        <asp:DropDownList ID="dpdType" runat="server" CssClass="dropdown" Width="131px">
                                            <asp:ListItem>Customer</asp:ListItem>
                                            <asp:ListItem>Vendor</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#d5e3f0" style="width: 100px">
                                        <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Description:"></asp:Label></td>
                                    <td align="left" style="width: 100px">
                                        <asp:TextBox ID="txtDescription" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#d5e3f0" style="width: 100px">
                                    </td>
                                    <td align="left" style="width: 100px">
                                        <asp:Button ID="cmdCommand" runat="server" CssClass="boton" OnClick="cmdCommand_Click"
                                            Text="OK" /></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtPrefix"
                                            FilterType="Numbers">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td align="left" style="width: 100px">
                                        <asp:Label ID="Label12" ForeColor="Red" runat="server" Text="Created Successful"
                                            Visible="False" Font-Names="Arial" Font-Size="8pt" Width="126px"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="eliminar" runat="server" HeaderText="DELETE">
                        <ContentTemplate>
                            <table id="deleteTable">
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label6" runat="server" CssClass="labelTurk" Text="DELETE "></asp:Label></td>
                                    <td style="width: 100px">
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#d5e3f0" style="width: 100px">
                                        <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Select ID:"></asp:Label></td>
                                    <td style="width: 100px">
                                        <asp:DropDownList ID="dpDelete" runat="server" CssClass="dropdown">
                                        </asp:DropDownList></td>
                                    <td align="left" style="width: 100px">
                                        <asp:Button ID="cmdView" runat="server" CssClass="boton" OnClick="cmdView_Click"
                                            Text="View details" Visible="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 23px">
                                    </td>
                                    <td style="width: 100px; height: 23px" align="center">
                                        <asp:Label ID="Label13" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red"
                                            Visible="false" Text="Label"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 100px; height: 23px">
                                        <asp:Button ID="cmdDelete" runat="server" CssClass="boton" OnClick="cmdDelete_Click"
                                            Text="Delete" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label ID="Label8" runat="server" Text="Prefix:" CssClass="labelBlue8"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 100px">
                                        <asp:Label ID="lblPrefix" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                    <td align="left" style="width: 100px">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label ID="Label9" runat="server" Text="ID:" CssClass="labelBlue8"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 100px">
                                        <asp:Label ID="lblID" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                    <td style="width: 100px">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label ID="Label10" runat="server" Text="Type:" CssClass="labelBlue8"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 100px">
                                        <asp:Label ID="lblType" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                    <td style="width: 100px">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                        <asp:Label ID="Label11" runat="server" Text="Description" CssClass="labelBlue8"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 100px">
                                        <asp:Label ID="lblDesc" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                    <td style="width: 100px">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="width: 100px">
                                    </td>
                                    <td align="left" style="width: 100px">
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="width: 100px">
                                    </td>
                                    <td align="center" style="width: 100px">
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager></td>
        </tr>
        
    </table>
</asp:Content>
