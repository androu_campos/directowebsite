using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SignIn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Redirect("http://drpmecca.directo.com/MeccaDirecto");
        try
        {
            txtUser.Focus();
            if (Request.Cookies["userMecca"] != null && Request.Cookies["passMecca"] != null)
            {
                string UserCooky = Request.Cookies["userMecca"].Value;
                string PassCooky = Request.Cookies["passMecca"].Value;
               

                //cdrdbDatasetTableAdapters.loginAdp ADPl = new cdrdbDatasetTableAdapters.loginAdp();
                //cdrdbDataset.loginDataTable tbl = ADPl.GetDataByUserPass(UserCooky, PassCooky);
                cdrdbDatasetTableAdapters.loginAdp ADPl = new cdrdbDatasetTableAdapters.loginAdp();
                cdrdbDataset.loginDataTable tbl = ADPl.GetDataByUserPassCaseSens(UserCooky, PassCooky);
                
                
                
                if (tbl.Count > 0)//usuario logeado
                {
                    Session["Rol"] = tbl.Rows[0]["Rol"].ToString();                   
                    Session["DirectoUser"] = tbl.Rows[0]["username"].ToString();
                    Session["BrokerName"] = tbl.Rows[0]["BrokerName"].ToString();

                    SessionUsuario SesUs= new SessionUsuario();

                    SesUs.Rol = tbl.Rows[0]["Rol"].ToString();
                    SesUs.DirectoUser = tbl.Rows[0]["username"].ToString();
                    SesUs.BrokerName = tbl.Rows[0]["BrokerName"].ToString();                   
                    Response.Redirect("Home.aspx",false);
                }
                else
                {
                    lblWrongLog.Visible = true;
                    txtUser.Focus();

                }

            }
            else
            {
                txtUser.Focus();                
            }
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }
        
        
    }   
    
    protected void LoginButton_Click(object sender, EventArgs e)
    {
        try
        {
            string UserId;
            string Password;
            HttpCookie username;
            HttpCookie pass;
            HttpCookie rol;
            HttpCookie broker;

            UserId = txtUser.Text;
            Password = txtPassword.Text;           
            
            cdrdbDatasetTableAdapters.loginAdp ADPl = new cdrdbDatasetTableAdapters.loginAdp();
            cdrdbDataset.loginDataTable tbl = ADPl.GetDataByUserPassCaseSens(UserId, Password);


            if (tbl.Count > 0)//usuario logeado
            {
                
                Session["Rol"] = tbl.Rows[0]["Rol"].ToString();                
                Session["DirectoUser"] = tbl.Rows[0]["username"].ToString();
                Session["BrokerName"] = tbl.Rows[0]["BrokerName"].ToString();
                                               
		        if (RememberMe.Checked)//quiere recordar contraseņa
                {
//                    rol["rol"] = UserId;
                    //rol.Expires = DateTime.Now.AddDays(365);
                    //Response.Cookies.Add(rol);

                    username = new HttpCookie("userMecca");
                    username.Value = UserId;
                    username.Expires = DateTime.Now.AddDays(365);
                    pass = new HttpCookie("passMecca");
                    pass.Value = Password;
                    pass.Expires = DateTime.Now.AddDays(365);
                    rol = new HttpCookie("Rol");
                    rol.Value = Session["Rol"].ToString();
                    rol.Expires = DateTime.Now.AddDays(365);
                    broker = new HttpCookie("BrokerName");
                    broker.Value = Session["BrokerName"].ToString();
                    broker.Expires = DateTime.Now.AddDays(365);

                    Response.Cookies.Add(username);
                    Response.Cookies.Add(pass);
                    Response.Cookies.Add(rol);
                    Response.Cookies.Add(broker);
                    
                    //Response.Cookies["userMecca"].Value = UserId;
                    //Response.Cookies["userMecca"].Expires = DateTime.Now.AddDays(365);
                    
                    //Response.Cookies["passMecca"].Value = Password;
                    //Response.Cookies["passMecca"].Expires = DateTime.Now.AddDays(365);		            
                    
                    //Response.Cookies["Rol"].Value = Session["Rol"].ToString();
                    //Response.Cookies["Rol"].Expires = DateTime.Now.AddDays(365);

                    //Response.Cookies["BrokerName"].Value = Session["BrokerName"].ToString();
                    //Response.Cookies["Rol"].Expires = DateTime.Now.AddDays(365);

                    Session["RememberMe"] = "ON";

                }

                if (Session["Rol"].ToString() == "Customer")
                {
                    Response.Redirect("Paccess.aspx", false);
                }
                else if (Session["Rol"].ToString() == "CustomerCDRS")
                {
                    Response.Redirect("CdrsDownloader.aspx", false);
                }
                else if (Session["Rol"].ToString() == "Vendor")
                {
                    Response.Redirect("VendorHome.aspx", false);
                }
                else if (Session["Rol"].ToString() == "VendorCDRS")
                {
                    Response.Redirect("CdrsVDownloader.aspx", false);
                }
                else if (Session["Rol"].ToString() == "PHAccess")
                {
                    Response.Redirect("HomePh.aspx", false);
                }
                else if (Session["Rol"].ToString() == "CallCenter")
                {
                    Response.Redirect("HomeCC.aspx", false);
                }
                else if (Session["Rol"].ToString() == "CallCenterCust")
                {
                    Response.Redirect("HomeCCCust.aspx", false);
                }
                else if (Session["Rol"].ToString() == "CallCenterPH")
                {
                    Response.Redirect("HomeCCPH.aspx", false);
                }
                else if (Session["DirectoUser"].ToString() == "jmanatlao" || Session["DirectoUser"].ToString() == "csph")
                {
                    Response.Redirect("HomeJun.aspx", false);
                }
                //else if (Session["DirectoUser"].ToString() == "jim.baumhart")
                //{
                //    Response.Redirect("HomeCM.aspx", false);
                //}
                else
                {
                    Response.Redirect("Home.aspx", false);
                }
            }
            else
            {
                lblWrongLog.Visible = true;
            }
        }
        catch(Exception ex)
        {
            string message = ex.Message + " Source: " + ex.Source + " Trace:" + ex.StackTrace + " link:" + ex.HelpLink;                
        }
    }
}
