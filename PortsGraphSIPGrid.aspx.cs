using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;
using System.Data.SqlClient;


public partial class PortsGraph : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string user = "";

        user = Session["DirectoUser"].ToString();

        if (!Page.IsPostBack)
        {
            filldropdowns(user);
        }

        //double[] yValues = { 155, 6997, 35, 3, 39144, 15, 655, 215264, 32, 6223, 5, 8780, 8, 36 };
        //string[] xValues = { "404", "408", "415", "433", "480", "484", "486", "487", "488", "500", "502", "503", "504" , "603"};
        //Chart2.Series["Attempts"].Points.DataBindXY(xValues, yValues);
    }

    private void filldropdowns(string user)
    {

        SqlConnection SqlConn1 = new SqlConnection();
        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand SqlCommand1 = null;

        string cmd = string.Empty;
        cmd = "SELECT DISTINCT PROVIDERNAME INTO #CustomerList FROM CDRDB.DBO.PROVIDERIP WHERE [TYPE] = 'C' \n ";
        cmd += "INSERT INTO #CustomerList SELECT '**ALL**' \n ";
        cmd += "UPDATE #CustomerList SET Providername = 'ICS-CTR' WHERE Providername like 'ICS-CTR' \n ";
        cmd += "UPDATE #CustomerList SET Providername = 'ICS' WHERE Providername like 'ICS%' and Providername not like 'ICS-CTR' \n ";
        //cmd += "UPDATE #CustomerList SET Providername = 'TALKTEL' WHERE Providername IN (SELECT CUSTOMER FROM CDRDB.DBO.CUSTBROKERREL WHERE CUSTBROKER = 'CallCenters' AND CUSTOMER NOT LIKE 'CCPH%' AND CUSTOMER NOT LIKE 'CC-PHIP%' ) \n ";
        cmd += "INSERT INTO #CustomerList SELECT 'ALLACCESS-CCPH06' \n ";
        cmd += "SELECT DISTINCT Providername FROM #CustomerList order by ProviderName \n ";

        SqlConn1.Open();

        SqlCommand1 = new SqlCommand(cmd, SqlConn1);
        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
        while (myReader1.Read())
        {
            dpdCustomer.Items.Add(myReader1.GetValue(0).ToString());
        }
        myReader1.Close();

        //VENDOR
        SqlCommand SqlCommand2 = null;
        SqlCommand2 = new SqlCommand("SELECT DISTINCT ProviderName FROM ProviderIP WHERE Type like 'V' UNION SELECT '**ALL**' FROM ProviderIP as ProviderName UNION SELECT 'NATIONAL ALL' FROM ProviderIP as ProviderName UNION SELECT 'OUTLANDER-ALL' FROM ProviderIP as ProviderName order by ProviderName", SqlConn1);
        SqlDataReader myReader2 = SqlCommand2.ExecuteReader();
        while (myReader2.Read())
        {
            dpdVendor.Items.Add(myReader2.GetValue(0).ToString());
        }
        myReader2.Close();

        SqlCommand SqlCommand3 = null;
        SqlCommand3 = new SqlCommand("SELECT Country FROM CountryCodes UNION SELECT '**ALL**' FROM CountryCodes as Country order by Country", SqlConn1);
        SqlDataReader myReader3 = SqlCommand3.ExecuteReader();
        while (myReader3.Read())
        {
            dpdCountry.Items.Add(myReader3.GetValue(0).ToString());
        }
        myReader3.Close();

        //LMC
        SqlCommand SqlCommand4 = new SqlCommand("SELECT DISTINCT LMC FROM Regions WHERE LMC is not NULL UNION SELECT '**ALL**' AS LMC FROM Regions ORDER BY LMC", SqlConn1);
        SqlDataReader myReader4 = SqlCommand4.ExecuteReader();
        while (myReader4.Read())
        {
            dpdLMC.Items.Add(myReader4.GetValue(0).ToString());
        }
        myReader4.Close();

        //Type
        SqlCommand SqlCommand5 = new SqlCommand("select distinct Regions.Type FROM Regions union select '**ALL**' as Type FROM Regions order by Type ASC", SqlConn1);
        SqlDataReader myReader5 = SqlCommand5.ExecuteReader();
        while (myReader5.Read())
        {
            dpdType.Items.Add(myReader5.GetValue(0).ToString());
        }

        myReader5.Close();

        //Cust SIP CAUSE
        SqlCommand SqlCommand6 = new SqlCommand("select SIPCauseCode FROM CDRDB.DBO.SIPCauseCodes UNION SELECT '**ALL**' order by SIPCauseCode ASC", SqlConn1);
        SqlDataReader myReader6 = SqlCommand6.ExecuteReader();
        while (myReader6.Read())
        {
            dpdCSIPCause.Items.Add(myReader6.GetValue(0).ToString());
        }

        myReader6.Close();

        //Vend SIP CAUSE
        SqlCommand SqlCommand7 = new SqlCommand("select SIPCauseCode FROM CDRDB.DBO.SIPCauseCodes UNION SELECT '**ALL**' order by SIPCauseCode ASC", SqlConn1);
        SqlDataReader myReader7 = SqlCommand6.ExecuteReader();
        while (myReader7.Read())
        {
            dpdVSIPCause.Items.Add(myReader7.GetValue(0).ToString());
        }

        myReader7.Close();

        SqlConn1.Close();
    }

    protected void cmdReport_Click(object sender, EventArgs e)
    {
        string Customer = dpdCustomer.SelectedValue.ToString();
        string Vendor = dpdVendor.SelectedValue.ToString();
        string type = dpdType.SelectedValue.ToString();
        string lmc = dpdLMC.SelectedValue.ToString();
        string country = dpdCountry.SelectedValue.ToString();
        string CSIPCause = dpdCSIPCause.SelectedValue.ToString();
        string VSIPCause = dpdVSIPCause.SelectedValue.ToString();
        string typeRep = dpdRtype.SelectedValue.ToString();
        
        string user = Session["DirectoUser"].ToString();
        string mrol = Session["Rol"].ToString();

        //Customer = Customer.Replace("**ALL**", "%");
        //Vendor = Vendor.Replace("**ALL**", "%");
        //type = type.Replace("**ALL**", "%");
        //lmc = lmc.Replace("**ALL**", "%");
        //country = country.Replace("**ALL**", "%");
        //CSIPCause = CSIPCause.Replace("**ALL**", "%");
        //VSIPCause = VSIPCause.Replace("**ALL**", "%");      

        string query = "";
        string query2 = "";
        string queryw = "";
        string group = "";
        string group2 = "";

        DataTable tbl;
        DataTable tbl2;
        
        if (typeRep == "OpSheetAllH") //Todays
        {
            if (CSIPCause == "**ALL**" && VSIPCause == "**ALL**")
            {
                query = "SELECT custsipcause as SIPCause, SUM(Attempts) Attempts, Definition ";
                query += "FROM MECCA2.dbo.OpSheetAllH ";
                query += "join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode ";
                //query += "WHERE CustSipCause not like '200' ";

                group = "GROUP BY CustSipCause, Definition ORDER BY CustSIPCause ";

                query2 = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition ";
                query2 += "FROM MECCA2.dbo.OpSheetAllH ";
                query2 += "join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode ";
                //query2 += "WHERE VendSipCause not like '200' ";
                
                group2 = "GROUP BY VendSipCause, Definition ORDER BY VendSIPCause ";
            }
            else if (CSIPCause != "**ALL**" || VSIPCause != "**ALL**")
            {
                query = "SELECT Hour as SIPCause, SUM(Attempts) Attempts, Definition ";
                query += "FROM MECCA2.dbo.OpSheetAllH ";
                query += "join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode ";
                query += "WHERE CustSIPCause = '" + CSIPCause.Replace("**ALL**", "%") + "' ";

                group = "GROUP BY Hour, Definition ORDER BY Hour ";

                query2 = "SELECT Hour as SIPCause, SUM(Attempts) Attempts, Definition ";
                query2 += "FROM MECCA2.dbo.OpSheetAllH ";
                query2 += "join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode ";
                query2 += "WHERE VendSIPCause = '" + VSIPCause.Replace("**ALL**", "%") + "' ";
                //query2 += "and VendSipCause like '" + CSIPCause.Replace("**ALL**", "%") + "' ";

                group2 = "GROUP BY Hour, Definition ORDER BY Hour";
            }

            else if (VSIPCause != "**ALL**" || CSIPCause != "**ALL**")
            {
                query = "SELECT custsipcause as SIPCause, SUM(Attempts) Attempts, Definition ";
                query += "FROM MECCA2.dbo.OpSheetAll ";
                query += "join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode ";
                query += "WHERE CustSipCause like '" + CSIPCause.Replace("**ALL**", "%") + "' "; 
                //query += "and CustSipCause not like '200' ";

                group = "GROUP BY CustSipCause, Definition ORDER BY CustSipCause ";

                query2 = "SELECT VendSipCause as SIPCause, SUM(Attempts) Attempts, Definition ";
                query2 += "FROM MECCA2.dbo.OpSheetAll ";
                query2 += "join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode ";
                query2 += "WHERE VendSipCause like '" + VSIPCause.Replace("**ALL**", "%") + "' ";
                //query2 += "and VendSipCause not like '200' ";

                group2 = "GROUP BY VendSipCause, Definition ORDER BY VendSipCause ";
            }
        }
        else if (typeRep == "OpSheetINC") //RECENT
        {
            if (CSIPCause == "**ALL**" && VSIPCause == "**ALL**")
            {
                query = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition ";
                query += "FROM MECCA2.dbo.OpSheetINC ";
                query += "join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode ";
                //query += "WHERE CustSipCause not like '200' ";
                //query += "and CustSipCause like '" + CSIPCause.Replace("**ALL**", "%") + "' ";

                group = "GROUP BY CustSipCause, Definition ORDER BY CustSipCause";

                query2 = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition ";
                query2 += "FROM MECCA2.dbo.OpSheetINC ";
                query2 += "join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode ";
                //query2 += "WHERE VendSipCause not like '200' ";
                //query2 += "and VendSipCause like '" + CSIPCause.Replace("**ALL**", "%") + "' ";

                group2 = "GROUP BY VendSipCause, Definition ORDER BY VendSipCause";
            }
            else if (VSIPCause != "**ALL**" || CSIPCause != "**ALL**")
            {
                query = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition ";
                query += "FROM MECCA2.dbo.OpSheetINC ";
                query += "join CDRDB.dbo.SIPCauseCodes on CustSipCause = SIPCauseCode ";
                query += "WHERE CustSipCause like '" + VSIPCause.Replace("**ALL**", "%") + "' ";
                //query += "and CustSipCause not like '200' ";

                group = "GROUP BY VendSipCause, Definition ORDER BY VendSipCause";

                query2 = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition ";
                query2 += "FROM MECCA2.dbo.OpSheetINC ";
                query2 += "join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode ";
                query2 += "WHERE VendSipCause like '" + VSIPCause.Replace("**ALL**", "%") + "' ";
                //query2 += "and VendSipCause not like '200' ";

                group2 = "GROUP BY VendSipCause, Definition ORDER BY VendSipCause";
            }
        }
        else if (typeRep == "OpSheetYH") //Yesterdays
        {
            if (CSIPCause == "**ALL**" && VSIPCause == "**ALL**")
            {
                query = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition ";
                query += "FROM MECCA2.dbo.OpSheetY ";
                query += "join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode ";
                //query += "WHERE CustSipCause not like '200' ";
                //query += "and CustSipCause like '" + CSIPCause.Replace("**ALL**", "%") + "' ";

                group = "GROUP BY CustSipCause, Definition ORDER BY CustSipCause";

                query2 = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition ";
                query2 += "FROM MECCA2.dbo.OpSheetY ";
                query2 += "join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode ";
                //uery2 += "WHERE VendSipCause not like '200' ";
                //query2 += "and VendSipCause like '" + CSIPCause.Replace("**ALL**", "%") + "' ";

                group2 = "GROUP BY VendSipCause, Definition ORDER BY VendSipCause";
            }
            else if (VSIPCause != "**ALL**" || CSIPCause != "**ALL**")
            {
                query = "SELECT Hour as SIPCause, SUM(Attempts) Attempts, Definition ";
                query += "FROM MECCA2.dbo.OpSheetYH ";
                query += "join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode ";
                query += "WHERE CustSIPCause = '" + CSIPCause.Replace("**ALL**", "%") + "' ";

                group = "GROUP BY Hour, Definition ORDER BY Hour ";

                query2 = "SELECT Hour as SIPCause, SUM(Attempts) Attempts, Definition ";
                query2 += "FROM MECCA2.dbo.OpSheetYH ";
                query2 += "join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode ";
                query2 += "WHERE VendSIPCause = '" + VSIPCause.Replace("**ALL**", "%") + "' ";
                //query2 += "and VendSipCause like '" + CSIPCause.Replace("**ALL**", "%") + "' ";

                group2 = "GROUP BY Hour, Definition ORDER BY Hour";
            }
        }
        queryw += "and Customer like '" + Customer.Replace("**ALL**", "%") +"' ";
        queryw += "and vendor like '" + Vendor.Replace("**ALL**", "%") + "' ";
        queryw += "and Country like '" + country.Replace("**ALL**", "%") + "' ";
        queryw += "and LMC like '" + lmc.Replace("**ALL**", "%") + "' ";
        queryw += "and Type like '" + type.Replace("**ALL**", "%") + "' ";
        
        query = query + queryw + group;

        query2 = query2 +queryw + group2;

        lblWarning.Text = query + " " + query2;
        lblWarning.Visible = true;

        //Response.Write(query + "<br /> " + query2);
        //Response.End();
        
        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        tbl2 = Util.RunQueryByStmntatCDRDB(query2).Tables[0];

        makeChart(tbl, tbl2 ,"Customer: " + Customer + " Vendor: " + Vendor);
        
        typeRep = typeRep.Replace("OpSheetAllH","Today");
        typeRep = typeRep.Replace("OpSheetINC", "Recent");
        typeRep = typeRep.Replace("OpSheetY", "Yesterdays");

        SqlCommand qryCommand = new SqlCommand();
        query = "INSERT INTO DirectoReports.dbo.QueryLog VALUES (getdate(),'PortsGraphSIP.aspx, Customer:" + Customer + ", Vendor:" + Vendor + ", Country: " + country + ", LMC: " + lmc + ", Type: " + type + ", CSIPCause:" + CSIPCause + ", VSIPCause: " + VSIPCause + ", TypeReport: " + typeRep + "','" + user + "','" + mrol + "')";
        qryCommand.CommandText = query;
        RunQuery(qryCommand);

        qryCommand.Connection.Close();
    }

    private void makeChart(DataTable tbl, DataTable tbl2, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0 || tbl2.Rows.Count > 0)
        {
            yValues = new double [tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }

            

            Chart2.Series["Attempts Customer"].Points.DataBindXY(xValues, yValues);
            Chart2.Series["Attempts Customer"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";

            yValues = new double[tbl2.Rows.Count];
            xValues = new string[tbl2.Rows.Count];
            i = 0;

            foreach (DataRow myRow in tbl2.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }

            Chart2.Series["Attempts Vendor"].Points.DataBindXY(xValues, yValues);
            Chart2.Series["Attempts Vendor"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";

            this.cmdDetail.Visible = true;
            lblWarning.Visible = false;

            Session["tblDetail"] = tbl;
            Session["tblDetail2"] = tbl2;
        }
        

        //if (tbl.Rows.Count > 0)
        //{
        //    int pdx;
        //    string script = string.Empty;
        //    string script2 = string.Empty;

        //    //this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

        //    foreach (DataRow myRow in tbl.Rows)
        //    {

        //        pdx = Chart1.Series["C SIP Cause"].Points.AddXY(myRow["SIPCause"],Convert.ToDouble(myRow["Attempts"]));
        //        script = "onclick=dispDataC('" + myRow["SIPCause"].ToString() + "','" + myRow["Definition"].ToString() + "','" + myRow["Attempts"] + "','P')";
        //        script = script.Replace(" ", "");
        //        this.Chart1.Series["C SIP Cause"].Points[pdx].MapAreaAttributes = script;
        //        Chart1.Series["C SIP Cause"]["PointWidth"] = "3";
                
        //    }

        //    foreach (DataRow myRow in tbl2.Rows)
        //    {

        //        pdx = Chart1.Series["V SIP Cause"].Points.AddXY(myRow["SIPCause"], Convert.ToDouble(myRow["Attempts"]));
        //        script = "onclick=dispDataV('" + myRow["SIPCause"].ToString() + "','" + myRow["Definition"].ToString() + "','" + myRow["Attempts"] + "','P')";
        //        script = script.Replace(" ", "");
        //        this.Chart1.Series["V SIP Cause"].Points[pdx].MapAreaAttributes = script;
        //        Chart1.Series["V SIP Cause"]["PointWidth"] = "3";
        //    }

            
        //    Chart1.Series[0]["DrawSideBySide"] = "auto";
        //    //this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
        //    DataPoint maxpointY = this.Chart1.Series["C SIP Cause"].Points.FindMaxValue("Y");
        //    //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
        //    this.Chart1.DataBind();
        //    this.Chart1.Titles.Add(tITLE);
        //    this.Chart1.RenderType = RenderType.ImageTag;
        //    Session["tblDetail"] = tbl;
        //    Session["tblDetail2"] = tbl2;

        //    this.Chart1.Visible = true;
        //    lblWarning.Visible = false;
        //    this.cmdDetail.Visible = true;
        //}
        else
        {
            this.Chart2.Visible = false;
            lblWarning.Visible = true;
            lblWarning.Text = "Nothing found!!";
            Session["tblDetail"] = tbl;
            this.cmdDetail.Visible = false;

        }

    }

    protected void cmdDetail_Click(object sender, EventArgs e)
    {
        gdvDetailC.DataSource = ((DataTable)Session["tblDetail"]);
        gdvDetailC.DataBind();

        gdvDetailV.DataSource = ((DataTable)Session["tblDetail2"]);
        gdvDetailV.DataBind();

        gdvDetailC.Visible = true;
        gdvDetailV.Visible = true;
        lblGrid.Visible = true;
        lblGrid.Text = "Customer: " + dpdCustomer.SelectedValue.ToString() + " Vendor: " + dpdVendor.SelectedValue.ToString();;
        cmdHide.Visible = true;
        cmdDetail.Visible = false;


    }

    protected void cmdHide_Click(object sender, EventArgs e)
    {
        gdvDetailC.Visible = false;
        gdvDetailV.Visible = false;
        lblGrid.Visible = false;
        cmdDetail.Visible = true;
        cmdHide.Visible = false;

    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}

