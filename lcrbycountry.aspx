<%@ Page Language="C#" MasterPageFile="~/Master.master" StylesheetTheme="Theme1"  AutoEventWireup="true" CodeFile="lcrbycountry.aspx.cs" Inherits="LCR_lcrbycountry" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="lblHeader" runat="server" Font-Bold="True" Text="Label" CssClass="labelTurkH" Width="212px"></asp:Label>
    <table width="100%" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td align="left" style="width: 100px">
                
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <asp:GridView ID="gdv2" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <Columns>
          
            <asp:BoundField DataField="Country" HeaderText="Country">
            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>
            
            <asp:BoundField DataField="Region" HeaderText="Region" AccessibleHeaderText="Region" >
                <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>
            
            <asp:BoundField DataField="Customer" HeaderText="Customer">
             <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />   
            </asp:BoundField>
            
            <asp:BoundField DataField="Price" HeaderText="Price" DataFormatString="${0:f4}" HtmlEncode="false">
            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>
            
            <asp:BoundField DataField="Vendor" HeaderText="Vendor">
            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>
            
            <asp:BoundField DataField="Cost" HeaderText="Cost" DataFormatString="${0:f4}" HtmlEncode="false">
            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>
            
            <asp:BoundField DataField="Minutes" HeaderText="Minutes" HtmlEncode="False" DataFormatString="{0:0,0}">
            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>
            
            <asp:BoundField DataField="Profit" HeaderText="Profit" DataFormatString="${0:f2}" HtmlEncode="false">
            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>         
            
            <asp:BoundField DataField="ASR" HeaderText="ASR" HtmlEncode="False" DataFormatString="{0:0,0}">
            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>
            
            <asp:BoundField DataField="ACD" HeaderText="ACD" HtmlEncode="False" DataFormatString="{0:0,0}">
            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
            </asp:BoundField>                                                                       
            
            <asp:HyperLinkField DataNavigateUrlFields="Country,Region" DataNavigateUrlFormatString="lcrdetail.aspx?Mode=L&amp;Country={0}&amp;Region={1}&amp;CCountry=N" Text="REGION LCR">
            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" ForeColor="#F38330" />
             <HeaderStyle HorizontalAlign="Left" />
                <ControlStyle CssClass="labelSteelBlue" Font-Size="Small" Font-Underline="True" />
            </asp:HyperLinkField>
            
            <asp:HyperLinkField DataNavigateUrlFields="Country,Region" DataNavigateUrlFormatString="lcrdetail.aspx?Mode=L&amp;Country={0}&amp;Region={1}&amp;CCountry=Y" Text="Country LCR">
            <ItemStyle Font-Names="Arial" Font-Size="9pt" ForeColor="#F38330" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                <ControlStyle CssClass="labelSteelBlue" Font-Size="Small" Font-Underline="True" />
            </asp:HyperLinkField>
                
        </Columns>
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#999999" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle Font-Bold="True" CssClass="titleOrangegrid" Font-Names="Arial" Font-Size="8pt" Height="40px" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
            </td>
        </tr>
    
        <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>   <tr></tr>
    
    </table>
    
</asp:Content>

