<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="VendorsEdit.aspx.cs" Inherits="VendorsEdit" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="left">
        <tr>
            <td align="left" style="width: 192px">
                <asp:Label ID="Label4" runat="server" CssClass="labelTurkH" Text="VENDOR EDIT"></asp:Label></td>
            <td align="left" style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 192px; height: 26px">
            </td>
            <td align="left" style="width: 100px; height: 26px">
            </td>
            <td style="width: 100px; height: 26px">
            </td>
        </tr>
        <tr>
            <td style="width: 192px" bgcolor="#d5e3f0">
                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Step 1"></asp:Label>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Choose Vendor:"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdVendor" runat="server" AutoPostBack="True" CssClass="dropdown"
                    OnSelectedIndexChanged="dpdProvider_SelectedIndexChanged" Width="157px">
                    <asp:ListItem Selected="True">** NONE **</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 192px" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Step 2"></asp:Label>
                &nbsp;<asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Enter new Provider Name:"></asp:Label></td>
            <td align="left" style="width: 100px">
                <asp:TextBox ID="txtName" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 192px" bgcolor="#d5e3f0">
                <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Step 3"></asp:Label>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Enter new IP:"></asp:Label></td>
            <td align="left" style="width: 100px">
                <asp:TextBox ID="txtIP" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 192px; height: 21px;">
            </td>
            <td align="left" style="width: 100px; height: 21px;">
            </td>
            <td style="width: 100px; height: 21px;">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblUpdated" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="8pt"
                    ForeColor="Red" Text="Vendor updated !!" Visible="False"></asp:Label></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                &nbsp;<asp:Button ID="Button1" runat="server" Font-Names="Arial" Font-Size="8pt"
                    OnClick="Button1_Click" Text="Update" CssClass="boton" /></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                &nbsp;</td>
            <td colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" style="height: 26px">
                &nbsp;</td>
            <td colspan="1" style="height: 26px">
            </td>
        </tr>
    </table>
</asp:Content>

