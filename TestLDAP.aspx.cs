using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.DirectoryServices;

public partial class TestLDAP : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string username = "randrade";
        string pwd = "69rand45\"";
        string strPath = "LDAP://mecca.directo.com/DC=mecca.directo,DC=com";
        string strDomain = "directo";
        string domainAndUsername = strDomain + @"\" + username;

        //DirectoryEntry entry = new DirectoryEntry(strPath, domainAndUsername, pwd,AuthenticationTypes.Secure);
        DirectoryEntry entry = new DirectoryEntry(strPath);
        DirectorySearcher search = new DirectorySearcher(entry);
        search.Filter = "name=" + username;
        SearchResultCollection results = search.FindAll();

        foreach (SearchResult resultados in results)
        {
            ResultPropertyCollection colProperties = resultados.Properties;

            foreach (string key in colProperties.PropertyNames)
            {
                foreach (object value in colProperties[key])
                {
                    Response.Write("" + key.ToString() + ": " + value + " ");
                }
            }
        }
    }

}
