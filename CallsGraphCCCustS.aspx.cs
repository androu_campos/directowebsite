using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;


public partial class PortsGraphCCCust : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();
        }

    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {

        DataTable tbl;
        string username = string.Empty;

        username = Session["DirectoUser"].ToString();

        string from = txtFrom.Text;
        string to = txtTo.Text;

        string agency = dpdAgen.SelectedValue.ToString();
        string minicall = dpdCC.SelectedValue.ToString();

        string query = string.Empty;
        string title = string.Empty;


        if (agency == "**ALL**" && minicall == "**ALL**")
        {

            query = "SELECT Sum([Calls]) as Live_Calls,[Time] FROM [MECCA2].[dbo].[TrafficLogLive] WHERE [Customer] in (select ProviderName from cdrdb..providerip where (ProviderName like 'CC-%SAN%' or ProviderName like 'CC-%-SAN%') and [Type] like 'C%') and customer not in ('CC-CORTIZO-SANTANDER','CC-CORTIZO-SANTAN044','PALO-SANTO-TEST') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
            title = "Customer: Santander";

        }
        else if (agency != "**ALL**")
        {
            if (minicall != "**ALL**")
            {
                query = "SELECT sum([Calls]) as Live_Calls,[Time] FROM [MECCA2].[dbo].[TrafficLogLive] WHERE [Customer] like '" + minicall + "' and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                title = "Customer: " + minicall;
            }
            else
            {
                query = "SELECT sum([Calls]) as Live_Calls,[Time] FROM [MECCA2].[dbo].[TrafficLogLive] WHERE [Customer] in (select ProviderName from cdrdb..providerip where ProviderName like 'CC-" + agency + "SAN%' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                title = "Customer: " + agency;
            }
        }

        

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
        else cmdDetail.Visible = true;

        makeChart(tbl, title, from, to);
        //gdvDetail.Visible = false;
        lblGrid.Visible = false;

    }

    private void makeChart(DataTable tbl, string tITLE, string from, string to)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Calls"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Calls"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Calls"].Points.FindMaxValue("Y");
            DataPoint maxpoint = this.Chart1.Series["Calls"].Points.FindMaxValue();

            string origen = string.Empty;
            string to2 = string.Empty;
            //set the Title
            this.Chart1.Titles.Add(tITLE);
            //draw the MaxLine            

            origen = from + " 00:00:00";
            to2 = to + " 23:59:59";

            this.Chart1.Series["MaxCalls"].Points.AddXY(Convert.ToDateTime(to2), Convert.ToDouble(maxpointY.YValues[0]));
            this.Chart1.Series["MaxCalls"].Points.AddXY(Convert.ToDateTime(origen), Convert.ToDouble(maxpointY.YValues[0]));


            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetail"] = tbl;

            this.Chart1.Visible = true;
            lblWarning.Visible = false;
            this.cmdDetail.Visible = true;
        }
        else
        {
            this.Chart1.Visible = true;
            lblWarning.Visible = true;
            Session["tblDetail"] = tbl;
            this.cmdDetail.Visible = false;
            this.Chart1.Titles.Add(tITLE);
        }

    }

    protected void cmdDetail_Click(object sender, EventArgs e)
    {
        if (((DataTable)Session["tblDetail"]).Rows.Count > 0)
        {

            gdvDetail.DataSource = ((DataTable)Session["tblDetail"]);
            gdvDetail.DataBind();

            gdvDetail.Visible = true;
            lblGrid.Visible = true;
            lblGrid.Text = dpdAgen.SelectedValue.ToString();
            cmdHide.Visible = true;
            cmdDetail.Visible = false;
        }
        else
        {
        }
    }

    protected void cmdHide_Click(object sender, EventArgs e)
    {
        gdvDetail.Visible = false;
        lblGrid.Visible = false;
        cmdDetail.Visible = true;
        cmdHide.Visible = false;

    }

    protected void dpdAgen_SelectedIndexChanged(object sender, EventArgs e)
    {
        dpdCC.Enabled = true;

        string agen = dpdAgen.SelectedValue.ToString();

        SqlConnection SqlConn1 = new SqlConnection();
        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand SqlCommand1 = new SqlCommand();

        if (dpdAgen.SelectedValue.ToString() == "VCIP")
        {
            SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE (PROVIDERNAME LIKE 'CC-" + agen + "SAN%' or PROVIDERNAME LIKE 'CC-" + agen + "-SAN%') AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
        }
        else
        {
            SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-" + agen + "SAN%' AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
        }

        SqlCommand1.Connection = SqlConn1;
        SqlConn1.Open();

        dpdCC.Items.Clear();

        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();

        while (myReader1.Read())
        {
            
            dpdCC.Items.Add(myReader1.GetValue(0).ToString());
        }
        myReader1.Close();
        SqlConn1.Close();
    }

}

