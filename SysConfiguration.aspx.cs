using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class SysConfiguration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["sessionId"] = Session.SessionID;

        getCustomers();

        System.Web.UI.WebControls.ListBox listbox1 = (ListBox)Accordion1.FindControl("ListBox1");
        System.Web.UI.WebControls.ListBox listbox2 = (ListBox)Accordion1.FindControl("ListBox2");
        System.Web.UI.WebControls.CheckBoxList checkboxlist1 = (CheckBoxList)Accordion1.FindControl("CheckBoxList1");

        listbox1.Enabled = false;
        listbox2.Enabled = false;
        checkboxlist1.Enabled = false;

        //System.Web.UI.Control lblima2 = Accordion1.FindControl("lblimg2");
        //lblima2.Visible = false;

        //System.Web.UI.Control ima1 = Accordion1.FindControl("Image1");
        //ima1.Visible = false;


        
        //divCustomersButton.Visible = false;
        
    }

    protected void Convert(object sender, EventArgs e)
    {
        //string s = TextBox1.Text;        
        //string sid = string.Empty;
        //s = s.Replace("\r\n","</br>");

        //Session["bodyPreview"] = s;        
           

    }

    protected void Save(object sender, EventArgs e)
    {

    }

    protected void Upload(object sender, EventArgs e)
    {
        //string sid = string.Empty;
        //sid = Session["sessionId"].ToString();
        //FU.SaveAs(@"E:\Mecca\WebSite\DIRECTO_\Pricing\Images\BodySignature\" + sid + ".jpg");
        //Image1.ImageUrl="~/Pricing/Images/BodySignature/" + sid + ".jpg";
        
        //lblimg2.Visible = true;
        //Image1.Visible = true;
        //divCustomersButton.Visible = true;
    }

    protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
        int flag = 0;
        System.Web.UI.WebControls.ListBox listbox1 = (ListBox)Accordion1.FindControl("ListBox1");
        System.Web.UI.WebControls.ListBox listbox2 = (ListBox)Accordion1.FindControl("ListBox2");

        for (int i = 0; i < listbox2.Items.Count; i++)
        {
            if (listbox2.Items[i].Text == listbox1.SelectedItem.Text)
            {
                flag = 1;
            }
        }

        if (flag == 0)
        {
            listbox2.Items.Add(new ListItem(listbox1.SelectedItem.Text, listbox1.SelectedValue));
        }

        lblcountcustomers.Text = "Total: " + listbox2.Items.Count + " Customers";
    }

    protected void Button1_OnClick(object sender, EventArgs e)
    {
        //ListBox2.Items.Clear();
    }

    protected void Button2_OnClick(object sender, EventArgs e)
    {
        //if (ListBox2.Items.Count > 0 && ListBox2.Items[0] != null)
        //{
        //    ListBox2.Items.RemoveAt(ListBox2.SelectedIndex);
        //    lblcountcustomers.Text = "Total: " + ListBox2.Items.Count + " Customers";
        //}
    }

    protected void cvListbox_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        //args.IsValid = ListBox2.Items.Count > 0;
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        //string coo = "";
        //bool cont = false;
        //String connectionString;
        //SqlConnection DBConnection;
        //SqlDataAdapter dbAdapter;
        //SqlCommand sQuery;
        //DataSet resultSet;

        //if (!FU.HasFile)
        //{
        //    lblUpload.Visible = true;
        //    lblUpload.Text = "File not loaded";
        //}
        //else
        //{
        //    //File.Delete(@"D:\DIRECTO_\Pricing\RAFiles\ExceptionCustomers.csv");

        //    Process oldFiles = new Process();
        //    ProcessStartInfo oldFilesStartInfo = new ProcessStartInfo();

        //    oldFilesStartInfo.WorkingDirectory = "E:\\Mecca\\WebSite\\DIRECTO_\\Pricing\\RAFiles\\Templates\\";
        //    oldFilesStartInfo.FileName = "E:\\Mecca\\WebSite\\DIRECTO_\\Pricing\\RAFiles\\Templates\\deleteoldtempfile.bat";

        //    oldFiles.StartInfo = oldFilesStartInfo;

        //    oldFiles.Start();

        //    oldFiles.WaitForExit();

        //    oldFiles.Close();
        //    oldFiles.Dispose();

        //    FU.SaveAs(@"E:Mecca\WebSite\DIRECTO_\Pricing\RAFiles\Templates\" + FU.FileName.ToString());
        //    lblUpload.Visible = true;
        //    lblUpload.Text = "File " + FU.FileName.ToString() + " loaded succesfully";

        //    connectionString = ConfigurationManager.ConnectionStrings["Pricing"].ConnectionString;
        //    DBConnection = new SqlConnection(connectionString);
        //    dbAdapter = new SqlDataAdapter();
        //    sQuery = new SqlCommand();
        //    sQuery.Connection = DBConnection;
        //    sQuery.CommandType = System.Data.CommandType.Text;
        //    sQuery.CommandText = "sp_RatesTemplateRunDTSX";
        //    dbAdapter.SelectCommand = sQuery;
        //    resultSet = new DataSet();
        //    dbAdapter.Fill(resultSet);

        //    if (resultSet.Tables.Count > 0)
        //    {

        //        if (resultSet.Tables[1].Rows.Count > 0)
        //        {
        //            foreach (DataRow row in resultSet.Tables[1].Rows)
        //            {

        //                coo = row["corre"].ToString();
        //                if (coo != "0")
        //                    lblUpload.Text = "Failed !!!";


        //            }
        //        }
        //    }

        //    if (coo == "0")
        //    {
        //        connectionString = ConfigurationManager.ConnectionStrings["Pricing"].ConnectionString;
        //        DBConnection = new SqlConnection(connectionString);
        //        dbAdapter = new SqlDataAdapter();
        //        sQuery = new SqlCommand();
        //        sQuery.Connection = DBConnection;
        //        sQuery.CommandType = System.Data.CommandType.Text;
        //        sQuery.CommandText = "SELECT * FROM PRICING..RatesTemplateGood";
        //        dbAdapter.SelectCommand = sQuery;
        //        resultSet = new DataSet();
        //        dbAdapter.Fill(resultSet);

        //        if (resultSet.Tables.Count > 0)
        //        {
        //            if (resultSet.Tables[0].Rows.Count > 0)
        //            {
        //                gvGood.DataSource = resultSet;
        //                gvGood.DataBind();
        //                gvGood.Visible = true;
        //                lblgood.Visible = true;
        //                Session["goodRecords"] = resultSet;
        //                btnContinue.Visible = true;
        //                ListBox1.Visible = true;
        //                ListBox2.Visible = true;
        //                Button1.Visible = true;
        //                Button2.Visible = true;
        //                Label1.Visible = true;
        //                cvListBox2.Visible = true;
        //                cont = true;
        //            }
        //        }




        //        sQuery.CommandText = "SELECT * FROM PRICING..RatesTemplateBad";
        //        dbAdapter.SelectCommand = sQuery;
        //        resultSet = new DataSet();
        //        dbAdapter.Fill(resultSet);

        //        if (resultSet.Tables.Count > 0)
        //        {
        //            gvBad.DataSource = resultSet;
        //            gvBad.DataBind();
        //            gvBad.Visible = true;
        //            lblbad.Visible = true;
        //            Session["badRecords"] = resultSet;
        //            cont = true;
        //        }


        //        sQuery.CommandText = "SELECT * FROM PRICING..RRatesTemplateDup";
        //        dbAdapter.SelectCommand = sQuery;
        //        resultSet = new DataSet();
        //        dbAdapter.Fill(resultSet);

        //        if (resultSet.Tables.Count > 0)
        //        {
        //            gvDup.DataSource = resultSet;
        //            gvDup.DataBind();
        //            gvDup.Visible = true;
        //            lblduplicated.Visible = true;
        //            Session["dupRecords"] = resultSet;
        //            cont = true;
        //        }

        //        sQuery.CommandText = "SELECT * FROM PRICING..RRatesTemplateDupDif";
        //        dbAdapter.SelectCommand = sQuery;
        //        resultSet = new DataSet();
        //        dbAdapter.Fill(resultSet);

        //        if (resultSet.Tables.Count > 0)
        //        {
        //            gvDupDif.DataSource = resultSet;
        //            gvDupDif.DataBind();
        //            gvDupDif.Visible = true;
        //            lblduplicatedDif.Visible = true;
        //            Session["dupDifRecords"] = resultSet;
        //            cont = true;
        //        }

        //        //if (cont == true)
        //        //{


        //        //}

        //    }

        //    //File.Delete(@"D:\DIRECTO_\Pricing\RAFiles\ExceptionCustomers.csv");
        //    lblCodesUploaded.Text = "";
        //    sQuery.CommandText = "SELECT count(*) FROM PRICING..RatesTemplateGood";
        //    dbAdapter.SelectCommand = sQuery;
        //    resultSet = new DataSet();
        //    dbAdapter.Fill(resultSet);

        //    lblCodesUploaded.Text = "Codes Loaded :" + resultSet.Tables[0].Rows[0][0].ToString();
        //    lblCodesUploaded.Visible = true;
        //    lblcountcustomers.Visible = true;


        //}
    }

    protected void btnContinue_Click(Object sender, EventArgs e)
    {

        //string coo = "";
        //string customers = "";
        //String connectionString;
        //SqlConnection DBConnection;
        //SqlDataAdapter dbAdapter;
        //SqlCommand sQuery;
        //DataSet resultSet;

        //if (ListBox2.Items.Count > 0)
        //{

        //    for (int i = 0; i < ListBox2.Items.Count; i++)
        //    {
        //        customers += ListBox2.Items[i].Value + ";";
        //    }

        //    customers = customers.Substring(0, customers.Length - 1);

        //    connectionString = ConfigurationManager.ConnectionStrings["Pricing"].ConnectionString;
        //    DBConnection = new SqlConnection(connectionString);
        //    dbAdapter = new SqlDataAdapter();
        //    sQuery = new SqlCommand();
        //    sQuery.Connection = DBConnection;
        //    sQuery.CommandTimeout = 1800;

        //    sQuery.CommandType = System.Data.CommandType.StoredProcedure;
        //    sQuery.CommandText = "[sp_RatesTemplateProcess]";

        //    SqlParameter[] param = new SqlParameter[1];
        //    param[0] = new SqlParameter("@customerList", SqlDbType.VarChar);
        //    param[0].Value = customers;



        //    sQuery.Parameters.AddRange(param);

        //    dbAdapter.SelectCommand = sQuery;
        //    resultSet = new DataSet();
        //    dbAdapter.Fill(resultSet);

        //    Process bcpFiles = new Process();
        //    ProcessStartInfo bcpFilesStartInfo = new ProcessStartInfo();

        //    bcpFilesStartInfo.WorkingDirectory = "E:\\Mecca\\WebSite\\DIRECTO_\\Pricing\\RAFiles\\Templates\\";
        //    bcpFilesStartInfo.FileName = "E:\\Mecca\\WebSite\\DIRECTO_\\Pricing\\RAFiles\\Templates\\bcptemplatefile.bat";

        //    bcpFiles.StartInfo = bcpFilesStartInfo;

        //    bcpFiles.Start();

        //    bcpFiles.WaitForExit();

        //    bcpFiles.Close();
        //    bcpFiles.Dispose();


        //    lblUpload.Text = "File " + FU.FileName.ToString() + " processed succesfully";
        //}

    }

    protected void getCustomers()
    {
        SqlCommand sQuery = new SqlCommand();
        sQuery.CommandText = "SELECT [ProviderName] 'Customer', [IP] 'CustomerID' FROM Cdrdb.dbo.[Providerip] WHERE Type = 'C' ORDER BY Providername";
        DataSet dataSet = RunQuery(sQuery);

        System.Web.UI.WebControls.ListBox listbox1 = (ListBox)Accordion1.FindControl("ListBox1");        

        listbox1.DataSource = dataSet.Tables[0].DefaultView;
        listbox1.DataBind();
        
        
    }

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.ListBox listbox1 = (ListBox)Accordion1.FindControl("ListBox1");
        System.Web.UI.WebControls.ListBox listbox2 = (ListBox)Accordion1.FindControl("ListBox2");
        System.Web.UI.WebControls.RadioButtonList radiobuttonlist1 = (RadioButtonList)Accordion1.FindControl("RadioButtonList1");
        System.Web.UI.WebControls.CheckBoxList checkboxlist1 = (CheckBoxList)Accordion1.FindControl("CheckBoxList1");

        if (radiobuttonlist1.Items[0].Selected == true)
        {
            listbox1.Enabled = false;
            listbox2.Enabled = false;
            checkboxlist1.Enabled = false;
        }

        if (radiobuttonlist1.Items[1].Selected == true)
        {
            listbox1.Enabled = false;
            listbox2.Enabled = false;
            checkboxlist1.Enabled = true;
        }
        if (radiobuttonlist1.Items[2].Selected == true)
        {
            listbox1.Enabled = true;
            listbox2.Enabled = true;
            checkboxlist1.Enabled = false;
        }
        
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sQuery;
        sQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception e)
        {
            e.ToString();
        }
        return resultsDataSet;
    }



}
