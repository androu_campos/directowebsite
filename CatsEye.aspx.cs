using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class CatsEye : System.Web.UI.Page
{
    int[] reng = new int[100];
    protected void Page_Load(object sender, EventArgs e)
    {
        // 1 --- month to date       // 2 --- yesterdat       // 3 --- previous day

        ScriptManager1.RegisterAsyncPostBackControl(cmdReport);
        string user = Session["DirectoUser"].ToString();
        if (user == "vicente" || user == "huguette" || user == "david" || user == "felipe" || user == "rafael" || user == "rmanja" || user == "octavio" || user == "Fernando" || user == "hagi" || user == "rmanja")
        {
            DropDownList1.Visible = true;
            lnkHistory.Visible = true;
        }
        else
        {
            DropDownList1.Visible = false;
            lnkHistory.Visible = false;
        }

        if (Page.IsPostBack)
        {            

            MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adpBR = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
            MyDataSetTableAdapters.Billing_StatusDataTable tblBR = adpBR.GetData();
            if (tblBR.Rows.Count < 1)
            {//BILLING NOT RUNNING

             
                string broker = string.Empty;

                if (user == "vicente" || user == "huguette" || user == "david" || user == "felipe" || user == "rafael" || user == "rmanja" || user == "octavio" || user == "Fernando" || user=="hagi")
                {
                    broker = DropDownList1.SelectedValue.ToString();
                }
                else
                {
                    broker = Session["BrokerName"].ToString();
                }

                DateTime firstDayOfTheMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                ////Month to Date
                SqlCommand sqlSpQuery = new SqlCommand();
                sqlSpQuery.CommandType = System.Data.CommandType.Text;
                sqlSpQuery.CommandText = "SELECT Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')GROUP BY T.Country  HAVING SUM(CustMinutes) > 0 UNION SELECT '**ALL**' as Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker +"')  HAVING SUM(CustMinutes) > 0 ORDER BY   T.Country desc";
                DataSet DtsMonthToDate = Util.RunQuery(sqlSpQuery);
                GridView4.DataSource = DtsMonthToDate.Tables[0];
                GridView4.DataBind();
                ////Yesterday
                SqlCommand sqlSpQuery2 = new SqlCommand();
                sqlSpQuery2.CommandType = System.Data.CommandType.Text;
                sqlSpQuery2.CommandText = "SELECT Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')GROUP BY T.Country  HAVING SUM(CustMinutes) > 0  UNION SELECT '**ALL**' as Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')  HAVING SUM(CustMinutes) > 0 ORDER BY   T.Country desc";
                DataSet DtsYest = Util.RunQuery(sqlSpQuery2);
                GridView5.DataSource = DtsYest.Tables[0];
                GridView5.DataBind();

                //// Before Yesterday
                SqlCommand sqlSpQuery3 = new SqlCommand();
                sqlSpQuery3.CommandType = System.Data.CommandType.Text;
                sqlSpQuery3.CommandText = "SELECT Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')GROUP BY T.Country  HAVING SUM(CustMinutes) > 0 UNION SELECT '**ALL**' as Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')  HAVING SUM(CustMinutes) > 0 ORDER BY   T.Country desc";
                DataSet DtsBfrYest = Util.RunQuery(sqlSpQuery3);
                GridView6.DataSource = DtsBfrYest.Tables[0];
                GridView6.DataBind();
                
                //Vendors
                //Month to Date
                SqlCommand sqlSpQuery4 = new SqlCommand();
                sqlSpQuery4.CommandType = System.Data.CommandType.Text;
                sqlSpQuery4.CommandText = "SELECT Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes ,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') GROUP BY T.Vendor HAVING SUM(CustMinutes) > 0  UNION SELECT '**ALL**' as Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Vendor";
                DataSet DtsVndrMonth = Util.RunQuery(sqlSpQuery4);
                
                int days = Convert.ToInt32(DateTime.Now.AddDays(-1).Day.ToString());
                decimal prf = Convert.ToDecimal(DtsVndrMonth.Tables[0].Rows[0]["Profit"].ToString());
//------------->
                DataTable tblMonthVnd = DtsVndrMonth.Tables[0].Copy();
                tblMonthVnd.Rows[0]["TotalCost"] =  (prf/days)*30;
                gdvVendorsMonth.DataSource = tblMonthVnd;
                gdvVendorsMonth.DataBind();
                //Yesterday
                SqlCommand sqlSpQuery5 = new SqlCommand();
                sqlSpQuery5.CommandType = System.Data.CommandType.Text;
                sqlSpQuery5.CommandText = "SELECT Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes ,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') GROUP BY T.Vendor HAVING SUM(CustMinutes) > 0  UNION SELECT '**ALL**' as Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Vendor";
//------------->
                DataSet DtsVndrYest = Util.RunQuery(sqlSpQuery5);
                //Previous Day
                SqlCommand sqlSpQuery6 = new SqlCommand();
                sqlSpQuery6.CommandType = System.Data.CommandType.Text;
                sqlSpQuery6.CommandText = "SELECT Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes ,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') GROUP BY T.Vendor HAVING SUM(CustMinutes) > 0  UNION SELECT '**ALL**' as Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Vendor";
                DataSet DtsVndrBfrYest = Util.RunQuery(sqlSpQuery6);
//------------->                
                DataTable tblVndrBfr = DtsVndrBfrYest.Tables[0].Copy();
                gdvVendorsBeforeY.DataSource = DtsVndrBfrYest;
                gdvVendorsBeforeY.DataBind();    
    //*******************************************************************//
                DataTable tblDelta = DtsVndrYest.Tables[0].Copy();
                tblDelta.Rows[0]["TotalCost"] = Convert.ToDecimal(DtsVndrYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsVndrBfrYest.Tables[0].Rows[0]["Profit"].ToString());
                gdvVendorsYest.DataSource = tblDelta;
                gdvVendorsYest.DataBind();
                decimal deltaVend = Convert.ToDecimal(DtsVndrYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsVndrBfrYest.Tables[0].Rows[0]["Profit"].ToString());
                if (deltaVend < 0) gdvVendorsYest.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.Red;
                else gdvVendorsYest.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.Black;
    //*******************************************************************//
                //Customers
                //Month to Date
                SqlCommand sqlSpQuery7 = new SqlCommand();
                sqlSpQuery7.CommandType = System.Data.CommandType.Text;
                sqlSpQuery7.CommandText = "SELECT Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "')GROUP BY T.Customer HAVING SUM(CustMinutes) > 0  union SELECT '**ALL**' as Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes ,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Customer";
                DataSet DtsCustMonth = Util.RunQuery(sqlSpQuery7);
//------------->
                DataTable tblMonthCust = DtsCustMonth.Tables[0].Copy();
                prf = Convert.ToDecimal(DtsCustMonth.Tables[0].Rows[0]["Profit"].ToString());
                tblMonthCust.Rows[0]["TotalSales"] = (prf/days)*30;
                gdvCustMonth.DataSource =tblMonthCust;
                gdvCustMonth.DataBind();
                //Yesterday
                SqlCommand sqlSpQuery8 = new SqlCommand();
                sqlSpQuery8.CommandType = System.Data.CommandType.Text;
                sqlSpQuery8.CommandText = "SELECT Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "')GROUP BY T.Customer HAVING SUM(CustMinutes) > 0  union SELECT '**ALL**' as Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes ,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Customer";
                DataSet DtsCustYest = Util.RunQuery(sqlSpQuery8);
//-------------->
                DataTable tblYestCust = DtsCustYest.Tables[0].Copy();

                gdvCustYest.DataSource = DtsCustYest;
                gdvCustYest.DataBind();
                //Previous Day
                SqlCommand sqlSpQuery9 = new SqlCommand();
                sqlSpQuery9.CommandType = System.Data.CommandType.Text;
                sqlSpQuery9.CommandText = "SELECT Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "')GROUP BY T.Customer HAVING SUM(CustMinutes) > 0  union SELECT '**ALL**' as Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes ,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Customer";
                DataSet DtsCustBfrYest = Util.RunQuery(sqlSpQuery9);
//------------->
                DataTable tblCustBfr = DtsCustBfrYest.Tables[0].Copy();

                gdvCustBeforeY.DataSource = DtsCustBfrYest;
                gdvCustBeforeY.DataBind();

                DataTable tblDeltaCust = DtsCustYest.Tables[0].Copy();
                tblDeltaCust.Rows[0]["TotalSales"] = Convert.ToDecimal(DtsCustYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsCustBfrYest.Tables[0].Rows[0]["Profit"].ToString());
                gdvCustYest.DataSource = tblDeltaCust;
                gdvCustYest.DataBind();
                decimal deltaCust = Convert.ToDecimal(DtsCustYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsCustBfrYest.Tables[0].Rows[0]["Profit"].ToString());
                if (deltaCust < 0) gdvCustYest.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.Red;
                else gdvCustYest.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.Black;
//<-----------
                lblMontMinTot.Text = Convert.ToString( Convert.ToDecimal(tblMonthVnd.Rows[0]["VMinutes"].ToString()) + Convert.ToDecimal(tblMonthCust.Rows[0]["CMinutes"].ToString()) );
                lblMontProfTot.Text = Convert.ToString(Convert.ToDecimal(tblMonthVnd.Rows[0]["Profit"].ToString()) + Convert.ToDecimal(tblMonthCust.Rows[0]["Profit"].ToString()));
                lblMontPPTot.Text = Convert.ToString(Convert.ToDecimal(tblMonthVnd.Rows[0]["TotalCost"].ToString()) + Convert.ToDecimal(tblMonthCust.Rows[0]["TotalSales"].ToString()));

                lblYestMinTot.Text = Convert.ToString( Convert.ToDecimal(tblDelta.Rows[0]["VMinutes"].ToString()) + Convert.ToDecimal(tblYestCust.Rows[0]["CMinutes"].ToString()) );
                lblYestProfTot.Text = Convert.ToString( Convert.ToDecimal(tblDelta.Rows[0]["Profit"].ToString()) + Convert.ToDecimal(tblYestCust.Rows[0]["Profit"].ToString()) );
                lblYestPPTot.Text = Convert.ToString( Convert.ToDecimal(DtsVndrYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsVndrBfrYest.Tables[0].Rows[0]["Profit"].ToString()) + Convert.ToDecimal(DtsCustYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsCustBfrYest.Tables[0].Rows[0]["Profit"].ToString()) );

                lblBefMinTot.Text = Convert.ToString(Convert.ToDecimal(tblVndrBfr.Rows[0]["VMinutes"].ToString()) + Convert.ToDecimal(tblCustBfr.Rows[0]["CMinutes"].ToString()));
                lblBefProfTot.Text = Convert.ToString(Convert.ToDecimal(tblVndrBfr.Rows[0]["Profit"].ToString()) + Convert.ToDecimal(tblCustBfr.Rows[0]["Profit"].ToString()));

    //HISTORY
                MECCA2TableAdapters.HistoryCMAdp adp = new MECCA2TableAdapters.HistoryCMAdp();

                SqlCommand sqlSpQuery10 = new SqlCommand();
                sqlSpQuery10.CommandType = System.Data.CommandType.Text;
                sqlSpQuery10.CommandText = "select MonthN,CMinutes,Profit,TotalSales as Revenues from CustHistory where broker = '" + broker + "' order by DateLog";
                DataSet dtsCstHis = Util.RunQuery(sqlSpQuery10);
//------------->
                DataTable tblCstHis = dtsCstHis.Tables[0].Copy();
                gdvHistCust.DataSource =dtsCstHis.Tables[0];
                gdvHistCust.DataBind();
                                
                SqlCommand sqlSpQuery11 = new SqlCommand();
                sqlSpQuery11.CommandType = System.Data.CommandType.Text;
                sqlSpQuery11.CommandText = "select MonthN,VMinutes,Profit,TotalCost as Revenues from CustHistory where broker = '" + broker +"' order by DateLog";
                DataSet dtsVndHis = Util.RunQuery(sqlSpQuery11);
//------------->
                DataTable tblVndHis = dtsVndHis.Tables[0].Copy();
                gdvHistVend.DataSource = dtsVndHis.Tables[0];
                gdvHistVend.DataBind();


                //make ALL History CUSTOMERS AND VENDORS

                DataTable tblAllHistory = tblCstHis.Copy();
                int howMonths = tblAllHistory.Rows.Count;

                for (int i = 0; i < howMonths; i++)
                {
                    tblAllHistory.Rows[i]["CMinutes"] = Convert.ToDecimal(tblCstHis.Rows[i]["CMinutes"].ToString()) + Convert.ToDecimal(tblVndHis.Rows[i]["VMinutes"].ToString());
                    tblAllHistory.Rows[i]["Profit"] = Convert.ToDecimal(tblCstHis.Rows[i]["Profit"].ToString()) + Convert.ToDecimal(tblVndHis.Rows[i]["Profit"].ToString());
                    tblAllHistory.Rows[i]["Revenues"] = Convert.ToDecimal(tblCstHis.Rows[i]["Revenues"].ToString()) + Convert.ToDecimal(tblVndHis.Rows[i]["Revenues"].ToString());
                }

                gdvHistALL.DataSource = tblAllHistory;
                gdvHistALL.DataBind();

                
            }
            else
            {

            }

            cmdReport.Visible = true;
            lblSelectBroker.Visible = true;
        }
        else
        {
            lblMonthTo.Text = "Month to Date";
            lblSelectBroker.Visible = false;
            lblYesterday.Text = "Yesterday " + DateTime.Now.AddDays(-1).ToShortDateString();
            lblCstYest.Text = "Yesterday " + DateTime.Now.AddDays(-1).ToShortDateString();
            lblVndYest.Text = "Yesterday " + DateTime.Now.AddDays(-1).ToShortDateString();

            lblBefYesterday.Text = "Previous Day " + DateTime.Now.AddDays(-2).ToShortDateString();
            lblCstBef.Text = "Previous Day " + DateTime.Now.AddDays(-2).ToShortDateString();
            lblVndBef.Text = "Previous Day " + DateTime.Now.AddDays(-2).ToShortDateString();
            
                        
            

            if(user != "vicente" && user != "huguette" && user != "david" && user != "felipe" && user != "rafael" && user != "rmanja" && user != "octavio" && user != "Fernando" && user != "hagi")
            {

                cmdReport.Visible = false;
                MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adpBR = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
                MyDataSetTableAdapters.Billing_StatusDataTable tblBR = adpBR.GetData();
            if (tblBR.Rows.Count < 1)
            {//BILLING NOT RUNNING


                string broker = string.Empty;

                if (user == "vicente" || user == "huguette" || user == "david" || user == "felipe" || user == "rmanja" || user == "rmanja" || user == "octavio" || user == "Fernando" || user == "hagi")
                {
                    broker = DropDownList1.SelectedValue.ToString();
                }
                else
                {
                    broker = Session["BrokerName"].ToString();
                }

                DateTime firstDayOfTheMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                ////Month to Date
                SqlCommand sqlSpQuery = new SqlCommand();
                sqlSpQuery.CommandType = System.Data.CommandType.Text;
                sqlSpQuery.CommandText = "SELECT Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')GROUP BY T.Country  HAVING SUM(CustMinutes) > 0 UNION SELECT '**ALL**' as Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')  HAVING SUM(CustMinutes) > 0 ORDER BY   T.Country desc";
                DataSet DtsMonthToDate = Util.RunQuery(sqlSpQuery);
                GridView4.DataSource = DtsMonthToDate.Tables[0];
                GridView4.DataBind();
                ////Yesterday
                SqlCommand sqlSpQuery2 = new SqlCommand();
                sqlSpQuery2.CommandType = System.Data.CommandType.Text;
                sqlSpQuery2.CommandText = "SELECT Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')GROUP BY T.Country  HAVING SUM(CustMinutes) > 0  UNION SELECT '**ALL**' as Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')  HAVING SUM(CustMinutes) > 0 ORDER BY   T.Country desc";
                DataSet DtsYest = Util.RunQuery(sqlSpQuery2);
                GridView5.DataSource = DtsYest.Tables[0];
                GridView5.DataBind();

                //// Before Yesterday
                SqlCommand sqlSpQuery3 = new SqlCommand();
                sqlSpQuery3.CommandType = System.Data.CommandType.Text;
                sqlSpQuery3.CommandText = "SELECT Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')GROUP BY T.Country  HAVING SUM(CustMinutes) > 0 UNION SELECT '**ALL**' as Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.BillableCalls) as AnsweredCalls,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')  HAVING SUM(CustMinutes) > 0 ORDER BY   T.Country desc";
                DataSet DtsBfrYest = Util.RunQuery(sqlSpQuery3);
                GridView6.DataSource = DtsBfrYest.Tables[0];
                GridView6.DataBind();

                //Vendors
                //Month to Date
                SqlCommand sqlSpQuery4 = new SqlCommand();
                sqlSpQuery4.CommandType = System.Data.CommandType.Text;
                sqlSpQuery4.CommandText = "SELECT Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes ,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') GROUP BY T.Vendor HAVING SUM(CustMinutes) > 0  UNION SELECT '**ALL**' as Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Vendor";
                DataSet DtsVndrMonth = Util.RunQuery(sqlSpQuery4);

                int days = Convert.ToInt32(DateTime.Now.AddDays(-1).Day.ToString());
                decimal prf = Convert.ToDecimal(DtsVndrMonth.Tables[0].Rows[0]["Profit"].ToString());
                //--------->
                DataTable tblMonthVnd = DtsVndrMonth.Tables[0].Copy();
                tblMonthVnd.Rows[0]["TotalCost"] = (prf / days) * 30;
                gdvVendorsMonth.DataSource = tblMonthVnd;
                gdvVendorsMonth.DataBind();
                //Yesterday
                SqlCommand sqlSpQuery5 = new SqlCommand();
                sqlSpQuery5.CommandType = System.Data.CommandType.Text;
                sqlSpQuery5.CommandText = "SELECT Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes ,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') GROUP BY T.Vendor HAVING SUM(CustMinutes) > 0  UNION SELECT '**ALL**' as Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Vendor";
                //------------->
                DataSet DtsVndrYest = Util.RunQuery(sqlSpQuery5);
                //Previous Day
                SqlCommand sqlSpQuery6 = new SqlCommand();
                sqlSpQuery6.CommandType = System.Data.CommandType.Text;
                sqlSpQuery6.CommandText = "SELECT Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes ,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') GROUP BY T.Vendor HAVING SUM(CustMinutes) > 0  UNION SELECT '**ALL**' as Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Vendor in (select Provider from Accounts_CM where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Vendor";
                DataSet DtsVndrBfrYest = Util.RunQuery(sqlSpQuery6);
                //------------->                
                DataTable tblVndrBfr = DtsVndrBfrYest.Tables[0].Copy();
                gdvVendorsBeforeY.DataSource = DtsVndrBfrYest;
                gdvVendorsBeforeY.DataBind();
                //*******************************************************************//
                DataTable tblDelta = DtsVndrYest.Tables[0].Copy();
                tblDelta.Rows[0]["TotalCost"] = Convert.ToDecimal(DtsVndrYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsVndrBfrYest.Tables[0].Rows[0]["Profit"].ToString());
                gdvVendorsYest.DataSource = tblDelta;
                gdvVendorsYest.DataBind();
                decimal deltaVend = Convert.ToDecimal(DtsVndrYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsVndrBfrYest.Tables[0].Rows[0]["Profit"].ToString());
                if (deltaVend < 0) gdvVendorsYest.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.Red;
                else gdvVendorsYest.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.Black;
                //*******************************************************************//
                //Customers
                //Month to Date
                SqlCommand sqlSpQuery7 = new SqlCommand();
                sqlSpQuery7.CommandType = System.Data.CommandType.Text;
                sqlSpQuery7.CommandText = "SELECT Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "')GROUP BY T.Customer HAVING SUM(CustMinutes) > 0  union SELECT '**ALL**' as Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes ,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + firstDayOfTheMonth.ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Customer";
                DataSet DtsCustMonth = Util.RunQuery(sqlSpQuery7);
                //------------->
                DataTable tblMonthCust = DtsCustMonth.Tables[0].Copy();
                prf = Convert.ToDecimal(DtsCustMonth.Tables[0].Rows[0]["Profit"].ToString());
                tblMonthCust.Rows[0]["TotalSales"] = (prf / days) * 30;
                gdvCustMonth.DataSource = tblMonthCust;
                gdvCustMonth.DataBind();
                //Yesterday
                SqlCommand sqlSpQuery8 = new SqlCommand();
                sqlSpQuery8.CommandType = System.Data.CommandType.Text;
                sqlSpQuery8.CommandText = "SELECT Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "')GROUP BY T.Customer HAVING SUM(CustMinutes) > 0  union SELECT '**ALL**' as Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes ,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Customer";
                DataSet DtsCustYest = Util.RunQuery(sqlSpQuery8);
                //-------------->
                DataTable tblYestCust = DtsCustYest.Tables[0].Copy();

                gdvCustYest.DataSource = DtsCustYest;
                gdvCustYest.DataBind();
                //Previous Day
                SqlCommand sqlSpQuery9 = new SqlCommand();
                sqlSpQuery9.CommandType = System.Data.CommandType.Text;
                sqlSpQuery9.CommandText = "SELECT Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "')GROUP BY T.Customer HAVING SUM(CustMinutes) > 0  union SELECT '**ALL**' as Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes ,ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-2).ToShortDateString() + "' AND T.Customer in (Select Provider from Accounts_CM Where Broker like '" + broker + "') HAVING SUM(CustMinutes) > 0 ORDER BY   T.Customer";
                DataSet DtsCustBfrYest = Util.RunQuery(sqlSpQuery9);
                //------------->
                DataTable tblCustBfr = DtsCustBfrYest.Tables[0].Copy();

                gdvCustBeforeY.DataSource = DtsCustBfrYest;
                gdvCustBeforeY.DataBind();

                DataTable tblDeltaCust = DtsCustYest.Tables[0].Copy();
                tblDeltaCust.Rows[0]["TotalSales"] = Convert.ToDecimal(DtsCustYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsCustBfrYest.Tables[0].Rows[0]["Profit"].ToString());
                gdvCustYest.DataSource = tblDeltaCust;
                gdvCustYest.DataBind();
                decimal deltaCust = Convert.ToDecimal(DtsCustYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsCustBfrYest.Tables[0].Rows[0]["Profit"].ToString());
                if (deltaCust < 0) gdvCustYest.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.Red;
                else gdvCustYest.Columns[3].ItemStyle.ForeColor = System.Drawing.Color.Black;
                //<-----------
                lblMontMinTot.Text = Convert.ToString(Convert.ToDecimal(tblMonthVnd.Rows[0]["VMinutes"].ToString()) + Convert.ToDecimal(tblMonthCust.Rows[0]["CMinutes"].ToString()));
                lblMontProfTot.Text = Convert.ToString(Convert.ToDecimal(tblMonthVnd.Rows[0]["Profit"].ToString()) + Convert.ToDecimal(tblMonthCust.Rows[0]["Profit"].ToString()));
                lblMontPPTot.Text = Convert.ToString(Convert.ToDecimal(tblMonthVnd.Rows[0]["TotalCost"].ToString()) + Convert.ToDecimal(tblMonthCust.Rows[0]["TotalSales"].ToString()));

                lblYestMinTot.Text = Convert.ToString(Convert.ToDecimal(tblDelta.Rows[0]["VMinutes"].ToString()) + Convert.ToDecimal(tblYestCust.Rows[0]["CMinutes"].ToString()));
                lblYestProfTot.Text = Convert.ToString(Convert.ToDecimal(tblDelta.Rows[0]["Profit"].ToString()) + Convert.ToDecimal(tblYestCust.Rows[0]["Profit"].ToString()));
                lblYestPPTot.Text = Convert.ToString(Convert.ToDecimal(DtsVndrYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsVndrBfrYest.Tables[0].Rows[0]["Profit"].ToString()) + Convert.ToDecimal(DtsCustYest.Tables[0].Rows[0]["Profit"].ToString()) - Convert.ToDecimal(DtsCustBfrYest.Tables[0].Rows[0]["Profit"].ToString()));

                lblBefMinTot.Text = Convert.ToString(Convert.ToDecimal(tblVndrBfr.Rows[0]["VMinutes"].ToString()) + Convert.ToDecimal(tblCustBfr.Rows[0]["CMinutes"].ToString()));
                lblBefProfTot.Text = Convert.ToString(Convert.ToDecimal(tblVndrBfr.Rows[0]["Profit"].ToString()) + Convert.ToDecimal(tblCustBfr.Rows[0]["Profit"].ToString()));

                //HISTORY
                MECCA2TableAdapters.HistoryCMAdp adp = new MECCA2TableAdapters.HistoryCMAdp();

                SqlCommand sqlSpQuery10 = new SqlCommand();
                sqlSpQuery10.CommandType = System.Data.CommandType.Text;
                sqlSpQuery10.CommandText = "select MonthN,CMinutes,Profit,TotalSales as Revenues from CustHistory where broker = '" + broker + "' order by DateLog";
                DataSet dtsCstHis = Util.RunQuery(sqlSpQuery10);
                //------------->
                DataTable tblCstHis = dtsCstHis.Tables[0].Copy();
                gdvHistCust.DataSource = dtsCstHis.Tables[0];
                gdvHistCust.DataBind();

                SqlCommand sqlSpQuery11 = new SqlCommand();
                sqlSpQuery11.CommandType = System.Data.CommandType.Text;
                sqlSpQuery11.CommandText = "select MonthN,VMinutes,Profit,TotalCost as Revenues from CustHistory where broker = '" + broker + "' order by DateLog";
                DataSet dtsVndHis = Util.RunQuery(sqlSpQuery11);
                //------------->
                DataTable tblVndHis = dtsVndHis.Tables[0].Copy();
                gdvHistVend.DataSource = dtsVndHis.Tables[0];
                gdvHistVend.DataBind();


                //make ALL History CUSTOMERS AND VENDORS

                DataTable tblAllHistory = tblCstHis.Copy();
                int howMonths = tblAllHistory.Rows.Count;

                for (int i = 0; i < howMonths; i++)
                {
                    tblAllHistory.Rows[i]["CMinutes"] = Convert.ToDecimal(tblCstHis.Rows[i]["CMinutes"].ToString()) + Convert.ToDecimal(tblVndHis.Rows[i]["VMinutes"].ToString());
                    tblAllHistory.Rows[i]["Profit"] = Convert.ToDecimal(tblCstHis.Rows[i]["Profit"].ToString()) + Convert.ToDecimal(tblVndHis.Rows[i]["Profit"].ToString());
                    tblAllHistory.Rows[i]["Revenues"] = Convert.ToDecimal(tblCstHis.Rows[i]["Revenues"].ToString()) + Convert.ToDecimal(tblVndHis.Rows[i]["Revenues"].ToString());
                }

                gdvHistALL.DataSource = tblAllHistory;
                gdvHistALL.DataBind();
            }

          }

        }        
    }

   
    protected void cmdReport_Click(object sender, EventArgs e)
    {
        
                        

    }   
    protected void cmdUpdVndYest_Click(object sender, EventArgs e)
    {
        gdvVendorsYest.AllowPaging = false;
        gdvVendorsYest.DataBind();
        cmdUpdVndYestH.Visible = true;
        cmdUpdVndYest.Visible = false;
    }
    protected void cmdUpdVndMont_Click(object sender, EventArgs e)
    {
        gdvVendorsMonth.AllowPaging = false;
        gdvVendorsMonth.DataBind();
        cmdUpdVndMontH.Visible = true;
        cmdUpdVndMont.Visible = false;
    }
    protected void cmdUpdCstBef_Click(object sender, EventArgs e)
    {
        gdvCustBeforeY.AllowPaging = false;
        gdvCustBeforeY.DataBind();
        cmdUpdCstBef.Visible = false;
        cmdUpdCstBefH.Visible = true;
    }
    protected void cmdUpdCstYest_Click(object sender, EventArgs e)
    {
        gdvCustYest.AllowPaging = false;
        gdvCustYest.DataBind();
        cmdUpdCstYest.Visible = false;
        cmdUpdCstYestH.Visible = true;
    }
    protected void cmdUpdCstMonth_Click(object sender, EventArgs e)
    {
        gdvCustMonth.AllowPaging = false;
        gdvCustMonth.DataBind();
        cmdUpdCstMonth.Visible = false;
        cmdUpdCstMonthH.Visible = true;
    }
    protected void cmdUpdVndMontH_Click(object sender, EventArgs e)
    {
        cmdUpdVndMont.Visible = true;
        gdvVendorsMonth.AllowPaging = true;
        gdvVendorsMonth.PageSize = 1;
        gdvVendorsMonth.PagerSettings.Visible = false;
        gdvVendorsMonth.DataBind();
        cmdUpdVndMontH.Visible = false;
    }
    protected void cmdUpdVndYestH_Click(object sender, EventArgs e)
    {
        cmdUpdVndYestH.Visible = false;
        cmdUpdVndYest.Visible = true;
        gdvVendorsYest.AllowPaging = true;
        gdvVendorsYest.PageSize = 1;
        gdvVendorsYest.PagerSettings.Visible = false;
        gdvVendorsYest.DataBind();
        
    }
    protected void cmdUpdVndPrevH_Click(object sender, EventArgs e)
    {
        cmdUpdVndPrevH.Visible = false;
        cmdUpdVndPrev.Visible = true;
        gdvVendorsBeforeY.AllowPaging = true;
        gdvVendorsBeforeY.PageSize = 1;
        gdvVendorsBeforeY.PagerSettings.Visible = false;
        gdvVendorsBeforeY.DataBind();
    }
    protected void cmdUpdCstBefH_Click(object sender, EventArgs e)
    {
        cmdUpdCstBefH.Visible = false;
        cmdUpdCstBef.Visible = true;
        gdvCustBeforeY.AllowPaging = true;
        gdvCustBeforeY.PageSize = 1;
        gdvCustBeforeY.PagerSettings.Visible = false;
        gdvCustBeforeY.DataBind();
    }
    protected void cmdUpdCstYestH_Click(object sender, EventArgs e)
    {
        cmdUpdCstYestH.Visible = false;
        cmdUpdCstYest.Visible = true;
        gdvCustYest.AllowPaging = true;
        gdvCustYest.PageSize = 1;
        gdvCustYest.PagerSettings.Visible = false;
        gdvCustYest.DataBind();
    }
    protected void cmdUpdCstMonthH_Click(object sender, EventArgs e)
    {
        cmdUpdCstMonthH.Visible = false;
        cmdUpdCstMonth.Visible = true;
        gdvCustMonth.AllowPaging = true;
        gdvCustMonth.PageSize = 1;
        gdvCustMonth.PagerSettings.Visible = false;
        gdvCustMonth.DataBind();
    }
    protected void cmdUpdVndPrev_Click(object sender, EventArgs e)
    {
        gdvVendorsBeforeY.AllowPaging = false;
        gdvVendorsBeforeY.DataBind();
        cmdUpdVndPrev.Visible = false;
        cmdUpdVndPrevH.Visible = true;
    }
}
