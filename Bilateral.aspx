<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="Bilateral.aspx.cs" Inherits="SWAP" Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table>
        <tr>
            <td>
                <asp:Label Font-Bold="true" ID="lblHeader" runat="server" Text="Bilateral Agreements"
                    CssClass="labelBlue8" Width="200px"></asp:Label><br />
            </td>
        </tr>
        <tr>
            <td style="width: 1088px">
                <asp:GridView ID="gdvData" runat="server" Visible="false" Font-Names="Arial" Font-Size="8pt"
                    AllowPaging="true" PageSize="20" OnPageIndexChanging="gdvData_PageIndexChanging">
                    <HeaderStyle CssClass="titleOrangegrid" Height="40px" Width="150px" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <AlternatingRowStyle BackColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table >
        <tr>
            <td>
                <asp:Label Font-Bold="true" ID="Label4" runat="server" Text="Review Agreements" CssClass="labelBlue8"
                    Width="200px"></asp:Label><br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="Label6" Text="Client: " CssClass="labelBlue8"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlCust" CssClass="dropdown" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="Label7" Text="Period :" CssClass="labelBlue8"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlPeriod" CssClass="dropdown" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnReview" runat="server" Text="Review" OnClick="SubmitClick" Font-Names="Arial"
                    CssClass="boton" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table visible="false">
        <tr>
            <td>
                <asp:Label Font-Bold="true" ID="Label2" runat="server" Text="Add Agreements" CssClass="labelBlue8"
                    Width="200px"></asp:Label><br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblType" Text="Type: " CssClass="labelBlue8"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlType" CssClass="dropdown" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"
                    AutoPostBack="true">
                    <asp:ListItem Text="Choose One" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Customer" Value="C"></asp:ListItem>
                    <asp:ListItem Text="Vendor" Value="V"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblCust" Text="Client: " CssClass="labelBlue8"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlClient" CssClass="dropdown" Enabled="false">
                    <asp:ListItem Text="Choose Type"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblFrom" Text="From :" CssClass="labelBlue8"></asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFrom" CssClass="labelSteelBlue"></asp:TextBox>
                <asp:Image ID="imgFrom" runat="server" ImageUrl="~/Images/calendar1.gif" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblTo" Text="To :" CssClass="labelBlue8"></asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTo" CssClass="labelSteelBlue"></asp:TextBox>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="SubmitClick" Font-Names="Arial"
                    CssClass="boton" />
            </td>
        </tr>
        <tr>
            <td>
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom"
                    PopupButtonID="imgFrom" Format="M/d/yyyy">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"
                    PopupButtonID="imgTo" Format="M/d/yyyy">
                </cc1:CalendarExtender>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:Label Font-Bold="true" ID="Label3" runat="server" Text="Add Agreements" CssClass="labelBlue8"
                    Width="200px"></asp:Label><br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="Label1" Text="Expected Profit :" CssClass="labelBlue8"></asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="TextBox1" CssClass="labelSteelBlue"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
