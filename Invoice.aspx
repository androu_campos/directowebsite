<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Invoice.aspx.cs" Inherits="Invoice" Title="Untitled Page" StylesheetTheme="Theme1" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="1280" height="800">
        <tr>
            <td style="width: 100px">
                <table width="1280">
                    <tr>
                        <td style="height: 109px" width="15%" align="left" valign="middle">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/MECCA2.gif" /></td>
                        <td align="center" style="height: 109px" valign="middle" width="35%">
                            <table>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label1" runat="server" Text="Computer-Tel, Inc" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" Width="140px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label2" runat="server" Text="1050 Russett Way" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" Width="140px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label3" runat="server" Text="Carson City, NV 89703" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" Width="140px"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" style="height: 109px" valign="middle" width="50%">
                            <table width="70%">
                                <tr>
                                    <td bgcolor="#d5e3f0" width="30%">
                                        <asp:Label ID="Label4" runat="server" Text="Invoice Number:" CssClass="labelBlue8" Width="80px"></asp:Label></td>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblInvoice" runat="server" CssClass="labelBlue8" Text="Label" ForeColor="Black"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#d5e3f0" width="30%">
                                        <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Customer:"></asp:Label></td>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblCustomerH" runat="server" Text="Label" CssClass="labelBlue8" ForeColor="Black"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#d5e3f0" width="30%">
                                        <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Address:"></asp:Label></td>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblAddressH" runat="server" Text="Label" CssClass="labelBlue8" ForeColor="Black" Width="200px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#d5e3f0" width="30%">
                                        <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Phone:"></asp:Label></td>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblPhoneH" runat="server" Text="Label" CssClass="labelBlue8" ForeColor="Black" Width="200px"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/footer.gif" Width="1280px" /></td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <table width="100%">
                    <tr>
                        <td width="20%" align="left" valign="top">
                            <table width="100%" id="tblSummary">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label8" runat="server" Text="Invoice Summary" Width="122px" Font-Bold="True"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="lblBillDate" runat="server" Text="Bill Date:" Font-Size="9pt" Font-Names="Arial"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="lblBillingPeriod" runat="server" Text="Billing Period:" Font-Size="9pt" Font-Names="Arial"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="lblAmoundDue" runat="server" Text="Amound Due:" Font-Size="9pt" Font-Names="Arial" Width="250px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="lblDueDate" runat="server" Text="Due Date:" Font-Size="9pt" Font-Names="Arial"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 8px">
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/footer.gif" Width="200px" Height="12px" />
                                        </td>
                                </tr>
                            </table>
                            <table id="tblBank" width="100%">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="Bank Wiring Instructions"
                                            Width="176px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label10" runat="server" Text="Computer-Tel, Inc." Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label11" runat="server" Text="The Laredo National Bank" Width="165px" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label12" runat="server" Text="P.O. Box 59" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label13" runat="server" Text="700 San Bernardo" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label14" runat="server" Text="Laredo, TC 78042" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label15" runat="server" Text="ABA: 1 1 4 9 0 0 3 1 3" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label16" runat="server" Text="SWIFT: L B L T U S 4 L" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label17" runat="server" Text="Account #: 0 6 6 5 2 5 6" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Image ID="Image4" runat="server" Height="12px" ImageUrl="~/Images/footer.gif"
                                            Width="200px" /></td>
                                </tr>
                            </table>
                            <table id="tblAccount" width="100%">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Text="Account Status"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label19" runat="server" Text="Previous Balance:" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label20" runat="server" Text="Payments:" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label21" runat="server" Text="Adjustments:" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="Label22" runat="server" Text="Past Due Balance:" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="lblLateFees" runat="server" Text="Late Fees:" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="lblCurrentCharges" runat="server" Text="Current Charges:" Font-Names="Arial" Font-Size="9pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                    </td>
                                    <td width="85%">
                                        <asp:Label ID="lblTotDue" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Total Due:"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" rowspan="3" valign="top" width="60%">
                            <asp:GridView ID="gdv1" runat="server" AutoGenerateColumns="False" Width="400px" Font-Names="Arial" Font-Size="8pt">
                                <Columns>
                                    <asp:BoundField DataField="Date" HeaderText="Date" />
                                    <asp:BoundField DataField="Completed Calls" HeaderText="Completed Calls" />
                                    <asp:BoundField DataField="Billed Minutes" HeaderText="Billed Minutes" />
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                </Columns>
                                <HeaderStyle BackColor="#C6D8EB" Wrap="False" />
                            </asp:GridView>
                            <br />
                            <asp:GridView ID="gdv2" runat="server" AutoGenerateColumns="False" Width="400px" Font-Names="Arial" Font-Size="8pt">
                                <Columns>
                                    <asp:BoundField DataField="Completed Calls" HeaderText="Completed Calls" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Billed Minutes" HeaderText="Billed Minutes" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Price" HeaderText="Price" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" HeaderStyle-HorizontalAlign="Left" />
                                </Columns>
                                <HeaderStyle BackColor="#C6D8EB" Wrap="False" />
                            </asp:GridView>
                            <br />
                            <asp:GridView ID="gdv3" runat="server" AutoGenerateColumns="False" Width="400px" Font-Names="Arial" Font-Size="8pt">
                                <Columns>
                                    <asp:BoundField DataField="Total Calls" HeaderText="Total Calls" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Total Minutes" HeaderText="Total Minutes" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Price" HeaderText="Price" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Total Amount" HeaderText="Total Amount" ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                                <HeaderStyle BackColor="#C6D8EB" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="height: 273px" align="left" valign="top">
                            &nbsp;<br />
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left" valign="top">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>


    


