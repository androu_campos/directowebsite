using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class Regions_OffNet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            SqlDataSource3.SelectCommand = "select crdate as [Last Refresh] from [mecca2].[dbo].sysobjects  where type = 'U' and name like 'Regions_OFF_NET'";
            string OrderString, QueryString, QueryStringT;
            OrderString = "ORDER BY [%ASR], ACD, Region";
            QueryString = "SELECT * FROM MECCA2..Regions_OFF_NET WHERE Region LIKE '" + "%" + "' AND Vendor LIKE '" + "%" + "' AND Calls > 25 " + OrderString;
            QueryStringT = "SELECT * FROM MECCA2..Regions_OFF_NETT WHERE Region LIKE '" + "%" + "' AND Vendor LIKE '" + "%" + "' AND Calls > 100 " + OrderString;
            dsInc.SelectCommand = QueryString;
            dsAll.SelectCommand = QueryStringT;

            SqlCommand qry1 = new SqlCommand();
            qry1.CommandType = System.Data.CommandType.Text;
            qry1.CommandText = QueryString;
            DataSet ResultSet;
            ResultSet = RunQuery(qry1);

            Session["TrafficRecent"] = ResultSet;

            SqlCommand qry2 = new SqlCommand();
            qry2.CommandType = System.Data.CommandType.Text;
            qry2.CommandText = QueryStringT;
            DataSet ResultSet2;
            ResultSet2 = RunQuery(qry2);

            Session["TrafficToday"] = ResultSet2;
            Button1.Enabled = false;
        }
      

    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlDataSource2.SelectCommand = "SELECT '%' as Vendor UNION SELECT DISTINCT [Vendor] FROM [Regions_OFF_NET] where Region like '" + DropDownList1.SelectedValue.ToString() + "' AND Calls >25 UNION SELECT DISTINCT [Vendor] FROM [Regions_OFF_NETT] where Region like '" + DropDownList1.SelectedValue.ToString() + "' AND Calls > 100";

        Button1.Enabled = true;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string OrderString, QueryString, QueryStringT;
        OrderString = "ORDER BY [%ASR], ACD, Region";
        QueryString = "SELECT * FROM MECCA2..Regions_OFF_NET WHERE Region LIKE '" + DropDownList1.SelectedValue.ToString() + "' AND Vendor LIKE '" + DropDownList2.SelectedItem.ToString() + "' AND Calls > 25 " + OrderString;
        QueryStringT = "SELECT * FROM MECCA2..Regions_OFF_NETT WHERE Region LIKE '" + DropDownList1.SelectedValue.ToString() + "' AND Vendor LIKE '" + DropDownList2.SelectedItem.ToString() + "' AND Calls > 100 " + OrderString;
        dsInc.SelectCommand = QueryString;
        dsAll.SelectCommand = QueryStringT;

        SqlCommand qry1 = new SqlCommand();
        qry1.CommandType = System.Data.CommandType.Text;
        qry1.CommandText = QueryString;
        DataSet ResultSet;
        ResultSet = RunQuery(qry1);

        Session["TrafficRecent"] = ResultSet;
        
        SqlCommand qry2 = new SqlCommand();
        qry2.CommandType = System.Data.CommandType.Text;
        qry2.CommandText = QueryStringT;
        DataSet ResultSet2;
        ResultSet2 = RunQuery(qry2);
        
        Session["TrafficToday"] = ResultSet2;

        if (ResultSet.Tables[0].Rows.Count <= 0)
        {
            lblNothR.Visible = true;
        }
        else
        { 
            lblNothR.Visible = false; 
        }

        if (ResultSet2.Tables[0].Rows.Count <= 0)
        {
            lblNothT.Visible = true;
        }
        else
        {
            lblNothT.Visible = false;
        }

        Button1.Enabled = false;



    }
  
    protected void gvRegionsOff_Load(object sender, EventArgs e)
    {
      
    }


    protected void exportRecent_Click(object sender, EventArgs e)
    {
        try
        {

            DataTable dtEmployee = ((DataSet)Session["TrafficRecent"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "RecentTraffic" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

    }
    protected void exportToday_Click(object sender, EventArgs e)
    {
        try
        {

            DataTable dtEmployee = ((DataSet)Session["TrafficToday"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "TodaysTraffic" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
