<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Prepareinvoicebeta1.aspx.cs" Inherits="Prepareinvoicebeta1" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="SteelBlue"
        Text="Printable Invoice Maker"></asp:Label><br />
    <br />
    <table>
        <tr>
            <td bgcolor="steelblue" style="width: 128px" align="right">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Customer Name"
                    Width="114px"></asp:Label></td>
            <td style="width: 325px">
                <asp:DropDownList ID="dpdCustomerN" runat="server" Width="287px" AutoPostBack="True">
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 34px">
            </td>
            <td bgcolor="steelblue" style="width: 100px">
                <asp:Label ID="Label5" runat="server" Font-Bold="True" ForeColor="White" Text="Invoice Number"
                    Width="109px"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtInvoiceNumber" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td bgcolor="steelblue" style="width: 128px; height: 20px;" align="right">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="To"></asp:Label></td>
            <td style="width: 325px; height: 20px;">
                <asp:TextBox ID="txtCalendar" runat="server"></asp:TextBox></td>
            <td style="width: 34px; height: 20px;">
            </td>
            <td style="width: 100px; height: 20px;">
            </td>
            <td style="width: 100px; height: 20px;">
            </td>
        </tr>
        <tr>
            <td style="width: 128px; height: 10px" align="right" valign="middle">
                </td>
            <td style="width: 325px; height: 10px" valign="top">
                &nbsp;&nbsp;
                </td>
            <td style="width: 34px; height: 10px">
            </td>
            <td style="width: 100px; height: 10px">
            </td>
            <td style="width: 100px; height: 10px">
            </td>
        </tr>
        <tr>
            <td style="width: 128px; height: 27px">
            </td>
            <td align="right" style="width: 325px; height: 27px">
                &nbsp;&nbsp;
            </td>
            <td style="width: 34px; height: 27px">
            </td>
            <td style="width: 100px; height: 27px">
            </td>
            <td style="width: 100px; height: 27px">
            </td>
        </tr>
        <tr>
            <td style="width: 128px; height: 22px">
                <asp:Button ID="Button1" runat="server" Text="Create Invoice" BackColor="SteelBlue" BorderWidth="1px" Font-Bold="True" ForeColor="White" OnClick="Button1_Click" /></td>
            <td style="width: 325px; height: 22px" align="left">
                </td>
            <td style="width: 34px; height: 22px">
            </td>
            <td style="width: 100px; height: 22px">
            </td>
            <td style="width: 100px; height: 22px">
            </td>
        </tr>
        <tr>
            <td style="width: 128px; height: 22px">
            </td>
            <td align="right" style="width: 325px; height: 22px">
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtCalendar">
                </cc1:CalendarExtender>
            </td>
            <td style="width: 34px; height: 22px">
            </td>
            <td style="width: 100px; height: 22px">
            </td>
            <td style="width: 100px; height: 22px">
            </td>
        </tr>
    </table>
</asp:Content>

