<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Test" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Devarchive.Net" Namespace="Devarchive.Net" TagPrefix="dn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GridDemo</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <dn:HoverTooltip runat="server" ID="tooltip1" 
        OffsetX="10" 
        OffsetY="10" 
        DefaultTooltipText="This is default tooltip text"
        DefaultTooltipTitle="This is default title for the tooltip">
        <HtmlTemplate>
            <div style="border: solid 1px blue; background-color: Yellow; color: Blue; font-weight: bold">
                <b>{0}</b>
                <p>
                    {1}</p>
            </div>
        </HtmlTemplate>
    </dn:HoverTooltip>
    <div style="background-color:Yellow">
        <asp:Label runat="server" ID="lb1" Text="Hover me !" ToolTip="This is a tooltip for lb1" />
    </div>
      
        <cc1:Accordion ID="Accordion1" runat="server"
         SelectedIndex="0" FadeTransitions="true"
         FramesPerSecond="40" TransitionDuration="250"
         AutoSize="None" ContentCssClass="Content" HeaderCssClass="HEADER" > 
            <Panes>

                <cc1:AccordionPane ID="AccordionPane2" runat="server"> 

                <Header> 
                <a href=""onclick="return false;" class="Link" > 
                Open 
                </a> 
                </Header> 
                <Content> 
                <div> 
                First Content Area First Content AreaFirst Content Area First Content Area 
                First Content Area</div> 
                </Content> 
                </cc1:AccordionPane> 
                <cc1:AccordionPane ID="AccordionPane1" runat="server"> 
                <Header> 
                <a href=""onclick="return false;" class="Link" > 
                Open 
                </a> 
                </Header> 
                <Content> 
                <div> 
                Second Content Area Second ContentArea Second Content Area Second Content 
	                Area Second Content Area 
                </div> 
                </Content> 
                </cc1:AccordionPane> 
                <cc1:AccordionPane ID="AccordionPane3" runat="server"> 
                <Header> 
                <a href=""onclick="return false;" class="Link" > 
                Open 
                </a> 
                </Header> 
                <Content> 
                <div> 
                Third Content Area Third Content AreaThird Content Area Third Content 
	                Area Third Content Area 
                </div> 
                </Content> 
                </cc1:AccordionPane> 
                <cc1:AccordionPane ID="AccordionPane4" runat="server"> 
                <Header> 
                <a href=""onclick="return false;" class="Link" > 
                Open 
                </a> 
                </Header> 
                <Content> 
                <div> 
                Fourth Content Area Fourth Content AreaFourth Content Area 
                Fourth Content Area Fourth Content Area 
                </div> 
                </Content> 
                </cc1:AccordionPane> 
                </Panes>
        </cc1:Accordion> 
    </form>
</body>
</html>
