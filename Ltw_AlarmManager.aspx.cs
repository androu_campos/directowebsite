using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Ltw_AlarmManager : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)//Edita las Alarmas del NOC
    {
        if (!Page.IsPostBack)
        {
            CheckVacio();
        }
    }

    private void CheckVacio()
    {
        SqlCommand sqlQuery = new SqlCommand();
        sqlQuery.CommandText = "SELECT Count(*) as Lineas FROM MECCa2..NocAlarms";
        DataSet resultSet;
        resultSet = Util.RunQuery(sqlQuery);
        if (resultSet.Tables.Count == 0)
        {
            dvAlarm.DefaultMode = DetailsViewMode.Insert;
        }

    }

    protected void gvSumary_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        CheckVacio();
    }

    protected void dvAlarm_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
    {
        CheckVacio();
    }

    protected void MECCA_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("http://mecca3.computer-tel.com/mecca/");
    }
}
