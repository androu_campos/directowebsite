<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="InvoiceMaker.aspx.cs" Inherits="InvoiceMaker" Title="Untitled Page" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Invoice Maker" Font-Bold="True" Width="90px"></asp:Label></td>
            <td width="95%">
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td height="30">
            </td>
            <td height="30">
            </td>
            <td height="30">
            </td>
            <td height="30">
            </td>
            <td height="30">
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Customer"></asp:Label></td>
            <td>
                <asp:DropDownList ID="dpdCustomer" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
            <td align="center" bgcolor="#d5e3f0" valign="middle">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Select Rounding"></asp:Label></td>
            <td align="center" bgcolor="#d5e3f0" valign="top">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Select GMT"></asp:Label></td>
            <td align="center" rowspan="4" valign="top">
                <asp:UpdateProgress id="UpdateProgress1" runat="server">
                    <progresstemplate>
<TABLE><TR><TD style="WIDTH: 100px"><asp:Label id="Label5" runat="server" Text="Loading...Please wait" ForeColor="LimeGreen" Width="103px" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/Loading.gif"></asp:Image></TD></TR></TABLE>
</progresstemplate>
                </asp:UpdateProgress></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" valign="top">
                <asp:Label ID="lblCountry" runat="server" CssClass="labelBlue8" Text="Country"></asp:Label></td>
            <td valign="top">
                <asp:DropDownList ID="dpdCountry" runat="server" CssClass="dropdown">
                </asp:DropDownList></td>
            <td align="center" rowspan="3" valign="top">
                <asp:RadioButtonList ID="rblRounding" runat="server" CssClass="dropdown">
                    <asp:ListItem Selected="True">Mex 60 - ROW 1</asp:ListItem>
                    <asp:ListItem>Mex 60 - ROW 306</asp:ListItem>
                </asp:RadioButtonList></td>
            <td align="center" valign="top">
                <asp:DropDownList ID="dpdGmt" runat="server" CssClass="dropdown">
                    <asp:ListItem Value="-5">-11</asp:ListItem>
                    <asp:ListItem Value="-4">-10</asp:ListItem>
                    <asp:ListItem Value="-3">-9</asp:ListItem>
                    <asp:ListItem Value="-2">-8</asp:ListItem>
                    <asp:ListItem Value="-1">-7</asp:ListItem>
                    <asp:ListItem Value="0">-6</asp:ListItem>
                    <asp:ListItem Value="1">-5</asp:ListItem>
                    <asp:ListItem Value="2">-4</asp:ListItem>
                    <asp:ListItem Value="3">-3</asp:ListItem>
                    <asp:ListItem Value="4">-2</asp:ListItem>
                    <asp:ListItem Value="5">-1</asp:ListItem>
                    <asp:ListItem Value="6">0</asp:ListItem>
                    <asp:ListItem Value="7">1</asp:ListItem>
                    <asp:ListItem Value="8">2</asp:ListItem>
                    <asp:ListItem Value="9">3</asp:ListItem>
                    <asp:ListItem Value="10">4</asp:ListItem>
                    <asp:ListItem Value="10">5</asp:ListItem>
                    <asp:ListItem Value="11">6</asp:ListItem>
                    <asp:ListItem Value="12">7</asp:ListItem>
                    <asp:ListItem Value="13">8</asp:ListItem>
                    <asp:ListItem Value="14">9</asp:ListItem>
                    <asp:ListItem Value="15">10</asp:ListItem>
                    <asp:ListItem Value="16">11</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0">
                <asp:Label ID="lblFrom" runat="server" CssClass="labelBlue8" Text="From"></asp:Label></td>
            <td style="height: 21px">
                <asp:TextBox ID="txtFrom" runat="server" CssClass="dropdown" Width="124px"></asp:TextBox>&nbsp;<asp:Image
                    ID="imgFrom" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td rowspan="2" valign="top">
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0">
                <asp:Label ID="lblTo" runat="server" CssClass="labelBlue8" Text="To"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtTo" runat="server" CssClass="dropdown" Width="124px"></asp:TextBox>&nbsp;<asp:Image
                    ID="imgTo" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
        </tr>
        <tr>
            <td align="center">
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="cmdReport" runat="server" CssClass="boton" Text="View Report" OnClick="cmdReport_Click" /></td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="5">                

            </td>
        </tr>
    </table>
                
<TABLE align="left">
    <tr>
        <td style="width: 100px" valign="top">
            <asp:Button ID="cmdExport" runat="server" CssClass="boton" OnClick="cmdExport_Click"
                Text="Export Invoice" />
            <asp:Button ID="cmdExportDetails" runat="server" CssClass="boton" OnClick="cmdExportDetails_Click"
                Text="Export Details" /></td>
        <td style="width: 100px" valign="top">
        </td>
        <td style="width: 82px" valign="top">
        </td>
    </tr>
    <TR><TD style="WIDTH: 100px" valign="top">
    <asp:GridView ID="gdvPart1" runat="server" Font-Names="Arial" Font-Size="8pt">
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
            Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</td>
    <td style="width: 100px" valign="top">
        <asp:GridView ID="gdvPart2" runat="server" Font-Names="Arial" Font-Size="8pt">
            <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
            <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                Font-Size="8pt" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </td>
    <td style="width: 82px" valign="top">
        <asp:GridView ID="gdvPart3" runat="server" Font-Names="Arial" Font-Size="8pt">
            <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
            <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                Font-Size="8pt" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </td>
</tr>
</table>
    <br />
    <br />
    <table align="left">
        <tr>
            <td style="width: 100px" valign="top">
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgFrom"
                    TargetControlID="txtFrom" Format="MM/dd/yyyy HH:mm:ss">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="imgTo"
                    TargetControlID="txtTo" Format="MM/dd/yyyy HH:mm:ss">
                </cc1:CalendarExtender>
            </td>
        </tr>
    </table>
</asp:Content>

