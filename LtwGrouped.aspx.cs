using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;


public partial class LtwGrouped : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DirectoUser"].ToString() == "yurema.villa" || Session["DirectoUser"].ToString() == "maria.merlos" 
            || Session["DirectoUser"].ToString() == "david" || Session["DirectoUser"].ToString() == "jbaumhart" 
            || Session["DirectoUser"].ToString() == "alfonso.villareal" || Session["DirectoUser"].ToString() == "abautista" 
            || Session["DirectoUser"].ToString() == "rodrigo.andrade" || Session["DirectoUser"].ToString() == "fcarbia" 
            || Session["DirectoUser"].ToString() == "lourdes.blumenkron" )
        {
            makeHtml();
        }
        else
        {
            Response.Redirect("Home.aspx", false);
        }

    }

    private void makeHtml()
    {
        try
        {
            string sql = "";
            DataSet ds = null;
            int tvendors = 0;
            int cust = 0;

            //sql = "select '**ALL**' as Customer,sum(Live_calls) as Total ";
            //sql += " from cdrdb..cdrn n join cdrdb..providerip p ";
            //sql += " on n.Cust_Vendor = P.ip	 ";
            //sql += " where left([Time],19) in ";
            //sql += " (Select left(max([Time]),19) from cdrdb..cdrn) and n.type = 'Customer' ";
            //sql += " and cust_vendor not like '*%' ";
            //sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') and (id in ('NY03','NY07','MIA02','STC01') or (id = 'MIA03' and cust_vendor in ('DIP5','DIP2'))) and cust_vendor not in ('DIDP','DIDN') ";
            //sql += " UNION ";
            //sql += " select case when p.providername in ('ICS-800-CARDS','ICS-WEB-DID','ICS-WEB-UNL','ICS-800-WEB', ";
            //sql += " 'ICS-800-PARAMOUNT','ICS-PARAMOUNT','ICS-800-TSI','ICS','ICS-TSI','ICS-PHONETIME','ICS-800-TITAN') ";
            //sql += " THEN 'ICS-ALL-CARDS' ";
            //sql += " when p.providername in ('ICS-PREMIUM','ICS-PREMIUM-UNL','ICS-PAC-WEST-5','ICS-NUVOX-5') ";
            //sql += " THEN 'ICS-RELOADABLE' WHEN p.providername in ('ICS-PAC-WEST-2','ICS-NUVOX-2','ICS-PREMIUM-UNL-2') THEN 'ICS-RELOADABLE-2' ELSE p.providername END Customer, sum(n.Live_Calls ) Calls  ";
            //sql += " from cdrdb..cdrn n join cdrdb..providerip p  ";
            //sql += " on n.Cust_Vendor = P.ip ";	  
            //sql += " where left([Time],19) in ";
            //sql += " (Select left(max([Time]),19) from cdrdb..cdrn) and n.type = 'Customer' ";
            //sql += " and cust_vendor not like '*%'  ";
            //sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') and (id in ('NY03','NY07','MIA02','STC01') or (id = 'MIA03' and cust_vendor in ('DIP5','DIP2','ICS2','ICS5','DIDV','DIDB'))) and cust_vendor not in ('DIDP','DIDN') ";
            //sql += " group by case when p.providername in ('ICS-800-CARDS','ICS-WEB-DID','ICS-WEB-UNL','ICS-800-WEB', ";
            //sql += " 'ICS-800-PARAMOUNT','ICS-PARAMOUNT','ICS-800-TSI','ICS','ICS-TSI','ICS-PHONETIME','ICS-800-TITAN') ";
            //sql += " THEN 'ICS-ALL-CARDS' ";
            //sql += " when p.providername in ('ICS-PREMIUM','ICS-PREMIUM-UNL','ICS-PAC-WEST-5','ICS-NUVOX-5') ";
            //sql += " THEN 'ICS-RELOADABLE' when p.providername in ('ICS-PAC-WEST-2','ICS-NUVOX-2','ICS-PREMIUM-UNL-2') THEN 'ICS-RELOADABLE-2' ELSE p.providername END ";
            //sql += " order by sum(n.Live_Calls ) desc ";

            //sql = "select '**ALL**' as Customer,sum(Live_calls) as Total ";
            //sql += " from cdrdb..cdrn n join cdrdb..providerip p ";
            //sql += " on n.Cust_Vendor = P.ip	 ";
            //sql += " where left([Time],19) in ";
            //sql += " (Select left(max([Time]),19) from cdrdb..cdrn) and n.type = 'Customer' ";
            //sql += " and cust_vendor not like '*%' ";
            //sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') and (id in ('NY03','NY07','MIA02','STC01','STC03','MIA01') or (id = 'MIA03' and cust_vendor in ('DIP5','DIP2','ICS2','ICS5'))) and cust_vendor not in ('DIDP','DIDN') ";
            //sql += " UNION ";
            //sql += " select p.providername AS Customer, sum(n.Live_Calls ) Calls  ";
            //sql += " from cdrdb..cdrn n join (select providername,ip from cdrdb..providerip where providername not like 'ICS%' union select providername,ip from cdrdb..providerip where providername in ('ICS-CARDS-2DLS','ICS-CARDS-5DLS')) p  ";
            //sql += " on n.Cust_Vendor = P.ip ";
            //sql += " where left([Time],19) in ";
            //sql += " (Select left(max([Time]),19) from cdrdb..cdrn) and n.type = 'Customer' ";
            //sql += " and cust_vendor not like '*%'  ";
            //sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') and (id in ('NY03','NY07','MIA02','STC01','STC03','MIA01') or (id = 'MIA03' and cust_vendor in ('DIP5','DIP2','ICS2','ICS5'))) and cust_vendor not in ('DIDP','DIDN') ";
            //sql += " group by  p.providername  ";
            //sql += " order by sum(n.Live_Calls ) desc ";

            sql = "select '**ALL**' as Customer,sum(Live_calls) as Total ";
            sql += " from cdrdb..cdrn n join cdrdb..providerip p ";
            sql += " on n.Cust_Vendor = P.ip	 ";
            sql += " where left([Time],19) in ";
            sql += " (Select left(max([Time]),19) from cdrdb..cdrn) and n.type = 'Customer' ";
            sql += " and cust_vendor not like '*%' ";
            sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') and (id in ('NY03','NY07','STC01','STC02','MIA01','MIA02','MIA03','MIA04','MIA06','MIA08','STCB','MEX') or (id = 'MIA03' and cust_vendor in ('DIP5','DIP2','ICS2','ICS5'))) and cust_vendor not in ('DIDP','DIDN') ";
            sql += " UNION ";
            sql += " select case when p.providername like 'ICS%' then 'ICS' else p.providername end AS Customer, sum(n.Live_Calls ) Calls ";
            sql += " from cdrdb..cdrn n join (select providername,ip from cdrdb..providerip where providername not in ('ICS-BLITZ','ICS-NETTOWN','ICS-UNLIMITED','ICS-NETCOMM-CCENTER','ICS-NETCOMM')) p  ";
            sql += " on n.Cust_Vendor = P.ip ";
            sql += " where left([Time],19) in ";
            sql += " (Select left(max([Time]),19) from cdrdb..cdrn) and n.type = 'Customer' ";
            sql += " and cust_vendor not like '*%'  ";
            sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') ";
            sql += " and (id in ('NY03','NY07','STC01','STC02','MIA01','MIA02','MIA03','MIA04','MIA06','MIA08','STCB','MEX') or ";
            sql += " (id = 'MIA03' and cust_vendor in ('DIP5','DIP2','ICS2','ICS5'))) ";
            sql += " and cust_vendor not in ('DIDP','DIDN') ";
            sql += " group by  case when p.providername like 'ICS%' then 'ICS' else p.providername end  ";
            sql += " order by sum(n.Live_Calls ) desc  ";


            ds = Util.RunQueryByStmnt(sql);

            Customers.Columns[0].ItemStyle.HorizontalAlign = HorizontalAlign.Left;
            Customers.Columns[0].ItemStyle.Wrap = false;
            Customers.DataSource = ds.Tables[0];
            Customers.DataBind();

            cust = Convert.ToInt32(ds.Tables[0].Rows[0][1]);

            sql = "select '**ALL**' as [E Vendor],sum(Live_calls) as Total ";
            sql += " from cdrdb..cdrn n join (select * from cdrdb..providerip where providername not like 'OUTLANDER%' AND providername not like 'NATIONAL%' ) p ";
            sql += " on n.Cust_Vendor = P.ip	 ";
            sql += " where left([Time],19) in ";
            sql += " (Select left(max([Time]),19) from cdrdb..cdrn) and n.type = 'Vendor' ";
            sql += " and cust_vendor not like '*%' ";
            sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') ";
            sql += " UNION ";
            sql += " select P.providername, sum(n.Live_Calls ) Calls ";
            sql += " from cdrdb..cdrn n join (select * from cdrdb..providerip where providername not like 'OUTLANDER%' AND providername not like 'NATIONAL%' ) p ";
            sql += " on n.Cust_Vendor = P.ip	  ";
            sql += " where left([Time],19) in ";
            sql += " (Select left(max([Time]),19) from cdrdb..cdrn) and n.type = 'Vendor' ";
            sql += " and cust_vendor not like '*%'  ";
            sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') ";
            sql += " group by p.providername order by sum(n.Live_Calls ) desc";

            ds = Util.RunQueryByStmnt(sql);

            Vendors.DataSource = ds.Tables[0];
            Vendors.DataBind();

            tvendors = Convert.ToInt32(ds.Tables[0].Rows[0][1]);

            sql = "select '**ALL**' as Outlander,sum(Live_calls) as Total ";
            sql += " from cdrdb..cdrn n join (select * from cdrdb..providerip where providername like 'OUTLANDER%' ) p ";
            sql += " on n.Cust_Vendor = P.ip	 ";
            sql += " where left([Time],19) in ";
            sql += " (Select left(max([Time]),19) from cdrdb..cdrnON) and n.type = 'Vendor' ";
            sql += " and cust_vendor not like '*%' ";
            sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') ";
            sql += " UNION ";
            sql += " select P.providername, sum(n.Live_Calls ) Calls ";
            sql += " from cdrdb..cdrn n join (select * from cdrdb..providerip where providername like 'OUTLANDER%') p ";
            sql += " on n.Cust_Vendor = P.ip	  ";
            sql += " where left([Time],19) in ";
            sql += " (Select left(max([Time]),19) from cdrdb..cdrnON) and n.type = 'Vendor' ";
            sql += " and cust_vendor not like '*%'  ";
            sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') ";
            sql += " group by p.providername order by sum(n.Live_Calls ) desc";

            ds = Util.RunQueryByStmnt(sql);

            Outlander.Columns[0].ItemStyle.HorizontalAlign = HorizontalAlign.Left;
            Outlander.Columns[0].ItemStyle.Wrap = false;

            Outlander.DataSource = ds.Tables[0];
            Outlander.DataBind();

            tvendors += Convert.ToInt32(ds.Tables[0].Rows[0][1]);

            //sql = "select '**ALL**' as [National],sum(Live_calls) as Total ";
            //sql += " from cdrdb..cdrnON n join (select * from cdrdb..providerip where providername like 'NATIONAL%' ) p ";
            //sql += " on n.Cust_Vendor = P.ip ";
            //sql += " where left([Time],19) in ";
            //sql += " (Select left(max([Time]),19) from cdrdb..cdrnON) and n.type = 'Vendor' ";
            //sql += " and cust_vendor not like '*%' ";
            //sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') ";
            //sql += " UNION ";
            //sql += " select P.providername, sum(n.Live_Calls ) Calls ";
            //sql += " from cdrdb..cdrnON n join (select * from cdrdb..providerip where providername like 'NATIONAL%') p ";
            //sql += " on n.Cust_Vendor = P.ip ";
            //sql += " where left([Time],19) in ";
            //sql += " (Select left(max([Time]),19) from cdrdb..cdrnON) and n.type = 'Vendor' ";
            //sql += " and cust_vendor not like '*%'  ";
            //sql += " and cust_vendor not in ('TRAS','TRAI','ZLCR') ";
            //sql += " group by p.providername order by sum(n.Live_Calls ) desc ";

            //ds = Util.RunQueryByStmnt(sql);

            //National.Columns[0].ItemStyle.HorizontalAlign = HorizontalAlign.Left;
            //National.Columns[0].ItemStyle.Wrap = false;

            //National.DataSource = ds.Tables[0];
            //National.DataBind();


            tvendors += Convert.ToInt32(ds.Tables[0].Rows[0][1]);

            tvendors = (cust - tvendors);


            //sql = " SELECT '(AMERICATEL, AMERICATEL-PERU, CELL-TERMINATION, CONVERGIA, ";
            //sql += "IBASIS, INTER-CONNECTION-LLC, KEEN-OCEAN, NETSERVICE, NEXTCOM, NEXTCOM-OFFNET, ";
            //sql += "TELEFONICA-BRASIL, TELWISE, TRANSITAR, )' AS GK,'" + tvendors + "'AS Total ";

            //ds = Util.RunQueryByStmnt(sql);

            //GK.DataSource = ds.Tables[0];
            //GK.DataBind();



            //SqlCommand sqlCommand = new SqlCommand();
            //sqlCommand.CommandType = CommandType.StoredProcedure;
            //sqlCommand.CommandText = "sp_LtwGrouped";
            //sqlCommand.Parameters.Add("type", SqlDbType.Char).Value = type;

            //StringBuilder myString = new StringBuilder();
            //myString.Append("\n");
            //DataTable tbl = Util.RunQueryatCDRDB(sqlCommand).Tables[0];
            //foreach (DataRow myRow in tbl.Rows)
            //{
            //    myString.Append("                   <tr>\n");
            //    if (myRow["Cust_Vendor"].Equals(" **ALL** "))
            //    {
            //        myString.Append("                       <td style=\"font-family:Arial;font-size:9pt;font-weight:bold;\"> **ALL** </td>\n");
            //        myString.Append("                       <td style=\"font-family:Arial;font-size:9pt;font-weight:bold;\">" + myRow["TotalCalls"].ToString() + "</td>\n");
            //    }
            //    else
            //    {
            //        myString.Append("                       <td style=\"font-family:Arial;font-size:8pt;\">" + myRow["ProviderName"].ToString() + "</td>\n");
            //        myString.Append("                       <td style=\"font-family:Arial;font-size:8pt;\">" + myRow["TotalCalls"].ToString() + "</td>\n");
            //    }
            //    myString.Append("                   </tr>\n");
            //}
            //string table = myString.ToString();
            //return table;
        }
        catch (Exception ex)
        {



        }
    }
}
