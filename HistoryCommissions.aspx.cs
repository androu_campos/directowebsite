using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text;

public partial class test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            DateTime firstDayOfTheMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adpBR = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
            MyDataSetTableAdapters.Billing_StatusDataTable tblBR = adpBR.GetData();
            if (tblBR.Rows.Count > 0)//Billing running
            {
                lblNotMonth.Visible = true;
                lblNotYesterday.Visible = true;
                lblNotPrevious.Visible = true;
                lblNotProfit.Visible = true;
            }
            else
            {
                if (Session["DirectoUser"].ToString() == "vicente" || Session["DirectoUser"].ToString() == "huguette" || Session["DirectoUser"].ToString() == "david" || Session["DirectoUser"].ToString() == "felipe" || Session["DirectoUser"].ToString() == "rafael" || Session["DirectoUser"].ToString() == "rmanja" || Session["DirectoUser"].ToString() == "octavio" || Session["DirectoUser"].ToString() == "Fernando" || Session["DirectoUser"].ToString() == "hagi")
                {

                    lblNotMonth.Visible = false;
                    lblNotYesterday.Visible = false;
                    lblNotPrevious.Visible = false;
                    lblNotProfit.Visible = false;
                    //Month to Date
                    DataSet myDtstMnth = Util.RunQueryByStmnt("Select Broker,Minutes,Calls,Profit FROM [MECCA2].[dbo].[ResumeCommissions] where Id_Date=1");
                    DataTable tblMnth = myDtstMnth.Tables[0];
                    gdvMonth.DataSource = myDtstMnth.Tables[0];
                    gdvMonth.DataBind();
                    //Yesterday
                    DataSet myDtst = Util.RunQueryByStmnt("Select Broker,Minutes,Calls,Profit FROM [MECCA2].[dbo].[ResumeCommissions] where Id_Date=2");
                    DataTable tblYst = myDtst.Tables[0];
                    tblYst.Columns.Add("ImageStatus");


                    //Previous Day
                    DataSet myDtstPrv = Util.RunQueryByStmnt("Select Broker,Minutes,Calls,Profit FROM [MECCA2].[dbo].[ResumeCommissions] where Id_Date=3");
                    DataTable tblPrev = myDtstPrv.Tables[0];
                    gdvPrev.DataSource = myDtstPrv.Tables[0];
                    gdvPrev.DataBind();
                    //**//
                    for (int z = 0; z < 8; z++)
                    {
                        decimal gP = Convert.ToDecimal(tblYst.Rows[z]["Profit"].ToString());
                        decimal gp2 = Convert.ToDecimal(tblPrev.Rows[z]["Profit"].ToString());

                        if (z == 0)//top Andre
                        {

                            if (gP > gp2)
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                            }
                            else
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                            }
                        }
                        else if (z == 1)//Hamit
                        {
                            if (gP > gp2)
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                            }
                            else
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                            }
                        }
                        else if (z == 2)//Iliana
                        {
                            if (gP > gp2)
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                            }
                            else
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                            }
                        }
                        else if (z == 3)//Jake
                        {
                            if (gP > gp2)
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                            }
                            else
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                            }
                        }
                        else if (z == 4)//Jimb
                        {
                            if (gP > gp2)
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                            }
                            else
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                            }
                        }
                        else if (z == 5)//Martha
                        {
                            if (gP > gp2)
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                            }
                            else
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                            }
                        }
                        else if (z == 6)//Michelle
                        {
                            if (gP > gp2)
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                            }
                            else
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                            }
                        }
                        else if (z == 7)//Roberto
                        {
                            if (gP > gp2)
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                            }
                            else
                            {
                                tblYst.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                            }
                        }
                        //else if (z == 8)//Roberto
                        //{
                        //    if (gP > gp2)
                        //    {
                        //        tblYst.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                        //    }
                        //    else
                        //    {
                        //        tblYst.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                        //    }
                        //}

                    }

                    //Bind Yesterday
                    gdvYestr.DataSource = tblYst;
                    gdvYestr.DataBind();
                    //Projected this Month
                    Projected(tblMnth);
             
                    MakeHistory();

                }
                else
                {
                    Response.Redirect("Home.aspx");
                }

                }

            }                                            
        catch (Exception ex)
        {
            string error = ex.Message;
        }                                
    }

    private void RunCtryCmmsn(string storeProcedure,string fromY,string toY, string fromPY, string toPY,int flag )
    {
        try
        {
            DateTime firstDayOfTheMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = System.Data.CommandType.StoredProcedure;
            sqlSpQuery.CommandText = storeProcedure;
            sqlSpQuery.Parameters.Add("@fromP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();//fromY; //
            sqlSpQuery.Parameters.Add("@toP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();//toY; //
            sqlSpQuery.CommandTimeout = 160;
            DataSet ResultSetC;
            ResultSetC = Util.RunQuery(sqlSpQuery);
            DataTable tbl = (DataTable)ResultSetC.Tables[0].Copy();//yesterday
            for (int k = 1; k < ResultSetC.Tables.Count; k++)
            {
                tbl.LoadDataRow(ResultSetC.Tables[k].Rows[0].ItemArray, false);
            }

            SqlCommand sqlSpQuery2 = new SqlCommand();
            sqlSpQuery2.CommandType = System.Data.CommandType.StoredProcedure;
            sqlSpQuery2.CommandText = storeProcedure;
            sqlSpQuery2.Parameters.Add("@fromP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-2).ToShortDateString();//fromPY;// 
            sqlSpQuery2.Parameters.Add("@toP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-2).ToShortDateString();//toPY; // 
            sqlSpQuery2.CommandTimeout = 160;
            DataSet ResultSet2;
            ResultSet2 = Util.RunQuery(sqlSpQuery2);
            DataTable tbl2 = (DataTable)ResultSet2.Tables[0].Copy();//Prev Day

            for (int k = 1; k < ResultSetC.Tables.Count; k++)
            {
                tbl2.LoadDataRow(ResultSet2.Tables[k].Rows[0].ItemArray, false);
            }

            //if (flag == 1)
            //{
                DataTable tblYest1 = tbl.Copy();
                tblYest1.Columns.Add("ImageStatus");
                
                DataTable tblYest2 = tbl2.Copy();

                for (int z = 0; z < 9; z++)
                {
                    decimal gP = Convert.ToDecimal(tblYest1.Rows[z]["GrossProfit"].ToString());
                    decimal gp2 = Convert.ToDecimal(tblYest2.Rows[z]["GrossProfit"].ToString());

                    if (z == 0)//top Andre
                    {

                        if (gP > gp2)
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                        }
                        else
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                        }
                    }
                    else if (z == 1)//Hamit
                    {                        
                        if (gP > gp2)
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                        }
                        else
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                        }
                    }
                    else if (z == 2)//Iliana
                    {
                        if (gP > gp2)
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                        }
                        else
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                        }
                    }
                    else if (z == 3)//Jake
                    {
                        if (gP > gp2)
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                        }
                        else
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                        }
                    }
                    else if (z == 4)//Jimb
                    {
                        if (gP > gp2)
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                        }
                        else
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                        }
                    }
                    else if (z == 5)//Martha
                    {
                        if (gP > gp2)
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                        }
                        else
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                        }
                    }
                    else if (z == 6)//Michelle
                    {
                        if (gP > gp2)
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                        }
                        else
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                        }
                    }
                    else if (z == 7)//Roberto
                    {                        
                        if (gP > gp2)
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                        }
                        else
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                        }
                    }
                    else if (z == 8)//Roberto
                    {
                        if (gP > gp2)
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/verde.gif";
                        }
                        else
                        {
                            tblYest1.Rows[z]["ImageStatus"] = "./Images/roja.gif";
                        }
                    }

                }
                tblYest1.Columns.Add("Broker");
                tblYest1.Rows[0]["Broker"] = "Vicente";
                tblYest1.Rows[1]["Broker"] = "Roberto";
                tblYest1.Rows[2]["Broker"] = "Michelle";
                tblYest1.Rows[3]["Broker"] = "Martha";
                tblYest1.Rows[4]["Broker"] = "Jimb";
                tblYest1.Rows[5]["Broker"] = "Jake";
                tblYest1.Rows[6]["Broker"] = "Iliana";
                tblYest1.Rows[7]["Broker"] = "Hamit";
                tblYest1.Rows[8]["Broker"] = "Andre";

                gdvYestr.DataSource = tblYest1;
                gdvYestr.DataBind();
//            }

            //else if (flag == 2)//Prev Yesterday
            //{                
//****             
                DataTable tblPrv1 = tbl2.Copy();
                SqlCommand sqlSpQueryP = new SqlCommand();
                sqlSpQueryP.CommandType = System.Data.CommandType.StoredProcedure;
                sqlSpQueryP.CommandText = storeProcedure;
                sqlSpQueryP.Parameters.Add("@fromP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-3).ToShortDateString();// fromPY;// 
                sqlSpQueryP.Parameters.Add("@toP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-3).ToShortDateString(); //toPY; //                 
                DataSet ResultSetPv;
                ResultSetPv = Util.RunQuery(sqlSpQueryP);
                DataTable tblPrv2 = (DataTable)ResultSetPv.Tables[0].Copy();// 3 day before

                for (int k = 1; k < ResultSetC.Tables.Count; k++)
                {
                    tblPrv2.LoadDataRow(ResultSetPv.Tables[k].Rows[0].ItemArray, false);
                }
                tblPrv1.Columns.Add("ImageStatus");                
                gdvPrev.DataSource = tblPrv1;
                gdvPrev.DataBind();
//            }
//            else // Month to Date
//            {
                SqlCommand sqlSpQueryMnth = new SqlCommand();
                sqlSpQueryMnth.CommandType = System.Data.CommandType.StoredProcedure;
                sqlSpQueryMnth.CommandText = storeProcedure;
                sqlSpQueryMnth.Parameters.Add("@fromP", SqlDbType.VarChar).Value = firstDayOfTheMonth.ToShortDateString();// fromPY;// 
                sqlSpQueryMnth.Parameters.Add("@toP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString(); //toY; //
                sqlSpQueryMnth.CommandTimeout = 160;
                DataSet ResultSetM;
                ResultSetM = Util.RunQuery(sqlSpQueryMnth);
                DataTable tblMnth = (DataTable)ResultSetM.Tables[0].Copy();//

                for (int k = 1; k < ResultSetM.Tables.Count; k++)
                {
                    tblMnth.LoadDataRow(ResultSetM.Tables[k].Rows[0].ItemArray, false);
                }
                //CALL PROJECTED
                Projected(tblMnth);                              
                gdvMonth.DataSource = tblMnth;
                gdvMonth.DataBind();
//            }

            
        }
        catch (Exception ex)
        {
            string errormessage = ex.Message.ToString();
        }

    }

    private void Projected(DataTable tblMonth)
    {
        try
        {
            DataTable tblProjected = tblMonth.Copy();            
            tblProjected.Columns.Add("Flag");

            string qry = "SELECT TOP 9 [CM],[AnsweredCalls],[Minutes],[Profit] FROM [MECCA2].[dbo].[HistoryCommissions] order by DateLog desc";
            DataTable tblPrevMonth = Util.RunQueryByStmnt(qry).Tables[0];
            
            int count= tblPrevMonth.Rows.Count;
            count -= 1;
            for (int i = 0; i < tblMonth.Rows.Count; i++)
            {
                tblProjected.Rows[i][2] = (Convert.ToInt32(tblMonth.Rows[i][2].ToString()) / Convert.ToInt32(DateTime.Now.AddDays(-1).Day)) * 30;
                tblProjected.Rows[i][1] = (Convert.ToDecimal(tblMonth.Rows[i][1].ToString()) / Convert.ToInt32(DateTime.Now.AddDays(-1).Day)) * 30;
                tblProjected.Rows[i][3] = (Convert.ToDecimal(tblMonth.Rows[i][3].ToString()) / Convert.ToDecimal(DateTime.Now.AddDays(-1).Day)) * 30;
                
                if (  Convert.ToDecimal(tblProjected.Rows[i][3].ToString())   >   Convert.ToDecimal(tblPrevMonth.Rows[count][3].ToString())  )
                {
                    tblProjected.Rows[i]["Flag"] = "./Images/verde.gif";
                    count -= 1;
                }
                else
                {
                    tblProjected.Rows[i]["Flag"] = "./Images/roja.gif";
                    count -= 1;
                }

                
            }
            gdvProj.DataSource = tblProjected;
            gdvProj.DataBind();            
        }
        catch (Exception ex)
        {
            string errorMessage = ex.Message.ToString();
        }
    }

    private void MakeHistory()
    {
        try
        {
            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = CommandType.Text;
            sqlSpQuery.CommandText = "SELECT TOP 45 [CM],[AnsweredCalls],[Minutes],[Profit] FROM [MECCA2].[dbo].[HistoryCommissions] order by DateLog desc";
            DataSet myDtst = Util.RunQuery(sqlSpQuery);

            lblMonth0.Text = Util.GetMonth(Convert.ToInt32( DateTime.Now.AddMonths(-1).Month));
            gdvMonth1.DataSource = myDtst.Tables[0];
            gdvMonth1.DataBind();
            gdvMonth1.PageIndex = 0;

            lblMonth1.Text = Util.GetMonth(Convert.ToInt32(DateTime.Now.AddMonths(-2).Month));
            gdvMonth2.DataSource = myDtst.Tables[0];
            gdvMonth2.DataBind();
            gdvMonth2.PageSize = 1;

            lblMonth2.Text = Util.GetMonth(Convert.ToInt32(DateTime.Now.AddMonths(-3).Month));
            gdvMonth3.DataSource = myDtst.Tables[0];
            gdvMonth3.DataBind();
            gdvMonth3.PageSize = 2;

            lblMonth3.Text = Util.GetMonth(Convert.ToInt32(DateTime.Now.AddMonths(-4).Month));
            gdvMonth4.DataSource = myDtst.Tables[0];
            gdvMonth4.DataBind();
            gdvMonth4.PageSize = 3;

            lblMonth4.Text = Util.GetMonth(Convert.ToInt32(DateTime.Now.AddMonths(-5).Month));
            gdvMonth5.DataSource = myDtst.Tables[0];
            gdvMonth5.DataBind();
            gdvMonth5.PageSize = 4;



        }
        catch (Exception ex)
        {
            string message = ex.Message.ToString();
        }

    }  
               
    public void Hilos()
    {
        System.Threading.Thread.Sleep(20000);
        this.LoadComplete += new EventHandler(test_LoadComplete);
    }

    void test_LoadComplete(object sender, EventArgs e)
    {
        //algo
    }


}
