<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" Theme="Theme1"
    AutoEventWireup="true" CodeFile="RatesUpload.aspx.cs" Inherits="RatesUpload"
    Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table style="position:absolute; top:220px; left:230px;">
        <tr>
            <td style="width: 100px">
                <asp:FileUpload ID="FU" runat="server" /></td>
            <td style="width: 100px">
                <asp:Button ID="btnApply" runat="server" OnClick="btnApply_Click" Text="Apply" CssClass="boton" /></td>
            <td style="width: 100px">
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/RateSchedule.aspx" Width="87px">Rate Schedule</asp:HyperLink></td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="lblUpload" runat="server" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <%--<asp:Button ID="btnNext" runat="server" Text="Next" Enabled="False"
                    CssClass="boton" OnClick = "btnNext_c"/>--%>
                <asp:HyperLink ID="lnknext" runat ="server" Text="Next" Visible="false" NavigateUrl= "~/RatesAssign.aspx"></asp:HyperLink>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
    &nbsp;
    <table style="position:absolute; top:320px; left:230px;">
        <tr>
            <td style="width: 103px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager></td>
        </tr>
        <tr>
            <td style="width: 103px" valign="top" align="left">
                <asp:Label ID="Label1" runat="server" Text="Good Records" Visible="False" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px" valign="top" align="left">
                <asp:Label ID="Label2" runat="server" Text="Bad Records" Visible="False" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px" valign="top" align="left">
                <asp:Label ID="Label3" runat="server" Text="Removed Duplicates" Visible="False" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px" valign="top" align="left">
                <asp:Label ID="Label4" runat="server" Text="Duplicate Requiring Attention" Visible="False" CssClass="labelBlue8" Width="154px"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 103px" valign="top">
                <asp:GridView ID="gvGood" runat="server" Visible="False" Font-Names="Arial" Font-Size="8pt">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <asp:SqlDataSource ID="sqlsaGood" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>" SelectCommand="SELECT ProviderName, Prefix, Rate FROM RatesTmpBuena">
                </asp:SqlDataSource>
            </td>
            <td style="width: 100px" valign="top">
                <asp:GridView ID="gvBad" runat="server" DataSourceID="sqldsBad" Visible="False" Font-Names="Arial" Font-Size="8pt">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <asp:SqlDataSource ID="sqldsBad" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>" SelectCommand="SELECT ProviderName, Prefix, Rate FROM RatesTmpMal">
                </asp:SqlDataSource>
            </td>
            <td style="width: 100px" valign="top">
                <asp:GridView ID="gvDupDEl" runat="server" Visible="False" DataSourceID="sqldsDupDel" Font-Names="Arial" Font-Size="8pt">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <asp:SqlDataSource ID="sqldsDupDel" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>" SelectCommand="SELECT ProviderName, Prefix, Rate FROM RRatesDup">
                </asp:SqlDataSource>
            </td>
            <td style="width: 100px" valign="top">
                <asp:UpdatePanel id="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <contenttemplate>
<asp:GridView accessKey="0" id="GridView1" runat="server" __designer:dtid="7036874417766459" OnRowCommand="GridView1_RowCommand" AutoGenerateEditButton="True" __designer:wfdid="w11" OnRowEditing="GridView1_RowEditing" OnRowDeleting="GridView1_RowDeleting" DataKeyNames="ProviderName,Prefix,Ty">
<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
</asp:GridView> <asp:Button id="Button1" runat="server" Text="Button" ForeColor="White" BackColor="White" __designer:wfdid="w13" BorderColor="White" BorderStyle="None"></asp:Button> <cc1:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w20" TargetControlID="Button1" PopupControlID="Panel1" DropShadow="true" BackgroundCssClass="modalBackground" SkinID="modalBackground"></cc1:ModalPopupExtender> 
</contenttemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
    </table>
    <asp:Panel ID="Panel1" runat="server" Height="84px" Width="397px" BackColor="SteelBlue" OnLoad="Panel1_Load">
        <br />
        <table>
            <tr>
                <td style="width: 100px; height: 21px;">
                    </td>
                <td style="width: 100px; height: 21px;">
                    </td>
                <td style="width: 100px; height: 21px;">
                    </td>
                <td style="width: 100px; height: 21px;">
                </td>
                <td style="width: 100px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    </td>
                <td style="width: 100px">
                    <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" ForeColor="White" Text="New rate:"></asp:Label></td>
                <td style="width: 100px">
        <asp:TextBox ID="TextBox1" runat="server" CssClass="labelSteelBlue" MaxLength="6"></asp:TextBox></td>
                <td style="width: 100px">
        <asp:Button ID="Button2" runat="server" Text="Fix" CssClass="boton" OnClick="Button2_Click" /></td>
                <td style="width: 100px">
        </td>
            </tr>
            <tr>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" FilterType="Custom, Numbers" ValidChars="." runat="server" TargetControlID="TextBox1">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
            </tr>
        </table>
        &nbsp;
    </asp:Panel>
</asp:Content>
