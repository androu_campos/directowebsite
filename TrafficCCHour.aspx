<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="TrafficCCHour.aspx.cs"
    Inherits="OutlanderReport" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 159px">
        <tr>
            <td>
                <asp:Label ID="lblBilling" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="Label"
                    Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <asp:Label ID="lblHeader" runat="server" CssClass="labelBlue8" Font-Bold="True" Font-Size="10pt"
                    Text=""></asp:Label>
            </td>
        </tr>
    </table>
    <table style="width: 159px">
        <tr>
            <td style="width: 50px" align="center" >
                <asp:Label ID="lblAg" runat="server" CssClass="labelBlue8" Text="Agency" Visible="false" BackColor="#d5e3f0"></asp:Label></td>
            <td style="width: 117px; height: 24px;">
                <asp:DropDownList ID="dpdAgen" runat="server" CssClass="dropdown" AutoPostBack="true"
                    OnSelectedIndexChanged="dpdAgen_SelectedIndexChanged" Visible="false">
                    <asp:ListItem Text="**ALL**" Value="**ALL**"></asp:ListItem>
                    <asp:ListItem Text="BENTO" Value="BENTO"></asp:ListItem>
                    <asp:ListItem Text="CONTACTO" Value="CONTACTO"></asp:ListItem>
                    <asp:ListItem Text="ECI" Value="ECI"></asp:ListItem>
                    <asp:ListItem Text="ERGON" Value="ERGON"></asp:ListItem>
                    <asp:ListItem Text="MEGACAL" Value="MEGACAL"></asp:ListItem>
                    <asp:ListItem Text="MKT911" Value="MKT911"></asp:ListItem>
                    <asp:ListItem Text="STO" Value="STO"></asp:ListItem>
                    <asp:ListItem Text="TARGETONE" Value="TARGETONE"></asp:ListItem>
                    <asp:ListItem Text="VCIP" Value="VCIP"></asp:ListItem>
                </asp:DropDownList></td>
            <td align="center" width="61" style="height: 24px">
                <asp:Label ID="lblCC" runat="server" CssClass="labelBlue8" Text="CallCenter:" Visible="false" BackColor="#d5e3f0"></asp:Label></td>
            <td style="width: 117px; height: 24px;">
                <asp:DropDownList ID="dpdCC" runat="server" CssClass="dropdown" Enabled="false" Visible="false">
                    <asp:ListItem Text="**ALL**" Value="**ALL**"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="right" style="width: 108px; height: 24px;">
                <asp:Button ID="cmdReport" runat="server" CssClass="boton" OnClick="cmdReport_Click"
                    Text="View Stats" Visible="false" /></td>
        </tr>
        <tr>
            <td align="right" style="width: 321px">
                <asp:Label ID="lblWarning" runat="server" CssClass="labelBlue8" Font-Bold="True"
                    ForeColor="Red" Text="Nothing found!!" Visible="False" Width="92px"></asp:Label></td>
        </tr>
    </table>
    <table>        
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Today Traffic"></asp:Label><br />
                <asp:GridView ID="gdvToday" runat="server" Font-Names="Arial" Font-Size="8pt" Width="700px"
                    AllowPaging="True" OnPageIndexChanging="gdvToday_PageIndexChanging" PageSize="20">
                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                    <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                    <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                    <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial"
                        Font-Bold="True" ForeColor="Orange"></HeaderStyle>
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                </asp:GridView>
                <asp:Label ID="lblToday" runat="server" CssClass="labelBlue8" Text=""></asp:Label><br />
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Button ID="bexport_today" runat="server" Text="Excel" CssClass="boton" OnClick="exportTodays" />
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 23px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Yesterday Traffic"></asp:Label><br />
                <asp:GridView ID="gdvYesterday" runat="server" Font-Names="Arial" Font-Size="8pt"
                    Width="700px" AllowPaging="True" OnPageIndexChanging="gdvYesterday_PageIndexChanging"
                    PageSize="20">
                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                    <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                    <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                    <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial"
                        Font-Bold="True" ForeColor="Orange"></HeaderStyle>
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                </asp:GridView>
                <asp:Label ID="lblYesterday" runat="server" CssClass="labelBlue8" Text=""></asp:Label><br />
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Button ID="bexport_yesterday" runat="server" Text="Excel" CssClass="boton" OnClick="exportYesterdays" />
            </td>
        </tr>
        <%--<tr>
                <td style="width: 100px; height: 24px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Yesterday Peak Traffic"></asp:Label><br />
                    <asp:GridView ID="gdvYesterdayHour" runat="server" Font-Names="Arial" Font-Size="8pt" Width="100%">
                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                    <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                    <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                    <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial" Font-Bold="True" ForeColor="Orange"></HeaderStyle>
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                    </asp:GridView>
                    </td>
            </tr>--%>
        <tr>
            <td style="width: 106px; height: 44px;" valign="top">
                <asp:Timer ID="Timer1" runat="server" Interval="300000">
                </asp:Timer>
            </td>
            <td style="width: 100px; height: 44px;" valign="top">
            </td>
        </tr>
    </table>
</asp:Content>
