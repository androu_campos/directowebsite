using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class RateViewerAA : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            SqlConnection SqlConn1 = new SqlConnection();
            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
            SqlCommand SqlCommand1 = new SqlCommand(" SELECT '**ALL**' AS Customer FROM CDRDB.dbo.ProviderIP Union Select ProviderName As Customer From CDRDB.dbo.ProviderIP Where ProviderName Like 'ALL%' ", SqlConn1);

            //SqlCommand1 = new SqlCommand(cmd, SqlConn1);
            DataSet dataSet = RunQuery(SqlCommand1);

            DataTable dataTable = dataSet.Tables[0];
            dpdCustname.Items.Add("**NONE**");
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                dpdCustname.Items.Add(dataTable.Rows[i][0].ToString());
            }
            
            SqlConn1.Close();

        }
        else
        { 
        
        
        }

        if (Session["Rol"].ToString() == "CallCenter")
        {
            rdbType.Items.RemoveAt(1);
            Label4.Text = "Select Customer";

            cdrdbDatasetTableAdapters.CustBreakoutAdp adpCC = new cdrdbDatasetTableAdapters.CustBreakoutAdp();
            cdrdbDataset.CustBreakoutDataTable tblCC = adpCC.GetCustNameCC();
            int count = tblCC.Rows.Count;
            dpdCustname.Items.Clear();
            for (int i = 0; i < count; i++)
            {
                dpdCustname.Items.Add(tblCC.Rows[i]["CustName"].ToString());
            }

        }
        
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            Session["rdbType"] = rdbType.SelectedValue.ToString();            
            Session["dpdCustname"] = dpdCustname.SelectedValue.ToString();
            Session["dpdRate"] = txtRate.Text;
            if (txtPrefix.Text == string.Empty) 
                Session["txtPrefix"] = "%";
            else
            Session["txtPrefix"] = txtPrefix.Text;

        
                        
            Response.Redirect("Modify.aspx", false);
        
           
        }
        catch(Exception ex)
        {

            string errormessage = ex.ToString();
            Response.Redirect("Home.aspx");
        
        }


    }
    protected void rdbType_SelectedIndexChanged(object sender, EventArgs e)
    {
        /*LLenar el dropdownlist con Customers o Vendors*/

        if (rdbType.SelectedValue.ToString() == "C")//Customers
        {
            cdrdbDatasetTableAdapters.CustBreakoutAdp adpC = new cdrdbDatasetTableAdapters.CustBreakoutAdp();
            cdrdbDataset.CustBreakoutDataTable tblC = adpC.GetCustName();
            int count = tblC.Rows.Count;
            dpdCustname.Items.Clear();
            for (int i = 0; i < count; i++)
            {
                dpdCustname.Items.Add(tblC.Rows[i]["CustName"].ToString());
            }

            Label2.Text = "CustName:";

        }
        else                                        //Vendors
        {
            cdrdbDatasetTableAdapters.VendBreakoutTableAdapter adpV = new cdrdbDatasetTableAdapters.VendBreakoutTableAdapter();
            cdrdbDataset.VendBreakoutDataTable tblV = adpV.GetVendName();            
            int count = tblV.Rows.Count;
            dpdCustname.Items.Clear();
            for (int i = 0; i < count; i++)
            {
                dpdCustname.Items.Add(tblV.Rows[i]["VendorName"].ToString());
            }
          
            Label2.Text = "VendorName:";
           
        }

    }
    

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

}
