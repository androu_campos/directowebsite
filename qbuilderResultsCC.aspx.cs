using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RKLib.ExportData;
using System.Data.SqlClient;

public partial class qbuilderResults : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                string qry = Session["AdQbuilder1"].ToString();

                if (Session["hometype"].ToString() == "R")
                {
                    qry.Replace("FROM OpSheetInc", "FROM ICS.dbo.OpSheetIncR");
                    qry.Replace("FROM OpSheetAll", "FROM ICS.dbo.OpSheetAllR");
                    qry.Replace("FROM OpSheetY", "FROM ICS.dbo.OpSheetYR");
                }



                SqlDataSource1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";
                SqlDataSource1.SelectCommandType = SqlDataSourceCommandType.Text;
                SqlDataSource1.SelectCommand = qry;

                Session["AQB"] = Util.RunQueryByStmnt(qry);
                gdvQbuilder.AllowPaging = true;
                gdvQbuilder.AllowSorting = true;
                gdvQbuilder.PageSize = Convert.ToInt32(Request.QueryString["Pz"]);

                gdvQbuilder.DataSourceID = "SqlDataSource1";
                gdvQbuilder.DataBind();                
                

                /**/
                cmdExport.Visible = true;

                string table = Request.QueryString[0].ToString();
                string query = "select crdate as Modified from [mecca2].[dbo].sysobjects  where type = 'U' and name like '" + table + "'";
                SqlCommand sqlQuery = new SqlCommand();
                sqlQuery.CommandType = System.Data.CommandType.Text;
                sqlQuery.CommandText = query;
                DataSet ResultSet1;
                ResultSet1 = Util.RunQuery(sqlQuery);
                Session["AdvQB"] = ResultSet1;
                Label2.Text = "Last Modified:" + ResultSet1.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                string messageerror = ex.Message.ToString();
                Response.Write(messageerror);
            }
        }
    }
    protected void gdvQbuilder_Sorting(object sender, GridViewSortEventArgs e)
    {
        SqlDataSource1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlDataSource1.SelectCommand = Session["AdQbuilder1"].ToString();
        gdvQbuilder.DataSourceID = "SqlDataSource1";
        gdvQbuilder.DataBind();
    }
    protected void gdvQbuilder_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gdvQbuilder_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
    protected void gdvQbuilder_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    {
        SqlDataSource1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlDataSource1.SelectCommand = Session["AdQbuilder1"].ToString();
        
        gdvQbuilder.PageIndex = e.NewPageIndex;
        
        gdvQbuilder.DataSourceID = "SqlDataSource1";
        gdvQbuilder.DataBind();
    }

    protected void cmdExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {
            DataTable dtEmployee = ((DataSet)Session["AQB"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "AdvanceQueryBuilder" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
}
