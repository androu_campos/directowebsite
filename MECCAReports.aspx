<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="MECCAReports.aspx.cs" Inherits="MECCAReports"
    Title="DIRECTO - Connections Worldwide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="MECCA REPORTS" CssClass="labelTurk"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="cborol" runat="server" OnSelectedIndexChanged="rol_SelectedIndexChanged"
                    Visible="false" AutoPostBack="true">
                    <asp:ListItem Text="WS" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="CC" Value="2"></asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="panelWS" Width="100%">
        <table>
            <tr>
                <td>
                    <table align="left">
                        <tr>
                            <td style="width: 33%">
                                <asp:Label ID="lbl1" runat="server" CssClass="labelBlue8" Text="Unique Report WholeSale"></asp:Label>
                            </td>
                            <td>
                            </td>
                            <td style="width: 33%">
                                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="WholeSale ExcelReports"></asp:Label>
                            </td>
                            <td style="width: 33%">
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:GridView ID="gvUniqueWS" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                    Width="529px" AutoGenerateColumns="true">
                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                        Font-Size="8pt" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="lblContent" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="linkLoss" runat="server" CssClass="labelTurk" Text="LOSS REPORTS"
                        OnClick="linkLoss_Click"></asp:LinkButton>
                    <asp:Panel ID="panelLoss" runat="server" Visible="false">
                        <table>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="LOSS "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvMRLoss" runat="server" Height="150px" Font-Names="Arial" Font-Size="8pt"
                                        Width="400px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="MEXICO LOSS"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvVL_BillingDateLOSS" runat="server" Height="150px" Font-Names="Arial"
                                        Font-Size="8pt" Width="400px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Text="MEXICO LOSS - LAST DAY"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvCV_Loss" runat="server" Height="150px" Font-Names="Arial" Font-Size="8pt"
                                        Width="450px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label9" runat="server" CssClass="labelBlue8" Text="ROW LOSS"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvRC_Loss" runat="server" Height="150px" Font-Names="Arial" Font-Size="8pt"
                                        Width="400px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label10" runat="server" CssClass="labelBlue8" Text="ROW LOSS - LAST DAY"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvCV_CLoss" runat="server" Height="150px" Font-Names="Arial" Font-Size="8pt"
                                        Width="450px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="linkSem" runat="server" CssClass="labelTurk" Text="DAILY TRAFFIC REPORTS"
                        OnClick="linkSem_Click"></asp:LinkButton>
                    <asp:Panel ID="panelSem" runat="server" Visible="false">
                        <table>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label11" runat="server" CssClass="labelBlue8" Text="TRAFFIC "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvTraffic" runat="server" Height="150px" Font-Names="Arial" Font-Size="8pt"
                                        Width="400px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label12" runat="server" CssClass="labelBlue8" Text="PROFIT  "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvProfit" runat="server" Height="150px" Font-Names="Arial" Font-Size="8pt"
                                        Width="400px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label13" runat="server" CssClass="labelBlue8" Text="CUSTOMER TELCEL "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDailyCustTel" runat="server" Height="150px" Font-Names="Arial"
                                        Font-Size="8pt" Width="450px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label14" runat="server" CssClass="labelBlue8" Text="Vendor TELCEL "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDailyVendorTel" runat="server" Height="150px" Font-Names="Arial"
                                        Font-Size="8pt" Width="450px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label15" runat="server" CssClass="labelBlue8" Text="Customer TELMEX "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDailyCustTelmex" runat="server" Height="150px" Font-Names="Arial"
                                        Font-Size="8pt" Width="450px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label16" runat="server" CssClass="labelBlue8" Text="Vendor TELMEX "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDailyVendorTelmex" runat="server" Height="150px" Font-Names="Arial"
                                        Font-Size="8pt" Width="450px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label17" runat="server" CssClass="labelBlue8" Text="Customer IUSA "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDailyCustIUSA" runat="server" Height="150px" Font-Names="Arial"
                                        Font-Size="8pt" Width="450px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label18" runat="server" CssClass="labelBlue8" Text="Vendor IUSA "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDailyVendorIUSA" runat="server" Height="150px" Font-Names="Arial"
                                        Font-Size="8pt" Width="450px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label19" runat="server" CssClass="labelBlue8" Text="Customer MOVISTAR "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDailyCustMOVI" runat="server" Height="150px" Font-Names="Arial"
                                        Font-Size="8pt" Width="450px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label20" runat="server" CssClass="labelBlue8" Text="Vendor MOVISTAR "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDailyVendorMOVI" runat="server" Height="150px" Font-Names="Arial"
                                        Font-Size="8pt" Width="450px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:Label ID="Label21" runat="server" CssClass="labelBlue8" Text="ROW "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDailyCountry" runat="server" Height="150px" Font-Names="Arial"
                                        Font-Size="8pt" Width="450px" AutoGenerateColumns="true">
                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="panelCC" Width="100%">
        <table>
            <tr>
                <td>
                    <table align="left">
                        <tr>
                            <td style="width: 50%">
                                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Unique Report CallCenter"></asp:Label>
                            </td>
                            <td>
                            </td>
                            <td style="width: 50%">
                                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="CallCenter ExcelReports"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvUniqueCC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                    Width="529px" AutoGenerateColumns="true">
                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                        Font-Size="8pt" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:LinkButton ID="linkSemCC" runat="server" CssClass="labelTurk" Text="DAILY TRAFFIC REPORTS"
                                    OnClick="linkSemcc_Click"></asp:LinkButton>
                                <asp:Panel ID="panelSemCC" runat="server" Visible="false">
                                    <table>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Label ID="Label22" runat="server" CssClass="labelBlue8" Text="CallCenter SALES"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvProfitCC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                                    Width="529px" AutoGenerateColumns="true">
                                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                        Font-Size="8pt" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Label ID="Label23" runat="server" CssClass="labelBlue8" Text="CallCenter MINUTES"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvTrafficCC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                                    Width="529px" AutoGenerateColumns="true">
                                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                        Font-Size="8pt" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Label ID="Label24" runat="server" CssClass="labelBlue8" Text="LMC TOTAL TRAFFIC"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvLMC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                                    Width="529px" AutoGenerateColumns="true">
                                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                        Font-Size="8pt" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Label ID="Label25" runat="server" CssClass="labelBlue8" Text="TELCEL CLIENT"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvCTelcelCC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                                    Width="529px" AutoGenerateColumns="true">
                                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                        Font-Size="8pt" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Label ID="Label26" runat="server" CssClass="labelBlue8" Text="TELCEL VENDOR"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvVTelcelCC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                                    Width="529px" AutoGenerateColumns="true">
                                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                        Font-Size="8pt" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Label ID="Label27" runat="server" CssClass="labelBlue8" Text="MOVISTAR CLIENT"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvCMoviCC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                                    Width="529px" AutoGenerateColumns="true">
                                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                        Font-Size="8pt" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Label ID="Label28" runat="server" CssClass="labelBlue8" Text="MOVISTAR VENDOR"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvVMoviCC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                                    Width="529px" AutoGenerateColumns="true">
                                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                        Font-Size="8pt" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Label ID="Label29" runat="server" CssClass="labelBlue8" Text="IUSACELL CLIENT"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvCIusaCC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                                    Width="529px" AutoGenerateColumns="true">
                                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                        Font-Size="8pt" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Label ID="Label30" runat="server" CssClass="labelBlue8" Text="IUSACELL VENDOR"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvVIusaCC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                                    Width="529px" AutoGenerateColumns="true">
                                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                        Font-Size="8pt" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Label ID="Label31" runat="server" CssClass="labelBlue8" Text="TELMEX CLIENT"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvCTelmexCC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                                    Width="529px" AutoGenerateColumns="true">
                                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                        Font-Size="8pt" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Label ID="Label32" runat="server" CssClass="labelBlue8" Text="TELMEX VENDOR"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvVTelmexCC" runat="server" Height="330px" Font-Names="Arial" Font-Size="8pt"
                                                    Width="529px" AutoGenerateColumns="true">
                                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                        Font-Size="8pt" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <%--FOOTER DE MECCA--%>
    <table align="left">
        <tr>
            <td align="right">
                <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                    Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
            <td>
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                    Width="146px" Visible="True" /></td>
        </tr>
    </table>
</asp:Content>
