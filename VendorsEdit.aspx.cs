using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class VendorsEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            cdrdbDatasetTableAdapters.ProviderIPTableAdapter adp = new cdrdbDatasetTableAdapters.ProviderIPTableAdapter();
            cdrdbDataset.ProviderIPDataTable tbl = adp.GetDataByType("V");
            dpdVendor.Items.Clear();
            dpdVendor.Items.Add("");
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                dpdVendor.Items.Add(tbl.Rows[i]["ProviderName"].ToString());

            }
            
            
        
        }
    }
    protected void dpdProvider_SelectedIndexChanged(object sender, EventArgs e)
    {
        string qry = "SELECT * FROM ProviderIP WHERE Type='V' and ProviderName='" +dpdVendor.SelectedValue.ToString() + "'";

        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = System.Data.CommandType.Text;
        sqlSpQuery.CommandText = qry;
        DataSet ResultSetC;
        ResultSetC = RunQuery(sqlSpQuery);
        
        txtIP.Text = ResultSetC.Tables[0].Rows[0]["IP"].ToString();
        txtName.Text = ResultSetC.Tables[0].Rows[0]["ProviderName"].ToString();
        lblUpdated.Visible = false;

    }
    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            string qry = "UPDATE ProviderIP SET ProviderName='" + txtName.Text + "',IP='" + txtIP.Text + "' WHERE ProviderName='" + txtName.Text + "' and Type='V'";
            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = System.Data.CommandType.Text;
            sqlSpQuery.CommandText = qry;
            RunQuery(sqlSpQuery);
            lblUpdated.Visible = true;
        }
        catch (Exception ex)
        {
            string message = ex.Message.ToString();
        }

    }
}
