using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;

public partial class BlackList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DirectoUser"].ToString() == "santander-demo")
        {

        }
        else
        {
            Response.Redirect("HomeCCCUST.aspx", false);
        }

        if (!IsPostBack)
        {

        }
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        sqlsQualifSearch.DataBind();
        gvQualif.Visible = false;
        gvSearch.Visible = true;

        if (gvSearch.Rows.Count == 0)
        {
            lblError.Visible = true;
            lblError.Text = "Not Found!";
        }
        else
        {
            lblError.Visible = false;
        }
    }
    protected void btnExpCSV_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=Qualification.csv");
            Response.ContentType = "application/text";
            StringBuilder strBr = new StringBuilder();

                for (int i = 0; i < gvQualif.Columns.Count; i++)
                {
                    strBr.Append(gvQualif.Columns[i].HeaderText + ',');
                }
                strBr.Append("\n");
                for (int j = 0; j < gvQualif.Rows.Count; j++)
                {
                    for (int k = 0; k < gvQualif.Columns.Count; k++)
                    {
                        strBr.Append(gvQualif.Rows[j].Cells[k].Text + ',');
                    }
                    strBr.Append("\n");
                }
                Response.Write(strBr.ToString());
                Response.Flush();
                Response.End();
        }
        catch (Exception ex) { string message = ex.Message.ToString(); }
    }
}


