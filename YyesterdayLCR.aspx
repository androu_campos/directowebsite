<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="YyesterdayLCR.aspx.cs" Inherits="YyesterdayLCR"
    Title="DIRECTO - Connections Worldwide" %>

<asp:content id="Content1" contentplaceholderid="ContentPlaceHolder1" runat="Server">
    
    <asp:Label ID="Label1" runat="server" CssClass="labelTurkH" Text="Label"></asp:Label>
    <asp:Label ID="Label3" runat="server" CssClass="labelTurkH" Text=""></asp:Label>
    <table style="position:absolute; top:220px; left:230px;">
       
        <tr>
            <td align="left" colspan="2"  valign="bottom">
                <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="Button1_Click"
                    Text="Export to EXCEL" /></td>
        </tr>
        <tr>
            <td colspan="2">
    <asp:GridView ID="gdvYester" runat="server" AutoGenerateColumns="false" AllowPaging="True" PageSize="200" OnPageIndexChanging="gdvYester_PageIndexChanging">
    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />        
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <Columns>
        
        <asp:BoundField DataField="Country" HeaderText="Country">
         <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="160px" />
         <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <asp:BoundField DataField="Type" HeaderText="Type" >
         <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="160px" />
         <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
             
        <asp:BoundField DataField="LMC" HeaderText="LMC" >
         <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="160px" />
         <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <asp:BoundField DataField="Class" HeaderText="Class" >
         <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="160px" />
         <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <asp:BoundField DataField="Region" HeaderText="Region" >
         <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="160px" />
         <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>

        <asp:BoundField DataField="Minutes" HeaderText="Minutes" HtmlEncode="False" DataFormatString="{0:0,0}" >
         <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="160px" />
         <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        
        <asp:BoundField DataField="Completed Calls" HeaderText="Completed Calls" HtmlEncode="False" DataFormatString="{0:0,0}">
         <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="160px" />
         <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        
        <asp:BoundField DataField="Cost" HeaderText="Cost" HtmlEncode="False" DataFormatString="${0:f4}" >
         <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="160px" />
         <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>  

        <asp:BoundField DataField="ASR" HeaderText="ASR" HtmlEncode="False" >
         <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="160px" />
         <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>  
        
        <asp:BoundField DataField="ABR" HeaderText="ABR" HtmlEncode="False" >
         <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="160px" />
         <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>        
        
        <asp:BoundField DataField="ACD" HeaderText="ACD" HtmlEncode="False" >
         <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="160px" />
         <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>                        
        
        </Columns>
    </asp:GridView>
            </td>
        </tr>
        
        
        
        
        <tr>
            <td width="100%" colspan="2">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
</asp:content>