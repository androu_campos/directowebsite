<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="TestGraph.aspx.cs"
    Inherits="TestGraph" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
    
    function dispData(date,calls,graph)
    {
    
    if (document.getElementById)
	{
		document.getElementById('<%=idFrame.ClientID%>').src = "DynamicData.aspx?c=" + calls + "&d=" + date + "&g=" + graph;
	}
	
    }    
    
    </script>

    <table width="1024px" height="768px" style="position: absolute; top: 160px; left: 21%;">
        <tr>
            <td valign="top" width="20%" style="height: 20px">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="Active Calls Graph"
                    Width="103px"></asp:Label></td>
            <td valign="top" width="75%" style="height: 20px">
            </td>
            <td valign="top" width="5%" style="height: 20px">
            </td>
        </tr>
        <tr>
            <td valign="top" width="20%">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 100px">
                                                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" ForeColor="LimeGreen"
                                                    Text="Loading...please wait" Width="104px"></asp:Label></td>
                                            <td style="width: 100px">
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Loading.gif" /></td>
                                        </tr>
                                    </table>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
            </td>
            <td valign="top" width="75%" align="center">
                <iframe src="DynamicData.aspx?c=0&d=0&g=X" runat="server" id="idFrame" frameborder="0"
                    style="width: 177px" height="60"></iframe>
            </td>
            <td valign="top" width="5%">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td align="left" style="height: 65px" valign="top" colspan="2">
                <table align="left" style="width: 177px; height: 120px">
                    <tr>
                        <td align="left" colspan="2" style="height: 43px" valign="top">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
<TABLE><TBODY><TR><TD style="WIDTH: 100px" vAlign=top><cc1:TabContainer id="TabContainer1" runat="server" Width="177px" Height="310px" ActiveTabIndex="1">
                    <cc1:TabPanel ID="tblQuery" runat="server">
                        <HeaderTemplate>
                            Query Builder
                        </HeaderTemplate>
                        <ContentTemplate>
                            <cc1:CalendarExtender runat="server" TargetControlID="txtFrom" BehaviorID="TabContainer1_tblQuery_CalendarExtender1"
                                Enabled="True" Format="yyyy/MM/dd" PopupButtonID="imgFrom" ID="CalendarExtender1">
                            </cc1:CalendarExtender>
                            <cc1:CalendarExtender runat="server" TargetControlID="txtTo" BehaviorID="TabContainer1_tblQuery_CalendarExtender2"
                                Enabled="True" Format="yyyy/MM/dd" PopupButtonID="imgTo" ID="CalendarExtender2">
                            </cc1:CalendarExtender>
                            <table id="tblHorizontal">
                                <tbody>
                                    <tr>
                                    <td height="15px"></td>
                                    <td></td>
                                    
                                    </tr>
                                    <tr>
                                        <td style="height: 19px" valign="top" align="center" bgcolor="#d5e3f0">
                                            <asp:Label ID="lblCustomer" runat="server" Text="Customer:" CssClass="labelBlue8"></asp:Label></td>
                                        <td style="height: 19px" valign="top">
                                            <asp:DropDownList ID="dpdCustomer" runat="server" Font-Size="6pt" Width="101px" CssClass="dropdown"
                                               DataTextField="ProviderName" DataValueField="ProviderName">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px" valign="top" align="center" bgcolor="#d5e3f0">
                                            <asp:Label ID="lblVendor" runat="server" Text="Vendor:" CssClass="labelBlue8"></asp:Label></td>
                                        <td style="height: 19px" valign="top">
                                            <asp:DropDownList ID="dpdVendor" runat="server" Font-Size="6pt" Width="101px" CssClass="dropdown"
                                                 DataTextField="ProviderName" DataValueField="ProviderName">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px" valign="top" align="center" bgcolor="#d5e3f0">
                                            <asp:Label ID="lblCountry" runat="server" Text="Country:" CssClass="labelBlue8"></asp:Label></td>
                                        <td style="height: 19px" valign="top">
                                            <asp:DropDownList ID="dpdCountry" runat="server" Font-Size="6pt" Width="101px" CssClass="dropdown"
                                                DataTextField="Country" DataValueField="Country">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px" valign="top" align="center" bgcolor="#d5e3f0">
                                            <asp:Label ID="lblRegion" runat="server" Text="Region:" CssClass="labelBlue8"></asp:Label></td>
                                        <td style="height: 19px" valign="top">
                                            <asp:DropDownList ID="dpdRegion" runat="server" Font-Size="6pt" Width="101px" CssClass="dropdown"
                                                DataTextField="Region" DataValueField="Region">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px" valign="top" align="center" bgcolor="#d5e3f0">
                                            <asp:Label ID="lblType" runat="server" Text="Type:" CssClass="labelBlue8"></asp:Label></td>
                                        <td style="height: 19px" valign="top">
                                            <asp:DropDownList ID="dpdOrType" runat="server" Font-Size="7pt" Width="39px" CssClass="dropdown">
                                                <asp:ListItem Value="2">==</asp:ListItem>
                                                <asp:ListItem Value="1">&lt;&gt;</asp:ListItem>
                                            </asp:DropDownList><asp:DropDownList ID="dpdType" runat="server" Font-Size="6pt"
                                                Width="62px" CssClass="dropdown"  DataTextField="Type"
                                                DataValueField="Type">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 24px" valign="top" align="center" bgcolor="#d5e3f0">
                                            <asp:Label ID="lblClass" runat="server" Text="Class:" CssClass="labelBlue8"></asp:Label></td>
                                        <td style="height: 24px" valign="top">
                                            <asp:DropDownList ID="dpdOrClass" runat="server" Font-Size="7pt" Width="39px" CssClass="dropdown">
                                                <asp:ListItem Value="2">==</asp:ListItem>
                                                <asp:ListItem Value="1">&lt;&gt;</asp:ListItem>
                                            </asp:DropDownList><asp:DropDownList ID="dpdClass" runat="server" Font-Size="6pt"
                                                Width="62px" CssClass="dropdown"  DataTextField="Class"
                                                DataValueField="Class">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 24px" valign="top" align="center" bgcolor="#d5e3f0">
                                            <asp:Label ID="lblFrom" runat="server" Text="From:" CssClass="labelBlue8"></asp:Label></td>
                                        <td style="height: 24px" valign="top">
                                            <asp:TextBox ID="txtFrom" runat="server" Width="56px" CssClass="dropdown"></asp:TextBox>
                                            <asp:Image ID="imgFrom" runat="server" ImageUrl="~/Images/calendar1.gif"></asp:Image></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 24px" valign="top" align="center" bgcolor="#d5e3f0">
                                            <asp:Label ID="Label3" runat="server" Text="To:" CssClass="labelBlue8"></asp:Label></td>
                                        <td style="height: 24px" valign="top">
                                            <asp:TextBox ID="txtTo" runat="server" Width="56px" CssClass="dropdown"></asp:TextBox>
                                            <asp:Image ID="imgTo" runat="server" ImageUrl="~/Images/calendar1.gif"></asp:Image></td>
                                    </tr>
                                    <tr>
                                    <td colspan="2" align="left" height="15px"> </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            <asp:Button ID="cmdGraph" OnClick="cmdGraph_Click" runat="server" Text="View Graph"
                                                Font-Size="8pt" Width="74px" Height="17px" CssClass="boton"></asp:Button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="productsTab" runat="server" TabIndex="1" HeaderText="Products">
                        <HeaderTemplate>
                            <asp:Label runat="server" Text="Products" ID="Label4"></asp:Label>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <cc1:CalendarExtender runat="server" TargetControlID="txtFrom2" BehaviorID="TabContainer1_productsTab_CalendarExtender3"
                                Enabled="True" PopupButtonID="imgFrom2" ID="CalendarExtender3">
                            </cc1:CalendarExtender>
                            <cc1:CalendarExtender runat="server" TargetControlID="txtTo2" BehaviorID="TabContainer1_productsTab_CalendarExtender4"
                                Enabled="True" PopupButtonID="imgTo2" ID="CalendarExtender4">
                            </cc1:CalendarExtender>
                            <table align="left">
                                <tbody>
                                    <tr>
                                        <td valign="top" align="left">
                                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" Font-Size="7pt" CssClass="dropdown"
                                                RepeatDirection="Vertical" AppendDataBoundItems="True">
                                                <asp:ListItem Value="0">Mexico CPP</asp:ListItem>
                                                <asp:ListItem Value="1">Mexico NEA</asp:ListItem>
                                                <asp:ListItem Value="2">Mexico On Net</asp:ListItem>
                                                <asp:ListItem Value="3">Philippines Globe</asp:ListItem>
                                                <asp:ListItem Value="4">Philippines Smart</asp:ListItem>
                                                <asp:ListItem Value="5">Philippines PLDT</asp:ListItem>
                                                <asp:ListItem Value="6">Brazil Fijo</asp:ListItem>
                                                <asp:ListItem Value="7">Brazil Mobile</asp:ListItem>
                                                <asp:ListItem Value="8">Peru ROC</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td align="left">
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                        <table align="left">
                                            <tbody>
                                                <tr>
                                                    <td align="left" width="20">
                                                        <asp:Label ID="lblFrom2" runat="server" Text="From:" CssClass="labelBlue8"></asp:Label>
                                                    </td>
                                                    <td align="left" width="20">
                                                        <asp:TextBox ID="txtFrom2" runat="server" Width="60px" Height="15px" CssClass="dropdown"></asp:TextBox>
                                                    </td>
                                                    <td align="left" width="20">
                                                        <asp:Image ID="imgFrom2" runat="server" ImageUrl="NeoIma/calendar1.gif"></asp:Image></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20">
                                                        <asp:Label ID="lblTo2" runat="server" Text=" To:" CssClass="labelBlue8"></asp:Label></td>
                                                    <td align="left" width="20">
                                                        <asp:TextBox ID="txtTo2" runat="server" Width="60px" Height="15px" CssClass="dropdown"></asp:TextBox></td>
                                                    <td align="left" width="20">
                                                        <asp:Image ID="imgTo2" runat="server" ImageUrl="NeoIma/calendar1.gif"></asp:Image></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Button ID="Button1" OnClick="Button1_Click" Font-Size="7pt" runat="server" Text="View Graph"
                                                CssClass="boton"></asp:Button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer> 
                <asp:Button id="cmdReset" onclick="cmdReset_Click" runat="server" Text="reset" Width="46px" CssClass="boton"></asp:Button> <TABLE accessKey="tblMaxpoint" id="tblMaxpoint" width=177 border=1 runat="server"><TBODY><TR><TD align=center bgColor=#d5e3f0 colSpan=2><asp:Label id="lblYmax" runat="server" Text="MAX" CssClass="labelBlue8"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblValue" runat="server" Text="Label" ForeColor="Black" Font-Size="7pt" Font-Bold="True" Width="100%" CssClass="labelBlue8"></asp:Label><BR /><asp:Label id="lblMaxPorts" runat="server" Text="Label" ForeColor="Black" Font-Size="7pt" Font-Bold="True" Width="100%" CssClass="labelBlue8" __designer:wfdid="w1"></asp:Label></TD></TR></TBODY></TABLE></TD><TD style="WIDTH: 102px" vAlign=top rowSpan=2>
                <DCWC:Chart id="Chart1" runat="server" Width="905px" Height="445px" __designer:wfdid="w5" Palette="Pastel"><Legends>
<DCWC:Legend Name="Default"></DCWC:Legend>
</Legends>
<Series>
<DCWC:Series BorderColor="Transparent" ShadowOffset="1" XValueType="DateTime" Name="Ports" Color="252, 255, 165, 0"></DCWC:Series>
<DCWC:Series BorderStyle="Dot" XValueType="DateTime" Name="MaxPorts" ChartType="Line" Color="252, 255, 165, 0"></DCWC:Series>
<DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Calls" ChartType="Area" Color="165, 255, 69, 0"></DCWC:Series>
<DCWC:Series BorderStyle="Dot" XValueType="DateTime" Name="MaxCalls" ChartType="Line" Color="Red"></DCWC:Series>
</Series>

<BorderSkin PageColor="AliceBlue"></BorderSkin>
<ChartAreas>
<DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft" Name="Default">
<AxisX Title="DateTime" Margin="False">
<LabelStyle Format="g"></LabelStyle>
</AxisX>

<AxisY Title="Calls">
<LabelStyle Format="N0"></LabelStyle>
</AxisY>
</DCWC:ChartArea>
</ChartAreas>
<Titles>
<DCWC:Title Alignment="TopLeft" Font="Arial, 8.25pt, style=Bold" Color="245, 0, 0, 0" Name="Title1"></DCWC:Title>
</Titles>
</DCWC:Chart><BR /><asp:Label id="lblGraphs" runat="server" Text="Label" Visible="False" CssClass="labelBlue8"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px; HEIGHT: 62px" vAlign=top></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="cmdReset" />
                                </Triggers>
                            </asp:UpdatePanel>
                            
                            </td>
                    </tr>
                    <tr>
                        <td align="left" width="30%">
                            <asp:Button ID="btn_excel" runat="server" CssClass="boton" OnClick="btn_excel_Click"
                                Text="Export To EXCEL" Width="83px" /></td>
                        <td align="left" width="30%">
                        </td>
                    </tr>
                </table>
                            
            </td>
            <td style="height: 65px" valign="top" width="5%">
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="1" width="20%">
            </td>
            <td colspan="2" width="80%">
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
