<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="LtwGrouped.aspx.cs" Inherits="LtwGrouped" Title="Untitled Page" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                 <td  rowspan = "4" valign="top">
                     <asp:GridView ID="Customers" runat="server" Height="33px" 
                        Font-Names="Arial" Font-Size="8pt" Width="100px" AutoGenerateColumns="false">
                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                            Font-Size="8pt" />
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="Customer" HeaderText="Customer" HtmlEncode="False"  >
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            
                            <asp:BoundField DataField="Total" HeaderText="Total" HtmlEncode="False"  >
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>                            
                        </Columns>
                        
                        
                        
                    </asp:GridView>
                </td>
                <td valign = "top">
                     <asp:GridView ID="Outlander" runat="server" Height="33px"
                        Font-Names="Arial" Font-Size="8pt" Width="100px" AutoGenerateColumns="false">
                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                            Font-Size="8pt" />
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="Outlander" HeaderText="Outlander" HtmlEncode="False"  >
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            
                            <asp:BoundField DataField="Total" HeaderText="Total" HtmlEncode="False"  >
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>                            
                        </Columns>
                    </asp:GridView>
                    
                     <asp:GridView ID="Vendors" runat="server" Height="33px" 
                        Font-Names="Arial" Font-Size="8pt" Width="100px" AutoGenerateColumns="false">
                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                            Font-Size="8pt" />
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="E Vendor" HeaderText="E Vendor" HtmlEncode="False"  >
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            
                            <asp:BoundField DataField="Total" HeaderText="Total" HtmlEncode="False"  >
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>                            
                        </Columns>
                        
                    </asp:GridView>
                </td>   
            <%--<tr>
                <td>
                    <asp:GridView ID="National" runat="server" Height="33px" 
                        Font-Names="Arial" Font-Size="8pt" Width="100px" AutoGenerateColumns="false">
                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                            Font-Size="8pt" />
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="National" HeaderText="National" HtmlEncode="False"  >
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left"  />
                            <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            
                            <asp:BoundField DataField="Total" HeaderText="Total" HtmlEncode="False"  >
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>                            
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>--%>
                <td>
                   
                </td>
                <td style="width: 5px">
                </td>
            </tr>
            <%--<tr>
                <td>
                    &nbsp;<asp:GridView ID="GK" runat="server" Height="33px" 
                        Font-Names="Arial" Font-Size="8pt" Width="100px" AutoGenerateColumns="true">
                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                            Font-Size="8pt" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </td>
                <td style="width: 5px">
                    &nbsp;</td>
            </tr>--%>
        </table>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <asp:Timer ID="Timer1" runat="server" Interval="900000">
        </asp:Timer>
</asp:Content>

