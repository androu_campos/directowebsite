using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class TopStats : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string s = "";
        string sql = "";
        string sqlCustomers = "";
        string sqlCheckPoints = "";
        string sqlSummary = "";
        string customer = "";
        string currentHour = "";
        string range = "";
        string currentDate = "";
        string user = "";
       
        int rows = 0;
        int rowCostumers = 0;
        DataSet dts = null;
        DataSet dtsCustomers = null;
        DataSet dtsSummary = null;
        DataSet dtsCheckP = null;
        int start = 0;
        int stop = 0;
        int id = 0;
        string dateRangeStart = "";
        string dateRangeStop = "";
        string dateRangeStart2 = "";
        string dateRangeStop2 = "";
        string datetime = "";
        int bandera = 0;
        string dt = "";
        
        
        dt=DateTime.Now.ToString();


        if (!Page.IsPostBack)
        {
            user = Session["DirectoUser"].ToString();
            sql = "INSERT INTO mecca2..LoginTopCustVendors VALUES (";
            sql += "'" + user + "','" + dt + "'," + "'Top Customers')";
            try
            {
                Util.RunQueryByStmnt(sql);
            }catch(Exception exception){
                exception.ToString();
            }

        }



        currentHour = DateTime.Now.Hour.ToString();
        currentDate = DateTime.Now.ToShortDateString();

        dateRangeStart = currentDate + " ";
        dateRangeStop = currentDate + " ";
        //currentHour=currentHour.Substring(0, 2);


        s = "<table ID=\"Table1\" runat=\"server\" border=\"1\">";

        

        sqlCustomers = "SELECT NAME FROM TopMonitorConfig WHERE TYPE = 'C' ORDER BY NAME";    
        dtsCustomers = (DataSet)Util.RunQueryByStmnt(sqlCustomers);

        sqlCheckPoints = "SELECT ID,START,STOP FROM TopMonCheckpoints WHERE ";
        sqlCheckPoints = sqlCheckPoints + currentHour + " BETWEEN START AND STOP ";

        dtsCheckP = (DataSet)Util.RunQueryByStmnt(sqlCheckPoints);

        if (dtsCheckP.Tables[0].Rows.Count > 0)
        {
            range = "'"+dtsCheckP.Tables[0].Rows[0][1]+":00 - ";
            range = range + dtsCheckP.Tables[0].Rows[0][2]+":00'";

            dateRangeStart += dtsCheckP.Tables[0].Rows[0][1] + ":00";
            dateRangeStop += dtsCheckP.Tables[0].Rows[0][2] + ":00";
            start = (int) dtsCheckP.Tables[0].Rows[0][1];


            id = (int)dtsCheckP.Tables[0].Rows[0][0];

            dtsCheckP.Dispose();

            if ((Convert.ToInt16(currentHour) - start) == 2 )
            {
                bandera = 1;

                if (id == 1)
                    sqlCheckPoints = "SELECT START,STOP FROM TopMonCheckpoints WHERE id = (SELECT MAX(ID) FROM TopMonCheckpoints)";
                else
                    sqlCheckPoints = "SELECT START,STOP FROM TopMonCheckpoints WHERE id = " + (id - 1);

                dtsCheckP = (DataSet)Util.RunQueryByStmnt(sqlCheckPoints);

                start = (int)dtsCheckP.Tables[0].Rows[0][0];
                stop = (int)dtsCheckP.Tables[0].Rows[0][1];
            }

            if (bandera == 1 && (start > stop))
            {
                dateRangeStart2 = DateTime.Now.AddDays(-1).ToShortDateString() + " " + start + ":00";
                dateRangeStop2 = DateTime.Now.ToShortDateString() + " " + stop + ":00";
            }
            else
            {
                dateRangeStart2 = DateTime.Now.ToShortDateString() + " " + start + ":00";
                dateRangeStop2 = DateTime.Now.ToShortDateString() + " " + stop + ":00";
            }
             
        }


        
        if (dtsCustomers.Tables[0].Rows.Count > 0)
        {
            rowCostumers = dtsCustomers.Tables[0].Rows.Count;
            Label1.Text = "TOP " + rowCostumers + " CUSTOMERS STATS";

            for (int i = 0; i < rowCostumers; i++)
            {
                customer = "";
                customer = "'"+dtsCustomers.Tables[0].Rows[i][0]+"'";
                datetime = "";

                sqlSummary = "SELECT CONVERT(varchar,GETDATE(),101)+' '+"+ range +"as Time,";
                sqlSummary = sqlSummary + "CUSTOMER , TYPE, CASE WHEN TYPE = 'CPP' THEN '' ELSE CLASS END AS CLASS,sum(isnull(minutes,0)) as TotalMinutes, sum(Attempts) as Attempts,";
                sqlSummary = sqlSummary + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected,";
                sqlSummary = sqlSummary + "mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,";
                sqlSummary = sqlSummary + "mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes),";
                sqlSummary = sqlSummary + "SUM(Calls)) as ACD ";
                sqlSummary = sqlSummary + "FROM opLiveMonitor ";
                sqlSummary = sqlSummary + "WHERE CUSTOMER = "+customer+" AND TIME BETWEEN '" + dateRangeStart +"' AND '" + dateRangeStop+"' AND (TYPE = 'FIXED' OR TYPE='CPP') ";
                sqlSummary = sqlSummary + "GROUP by CUSTOMER,TYPE, CASE WHEN TYPE = 'CPP' THEN '' ELSE CLASS END ";
                sqlSummary = sqlSummary + "Order by CUSTOMER, TYPE";

                dtsSummary = (DataSet)Util.RunQueryByStmnt(sqlSummary);

                s = s + "<tr><td colspan=\"11\" bgcolor=\"99CCFF\"><b>" + dtsCustomers.Tables[0].Rows[i][0]+"</b></td></tr>";
                s = s + "<tr><td width=\"500\"><b>DateTime</b></td>";
                s = s + "<td width=\"500\"><b>Customer</b></td>";
                s = s + "<td width=\"500\"><b>Type</b></td>";
                s = s + "<td width=\"500\"><b>Class</b></td>";
                s = s + "<td width=\"500\"><b>TotalMinutes</b></td>";
                s = s + "<td width=\"500\"><b>Attempts</b></td>";
                s = s + "<td width=\"500\"><b>AnsweredCalls</b></td>";
                s = s + "<td width=\"500\"><b>Rejected</b></td>";
                s = s + "<td width=\"500\"><b>ASR</b></td>";
                s = s + "<td width=\"500\"><b>ABR</b></td>";
                s = s + "<td width=\"500\"><b>ACD</td></b></tr>";

                if (dtsSummary.Tables[0].Rows.Count > 0)
                {
                    for (int j = 0; j < dtsSummary.Tables[0].Rows.Count; j++)
                    {
                        s = s + "<tr>";
                        for (int k = 0; k < 11; k++)
                        {
                            s = s + "<td width=\"500\">" + dtsSummary.Tables[0].Rows[j][k].ToString() + "</td>";
                        }
                        s = s + "</tr>";
                    }
                }
                s = s + "<tr><td colspan=\"11\" bgcolor=\"CCCCC\">&nbsp</td></tr>";
                dtsSummary.Dispose();

                
                sql = "Select convert(varchar,time,101)+' '+left(convert(varchar,time,114),2)+':00' as Time,";
                sql = sql + "CUSTOMER , TYPE, CASE WHEN TYPE = 'CPP' THEN '' ELSE CLASS END AS CLASS,sum(isnull(minutes,0)) as TotalMinutes, sum(Attempts) as Attempts,";
                sql = sql + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected, mecca2.dbo.fGetASR(SUM(Attempts),";
                sql = sql + "SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,";
                sql = sql + "mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) as ACD ";
                sql = sql + "FROM opLiveMonitor ";
                sql = sql + "WHERE CUSTOMER = " + customer + " AND TIME BETWEEN '" + dateRangeStart + "' AND '" + dateRangeStop + "' AND (TYPE = 'FIXED' OR TYPE='CPP') ";
                sql = sql + "GROUP by CUSTOMER,TYPE,CASE WHEN TYPE = 'CPP' THEN '' ELSE CLASS END,convert(varchar,time,101)+' '+left(convert(varchar,time,114),2)+':00' ";
                sql = sql + "Order by Time, CUSTOMER, TYPE";

                dts = (DataSet)Util.RunQueryByStmnt(sql);

                if (dts.Tables[0].Rows.Count > 0)
                {
                    rows = dts.Tables[0].Rows.Count;

                    
                    for (int l = 0; l < rows; l++)
                    {
                        if (dts.Tables[0].Rows[l][0].ToString() != datetime && datetime != "")
                            s = s + "<tr><td colspan=\"11\" bgcolor=\"FFCC00\">&nbsp</td></tr>";

                        s = s + "<tr>";
                        for (int m = 0; m < 11; m++)
                        {
                            s = s + "<td width=\"500\">" + dts.Tables[0].Rows[l][m].ToString() + "</td>";
                        }
                        s = s + "</tr>";

                        datetime = dts.Tables[0].Rows[l][0].ToString();
                    }                    
                }

                s = s + "<tr><td colspan=\"11\">&nbsp</td></tr>";
                dts.Dispose();
            }

            s = s + "</table>";
        }

        this.stats.Text = s;

    }
}
