using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Office.Interop;
using System.Data.SqlClient;
using ADODB;
using System.Net.Mail;
using System.Net;
using System.Diagnostics;
using System.Net.Mime;
using System.Globalization;

public partial class WSReportsBuilder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //string from = Request.QueryString["from"].ToString();
        //string to = Request.QueryString["to"].ToString();            
        createExcelFile("","");
    }

    protected void createExcelFile(string from,string to)
    {
        object missValue = System.Reflection.Missing.Value;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        char weekLimitData;
        char weekLimitDataB;
        char weekLimitDataC;
        char weekLimitDataD;
        char weekLimitDataE;
        char weekLimitDataF;
        char weekLimitDataG;
        char weekLimitDataH;
        char weekLimitDataK;
        char weekLimitDataFormulas;
        int endColumn = 0;
        int endColumnB = 0;
        int endColumnC = 0;
        int endColumnD = 0;
        int endColumnK = 0;
        int endColumnFormulas = 0;


        Microsoft.Office.Interop.Excel.Application xlApp = default(Microsoft.Office.Interop.Excel.Application);
        Microsoft.Office.Interop.Excel.Workbook xlWorkBook = default(Microsoft.Office.Interop.Excel.Workbook);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetA = default(Microsoft.Office.Interop.Excel.Worksheet);
        
        CultureInfo ciCurr = CultureInfo.CurrentCulture;
        int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

        String sToday = DateTime.Now.ToString("MM.dd.yyyy.HH.mm");
        String sPathFiles = "E:\\Mecca\\Reports\\";
        String sStandardFile = sPathFiles + "CallMexCustReport.xls";

        String connectionString = ConfigurationManager.ConnectionStrings["A2BillConnectionString"].ConnectionString;

        xlApp = new Microsoft.Office.Interop.Excel.ApplicationClass();

        ADODB.Connection DBConnection = new ADODB.Connection();
        ADODB.Recordset rsDataA = new ADODB.Recordset();
        //ADODB.Recordset rsDataB = new ADODB.Recordset();
        //ADODB.Recordset rsDataC = new ADODB.Recordset();
        //ADODB.Recordset rsDataD = new ADODB.Recordset();

        DBConnection.Open("Provider='MySQLProv';" + connectionString, "", "", -1);
        DBConnection.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
        DBConnection.CommandTimeout = 300;

        rsDataA.Open("Select str_to_date(starttime, '%Y-%m-%d') date,  substring(src, 2, 11) anum, calledstation destination, sessiontime total_seconds , sec_to_time( sessiontime) total_minutes, operator, cc_prefix.destination location from cc_call inner join cc_prefix on cc_prefix.prefix = cc_call.destination where sessiontime > 0", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);       
        //rsDataB.Open("EXEC Mecca2.dbo.sp_WSReportWeeklyProductResults WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        //rsDataC.Open("EXEC Mecca2.dbo.sp_WSReportMonthlyResults WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        //rsDataD.Open("EXEC Mecca2.dbo.sp_WSReportMonthlyProductResults WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        
        xlWorkBook = xlApp.Workbooks.Open(sStandardFile, 0, false, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", false, false, 0, false, false, 0);
        xlWorkSheetA = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        
        Microsoft.Office.Interop.Excel.Range tblRangeA = xlWorkSheetA.get_Range("A2", "A2");
 
        int iDataA = tblRangeA.CopyFromRecordset(rsDataA, missValue, missValue);

        int headersCountA = rsDataA.Fields.Count;

        endColumn = 65 + headersCountA - 1;

        weekLimitData = Convert.ToChar(endColumn);

        weekLimitDataFormulas = Convert.ToChar(endColumnFormulas);


        Microsoft.Office.Interop.Excel.Range tblRangeE = xlWorkSheetA.get_Range("A"+(3 + iDataA), (weekLimitDataE.ToString()) + (3 + iDataA));


        Microsoft.Office.Interop.Excel.Range tblRangeF = xlWorkSheetA.get_Range("A" + (3 + iDataB + 49), (weekLimitDataF.ToString()) + (3 + iDataB + 49));
        int iDataF = tblRangeF.CopyFromRecordset(rsDataF, missValue, missValue);

        xlApp.get_Range("A2", sb.ToString() + weekLimitData.ToString() + "2").Copy(missValue);

        Microsoft.Office.Interop.Excel.Range tblDataA = xlWorkSheetA.get_Range("A2", weekLimitData.ToString() + (1 + iDataA));

        tblDataA.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);

        xlWorkSheetA.get_Range("A1", weekLimitData.ToString() + "1").Font.Bold = true;
        xlWorkSheetA.get_Range("A50", weekLimitDataB.ToString() + "50").Font.Bold = true;        

        xlWorkSheetA.get_Range("A" + (iDataA + 2), weekLimitData.ToString() + (iDataA + 5)).Font.Bold = true;
        xlWorkSheetA.get_Range("A" + (49 + iDataB + 2), weekLimitData.ToString() + (49 + iDataB + 3)).Font.Bold = true;


        xlWorkSheetA.get_Range("A1", weekLimitData.ToString() + "1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheetA.get_Range("A1", weekLimitData.ToString() + "1").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

        xlWorkSheetA.get_Range("A50", weekLimitDataB.ToString() + "50").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheetA.get_Range("A50", weekLimitDataB.ToString() + "50").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

        xlWorkSheetA.get_Range("B2", weekLimitData.ToString() + (5 + iDataA)).NumberFormat = "#,##0;[Red]-#,##0";
        xlWorkSheetA.get_Range("B51", weekLimitDataB.ToString() + (2 + iDataB + 50)).NumberFormat = "#,##0;[Red]-#,##0";
        
        /************* Total Profit *********************/

        char n = 'B';

        xlWorkSheetA.get_Range("A" + (2 + iDataA), "A" + (2 + iDataA)).Value2 = "Total Profit";
        xlWorkSheetA.get_Range("A" + (1 + iDataB + 50), "A" + (1 + iDataB + 50)).Value2 = "Total Profit";

        for (int y = 0; y < headersCountA - 3 ; y++)
        {
            xlWorkSheetA.get_Range(n.ToString() + (2 + iDataA), n.ToString() + (2 + iDataA)).Formula = "=SUBTOTAL(109,"+n.ToString()+"2:"+n.ToString() + (1 + iDataA) + ")";
            xlWorkSheetA.get_Range(n.ToString() + (3 + iDataA), n.ToString() + (3 + iDataA)).NumberFormat = "0.0%_);[Red](0.0%)";

            xlWorkSheetA.get_Range(n.ToString() + (1 + iDataB + 50), n.ToString() + (1 + iDataB + 50)).Formula = "=SUBTOTAL(109," + n.ToString() + "51:" + n.ToString() + (iDataB + 50) + ")";
            xlWorkSheetA.get_Range(n.ToString() + (3 + iDataB + 49), n.ToString() + (3 + iDataB + 49)).NumberFormat = "0.0%_);[Red](0.0%)";
            n++;
        }

        //n = 'B';

        //xlWorkSheetB.get_Range("A" + (2 + iDataC), "A" + (2 + iDataC)).Value2 = "Total Profit";
        //xlWorkSheetB.get_Range("A" + (1 + iDataD+ 50), "A" + (1 + iDataD + 50)).Value2 = "Total Profit";

        //for (int y = 0; y < headersCountC - 1; y++)
        //{
        //    if ((y%2) == 0 || y == 0)
        //    {
        //        xlWorkSheetB.get_Range(n.ToString() + (2 + iDataC), n.ToString() + (2 + iDataC)).Formula = "=SUBTOTAL(109," + n.ToString() + "2:" + n.ToString() + (1 + iDataC) + ")";
        //        xlWorkSheetB.get_Range(n.ToString() + (3 + iDataC), n.ToString() + (3 + iDataC)).NumberFormat = "0.0%_);[Red](0.0%)";

        //        xlWorkSheetB.get_Range(n.ToString() + (2 + iDataD + 49), n.ToString() + (2 + iDataD + 49)).Formula = "=SUBTOTAL(109," + n.ToString() + "51:" + n.ToString() + (1 + iDataD + 49) + ")";
        //        xlWorkSheetB.get_Range(n.ToString() + (3 + iDataD + 49), n.ToString() + (3 + iDataD + 49)).NumberFormat = "0.0%_);[Red](0.0%)";
        //    }            
        //    n++;
        //}
        //n = 'B';



        for (int x = 0; x < iDataA + 4 ; x++)
        {
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString(x + 2) + "-" + Convert.ToChar(endColumn - 5).ToString() + Convert.ToString(x + 2);
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2) + "/" + Convert.ToChar(endColumn - 5).ToString() + Convert.ToString(x + 2);


            xlWorkSheetA.get_Range(Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 1).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 1).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            
        }

        for (int x = 0; x < iDataA + 1; x++)
        {
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString(x + 2) + "/" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString((2 + iDataA));
        }

        for (int x = 49; x < (iDataB + 51); x++)
        {
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString(x + 2) + "-" + Convert.ToChar(endColumn - 5).ToString() + Convert.ToString(x + 2);
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2) + "/" + Convert.ToChar(endColumn - 5).ToString() + Convert.ToString(x + 2);

            xlWorkSheetA.get_Range(Convert.ToChar(endColumnB).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumnB).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            xlWorkSheetA.get_Range(Convert.ToChar(endColumnB - 1).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumnB - 1).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            xlWorkSheetA.get_Range(Convert.ToChar(endColumnB - 2).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumnB - 2).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";

        }

        for (int x = 49; x < iDataB + 50; x++)
        {
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString(x + 2) + "/" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString((2 + iDataA));
        }
        
        string[] hnamesa = new string[headersCountA];
        string[] hnamesb = new string[headersCountB];
        string[] hnamesc = new string[headersCountC];
        string[] hnamesd = new string[headersCountD];

        int i = 0;
        int h = 0;

        foreach (ADODB.Field adoField in rsDataA.Fields)
        {
            hnamesa[i] = adoField.Name;
            i++;
        }

        foreach (ADODB.Field adoField in rsDataB.Fields)
        {
            hnamesb[h] = adoField.Name;
            h++;
        }

        i = 0;
        h = 0;

        foreach (ADODB.Field adoField in rsDataC.Fields)
        {
            hnamesc[i] = adoField.Name;
            i++;
        }

        foreach (ADODB.Field adoField in rsDataD.Fields)
        {
            hnamesd[h] = adoField.Name;
            h++;
        }

        i = 0;
        //h = 0;

        foreach (ADODB.Field adoField in rsDataK.Fields)
        {
            hnamesk[i] = adoField.Name;
            i++;
        }

        xlWorkSheetA.get_Range("A1", weekLimitData.ToString() + "1").Value2 = hnamesa;
        xlWorkSheetA.get_Range("A50", weekLimitDataB.ToString() + "50").Value2 = hnamesb;


        Microsoft.Office.Interop.Excel.Range chartRange;
        Microsoft.Office.Interop.Excel.ChartObjects xlCharts = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChart = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(10, 210, 450, 250);
        
        Microsoft.Office.Interop.Excel.Chart chartPage = myChart.Chart;


        chartRange = xlWorkSheetA.get_Range("A1", weekLimitDataE.ToString() + (1 + iDataA));
        chartPage.SetSourceData(chartRange, Excel.XlRowCol.xlRows);
        chartPage.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        chartPage.ApplyLayout(3, Microsoft.Office.Interop.Excel.XlChartType.xlLine);
        chartPage.ChartTitle.Text = "Profit Per AM Per Week";    

        
        Microsoft.Office.Interop.Excel.ChartObjects xlChartsA2 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartA2 = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(470, 210, 450, 250);

        Microsoft.Office.Interop.Excel.Chart chartPageA2 = myChartA2.Chart;


        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollection = (Microsoft.Office.Interop.Excel.SeriesCollection)myChartA2.Chart.SeriesCollection(missValue);
        Microsoft.Office.Interop.Excel.Series seriesA21 = seriesCollection.NewSeries();
        Microsoft.Office.Interop.Excel.Series seriesA22 = seriesCollection.NewSeries();
        Microsoft.Office.Interop.Excel.Series seriesA23 = seriesCollection.NewSeries();

        Microsoft.Office.Interop.Excel.Range seriesA21_range = xlWorkSheetA.get_Range("B" + (iDataA + 2),weekLimitDataE.ToString() + (iDataA + 2));
        Microsoft.Office.Interop.Excel.Range seriesA22_range = xlWorkSheetA.get_Range("B" + (iDataA + 4),weekLimitDataE.ToString() + (iDataA + 4));
        Microsoft.Office.Interop.Excel.Range seriesA23_range = xlWorkSheetA.get_Range("B" + (iDataA + 5), weekLimitDataE.ToString() + (iDataA + 5)); 

        seriesA21.XValues = xlWorkSheetA.get_Range("B1",  weekLimitDataE.ToString() + "1");
        seriesA22.XValues = xlWorkSheetA.get_Range("B1",  weekLimitDataE.ToString() + "1");
        seriesA23.XValues = xlWorkSheetA.get_Range("B1", weekLimitDataE.ToString() + "1");

        seriesA21.Name = "Profit";
        seriesA22.Name = "Sales";
        seriesA23.Name = "Minutes";

        seriesA21.Values = seriesA21_range;
        seriesA22.Values = seriesA22_range;
        seriesA23.Values = seriesA23_range;



        seriesA21.AxisGroup = Microsoft.Office.Interop.Excel.XlAxisGroup.xlSecondary;
        seriesA22.AxisGroup = Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary;
        seriesA23.AxisGroup = Microsoft.Office.Interop.Excel.XlAxisGroup.xlSecondary;
        

        seriesA21.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
        seriesA22.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        seriesA23.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlAreaStacked;
        myChartA2.Chart.HasTitle = true;
        myChartA2.Chart.ChartTitle.Text = "Minutes, Sales & Profit Per Week";
        myChartA2.Chart.Legend.Position = Microsoft.Office.Interop.Excel.XlLegendPosition.xlLegendPositionBottom;

        Microsoft.Office.Interop.Excel.Axis pAxis = (Microsoft.Office.Interop.Excel.Axis) myChartA2.Chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        pAxis.HasTitle = true;
        pAxis.AxisTitle.Text = "Sales";
        pAxis.MinimumScaleIsAuto = false;
        pAxis.MinimumScale = 200000;

        Microsoft.Office.Interop.Excel.Axis sAxis = (Microsoft.Office.Interop.Excel.Axis) myChartA2.Chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlSecondary);
        sAxis.HasTitle = true;
        sAxis.AxisTitle.Text = "Minutes & Profit";
        sAxis.MinimumScaleIsAuto = false;
        sAxis.MajorUnitIsAuto = false;
        sAxis.MajorUnit = 2500;
        sAxis.MinimumScale = 10000;


        Microsoft.Office.Interop.Excel.Range chartRangeA3;
        Microsoft.Office.Interop.Excel.ChartObjects xlChartsA3 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartA3 = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(10, 470, 450, 250);

        Microsoft.Office.Interop.Excel.Chart chartPageA3 = myChartA3.Chart;

        chartRangeA3 = xlWorkSheetE.get_Range("A2", "B" + (iDataI + 1));

        chartPageA3.SetSourceData(chartRangeA3, missValue);
        chartPageA3.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded;
        chartPageA3.ApplyLayout(1, Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded);
        chartPageA3.ChartTitle.Text = "Customer Profit Week ";
        chartPageA3.Elevation = 30;



        Microsoft.Office.Interop.Excel.Range chartRangeA4;
        Microsoft.Office.Interop.Excel.ChartObjects xlChartsA4 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartA4 = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(470, 470, 450, 250);

        Microsoft.Office.Interop.Excel.Chart chartPageA4 = myChartA4.Chart;

        chartRangeA4 = xlWorkSheetF.get_Range("A2", "B" + (iDataJ + 1));

        chartPageA4.SetSourceData(chartRangeA4, missValue);
        chartPageA4.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded;
        chartPageA4.ApplyLayout(1, Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded);
        chartPageA4.ChartTitle.Text = "Vendor Profit Week ";

        chartPageA4.Elevation = 30;


        

        Microsoft.Office.Interop.Excel.ChartObjects xlChartsA5 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartA5 = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(10, 1070, 455, 350);
        Microsoft.Office.Interop.Excel.Chart chartPageA5 = myChartA5.Chart;

        Microsoft.Office.Interop.Excel.Range chartRangeA5x;
        Microsoft.Office.Interop.Excel.Range chartRangeA5y;

        //chartRangeA5y = xlWorkSheetA.get_Range("R51", "R65");
        chartRangeA5y = xlWorkSheetA.get_Range(weekLimitDataB.ToString() + "51", weekLimitDataB.ToString() + (iDataB + 50));
        //chartRangeA5x = xlWorkSheetA.get_Range("A51", "A65");
        chartRangeA5x = xlWorkSheetA.get_Range("A51", "A" + (iDataB + 50));

        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionA5 = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPageA5.SeriesCollection(missValue);
        Microsoft.Office.Interop.Excel.Series seriesA51 = seriesCollectionA5.NewSeries();

        seriesA51.XValues = chartRangeA5x;
        seriesA51.Values = chartRangeA5y;
        
        chartPageA5.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPieExploded;
        chartPageA5.ApplyLayout(1, Microsoft.Office.Interop.Excel.XlChartType.xlPieExploded);
        chartPageA5.ChartTitle.Text = "Product Share Week";



        Microsoft.Office.Interop.Excel.ChartObjects xlChartsA6 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartA6 = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(475, 1070, 455, 350);
        Microsoft.Office.Interop.Excel.Chart chartPageA6 = myChartA6.Chart;

        Microsoft.Office.Interop.Excel.Range chartRangeA6x;
        Microsoft.Office.Interop.Excel.Range chartRangeA6y;

        chartRangeA6y = xlWorkSheetA.get_Range(Convert.ToChar(endColumnB-1).ToString() + "51", Convert.ToChar(endColumnB-1).ToString() + (iDataB + 50));
        chartRangeA6x = xlWorkSheetA.get_Range("A51", "A" + (iDataB + 50));

        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionA6 = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPageA6.SeriesCollection(missValue);
        Microsoft.Office.Interop.Excel.Series seriesA61 = seriesCollectionA6.NewSeries();

        seriesA61.XValues = chartRangeA6x;
        seriesA61.Values = chartRangeA6y;

        chartPageA6.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
        chartPageA6.HasTitle = true;
        chartPageA6.ChartTitle.Text = "Margin Product Week";



        Microsoft.Office.Interop.Excel.Range chartRangeB1;
        Microsoft.Office.Interop.Excel.ChartObjects xlChartsB1 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetB.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartB1 = (Microsoft.Office.Interop.Excel.ChartObject)xlChartsB1.Add(10, 180, 300, 250);

        Microsoft.Office.Interop.Excel.Chart chartPageB1 = myChartB1.Chart;


        chartRangeB1 = xlWorkSheetG.get_Range("A1", weekLimitDataK.ToString() + (1 + iDataK));
        chartPageB1.SetSourceData(chartRangeB1, Microsoft.Office.Interop.Excel.XlRowCol.xlRows);
        chartPageB1.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        chartPageB1.ApplyLayout(3, Microsoft.Office.Interop.Excel.XlChartType.xlLine);
        chartPageB1.ChartTitle.Text = "Profit Per AM Per Month";



        Microsoft.Office.Interop.Excel.ChartObjects xlChartsB2 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetB.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartB2 = (Microsoft.Office.Interop.Excel.ChartObject)xlChartsB2.Add(320, 180, 300, 250);
        Microsoft.Office.Interop.Excel.Chart chartPageB2 = myChartB2.Chart;

        Microsoft.Office.Interop.Excel.Range chartRangeB2x;
        Microsoft.Office.Interop.Excel.Range chartRangeB2y;

        //chartRangeB2y = xlWorkSheetG.get_Range(Convert.ToChar(endColumnB - 1).ToString() + "51", Convert.ToChar(endColumnB - 1).ToString() + (iDataB + 50));
        //chartRangeB2x = xlWorkSheetG.get_Range("A51", "A" + (iDataB + 50));

        chartRangeB2y = xlWorkSheetG.get_Range("B11", "Q11");
        chartRangeB2x = xlWorkSheetG.get_Range("B1", "Q1");

        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionB2 = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPageB2.SeriesCollection(missValue);
        Microsoft.Office.Interop.Excel.Series seriesB21 = seriesCollectionB2.NewSeries();

        seriesB21.XValues = chartRangeB2x;
        seriesB21.Values = chartRangeB2y;

        chartPageB2.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
        chartPageB2.HasTitle = true;
        chartPageB2.ChartTitle.Text = "Profit Product Month";


        Microsoft.Office.Interop.Excel.Range chartRangeB3;
        Microsoft.Office.Interop.Excel.ChartObjects xlChartsB3 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetB.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartB3 = (Microsoft.Office.Interop.Excel.ChartObject)xlChartsB3.Add(10, 440, 300, 250);

        Microsoft.Office.Interop.Excel.Chart chartPageB3 = myChartB3.Chart;

        chartRangeB3 = xlWorkSheetH.get_Range("A2", "B" + (iDataL + 1));

        chartPageB3.SetSourceData(chartRangeB3, missValue);
        chartPageB3.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded;
        chartPageB3.ApplyLayout(1, Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded);
        chartPageB3.ChartTitle.Text = "Customer Profit Month ";
        chartPageB3.Elevation = 30;



        Microsoft.Office.Interop.Excel.Range chartRangeB4;
        Microsoft.Office.Interop.Excel.ChartObjects xlChartsB4 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetB.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartB4 = (Microsoft.Office.Interop.Excel.ChartObject)xlChartsB4.Add(320, 440, 300, 250);

        Microsoft.Office.Interop.Excel.Chart chartPageB4 = myChartB4.Chart;

        chartRangeB4 = xlWorkSheetI.get_Range("A2", "B" + (iDataM + 1));

        chartPageB4.SetSourceData(chartRangeB4, missValue);
        chartPageB4.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded;
        chartPageB4.ApplyLayout(1, Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded);
        chartPageB4.ChartTitle.Text = "Vendor Profit Month ";

        chartPageB4.Elevation = 30;



        Microsoft.Office.Interop.Excel.ChartObjects xlChartsB5 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetB.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartB5 = (Microsoft.Office.Interop.Excel.ChartObject)xlChartsB5.Add(50, 1020, 455, 350);
        Microsoft.Office.Interop.Excel.Chart chartPageB5 = myChartB5.Chart;

        Microsoft.Office.Interop.Excel.Range chartRangeB5x;
        Microsoft.Office.Interop.Excel.Range chartRangeB5y;

        chartRangeB5y = xlWorkSheetB.get_Range(Convert.ToChar(endColumnD - 2).ToString() + "51", Convert.ToChar(endColumnD - 2).ToString() + (iDataD + 50));
        chartRangeB5x = xlWorkSheetB.get_Range("A51", "A" + (iDataB + 50));

        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionB5 = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPageB5.SeriesCollection(missValue);
        Microsoft.Office.Interop.Excel.Series seriesB51 = seriesCollectionB5.NewSeries();

        seriesB51.XValues = chartRangeB5x;
        seriesB51.Values = chartRangeB5y;

        chartPageB5.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPieExploded;
        chartPageB5.ApplyLayout(1, Microsoft.Office.Interop.Excel.XlChartType.xlPieExploded);
        chartPageB5.HasTitle = true;
        chartPageB5.ChartTitle.Text = "Share Month";

        
        rsDataA.Close();
        rsDataB.Close();
        rsDataC.Close();
        rsDataD.Close();
        
        DBConnection.Close();

        xlApp.DisplayAlerts = false;
        xlApp.AlertBeforeOverwriting = false;



        xlWorkBook.SaveAs(sPathFiles + "CallMexCustReport_"+ (weekNum-1) + "_" + sToday + ".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, missValue, missValue, missValue, missValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, missValue, missValue, missValue, missValue);
        xlWorkBook.Close(false, missValue, missValue);

        xlApp.Quit();
        //xlApp.Visible = true;

        releaseObject(xlWorkSheetA);
        releaseObject(xlWorkSheetB);
        releaseObject(xlWorkSheetC);
        releaseObject(xlWorkSheetD);
        releaseObject(xlWorkSheetE);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        while (xlApp != null)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
            xlApp = null;
        }

        GC.Collect();
        GC.WaitForPendingFinalizers();



        Response.Redirect("http://mecca.directo.com/reports-mecca/CallMexCustReport_" + (weekNum - 1) + "_" + sToday + ".xls");


    }

    private void releaseObject(object obj)
    {
        if (obj != null)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //lblError.Text = "Exception Occured while releasing object " + ex.ToString();
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}

