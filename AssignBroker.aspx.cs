using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class AssignBroker : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ddlCBroker_SelectedIndexChanged(object sender, EventArgs e)
    {
        sqldsCustAs.SelectCommand = "SELECT [Customer],[CustBroker] FROM [MECCA2].[dbo].[CBroker] WHERE CustBroker = '" + ddlCBroker.SelectedValue.ToString() + "'";
    }
    protected void ddlVBroker_SelectedIndexChanged(object sender, EventArgs e)
    {
        sqldsCustAs.SelectCommand = "SELECT [Customer],[CustBroker] FROM [MECCA2].[dbo].[CBroker] WHERE CustBroker = '" + ddlCBroker.SelectedValue.ToString() + "'";
    }
    protected void ddlNewCust_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnShowC_Click(object sender, EventArgs e)
    {
        sqldsCustAs.SelectCommand = "SELECT [Customer],[CustBroker] FROM [MECCA2].[dbo].[CBroker] WHERE CustBroker = '" + ddlCBroker.SelectedValue.ToString() + "'";
    }
    protected void btnShowV_Click(object sender, EventArgs e)
    {
        sqldsVenAs.SelectCommand = "SELECT [Vendor],[VendorBroker] FROM [MECCA2].[dbo].[VBroker] WHERE VendorBroker = '" + ddlVBroker.SelectedValue.ToString() + "'";
    }
    protected void btnAsignarC_Click(object sender, EventArgs e)
    {
        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;

        sqlSpQuery.CommandText = "sp_UpdateBrokers";

        sqlSpQuery.Parameters.Add("@Provider", SqlDbType.VarChar).Value = ddlNewCust.SelectedValue.ToString();
        sqlSpQuery.Parameters.Add("@Broker", SqlDbType.VarChar).Value = ddlCBroker.SelectedValue.ToString();
        sqlSpQuery.Parameters.Add("@tipo", SqlDbType.VarChar).Value = "C";
        Util.RunQuery(sqlSpQuery);
        sqldsCustAs.SelectCommand = "SELECT [Customer],[CustBroker] FROM [MECCA2].[dbo].[CBroker] WHERE CustBroker = '" + ddlCBroker.SelectedValue.ToString() + "'";   
    }
    protected void btnAsignarV_Click(object sender, EventArgs e)
    {
        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;

        sqlSpQuery.CommandText = "sp_UpdateBrokers";

        sqlSpQuery.Parameters.Add("@Provider", SqlDbType.VarChar).Value = ddlVendor.SelectedValue.ToString();
        sqlSpQuery.Parameters.Add("@Broker", SqlDbType.VarChar).Value = ddlVBroker.SelectedValue.ToString();
        sqlSpQuery.Parameters.Add("@tipo", SqlDbType.VarChar).Value = "V";
        Util.RunQuery(sqlSpQuery);


        sqldsVenAs.SelectCommand = "SELECT [Vendor],[VendorBroker] FROM [MECCA2].[dbo].[VBroker] WHERE VendorBroker = '" + ddlVBroker.SelectedValue.ToString() + "'";

    }
}
