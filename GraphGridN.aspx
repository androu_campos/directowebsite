<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="GraphGridN.aspx.cs"
    Inherits="GraphGridV" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table>
        <tr>
            <td>
                <DCWC:Chart ID="Chart1" runat="server" Width="610px" Height="300px" Palette="Pastel">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line"
                            Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
            <td>
                <DCWC:Chart ID="Chart2" runat="server" Width="610px" Height="300px" Palette="Pastel">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line"
                            Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
        </tr>
        <tr>
            <td>
                <DCWC:Chart ID="Chart3" runat="server" Width="610px" Height="300px" Palette="Pastel">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line"
                            Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
            <td>
                <DCWC:Chart ID="Chart4" runat="server" Width="610px" Height="300px" Palette="Pastel">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line"
                            Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
        </tr>
        <tr>
            <td>
                <DCWC:Chart ID="Chart5" runat="server" Width="610px" Height="300px" Palette="Pastel">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line"
                            Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
            <td>
                <DCWC:Chart ID="Chart6" runat="server" Width="610px" Height="300px" Palette="Pastel">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line"
                            Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
        </tr>
        <tr>
            <td>
                <DCWC:Chart ID="Chart7" runat="server" Width="610px" Height="300px" Palette="Pastel">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line"
                            Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
            <td>
                <DCWC:Chart ID="Chart8" runat="server" Width="610px" Height="300px" Palette="Pastel">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line"
                            Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
        </tr>
    </table>
</asp:Content>
