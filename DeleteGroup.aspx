<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="DeleteGroup.aspx.cs" Inherits="DeleteGroup" Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="left">
        <tr>
            <td align="left" style="width: 91px; height: 23px">
            </td>
            <td style="width: 95px; height: 23px">
            </td>
            <td style="width: 100px; height: 23px">
            </td>
        </tr>
        <tr>
            <td style="width: 91px" align="left">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="DELETE GROUP"></asp:Label></td>
            <td style="width: 95px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 91px; height: 22px">
            </td>
            <td style="width: 95px; height: 22px">
            </td>
            <td style="width: 100px; height: 22px">
            </td>
        </tr>
        <tr>
            <td style="width: 91px; height: 22px;" align="right" bgcolor="#d5e3f0">
                <asp:Label ID="Label2" runat="server" Text="Select Group:" CssClass="labelTurk"></asp:Label></td>
            <td style="width: 95px; height: 22px;" align="left">
                <asp:DropDownList ID="DropDownList1" runat="server" DataTextField="Name" CssClass="dropdown" Width="94px">
                </asp:DropDownList></td>
            <td style="width: 100px; height: 22px;" align="left">
                <asp:Button ID="cmdDelete" runat="server" CssClass="boton" OnClick="cmdDelete_Click"
                    Text="Delete" /></td>
        </tr>
        <tr>
            <td style="width: 91px; height: 29px">
            </td>
            <td align="right" style="width: 95px; height: 29px">
            </td>
            <td style="width: 100px; height: 29px">
            </td>
        </tr>
        <tr>
            <td style="width: 91px">
            </td>
            <td align="right" colspan="2">
                <asp:Label ID="lblDelete" runat="server" CssClass="labelTurk" Text="Label" Visible="False"></asp:Label></td>
        </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
        SelectCommand="SELECT DISTINCT Name FROM PlanBreakOut UNION SELECT ' ' AS Name FROM PlanBreakOut AS PlanBreakOut_1">
    </asp:SqlDataSource>



</asp:Content>

