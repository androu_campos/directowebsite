<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="Up.aspx.cs" Inherits="Up" Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label1" runat="server" CssClass="labelTurkH" Text="MECCA UPLOAD RATES" Font-Bold="True"></asp:Label>
    <table width="100%" style="position: absolute; top: 220px; left: 230px;">
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td>
                            <asp:FileUpload ID="FileUpload1" Font-Names="Arial" Font-Size="Small" runat="server" />
                        </td>
                        <td>
                            <asp:Button ID="Button1" runat="server" CssClass="boton" Text="Upload" OnClick="Button1_Click1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="left">
            </td>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Label ID="lblUpload" runat="server" CssClass="labelTurk" Visible="False" Width="100%"></asp:Label></td>
            <td>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="3" height="500px" valign="bottom">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
</asp:Content>
