<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SetEnlaceIPCost.aspx.cs" Inherits="SetEnlaceIPCost" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Directo - Connections Worldwide</title>
    <style type="text/css">
        .title {
            font-family: Calibri, Arial;
            font-size: 32pt;
            font-weight: bold;
        }
        .cell {
            font-family: Calibri, Arial;
        }
        .lbl 
        {
            font-family: Calibri, Arial;
            font-size: 24pt;
            font-weight: bold;
            color: #4F81BD;
        }
        .refresh 
        {
            font-family: Calibri, Arial;
            font-size: 16pt;
            font-weight: bold;
            color: #4F81BD;        	
        }
    </style>
</head>
<body>
    <form id="frmCostValues" runat="server" method="post">
    <span class="title">ENLACE-IP Cost Values</span>
    <br />
    <br />
    <table>
		<tr>
			<asp:Label ID="lblCurrentCostValues" runat="server" Text="Current Cost Values" Font-Underline="false" Font-Size="13pt" Font-Names="Arial"></asp:Label></tr>
    </table>
    <br />
    
    <asp:ScriptManager id="ScriptManager" runat="server" />
    <table>  
        <tr>
            <td>
                <asp:Label ID="lblmxcitycost" Text="Mexico" runat="server" Font-Bold="True"></asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:Label ID="lblpueblacost" Text="Other" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DetailsView ID="gvCostValues" runat="server"
                    AutoGenerateEditButton="True" AutoGenerateRows="False" DataSourceID="SqlDsCostValues" Height="50px" Width="350px" 
                    Font-Names="Arial" Font-Size="8pt" CellPadding="4" ForeColor="#333333">
                    
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="14pt" BackColor="#507CD1" ForeColor="White" />
                        
                    
                    <AlternatingRowStyle BackColor="White" />
                        
                    <Fields>            
                        <asp:BoundField DataField="priceline" HeaderText="Price Per Line" SortExpression="priceline" />
                        <asp:BoundField DataField="lines" HeaderText="Lines" SortExpression="lines" >
                            <ItemStyle Font-Bold="True" Font-Size="10pt" />
                        </asp:BoundField>
                    </Fields>
                    <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
                    <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
                </asp:DetailsView>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:DetailsView ID="DetailsView1" runat="server"
                    AutoGenerateEditButton="True" AutoGenerateRows="False" DataSourceID="SqlDsPueblaCost" Height="50px" Width="350px" 
                    Font-Names="Arial" Font-Size="8pt" CellPadding="4" ForeColor="#333333">
                    
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="14pt" BackColor="#507CD1" ForeColor="White" />
                        
                    
                    <AlternatingRowStyle BackColor="White" />
                        
                    <Fields>            
                        <asp:BoundField DataField="priceline" HeaderText="Price Per Line" SortExpression="priceline" />
                        <asp:BoundField DataField="lines" HeaderText="Lines" SortExpression="lines" >
                            <ItemStyle Font-Bold="True" Font-Size="10pt" />
                        </asp:BoundField>
                    </Fields>
                    <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
                    <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
                </asp:DetailsView>
            </td>
        </tr> 
        <tr>
            <td valign="top">
                <asp:GridView ID="gvCostHistoryMexico" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDSHistoryMexico" ForeColor="#333333" Font-Names="Arial" Font-Size="8pt">
                    <RowStyle BackColor="#EFF3FB" />
                    <Columns>
                        <asp:BoundField DataField="id" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                            SortExpression="id" />
                        <asp:BoundField DataField="insertdate" HeaderText="Date" SortExpression="date" />
                        <asp:BoundField DataField="priceline" HeaderText="Price Line" SortExpression="priceline" >
                            <ItemStyle Font-Bold="True" />
                        </asp:BoundField>
                        <asp:BoundField DataField="lines" HeaderText="Lines" SortExpression="lines" >
                            <ItemStyle Font-Bold="True" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#2461BF" />
                    <AlternatingRowStyle BackColor="White" />
                    <EmptyDataTemplate>
                        <span style="font-size: 11pt; color: #ff6600"><strong>No History Available</strong></span>
                    </EmptyDataTemplate>
                </asp:GridView>  
            </td>
            <td>
                &nbsp;
            </td>
            <td valign="top">
                <asp:GridView ID="gvCostHistoryPuebla" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDSHitoryPuebla" ForeColor="#333333" Font-Names="Arial" Font-Size="8pt">
                    <RowStyle BackColor="#EFF3FB" />
                    <Columns>
                        <asp:BoundField DataField="id" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                            SortExpression="id" />
                        <asp:BoundField DataField="insertdate" HeaderText="Date" SortExpression="date" />
                        <asp:BoundField DataField="priceline" HeaderText="Price Line" SortExpression="priceline" >
                            <ItemStyle Font-Bold="True" />
                        </asp:BoundField>
                        <asp:BoundField DataField="lines" HeaderText="Lines" SortExpression="lines" >
                            <ItemStyle Font-Bold="True" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#2461BF" />
                    <AlternatingRowStyle BackColor="White" />
                    <EmptyDataTemplate>
                        <span style="font-size: 11pt; color: #ff6600"><strong>No History Available</strong></span>
                    </EmptyDataTemplate>
                </asp:GridView>  
            </td>
        </tr>
    </table>


    <table>
        <tr>
            <td>
                <asp:SqlDataSource ID="SqlDsCostValues" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                    SelectCommand="SELECT [priceline], [lines] FROM [Enlaceipcostvalue] WHERE Country = 'MEXICO' AND City = 'MEXICO'"
                    UpdateCommand="UPDATE [Enlaceipcostvalue] SET [priceline] = @priceline , [lines] = @lines, [Country] = 'MEXICO', [City] = 'MEXICO' WHERE Country = 'MEXICO' AND City = 'MEXICO'" >   
                    <UpdateParameters>                        
                        <asp:Parameter Name="priceline" Type="String" />
                        <asp:Parameter Name="lines" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>                 
                <asp:SqlDataSource ID="SqlDSHistoryMexico" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                    SelectCommand="SELECT [id],[insertdate], [priceline], [lines] FROM [Enlaceipcostvaluelog] WHERE Country = 'MEXICO' and City = 'MEXICO'"
                    ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDSHitoryPuebla" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                    SelectCommand="SELECT [id],[insertdate], [priceline], [lines] FROM [Enlaceipcostvaluelog] WHERE Country = 'MEXICO' and City = 'OTHER'"
                    ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDsPueblaCost" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                    SelectCommand="SELECT [priceline], [lines] FROM [Enlaceipcostvalue] WHERE Country = 'MEXICO' and City = 'OTHER'"
                    UpdateCommand="UPDATE [Enlaceipcostvalue] SET [priceline] = @priceline , [lines] = @lines , [Country] = 'MEXICO', [City] = 'OTHER' WHERE Country = 'MEXICO' and City = 'OTHER'" >   
                    <UpdateParameters>                        
                        <asp:Parameter Name="priceline" Type="String" />
                        <asp:Parameter Name="lines" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource> 
            </td>
        </tr>
    </table>
    <br /><br />
    
      
    </form>
</body>
</html>
