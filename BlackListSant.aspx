<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    Theme="Theme1" AutoEventWireup="true" CodeFile="BlackListSant.aspx.cs" Inherits="BlackList"
    Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td>
                <asp:Label ID="lblsrch" runat="server" CssClass="labelBlue8" Text="Number: "></asp:Label>
                <asp:TextBox ID="txtSearch" runat="server" CssClass="textBox"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnsearch" runat="server" CssClass="boton" Text="Search" OnClick="btnsearch_Click" />
            </td>
            <td>
                <asp:Button ID="btnExpCSV" runat="server" CssClass="boton" Text="Export to CSV" OnClick="btnExpCSV_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCount" runat="server" CssClass="labelBlue8" Text="Total Records:"></asp:Label>
                <asp:TextBox ID="txtCount" runat="server" CssClass="textBox" Enabled="false" Width="90px"
                    TextAlignment="Right"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblError" runat="server" CssClass="lblNotFound" Visible="false"></asp:Label>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="width: 103px" valign="top">
                <asp:GridView ID="gvGood" runat="server" Font-Names="Arial" Font-Size="8pt" DataSourceID="sqldsBlackStatus"
                    AutoGenerateColumns="False" AllowPaging="True" PageSize="50" EnableModelValidation="True"
                    Visible="true">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Number" HeaderText="Number" SortExpression="digits" ItemStyle-Wrap="true" />
                        <asp:BoundField DataField="date_start" HeaderText="Date Start" SortExpression="date_start"
                            ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="date_end" HeaderText="Date End" SortExpression="date_end"
                            ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="enabled" HeaderText="Enabled" SortExpression="enabled" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqldsBlackStatus" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListSantander %>"
                    ProviderName="<%$ ConnectionStrings:BlackListSantander.ProviderName %>" SelectCommand="SELECT right(digits,13) as Number, date_start, date_end, enabled FROM lcr where reliability = 2">
                </asp:SqlDataSource>
                <asp:GridView ID="gvSearch" runat="server" Font-Names="Arial" Font-Size="8pt" DataSourceID="sqldsBlackStatusSearch"
                    AutoGenerateColumns="False" AllowPaging="True" PageSize="50" EnableModelValidation="True"
                    Visible="False">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Number" HeaderText="Number" ReadOnly="True" SortExpression="Number"
                            ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="date_start" HeaderText="date_start" SortExpression="date_start"
                            ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="date_end" HeaderText="date_end" SortExpression="date_end"
                            ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="enabled" HeaderText="enabled" SortExpression="enabled"
                            ItemStyle-Wrap="false" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqldsBlackStatusSearch" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListSantander %>"
                    ProviderName="<%$ ConnectionStrings:BlackListSantander.ProviderName %>" SelectCommand="SELECT right(digits,13) as Number, date_start, date_end, enabled FROM lcr where reliability = 2"
                    FilterExpression="Number like '%{0}%'">
                    <FilterParameters>
                        <asp:ControlParameter ControlID="txtSearch" Name="Number" PropertyName="Text" />
                    </FilterParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sqldsBlackListCount" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListSantander %>"
                    ProviderName="<%$ ConnectionStrings:BlackListSantander.ProviderName %>" SelectCommand="SELECT count(digits) Count FROM lcr where reliability = 2">
                    <FilterParameters>
                        <asp:ControlParameter ControlID="txtSearch" Name="Number" PropertyName="Text" />
                    </FilterParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
</asp:Content>
