<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="AlarmViewer.aspx.cs" Inherits="AlarmViewer" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurkH" Text="Alarm Noc Viewer"></asp:Label>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
    <br />
    <table align="left">
        <tr>
            <td style="width: 127px">
                </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 127px; height: 26px">
            </td>
            <td style="width: 100px; height: 26px">
            </td>
            <td style="width: 100px; height: 26px">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/alarmManagerNoc.aspx" CssClass="labelBlue8" Font-Italic="True" Font-Underline="True">Alarm Manager Noc</asp:HyperLink></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 127px">
            </td>
            <td style="width: 100px">
                <asp:GridView ID="GridView0" runat="server" Font-Names="Arial" Font-Size="8pt" GridLines="None" OnSelectedIndexChanged="GridView0_SelectedIndexChanged" Width="950px">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 127px; height: 51px;">
            </td>
            <td style="width: 100px; height: 51px;">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
            <td style="width: 100px; height: 51px;">
            </td>
        </tr>
        
             <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager><asp:Timer id="Timer2" runat="server" Interval="900000"></asp:Timer></td>
        </tr>
        
        
        
    </table>
    <br />
    <br />
    &nbsp;<asp:SqlDataSource ID="SqlDsNocAlarm" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        DeleteCommand="DELETE [MECCA2].[dbo].[NocAlarmsLog] WHERE ID = @ID" SelectCommand="SELECT ID, Name, Country, Region, Customer, Vendor, Source, OriginationIP, TerminationIP, Type, ASR, ACD, Minutes, TotalCalls AS Attemps, CompCalls AS Calls, Time FROM NocAlarmsLog ORDER BY Time DESC" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
    </asp:SqlDataSource>

<asp:Timer id="Timer1" runat="server" OnTick="Timer1_Tick" Interval="300000">
    </asp:Timer>

</asp:Content>

