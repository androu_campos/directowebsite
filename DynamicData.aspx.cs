using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DynamicData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {                   
            if (Request.QueryString.Count > 0)
            {
                string calls = Request.QueryString[0].ToString();
                string date = Request.QueryString[1].ToString();
                string graph = Request.QueryString[2].ToString();

                if (date.Length > 3)
                {
                    int indx = date.LastIndexOf("/");
                    date = date.Insert(indx + 5, " ");
                }
                txtCalls.Text = calls;
                txtDate.Text = date;
                if (graph == "C")
                    Label1.Text = "Calls";
                else
                    Label1.Text = "Ports";

            }
            else
            {

            }

            if (Session["idFrame"].ToString() == "0")
            {
                mainTable.Visible = false;
                Session["idFrame"] = 1;
            }
            else
            {
                mainTable.Visible = true;
            }

        
    }
}
