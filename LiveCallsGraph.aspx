<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="LiveCallsGraph.aspx.cs" Inherits="LiveCallsGraph" Title="Untitled Page" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server" >
		
    <table>
        <tr>
            <td style="width: 100px" height="50" valign="top">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="Graph Live Calls"
                    Width="97px"></asp:Label>
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager></td>
            <td height="50" style="width: 100px" valign="top">
            </td>
            <td rowspan="6" valign="top" align="left">
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3" height="50" valign="top">
                <table align="left">
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="lblCustomer" runat="server" CssClass="labelBlue8" Text="Customer:"></asp:Label></td>
                        <td style="width: 100px">
                <asp:DropDownList ID="dpdCustomer" runat="server" CssClass="dropdown" DataSourceID="sqlCustomer" DataTextField="ProviderName" DataValueField="ProviderName">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="lblVendor" runat="server" CssClass="labelBlue8" Text="Vendor:"></asp:Label></td>
                        <td style="width: 100px">
                <asp:DropDownList ID="dpdVendor" runat="server" CssClass="dropdown" DataSourceID="sqlVendor" DataTextField="ProviderName" DataValueField="ProviderName">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="lblCountry" runat="server" CssClass="labelBlue8" Text="Country:"></asp:Label></td>
                        <td style="width: 100px">
                <asp:DropDownList ID="dpdCountry" runat="server" CssClass="dropdown" DataSourceID="sqlCountry" DataTextField="Country" DataValueField="Country">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="lblRegion" runat="server" CssClass="labelBlue8" Text="Region:"></asp:Label></td>
                        <td style="width: 100px">
                <asp:DropDownList ID="dpdRegion" runat="server" CssClass="dropdown" DataSourceID="sqlRegion" DataTextField="Region" DataValueField="Region">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="lblType" runat="server" CssClass="labelBlue8" Text="Type:"></asp:Label></td>
                        <td style="width: 100px" valign="middle">
                <table>
                    <tr>
                        <td style="width: 54px">
                            <asp:DropDownList ID="dpdOrType" runat="server" CssClass="dropdown" Width="53px">
                                <asp:ListItem Value="2">==</asp:ListItem>
                                <asp:ListItem Value="1">&lt;&gt;</asp:ListItem>
                            </asp:DropDownList></td>
                        <td style="width: 95px">
                <asp:DropDownList ID="dpdType" runat="server" CssClass="dropdown" DataSourceID="sqlType" DataTextField="Type" DataValueField="Type" Width="93px">
                </asp:DropDownList></td>
                    </tr>
                </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="lblClass" runat="server" CssClass="labelBlue8" Text="Class:"></asp:Label></td>
                        <td style="width: 100px" valign="middle">
                <table>
                    <tr>
                        <td style="width: 53px">
                            <asp:DropDownList ID="dpdOrClass" runat="server" CssClass="dropdown" Width="53px">
                                <asp:ListItem Value="2">==</asp:ListItem>
                                <asp:ListItem Value="1">&lt;&gt;</asp:ListItem>
                            </asp:DropDownList></td>
                        <td style="width: 94px">
                <asp:DropDownList ID="dpdClass" runat="server" CssClass="dropdown" DataSourceID="sqlClass" DataTextField="Class" DataValueField="Class" Width="93px">
                </asp:DropDownList></td>
                    </tr>
                </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                            <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="From:"></asp:Label></td>
                        <td style="width: 100px" valign="middle">
                            <table>
                                <tr>
                                    <td style="width: 100px">
                            <asp:TextBox ID="txtFrom" runat="server" CssClass="dropdown" Width="120px"></asp:TextBox></td>
                                    <td style="width: 100px">
                                        <asp:Image ID="imgFrom" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                            <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="To:"></asp:Label></td>
                        <td style="width: 100px" valign="middle">
                            <table>
                                <tr>
                                    <td style="width: 100px">
                            <asp:TextBox ID="txtTo" runat="server" CssClass="dropdown" Width="120px"></asp:TextBox></td>
                                    <td style="width: 100px">
                                        <asp:Image ID="imgTo" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                <asp:Button ID="cmdGraph" runat="server" CssClass="boton" OnClick="cmdGraph_Click"
                    Text="View Graph" /></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 100px; color: navy; font-family: Arial; letter-spacing: normal;
                            text-align: center">
                            <asp:Label ID="Label4" runat="server" CssClass="labelTurk" Text="Products"></asp:Label></td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="6">
                           r</td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3" height="50" valign="top">
                <cc1:TabContainer ID="TabContainer1" runat="server">
                <cc1:TabPanel ID="productsTab" runat="server" TabIndex=0>
                <HeaderTemplate>
                Products
                </HeaderTemplate>
                
                <ContentTemplate>
                 <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="dropdown">
                                <asp:ListItem Value="0">Mexico CPP</asp:ListItem>
                                <asp:ListItem Value="1">Mexico NEA</asp:ListItem>
                                <asp:ListItem Value="2">Mexico On Net</asp:ListItem>
                                <asp:ListItem Value="3">Philippines Globe</asp:ListItem>
                                <asp:ListItem Value="4">Philippines Smart</asp:ListItem>
                                <asp:ListItem Value="5">Philippines PLDT</asp:ListItem>
                                <asp:ListItem Value="6">Brazil Fijo</asp:ListItem>
                                <asp:ListItem Value="7">Brazil Mobile</asp:ListItem>
                                <asp:ListItem Value="8">Peru ROC</asp:ListItem>
                            </asp:RadioButtonList>
                </ContentTemplate>
                
                </cc1:TabPanel>
                
                
                </cc1:TabContainer></td>
        </tr>
        <tr>
            <td style="width: 100px" align="left">
                <asp:Button ID="btn_excel" runat="server" CssClass="boton" OnClick="btn_excel_Click"
                    Text="Export To EXCEL" Width="83px" />
                </td>
            <td align="left" style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                
                <dcwc:chart id="Chart1" ImageType="Bmp" runat="server" borderlinestyle="Solid" palette="Pastel" width="967px" Height="464px">
                <Series>
<DCWC:Series BackGradientType="VerticalCenter" CustomAttributes="LabelStyle=TopRight" ShadowOffset="1" Font="Arial, 8.25pt" XValueType="DateTime" Name="Series1" Color="DarkBlue" ChartType="FastLine" MarkerBorderColor="64, 64, 64"></DCWC:Series>
</Series>
<ChartAreas>
<DCWC:ChartArea Name="Default" BackColor="226, 245, 245, 245" BorderColor="" BorderStyle="Solid">
<AxisX TitleFont="Arial, 8.25pt, style=Bold" Interlaced="True" Title="Date/Time">
<LabelStyle Format="g"></LabelStyle>
    <MajorGrid LineColor="Gray" />
</AxisX>

<Area3DStyle Light="Realistic"></Area3DStyle>

<AxisY Title="Calls" TitleFont="Microsoft Sans Serif, 8.25pt, style=Bold">
<LabelStyle Format="N0"></LabelStyle>

<MajorTickMark Style="None"></MajorTickMark>
    <MajorGrid LineColor="Gray" />
</AxisY>
</DCWC:ChartArea>
</ChartAreas>
<Titles>
    <DCWC:Title Color="MidnightBlue" Font="Microsoft Sans Serif, 8.25pt, style=Bold"
        Name="Title1" Text="Per Day/Time for Provider">
    </DCWC:Title>
</Titles>
<Legends>
    <DCWC:Legend Enabled="False" Name="Default">
    </DCWC:Legend>
</Legends>
                    <BorderSkin PageColor="AliceBlue" />
</dcwc:chart>
                &nbsp; &nbsp;
             
                </td>
            <td style="width: 100px" valign="top">

            <%--<iframe id="myframe" src="graphdetails.aspx?c=0">
            
            </iframe>--%>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 100px">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<asp:GridView id="gdvLiveCalls" runat="server" Font-Size="8pt" Font-Names="Arial" AllowPaging="True">
<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> 
</contenttemplate>
                </asp:UpdatePanel>
            </td>
            <td style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:SqlDataSource ID="sqlCustomer" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>" SelectCommand="SELECT DISTINCT ProviderName FROM ProviderIP WHERE (Type = 'C') UNION SELECT '**ALL**' AS ProviderName FROM ProviderIP AS ProviderIP_1 UNION SELECT '** NONE **' as ProviderName FROM ProviderIP"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sqlVendor" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    SelectCommand="SELECT DISTINCT ProviderName FROM ProviderIP WHERE (Type = 'V') UNION SELECT '**ALL**' AS ProviderName FROM ProviderIP AS ProviderIP_1 UNION SELECT '** NONE **' as ProviderName FROM ProviderIP">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sqlCountry" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    SelectCommand="SELECT DISTINCT Country FROM Regions UNION SELECT '**ALL**' AS Country FROM Regions AS Regions_1 UNION SELECT '** NONE **' as Country FROM Regions">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sqlRegion" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    SelectCommand="SELECT DISTINCT Region FROM Regions UNION SELECT '**ALL**' AS Region FROM Regions AS Regions_1 UNION SELECT '** NONE **' as Region FROM Regions">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sqlType" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    SelectCommand="SELECT DISTINCT Type FROM Regions UNION SELECT '**ALL**' AS Type FROM Regions AS Regions_2 UNION SELECT '** NONE **' AS Type FROM Regions AS Regions_1">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sqlClass" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    SelectCommand="SELECT distinct Class from Regions&#13;&#10;UNION&#13;&#10;SELECT     '**ALL**' AS Class&#13;&#10;FROM         Regions AS Class&#13;&#10;UNION&#13;&#10;SELECT     '** NONE **' AS Class&#13;&#10;FROM         Regions AS Class">
                </asp:SqlDataSource>
                <cc1:calendarextender id="CalendarExtender1" runat="server" format="yyyy/MM/dd" popupbuttonid="imgFrom"
                    targetcontrolid="txtFrom"></cc1:calendarextender>
                <cc1:calendarextender id="CalendarExtender2" runat="server" format="yyyy/MM/dd" popupbuttonid="imgTo"
                    targetcontrolid="txtTo"></cc1:calendarextender>
            </td>
        </tr>
    </table>
</asp:Content>

