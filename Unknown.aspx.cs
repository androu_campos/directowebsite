using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Unknown : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MyDataSetTableAdaptersTableAdapters.UnknownIDsTableAdapter adp = new MyDataSetTableAdaptersTableAdapters.UnknownIDsTableAdapter();
        MyDataSetTableAdapters.UnknownIDsDataTable tbl = adp.GetData();
        if (tbl.Rows.Count <= 0)
        {
            lblmessage.Visible = true;
        }
        else
        {
            lblmessage.Visible = false;
            gdvUnknown.DataSource = adp.GetData();
            gdvUnknown.DataBind();
        }
    }
}
