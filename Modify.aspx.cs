using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class Modify : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        try
        {
            string rdbType = Session["rdbType"].ToString();
            string dpdCustname = Session["dpdCustname"].ToString();
            string txtRate= Session["dpdRate"].ToString();
            string txtPrefix= Session["txtPrefix"].ToString();
            string qrysql = "SELECT  ";

            if (txtRate == string.Empty)
            {
                if (rdbType == "C")
                {

                    qrysql = qrysql + " CustName, Prefix, Price from CustBreakoutEZ WHERE Price >=0  and CustName like '" + dpdCustname + "' and Prefix like '" + txtPrefix + "%'";

                }
                else if (rdbType == "V")
                {

                    qrysql = qrysql + " VendorName, Prefix, Cost from VendorBreakout WHERE Cost >=0 and VendorName like '" + dpdCustname + "' and Prefix  like '" + txtPrefix + "%'";

                }
            }
             else
            {


            if(rdbType == "C")
            {

                qrysql = qrysql + " CustName, Prefix, Price from CustBreakoutEZ WHERE Price = '" + txtRate + "%' and CustName like '" + dpdCustname + "' and Prefix like '" + txtPrefix + "%'";
                qrysql = qrysql.Replace("%%", "%");
            
            }
            else if (rdbType == "V")
            {
                
               qrysql = qrysql + " VendorName, Prefix, Cost from VendorBreakout WHERE Cost = '" + txtRate + "' and VendorName like '" + dpdCustname + "' and Prefix  like '" + txtPrefix + "%'";
               qrysql = qrysql.Replace("%%", "%");
            }
             
            }

            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = System.Data.CommandType.Text;
            sqlSpQuery.CommandText = qrysql;
            DataSet ResultSetC;
            ResultSetC = RunQuery(sqlSpQuery);
            Session["Rates"] = ResultSetC;
            gdvModify.DataSource = ResultSetC.Tables[0];
            if (ResultSetC.Tables[0].Rows.Count > 0)
            {
                gdvModify.DataBind();
                Label1.Visible = false;
                Button1.Visible = true;
            }
            else
            {
                Label1.Visible = true;
                Button1.Visible = false;
            }
        }
        catch (Exception ex)
        {
            string errormessage = ex.ToString();            
        }
    }
    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void gdvModify_RowEditing(object sender, GridViewEditEventArgs e)
    {        
        Session["pref"] = gdvModify.Rows[e.NewEditIndex].Cells[3].Text;
        int index = e.NewEditIndex;
        Response.Redirect("Editing.aspx", false);
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["Rates"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "Rates" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string erro = ex.Message.ToString();
        }

    }
}
