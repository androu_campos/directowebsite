using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;
public partial class DidPortGraphsPh : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Text = DateTime.Now.ToShortDateString();
            txtTo.Text = DateTime.Now.ToShortDateString();
            if (Request.QueryString.Count > 0)
            {
                string provider = Request.QueryString["P"].ToString();
                string query = string.Empty;
                string title = string.Empty;
                DataTable tbl;
                string from = Request.QueryString["dF"].ToString();
                string to = Request.QueryString["dT"].ToString();
                from = from.Replace("_", "/");
                to = to.Replace("_", "/");

                if (provider == "**ALL**") provider = "%";

                if (Request.QueryString["T"].ToString() == "0") //Customer
                {                    
                    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' and ID IN ('STC02','MIA03') group by [Time] order by [Time]";
                    

                    tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                    if (provider == "%") provider = "ALL";
                    title = "Customer: " + provider;

                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;
                }
                else//Vendor
                {

                    if (provider == "AMERICATEL" || provider == "AMERICATEL-PERU" || provider == "CELL-TERMINATION" ||
                             provider == "CONVERGIA" || provider == "IBASIS" || provider == "INTER-CONNECTION-LLC" ||
                             provider == "KEEN-OCEAN" || provider == "NETSERVICE" || provider == "NEXTCOM" ||
                             provider == "NEXTCOM-OFFNET" || provider == "TELEFONICA-BRASIL" || provider == "TELWISE" ||
                             provider == "TRANSITAR" || provider == "NATIONAL" || provider == "NATIONAL-NEXT" ||
                             provider == "NATIONAL-OFF" || provider == "NATIONAL-TELULAR" || provider == "OUTLANDER" || provider == "OUTLANDER-CALL" ||
                             provider == "OUTLANDER-CALL2" || provider == "OUTLANDER-CALL3" || provider == "OUTLANDER-GUATE-CLAR" ||
                             provider == "OUTLANDER-ICS" || provider == "OUTLANDER-MS" || provider == "OUTLANDER-POSPAY" || provider == "OUTLANDER-POSPAY-ICS" || provider == "OUTLANDER-POSPREMIUM" || provider == "OUTLANDER-POSPRE-ICS")
                    {

                        if (provider == "OUTLANDER")
                        {

                            //query = "SELECT SUM(CALLS) as Live_Calls,[Time] FROM (SELECT SUM(CALLS) AS CALLS, [TIME] FROM tmpLiveCallsTraffic WHERE VENDORTAG IN  ( 'OLDR', 'OLAM', 'BSTI', 'OLBR','OLCL','OLC2','OLC3','OLEC','OLFX','OGCL','OGMS','OGTI','OCIC','OLMS','OLPL','OLPH','OLPO','OLPR') AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] UNION SELECT SUM(CALLS) AS CALLS, [TIME] FROM TrafficLogLive WHERE VENDOR in ( select PROVIDERNAME from cdrdb..providerip where ProviderName like 'OUTLANDER%' and [Type] like 'V%') AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] ) A group by [Time] order by [Time] ";
                            query = "select isnull(SUM(Live_Calls),0)  as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername like 'OUTLANDER%' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time] order by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                        }
                        else if (provider == "OUTLANDER-POSPAY" || provider == "OUTLANDER-POSPAY-ICS")
                        {
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('OUTLANDER-POSPAY','OUTLANDER-POSPAY-ICS') and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else if (provider == "OUTLANDER-POSPREMIUM" || provider == "OUTLANDER-POSPRE-ICS")
                        {
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('OUTLANDER-POSPREMIUM','OUTLANDER-POSPRE-ICS') and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else if (provider == "OUTLANDER-CALL" || provider == "OUTLANDER-CALL2" || provider == "OUTLANDER-CALL3" || provider == "OUTLANDER-GUATE-CLAR" || provider == "OUTLANDER-ICS" || provider == "OUTLANDER-MS")
                        {
                            //query = "select SUM(Live_Calls) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername like '" + provider + "' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdrn where Cust_Vendor in (select ip from cdrdb..providerip where providername like '" + provider + "' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else
                        {
                            //query = "SELECT SUM(CALLS) as Live_Calls,[Time] FROM (SELECT SUM(CALLS) AS CALLS, [TIME] FROM cdrdb..cdrn WHERE VENDOR LIKE '" + provider + "' AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] UNION SELECT SUM(CALLS) AS CALLS, [TIME] FROM TrafficLogLive WHERE VENDOR LIKE '" + provider + "' AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] ) A group by [Time] order by [Time] ";
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('" + provider + "') and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmnt(query).Tables[0];
                        }


                    }
                    else
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'V%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'V%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                    }



                    if (provider == "%") provider = "ALL";
                    title = "Vendor: " + provider;

                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;
                }

                makeChart(tbl, title);
                //gdvDetail.Visible = false;
                lblGrid.Visible = false;

                txtFrom.Text = from;
                txtTo.Text = to;
                dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
                dpdType.SelectedIndex = Convert.ToInt32(Request.QueryString["T"]);
                cmdDetail.Visible = true;

            }

        }
        else
        {

            if (Request.QueryString.Count > 0)
            {
                string provider = Request.QueryString["P"].ToString();
                string query = string.Empty;
                string title = string.Empty;
                DataTable tbl;
                string from = txtFrom.Text;//Request.QueryString["dF"].ToString();
                string to = txtTo.Text;//Request.QueryString["dT"].ToString();
                from = from.Replace("_", "/");
                to = to.Replace("_", "/");

                if (provider == "**ALL**") provider = "%";

                if (Request.QueryString["T"].ToString() == "0") //Customer
                {
                    //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";


                    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' and ID IN ('STC02') group by [Time] order by [Time]";
                    


                    tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                    if (provider == "%") provider = "ALL";
                    title = "Customer: " + provider;

                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;
                }
                else//Vendor
                {
                    if (provider == "AMERICATEL" || provider == "AMERICATEL-PERU" || provider == "CELL-TERMINATION" ||
                             provider == "CONVERGIA" || provider == "IBASIS" || provider == "INTER-CONNECTION-LLC" ||
                             provider == "KEEN-OCEAN" || provider == "NETSERVICE" || provider == "NEXTCOM" ||
                             provider == "NEXTCOM-OFFNET" || provider == "TELEFONICA-BRASIL" || provider == "TELWISE" ||
                             provider == "TRANSITAR" || provider == "NATIONAL" || provider == "NATIONAL-NEXT" ||
                             provider == "NATIONAL-OFF" || provider == "NATIONAL-TELULAR" || provider == "OUTLANDER" || provider == "OUTLANDER-CALL" ||
                             provider == "OUTLANDER-CALL2" || provider == "OUTLANDER-CALL3" || provider == "OUTLANDER-GUATE-CLAR" ||
                             provider == "OUTLANDER-ICS" || provider == "OUTLANDER-MS" || provider == "OUTLANDER-POSPAY" || provider == "OUTLANDER-POSPAY-ICS" || provider == "OUTLANDER-POSPREMIUM" || provider == "OUTLANDER-POSPRE-ICS")
                    {

                        if (provider == "OUTLANDER")
                        {

                            //query = "SELECT SUM(CALLS) as Live_Calls,[Time] FROM (SELECT SUM(CALLS) AS CALLS, [TIME] FROM tmpLiveCallsTraffic WHERE VENDORTAG IN  ( 'OLDR', 'OLAM', 'BSTI', 'OLBR','OLCL','OLC2','OLC3','OLEC','OLFX','OGCL','OGMS','OGTI','OCIC','OLMS','OLPL','OLPH','OLPO','OLPR') AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] UNION SELECT SUM(CALLS) AS CALLS, [TIME] FROM TrafficLogLive WHERE VENDOR in ( select PROVIDERNAME from cdrdb..providerip where ProviderName like 'OUTLANDER%' and [Type] like 'V%') AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] ) A group by [Time] order by [Time] ";
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername like 'OUTLANDER%' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                        }
                        else if (provider == "OUTLANDER-POSPAY" || provider == "OUTLANDER-POSPAY-ICS")
                        {
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('OUTLANDER-POSPAY','OUTLANDER-POSPAY-ICS') and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else if (provider == "OUTLANDER-POSPREMIUM" || provider == "OUTLANDER-POSPRE-ICS")
                        {
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('OUTLANDER-POSPREMIUM','OUTLANDER-POSPRE-ICS') and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else if (provider == "OUTLANDER-CALL" || provider == "OUTLANDER-GUATE-CLAR" || provider == "OUTLANDER-ICS" || provider == "OUTLANDER-MS" || provider == "OUTLANDER-CALL2" || provider == "OUTLANDER-CALL3")
                        {
                            //query = "select SUM(Live_Calls) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername like '" + provider + "' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername like '" + provider + "' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else
                        {
                            query = "SELECT SUM(CALLS) as Live_Calls,[Time] FROM (SELECT SUM(CALLS) AS CALLS, [TIME] FROM tmpLiveCallsTraffic WHERE VENDOR LIKE '" + provider + "' AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] UNION SELECT SUM(CALLS) AS CALLS, [TIME] FROM TrafficLogLive WHERE VENDOR LIKE '" + provider + "' AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] ) A group by [Time] order by [Time] ";
                            tbl = Util.RunQueryByStmnt(query).Tables[0];
                        }


                    }
                    else
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'V%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'V%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                    }

                    if (provider == "%") provider = "ALL";
                    title = "Vendor: " + provider;

                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;
                }

                makeChart(tbl, title);
                //gdvDetail.Visible = false;
                lblGrid.Visible = false;

                txtFrom.Text = from;
                txtTo.Text = to;

                //dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
            }

        }

    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {

        string from = txtFrom.Text;
        string to = txtTo.Text;

        from = from.Replace("/", "_");
        to = to.Replace("/", "_");

        if (dpdType.SelectedIndex == 0)   //Customer
            Response.Redirect("DidPortGraphsPh.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=0&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
        else                             //Vendor
            Response.Redirect("DidPortGraphsPh.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=1&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);


    }

    private void makeChart(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart1.Titles.Add(tITLE);
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetail"] = tbl;

            this.Chart1.Visible = true;
            lblWarning.Visible = false;
            this.cmdDetail.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            lblWarning.Visible = true;
            Session["tblDetail"] = tbl;
            this.cmdDetail.Visible = false;

        }

    }
    protected void cmdDetail_Click(object sender, EventArgs e)
    {
        if (((DataTable)Session["tblDetail"]).Rows.Count > 0)
        {

            gdvDetail.DataSource = ((DataTable)Session["tblDetail"]);
            gdvDetail.DataBind();

            gdvDetail.Visible = true;
            lblGrid.Visible = true;
            lblGrid.Text = dpdProvider.SelectedValue.ToString();
            cmdHide.Visible = true;
            cmdDetail.Visible = false;
        }
        else
        {
        }
    }
    protected void cmdHide_Click(object sender, EventArgs e)
    {
        gdvDetail.Visible = false;
        lblGrid.Visible = false;
        cmdDetail.Visible = true;
        cmdHide.Visible = false;

    }
}
