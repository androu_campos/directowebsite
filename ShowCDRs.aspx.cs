using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;
using System.Text;


public partial class ShowCDRs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string user;
        user = Session["DirectoUser"].ToString();

        if (Page.IsPostBack)
        {

        }
        else
        {
            //if (user == "arturo.marquez" || user == "rodrigo.andrade")
            //{
                FillCustVend();
            //}
            //else
            //{
            //    Response.Redirect("ContactUs.aspx", false);
            //}
        }
    }

    private void FillCustVend()
    {
        SqlConnection SqlConn1 = new SqlConnection();
        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand SqlCommand1 = null;

        string cmd = string.Empty;
        cmd = "SELECT DISTINCT PROVIDERNAME INTO #CustomerList FROM CDRDB.DBO.PROVIDERIP WHERE [TYPE] = 'C' \n ";
        cmd += "INSERT INTO #CustomerList SELECT '**ALL**' \n ";
        cmd += "UPDATE #CustomerList SET Providername = 'ICS-CTR' WHERE Providername like 'ICS-CTR' \n ";
        cmd += "UPDATE #CustomerList SET Providername = 'ICS' WHERE Providername like 'ICS%' and Providername not like 'ICS-CTR' \n ";
        ////cmd += "UPDATE #CustomerList SET Providername = 'TALKTEL' WHERE Providername IN (SELECT CUSTOMER FROM CDRDB.DBO.CUSTBROKERREL WHERE CUSTBROKER = 'CallCenters' AND CUSTOMER NOT LIKE 'CCPH%' AND CUSTOMER NOT LIKE 'CC-PHIP%' ) \n ";
        cmd += "INSERT INTO #CustomerList SELECT 'ALLACCESS-CCPH06' \n ";
        cmd += "SELECT DISTINCT Providername FROM #CustomerList order by ProviderName \n ";

        SqlConn1.Open();
        //SqlCommand1 = new SqlCommand("SELECT DISTINCT (CASE WHEN ProviderName LIKE 'ICS%' THEN 'ICS' ELSE ProviderName END) ProviderName from ProviderIP WHERE Type like 'C' UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT '** NONE **' from ProviderIP as ProviderName", SqlConn1);
        SqlCommand1 = new SqlCommand(cmd, SqlConn1);

        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
        while (myReader1.Read())
        {
            dpdCustCode.Items.Add(myReader1.GetValue(0).ToString());
        }
        myReader1.Close();

        //VENDOR
        SqlCommand SqlCommand = null;
        SqlCommand = new SqlCommand("SELECT DISTINCT ProviderName from ProviderIP WHERE Type like 'V' UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT 'NATIONAL ALL' from ProviderIP as ProviderName UNION SELECT 'OUTLANDER-ALL' from ProviderIP as ProviderName order by ProviderName", SqlConn1);

        SqlDataReader myReader = SqlCommand.ExecuteReader();
        while (myReader.Read())
        {
            dpdVendId.Items.Add(myReader.GetValue(0).ToString());
        }
        myReader.Close();

        //COUNTRY
        SqlCommand SqlCommand2 = null;
        SqlCommand2 = new SqlCommand("SELECT Country from CountryCodes UNION SELECT '**ALL**' from CountryCodes as Country order by Country", SqlConn1);
        
        SqlDataReader myReader2 = SqlCommand2.ExecuteReader();
        while (myReader2.Read())
        {
            dpdCountry.Items.Add(myReader2.GetValue(0).ToString());
        }
        myReader2.Close();

        //LMC
        SqlCommand SqlCommand10 = new SqlCommand("SELECT DISTINCT LMC FROM Regions WHERE LMC is not NULL UNION SELECT '**ALL**' AS LMC FROM Regions ORDER BY LMC", SqlConn1);
        SqlDataReader myReader10 = SqlCommand10.ExecuteReader();
        while (myReader10.Read())
        {
            dpdLMC.Items.Add(myReader10.GetValue(0).ToString());
        }
        myReader10.Close();

        //TYPE
        SqlCommand SqlCommand6 = new SqlCommand("select distinct Regions.Type from Regions union select '**ALL**' as Type from Regions order by Type ASC", SqlConn1);
        SqlDataReader myReader6 = SqlCommand6.ExecuteReader();
        while (myReader6.Read())
        {
            dpdType.Items.Add(myReader6.GetValue(0).ToString());
        }

        myReader6.Close();

        //Cust SIP CAUSE
        SqlCommand SqlCommand7 = new SqlCommand("select SIPCauseCode FROM CDRDB.DBO.SIPCauseCodes UNION SELECT '**ALL**' order by SIPCauseCode ASC", SqlConn1);
        SqlDataReader myReader7 = SqlCommand7.ExecuteReader();
        while (myReader7.Read())
        {
            dpdCSIPCause.Items.Add(myReader7.GetValue(0).ToString());
        }

        myReader7.Close();

        //Vend SIP CAUSE
        SqlCommand SqlCommand8 = new SqlCommand("select SIPCauseCode FROM CDRDB.DBO.SIPCauseCodes UNION SELECT '**ALL**' order by SIPCauseCode ASC", SqlConn1);
        SqlDataReader myReader8 = SqlCommand8.ExecuteReader();
        while (myReader8.Read())
        {
            dpdVSIPCause.Items.Add(myReader8.GetValue(0).ToString());
        }
        
        myReader8.Close();

        //Vend SIP CAUSE
        SqlCommand SqlCommand9 = new SqlCommand("Select distinct(NOMBRE_CORTO) as FinalLMC  From dbo.PNN_PST UNION SELECT '**ALL**' order by FinalLMC ASC", SqlConn1);
        SqlDataReader myReader9 = SqlCommand9.ExecuteReader();
        while (myReader9.Read())
        {
            ddlFinalLMC.Items.Add(myReader9.GetValue(0).ToString());
        }
        myReader8.Close();

        SqlConn1.Close();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (txtReason.Text == "")
        {
            lbl14.Text = "Please enter valid reason.";
            lbl14.Visible = true;
            txtReason.Focus();

            gdvCdrs.Visible = false;
        }
        else
        {
            lbl14.Visible = false;

            string day = dpdDay.SelectedValue.ToString();
            string country = dpdCountry.SelectedValue.ToString();
            string codeCustomer = dpdCustCode.SelectedValue.ToString();
            string vendorId = dpdVendId.SelectedValue.ToString();
            string numbera = txtNumbera.Text;
            string numberb = txtNumberb.Text;
            string lmc = dpdLMC.SelectedValue.ToString();
            string type = dpdType.SelectedValue.ToString();
            string finalLMC = ddlFinalLMC.SelectedValue.ToString();
            string CSIPCause = dpdCSIPCause.SelectedValue.ToString();
            string VSIPCause = dpdVSIPCause.SelectedValue.ToString();
            string user = Session["DirectoUser"].ToString();
            string mrol = Session["Rol"].ToString();
            string reason = txtReason.Text;
            string queryw = "";
            string queryc = "";

            lmc.Replace("**ALL**", "%");
            lmc.Replace("** NONE **", "%");

            type.Replace("**ALL**", "%");
            type.Replace("** NONE **", "%");


            finalLMC.Replace("**ALL**", "%");
            finalLMC.Replace("** NONE **", "%");

            //string query = "select top  " + txtRecords.Text + " [OriginationID],[m_sIPAddress],[m_sRemoteIPAddress],[m_sConnectionId],CDRDB.DBO.fn_TrimInto([m_sCallingStationId]) AS 'm_sCallingStationId',CDRDB.DBO.fn_TrimInto([m_sCalledStationID]) AS 'm_sCalledStationID',[stop],[start],[lDuration],[m_dwDisconnectReason],[TerminationID],[VendorID],[PDD],[Match],[call_hold_time],[call_dest_crname],[isdn_cause_code],[original_isdn_cause_code],[codec_on_src_leg],[codec_on_dest_leg],[CallID],[NextoneCallID],CDRDB.DBO.fn_TrimInto([m_sCallingStationAux]) AS 'm_sCallingStationAux',[MatchB],CDRDB.DBO.fn_TrimInto([m_sCallingStationSend]) AS 'm_sCallingStationSend' from cdrdb.dbo.cdrs where m_sRemoteIPAddress like '" + codeCustomer.Replace("** ALL **", "%") + "' and m_sIPAddress like '" + vendorId.Replace("** ALL **", "%") + "' and stop > dateadd(minute,-" + txtMinutes.Text + ",GetDate())";
            string query = "SELECT TOP 10000 convert(varchar(11),dateadd(day,0,left(stop,11)),105) 'Billingdate',  ";
            query += "m_scallingstationid as 'DialedNumber', ";
            query += "case when m_scallingstationid like '521%' then substring(m_scallingstationid,4,10) ";
            query += "when m_scallingstationid like '52%' then substring(m_scallingstationid,3,10) end 'Number', ";
            query += "convert(varchar(50),null) NIR, ";
            query += "convert(varchar(50),null) SERIE, ";
            query += "convert(varchar(50),null) SERIE2, ";
            query += "convert(varchar(50),null) isPorted, ";
            query += "convert(varchar(50),null) finalLMC, ";
            query += "convert(varchar(50),null) PN, ";
            query += "convert(varchar(50),null) PNType, ";
            query += "convert(varchar(50),null) PNMode, ";
            query += "convert(varchar(50),null) LMC, ";
            query += "convert(varchar(50),null) tipomecca, ";
            query += "convert(varchar(50),null) classmecca, ";
            query += "convert(varchar(50),null) country, ";
            query += "mecca2.dbo.billableminutes(lduration,60) 'Minutes', ";
            query += "case when lduration > 0 then 1 else 0 end 'AnsweredCalls', ";
            query += "1 'Attempts', ";
            query += "a.lduration 'Duration', ";
            query += "replace(m_sConnectionId,' ','') Source, ";
            query += "b.providername 'Customer', ";
            query += "ingressadjacency, ";
            query += "c.providername 'Vendor', ";
            query += "egressadjacency, ";
            query += "m_scalledstationid 'Txnumber', ";
            query += "m_dwDisconnectReason 'isdn_cause_code', ";
            query += "isdn_cause_code 'v_sip_cause', ";
            query += "original_isdn_cause_code 'c_sip_cause', ";
            query += "[stop], ";
            query += "PDD, ";
            query += "OriginationID OriginationIP, ";
            query += "TerminationID TerminationIP, ";
            query += "TerminateCause, ";
            query += "UnexpectedReleaseReason ";
            query += "Into #CDRS ";
            query += "from cdrdb.dbo." + day + " a ";
            query += "left join cdrdb.dbo.providerip b on a.m_sremoteipaddress = b.ip ";
            query += "left join cdrdb.dbo.providerip c on a.m_sipaddress = c.ip ";

            if (country != "**ALL**")
            {
                query += "left join cdrdb.dbo.CountryCodes d on d.code = left(m_scallingstationid,len(code)) ";
            }
            
            
            query += "where b.providername like '" + codeCustomer.Replace("**ALL**", "%") + "' ";
            query += "and c.providername like '" + vendorId.Replace("**ALL**", "%") + "' ";

            if (country != "**ALL**")
            {
                query += "and d.Country like '" + country.Replace("**ALL**", "%") + "' ";
            }

            
            query += "and m_scalledstationid like '%" + numbera + "%' ";
            query += "and m_scallingstationid like '%" + numberb + "%' ";
            query += "and isdn_cause_code like '%" + CSIPCause.Replace("**ALL**", "%") + "%' ";
            query += "and original_isdn_cause_code like '%" + VSIPCause.Replace("**ALL**", "%") + "%' ";

            query += "UPDATE #CDRs ";
            query += "SET NIR = CASE WHEN LEFT(NUMBER,2) IN ('55','33','81') THEN LEFT(NUMBER,2) ELSE LEFT(NUMBER,3) END, ";
            query += "SERIE = CASE WHEN LEFT(NUMBER,2) IN ('55','33','81') THEN SUBSTRING(NUMBER,3,4) ELSE SUBSTRING(NUMBER,4,3) END, ";
            query += "SERIE2 = RIGHT(NUMBER,4) ";

            query += "UPDATE #CDRs ";
            query += "SET PN = B.[NOMBRE_CORTO], PNTYPE = B.[TIPO_RED], PNMODE = B.[MODALIDAD] ";
            query += "FROM #CDRs a  ";
            query += "JOIN CDRDB.dbo.PNN_PST B ON A.NIR = B.[NIR] AND A.SERIE = B.[SERIE]  ";
            query += "AND CAST(A.SERIE2 AS BIGINT) BETWEEN B.[NUMERACION_INICIAL] AND B.[NUMERACION_FINAL] ";
            query += "WHERE ISNUMERIC(A.SERIE2) = 1 ";

            query += "UPDATE #CDRs  ";
            query += "SET lmc = B.LMC, classmecca = B.Class, country = B.Country, tipomecca = Type  ";
            query += "FROM #CDRs A JOIN CDRDB.dbo.Regions B ON LEFT(A.DialedNumber,11) = B.Code ";
            query += "WHERE LEN(B.Code) = 11 and A.lmc is null	 ";

            query += "UPDATE #CDRs ";
            query += "SET lmc = B.LMC, classmecca = B.Class, country = B.Country, tipomecca = Type  ";
            query += "FROM #CDRs A JOIN CDRDB.dbo.Regions B ON LEFT(A.DialedNumber,10) = B.Code ";
            query += "WHERE LEN(B.Code) = 10 and A.lmc is null ";

            query += "UPDATE #CDRs ";
            query += "SET lmc = B.LMC, classmecca = B.Class, country = B.Country, tipomecca = Type  ";
            query += "FROM #CDRs A JOIN CDRDB.dbo.Regions B ON LEFT(A.DialedNumber,9) = B.Code ";
            query += "WHERE LEN(B.Code) = 9 and A.lmc is null ";

            query += "UPDATE #CDRs ";
            query += "SET lmc = B.LMC, classmecca = B.Class, country = B.Country, tipomecca = Type  ";
            query += "FROM #CDRs A JOIN CDRDB.dbo.Regions B ON LEFT(A.DialedNumber,8) = B.Code ";
            query += "WHERE LEN(B.Code) = 8 and A.lmc is null ";

            query += "UPDATE #CDRs ";
            query += "SET lmc = B.LMC, classmecca = B.Class, country = B.Country, tipomecca = Type  ";
            query += "FROM #CDRs A JOIN CDRDB.dbo.Regions B ON LEFT(A.DialedNumber,7) = B.Code ";
            query += "WHERE LEN(B.Code) = 7 and A.lmc is null ";

            query += "UPDATE #CDRs ";
            query += "SET lmc = B.LMC, classmecca = B.Class, country = B.Country, tipomecca = Type  ";
            query += "FROM #CDRs A JOIN CDRDB.dbo.Regions B ON LEFT(A.DialedNumber,6) = B.Code ";
            query += "WHERE LEN(B.Code) = 6 and A.lmc is null ";

            query += "UPDATE #CDRs ";
            query += "SET lmc = B.LMC, classmecca = B.Class, country = B.Country, tipomecca = Type  ";
            query += "FROM #CDRs A JOIN CDRDB.dbo.Regions B ON LEFT(A.DialedNumber,5) = B.Code ";
            query += "WHERE LEN(B.Code) = 5 and A.lmc is null ";

            query += "UPDATE #CDRs ";
            query += "SET lmc = B.LMC, classmecca = B.Class, country = B.Country, tipomecca = Type  ";
            query += "FROM #CDRs A JOIN CDRDB.dbo.Regions B ON LEFT(A.DialedNumber,4) = B.Code ";
            query += "WHERE LEN(B.Code) = 4 and A.lmc is null ";

            query += "UPDATE #CDRs ";
            query += "SET lmc = B.LMC, classmecca = B.Class, country = B.Country, tipomecca = Type  ";
            query += "FROM #CDRs A JOIN CDRDB.dbo.Regions B ON LEFT(A.DialedNumber,3) = B.Code ";
            query += "WHERE LEN(B.Code) = 3 and A.lmc is null ";

            query += "UPDATE #CDRs ";
            query += "SET lmc = B.LMC, classmecca = B.Class, country = B.Country, tipomecca = Type  ";
            query += "FROM #CDRs A JOIN CDRDB.dbo.Regions B ON LEFT(A.DialedNumber,2) = B.Code ";
            query += "WHERE LEN(B.Code) = 2 and A.lmc is null ";

            query += "UPDATE #CDRs ";
            query += "SET lmc = B.LMC, classmecca = B.Class, country = B.Country, tipomecca = Type  ";
            query += "FROM #CDRs A JOIN CDRDB.dbo.Regions B ON LEFT(A.DialedNumber,1) = B.Code ";
            query += "WHERE LEN(B.Code) = 1 and A.lmc is null ";

            query += "update #CDRs ";
            query += "set isPorted = b.carrierdest ";
            query += "from #CDRs a join NP.dbo.portabilidad b on a.Number = b.Number ";

            query += "update #CDRs ";
            query += "set finalLMC =  case when isPorted is null then replace([pn],',','') else replace(isPorted,',','-') end ";
            query += "from #CDRs ";

            query += " update #CDRs ";
            query += " set finalLMC =  '' ";
            query += " from #CDRs Where finalLMC is null ";

            query += " TRUNCATE TABLE DIRECTOREPORTS.DBO.CDRS_COUNT ";
            query += " INSERT INTO DIRECTOREPORTS.DBO.CDRS_COUNT SELECT COUNT (*) Count FROM #CDRS WHERE LMC LIKE '" + lmc.Replace("**ALL**", "%") + "' and tipomecca like '" + type.Replace("**ALL**", "%")  + "' and  finalLMC Like '" + finalLMC.Replace("**ALL**", "%") + "' ";
            
            if (country == "Mexico")
            {
                query += "SELECT TOP 500 [DialedNumber],[Number],[Minutes],[AnsweredCalls],[Country],[LMC],[tipomecca] Type,replace(isPorted,',','-') as 'isPorted',Duration,replace([pn],',','') PN,[PNType],[PNMode],[Customer],[Billingdate],[Stop],[Attempts],[TxNumber],[Vendor],[ISDN_cause_code],c_sip_cause, v_sip_cause,[OriginationIP],[TerminationIP],[PDD],case when isPorted is null then replace([pn],',','') else replace(isPorted,',','-') end 'FinalLMC', TerminateCause, UnexpectedReleaseReason FROM #CDRs ";
            }            
            else
            {
                query += "SELECT TOP 500 BillingDate, DialedNumber, Number, Country, LMC, tipomecca Type, Minutes, AnsweredCalls, Attempts, Duration, Source, Customer, IngressAdjacency, Vendor, EgressAdjacency, TxNumber, ISDN_Cause_Code, c_sip_cause, v_sip_cause, Stop, PDD, OriginationIP, TerminationIP, TerminateCause, UnexpectedReleaseReason FROM #CDRs ";
            }

            if (country == "USA")
            {
                query += "WHERE Country = 'USA' AND LMC LIKE '" + lmc.Replace("**ALL**", "%") + "' and tipomecca like '" + type.Replace("**ALL**", "%") + "' order by stop desc ";
            }
            else if (country == "Canada")
            {
                query += "WHERE Country = 'CANADA' AND LMC LIKE '" + lmc.Replace("**ALL**", "%") + "' and tipomecca like '" + type.Replace("**ALL**", "%") + "' order by stop desc ";
            }
            else if (country == "Dominican Republic")
            {
                query += "WHERE Country = 'DOMINICAN REPUBLIC' AND LMC LIKE '" + lmc.Replace("**ALL**", "%") + "' and tipomecca like '" + type.Replace("**ALL**", "%") + "' order by stop desc ";
            }
            else
            {
                query += "WHERE LMC LIKE '" + lmc.Replace("**ALL**", "%") + "' and tipomecca like '" + type.Replace("**ALL**", "%") +  "' And  finalLMC Like '"  + finalLMC.Replace("**ALL**", "%") +  "'  order by stop desc ";
            }

            //Response.Write(query.ToString());
            //Response.End();

            query = query + queryw;

            //INFORMATION
            SqlCommand qryCommand = new SqlCommand();
            qryCommand.CommandText = query;
            DataSet myDataSet = null;
            myDataSet = RunQuery(qryCommand);
            
            //COUNT
            queryc = "SELECT COUNT FROM DIRECTOREPORTS.DBO.CDRS_COUNT ";
            SqlCommand qryCommand2 = new SqlCommand();
            qryCommand2.CommandText = queryc;
            DataSet myDataSet2 = null;
            myDataSet2 = RunQuery(qryCommand2);

            try
            {
                gdvCdrs.DataSource = myDataSet.Tables[0];
                gdvCdrs.DataBind();
                gdvCdrs.Visible = true;
                txtCount.Text = myDataSet2.Tables[0].Rows[0].ItemArray[0].ToString();
            }
            catch (Exception ex )
            {
                string errormessage = myDataSet.HasErrors.ToString() + " " + ex.Message.ToString();
                lblError.Text = errormessage;
                lblError.Visible = true;
            }

            Button2.Visible = true;
            Session["ShowCDRS"] = myDataSet;

            query = "INSERT INTO DirectoReports.dbo.QueryLog VALUES (getdate(),'ShowCDRs.aspx, Customer:" + codeCustomer + ", Vendor:" + vendorId + ", Country: " + country + ", LMC: " + lmc + ", Type: " + type + ", CSIPCause:" + CSIPCause + ", VSIPCause:" + VSIPCause + ", NumberA: " + numbera + ", NumberB: " + numberb + ", Day:" + dpdDay.SelectedItem.ToString() + ", CDRsCount: " + txtCount.Text + ", Reason: " + reason + "','" + user + "','" + mrol + "')";
            qryCommand.CommandText = query;
            RunQuery(qryCommand);

            qryCommand.Connection.Close();
            
        }
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        dbAdapter.SelectCommand.CommandTimeout = 120;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
            
        }

        return resultsDataSet;
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        // Export all the details
        DataTable dtEmployee = ((DataSet)Session["ShowCDRS"]).Tables[0].Copy();
        // Export all the details to CSV
        RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
        string filename = "CDRs" + ".csv";
        objExport.ExportDetails(dtEmployee, Export.ExportFormat.CSV, filename);
    }

    protected void gdvCdrs_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvCdrs.PageIndex = e.NewPageIndex;
        DataSet ds = (DataSet)Session["ShowCDRS"];
        gdvCdrs.DataSource = ds;
        gdvCdrs.DataBind();
    }
}

