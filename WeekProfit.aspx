<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="WeekProfit.aspx.cs" Inherits="WeekProfit" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" EnableEventValidation="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager id="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <asp:Label ID="lblTitle" runat="server" Text="MECCA WEEK PROFIT" CssClass="labelTurkH">
    </asp:Label>
    <br />
    <br />
    <br />
    
    <table style="position:absolute; left:230px;">
        <tr>
            <td>                
                <table >
                    
                    <tr>    
                        <td align="left" style="width: 100px; height: 21px" valign="top">            
                            <asp:Label ID="lblfrom" runat="server" Text="From: " Font-Bold="False" CssClass="labelSteelBlue"></asp:Label>            
                        </td>
                        <td>
                            <asp:TextBox ID="txtfrom" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                        </td>     
                        <td>
                            <asp:Image ID="imgCalFrom" runat="server" ImageUrl="~/Images/calendar1.gif" />
                        </td>   
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px; height: 21px" valign="top">
                            <asp:Label ID="lblto" runat="server" Text="To: " Font-Bold="False" CssClass="labelSteelBlue"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtTo" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Image ID="imgCalTo" runat="server" ImageUrl="~/Images/calendar1.gif" />
                        </td>
                    </tr>
                </table>    
                <table >    
                    <tr>
                        <td>
                        </td>
                         <td>
                        </td>
                    </tr>        
                    <tr>
                        <td align="left" style="width: 100px; height: 21px" valign="top">
                            <asp:Label ID="lblcustomer" runat="server" Text="Customer: " Font-Bold="False" CssClass="labelSteelBlue"></asp:Label>            
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCustomer" runat="server" DataSourceID="sdldsCustomer" DataTextField="Customer" DataValueField="Customer" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" Width="300px" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px; height: 21px" valign="top">
                            <asp:Label ID="lblvendor" runat="server" Text="Vendor: " Font-Bold="False" CssClass="labelSteelBlue"></asp:Label>            
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlVendor" runat="server" DataSourceID="sdldsVendor" DataTextField="Vendor" DataValueField="Vendor" OnSelectedIndexChanged="ddlCVendor_SelectedIndexChanged" Width="300px" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px; height: 21px" valign="top" colspan="2">
                            <asp:RadioButtonList ID="rboption" runat="server" CssClass="labelBlue8" Width="80px" Visible="true" RepeatDirection="Horizontal">
                                <asp:listitem Text="Profit" Value="Profit" Selected="True"/>
                                <asp:listitem Text="Minutes" Value="Minutes" />                    
                                <asp:listitem Text="Sales" Value="Sales" /> 
                                <asp:listitem Text="Cost" Value="Cost" />                                 
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
                <table >
                   <tr>
                        <td>                &nbsp;</td>
                   </tr>
                   <tr>
                        <td>    
                            <asp:Button ID="btnView" runat="server" OnClick="btnView_Click" Text="View Report" CssClass="boton" />            
                        </td>
                        <td>    
                            <asp:Button ID="btnExp" runat="server" OnClick="btnExp_Click" Text="Export Excel" CssClass="boton" />            
                        </td>
                   </tr>
                   <tr>
                        <td>
                            <asp:SqlDataSource ID="sdldsCustomer" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT '**NONE**' AS Customer FROM CDRDB.dbo.ProviderIP UNION SELECT '**ALL**' AS Customer FROM CDRDB.dbo.ProviderIP UNION SELECT DISTINCT ProviderName AS Customer FROM CDRDB.dbo.ProviderIP AS ProviderIP_2 WHERE (Type = 'C') UNION SELECT 'NIP-CALLING-CARD' AS Customer  " ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdldsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT DISTINCT ProviderName AS Vendor FROM CDRDB.dbo.ProviderIP WHERE (Type LIKE 'V') AND (ProviderName <> 'OUTLANDER-AMIGO') AND (ProviderName <> 'OUTLANDER-CALL') AND (ProviderName <> 'OUTLANDER-MS') AND (ProviderName <> 'OUTLANDER-PLAN') UNION SELECT '**ALL**' AS Vendor UNION SELECT 'NIP-CALLING-CARD' AS Vendor UNION SELECT '**NONE**' AS Vendor">
                            </asp:SqlDataSource>
                            <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtfrom" runat="server" PopupButtonID="imgCalFrom">
                            </cc1:CalendarExtender>

                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtto" PopupButtonID="imgCalTo">
                </cc1:CalendarExtender>
                        </td>
                   </tr>
                </table>
                <table>
                    <tr>
                        <td>
                             <asp:GridView ID="GridView1" runat="server" Font-Names="Arial" Font-Size="9pt" AllowPaging="True" 
                                    OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="50" AutoGenerateColumns="false" EmptyDataText='<font size="4pt" color="red"><strong>Not Found...</strong></font>' Visible="False">
                                    <HeaderStyle CssClass="titleOrangegrid" Height="40px" Width="150px" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <AlternatingRowStyle BackColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                </asp:GridView>
                        </td>
                    </tr>
                </table>    
            </td>
        </tr>
    </table>

</asp:Content>

