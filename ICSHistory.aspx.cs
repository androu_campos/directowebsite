using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class ICSHistory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TextBox1.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            TextBox2.Text = DateTime.Now.AddDays(-1).ToShortDateString();

            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandType = CommandType.Text;

            string sql = "";

            sql = "SELECT PROVIDERNAME FROM CDRDB..PROVIDERIP WHERE LEN(IP) = 4 AND TYPE = 'C' AND PROVIDERNAME LIKE 'ICS%'";

            sqlQry.CommandText = sql;
            sqlQry.CommandTimeout = 180;
            DataSet myDataset = RunQuery(sqlQry);

            if (myDataset.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < myDataset.Tables[0].Rows.Count; i++)
                {
                    ListBox1.Items.Add(myDataset.Tables[0].Rows[i][0].ToString());
                }
            }



            ListBox1.Items.Add("ICS-FIRSTU2");
            ListBox1.Items.Add("ICS-FIRSTU5");
            //ListBox1.Items.Add("ICS-ALL-CARDS");
            ListBox1.Items.Add("ALLACCESSPH-PINLESS");
            ListBox1.Items.Add("ALL-ACCESS-FLAT-MINS");
            ListBox1.Items.Add("ALL-ACCESS-CALL");
            

        }
    }

    protected void cmdShow_Click(object sender, EventArgs e)
    {

        SqlCommand sqlQry = new SqlCommand();
        sqlQry.CommandType = CommandType.Text;



        string sql = "";
        string name = "";

        foreach (ListItem i in ListBox1.Items)
        {
            name += "'" + i.Text + "',";
        }

        name = name.Substring(0, name.Length - 1);


        sql = "SELECT CONVERT(VARCHAR(11) ,T.Billingdate,101) as BillingDate,T.Customer,T.Vendor,";
        sql += "T.Country,T.Region,";
        sql += "T.Type,T.LMC,T.Class,ROUND(SUM(T.CustMinutes),4) as CMinutes, ";
        sql += "ROUND(SUM(T.VendorMinutes),4) as VMinutes, ";
        sql += "SUM(T.tCalls) as Attempts,SUM(T.BillableCalls) as AnsweredCalls,SUM(T.RA) as [Rejected Calls],ROUND(SUM(TotalPrice),2) as Price, ";
        sql += "mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR, ";
        sql += "mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR, ";
        sql += "mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD ";
        sql += "FROM MECCA2..trafficlog T ";
        sql += "JOIN CDRDB..ProviderIP p ON ";
        sql += "p.ProviderName = t.Customer  JOIN CDRDB..ProviderIP p2 ON p2.ProviderName = t.Vendor ";
        sql += "WHERE BillingDate BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' AND LEN(p.IP) = 4 AND ";
        sql += "p.Type = 'C' AND LEN(p2.IP) = 4 AND p2.Type = 'V'  AND T.Customer in (" + name + ")";
        sql += "GROUP BY T.BillingDate,T.Customer,T.Vendor,T.Country,T.Region, ";
        sql += "T.Type,T.LMC,T.Class ";
        sql += "HAVING SUM(CustMinutes) > 0 ";
        sql += "UNION SELECT '** ALL **' AS BillingDate,'****' AS Customer,'****' AS Vendor, ";
        sql += "'****' as Country,'****' as Region, ";
        sql += "'****' as [Type],'****' as LMC,'****' as Class, ";
        sql += "ROUND(SUM(T.CustMinutes),4) as CMinutes,ROUND(SUM(T.VendorMinutes),4) as VMinutes, ";        
        sql += "SUM(T.tCalls) as Attempts, ";
        sql += "SUM(T.BillableCalls) as AnsweredCalls,SUM(T.RA) as [Rejected Calls],ROUND(SUM(TotalPrice),2) as Price, ";
        sql += "mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR, ";
        sql += "mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR, ";
        sql += "mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD ";
        sql += "FROM MECCA2..trafficlog T ";
        sql += "JOIN ";
        sql += "CDRDB..ProviderIP p ON p.ProviderName = t.Customer  JOIN ";
        sql += "CDRDB..ProviderIP p2 ON p2.ProviderName = t.Vendor  ";
        sql += "WHERE BillingDate BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' AND LEN(p.IP) = 4 AND p.Type = 'C' AND ";
        sql += "LEN(p2.IP) = 4 AND p2.Type = 'V' AND T.Customer in (" + name + ")";
        sql += "HAVING SUM(CustMinutes) > 0  ORDER BY   BillingDate,T.Customer,T.Vendor, ";
        sql += "T.Country,T.Region,T.Type, ";
        sql += "T.LMC,T.Class ";



        sqlQry.CommandText = sql;

        sqlQry.CommandTimeout = 180;

        DataSet myDataset = RunQuery(sqlQry);
        Session["ICSHistory"] = myDataset;
        if (myDataset.Tables[0].Rows.Count > 0)
        {
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            cmdExport.Visible = true;
            Label4.Visible = false;
        }
        else
        {
            cmdExport.Visible = false;
            Label4.Visible = true;
        }

        //sql = "SELECT CONVERT(VARCHAR(11) ,T.Billingdate,101) as BillingDate,T.Customer,";        
        //sql += "T.Type,ROUND(SUM(T.CustMinutes),4) as CMinutes, ";        
        //sql += "SUM(T.tCalls) as Attempts,SUM(T.BillableCalls) as AnsweredCalls,SUM(T.RA) as [Rejected Calls], ";
        //sql += "mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR, ";
        //sql += "mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR, ";
        //sql += "mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD ";
        //sql += "FROM MECCA2..trafficlog T ";
        //sql += "JOIN CDRDB..ProviderIP p ON ";
        //sql += "p.ProviderName = t.Customer  JOIN CDRDB..ProviderIP p2 ON p2.ProviderName = t.Vendor ";
        //sql += "WHERE BillingDate BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' AND LEN(p.IP) = 4 AND ";
        //sql += "p.Type = 'C' AND LEN(p2.IP) = 4 AND p2.Type = 'V'  AND T.Customer in (" + name + ")";
        //sql += "GROUP BY T.BillingDate,T.Customer, ";
        //sql += "T.Type ";
        //sql += "HAVING SUM(CustMinutes) > 0 ";
        //sql += "UNION SELECT '** ALL **' AS BillingDate,'****' AS Customer, ";        
        //sql += "'****' as [Type], ";
        //sql += "ROUND(SUM(T.CustMinutes),4) as CMinutes, ";
        //sql += "SUM(T.tCalls) as Attempts, ";
        //sql += "SUM(T.BillableCalls) as AnsweredCalls,SUM(T.RA) as [Rejected Calls], ";
        //sql += "mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR, ";
        //sql += "mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR, ";
        //sql += "mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD ";
        //sql += "FROM MECCA2..trafficlog T ";
        //sql += "JOIN ";
        //sql += "CDRDB..ProviderIP p ON p.ProviderName = t.Customer  JOIN ";
        //sql += "CDRDB..ProviderIP p2 ON p2.ProviderName = t.Vendor  ";
        //sql += "WHERE BillingDate BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' AND LEN(p.IP) = 4 AND p.Type = 'C' AND ";
        //sql += "LEN(p2.IP) = 4 AND p2.Type = 'V' AND T.Customer in (" + name + ")";
        //sql += "HAVING SUM(CustMinutes) > 0  ORDER BY   BillingDate,T.Customer, ";
        //sql += "T.Type ";

        sql = "SELECT CONVERT(VARCHAR(11) ,T.Billingdate,101) as BillingDate,T.Customer, T.Vendor,";
        sql += "T.Type,ROUND(SUM(T.TotalCost),4) as Cost,ROUND(SUM(T.CustMinutes),4) as CMinutes, SUM(T.tCalls) as Attempts,SUM(T.BillableCalls) as AnsweredCalls,SUM(T.RA) as [Rejected Calls], ";
                  sql += "mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR, mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR, ";
                  sql += "mecca2.dbo.fGetACD(SUM(T.CustMinutes), SUM(T.BillableCalls)) as ACD ";
                  sql += "FROM MECCA2..trafficlog T ";
                  sql += "JOIN CDRDB..ProviderIP p ON ";
                  sql += "p.ProviderName = t.Customer  JOIN CDRDB..ProviderIP p2 ON p2.ProviderName = t.Vendor  ";
                  sql += "WHERE BillingDate BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' AND LEN(p.IP) = 4 AND  ";
                  sql += "  p.Type = 'C' AND LEN(p2.IP) = 4 AND p2.Type = 'V'  AND T.Customer in (" + name + ")";
                  sql += "  GROUP BY T.BillingDate,T.Customer,T.Vendor, T.Type  ";
                  sql += "  HAVING SUM(CustMinutes) > 0  ";
                  sql += "  UNION  ";
                  sql += "  SELECT CONVERT(VARCHAR(11) ,T.Billingdate,101) as BillingDate,'-' as [Customer],T.Vendor ,  ";
                  sql += "  ISNULL(T.Type,'TBD') as [Type],ROUND(SUM(T.TotalCost),4) as Cost,ROUND(SUM(ISNULL(T.VendorMinutes,0)),4) as CMinutes,  ";        
                  sql += "  SUM(T.tCalls) as Attempts,SUM(T.BillableCalls) as AnsweredCalls,SUM(T.RA) as [Rejected Calls],  ";
                  sql += "  mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,  ";
                  sql += "  mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR,  ";
                  sql += "  mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD  ";
                  sql += "  FROM MECCA2..trafficlog T  ";
                  sql += "  JOIN CDRDB..ProviderIP p ON  ";
                  sql += "  p.ProviderName = t.Customer  JOIN CDRDB..ProviderIP p2 ON p2.ProviderName = t.Vendor  ";
                  sql += "  WHERE BillingDate BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' AND LEN(p.IP) = 4 AND  ";
                  sql += "  p.Type = 'C' AND LEN(p2.IP) = 4 AND p2.Type = 'V'  AND T.Vendor like 'REJECTED%' and T.Customer in (" + name + ")";
                  sql += "  GROUP BY T.BillingDate,T.Vendor,ISNULL(T.Type,'TBD')  ";
                  sql += "  UNION  ";
                  sql += "  SELECT '** ALL **' AS BillingDate,'****' AS Customer, '****' AS Vendor,'****' as [Type],  ";
                  sql += "         SUM(Cost) as Cost,SUM(CMinutes) as CMinutes, SUM(Attempts) as Attempts,  ";
	              sql += "         SUM(AnsweredCalls) as AnsweredCalls, SUM([Rejected Calls]) as [Rejected Calls],  ";
	              sql += "         SUM(ASR) as ASR, SUM(ABR) as ABR, SUM(ACD) as ACD  ";
                  sql += "  FROM (  ";
                  sql += "  SELECT ROUND(SUM(T.TotalCost),4) as [Cost],ROUND(SUM(T.CustMinutes),4) as CMinutes, SUM(T.tCalls) as Attempts,  ";
                  sql += "  SUM(T.BillableCalls) as AnsweredCalls,SUM(T.RA) as [Rejected Calls],  ";
                  sql += "  mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,  ";
                  sql += "  mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR,  ";
                  sql += "  mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD  ";
                  sql += "  FROM MECCA2..trafficlog T JOIN  ";
                  sql += "  CDRDB..ProviderIP p ON p.ProviderName = t.Customer  JOIN  ";
                  sql += "  CDRDB..ProviderIP p2 ON p2.ProviderName = t.Vendor   ";
                  sql += "  WHERE BillingDate BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' AND LEN(p.IP) = 4 AND p.Type = 'C' AND  ";
                  sql += "  LEN(p2.IP) = 4 AND p2.Type = 'V' AND T.Customer in (" + name + ")";
                  sql += "  UNION  ";
                  sql += "  SELECT ROUND(SUM(T.TotalCost),4) as [Cost],ROUND(SUM(T.CustMinutes),4) as CMinutes, SUM(T.tCalls) as Attempts,  ";
                  sql += "  SUM(T.BillableCalls) as AnsweredCalls,SUM(T.RA) as [Rejected Calls],  ";
                  sql += "  mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR,  ";
                  sql += "  mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR,  ";
                  sql += "  mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD  ";
                  sql += "  FROM MECCA2..trafficlog T JOIN  ";
                  sql += "  CDRDB..ProviderIP p ON p.ProviderName = t.Customer  JOIN  ";
                  sql += "  CDRDB..ProviderIP p2 ON p2.ProviderName = t.Vendor   ";
                  sql += "  WHERE BillingDate BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' AND LEN(p.IP) = 4 AND  ";
                  sql += "  p.Type = 'C' AND LEN(p2.IP) = 4 AND p2.Type = 'V'  AND T.Vendor like 'REJECTED%' and T.Customer in (" + name + ")";
                  sql += "  ) a  ";

        sqlQry.CommandText = sql;

        sqlQry.CommandTimeout = 180;

        myDataset = RunQuery(sqlQry);
        Session["ICSHistCompact"] = myDataset;
        if (myDataset.Tables[0].Rows.Count > 0)
        {
            GridView2.DataSource = myDataset.Tables[0];
            GridView2.DataBind();
            cmdExportC.Visible = true;
            Label4.Visible = false;
        }
        else
        {
            cmdExportC.Visible = false;
            Label4.Visible = true;
        }
        

    }

    protected void cmdAdd(object sender, EventArgs e)
    {
        ListBox1.Items.Add(dpdProvider.Text);
    }

    protected void cmdDel(object sender, EventArgs e)
    {
        ListBox1.Items.Remove(ListBox1.SelectedItem);
    }

    protected void cmdReset(object sender, EventArgs e)
    {
        ListBox1.Items.Clear();
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

        return resultsDataSet;
    }
    protected void cmdExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {
            DataTable dtEmployee = ((DataSet)Session["ICSHistory"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "ICSHistoryReport" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

    }

    protected void cmdExportC_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {
            DataTable dtEmployee = ((DataSet)Session["ICSHistCompact"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "ICSHistCompact" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = Session["ICSHistory"];
        GridView1.DataBind();
    }

    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        GridView2.DataSource = Session["ICSHistCompact"];
        GridView2.DataBind();
    }
}
