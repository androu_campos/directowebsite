<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="ManagesGroup.aspx.cs" Inherits="MasterAccess"
    Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager id="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="position:absolute; top:220px; left:230px;">
        <tr>
            <td style="width: 100px">
    
    
        
            <table align="left" style="width: 349px">
                <tr>
                    <td align="left">
                    <asp:Label ID="Label7" runat="server" Text="ManagePlanSim" CssClass="labelTurk"></asp:Label>
                        </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            <tr> <td align="left"><asp:Label ID="Label3" runat="server" Text="Create Group" CssClass="labelTurk" Width="100px"></asp:Label></td> <td></td> <td></td> </tr>
                <tr>
                    <td align="left" style="height: 22px">
                    </td>
                    <td style="height: 22px">
                    </td>
                    <td style="height: 22px">
                    </td>
                </tr>
            <tr> <td bgcolor="#d5e3f0"><asp:Label ID="Label1" runat="server" Text="Name:" CssClass="labelTurk"></asp:Label></td> <td align="left"><asp:TextBox ID="txtName" runat="server" CssClass="labelSteelBlue" Width="148px"></asp:TextBox></td> <td align="left"></td> </tr>
            <tr> <td bgcolor="#d5e3f0"><asp:Label ID="Label2" runat="server" Text="Rate:" CssClass="labelTurk"></asp:Label></td> <td align="left"><asp:TextBox ID="txtRate" runat="server" CssClass="labelSteelBlue" Width="148px"></asp:TextBox></td> <td align="left"></td> </tr>
            <tr> <td></td> <td></td> <td align="left"><asp:Button ID="cmdInsert" runat="server" Text="Create" CssClass="boton" OnClick="cmdInsert_Click" /></td> </tr>
            <tr> <td></td> <td><asp:Label ID="lblAfter" runat="server" CssClass="labelTurk"></asp:Label></td> <td align="left"></td> </tr>
            </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
    <table>
        
        <tr>
            <td style="width: 91px" align="left">
                <asp:Label ID="Label5" runat="server" CssClass="labelTurk" Text="Delete Group" Width="100px"></asp:Label></td>
            <td style="width: 95px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 91px; height: 22px">
            </td>
            <td style="width: 95px; height: 22px">
            </td>
            <td style="width: 100px; height: 22px">
            </td>
        </tr>
        <tr>
            <td style="width: 91px; height: 22px;" align="right" bgcolor="#d5e3f0">
                <asp:Label ID="Label6" runat="server" Text="Select Group:" CssClass="labelTurk"></asp:Label></td>
            <td style="width: 95px; height: 22px;" align="left">
                <asp:DropDownList ID="DropDownList1" runat="server" DataTextField="Name" CssClass="dropdown" DataValueField="Name">
                </asp:DropDownList></td>
            <td style="width: 100px; height: 22px;" align="left">
                <asp:Button ID="cmdDelete" runat="server" CssClass="boton" OnClick="cmdDelete_Click"
                    Text="Delete" /></td>
        </tr>
        <tr>
            <td style="width: 91px; height: 29px">
            </td>
            <td align="right" style="width: 95px; height: 29px">
            </td>
            <td style="width: 100px; height: 29px">
            </td>
        </tr>
        <tr>
            <td style="width: 91px">
            </td>
            <td align="left" colspan="2">
                <asp:Label ID="lblDelete" runat="server" CssClass="labelTurk" Text="Label" Visible="False"></asp:Label></td>
        </tr>
    </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
    
    
        
        
     <table align="left">
                <tr>
                    <td style="width: 168px" align="left">
                        <asp:Label ID="lblHeaderEdit" runat="server" CssClass="labelTurk" Text="Edit Group" Width="100px"></asp:Label></td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 168px; height: 25px">
                    </td>
                    <td style="width: 100px; height: 25px">
                    </td>
                    <td style="width: 100px; height: 25px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 168px; height: 22px;" bgcolor="#d5e3f0">
                        <asp:Label ID="lblSelectGroup" runat="server" CssClass="labelTurk" Text="Select Group:"></asp:Label></td>
                    <td style="width: 100px; height: 22px;">
                        <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" DataTextField="Name" DataValueField="Name" CssClass="dropdown">
                        </asp:DropDownList></td>
                    <td style="width: 100px; height: 22px;" align="left">
                        <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="Button1_Click"
                            Text="Show data" Visible="false" /></td>
                </tr>
                <tr>
                    <td style="width: 168px">
                    </td>
                    <td style="width: 100px" align="right">
                        </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 168px" bgcolor="#d5e3f0">
                        <asp:Label ID="lblEditName" runat="server" CssClass="labelTurk" Text="Name:"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtNewName" runat="server" CssClass="labelSteelBlue" Width="148px"></asp:TextBox></td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 168px" bgcolor="#d5e3f0">
                        <asp:Label ID="lblEditRate" runat="server" CssClass="labelTurk" Text="Rate:"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtNewRate" runat="server" CssClass="labelSteelBlue" Width="148px"></asp:TextBox></td>
                    <td style="width: 100px">
                        <asp:Button ID="cmdUpdate" runat="server" CssClass="boton" OnClick="cmdUpdate_Click"
                            Text="Update" /></td>
                </tr>
                <tr>
                    <td style="width: 168px">
                    </td>
                    <td align="left" style="width: 100px">
                        </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
            <td style="width: 168px">
            </td>
            <td align="right" style="width: 100px">
                <asp:Label ID="Label4" runat="server" Text="Label" CssClass="labelTurk" Visible="False"></asp:Label></td>
            <td style="width: 100px">
            </td>
        </tr>
            </table>    
            </td>
        </tr>
    </table>
    <br />
    <br />
    
     <asp:SqlDataSource ID="SqlDistinctNames" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
                OnLoad="SqlDistinctNames_Load" SelectCommand="SELECT DISTINCT Name FROM PlanBreakOut">
            </asp:SqlDataSource>    
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
        SelectCommand="SELECT DISTINCT Name FROM PlanBreakOut">
    </asp:SqlDataSource>

    
    

    
</asp:Content>
