<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="lcrdetail.aspx.cs" Inherits="lcrdetail" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="left" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td style="width: 100px" align="left">
                <asp:Label ID="Label1" runat="server" CssClass="labelSteelBlue" Text="Volumes:"></asp:Label></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px">
                <asp:Button ID="cmdXport" runat="server" CssClass="boton" Text="Export to EXCEL" OnClick="cmdXport_Click" /></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <asp:GridView ID="grdVolumes" AutoGenerateColumns="false" runat="server" PageSize="250" OnPageIndexChanging="grdVolumes_PageIndexChanging">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="9pt" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                    <asp:BoundField DataField="Vendor" HeaderText="Vendor">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    
                    <asp:BoundField DataField="cost" HeaderText="Cost" DataFormatString="${0:f4}" HtmlEncode="false">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="TotalMinutes" HeaderText="TotalMinutes" HtmlEncode="False" DataFormatString="{0:0,0}">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="BillableCalls" HeaderText="BillableCalls" HtmlEncode="False" DataFormatString="{0:0,0}">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px"/>
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <asp:Label ID="lblNothingV" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="Nothing found !!"
                    Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" style="width: 100px; height: 53px" valign="bottom">
                <asp:Label ID="Label2" runat="server" CssClass="labelSteelBlue" Text="LCR:"></asp:Label></td>
            <td style="width: 100px; height: 53px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px; height: 42px;" valign="bottom">
                <asp:Button ID="cmdExport2" runat="server" CssClass="boton" OnClick="Button1_Click"
                    Text="Export to EXCEL" Width="81px" /></td>
            <td style="width: 100px; height: 42px;">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:GridView ID="grdLCR" AutoGenerateColumns="false" runat="server" AllowPaging="True" PageSize="250" OnPageIndexChanging="grdLCR_PageIndexChanging">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Prefix" HeaderText="Prefix">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="Country" HeaderText="Country">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Region" HeaderText="Region">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Type" HeaderText="Type">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Provider1" HeaderText="Provider1">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Cost1" HeaderText="Cost1" HtmlEncode="False" DataFormatString="${0:f4}">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Provider2" HeaderText="Provider2">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Cost2" HeaderText="Cost2" HtmlEncode="False" DataFormatString="${0:f4}">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Provider2" HeaderText="Provider3">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Cost2" HeaderText="Cost3" HtmlEncode="False" DataFormatString="${0:f4}">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Provider2" HeaderText="Provider4">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Cost2" HeaderText="Cost4" HtmlEncode="False" DataFormatString="${0:f4}">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    
                    <asp:BoundField DataField="Provider2" HeaderText="Provider5">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Cost2" HeaderText="Cost5" HtmlEncode="False" DataFormatString="${0:f4}">
                     <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    
                    </Columns>
                </asp:GridView>
                <asp:Label ID="lblNothingLCR" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="Nothing found"
                    Visible="False"></asp:Label></td>
        </tr>
         <tr>
            <td width="100%" colspan="2">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

