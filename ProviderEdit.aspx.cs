using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class ProviderEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Page.IsPostBack)
        {

            

        }
        else
        {
            //cdrdbDatasetTableAdapters.InvoiceTableAdapter adp = new cdrdbDatasetTableAdapters.InvoiceTableAdapter();
            //cdrdbDataset.InvoiceDataTable tbl = adp.GetData();
            //for (int i = 0; i < tbl.Rows.Count; i++)
            //{
            //    dpdProvider.Items.Add(tbl.Rows[i]["MeccaName"].ToString());
            //}
        }

    }
  

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        int flag = 0,due;
        string qry="";
        try
        {
            //cdrdbDatasetTableAdapters.InvoiceTableAdapter adpN = new cdrdbDatasetTableAdapters.InvoiceTableAdapter();
            //cdrdbDataset.InvoiceDataTable tblN = adpN.GetDataByMeccaName(dpdProvider.SelectedValue.ToString());
            //validation

            if (txtDueDate.Text.Length == 0)
            {
                lblUpdate.Visible = true;
                lblUpdate.Text = "Cheak Due Date";
                flag = 1;
            }
            else
            {
                try
                {
                    due = Convert.ToInt32(txtDueDate.Text);
                }
                catch
                {
                    lblUpdate.Visible = true;
                    lblUpdate.Text = "Cheak Due Date";
                    flag = 1;
                }
            }

            if (txtNum.Text.Length == 0)
            {
                lblUpdate.Visible = true;
                lblUpdate.Text = "Cheak NumInvoice";
                flag = 1;
            }
            else
            {
                try
                {
                    due = Convert.ToInt32(txtNum.Text);
                }
                catch
                {
                    lblUpdate.Visible = true;
                    lblUpdate.Text = "Cheak NumInvoice";
                    flag = 1;
                }
            }

            if (flag == 0)
            {
                if (Button1.Text == "Update")
                {
                    qry = "UPDATE CDRDB..Invoice SET BussinesName='" + txtBussines.Text + "',Address='" + txtAddress.Text + "',City='" + txtCity.Text + "',ZipCode='" + txtZipCode.Text + "',ST='" + txtST.Text + "',Phone='" + txtPhone.Text + "',DueDate='" + txtDueDate.Text + "',PrefixInvoice='" + txtPrefix.Text + "',NumInvoice='" + txtNum.Text + "',Tip='" + DropDownList1.SelectedValue.ToString() + "' WHERE MeccaName = '" + dpdProvider.SelectedValue.ToString() + "'";
                    lblUpdate.Text = "Customer Updated";
                    lblUpdate.Visible = true;
                }
                else if (Button1.Text == "Create")
                {
                    qry = "INSERT INTO [CDRDB].[dbo].[Invoice] VALUES (" +
                    "'" + txtBussines.Text
                    + "','" + ddlNew.SelectedValue.ToString()
                    + "','" + txtAddress.Text
                    + "','" + txtCity.Text
                    + "','" + txtZipCode.Text
                    + "','" + txtST.Text
                    + "','" + txtPhone.Text
                    + "','" + txtDueDate.Text
                    + "','" + txtPrefix.Text
                    + "','" + txtNum.Text
                    + "','" + DropDownList1.SelectedValue.ToString() + "')"; 
                    
                    lblUpdate.Text = "Customer Created";
                    lblUpdate.Visible = true;
                }

                SqlCommand sqlSpQuery2 = new SqlCommand();
                sqlSpQuery2.CommandType = System.Data.CommandType.Text;
                sqlSpQuery2.CommandText = qry;
                RunQuery(sqlSpQuery2);
            }   

        }
        catch(Exception ex)
        {
            string message = ex.Message.ToString();
        }
        
    }
    protected void dpdProvider_SelectedIndexChanged(object sender, EventArgs e)
    {
        //llenar los textbox aki
        //ddlNew.Enabled = false;
        lblUpdate.Visible = false;

        string customer = dpdProvider.SelectedValue.ToString();
        txtAddress.Text = "";
        txtBussines.Text = "";
        txtCity.Text = "";
        txtDueDate.Text = "";
        txtMecca.Text = "";
        txtNum.Text = "";
        txtPhone.Text = "";
        txtPrefix.Text = "";
        txtST.Text = "";        
        txtZipCode.Text = "";
        cdrdbDatasetTableAdapters.InvoiceTableAdapter adpN = new cdrdbDatasetTableAdapters.InvoiceTableAdapter();
        cdrdbDataset.InvoiceDataTable tblN = adpN.GetDataByMeccaName(dpdProvider.SelectedValue.ToString());
        txtBussines.Text = tblN.Rows[0]["BussinesName"].ToString();
        txtBussines.Visible = true;
        txtMecca.Text = tblN.Rows[0]["MeccaName"].ToString();
        txtMecca.Visible = true;
        txtMecca.ReadOnly = true;
        txtAddress.Text = tblN.Rows[0]["Address"].ToString();
        txtAddress.Visible = true;
        txtCity.Text = tblN.Rows[0]["City"].ToString();
        txtCity.Visible = true;
        txtZipCode.Text = tblN.Rows[0]["ZipCode"].ToString();
        txtZipCode.Visible = true;
        txtST.Text = tblN.Rows[0]["ST"].ToString();
        txtST.Visible = true;
        txtPhone.Text = tblN.Rows[0]["Phone"].ToString();
        txtPhone.Visible = true;
        txtDueDate.Text = tblN.Rows[0]["DueDate"].ToString();
        txtDueDate.Visible = true;
        txtPrefix.Text = tblN.Rows[0]["PrefixInvoice"].ToString();
        txtPrefix.Visible = true;
        txtNum.Text = tblN.Rows[0]["NumInvoice"].ToString();
        txtNum.Visible = true;
        if (tblN.Rows[0]["Tip"].ToString() == "1")
        {
            DropDownList1.SelectedIndex = 0;
        }
        if (tblN.Rows[0]["Tip"].ToString() == "2")
        {
            DropDownList1.SelectedIndex = 1;
        }
        if (tblN.Rows[0]["Tip"].ToString() == "3")
        {
            DropDownList1.SelectedIndex = 2;
        }
        

        Label2.Visible = true; Label3.Visible = true; Label4.Visible = true; Label5.Visible = true;
        Label6.Visible = true; Label7.Visible = true; Label8.Visible = true; Label9.Visible = true;
        Label10.Visible = true; Label11.Visible = true; Label12.Visible = true;


        Button1.Text = "Update";
        Button1.Visible = true;
        DropDownList1.Visible = true;
        txtNum.Enabled = false;
        lblUpdate.Visible = false;
        sqldsNew.SelectCommand = "SELECT ' Choose Customer' AS MeccaName UNION SELECT DISTINCT ProviderName as MeccaName FROM ProviderIp WHERE ProviderName  NOT IN (SELECT MeccaName from invoice) AND [Type] = 'C' Order By MeccaName";
        sqldsNew.DataBind();
    }
    protected void ddlNew_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblUpdate.Visible = false;
        //dpdProvider.Enabled = false;
        Button1.Text = "Create";
        Button1.Visible = true;
        DropDownList1.Visible = true;
        txtAddress.Text = "";
        txtBussines.Text = "";
        txtCity.Text = "";
        txtDueDate.Text = "";
        txtMecca.Text = ddlNew.SelectedValue.ToString();
        txtNum.Text = "";
        txtPhone.Text = "";
        txtPrefix.Text = "";
        txtST.Text = "";
        txtZipCode.Text = "";
        sqldsUpdate.SelectCommand = "SELECT ' Choose Customer' AS MeccaName UNION SELECT MeccaName from invoice Order By MeccaName";
        sqldsUpdate.DataBind();


        txtBussines.Visible = true;
        txtMecca.Visible = true;
        txtMecca.ReadOnly = true;
        txtAddress.Visible = true;
        txtCity.Visible = true;
        txtZipCode.Visible = true;
        txtST.Visible = true;
        txtPhone.Visible = true;
        txtDueDate.Visible = true;
        txtPrefix.Visible = true;
        txtNum.Visible = true;
        
        Label2.Visible = true; Label3.Visible = true; Label4.Visible = true; Label5.Visible = true;
        Label6.Visible = true; Label7.Visible = true; Label8.Visible = true; Label9.Visible = true;
        Label10.Visible = true; Label11.Visible = true; Label12.Visible = true;
    }
}
