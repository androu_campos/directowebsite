<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="NationalReport.aspx.cs" Inherits="NationalReport" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" EnableEventValidation="true"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
    <tr>
    <table align="left">
    <tr>
    <td style="width: 577px" colspan = "3">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="National Report"
                    Width="235px"></asp:Label></td>
    </tr>
        <tr>
            
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 577px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 577px; height: 28px;" align="right" valign="top">
                &nbsp; &nbsp; &nbsp;<asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="from:"></asp:Label>
                &nbsp; &nbsp;
            </td>
            <td style="width: 100px; height: 28px;" align="left" valign="top">
                <asp:TextBox ID="TextBox1" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td style="width: 100px; height: 28px;" valign="top">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td style="width: 100px; height: 28px;" valign="top">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="to:"></asp:Label></td>
            <td align="left" style="width: 100px; height: 28px;" valign="top">
                <asp:TextBox ID="TextBox2" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td align="left" style="width: 100px; height: 28px;" valign="top">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td align="left" style="width: 100px; height: 28px;" valign="top">
                <asp:Button ID="cmdShow" runat="server" CssClass="boton" OnClick="cmdShow_Click"
                    Text="View report" /></td>
        </tr>        
        <tr>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
                <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="cmdAdd"
                    Text="Add" />
            </td>
        </tr>
        <tr>
            <td style="width: 577px" valign="top" align="right">
                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Vendor:"></asp:Label></td>            
            <td align="left" style="width: 100px" valign="top">
                <asp:DropDownList ID="dpdProvider" runat="server" CssClass="dropdown" DataSourceID="sqldsCust" DataTextField="Customer" DataValueField="Customer">
                </asp:DropDownList>
            </td>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
                
            </td>
            <td align="left" style="width: 100px" valign="top">
               
            </td>
            <td align="left" style="width: 100px" valign="top">
                
            </td>
            <td align="left" style="width: 100px" valign="top">
                <asp:Button ID="buttonDel" runat="server" CssClass="boton" OnClick="cmdDel"
                    Text="Delete" />
            </td>
        </tr>
        <tr>
            <td style="width: 577px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
                <asp:Button ID="buttonReset" runat="server" CssClass="boton" OnClick="cmdReset"
                    Text="Reset" />
            </td>
        </tr>
        <tr>
            <td style="width: 577px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
                <asp:ListBox ID="ListBox1" runat="server" CssClass="dropdown" Rows="10"></asp:ListBox></td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 577px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 577px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="Nothing found"
                    Visible="False"></asp:Label></td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 577px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
                <asp:Button ID="cmdExport" runat="server" CssClass="boton" OnClick="cmdExport_Click"
                    Text="Export to EXCEL" Visible="False" Width="86px" /></td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 577px">
            </td>
            <td colspan="4" valign="top">
                <%--<asp:GridView ID="GridView1" runat="server" AllowPaging="True" Font-Names="Arial" Font-Size="8pt" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="25">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    
                    
                    
                </asp:GridView>--%>
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 577px">
            </td>
            <td colspan="4" valign="top">
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager><br />
                <cc1:calendarextender id="CalendarExtender1" runat="server" popupbuttonid="Image1"
                    targetcontrolid="TextBox1" Format="MM/dd/yyyy HH:mm"></cc1:calendarextender>
                <cc1:calendarextender id="CalendarExtender2" runat="server" popupbuttonid="Image2"
                    targetcontrolid="TextBox2" Format="MM/dd/yyyy HH:mm"></cc1:calendarextender>
                <asp:SqlDataSource ID="sqldsCust" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT '**ALL**' AS Customer &#13;&#10;FROM CDRDB.dbo.ProviderIP &#13;&#10;UNION &#13;&#10;SELECT DISTINCT ProviderName AS Customer &#13;&#10;FROM CDRDB.dbo.ProviderIP AS ProviderIP_2 &#13;&#10;WHERE (Type = 'C' OR Type = 'D' OR Type='V') &#13;&#10;UNION &#13;&#10;SELECT 'NIP-CALLING-CARD' AS Customer" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
    </tr>
    <tr>
    <table>
        <tr>
            <td colspan="4" valign="top">
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" Font-Names="Arial" Font-Size="8pt" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="25">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    
                    
                    
                </asp:GridView>
            </td>
        </tr>
    </table>
    </tr>
    </table>
    
    
</asp:Content>


