using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;


public partial class LtwGrouped : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
            lblCustomer.Text = makeHtml("Customer%");
            lblVendor.Text = makeHtml("Vendor%");
    }

    private string makeHtml(string type)
    {
        try
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "sp_LtwGrouped";
            sqlCommand.Parameters.Add("type", SqlDbType.Char).Value = type;

            StringBuilder myString = new StringBuilder();
            myString.Append("\n");
            DataTable tbl = Util.RunQueryatCDRDB(sqlCommand).Tables[0];
            foreach (DataRow myRow in tbl.Rows)
            {
                myString.Append("                      <tr>\n");
                if (myRow["Cust_Vendor"].Equals(" **ALL** "))
                {
                    myString.Append("                          <td style=\"font-family:Arial;font-size:9pt;font-weight:bold;\"> **ALL** </td>\n");
                    myString.Append("                          <td style=\"font-family:Arial;font-size:9pt;font-weight:bold;\">" + myRow["TotalCalls"].ToString() + "</td>\n");
                }
                else
                {
                    myString.Append("                          <td style=\"font-family:Arial;font-size:8pt;\">" + myRow["ProviderName"].ToString() + "</td>\n");
                    myString.Append("                          <td style=\"font-family:Arial;font-size:8pt;\">" + myRow["TotalCalls"].ToString() + "</td>\n");
                }
                myString.Append("                      </tr>\n");
            }
            string table = myString.ToString();
            return table;
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            string table = "This table is not available, please try later";
            return table;
            
        }
    }
}
