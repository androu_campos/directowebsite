<%@ Page Language="C#" MasterPageFile="~/MasterPh.master" AutoEventWireup="true" CodeFile="DidGraphsPh.aspx.cs" Inherits="DidGraphsPh" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart1" runat="server" Width="603px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                   <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                         <dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
		                        Name="Title1" Color="26, 59, 105" >
		                 </dcwc:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart2" runat="server" Width="603px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                    <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                         <dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
		                        Name="Title1" Color="26, 59, 105" >
		                 </dcwc:Title>                        
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart3" runat="server" Width="603px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                    <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea  BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                         <dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
		                        Name="Title1" Color="26, 59, 105" >
		                 </dcwc:Title>                        
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart4"  runat="server" Width="603px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                    <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                         <dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
		                        Name="Title1" Color="26, 59, 105" >
		                 </dcwc:Title>                        
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart5"  runat="server" Width="603px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                    <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                         <dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
		                        Name="Title1" Color="26, 59, 105" >
		                 </dcwc:Title>                        
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                <DCWC:Chart ID="Chart6" runat="server" Width="603px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                   <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                         <dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
		                        Name="Title1" Color="26, 59, 105" >
		                 </dcwc:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart7" runat="server" Width="603px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                   <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                         <dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
		                        Name="Title1" Color="26, 59, 105" >
		                 </dcwc:Title>
                    </Titles>
                </DCWC:Chart>
            </td>    
        </tr>
         <tr>
            <td>
                 <DCWC:Chart ID="Chart8" runat="server" Width="603px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                   <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                         <dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
		                        Name="Title1" Color="26, 59, 105" >
		                 </dcwc:Title>
                    </Titles>
                </DCWC:Chart>
            </td>    
        </tr>
         <tr>
            <td>
                 <DCWC:Chart ID="Chart9" runat="server" Width="603px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                   <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                         <dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
		                        Name="Title1" Color="26, 59, 105" >
		                 </dcwc:Title>
                    </Titles>
                </DCWC:Chart>
            </td>    
        </tr>
    </table>

</asp:Content>

