using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RKLib.ExportData;

public partial class Showsalesmpop1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            try
            {
                string customer = Session["dpdCustomer"].ToString();
                string vendor = Session["dpdVendor"].ToString();
                string from = Session["from"].ToString();
                string to = Session["to"].ToString();

                if (customer == "** ALL **")
                {
                    customer = "%";
                }


                if (vendor == "** ALL **" )
                {
                    vendor = "%";
                }

                cdrdbDatasetTableAdapters.SalesLogMpopAdp adp = new cdrdbDatasetTableAdapters.SalesLogMpopAdp();
                cdrdbDataset.SalesLogMpopDataTable tbl = adp.getDatabyVenCusToFr(Convert.ToDateTime(to), customer, vendor, Convert.ToDateTime(from));
                Session["ResultSalesmpop"] = tbl;
                gdvShow.DataSource = tbl;
                gdvShow.DataBind();

                if (tbl.Rows.Count < 2)
                {
                    gdvShow.Visible = false;
                    lblSales.Visible = true;
                }


            }
            catch(Exception ex)
            {
                string message = ex.Message;
            }

        }


    }
    protected void gdvShow_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvShow.PageIndex = e.NewPageIndex;
        gdvShow.DataSource = (DataTable)Session["ResultSalesmpop"];
        gdvShow.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {

            DataTable dtEmployee = ((DataTable)Session["ResultSalesmpop"]).Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "OffnetMinipop" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
}
