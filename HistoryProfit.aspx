<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="HistoryProfit.aspx.cs" Inherits="HistoryProfit" Title="Untitled Page" StylesheetTheme="Theme1" %>



<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="tblMain">
        <tr>
            <td style="width: 100px">
                <asp:Label ID="lblTitle" runat="server" CssClass="labelBlue8" Text="History Profit" Width="66px"></asp:Label></td>
            <td style="width: 215px">
            </td>
            <td style="width: 100px">
                <asp:ScriptManager id="ScriptManager2" runat="server">
                </asp:ScriptManager>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 215px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="right" valign="top">
                </td>
            <td style="width: 215px">
                </td>
            <td style="width: 100px">
            </td>
            <td align="left" rowspan="3" style="width: 100px" valign="top">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<asp:GridView id="gdvProfit" runat="server" width="500px" Font-Size="8pt" Font-Names="Arial" __designer:wfdid="w35" AutoGenerateColumns="false" AllowPaging="True" OnPageIndexChanging="gdvProfit_PageIndexChanging" PageSize="8">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />
<Columns>
<asp:BoundField DataField="Billingdate" HeaderText="Billing Date">
<ItemStyle Wrap="False"></ItemStyle>

<HeaderStyle Wrap="False" HorizontalAlign="Center"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Vendor" HeaderText="Vendor">
<ItemStyle Wrap="False"></ItemStyle>

<HeaderStyle Wrap="False" HorizontalAlign="Center"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Minutes" HeaderText="Minutes">
<ItemStyle Wrap="False"></ItemStyle>

<HeaderStyle Wrap="False" HorizontalAlign="Center"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="profit" HeaderText="Profit">
<ItemStyle Wrap="False"></ItemStyle>

<HeaderStyle Wrap="False" HorizontalAlign="Center"></HeaderStyle>
</asp:BoundField>
</Columns>

<HeaderStyle CssClass="titleOrangegrid"></HeaderStyle>
</asp:GridView> 
</contenttemplate>
                </asp:UpdatePanel></td>
        </tr>
        <tr>
            <td style="height: 47px;" align="left" valign="top" colspan="2">
                <table>
                    <tr>
                        <td style="width: 100px" valign="top">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Select Provider:"
                    Width="76px"></asp:Label></td>
                        <td style="width: 100px" valign="top">
                <asp:DropDownList ID="dpdProvider" runat="server" CssClass="dropdown" DataSourceID="SqlProvider" DataTextField="Vendor" DataValueField="Vendor">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top">
                <asp:Label ID="lblFrom" runat="server" CssClass="labelBlue8" Text="From:"></asp:Label></td>
                        <td style="width: 100px" valign="top">
                <table>
                    <tr>
                        <td style="width: 100px">
                <asp:TextBox ID="txtFrom" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
                        <td style="width: 100px">
                <asp:Image ID="imgFrom" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                    </tr>
                </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top">
                <asp:Label ID="lblTo" runat="server" CssClass="labelBlue8" Text="To:"></asp:Label></td>
                        <td style="width: 100px" valign="top">
                <table>
                    <tr>
                        <td style="width: 100px">
                <asp:TextBox ID="txtTo" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
                        <td style="width: 100px">
                <asp:Image ID="imgTo" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                    </tr>
                </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top">
                        </td>
                        <td style="width: 100px" valign="top">
                <asp:Button ID="cmdReport" runat="server" CssClass="boton" OnClick="cmdReport_Click"
                    Text="View Report" /></td>
                    </tr>
                </table>
            </td>
            <td style="width: 100px; height: 47px;" align="left">
                </td>
        </tr>
        <tr>
            <td align="right" style="width: 100px" valign="top">
                </td>
            <td style="width: 215px" valign="top">
            </td>
            <td style="width: 100px" align="left">
                </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td align="left" style="width: 215px" valign="top">
                </td>
            <td style="width: 100px" align="left" rowspan="2" valign="top">
                <asp:UpdateProgress id="UpdateProgress1" runat="server">
                    <progresstemplate>
<TABLE id="tblUpdateProgress"><TR><TD style="WIDTH: 101px"><asp:Label id="Label1" runat="server" Text="Loading, please wait..." ForeColor="LimeGreen" Width="111px" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:Image id="imgLoading" runat="server" ImageUrl="~/Images/Loading.gif"></asp:Image></TD></TR><TR><TD style="WIDTH: 101px"></TD><TD style="WIDTH: 100px"></TD></TR></TABLE>
</progresstemplate>
                </asp:UpdateProgress>
            </td>
            <td align="left" rowspan="2" style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 21px">
            </td>
            <td align="left" style="width: 215px" valign="top">
                <asp:SqlDataSource ID="SqlProvider" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT DISTINCT Vendor FROM ProfitR UNION SELECT '**ALL**' AS Vendor FROM ProfitR ORDER BY Vendor"></asp:SqlDataSource>
                </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td align="left" colspan="2" valign="top">
                
            </td>
            <td align="left" colspan="1" valign="top">
   
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td align="right" style="width: 215px">
                &nbsp;</td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <cc1:calendarextender id="CalendarExtender1" runat="server" popupbuttonid="imgFrom"
                    targetcontrolid="txtFrom"></cc1:calendarextender>
                <cc1:calendarextender id="CalendarExtender2" runat="server" popupbuttonid="imgTo"
                    targetcontrolid="txtTo"></cc1:calendarextender>
            </td>
            <td colspan="1">
            </td>
        </tr>
    </table>
</asp:Content>

