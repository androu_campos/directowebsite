<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Home.aspx.cs"
    Inherits="Home" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>--%>
<%@ OutputCache Location="None" VaryByParam="None" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="x">
    </asp:ScriptManager>

    <script language="javascript" type="text/javascript">
    
     function showImage(imgId,typ) {
                var objImg = document.getElementById(imgId);
                if (objImg) {
                    if (typ == "1")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_1b.gif";
                   else if (typ == "2")
                       objImg.src = "Images/botonesMeccaHome/tabmeccahome_1a.gif";
                   else if (typ == "3")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_2b.gif";
                   else if (typ == "4")
                       objImg.src = "Images/botonesMeccaHome/tabmeccahome_2a.gif";  
                   else if (typ == "5")
                       objImg.src = "Images/botonesMeccaHome/tabmeccahome_3b.gif";  
                   else if (typ == "6")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_3a.gif";
                    else if (typ == "7")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_4b.png";
                    else if (typ == "8")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_4a.png";
                    else if (typ == "9")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_5b.png";
                    else if (typ == "10")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_5a.png";
               }
         }
         
    </script>

    <table align="left" id="tblMain" cellpadding="6" style="height: 50px; width: 80%;
        position: absolute; top: 130px; left: 220px;">
        <tr valign="bottom" height="20px">
            <td align="left" valign="bottom" colspan="3" style="height: 20px">
                <asp:ImageButton ID="img1" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_1b.gif"
                    ImageAlign="Bottom" Enabled="false"/>
                <asp:ImageButton ID="img2" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_2a.gif"
                    OnClick="buttonicshome_click"  />
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_3a.gif"
                    OnClick="buttoncchome_click" />
                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_4a.png"
                    OnClick="buttonsmshome_click" />
                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_5a.png"
                    OnClick="buttonaahome_click" /><br />
                <asp:Image Width="1060px" Height="2px" ID="tabline" runat="server" ImageUrl="~/Images/tabmeccahome2_3.gif" />
            </td>
        </tr>
        <tr>
            <td width="70px">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/unrated.aspx" Font-Size="10pt"
                    Font-Names="Arial" ForeColor="#666666" Width="250px">HyperLink1</asp:HyperLink>
            </td>
            <td width="50px">
                <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Arial" Font-Size="10pt"
                    ForeColor="#666666" NavigateUrl="~/Unknown.aspx" Width="250px">HyperLink2</asp:HyperLink>
            </td>
            <td width="20px">
                <asp:Label ID="lblBillingRunning" runat="server" CssClass="labelSteelBlue" Text="Billing Running"
                    Font-Size="8pt" Font-Names="Arial" ForeColor="#FFFFFF" Width="180px"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table>
        <tr>
            <td rowspan="4" style="height: 850px; width: 296px;" valign="top">
                <img border="0" src="Images/meccasummary.gif" height="24">
                <table border="0" width="264" id="tblSummary" cellspacing="0" cellpadding="0">
                    <tr height="18px">
                        <td width="72" height="20">
                        </td>
                        <td width="71" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">&nbsp; &nbsp;&nbsp; Latest</font></b>
                        </td>
                        <td width="113" colspan="2" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">&nbsp; &nbsp; &nbsp;&nbsp; Change</font></b></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72" align="left">
                            <font color="#004D88" style="font-size: 8pt; margin-left: 10px; margin-top: 5px;
                                margin-bottom: 5px; color: #004d88; font-family: Arial">Attempts</font>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp;
                                <asp:Label ID="lblAttemptsY" runat="server"></asp:Label>
                            </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblAttemptsC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image10" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72" align="left">
                            <font color="#004D88" style="font-size: 8pt; margin-left: 10px; margin-top: 5px;
                                margin-bottom: 5px; color: #004d88; font-family: Arial">Minutes</font>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp;
                                <asp:Label ID="lblMinutesY" runat="server"></asp:Label>
                            </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblMinutesC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;</td>
                        <td width="19" align="center">
                            <asp:Image ID="Image4" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">Calls</font>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp; &nbsp;
                                <asp:Label ID="lblCallsY" runat="server"></asp:Label></font></td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#FF3300" style="font-size: 8pt; color: red; font-family: Arial">
                                    <asp:Label ID="lblCallsC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image5" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Profit"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblProfitY" runat="server"></asp:Label></font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblProfitC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image6" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="Label11" runat="server" CssClass="labelBlue8" Text="Total Sales"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblTotSalesY" runat="server"></asp:Label></font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblTotSalesC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image13" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">ASR</font>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblASRY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblASRC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image7" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">ABR</font>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblABRY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblABRC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image15" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">ACD</font>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblACDY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblACDC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image8" runat="server" /></td>
                    </tr>
                </table>
                <br />
                <p>
                </p>
                <table border="0" width="300px" id="Table1" cellspacing="0" cellpadding="0" align="left">
                    <tr>
                        <td valign="top" style="height: 38px">
                            <table border="0" width="300px" id="table2" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" bgcolor="#d5e3f0" colspan="3" height="20" style="width: 377px">
                                        <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/topcountries.gif" /></td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="left" height="24" valign="bottom" style="width: 377px">
                                        <table style="width: 300px">
                                            <tr>
                                                <td style="width: 400px" valign="bottom">
                                                    <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Countries"></asp:Label></td>
                                                <td style="width: 50px" align="right" valign="bottom">
                                                    <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Minutes"></asp:Label></td>
                                                <td align="right" valign="bottom" style="width: 100px">
                                                    <asp:Label ID="labelSales3" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="Sales"></asp:Label></td>
                                                <td align="right" valign="bottom" style="width: 50px">
                                                    <asp:Label ID="lblHeaderProfit3" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="Profit"></asp:Label></td>
                                                <td align="center" valign="bottom" style="width: 30px">
                                                    <asp:Label ID="Label18" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="ASR"></asp:Label>
                                                </td>
                                                <td align="center" valign="bottom" style="width: 30px">
                                                    <asp:Label ID="Label19" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="ABR"></asp:Label>
                                                </td>
                                                <td align="center" valign="bottom" style="width: 30px">
                                                    <asp:Label ID="Label20" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="ACD"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td id="Td1" colspan="3" align="center" valign="top" style="height: 14px; width: 377px;">
                                        <asp:Label ID="lblCouN" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="5" rowspan="5">
                                        <asp:GridView ID="gdvCountry" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Country" ItemStyle-Width="130px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Minutes" DataFormatString="{0:0,0}" ItemStyle-Width="50px"
                                                    HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Sales" DataFormatString="{0:C0}" ItemStyle-Width="50px"
                                                    HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" DataFormatString="{0:C0}" ItemStyle-Width="50px"
                                                    HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ASR" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ABR" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ACD" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="hypNational" runat="server" CssClass="labelBlue8" NavigateUrl="http://national.directo.com">Mecca NATIONAL</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td rowspan="4" style="height: 850px; width: 250px;" valign="top" align="left">
                <table border="0" width="250px" id="tblCenter" cellspacing="0" cellpadding="0" align="left">
                    <tr>
                        <td valign="top" style="height: 351px; width: 250px;">
                            <table border="0" width="250px" id="table15" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" bgcolor="#d5e3f0" colspan="3" height="20">
                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/topcustvend.gif" /></td>
                                </tr>
                                <tr>
                                    <td width="450px" colspan="3" align="left" height="24" valign="bottom">
                                        <table style="width: 400px">
                                            <tr>
                                                <td style="width: 130px" valign="bottom">
                                                    <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Customers"></asp:Label></td>
                                                <td align="center" valign="bottom" style="width: 50px">
                                                    <asp:Label ID="Label15" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Sales"></asp:Label>
                                                </td>
                                                <td style="width: 50px" align="right" valign="bottom">
                                                    <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Minutes"></asp:Label></td>
                                                <td align="center" valign="bottom" style="width: 50px">
                                                    <asp:Label ID="lblHeaderProfit1" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="Profit"></asp:Label>
                                                </td>
                                                <td align="center" valign="bottom" style="width: 30px">
                                                    <asp:Label ID="lblHeaderASR" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="ASR"></asp:Label>
                                                </td>
                                                <td align="center" valign="bottom" style="width: 30px">
                                                    <asp:Label ID="lblHeaderABR" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="ABR"></asp:Label>
                                                </td>
                                                <td align="center" valign="bottom" style="width: 30px">
                                                    <asp:Label ID="lblHeaderACD" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="ACD"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td id="IMG" width="450px" colspan="3" align="center" valign="top">
                                        <asp:Label ID="lblCusN" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <asp:GridView RowStyle-Height="18px" ID="gdvCustomers" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Customer" HtmlEncode="False" InsertVisible="False" ShowHeader="False">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Sales" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:C0}"
                                                    ItemStyle-Width="50px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Minutes" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="50px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:C0}"
                                                    ItemStyle-Width="50px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ASR" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ABR" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ACD" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                            <RowStyle Height="18px" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="3">
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td width="450px" colspan="3" align="left" height="24">
                                        <table width="400px" align="center">
                                            <tr>
                                                <td style="width: 130px" valign="bottom">
                                                    <asp:Label ID="lblHeaderVendors" runat="server" Text="Vendors" CssClass="labelBlue8"
                                                        Font-Bold="True"></asp:Label></td>
                                                <td align="center" valign="bottom" style="width: 50px">
                                                    <asp:Label ID="Label17" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Cost"></asp:Label>
                                                </td>
                                                <td align="right" valign="bottom" width="50px">
                                                    <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Minutes"></asp:Label>
                                                <td valign="bottom" width="50px" align="center">
                                                    <asp:Label ID="lblHeaderProfit2" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="Profit"></asp:Label>
                                                </td>
                                                <td valign="bottom" width="30px" align="center">
                                                    <asp:Label ID="lblHeaderASR2" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="ASR"></asp:Label>
                                                </td>
                                                <td valign="bottom" width="30px" align="center">
                                                    <asp:Label ID="lblHeaderABR2" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="ABR"></asp:Label>
                                                </td>
                                                <td valign="bottom" width="30px" align="center">
                                                    <asp:Label ID="lblHeaderACD2" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="ACD"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="3" align="center">
                                        <img border="0" src="Images/line.gif" width="264" height="1"><br />
                                        <asp:Label ID="lblVenN" runat="server" ForeColor="Red" Text="Not available"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5" width="100%" style="height: 100px">
                                        <asp:GridView ID="gdVendors" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Vendor">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="150px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Cost" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:C0}"
                                                    ItemStyle-Width="50px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Minutes" DataFormatString="{0:0,0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" DataFormatString="{0:C0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ASR" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ABR" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ACD" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                                SelectCommand="SELECT Country, City, Average_Cost, Expected_ASR, Yesterday_ACD, Avail_Capacity, Service_Level, Comments, Vendor_Billing, Date_Updated, Minimum FROM Push_List">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="4" style="height: 850px; width: 239px;" valign="top" align="left">
                <table border="0" width="264" cellspacing="0" cellpadding="0" id="NOCRouting" visible="false">
                    <tr>
                        <td align="left">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/noc1.gif" />
                            <asp:GridView ID="gdvRouting" runat="server" Font-Size="8pt" Font-Names="Arial" Width="264px"
                                AutoGenerateColumns="false" CssClass="labelSteelBlue">
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#2461BF" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                    Font-Size="8pt" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="Vendor" HeaderText="Vendor" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PenaltyStatus" HeaderText="PenaltyStatus" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Destination" HeaderText="Destination" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="labelSteelBlue" Font-Size="7pt"
                                PostBackUrl="~/MonitoringSearch.aspx">more...</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td bgcolor="#F7F7F7" align="left">
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#F7F7F7" align="left">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 27px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#f7f7f7">
                        </td>
                    </tr>
                    <%--<tr>
                        <td align="left" style="height: 169px">
                            <%--<asp:Image ID="Image10" runat="server" ImageUrl="~/Images/deploy.gif" />--%>
                    <%--<asp:GridView ID="gdvDeployment" runat="server" Font-Names="Arial" Font-Size="8pt"
                                AutoGenerateColumns="false" Width="264px" CssClass="labelSteelBlue">
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#2461BF" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                    Font-Size="8pt" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="Vendor" HeaderText="Vendor" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Capacity" HeaderText="Capacity" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>--%>
                    <%--<asp:HyperLink ID="HyperLink3" runat="server" CssClass="labelSteelBlue" Font-Size="7pt"
                                NavigateUrl="~/MonitoringSearch.aspx">more...</asp:HyperLink></td>
                    </tr>--%>
                </table>
                <br />
                <asp:Panel ID="pnlCtryMan" runat="server" Height="207px" Width="125px" Visible="false">
                    <table id="Commissions">
                        <tr>
                            <td colspan="3" height="9">
                                <asp:Image ID="imgCtryMan5" runat="server" ImageUrl="~/Images/Commissions.gif" /></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" style="height: 100%">
                                <asp:Label ID="Label16" runat="server" ForeColor="Red" Text="Not available" Visible="False"></asp:Label>
                                <table border="0" id="tblTop5" cellspacing="0" cellpadding="0" style="height: 100%"
                                    width="71">
                                    <tr height="18">
                                        <td width="20%" style="height: 20px">
                                        </td>
                                        <td width="25%" style="height: 20px">
                                            <b><font color="orange" style="font-size: 7pt; font-family: Arial; margin-bottom: 2px;
                                                vertical-align: middle;">Calls</font></b>
                                        </td>
                                        <td valign="middle" width="25%" style="height: 20px">
                                            <b><font color="orange" style="font-size: 7pt; font-family: Arial; margin-bottom: 2px;
                                                vertical-align: middle;">Minutes</font></b></td>
                                        <td colspan="2" valign="middle" width="25%" style="height: 20px">
                                            <b><font color="orange" style="font-size: 7pt; font-family: Arial; margin-bottom: 2px;
                                                vertical-align: middle;">Gross Profit</font></b></td>
                                        <td style="height: 20px" width="5%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                    </tr>
                                    <tr>
                                        <td valign="middle" width="20%">
                                            <asp:Label ID="lblCtry1" runat="server" CssClass="labelBlue8" Text="Andre"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall1" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                        <td width="25%" align="center">
                                            <asp:Label ID="lblTMin1" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td align="center" colspan="2" width="25%">
                                            <asp:Label ID="lblTgross1" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                        </td>
                                        <td width="5%">
                                            <asp:Image ID="imgStatus1" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                    </tr>
                                    <tr>
                                        <td valign="middle" width="20%">
                                            <asp:Label ID="lblCtry3" runat="server" CssClass="labelBlue8" Text="Hamit"></asp:Label></td>
                                        <td width="25%" align="center">
                                            <asp:Label ID="lblTCall2" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                        <td width="25%" align="center">
                                            <asp:Label ID="lblTMin2" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td colspan="2" width="25%">
                                            <asp:Label ID="lblTgross2" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                        </td>
                                        <td width="5%">
                                            <asp:Image ID="imgStatus2" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <font color="#004d88">
                                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <asp:Label ID="lblCtry5" runat="server" CssClass="labelBlue8" Text="Iliana"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall3" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                        <td width="25%" align="center">
                                            <asp:Label ID="lblTMin3" runat="server" CssClass="labelSteelBlue"></asp:Label>
                                        </td>
                                        <td colspan="2" width="25%">
                                            <asp:Label ID="lblTgross3" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                        </td>
                                        <td width="5%">
                                            <asp:Image ID="imgStatus3" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <font color="#004d88">
                                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <asp:Label ID="lblCtry6" runat="server" Text="Jaime" CssClass="labelBlue8"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall4" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                        <td width="25%" align="center">
                                            <asp:Label ID="lblTMin4" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td colspan="2" width="25%">
                                            <asp:Label ID="lblTgross4" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                        </td>
                                        <td width="5%">
                                            <asp:Image ID="imgStatus4" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5" style="height: 1px">
                                            <font color="#004d88">
                                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <asp:Label ID="lblCtry8" runat="server" Text="Jake" CssClass="labelBlue8"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall5" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                        <td width="25%" align="center">
                                            <asp:Label ID="lblTMin5" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td colspan="2" width="25%">
                                            <asp:Label ID="lblTgross5" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                        </td>
                                        <td width="5%">
                                            <asp:Image ID="imgStatus5" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <asp:Label ID="lblCtry9" runat="server" Text="Martha" CssClass="labelBlue8"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall6" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label>
                                        </td>
                                        <td width="25%" align="center">
                                            &nbsp;<asp:Label ID="lblTMin6" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td colspan="2" width="25%">
                                            <asp:Label ID="lblTgross6" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                        </td>
                                        <td width="5%">
                                            <asp:Image ID="imgStatus6" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <font color="#004d88">
                                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" style="height: 30px">
                                            <asp:Label ID="lblCtry10" runat="server" Text="Michelle" CssClass="labelBlue8"></asp:Label></td>
                                        <td align="center" width="25%" style="height: 30px">
                                            <asp:Label ID="lblTCall7" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></td>
                                        <td width="25%" align="center" style="height: 30px">
                                            <asp:Label ID="lblTMin7" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td colspan="2" width="25%" style="height: 30px">
                                            <asp:Label ID="lblTgross7" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                        </td>
                                        <td width="5%" style="height: 30px">
                                            <asp:Image ID="imgStatus7" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <font color="#004d88">
                                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <asp:Label ID="lblCtry11" runat="server" Text="Roberto" CssClass="labelBlue8"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall8" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></td>
                                        <td width="25%" align="center">
                                            <asp:Label ID="lblTMin8" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td colspan="2" width="25%">
                                            <asp:Label ID="lblTgross8" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                        </td>
                                        <td width="5%">
                                            <asp:Image ID="imgStatus8" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <font color="#004d88">
                                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <font color="#004d88"></font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5" style="height: 12px">
                                            <font color="#004d88"></font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" align="left">
                                            <asp:LinkButton ID="LinkButton2" runat="server" CssClass="labelSteelBlue" Font-Size="7pt"
                                                PostBackUrl="~/Commsns.aspx">more...</asp:LinkButton></td>
                                        <td align="center" width="25%">
                                        </td>
                                        <td width="25%">
                                            <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial"></font>
                                        </td>
                                        <td colspan="2" width="25%">
                                            <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial"></font>
                                        </td>
                                        <td width="5%">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" bgcolor="white">
                                &nbsp; &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <%-- <table>
                        <tr>
                            <td style="height: 15px; width: 200px;">
                                <img border="0" src="Images/specialc.gif" height="24"><br />
                            </td>
                        </tr>
                    </table>
        </tr>
       <tr>
            <td style="width: 267px; height: 19px;">
                <ul style="color: #004D88; font-size: 8pt" type="square">
                    <li style="text-align: left;">
                        <asp:Label ID="Label8" runat="server" Text="Label" CssClass="labelSteelBlue" ForeColor="DarkBlue"
                            Width="193px"></asp:Label>
                    </li>
                    <li style="text-align: left;">
                    <asp:Label ID="Label9" runat="server" Text="Label" CssClass="labelSteelBlue" ForeColor="DarkBlue"
                        Width="193px"></asp:Label>
                    </li>
                                    <li style="text-align: left;">
                    <asp:Label ID="Label10" runat="server" Text="Label" CssClass="labelSteelBlue" ForeColor="DarkBlue"
                        Width="193px"></asp:Label>
                    </li>
                </ul>
            </td>
        </tr>--%>
    </table>
    <%--<td valign="top">
                <asp:Image ID="Image17" runat="server" ImageUrl="~/Images/wid.gif" />
                <table>
                    <tr>
                        <td style="width: 606px">
                            <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1" Font-Names="Arial"
                                Font-Size="8pt" CssClass="dropdown" Width="275px" AutoGenerateColumns="False"
                                AllowPaging="True" PageSize="5">
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#2461BF" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <PagerStyle BackColor="#D5E3F0" ForeColor="#004A8F" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                    Font-Size="8pt" Width="25px" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="Country" HeaderText="Country">
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="25px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="City" HeaderText="City" HtmlEncode="False">
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="25px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Minimum" HeaderText="Minimum Sell Price" HtmlEncode="False">
                                        <HeaderStyle Width="25px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Average_Cost" HeaderText="Average Cost">
                                        <HeaderStyle Width="25px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Expected_ASR" HeaderText="Expected ASR">
                                        <HeaderStyle Width="25px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Yesterday_ACD" HeaderText="Yesterday ACD">
                                        <HeaderStyle Width="25px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Avail_Capacity" HeaderText="Available Channels">
                                        <HeaderStyle Width="25px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Service_Level" HeaderText="Service Level">
                                        <HeaderStyle Width="25px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Comments" HeaderText="Comments">
                                        <HeaderStyle Width="25px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Vendor_Billing" HeaderText="Vendor Billing">
                                        <HeaderStyle Width="25px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Date_Updated" HeaderText="Date Updated">
                                        <HeaderStyle Width="25px" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:LinkButton ID="LinkButton3" runat="server" CssClass="labelSteelBlue" Font-Size="7pt"
                                PostBackUrl="~/PushListEdit.aspx">More...</asp:LinkButton></td>
                    </tr>
                </table>
                <p>
                    <asp:Image ID="Image12" runat="server" ImageUrl="~/Images/WIDGET.gif" />&nbsp;
                </p>
                <ul style="color: #004D88; font-size: 8pt" type="square">
                    <li>
                        <asp:Label ID="Label12" runat="server" Text="Label" CssClass="labelBlue8" Width="218px"></asp:Label>
                    </li>
                    <asp:Label ID="Label13" runat="server" Text="Label" CssClass="labelBlue8" Width="218px"></asp:Label>
                    <asp:Label ID="Label14" runat="server" Text="Label" CssClass="labelBlue8" Width="218px"></asp:Label></ul>
                <table width="266">
                    <tr>
                        <td style="width: 100px">
                            <asp:Image ID="imgWidget" runat="server" ImageUrl="~/Images/widgetaz.gif" Width="266px" /></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:GridView ID="gdvWidgetLCR" runat="server" Width="266px" CssClass="labelSteelBlue"
                                DataSourceID="SqL_WidgetLcr" Font-Names="Arial" Font-Size="7pt" Height="124px">
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#2461BF" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                    Font-Size="8pt" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                            <asp:HyperLink ID="HyperLink5" runat="server" CssClass="labelSteelBlue" Font-Size="7pt"
                                NavigateUrl="~/WidgetLcr.aspx">more...</asp:HyperLink></td>
                    </tr>
                </table>
                &nbsp;
                <p>
                    <asp:Image ID="Image14" runat="server" ImageAlign="Left" ImageUrl="~/Images/tutorial.gif" />&nbsp;</p>
                <p>
                </p>
                <ul style="color: #004D88; font-size: 8pt" type="square">
                    <li>
                        <asp:HyperLink ID="HyperLink4" runat="server" CssClass="labelBlue8" NavigateUrl="~/graphs.ppt">New Active Calls Graph Tutorial</asp:HyperLink>
                    </li>
                </ul>
            </td>--%>
    <table>
        <tr>
            <td align="left" valign="top">
                &nbsp;<br />
            </td>
        </tr>
        <tr>
            <td align="left" style="height: 38px">
                <br />
            </td>
        </tr>
        <tr>
            <td style="height: 27px">
                <p style="margin-bottom: 2px">
            </td>
        </tr>
        <tr>
            <td colspan="3" width="250">
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/footer.gif" /></td>
        </tr>
        <tr>
            <td bgcolor="" width="250">
                <br>
                <table>
                    <tr>
                        <td align="right" colspan="3">
                            <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
            <td style="width: 168px">
            </td>
            <td style="width: 239px">
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="SqL_WidgetLcr" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT TOP (10) countryLcr, owner AS Responsible, status, priority, Date_received, Date_Completed, RequestedBy, TypeOf AS LCRType, Comments FROM WidgetEntry ORDER BY Date_received DESC">
    </asp:SqlDataSource>
</asp:Content>
