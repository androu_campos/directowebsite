using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

public partial class ICSQBuilder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            //Customer & Vendor

            string hometype = string.Empty;
            Session["Table"] = string.Empty;
            SqlCommand SqlCommand1 = null;
            txtPageSize.Text = "25";

            //CUSTOMER
            SqlConnection SqlConn1 = new SqlConnection();
            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";

            SqlCommand1 = new SqlCommand("SELECT DISTINCT ProviderName from CDRDB.dbo.ProviderIP WHERE Type like 'C' AND Providername in (select providername from ICS.dbo.DIDPRoviders) UNION SELECT '**ALL**' from CDRDB.dbo.ProviderIP as ProviderName UNION SELECT '** NONE **' from CDRDB.dbo.ProviderIP as ProviderName", SqlConn1);            

            SqlConn1.Open();
            SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
            while (myReader1.Read())
            {
                dpdCustomer.Items.Add(myReader1.GetValue(0).ToString());
            }
            myReader1.Close();
            SqlConn1.Close();
            //VENDOR

            SqlCommand SqlCommand = new SqlCommand("SELECT DISTINCT ProviderName from ProviderIP WHERE Type like 'V' UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT '** NONE **' from ProviderIP as ProviderName ", SqlConn1);
            SqlConn1.Open();
            SqlDataReader myReader = SqlCommand.ExecuteReader();
            while (myReader.Read())
            {
                dpdVendor.Items.Add(myReader.GetValue(0).ToString());
            }
            myReader.Close();
            SqlConn1.Close();
            //COUNTRY
            SqlConnection SqlConn = new SqlConnection();
            SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";

            SqlCommand SqlCommand4 = new SqlCommand("select distinct Country from ICS.dbo.sourceDID union  select '** NONE **' as Country from ICS.dbo.sourceDID union select '**ALL**' as Country from ICS.dbo.sourceDID order by Country ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader4 = SqlCommand4.ExecuteReader();
            while (myReader4.Read())
            {
                dpdCountry.Items.Add(myReader4.GetValue(0).ToString());
            }
            myReader4.Close();
            SqlConn.Close();
            //REGION
            SqlCommand SqlCommand5 = new SqlCommand("select distinct city + ' - ' + stateprefix as Region from ICS.dbo.sourceDID where city is not NULL union select '** NONE **' as Region from ICS.dbo.sourceDID union select '**ALL**' as Region from ICS.dbo.sourceDID order by Region ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader5 = SqlCommand5.ExecuteReader();
            while (myReader5.Read())
            {
                dpdRegions.Items.Add(myReader5.GetValue(0).ToString());
            }
            myReader5.Close();
            SqlConn.Close();
            //TYPE
            //SqlCommand SqlCommand6 = new SqlCommand("select distinct Regions.Type from Regions union select '** NONE **' as Type from Regions union select '**ALL**' as Type from Regions order by Type ASC", SqlConn);
            //SqlConn.Open();
            //SqlDataReader myReader6 = SqlCommand6.ExecuteReader();
            //while (myReader6.Read())
            //{
            //    dpdType.Items.Add(myReader6.GetValue(0).ToString());
            //}
            //myReader6.Close();
            //SqlConn.Close();
            //Providername
            SqlCommand SqlCommand7 = new SqlCommand("SELECT DISTINCT Providername FROM ICS.dbo.sourceDID UNION SELECT '** NONE **' AS Providername FROM ICS.dbo.sourceDID UNION SELECT '**ALL**' AS Providername FROM ICS.dbo.sourceDID ORDER BY Providername", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader7 = SqlCommand7.ExecuteReader();
            while (myReader7.Read())
            {
                dpdClass.Items.Add(myReader7.GetValue(0).ToString());
            }
            myReader7.Close();
            SqlConn.Close();

            //Framework
            SqlCommand SqlCommand8 = new SqlCommand("SELECT DISTINCT Framework FROM ICS.dbo.sourceDID UNION SELECT '** NONE **' AS Framework FROM ICS.dbo.sourceDID UNION SELECT '**ALL**' AS Framework FROM ICS.dbo.sourceDID ORDER BY Framework", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader8 = SqlCommand8.ExecuteReader();
            while (myReader8.Read())
            {
                dpdLmc.Items.Add(myReader8.GetValue(0).ToString());
            }
            myReader8.Close();
            SqlConn.Close();


            dpdOriginationIP.DataSource = OriginationIPS;
            dpdOriginationIP.DataTextField = "OriginationIP";
            dpdOriginationIP.DataValueField = "OriginationIP";
            dpdOriginationIP.DataBind();

            dpdTerminationIP.DataSource = TerminationIPS;
            dpdTerminationIP.DataTextField = "TerminationIP";
            dpdTerminationIP.DataValueField = "TerminationIP";
            dpdTerminationIP.DataBind();

            dpdSourceMSW.DataSource = SqlDataSource1;
            dpdSourceMSW.DataTextField = "IP";
            dpdSourceMSW.DataValueField = "IP";            

        }
        else
        {

        }

    }
    protected void cmdQbuilder_Click(object sender, EventArgs e)
    {

        try
        {

            string where = string.Empty;
            string orderby = " Order by ";
            string WHERE = string.Empty;
            string hometype = string.Empty;
            StringBuilder newQuery = new StringBuilder();
            StringBuilder newWhere = new StringBuilder();
            StringBuilder newAll = new StringBuilder();
            StringBuilder newGroup = new StringBuilder();

            newQuery.Append("Select ");
            newWhere.Append(" WHERE ");
            newAll.Append(" UNION Select ");
            newGroup.Append(" Group by ");

            if (dpdCustomer.SelectedIndex != 0)     //Customer
            {                
                newQuery.Append("Customer,");
                orderby = orderby + "Customer,";
                newGroup.Append("Customer,");
                newWhere.Append("Customer like '" + dpdCustomer.SelectedValue.ToString() + "' AND ");
                newAll.Append("'**ALL**' as Customer,");                
            }
            if (dpdVendor.SelectedIndex != 0)       //Vendor
            {
                newQuery.Append("Vendor,");
                newGroup.Append("Vendor,");
                orderby = orderby + "Vendor,";
                newWhere.Append("Vendor like '" + dpdVendor.SelectedValue.ToString() + "' AND ");
                newAll.Append(" '**ALL**' as Vendor,");
            }
            if (dpdOriginationIP.SelectedIndex != 0)    //OriginationIp
            {
                newQuery.Append("OriginationIP,");
                newGroup.Append("OriginationIP,");
                newWhere.Append("OriginationIP like '" + dpdOriginationIP.SelectedValue.ToString() + "' AND ");
                newAll.Append("NULL as OriginationIP,");

            }
            if (dpdTerminationIP.SelectedIndex != 0)    //TerminationIp
            {
                newQuery.Append("TerminationIP,");
                newGroup.Append("TerminationIP,");
                newWhere.Append("TerminationIP like '" + dpdTerminationIP.SelectedValue.ToString() + "' AND ");
                newAll.Append("NULL as TerminationIP,");
            }
            if (dpdCountry.SelectedIndex != 0)
            {
                newQuery.Append("Country,");
                newGroup.Append("Country,");
                newWhere.Append("Country like '" + dpdCountry.SelectedValue.ToString() + "' AND ");
                newAll.Append("NULL as Country,");
            }

            if (dpdRegions.SelectedIndex != 0)  //Regions
            {
                string regionS = dpdRegions.SelectedValue.ToString();
                regionS = regionS.Replace("**ALL**", "%");

                newQuery.Append("Region,");
                newGroup.Append("Region,");
                newWhere.Append("Region like '" + regionS + "' AND ");
                newAll.Append("'**ALL**' as Region,");
            }
            //if (chkDetail.Checked == true)      //Detail
            //{
            //    newQuery.Append("RegionPrefix,");
            //    newGroup.Append("RegionPrefix,");
            //    newAll.Append("NULL as RegionPrefix,");
            //}
            //if (dpdType.SelectedIndex != 0)     //Type
            //{
            //    newQuery.Append("Type,");
            //    newGroup.Append("Type,");
            //    newWhere.Append("Type like '" + dpdType.SelectedValue.ToString() + "' AND ");
            //    newAll.Append("NULL as Type,");
            //}
            if (dpdClass.SelectedIndex != 0)    //Class
            {
                newQuery.Append("Providername,");
                newGroup.Append("Providername,");
                newWhere.Append("Providername like '" + dpdClass.SelectedValue.ToString() + "' AND ");
                newAll.Append("NULL as Providername,");
            }

            if (dpdLmc.SelectedIndex != 0)    //LMC
            {
                newQuery.Append("FrameWork,");
                newGroup.Append("FrameWork,");
                newWhere.Append("FrameWork like '" + dpdLmc.SelectedValue.ToString() + "' AND ");
                newAll.Append("NULL as FrameWork,");

                //if (dpdLmc.SelectedValue.ToString().Equals("**ALL**"))
                //{
                //    newQuery.Append("Case WHEN p3.ENTERPRISE IS NULL THEN A.LMC ELSE p3.ENTERPRISE END AS LMC,");
                //    newGroup.Append("Case WHEN p3.ENTERPRISE IS NULL THEN A.LMC ELSE p3.ENTERPRISE END,");
                //    //newWhere.Append("LMC in (SELECT DISTINCT CASE WHEN ENTERPRISE IS NULL THEN LMC ELSE ENTERPRISE END LMC FROM LMC) AND ");
                //}
                //else
                //{
                //    newQuery.Append("'" + dpdLmc.SelectedValue.ToString() + "' as LMC, ");
                //    newWhere.Append("LMC in (SELECT LMC FROM MECCA2..LMC WHERE ENTERPRISE = '" + dpdLmc.SelectedValue.ToString() + "') AND ");
                //}
                //newAll.Append("NULL as LMC,");
            }


            if (dpdGateway.SelectedIndex != 0)    //Gateway
            {
                newQuery.Append("VendorID,");
                newGroup.Append("VendorID,");
                newWhere.Append("VendorID like '" + dpdGateway.SelectedValue.ToString() + "' AND ");
                newAll.Append(" '**ALL**' as VendorID,");
            }

            if (dpdSourceMSW.SelectedValue.ToString() != "%")//Source
            {
                newQuery.Append("Source,");
                newGroup.Append("Source,");
                newAll.Append("NULL as Source,");

                if (dpdReportType.SelectedValue.ToString() == "OpSheetInc" || dpdReportType.SelectedValue.ToString() == "OpSheetAll")
                {
                    if (dpdSourceMSW.SelectedIndex == 1)
                    {
                        newWhere.Append("Source like '" + dpdSourceMSW.SelectedValue.ToString() + "' AND ");
                    }
                    else
                    {
                        newWhere.Append("Source like '" + dpdSourceMSW.SelectedValue.ToString() + "CTMP" + "' AND ");
                    }
                }

                else if (dpdReportType.SelectedIndex == 2)
                {
                    newWhere.Append("Source like '" + dpdSourceMSW.SelectedValue.ToString() + "' AND ");
                }

            }

            //NEW WHERE

            //if (Session["hometype"] != string.Empty)
            //{
            //    hometype = Session["hometype"].ToString();

            //    if (hometype == "R")
            //        //newWhere.Replace("Customer like '" + dpdCustomer.SelectedValue.ToString() + "'", "Customer like 'ICS%'");
            //        newWhere.Replace("**ALL**", "ICS%");
            //}

            newWhere.Replace("**ALL**", "%");

            newWhere.Remove(newWhere.Length - 4, 4);
            if (newWhere.Length < 1) newWhere.Replace(newWhere.ToString(), "");

            //Select finalizado
            newQuery.Append("ROUND(SUM(Minutes),2)as Minutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls],mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls),SUM(RA)) AS ASR, mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR, mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) AS ACD,ROUND(CAST(AVG(PDD/1000) as float),1) AS PDD ");
            newQuery.Append(" FROM " + dpdReportType.SelectedValue.ToString() + " A");

            //if (dpdLmc.SelectedIndex != 0 && dpdLmc.SelectedValue.ToString().Equals("**ALL**"))
            //    newQuery.Append(" LEFT OUTER JOIN MECCA2..LMC p3 ON p3.Enterprise like A.LMC + '%'");

            //if (Session["hometype"].ToString() == "W")
            //{
            //    newWhere.Append(" AND Routetype = 'W'");
            //}
            //else if (Session["hometype"].ToString() == "R")
            //{
            //    newWhere.Append(" AND Routetype = 'R'");
            //}

            newQuery.Append(" " + newWhere.ToString() + " ");


            if (newGroup.Length < 11)
            {
                newGroup.Replace(newGroup.ToString(), " ");
            }
            else
            {
                newGroup.Remove(newGroup.Length - 1, 1);
            }





            newAll.Append("ROUND(SUM(Minutes),2)AS Minutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls],mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls),SUM(RA)) AS ASR, mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR, mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) AS ACD,ROUND(AVG(PDD)/1000,1) AS PDD ");
            newAll.Append(" FROM " + dpdReportType.SelectedValue.ToString() + newWhere.ToString() + " ");
            newQuery.Append(newGroup.ToString());
            newQuery.Append(newAll.ToString() + " ORDER BY Minutes DESC ");

            //Response.Write(newQuery.ToString());
            //Response.End();

            Session["AdQbuilder1"] = newQuery.ToString();
            string tableReport = dpdReportType.SelectedValue.ToString();
            Response.Redirect("ICSQResults.aspx?tbl=" + tableReport + "&Pz=" + txtPageSize.Text, false);

        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }

    }


    protected void dpdCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        dpdRegions.Items.Clear();
        string country = dpdCountry.SelectedValue.ToString();
        country = country.Replace("**ALL**", "%");
        DataSet dtst = Util.RunQueryByStmntatCDRDB("Select distinct city + ' - ' + stateprefix region from ICS.dbo.sourceDID where country = '" + country + "' UNION SELECT '**ALL**' AS REGION FROM ICS.dbo.sourceDID UNION SELECT '** NONE **' AS REGION FROM ICS.dbo.sourceDID ORDER BY REGION ");
        DataTable tbl = dtst.Tables[0];
        foreach (DataRow row in tbl.Rows)
        {
            dpdRegions.Items.Add(row[0].ToString());
        }
    }
}
