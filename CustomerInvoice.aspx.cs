using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class MasterAccess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (!Page.IsPostBack)
            {
                txtTo.Text = System.DateTime.Now.AddDays(-1).ToShortDateString();
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();

        }
    }


    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {
        Int16 tip;
        tip=Convert.ToInt16(RadioButtonList1.SelectedValue);

        DateTime from,to;
        to = Convert.ToDateTime(txtTo.Text);
        from = to;
        if(tip==1)
        {
            from = to.AddDays(-6);
        }
        else if(tip ==2)
        {
            if(to.Day < 16)
            {
                from = to.AddDays(-to.Day + 1);                
            }
            else if(to.Day > 15)
            {
                from = to.AddDays(-to.Day + 16);
            }
        }
        else if(tip == 3)
        {
            from = to.AddDays(-to.Day + 1);                
        }
        
        lblUpdate.Visible = true;
        lblUpdate.Text = "Report From " + from.ToShortDateString() + " To " + to.ToShortDateString();
        string qry;
        DataSet dattss;

        qry = "SELECT Customer,SUM(BillableCalls) as Calls,ROUND(SUM(CustMinutes),2) as CMinutes,ROUND(SUM(VendorMinutes),2) as VMinutes,ROUND(SUM(TotalCost),2) as TotalCost,ROUND(SUM(TotalPrice),2) as TotalSales FROM MECCA2..TrafficLog WHERE BillingDate BETWEEN '" + from.ToShortDateString() + "' AND '" + to.ToShortDateString() + "' AND Customer IN (SELECT MeccaName FROM CDRDB..Invoice WHERE tip = " + tip.ToString() + ") AND CustMinutes > 0 GROUP BY Customer ORDER BY Customer";
        SqlCommand sqlSpQuery2 = new SqlCommand();
        sqlSpQuery2.CommandType = System.Data.CommandType.Text;
        sqlSpQuery2.CommandText = qry;
        dattss = RunQuery(sqlSpQuery2);
        GridView1.DataSource = dattss.Tables[0];
        GridView1.DataBind();
        
      
    }
    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

}
