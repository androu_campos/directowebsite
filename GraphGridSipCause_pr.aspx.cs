using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;
using System.Data.SqlClient;

public partial class GraphGrid : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Session["DirectoUser"] as string))//Usuario Logeado            
        {
            String queryCustomer = null;
            String queryVendor = null;
            DataTable tblCustomer;
            DataTable tblVendor;

            //RECENT GRAPHS

            queryCustomer = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetINC join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode and Customer like 'CC-TKM' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY CustSipCause, Definition ORDER BY CustSipCause";
            queryVendor = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetINC join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode and Customer like 'CC-TKM' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY VendSipCause, Definition ORDER BY VendSipCause";

            tblCustomer = Util.RunQueryByStmntatCDRDB(queryCustomer).Tables[0];
            tblVendor = Util.RunQueryByStmntatCDRDB(queryVendor).Tables[0];

            makeChart1(tblCustomer, tblVendor, "CC-TKM      Recent");

            tblCustomer.Dispose();
            tblVendor.Dispose();

            queryCustomer = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetINC join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode and Customer like 'CC-TKM_10D' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY CustSipCause, Definition ORDER BY CustSipCause";
            queryVendor = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetINC join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode and Customer like 'CC-TKM_10D' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY VendSipCause, Definition ORDER BY VendSipCause";

            tblCustomer = Util.RunQueryByStmntatCDRDB(queryCustomer).Tables[0];
            tblVendor = Util.RunQueryByStmntatCDRDB(queryVendor).Tables[0];

            makeChart2(tblCustomer, tblVendor, "CC-TKM_10D     Recent");

            tblCustomer.Dispose();
            tblVendor.Dispose();

            queryCustomer = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetINC join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode and Customer like 'CC-TKM_P3' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY CustSipCause, Definition ORDER BY CustSipCause";
            queryVendor = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetINC join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode and Customer like 'CC-TKM_P3' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY VendSipCause, Definition ORDER BY VendSipCause";

            tblCustomer = Util.RunQueryByStmntatCDRDB(queryCustomer).Tables[0];
            tblVendor = Util.RunQueryByStmntatCDRDB(queryVendor).Tables[0];

            makeChart3(tblCustomer, tblVendor, "CC-TKM_P3      Recent");

            queryCustomer = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetINC join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode and Customer like 'CC-TKM-RELOX' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY CustSipCause, Definition ORDER BY CustSipCause";
            queryVendor = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetINC join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode and Customer like 'CC-TKM-RELOX' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY VendSipCause, Definition ORDER BY VendSipCause";

            tblCustomer = Util.RunQueryByStmntatCDRDB(queryCustomer).Tables[0];
            tblVendor = Util.RunQueryByStmntatCDRDB(queryVendor).Tables[0];

            makeChart7(tblCustomer, tblVendor, "CC-TKM-RELOX      Recent");

            tblCustomer.Dispose();
            tblVendor.Dispose();


            //queryCustomer = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetINC join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode and Customer like 'CC-BANCOMER' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY CustSipCause, Definition ORDER BY CustSipCause";
            //queryVendor = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetINC join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode and Customer like 'CC-BANCOMER' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY VendSipCause, Definition ORDER BY VendSipCause";

            //tblCustomer = Util.RunQueryByStmntatCDRDB(queryCustomer).Tables[0];
            //tblVendor = Util.RunQueryByStmntatCDRDB(queryVendor).Tables[0];

            //makeChart7(tblCustomer, tblVendor, "CC-BANCOMER      Recent");

            //tblCustomer.Dispose();
            //tblVendor.Dispose();


            //TODAYS GRAPHS

            queryCustomer = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetALL join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode and Customer like 'CC-TKM' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY CustSipCause, Definition ORDER BY CustSipCause";
            queryVendor = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetALL join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode and Customer like 'CC-TKM' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY VendSipCause, Definition ORDER BY VendSipCause";

            tblCustomer = Util.RunQueryByStmntatCDRDB(queryCustomer).Tables[0];
            tblVendor = Util.RunQueryByStmntatCDRDB(queryVendor).Tables[0];

            makeChart4(tblCustomer, tblVendor, "CC-TKM      Today");

            tblCustomer.Dispose();
            tblVendor.Dispose();

            queryCustomer = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetALL join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode and Customer like 'CC-TKM_10D' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY CustSipCause, Definition ORDER BY CustSipCause";
            queryVendor = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetALL join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode and Customer like 'CC-TKM_10D' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY VendSipCause, Definition ORDER BY VendSipCause";

            tblCustomer = Util.RunQueryByStmntatCDRDB(queryCustomer).Tables[0];
            tblVendor = Util.RunQueryByStmntatCDRDB(queryVendor).Tables[0];

            makeChart5(tblCustomer, tblVendor, "CC-TKM_10D     Today");

            tblCustomer.Dispose();
            tblVendor.Dispose();

            queryCustomer = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetALL join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode and Customer like 'CC-TKM_P3' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY CustSipCause, Definition ORDER BY CustSipCause";
            queryVendor = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetALL join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode and Customer like 'CC-TKM_P3' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY VendSipCause, Definition ORDER BY VendSipCause";

            tblCustomer = Util.RunQueryByStmntatCDRDB(queryCustomer).Tables[0];
            tblVendor = Util.RunQueryByStmntatCDRDB(queryVendor).Tables[0];

            makeChart6(tblCustomer, tblVendor, "CC-TKM_P3      Today");

            tblCustomer.Dispose();
            tblVendor.Dispose();

            queryCustomer = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetALL join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode and Customer like 'CC-TKM-RELOX' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY CustSipCause, Definition ORDER BY CustSipCause";
            queryVendor = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetALL join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode and Customer like 'CC-TKM-RELOX' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY VendSipCause, Definition ORDER BY VendSipCause";

            tblCustomer = Util.RunQueryByStmntatCDRDB(queryCustomer).Tables[0];
            tblVendor = Util.RunQueryByStmntatCDRDB(queryVendor).Tables[0];

            makeChart8(tblCustomer, tblVendor, "CC-TKM-RELOX     Today");

            tblCustomer.Dispose();
            tblVendor.Dispose();
            //queryCustomer = "SELECT SUM(Attempts) Attempts, CustSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetALL join CDRDB.dbo.SIPCauseCodes on custsipcause = SIPCauseCode and Customer like 'CC-BANCOMER' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY CustSipCause, Definition ORDER BY CustSipCause";
            //queryVendor = "SELECT SUM(Attempts) Attempts, VendSipCause as SIPCause, Definition FROM MECCA2.dbo.OpSheetALL join CDRDB.dbo.SIPCauseCodes on VendSipCause = SIPCauseCode and Customer like 'CC-BANCOMER' and vendor like '%' and Country like '%' and LMC like '%' and Type like '%' GROUP BY VendSipCause, Definition ORDER BY VendSipCause";

            //tblCustomer = Util.RunQueryByStmntatCDRDB(queryCustomer).Tables[0];
            //tblVendor = Util.RunQueryByStmntatCDRDB(queryVendor).Tables[0];

            //makeChart8(tblCustomer, tblVendor, "CC-BANCOMER      Today");

            //tblCustomer.Dispose();
            //tblVendor.Dispose();
            

            loadSipList(e);
        }
        else
        {
            Response.Redirect("~/SignIn.aspx");
        }

    }

    private void loadSipList(EventArgs e)
    {
        String qry = null;
        DataTable sipCodes = null;
        
        SipList.Text = "";
        SipList100.Text = "";
        SipList200.Text = "";
        SipList300.Text = "";
        SipList400.Text = "";
        SipList500.Text = "";
        SipList600.Text = "";
        qry = "SELECT SIPCauseCode 'Code', Definition from CDRDB.dbo.SIPCauseCodes  where SIPCauseCode < 100 order by SIPCauseCode";
        sipCodes = Util.RunQueryByStmntatCDRDB(qry).Tables[0];

        foreach (DataRow row in sipCodes.Rows)
        {
            SipList.Text += "<b>" + row["Code"].ToString() + "</b> - " + row["Definition"].ToString() + "   ";
        }

        sipCodes.Dispose();

        qry = "SELECT SIPCauseCode 'Code', Definition from CDRDB.dbo.SIPCauseCodes where SIPCauseCode >= 100 and SIPCauseCode < 200 order by SIPCauseCode ";
        sipCodes = Util.RunQueryByStmntatCDRDB(qry).Tables[0];

        foreach (DataRow row in sipCodes.Rows)
        {
            SipList100.Text += "<b>" + row["Code"].ToString() + "</b> - " + row["Definition"].ToString() + "   ";
        }

        sipCodes.Dispose();

        qry = "SELECT SIPCauseCode 'Code', Definition from CDRDB.dbo.SIPCauseCodes  where SIPCauseCode >= 200 and SIPCauseCode < 300 order by SIPCauseCode";
        sipCodes = Util.RunQueryByStmntatCDRDB(qry).Tables[0];

        foreach (DataRow row in sipCodes.Rows)
        {
            SipList200.Text += "<b>" + row["Code"].ToString() + "</b> - " + row["Definition"].ToString() + "   ";
        }

        sipCodes.Dispose();

        qry = "SELECT SIPCauseCode 'Code', Definition from CDRDB.dbo.SIPCauseCodes  where SIPCauseCode >= 300 and SIPCauseCode < 400 order by SIPCauseCode";
        sipCodes = Util.RunQueryByStmntatCDRDB(qry).Tables[0];

        foreach (DataRow row in sipCodes.Rows)
        {
            SipList300.Text += "<b>" + row["Code"].ToString() + "</b> - " + row["Definition"].ToString() + "   ";
        }

        sipCodes.Dispose();

        qry = "SELECT SIPCauseCode 'Code', Definition from CDRDB.dbo.SIPCauseCodes  where SIPCauseCode >= 400 and SIPCauseCode < 500 order by SIPCauseCode";
        sipCodes = Util.RunQueryByStmntatCDRDB(qry).Tables[0];

        foreach (DataRow row in sipCodes.Rows)
        {
            SipList400.Text += "<b>" + row["Code"].ToString() + "</b> - " + row["Definition"].ToString() + "   ";
        }

        sipCodes.Dispose();

        qry = "SELECT SIPCauseCode 'Code', Definition from CDRDB.dbo.SIPCauseCodes  where SIPCauseCode >= 500 and SIPCauseCode < 600 order by SIPCauseCode";
        sipCodes = Util.RunQueryByStmntatCDRDB(qry).Tables[0];

        foreach (DataRow row in sipCodes.Rows)
        {
            SipList500.Text += "<b>" + row["Code"].ToString() + "</b> - " + row["Definition"].ToString() + "   ";
        }

        sipCodes.Dispose();

        qry = "SELECT SIPCauseCode 'Code', Definition from CDRDB.dbo.SIPCauseCodes  where SIPCauseCode >= 600 and SIPCauseCode < 700 order by SIPCauseCode";
        sipCodes = Util.RunQueryByStmntatCDRDB(qry).Tables[0];

        foreach (DataRow row in sipCodes.Rows)
        {
            SipList600.Text += "<b>" + row["Code"].ToString() + "</b> - " + row["Definition"].ToString() + "   ";
        }

        sipCodes.Dispose();

    }



    private void makeChart1(DataTable tbl, DataTable tbl2, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0 || tbl2.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }



            Chart1.Series["Attempts Customer"].Points.DataBindXY(xValues, yValues);
            Chart1.Series["Attempts Customer"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";

            yValues = new double[tbl2.Rows.Count];
            xValues = new string[tbl2.Rows.Count];
            i = 0;

            foreach (DataRow myRow in tbl2.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }

            Chart1.Series["Attempts Vendor"].Points.DataBindXY(xValues, yValues);
            Chart1.Series["Attempts Vendor"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";
            Chart1.Titles["Title1"].Text = tITLE;
            
        }        
        else
        {
            this.Chart1.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";
        }

    }

    private void makeChart2(DataTable tbl, DataTable tbl2, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0 || tbl2.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }



            Chart2.Series["Attempts Customer"].Points.DataBindXY(xValues, yValues);
            Chart2.Series["Attempts Customer"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";

            yValues = new double[tbl2.Rows.Count];
            xValues = new string[tbl2.Rows.Count];
            i = 0;

            foreach (DataRow myRow in tbl2.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }

            Chart2.Series["Attempts Vendor"].Points.DataBindXY(xValues, yValues);
            Chart2.Series["Attempts Vendor"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";
            Chart2.Titles["Title1"].Text = tITLE;
        }
        else
        {
            this.Chart2.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";  
        }

    }

    private void makeChart3(DataTable tbl, DataTable tbl2, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0 || tbl2.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }



            Chart3.Series["Attempts Customer"].Points.DataBindXY(xValues, yValues);
            Chart3.Series["Attempts Customer"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";

            yValues = new double[tbl2.Rows.Count];
            xValues = new string[tbl2.Rows.Count];
            i = 0;

            foreach (DataRow myRow in tbl2.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }

            Chart3.Series["Attempts Vendor"].Points.DataBindXY(xValues, yValues);
            Chart3.Series["Attempts Vendor"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";
            Chart3.Titles["Title1"].Text = tITLE;
        }
        else
        {
            this.Chart3.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";  
        }

    }

    private void makeChart4(DataTable tbl, DataTable tbl2, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0 || tbl2.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }



            Chart4.Series["Attempts Customer"].Points.DataBindXY(xValues, yValues);
            Chart4.Series["Attempts Customer"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";

            yValues = new double[tbl2.Rows.Count];
            xValues = new string[tbl2.Rows.Count];
            i = 0;

            foreach (DataRow myRow in tbl2.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }

            Chart4.Series["Attempts Vendor"].Points.DataBindXY(xValues, yValues);
            Chart4.Series["Attempts Vendor"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";
            Chart4.Titles["Title1"].Text = tITLE;
        }
        else
        {
            this.Chart4.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";  
        }

    }

    private void makeChart5(DataTable tbl, DataTable tbl2, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0 || tbl2.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }



            Chart5.Series["Attempts Customer"].Points.DataBindXY(xValues, yValues);
            Chart5.Series["Attempts Customer"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";

            yValues = new double[tbl2.Rows.Count];
            xValues = new string[tbl2.Rows.Count];
            i = 0;

            foreach (DataRow myRow in tbl2.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }

            Chart5.Series["Attempts Vendor"].Points.DataBindXY(xValues, yValues);
            Chart5.Series["Attempts Vendor"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";
            Chart5.Titles["Title1"].Text = tITLE;
        }
        else
        {
            this.Chart5.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";  
        }

    }

    private void makeChart6(DataTable tbl, DataTable tbl2, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0 || tbl2.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }



            Chart6.Series["Attempts Customer"].Points.DataBindXY(xValues, yValues);
            Chart6.Series["Attempts Customer"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";

            yValues = new double[tbl2.Rows.Count];
            xValues = new string[tbl2.Rows.Count];
            i = 0;

            foreach (DataRow myRow in tbl2.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }

            Chart6.Series["Attempts Vendor"].Points.DataBindXY(xValues, yValues);
            Chart6.Series["Attempts Vendor"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";
            Chart6.Titles["Title1"].Text = tITLE;
        }
        else
        {
            this.Chart6.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";  
        }

    }

    private void makeChart7(DataTable tbl, DataTable tbl2, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0 || tbl2.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }



            Chart7.Series["Attempts Customer"].Points.DataBindXY(xValues, yValues);
            Chart7.Series["Attempts Customer"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";

            yValues = new double[tbl2.Rows.Count];
            xValues = new string[tbl2.Rows.Count];
            i = 0;

            foreach (DataRow myRow in tbl2.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }

            Chart7.Series["Attempts Vendor"].Points.DataBindXY(xValues, yValues);
            Chart7.Series["Attempts Vendor"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";
            Chart7.Titles["Title1"].Text = tITLE;
        }
        else
        {
            this.Chart7.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";  
        }

    }

    private void makeChart8(DataTable tbl, DataTable tbl2, string tITLE)
    {
        double[] yValues;
        string[] xValues;
        int i = 0;

        if (tbl.Rows.Count > 0 || tbl2.Rows.Count > 0)
        {
            yValues = new double[tbl.Rows.Count];
            xValues = new string[tbl.Rows.Count];

            foreach (DataRow myRow in tbl.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }



            Chart8.Series["Attempts Customer"].Points.DataBindXY(xValues, yValues);
            Chart8.Series["Attempts Customer"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";

            yValues = new double[tbl2.Rows.Count];
            xValues = new string[tbl2.Rows.Count];
            i = 0;

            foreach (DataRow myRow in tbl2.Rows)
            {
                xValues[i] = myRow["SIPCause"].ToString();
                yValues[i] = Convert.ToDouble(myRow["Attempts"]);
                i++;
            }

            Chart8.Series["Attempts Vendor"].Points.DataBindXY(xValues, yValues);
            Chart8.Series["Attempts Vendor"].ToolTip = "Cause = #VALX\nAttempts = #VALY{0,0}";
            Chart8.Titles["Title1"].Text = tITLE;
        }
        else
        {
            this.Chart8.Visible = false;
            //lblWarning.Visible = true;
            //lblWarning.Text = "Nothing found!!";  
        }

    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
