<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" 
CodeFile="EZLCRPrefix.aspx.cs" Inherits="EZLCRPrefix" Title="DIRECTO - Connections Worldwide"
StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table>
        <tr>
            <td align="left" colspan="2" style="width: 185px; height: 32px">
                <asp:Label ID="Label4" runat="server"
                    Text="Add Prefix" CssClass="labelTurk"></asp:Label></td>
            
        </tr>
        <tr>
           <td style="width: 185px" align="right">
                <asp:Label ID="prefixlbl" runat="server" Text="Prefix:" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 124px">
                <asp:TextBox ID="prefixtxt" runat="server" Width="156px" CssClass="labelSteelBlue"></asp:TextBox>
            </td>
        </tr>
        <tr>            
            <td style="width: 185px" align="right">
                <asp:Label ID="idlbl" runat="server" Text="ID:" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 124px">
                <asp:TextBox ID="idtxt" runat="server" Width="156px" CssClass="labelSteelBlue"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 185px">
            </td>
            <td align="left" style="width: 124px">
                <asp:Button ID="Button1" runat="server" Text="Add Prefix" OnClick="Button1_Click" CssClass="boton" />
            </td>             
        </tr>
    </table>

</asp:Content>

