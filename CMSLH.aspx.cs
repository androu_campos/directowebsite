using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using Microsoft.Vbe.Interop;

public partial class MasterSales : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string Query;
        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandTimeout = 350;
        sqlSpQuery.CommandType = System.Data.CommandType.Text;
        Query = Session["SalesLogS"].ToString();

        //Response.Write(Query);
        //Response.End();

        sqlSpQuery.CommandText = Query;
        DataSet ResultSet;
        ResultSet = Util.RunQuery(sqlSpQuery);

        Session["ResultSetMSLH"] = ResultSet;

        int jr = ResultSet.Tables[0].Rows.Count;
        if (jr <= 0)
        {
            Label1.Visible = true;
            Button1.Visible = false;
        }
        else
        {
            this.GridView1.AutoGenerateColumns = false;
            this.GridView1.DataSource = ResultSet;
            this.GridView1.Columns.Clear();
            for (int c = 0; c < ResultSet.Tables[0].Columns.Count; c++)
            {
                string columnName = ResultSet.Tables[0].Columns[c].ColumnName.ToString();
                BoundField columna = new BoundField();
                columna.HtmlEncode = false;
                if (columnName == "TotalCost" || columnName == "TotalSales" || columnName == "Profit")
                {
                    columna.DataFormatString = "${0:f2}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "CMinutes" || columnName == "VMinutes")
                {
                    columna.DataFormatString = "{0:0,0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "AnsweredCalls")
                {
                    columna.DataFormatString = "{0:0,0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 100;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "ASR")
                {
                    columna.DataFormatString = "{0:0,0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "ACD")
                {
                    columna.DataFormatString = "{0:0.0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "ABR")
                {
                    columna.DataFormatString = "{0:0.0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Attempts")
                {
                    columna.DataFormatString = "{0:0,0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Rejected Calls")
                {
                    columna.DataFormatString = "{0:0,0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 440;
                    columna.HeaderStyle.Width = 440;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Country")
                {

                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 100;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Cost" || columnName == "Price")
                {
                    columna.DataFormatString = "${0:f4}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;

                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Customer")
                {
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    //columna.ItemStyle.Width = 450;
                    columna.ItemStyle.Wrap = false;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Vendor")
                {
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //columna.ItemStyle.Width = 100;
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Wrap = false;
                }
                else if (columnName == "Region")
                {
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Wrap = false;
                }
                else if (columnName == "LMC")
                {
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //columna.ItemStyle.Width = 190;
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                }


                columna.DataField = columnName;
                columna.HeaderText = columnName;

                GridView1.Columns.Add(columna);
                GridView1.DataBind();
            }

            Label1.Visible = false;
        }

        GridView1.RowStyle.HorizontalAlign = HorizontalAlign.Justify;
        GridView1.RowStyle.BorderWidth = 0;
        GridView1.RowStyle.Font.Name = "Arial";
        GridView1.RowStyle.Font.Size = 8;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        GridView1.AllowPaging = false;
        System.Data.DataTable dtx = new System.Data.DataTable("Report");
        string attach = "attachement; filename= Report.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attach);
        Response.ContentType = "application/vnd.ms-excel";
        string tab = "";

        foreach (TableCell cell in GridView1.HeaderRow.Cells)
            dtx.Columns.Add(cell.Text);
        foreach (GridViewRow row in GridView1.Rows)
        {
            dtx.Rows.Add();
            for (int i = 0; i < row.Cells.Count; i++)
                dtx.Rows[dtx.Rows.Count - 1][i] = row.Cells[i].Text;
        }

        foreach (DataColumn dc in dtx.Columns)
        {
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");

        foreach (DataRow dr in dtx.Rows)
        {
            tab = "";
            for (int i = 1; i < dtx.Rows.Count; i++)
            {
                Response.Write(tab + dr[i].ToString());
                tab = "\t";
            }
            tab = "\n";
        }
        Response.End();
    }

    //HttpContext.Current.Response.Clear();
    //HttpContext.Current.Response.AddHeader(
    //    "content-disposition", string.Format("attachment; filename=Report.csv"));
    //HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";

    //using (StringWriter sw = new StringWriter())    
    //{
    //    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
    //    {
    //        //  Create a form to contain the grid
    //        Table table = new Table();

    //        //  add the header row to the table
    //        if (GridView1.HeaderRow != null)
    //            table.Rows.Add(GridView1.HeaderRow);

    //        //  add each of the data rows to the table
    //        foreach (GridViewRow row in GridView1.Rows)
    //        {
    //            table.Rows.Add(row);
    //        }

    //        //  add the footer row to the table
    //        if (GridView1.FooterRow != null)
    //            table.Rows.Add(GridView1.FooterRow);

    //        //  render the table into the htmlwriter
    //        table.RenderControl(htw);

    //        //  render the htmlwriter into the response
    //        HttpContext.Current.Response.Write(sw.ToString());
    //        HttpContext.Current.Response.End();
    //    }
    //}
    //}
    //DataTable dt = new DataTable("Report");
    //foreach (TableCell cell in GridView1.HeaderRow.Cells)
    //    dt.Columns.Add(cell.Text);
    //foreach (GridViewRow row in GridView1.Rows)
    //{
    //    dt.Rows.Add();
    //    for (int i = 0; i < row.Cells.Count; i++)
    //        dt.Rows[dt.Rows.Count - 1][i] = row.Cells[i].Text;
    //}
    //using (ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkBook())
    //{
    //    wb.Worksheets.Add(dt);

    //    Response.Buffer = true;
    //    Response.Charset = "";
    //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    //    Response.AddHeader("content-disposition", "attachement;filename=MasterSalesLog.xlsx");
    //    using (MemoryStream MyMem = new MemoryStream())
    //    {
    //        wb.SaveAs(MyMem);
    //        MyMem.WriteTo(Response.OutputStream);
    //        Response.Flush();
    //        Response.End();
    //    }
    //}

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {

    }
}
