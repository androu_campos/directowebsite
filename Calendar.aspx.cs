using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Calendar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Calen_SelectionChanged(object sender, EventArgs e)
    {
        string obj = Request.QueryString["textbox"];
        if (obj != null)
        {
            string strScript = "<script>window.opener.document.getElementById ('" + obj + "').value = '" + Calen.SelectedDate.ToString("MM/dd/yyyy") + "';self.close()" + "</" + "script>";
            string strScript2 = "<script>window.opener.document.getElementById ('" + obj + "V').value = '" + Calen.SelectedDate.ToString("MM/dd/yyyy") + "';self.close()" + "</" + "script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Calen_ChangeDa", strScript2);
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Calen_ChangeDate", strScript);

        }
    }
}
