using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;

public partial class BlackList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DirectoUser"].ToString() == "cc-bconnectsamarilla" || Session["DirectoUser"].ToString() == "santander-demo")
        {

        }
        else
        {
            Response.Redirect("HomeCCCUST.aspx", false);
        }

        if (!IsPostBack)
        {
            DataView dvSql = (DataView)sqlsacount.Select(DataSourceSelectArguments.Empty);

            foreach (DataRowView drvSql in dvSql)
                txtCount.Text = drvSql["Count"].ToString();
        }
    }

    protected void lnkTab1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/BlackList.aspx");
    }
    protected void btnApply_Click(object sender, EventArgs e)
    {

    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        sqlscountres.DataBind();
        gvGood.Visible = false;
        txtCount.Visible = false;
        gvSearch.Visible = true;
        lblCount.Visible = true;
        txtCountRes.Visible = true;

        if (gvGood.Visible == false || gvSearch.Visible == true)
        {
            DataView dvSql = (DataView)sqlscountres.Select(DataSourceSelectArguments.Empty);

            foreach (DataRowView drvSql in dvSql)
                txtCountRes.Text = drvSql["Count"].ToString();
        }

        if (gvSearch.Rows.Count == 0)
        {
            lblError.Visible = true;
            lblError.Text = "Not Found!";
        }
        else
        {
            lblError.Visible = false;
        }

    }
}


