using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SessionUsuario
/// </summary>
public class SessionUsuario : System.Web.UI.Page
{
    public string Rol
    {
        get
        {
            return Session["Rol"] as string;
        }
        set
        {
            Session["Rol"] = value;
        }
    }

    public string DirectoUser
    {
        get
        {
            return Session["DirectoUser"] as string;
        }
        set
        {
            Session["DirectoUser"] = value;
        }

    }
    public string BrokerName
    {
        get
        {
            return Session["BrokerName"] as string;
        }
        set
        {
            Session["BrokerName"] = value;
        }

    }
    public bool EsSessionValida()
    {
        bool result = false;
        if (!string.IsNullOrEmpty(Rol) && !string.IsNullOrEmpty(DirectoUser))
            result = true;
        return result;
    }
}
