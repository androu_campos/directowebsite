using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;

public partial class CDRSbyWeek : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string user;
        user = Session["DirectoUser"].ToString();

        if (user == "arturo.marquez" || user == "rodrigo.andrade" || user == "isabel.reyes")
        {
            StringBuilder content = new StringBuilder();

            DirectoryInfo dir = new DirectoryInfo("E:/Mecca/RatedCDRS/week/");

            FileSystemInfo[] dirOn = dir.GetFileSystemInfos();
            foreach (FileSystemInfo fi in dirOn)
            {
                content.Append("<br><a href=http://mecca.directo.com/week/" + fi.Name + " target=" + "_blank" + ">" + fi.Name + "</a>");
            }

            lblContent.Text = content.ToString();
        }
        else
        {
            Response.Redirect("ContactUs.aspx", false);
        }
    }
}
