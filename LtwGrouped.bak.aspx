<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LtwGrouped.bak.aspx.cs" Inherits="LtwGrouped" StylesheetTheme="Theme1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Live Traffic Watch</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="320">
            <tr>
                <td style="width: 100px" align="center" bgcolor="#d5e3f0" valign="middle">
                    <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Customers"></asp:Label></td>
                <td align="center" style="width: 100px" valign="middle">
                </td>
                <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                    <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Vendors"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px" align="center" height="100%" rowspan="2">
                    <asp:Label ID="lblCustomer" runat="server" Font-Names="Arial" Font-Size="8pt" Text="Label"></asp:Label></td>
                <td align="center" height="100%" rowspan="2" style="width: 100px">
                </td>
                <td style="width: 100px" align="center" height="100%" rowspan="2" valign="top">
                    <asp:Label ID="lblVendor" runat="server" Font-Names="Arial" Font-Size="8pt" Text="Label"></asp:Label></td>
            </tr>
            <tr>
            </tr>
        </table>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <asp:Timer ID="Timer1" runat="server" Interval="600000">
        </asp:Timer>
    
    </div>
    </form>
</body>
</html>
