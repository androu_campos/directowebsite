using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class AssignRounding : System.Web.UI.Page
{
   
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["DirectoUser"].ToString() == "yurema.villa" || Session["DirectoUser"].ToString() == "jorge.ibarra" || Session["Rol"].ToString() == "Admins")
        {


        }
        else
        {
            Response.Redirect("Home.aspx", false);
        
        }
        
    }

    protected void ddlProvider_SelectedIndexChanged(object sender, EventArgs e)
    {


        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;

        sqlSpQuery.CommandText = "sp_GetRoundingAssign";

        sqlSpQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = rblProvider.SelectedValue.ToString();

        sqlSpQuery.Parameters.Add("@provider", SqlDbType.VarChar).Value = ddlProvider.SelectedValue.ToString();

        DataSet ResultSet;
        ResultSet = RunQuery(sqlSpQuery);
        gvData.DataSource = ResultSet;
        gvData.DataBind();
        //Session["ds"] = ResultSet;
        btnUpdate.Enabled = true;
        btnUpdate.Width = gvData.Width;
        rblOptions.Enabled = true;
        rblOptions.SelectedValue = "0";       
    }


    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString =
            ConfigurationManager.ConnectionStrings
            ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
            //labelStatus.Text = "Unable to connect to SQL Server.";
        }
        return resultsDataSet;
    }


    protected void rblProvider_SelectedIndexChanged(object sender, EventArgs e)
    {

        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;

        sqlSpQuery.CommandText = "sp_GetRoundingAssign";

        sqlSpQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = rblProvider.SelectedValue.ToString();

        sqlSpQuery.Parameters.Add("@provider", SqlDbType.VarChar).Value = ddlProvider.SelectedValue.ToString();

        DataSet ResultSet;
        ResultSet = RunQuery(sqlSpQuery);
        gvData.DataSource = ResultSet;
        gvData.DataBind();
        //Session["ds"] = ResultSet;
        btnUpdate.Enabled = true;
        rblOptions.Enabled = true;
        rblOptions.SelectedValue = "0";
    }


    protected void btnUpdate_Click(object sender, EventArgs e)
    {


        string Provider = String.Empty;
        string Country = String.Empty;

        string Rounding = String.Empty;

        DataTable dt = new DataTable();

        dt.Columns.Add("Provider");
        dt.Columns.Add("Country");
        dt.Columns.Add("Rounding");

        foreach (GridViewRow row in gvData.Rows)
        {
            Provider = ((Label)row.FindControl("LProvider")).Text;
            Country = ((Label)row.FindControl("LCountry")).Text;
            Rounding = ((DropDownList)row.FindControl("ddlRounding")).SelectedItem.Value;


            DataRow dr = dt.NewRow();

            dr["Provider"] = Provider;
            dr["Country"] = Country;
            dr["Rounding"] = Rounding;

            dt.Rows.Add(dr);

        }

        string query = String.Empty;
        if (rblProvider.SelectedValue == "Customer")
        {
            query = "DELETE FROM [CDRDB].[dbo].[CustRounding] WHERE CustName = '" + Provider + "'";
        }
        else  //(rblProvider.SelectedValue == "Vendor")
        {
            query = "DELETE FROM [CDRDB].[dbo].[VendorRounding] WHERE VendorName = '" + Provider + "'";
        }
        SqlCommand sQuery = new SqlCommand();
        sQuery.CommandType = CommandType.Text;
        sQuery.CommandText = query;
        RunQueryE(sQuery);

        if (rblProvider.SelectedValue == "Customer")
        {
            foreach (DataRow rr in dt.Rows)
            {
                string pro, cou, rou;
                pro = rr["Provider"].ToString();
                cou = rr["Country"].ToString();
                rou = rr["Rounding"].ToString();
                query = "INSERT INTO CDRDB..CustRounding (CustName,Country,Rounding)  VALUES ('" + pro + "','" + cou + "','" + rou + "')";
                if (rou != "0")
                {
                    SqlCommand sqlSpQuery = new SqlCommand();
                    sqlSpQuery.CommandType = CommandType.StoredProcedure;
                    sqlSpQuery.CommandText = "sp_RoundingAssignInser";
                    sqlSpQuery.Parameters.Add("@provider", SqlDbType.VarChar).Value = pro;
                    sqlSpQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = cou;
                    sqlSpQuery.Parameters.Add("@rounding", SqlDbType.VarChar).Value = rou;
                    sqlSpQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = "Customer";
                    RunQueryE(sqlSpQuery);

                }
            }

        }
        else  //(rblProvider.SelectedValue == "Vendor")
        {
            foreach (DataRow rr in dt.Rows)
            {
                string pro, cou, rou;
                pro = rr["Provider"].ToString();
                cou = rr["Country"].ToString();
                rou = rr["Rounding"].ToString();

                if (rou != "0")
                {
                    SqlCommand sqlSpQuery = new SqlCommand();
                    sqlSpQuery.CommandType = CommandType.StoredProcedure;
                    sqlSpQuery.CommandText = "sp_RoundingAssignInser";
                    sqlSpQuery.Parameters.Add("@provider", SqlDbType.VarChar).Value = pro;
                    sqlSpQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = cou;
                    sqlSpQuery.Parameters.Add("@rounding", SqlDbType.VarChar).Value = rou;
                    sqlSpQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = "Vendor";
                    RunQueryE(sqlSpQuery);
                }

            }
        }

        

    }


    protected void rblOptions_SelectedIndexChanged(object sender, EventArgs e)
    {
        int op = Convert.ToInt32(rblOptions.SelectedValue);

        string Provider = String.Empty;
        string Country = String.Empty;
        string Rounding = String.Empty;

        DataTable dt = new DataTable();

        dt.Columns.Add("Provider");
        dt.Columns.Add("Country");
        dt.Columns.Add("Rounding");

        foreach (GridViewRow row in gvData.Rows)
        {
            Provider = ((Label)row.FindControl("LProvider")).Text;
            Country = ((Label)row.FindControl("LCountry")).Text;
            Rounding = ((DropDownList)row.FindControl("ddlRounding")).SelectedItem.Value;

            DataRow dr = dt.NewRow();

            dr["Provider"] = Provider;
            dr["Country"] = Country;
            if (op == 601 & Country == "MEXICO")
            {
                dr["Rounding"] = "60";
            }
            else if (op == 601 & Country != "MEXICO")
            {
                dr["Rounding"] = "1";
            }
            else
            {
                dr["Rounding"] = op.ToString();
            }



            dt.Rows.Add(dr);

        }

        gvData.DataSource = dt;

        gvData.DataBind(); 
    }


    private void RunQueryE(SqlCommand sqlQuery)
    {

        string connectionString =
            ConfigurationManager.ConnectionStrings
            ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection =
            new SqlConnection(connectionString);

        sqlQuery.Connection = DBConnection;
        DBConnection.Open();
        sqlQuery.ExecuteNonQuery();
        DBConnection.Close();

    }

}
