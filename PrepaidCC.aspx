<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PrepaidCC.aspx.cs"
    Inherits="PrepaidCC" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td width="30%">
                <asp:Label ID="LabelHeader" runat="server" Text="PREPAID CALL CENTER" Width="194px"
                    Font-Bold="True" CssClass="labelTurk"></asp:Label></td>
            <td width="70%">
            </td>
        </tr>
        <tr>
            <td height="100%" width="30%">
                <table align="left" width="100%">
                    <tr>
                        <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                            <asp:Label ID="lblRZ" runat="server" Text="Company:" CssClass="labelBlue8"></asp:Label></td>
                        <td style="width: 183px" colspan="3">
                            <asp:DropDownList ID="dpdRz" runat="server" DataTextField="RazonSocial" DataValueField="RazonSocial"
                                Width="550px" AutoPostBack="true" CssClass="dropdown" OnSelectedIndexChanged="dpdRz_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                            <asp:Label ID="lblCallCenter" runat="server" Text="CallCenter:" CssClass="labelBlue8"></asp:Label></td>
                        <td style="width: 183px">
                            <asp:ListBox ID="lbCallCenter" runat="server" CssClass="text" Width="200px"></asp:ListBox>
                            <%--<asp:DropDownList ID="dpdCallCenter" runat="server" DataTextField="Customer" DataValueField="Customer"
                                Width="130px" CssClass="dropdown">
                            </asp:DropDownList>--%>
                        </td>
                        <td align="center">
                        <asp:Button runat="server" ID="btnAddAll" Text=">>" CssClass="boton" OnClick="btnAddAll_Click"/>
                        <br />
                            <asp:Button runat="server" ID="btnAdd" Text=">" CssClass="boton" OnClick="btnAdd_Click" />
                            <br />
                            <asp:Button runat="server" ID="btnRemove" Text="<" CssClass="boton" OnClick="btnRemove_Click" />
                            <br />
                            <asp:Button runat="server" ID="btnRemAll" Text="<<" CssClass="boton" OnClick="btnRemAll_Click"/>
                        </td>
                        <td>
                            <asp:ListBox ID="lbCallCenterPP" runat="server" CssClass="text" Width="200px"></asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                            <asp:Label ID="lblAmount" runat="server" Text="Prepaid Amount: " CssClass="labelBlue8"></asp:Label></td>
                        <td style="width: 183px">
                            <asp:TextBox ID="txtAmount" runat="server" CssClass="text"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator ID="txtAmount_Validator" runat="server" ErrorMessage="Only Amounts"
                                ControlToValidate="txtAmount" ValidationExpression="^\d+(\.\d\d)?$" CssClass="lblNotFound">
                            </asp:RegularExpressionValidator>
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="cmdApply" runat="server" OnClick="cmdQbuilder_Click" Text="Apply prepay"
                                BorderStyle="Solid" BorderWidth="1px" Font-Bold="False" CssClass="boton" />
                        </td>
                        <td align="left" colspan="10">
                            <asp:Button ID="btnShow" runat="server" Text="Show Prepaid CCs" Width="110px" BorderStyle="Solid"
                                BorderWidth="1px" Font-Bold="False" CssClass="boton" OnClick="btnShow_Click" />
                            <asp:Button ID="btnHide" runat="server" Text="Hide Prepaid CCs" Width="110px" BorderStyle="Solid"
                                BorderWidth="1px" Font-Bold="False" CssClass="boton" OnClick="btnHide_Click"
                                Visible="false" />
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>
    </table>
    <asp:Label ID="lblNotFound" runat="server" Text="" Visible="false" CssClass="lblNotFound"></asp:Label>
    <asp:GridView ID="gdvPPCCs" runat="server" Font-Names="Arial" Font-Size="8pt" Width="800px">
        <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
        <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
        <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial"
            Font-Bold="True" ForeColor="Orange"></HeaderStyle>
        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
    </asp:GridView>
    <table>
        <tr>
            <td width="30%">
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <table visible="false" width="100%" style="position: absolute; top: 950px; left: 230px;">
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="right" colspan="3">
                            <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
