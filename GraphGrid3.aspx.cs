using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;

public partial class GraphGrid2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Session["DirectoUser"] as string))//Usuario Logeado            
        {
            String query = null;
            DataTable tbl;

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('ZAHC') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart1(tbl, "ZAHEEN");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('EXCL') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart2(tbl, "EXCEL");

            tbl.Dispose(); 

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('UCLI') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart3(tbl, "ULTRA-MOBILE-CLI");

            tbl.Dispose(); 

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('UCAP') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart4(tbl, "ULTRAMOBILE-CAP");

            tbl.Dispose(); 

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('NRET') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart5(tbl, "NOBEL-RETAIL");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('ADVG') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart6(tbl, "ADVANCE-GLOBAL");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('ALKC') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart7(tbl, "ALKA-IP");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('BTSS') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart8(tbl, "BTS");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('LEXC') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart9(tbl, "LEXICO");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('LUNX') and [type] like 'C%' group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart10(tbl, "LUNEX");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('QCLC') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart11(tbl, "QUICKOM-GLOBAL-CLI");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('MTIN') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart12(tbl, "MATRIX");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('AATL') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart13(tbl, "KDDI-GLOBAL");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('QXTC') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart14(tbl, "QXTEL");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('SWIC') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart15(tbl, "SWISSPHONE");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0 AND Cust_Vendor in ('CMTC') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart16(tbl, "COMTEL");

            tbl.Dispose();
        }
        else
        {
            Response.Redirect("~/SignIn.aspx");
        }
    }


    private void makeChart1(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart1.Titles.Add(tITLE);
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart1.Visible = true;
        }
        else
        {
            this.Chart1.Titles.Add(tITLE);
            this.Chart1.Visible = true;
            Session["tblDetail1"] = tbl;
        }

    }

    private void makeChart2(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart2.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart2.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart2.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart2.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart2.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart2.Titles.Add(tITLE);
            this.Chart2.RenderType = RenderType.ImageTag;
            Session["tblDetail2"] = tbl;

            this.Chart2.Visible = true;
        }
        else
        {
            this.Chart2.Titles.Add(tITLE);
            this.Chart2.Visible = true;
            Session["tblDetail2"] = tbl;
        }

    }

    private void makeChart3(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart3.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart3.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart3.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart3.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart3.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart3.Titles.Add(tITLE);
            this.Chart3.RenderType = RenderType.ImageTag;
            Session["tblDetail3"] = tbl;

            this.Chart3.Visible = true;
        }
        else
        {
            this.Chart3.Titles.Add(tITLE);
            this.Chart3.Visible = true;
            Session["tblDetail3"] = tbl;
        }

    }

    private void makeChart4(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart4.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart4.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart4.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart4.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart4.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart4.Titles.Add(tITLE);
            this.Chart4.RenderType = RenderType.ImageTag;
            Session["tblDetail4"] = tbl;

            this.Chart4.Visible = true;
        }
        else
        {
            this.Chart4.Titles.Add(tITLE);
            this.Chart4.Visible = true;
            Session["tblDetail4"] = tbl;
        }

    }

    private void makeChart5(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart5.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart5.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart5.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart5.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart5.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart5.Titles.Add(tITLE);
            this.Chart5.RenderType = RenderType.ImageTag;
            Session["tblDetail5"] = tbl;

            this.Chart5.Visible = true;
        }
        else
        {
            this.Chart5.Titles.Add(tITLE);
            this.Chart5.Visible = true;
            Session["tblDetail5"] = tbl;
        }

    }

    private void makeChart6(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart6.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart6.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart6.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart6.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart6.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart6.Titles.Add(tITLE);
            this.Chart6.RenderType = RenderType.ImageTag;
            Session["tblDetail6"] = tbl;

            this.Chart6.Visible = true;
        }
        else
        {
            this.Chart6.Titles.Add(tITLE);
            this.Chart6.Visible = true;
            Session["tblDetail6"] = tbl;
        }

    }

    private void makeChart7(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart7.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart7.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart7.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart7.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart7.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart7.Titles.Add(tITLE);
            this.Chart7.RenderType = RenderType.ImageTag;
            Session["tblDetail7"] = tbl;

            this.Chart7.Visible = true;
        }
        else
        {
            this.Chart7.Titles.Add(tITLE);
            this.Chart7.Visible = true;
            Session["tblDetail7"] = tbl;
        }

    }

    private void makeChart8(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart8.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart8.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart8.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart8.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart8.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart8.Titles.Add(tITLE);
            this.Chart8.RenderType = RenderType.ImageTag;
            Session["tblDetail8"] = tbl;

            this.Chart8.Visible = true;
        }
        else
        {
            this.Chart8.Visible = true;
            this.Chart8.Titles.Add(tITLE);
            Session["tblDetail8"] = tbl;
        }

    }

    private void makeChart9(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart9.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart9.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart9.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart9.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart9.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart9.Titles.Add(tITLE);
            this.Chart9.RenderType = RenderType.ImageTag;
            Session["tblDetail9"] = tbl;

            this.Chart9.Visible = true;
        }
        else
        {
            this.Chart9.Titles.Add(tITLE);
            this.Chart9.Visible = true;
            Session["tblDetail9"] = tbl;
        }

    }

    private void makeChart10(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart10.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart10.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart10.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart10.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart10.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart10.Titles.Add(tITLE);
            this.Chart10.RenderType = RenderType.ImageTag;
            Session["tblDetail10"] = tbl;

            this.Chart10.Visible = true;
        }
        else
        {
            this.Chart10.Titles.Add(tITLE);
            this.Chart10.Visible = true;
            Session["tblDetail10"] = tbl;
        }

    }
    private void makeChart11(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart11.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart11.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart11.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart11.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart11.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart11.Titles.Add(tITLE);
            this.Chart11.RenderType = RenderType.ImageTag;
            Session["tblDetail11"] = tbl;

            this.Chart11.Visible = true;
        }
        else
        {
            this.Chart11.Titles.Add(tITLE);
            this.Chart11.Visible = true;
            Session["tblDetail11"] = tbl;
        }

    }
    private void makeChart12(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart12.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart12.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart12.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart12.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart12.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart12.Titles.Add(tITLE);
            this.Chart12.RenderType = RenderType.ImageTag;
            Session["tblDetail12"] = tbl;

            this.Chart12.Visible = true;
        }
        else
        {
            this.Chart12.Titles.Add(tITLE);
            this.Chart12.Visible = true;
            Session["tblDetail12"] = tbl;
        }

    }

    private void makeChart13(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart13.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart13.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart13.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart13.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart13.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart13.Titles.Add(tITLE);
            this.Chart13.RenderType = RenderType.ImageTag;
            Session["tblDetail13"] = tbl;

            this.Chart13.Visible = true;
        }
        else
        {
            this.Chart13.Titles.Add(tITLE);
            this.Chart13.Visible = true;
            Session["tblDetail13"] = tbl;
        }
    }

    private void makeChart14(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart14.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart14.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart14.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart14.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart14.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart14.Titles.Add(tITLE);
            this.Chart14.RenderType = RenderType.ImageTag;
            Session["tblDetail13"] = tbl;

            this.Chart14.Visible = true;
        }
        else
        {
            this.Chart14.Titles.Add(tITLE);
            this.Chart14.Visible = true;
            Session["tblDetail14"] = tbl;
        }

    }

    private void makeChart15(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart15.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart15.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart15.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart15.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart15.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart15.Titles.Add(tITLE);
            this.Chart15.RenderType = RenderType.ImageTag;
            Session["tblDetail15"] = tbl;

            this.Chart15.Visible = true;
        }
        else
        {
            this.Chart15.Titles.Add(tITLE);
            this.Chart15.Visible = true;
            Session["tblDetail15"] = tbl;
        }

    }
    private void makeChart16(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart16.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart16.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart16.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart16.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart16.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart16.Titles.Add(tITLE);
            this.Chart16.RenderType = RenderType.ImageTag;
            Session["tblDetail16"] = tbl;

            this.Chart16.Visible = true;
        }
        else
        {
            this.Chart16.Titles.Add(tITLE);
            this.Chart16.Visible = true;
            Session["tblDetail16"] = tbl;
        }

    }

}
