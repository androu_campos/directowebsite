<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"  AutoEventWireup="true" CodeFile="Unknown.aspx.cs" Inherits="Unknown" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="left">
        <tr>
            <td style="width: 100px; height: 22px;" align="left">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="Unknown IPs" Font-Bold="True"></asp:Label></td>
            <td style="width: 100px; height: 22px;">
                <asp:Label ID="lblmessage" runat="server" Font-Overline="False" Font-Size="8pt" ForeColor="Red"
                    Text="No unknown IPs found !!" Visible="False" Width="194px" Font-Bold="True" Font-Names="Arial"></asp:Label></td>
            <td style="width: 100px; height: 22px;">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" rowspan="2">
            </td>
            <td colspan="2" rowspan="2">
                <asp:GridView ID="gdvUnknown" runat="server" AutoGenerateColumns="False" Font-Names="Arial" Font-Size="8pt">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <Columns>
                        <asp:BoundField DataField="Type" HeaderText="Type">
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Calls" HeaderText="Calls">
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Minutes" HeaderText="Minutes">
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PotentialMatch" HeaderText="PotentialMatch">
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="160px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="SubnetMatch" HeaderText="SubnetMatch"></asp:BoundField>
                        <asp:HyperLinkField DataNavigateUrlFormatString="AddIP.aspx?IP={0}" DataTextField="UnknownIP"
                            DataNavigateUrlFields="UnknownIP" HeaderText="Unknown IP"></asp:HyperLinkField>
                
                </Columns>
                
                
                </asp:GridView>
            </td>
        </tr>
        <tr>
        </tr>
    </table>
</asp:Content>

