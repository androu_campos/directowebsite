using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class VendorHome: System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string vendor = Session["DirectoUser"].ToString();
        string SqlCommandText = "";

        if (vendor == "ALPI")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandType = CommandType.Text;
            SqlCommandText = "SELECT SUM(ISNULL(MINUTES,0)) as MINUTES FROM OPSHEETINC WHERE VENDOR IN ('OUTLANDER-ALPI-ICS','OUTLANDER-ALPI') ";
            sqlQry.CommandText = SqlCommandText;
            DataSet myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {


                GridView1.DataSource = myDataset.Tables[0];
                GridView1.DataBind();

                Session["RcntProv"] = myDataset;
            }
            else
            {
                GridView1.EmptyDataText = "No info available...";
            }

            sqlQry.CommandText = "SELECT  SUM(ISNULL(MINUTES,0)) as MINUTES FROM OPSHEETALL WHERE VENDOR IN ('OUTLANDER-ALPI-ICS','OUTLANDER-ALPI') ";
            myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {

                GridView2.DataSource = myDataset.Tables[0];
                GridView2.DataBind();

                Session["TodayProv"] = myDataset;
            }
            else
            {
                GridView2.EmptyDataText = "No info available...";
            }
        }
        else if (vendor == "olbrasilnat")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandType = CommandType.Text;
            SqlCommandText = "SELECT VENDOR , SUM(ISNULL(MINUTES,0)) as MINUTES,mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls), SUM(RA)) as ASR ,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD FROM OPSHEETINC WHERE VENDOR = 'OL-BRASIL-NAT' GROUP BY VENDOR";
            sqlQry.CommandText = SqlCommandText;
            DataSet myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {
                
                
                GridView1.DataSource = myDataset.Tables[0];
                GridView1.DataBind();
               
                Session["RcntProv"] = myDataset;
            }
            else
            {
                GridView1.EmptyDataText = "No info available...";
            }


            sqlQry.CommandText = "SELECT VENDOR , SUM(ISNULL(MINUTES,0)) as MINUTES,mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls), SUM(RA)) as ASR ,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD FROM OPSHEETALL WHERE VENDOR = 'OL-BRASIL-NAT' GROUP BY VENDOR";
                myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {
                
                GridView2.DataSource = myDataset.Tables[0];
                GridView2.DataBind();
               
                Session["TodayProv"] = myDataset;
            }
            else
            {
                GridView2.EmptyDataText = "No info available...";
            }
        }
        else if (vendor == "TELCELPOST")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandType = CommandType.Text;
//            SqlCommandText = "SELECT SUM(ISNULL(MINUTES,0)) as MINUTES,SUM (ATTEMPTS) as ATTEMPTS,mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls), SUM(RA)) as ASR ,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD FROM OPSHEETINC WHERE VENDOR = 'OUTLANDER-POSPAY' ";
            SqlCommandText = "SELECT LMC, SUM(ISNULL(MINUTES,0)) as MINUTES,SUM (ATTEMPTS) as ATTEMPTS FROM OPSHEETINC WHERE VENDOR = 'OUTLANDER-POSPAY' GROUP BY LMC";
            sqlQry.CommandText = SqlCommandText;
            DataSet myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {


                GridView1.DataSource = myDataset.Tables[0];
                GridView1.DataBind();

                Session["RcntProv"] = myDataset;
            }
            else
            {
                GridView1.EmptyDataText = "No info available...";
            }


            sqlQry.CommandText = "SELECT LMC, SUM(ISNULL(MINUTES,0)) as MINUTES,SUM (ATTEMPTS) as ATTEMPTS FROM OPSHEETALL WHERE VENDOR = 'OUTLANDER-POSPAY' GROUP BY LMC";
            myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {

                GridView2.DataSource = myDataset.Tables[0];
                GridView2.DataBind();

                Session["TodayProv"] = myDataset;
            }
            else
            {
                GridView2.EmptyDataText = "No info available...";
            }
        }
        else if (vendor == "CIAO-TELECOM")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandType = CommandType.Text;
            SqlCommandText = "Select VENDOR , REGION , TYPE, sum (attempts) as ATTEMPTS,SUM(CALLS) AS ANWEREDCALLS, sum(ISNULL(minutes,0)) as MINUTES from OpSheetInc WHERE VENDOR IN ('CIAO-TELECOM-MOBILE','CIAO-TELECOM-FIXED') group by VENDOR, Region, Type ";
            sqlQry.CommandText = SqlCommandText;
            DataSet myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {


                GridView1.DataSource = myDataset.Tables[0];
                GridView1.DataBind();

                Session["RcntProv"] = myDataset;
            }
            else
            {
                GridView1.EmptyDataText = "No info available...";
            }


            SqlCommandText = "Select VENDOR , REGION , TYPE, sum (attempts) as ATTEMPTS,SUM(CALLS) AS ANWEREDCALLS, sum(ISNULL(minutes,0)) as MINUTES from OpSheetAll WHERE VENDOR IN ('CIAO-TELECOM-MOBILE','CIAO-TELECOM-FIXED') group by VENDOR, Region, Type ";
            myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {

                GridView2.DataSource = myDataset.Tables[0];
                GridView2.DataBind();

                Session["TodayProv"] = myDataset;
            }
            else
            {
                GridView2.EmptyDataText = "No info available...";
            }
        }
        else if (vendor == "VETECSURMIN")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandType = CommandType.Text;
            //            SqlCommandText = "SELECT SUM(ISNULL(MINUTES,0)) as MINUTES,SUM (ATTEMPTS) as ATTEMPTS,mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls), SUM(RA)) as ASR ,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD FROM OPSHEETINC WHERE VENDOR = 'OUTLANDER-POSPAY' ";
            SqlCommandText = "SELECT SUM(ISNULL(MINUTES,0)) as MINUTES,SUM (ATTEMPTS) as ATTEMPTS FROM OPSHEETINC WHERE VENDOR in ('VETECSUR','VETECSUR-ICS') ";
            sqlQry.CommandText = SqlCommandText;
            DataSet myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {


                GridView1.DataSource = myDataset.Tables[0];
                GridView1.DataBind();

                Session["RcntProv"] = myDataset;
            }
            else
            {
                GridView1.EmptyDataText = "No info available...";
            }


            sqlQry.CommandText = "SELECT  SUM(ISNULL(MINUTES,0)) as MINUTES,SUM (ATTEMPTS) as ATTEMPTS FROM OPSHEETALL WHERE VENDOR in ('VETECSUR','VETECSUR-ICS') ";
            myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {

                GridView2.DataSource = myDataset.Tables[0];
                GridView2.DataBind();

                Session["TodayProv"] = myDataset;
            }
            else
            {
                GridView2.EmptyDataText = "No info available...";
            }
        }
        else if (vendor == "MAS-COMUNICACION")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandType = CommandType.Text;
            SqlCommandText = "SELECT SUM(ISNULL(MINUTES,0)) as MINUTES,SUM (ATTEMPTS) as ATTEMPTS,mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls), SUM(RA)) as ASR ,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD FROM OPSHEETINC WHERE VENDOR = 'MAS-COMUNICACION' ";            
            sqlQry.CommandText = SqlCommandText;
            DataSet myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {


                GridView1.DataSource = myDataset.Tables[0];
                GridView1.DataBind();

                Session["RcntProv"] = myDataset;
            }
            else
            {
                GridView1.EmptyDataText = "No info available...";
            }


            sqlQry.CommandText = "SELECT SUM(ISNULL(MINUTES,0)) as MINUTES,SUM (ATTEMPTS) as ATTEMPTS,mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls), SUM(RA)) as ASR ,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD FROM OPSHEETALL WHERE VENDOR = 'MAS-COMUNICACION' ";            
            myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {

                GridView2.DataSource = myDataset.Tables[0];
                GridView2.DataBind();

                Session["TodayProv"] = myDataset;
            }
            else
            {
                GridView2.EmptyDataText = "No info available...";
            }
        }
        else if (vendor == "COSMORED")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandType = CommandType.Text;
            //            SqlCommandText = "SELECT SUM(ISNULL(MINUTES,0)) as MINUTES,SUM (ATTEMPTS) as ATTEMPTS,mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls), SUM(RA)) as ASR ,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD FROM OPSHEETINC WHERE VENDOR = 'OUTLANDER-POSPAY' ";
            SqlCommandText = "SELECT SUM(ISNULL(MINUTES,0)) as MINUTES,SUM (ATTEMPTS) as ATTEMPTS FROM OPSHEETINC WHERE VENDOR = 'COSMORED' ";
            sqlQry.CommandText = SqlCommandText;
            DataSet myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {


                GridView1.DataSource = myDataset.Tables[0];
                GridView1.DataBind();

                Session["RcntProv"] = myDataset;
            }
            else
            {
                GridView1.EmptyDataText = "No info available...";
            }


            sqlQry.CommandText = "SELECT  SUM(ISNULL(MINUTES,0)) as MINUTES,SUM (ATTEMPTS) as ATTEMPTS FROM OPSHEETALL WHERE VENDOR = 'COSMORED' ";
            myDataset = Util.RunQuery(sqlQry);

            if (myDataset.Tables.Count > 0)
            {

                GridView2.DataSource = myDataset.Tables[0];
                GridView2.DataBind();

                Session["TodayProv"] = myDataset;
            }
            else
            {
                GridView2.EmptyDataText = "No info available...";
            }
        }
        else if (vendor == "pldtretail")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandText = "SELECT CUSTOMER, COUNTRY, REGION, REGIONPREFIX, SUM(ISNULL(MINUTES,0)) as MINUTES, SUM (ATTEMPTS) as ATTEMPTS FROM OPSHEETINC WHERE VENDOR = 'PLDT-ALLACCESSPH' GROUP BY CUSTOMER, COUNTRY, REGION, REGIONPREFIX";
            sqlQry.CommandType = CommandType.Text;
            DataSet myDataset = Util.RunQuery(sqlQry);
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            Session["RcntProv"] = myDataset;

            sqlQry.CommandText = "SELECT CUSTOMER, COUNTRY, REGION, REGIONPREFIX, SUM(ISNULL(MINUTES,0)) as MINUTES, SUM (ATTEMPTS) as ATTEMPTS FROM OPSHEETALL WHERE VENDOR = 'PLDT-ALLACCESSPH' GROUP BY CUSTOMER, COUNTRY, REGION, REGIONPREFIX";
            myDataset = RunQuery(sqlQry);
            GridView2.DataSource = myDataset.Tables[0];
            GridView2.DataBind();
            Session["TodayProv"] = myDataset;

        }
        else if (vendor == "24-TRANSPORT")
        {
            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandText = "SELECT COUNTRY, REGION, REGIONPREFIX, SUM(ISNULL(MINUTES,0)) as MINUTES, SUM (ATTEMPTS) as ATTEMPTS FROM OPSHEETINC WHERE VENDOR = '24-TRANSPORT' GROUP BY COUNTRY, REGION, REGIONPREFIX";
            sqlQry.CommandType = CommandType.Text;
            DataSet myDataset = Util.RunQuery(sqlQry);
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            Session["RcntProv"] = myDataset;

            sqlQry.CommandText = "SELECT COUNTRY, REGION, REGIONPREFIX, SUM(ISNULL(MINUTES,0)) as MINUTES, SUM (ATTEMPTS) as ATTEMPTS FROM OPSHEETALL WHERE VENDOR = '24-TRANSPORT' GROUP BY COUNTRY, REGION, REGIONPREFIX";
            myDataset = RunQuery(sqlQry);
            GridView2.DataSource = myDataset.Tables[0];
            GridView2.DataBind();
            Session["TodayProv"] = myDataset;
        }
        else
        {

            SqlCommand sqlQry = new SqlCommand();
            sqlQry.CommandText = "Select Customer, Region, Type, sum(ISNULL(minutes,0)) as Minutes, sum (attempts) as Attempts from OpSheetInc Where Region in ('MANILA (PLDT)','ON NET (PAPTELCO - PLDT)','ON NET (PLDT)') and TYPE = 'TBD' group by Customer, Region, Type";
            sqlQry.CommandType = CommandType.Text;
            DataSet myDataset = Util.RunQuery(sqlQry);
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            Session["RcntProv"] = myDataset;

            sqlQry.CommandText = "Select Customer, Region, Type, sum(ISNULL(minutes,0)) as Minutes, sum (attempts) as Attempts from OpSheetAll Where Region in ('MANILA (PLDT)','ON NET (PAPTELCO - PLDT)','ON NET (PLDT)') and TYPE = 'TBD' group by Customer, Region, Type";
            myDataset = RunQuery(sqlQry);
            GridView2.DataSource = myDataset.Tables[0];
            GridView2.DataBind();
            Session["TodayProv"] = myDataset;
        }

    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        GridView2.DataBind();
    }
    protected void cmdExport1_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["RcntProv"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "Recent" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string error = ex.Message;
        }
    }
    protected void cmdExport2_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["TodayProv"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "Today" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string error = ex.Message;
        }
    }
}
