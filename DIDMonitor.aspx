<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="DIDMonitor.aspx.cs" Inherits="DIDMonitor" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="lblBilling" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="Label"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <asp:Label ID="lblHeader" runat="server" CssClass="labelBlue8" Font-Bold="True" Font-Size="10pt"
                        Text="DID Monitor"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px; height: 23px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Recent Traffic"></asp:Label><br />
                    <asp:GridView ID="gdvRecent" runat="server" Font-Names="Arial" Font-Size="8pt" Font-Underline="False" Width="700px">
                        <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                        <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                        <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial"
                            Font-Bold="True" Width= "250px" ForeColor="Orange"></HeaderStyle>
                        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                        
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblRU" runat="server" CssClass="labelBlue8" 
                        Text="Label">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 24px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Today Traffic"></asp:Label><br />
                    <asp:GridView ID="gdvToday" runat="server" Font-Names="Arial" Font-Size="8pt" Width="700px">
                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                    <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                    <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                    <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial" Font-Bold="True" ForeColor="Orange"></HeaderStyle>
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                    </asp:GridView>
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTU" runat="server" CssClass="labelBlue8" 
                        Text="Label">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 23px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Yesterday Traffic"></asp:Label><br />
                    <asp:GridView ID="gdvYesterday" runat="server" Font-Names="Arial" Font-Size="8pt" Width="700px">
                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                    <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                    <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                    <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial" Font-Bold="True" ForeColor="Orange"></HeaderStyle>
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                    </asp:GridView>
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblYU" runat="server" CssClass="labelBlue8" 
                        Text="Label">
                    </asp:Label>
                </td>
            </tr>
            <%--<tr>
                <td style="width: 100px; height: 24px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Yesterday Peak Traffic"></asp:Label><br />
                    <asp:GridView ID="gdvYesterdayHour" runat="server" Font-Names="Arial" Font-Size="8pt" Width="100%">
                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                    <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                    <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                    <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial" Font-Bold="True" ForeColor="Orange"></HeaderStyle>
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                    </asp:GridView>
                    </td>
            </tr>--%>
            <tr>
                <td style="width: 106px; height: 44px;" valign="top">
                    <asp:Timer ID="Timer1" runat="server" Interval="300000">
                    </asp:Timer>
                </td>
                <td style="width: 100px; height: 44px;" valign="top">
                    </td>
            </tr>
            
        </table>
    
</asp:Content>

