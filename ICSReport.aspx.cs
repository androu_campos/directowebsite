using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ICSReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = "";
        DataSet ds = null;



        sql = "Select ISNULL([Type],'TBD') as Type, CASE WHEN Customer IN ('ICS-PAC-WEST-2','ICS-NUVOX-2','ICS-PREMIUM-UNL-2') THEN 'ICS-RELOADABLE-2' WHEN Customer IN ('ICS-PREMIUM','ICS-PREMIUM-UNL', 'ICS-PAC-WEST-5', 'ICS-NUVOX-5') THEN 'ICS-RELOADABLE' "
        + " WHEN Customer in ('ICS-CARDS-2DLS','ICS-CARDS-5DLS') THEN Customer ELSE 'ICS-ALL-CARDS' END CUSTOMER,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
        + " SUM(Calls)) as ACD "
        + " from [ICS].[dbo].OpSheetAllR "
        + " where Customer like 'ICS%' "
        + " group by ISNULL([Type],'TBD'), CASE WHEN Customer IN ('ICS-PAC-WEST-2','ICS-NUVOX-2','ICS-PREMIUM-UNL-2') THEN 'ICS-RELOADABLE-2' WHEN Customer IN ('ICS-PREMIUM','ICS-PREMIUM-UNL', 'ICS-PAC-WEST-5', 'ICS-NUVOX-5') THEN 'ICS-RELOADABLE' WHEN Customer in ('ICS-CARDS-2DLS','ICS-CARDS-5DLS') THEN Customer ELSE 'ICS-ALL-CARDS' END  "
        + "UNION "
        + "SELECT '** ALL **','*****',sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + "sum(Attempts) as Attempts, "
        + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + "mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + "mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
        + "mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
        + "from [ICS].[dbo].OpSheetAllR "
        + "where Customer like 'ICS%' "
        + "order by  TotalMinutes DESC ";
            



       


        ds = Util.RunQueryByStmnt(sql);
        gdvToday.DataSource = ds.Tables[0];
        gdvToday.DataBind();


        sql = "Select ISNULL([Type],'TBD') as Type, CASE WHEN Customer IN ('ICS-PAC-WEST-2','ICS-NUVOX-2','ICS-PREMIUM-UNL-2') THEN 'ICS-RELOADABLE-2' WHEN Customer IN ('ICS-PREMIUM','ICS-PREMIUM-UNL', 'ICS-PAC-WEST-5', 'ICS-NUVOX-5') THEN 'ICS-RELOADABLE' "
        + " WHEN Customer in ('ICS-CARDS-2DLS','ICS-CARDS-5DLS') THEN Customer ELSE 'ICS-ALL-CARDS' END CUSTOMER,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
        + " SUM(Calls)) as ACD "
        + " from [ICS].[dbo].OpSheetIncR "
        + " where Customer like 'ICS%' "
        + " group by ISNULL([Type],'TBD'), CASE WHEN Customer IN ('ICS-PAC-WEST-2','ICS-NUVOX-2','ICS-PREMIUM-UNL-2') THEN 'ICS-RELOADABLE-2' WHEN Customer IN ('ICS-PREMIUM','ICS-PREMIUM-UNL', 'ICS-PAC-WEST-5', 'ICS-NUVOX-5') THEN 'ICS-RELOADABLE' WHEN Customer in ('ICS-CARDS-2DLS','ICS-CARDS-5DLS') THEN Customer ELSE 'ICS-ALL-CARDS' END  "
        + "UNION "
        + "SELECT '** ALL **','*****',sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + "sum(Attempts) as Attempts, "
        + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + "mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + "mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
        + "mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
        + "from [ICS].[dbo].OpSheetIncR "
        + "where Customer like 'ICS%' "
        + "order by  TotalMinutes DESC ";
        

        ds = Util.RunQueryByStmnt(sql);
        gdvRecent.DataSource = ds.Tables[0];
        gdvRecent.DataBind();


        sql = "Select ISNULL([Type],'TBD') as Type, CASE WHEN Customer IN ('ICS-PAC-WEST-2','ICS-NUVOX-2','ICS-PREMIUM-UNL-2') THEN 'ICS-RELOADABLE-2' WHEN Customer IN ('ICS-PREMIUM','ICS-PREMIUM-UNL', 'ICS-PAC-WEST-5', 'ICS-NUVOX-5') THEN 'ICS-RELOADABLE' "
        + " WHEN Customer in ('ICS-CARDS-2DLS','ICS-CARDS-5DLS') THEN Customer ELSE 'ICS-ALL-CARDS' END CUSTOMER,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
        + " SUM(Calls)) as ACD "
        + " from [ICS].[dbo].OpSheetYR "
        + " where Customer like 'ICS%' "
        + " group by ISNULL([Type],'TBD'), CASE WHEN Customer IN ('ICS-PAC-WEST-2','ICS-NUVOX-2','ICS-PREMIUM-UNL-2') THEN 'ICS-RELOADABLE-2' WHEN Customer IN ('ICS-PREMIUM','ICS-PREMIUM-UNL', 'ICS-PAC-WEST-5', 'ICS-NUVOX-5') THEN 'ICS-RELOADABLE' WHEN Customer in ('ICS-CARDS-2DLS','ICS-CARDS-5DLS') THEN Customer ELSE 'ICS-ALL-CARDS' END  "
        + "UNION "
        + "SELECT '** ALL **','*****',sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + "sum(Attempts) as Attempts, "
        + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + "mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + "mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
        + "mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
        + "from [ICS].[dbo].OpSheetYR "
        + "where Customer like 'ICS%' "
        + "order by  TotalMinutes DESC ";
        

        ds = Util.RunQueryByStmnt(sql);
        gdvYesterday.DataSource = ds.Tables[0];
        gdvYesterday.DataBind();

        sql = " Select CAST(datepart(hour,stop)as varchar) as [Hour], ISNULL([Type],'TBD') as Type, "
        + "Customer, "
        + "sum(ISNULL(CustBillMinutes,0)) as TotalMinutes, count(*) as Attempts, sum(CASE WHEN duration > 0 then 1 else 0 end) as AnsweredCalls, "
        + "sum(RA) as Rejected, mecca2.dbo.fGetASR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end), SUM(RA)) as ASR, "
        + "mecca2.dbo.fGetABR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ABR, "
        + "mecca2.dbo.fGetACD(SUM(CustBillMinutes), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ACD "
        + "from [mecca2].[dbo].Repsheet0_ "
        + "where Customer like 'ICS%' and datepart(hour,stop) > 18 and [Type] = 'CPP' "
        + "group by datepart(hour,stop), ISNULL([Type],'TBD'), "
        + "Customer "        
        + "UNION "
        + "Select '******' as [Hour], "
        + "'******' as Type, "
        + "'******' [Customer Group], "        
        + "sum(ISNULL(CustBillMinutes,0)) as TotalMinutes, "
        + "count(*) as Attempts, "
        + "sum(CASE WHEN duration > 0 then 1 else 0 end) as AnsweredCalls, "
        + "sum(RA) as Rejected, "
        + "mecca2.dbo.fGetASR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end), SUM(RA)) as ASR, "
        + "mecca2.dbo.fGetABR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ABR, "
        + "mecca2.dbo.fGetACD(SUM(CustBillMinutes), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ACD "
        + "from [mecca2].[dbo].Repsheet0_ "
        + "where Customer like 'ICS%' "
        + "and datepart(hour,stop) > 18 and [Type] = 'CPP' "
        + "order by [Hour], TotalMinutes ";

        ds = Util.RunQueryByStmnt(sql);
        gdvYesterdayHour.DataSource = ds.Tables[0];
        gdvYesterdayHour.DataBind();

    }
}
