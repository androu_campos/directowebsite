using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class CustVendorNames : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string qry = string.Empty;
            string type = string.Empty;

            if (Request.QueryString.Count > 0)
            {
                type = Request.QueryString["type"].ToString();

                if (type == "Customer")
                {
                    qry = "select distinct  CustName, Modified, Count(*) as Count from CDRDB.dbo.CustBreakout group by Custname, Modified order by CustName, modified";
                    Label1.Text = "CUSTOMER NAMES";
                }
                else if (type == "Vendor")
                {
                    qry = "select distinct VendorName, Modified, Count(*) as Count from CDRDB.dbo.VendorBreakout group by Vendorname, Modified order by VendorName, modified ";
                    Label1.Text = "VENDOR NAMES";
                }


                SqlCommand sqlSpQuery = new SqlCommand();
                sqlSpQuery.CommandType = System.Data.CommandType.Text;
                sqlSpQuery.CommandText = qry;
                DataSet ResultSet;
                ResultSet = RunQuery(sqlSpQuery);
                Session["CustVendorNames"] = ResultSet;
                gdvCV.DataSource = ResultSet.Tables[0];
                gdvCV.DataBind();



            }
        }
    }
        
        
    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }


    protected void gdvCV_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvCV.PageIndex = e.NewPageIndex;
        gdvCV.DataSource = ((DataSet)Session["CustVendorNames"]).Tables[0];
        gdvCV.DataBind();
    }
}
