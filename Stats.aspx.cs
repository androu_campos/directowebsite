using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class Stats : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TextBox1.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            TextBox2.Text = DateTime.Now.AddDays(-1).ToShortDateString();
                     
        }
    }
    protected void cmdShow_Click(object sender, EventArgs e)
    {        
       
        SqlCommand sqlQry = new SqlCommand();        
        sqlQry.CommandType = CommandType.Text;



        string sql = "";
        string name = "";

        if (dpdProvider.Text == "**ALL**")
            name = "%";
        else
            name = dpdProvider.Text;

        sql = "Select convert(varchar,time,101)+' '+left(convert(varchar,time,114),2)+':00' as Time,vendor , sum(minutes) as TotalMinutes, sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected, mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) as ACD from opLiveMonitor WHERE ";

        if (dpdType.Text == "V")
            sql = sql + "VENDOR like '" + name + "' AND TIME BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' group by vendor,convert(varchar,time,101)+' '+left(convert(varchar,time,114),2)+':00' Order by Time";
        else

            sql = sql + "CUSTOMER like '" + name + "' AND TIME BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' group by vendor,convert(varchar,time,101)+' '+left(convert(varchar,time,114),2)+':00' Order by Time";

        sqlQry.CommandText = sql;
        //sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Origination' as Direction, sum(CustMinutes)as TotalMinutes,sum(BillableCalls) as CompletedCalls from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Customer = '" + customer + "' group by BillingDate having sum(CustMinutes) > 0 union select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate,'Termination' as Direction, Cost as Rate, sum(CustMinutes)as TotalMinutes,sum(BillableCalls) as CompletedCalls from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Vendor = '" + vendor + "' group by BillingDate   having sum(CustMinutes) > 0 order by Direction";
        // Price as Rate
        //group by Price,
        
        DataSet myDataset = RunQuery(sqlQry);
        Session["Stats"] = myDataset;
        if (myDataset.Tables[0].Rows.Count > 0)
        {
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            cmdExport.Visible = true;
            Label4.Visible = false;
        }
        else
        {
            cmdExport.Visible = false;
            Label4.Visible = true;
        }
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void cmdExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {
         DataTable dtEmployee = ((DataSet)Session["Stats"]).Tables[0].Copy();
         // Export all the details to CSV
         RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");                  
         string filename = "StatsTraffic" + ".xls";
         objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = Session["Stats"];
        GridView1.DataBind();
    }
}
