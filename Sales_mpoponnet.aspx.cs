using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Sales_mpoponnet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            txtFrom.Text = DateTime.Now.AddDays(-7).ToShortDateString();
            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();

            cdrdbDatasetTableAdapters.ProviderIPTableAdapter adp = new cdrdbDatasetTableAdapters.ProviderIPTableAdapter();
            cdrdbDataset.ProviderIPDataTable tbl = adp.GetProviderName();
            int count = tbl.Rows.Count;

            for (int i = 0; i < count; i++)
            {
                dpdCustomer.Items.Add(tbl.Rows[i]["ProviderName"].ToString());

            }



            MyDataSetTableAdaptersTableAdapters.MpopOnnetAdp adpM = new MyDataSetTableAdaptersTableAdapters.MpopOnnetAdp();
            MyDataSetTableAdapters.MpopOnnetDataTable tblM = adpM.GetData();

            int countM = tblM.Rows.Count;

            for (int j = 0; j < countM; j++)
            {
                dpdVendor.Items.Add(tblM.Rows[j]["vendor"].ToString());
            }










        }



    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Session["dpdCustomer"] = dpdCustomer.SelectedValue.ToString();
        Session["dpdVendor"] = dpdVendor.SelectedValue.ToString();
        Session["from"] = txtFrom.Text;
        Session["to"] = txtTo.Text;
        Response.Redirect("Sales_mpoponnet1.aspx");
    }
}
