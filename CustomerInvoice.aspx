<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CustomerInvoice.aspx.cs" Inherits="MasterAccess" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="height: 427px" width="700" align="left">
        <tr>
            <td align="left" style="width: 35%" valign="top">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="Summary Invoice"></asp:Label></td>
            <td align="left" valign="top" width="30%">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 35%; height: 25px" valign="top">
            </td>
            <td align="left" style="height: 25px" valign="top" width="30%">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 35%" valign="top">
                <table width="100%">
                    <tr>
                        <td bgcolor="#d5e3f0" colspan="2">
                            <asp:Label ID="Label122" runat="server" CssClass="labelBlue8" Text="Step 1"></asp:Label></td>
                        <td bgcolor="#d5e3f0" colspan="2">
                            <asp:Label ID="Label133" runat="server" CssClass="labelBlue8" Text="Choose Type"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4" rowspan="2">
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="labelSteelBlue" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                                <asp:ListItem Selected="True" Value="1">Weekly</asp:ListItem>
                                <asp:ListItem Value="2">ByWeekly</asp:ListItem>
                                <asp:ListItem Value="3">Monthly</asp:ListItem>
                            </asp:RadioButtonList>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>
                </td>
            <td align="left" valign="top" width="30%">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 35%" valign="top">
                <table width="100%">
                    <tr>
                        <td bgcolor="#d5e3f0" colspan="2" style="height: 18px">
                            <asp:Label ID="Label12" runat="server" CssClass="labelBlue8" Text="Step 2"></asp:Label></td>
                        <td bgcolor="#d5e3f0" colspan="2" style="height: 18px">
                            <asp:Label ID="Label13" runat="server" CssClass="labelBlue8" Text="Choose Date"></asp:Label></td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="30%">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 35%" valign="top">
                <table>
                    <tr>
                        <td style="width: 31px">
                <asp:Label ID="Label5" runat="server" CssClass="labelSteelBlue" Text="To:"></asp:Label>
                        </td>
                        <td style="width: 100px">
                <asp:TextBox ID="txtTo" runat="server" CssClass="labelSteelBlue" Width="100%"></asp:TextBox></td>
                        <td style="width: 30px" align="center">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="30%">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 35%" valign="top">
                </td>
            <td align="left" valign="top" width="30%">
            </td>
        </tr>
        <tr>
            <td style="width: 60px" align="left">
                <asp:Button ID="cmdReport" runat="server" CssClass="boton" OnClick="cmdReport_Click"
                    Text="View report" />
                </td>
            <td width="30%">
            </td>
        </tr>
        <tr>
            <td style="width: 60px">
            </td>
            <td width="30%">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 60px">
                <asp:Label ID="lblUpdate" runat="server" Visible="False" CssClass="labelTurk" Width="222px"></asp:Label></td>
            <td width="30%">
            </td>
        </tr>
        <tr>
            <td style="width: 60px">
                <cc1:calendarextender id="CalendarExtender2" runat="server" popupbuttonid="Image2"
                    targetcontrolid="txtTo"></cc1:calendarextender>
                &nbsp; &nbsp; &nbsp;&nbsp;
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td width="30%">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 60px">
                <asp:GridView ID="GridView1" runat="server" Font-Names="Arial"
                    Font-Size="9pt" >
                    <HeaderStyle CssClass="titleOrangegrid" Height="40px" Width="150px" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <AlternatingRowStyle BackColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                </asp:GridView>
            </td>
            <td width="30%">
            </td>
        </tr>
    </table>
    <br />
    <br />
    &nbsp;
</asp:Content>

