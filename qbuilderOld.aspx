<%@ Page Language="C#" MasterPageFile="~/Master.master" StylesheetTheme="Theme1" AutoEventWireup="true" CodeFile="qbuilderOld.aspx.cs" Inherits="qbuilder" Title="DIRECTO - Connections Worldwide" %>
<%@ OutputCache Duration="21600" Location="Client" VaryByParam="None" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <br />
    <table>
        <tr>
            <td width="30%">
                <asp:Label ID="LabelHeader" runat="server" Text="MECCA ADVANCE QUERY BUILDER" Width="194px" Font-Bold="True" CssClass="labelTurk"></asp:Label></td>
            <td width="70%">
               </td>
        </tr>
        <tr>
            <td height="100%" width="30%">
                
    <table align="left" width="100%">
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                <asp:Label ID="lblCustomer" runat="server" Text="Customer" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCustomer" runat="server" DataTextField="ProviderName" DataValueField="ProviderName" Width="130px" CssClass="dropdown">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                <asp:Label ID="lblVendor" runat="server" Text="Vendor" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdVendor" runat="server" DataTextField="ProviderName" DataValueField="ProviderName" Width="130px" CssClass="dropdown">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="lblOriginationIP" runat="server" Text="Origination IP" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdOriginationIP" runat="server" DataTextField="OriginationIP" DataValueField="OriginationIP" Width="130px" CssClass="dropdown">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="lblTerminationIP" runat="server" Text="Termination IP" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdTerminationIP" runat="server" DataTextField="TerminationIP" DataValueField="TerminationIP" Width="130px" CssClass="dropdown">
                    <asp:ListItem Value="N">**NONE**</asp:ListItem>
                    <asp:ListItem Value="*">**ALL**</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="lblCountry" runat="server" Text="Country" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCountry" runat="server" DataTextField="Country" DataValueField="Country" Width="130px" CssClass="dropdown">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
    <asp:DropDownList ID="dpdRegions" runat="server" DataTextField="Region" DataValueField="Region" Width="130px" CssClass="dropdown">
    </asp:DropDownList></td>
            <td style="width: 100px">
                <asp:CheckBox ID="chkDetail" runat="server" Text="Detail" /></td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="lblType" runat="server" CssClass="labelBlue8" Text="Type"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdType" runat="server" DataTextField="Type" DataValueField="Type" Width="130px" CssClass="dropdown">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px; height: 22px;" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="lblClass" runat="server" Text="Class" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px; height: 22px;">
                <asp:DropDownList ID="dpdClass" runat="server" Width="130px" CssClass="dropdown">
                </asp:DropDownList></td>
            <td style="width: 100px; height: 22px;">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="lblSourceMSW" runat="server" Text="Source MSW" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdSourceMSW" runat="server" Width="130px" CssClass="dropdown" DataTextField="IP" DataValueField="IP">
                    <asp:ListItem Value="%">** NONE **</asp:ListItem>
                    <asp:ListItem Value="**ALL**">**ALL**</asp:ListItem>
                    <asp:ListItem Value="NY01">NY01</asp:ListItem>
                    <asp:ListItem Value="NY02">NY02</asp:ListItem>
                    <asp:ListItem>NY03</asp:ListItem>
                    <asp:ListItem Value="LA01">LA01</asp:ListItem>
                    <asp:ListItem Value="MI02">MI02</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 84px"> &nbsp;<asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdReportType" runat="server" Width="130px" CssClass="dropdown">
                    <asp:ListItem Value="OpSheetInc">Recent Traffic</asp:ListItem>
                    <asp:ListItem Value="OpSheetAll">Today's Traffic</asp:ListItem>
                    <asp:ListItem Value="OpSheetY">Yesterday's Traffic</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 100px">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 84px">
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Gateway" Visible="False"></asp:Label></td>
            <td align="left" style="width: 100px">
                <asp:DropDownList ID="dpdGateway" runat="server" CssClass="dropdown" Visible="False"
                    Width="130px">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px">
            </td>
            <td align="left" colspan="2">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                <asp:Button ID="cmdQbuilder" runat="server" OnClick="cmdQbuilder_Click" Text="ViewReport" BorderStyle="Solid" BorderWidth="1px" Font-Bold="False" CssClass="boton" /></td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                &nbsp;</td>
            <td align="right" style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="height: 136px" align="left" colspan="2" valign="top">
               <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT DISTINCT IP FROM MECCA2..VportViewer&#13;&#10;UNION&#13;&#10;SELECT '**ALL**' AS IP FROM MECCA2..VportViewer">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="CustomerS" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
        SelectCommand="select distinct ProviderName from ProviderIP &#13;&#10;union &#13;&#10;select '** NONE **' as ProviderName from ProviderIP&#13;&#10;union&#13;&#10;select '**ALL**' as ProviderName from ProviderIP&#13;&#10;order by ProviderName ASC">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="OriginationIPS" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT DISTINCT OriginationIP FROM OpSheetAll UNION SELECT '** NONE **' AS OriginationIP FROM OpSheetAll AS OpSheetAll_2 UNION SELECT '**ALL**' AS OriginationIP FROM OpSheetAll AS OpSheetAll_1 ORDER BY OriginationIP">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="CountryS" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="select distinct Country from Regions&#13;&#10;union &#13;&#10;select '** NONE **' as Country from Regions&#13;&#10;union&#13;&#10;select '**ALL**' as Country from Regions&#13;&#10;order by Country ASC">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="TerminationIPS" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="select distinct TerminationIP from OpSheetAll&#13;&#10;union &#13;&#10;select '** NONE **' as TerminationIP from OpSheetAll&#13;&#10;union&#13;&#10;select '**ALL**' as TerminationIP from OpSheetAll&#13;&#10;order by TerminationIP ASC">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="RegionS" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="select distinct Region from Regions&#13;&#10;union &#13;&#10;select '** NONE **' as Region from Regions&#13;&#10;union&#13;&#10;select '**ALL**' as Region from Regions&#13;&#10;order by Region ASC">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="TypeS" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="select distinct Regions.Type from Regions&#13;&#10;union &#13;&#10;select '** NONE **' as Type from Regions&#13;&#10;union&#13;&#10;select '**ALL**' as Type from Regions&#13;&#10;order by Type ASC">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="ClassS" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT DISTINCT Class FROM Regions WHERE (Class IS NOT NULL)">
    </asp:SqlDataSource>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td align="left" style="width: 100px; height: 136px" valign="top">
                &nbsp; &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
               
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td width="30%">
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
 
 
 
 
 <table visible="false" width="100%" style="position: absolute; top: 750px; left: 230px;">
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="right" colspan="3">
                            <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        
    </table>
 
</asp:Content>

