using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;

public partial class HomeCM : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adp = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
        MyDataSetTableAdapters.Billing_StatusDataTable tbl = adp.GetData();

        if (tbl.Rows.Count > 0)//Billing running
        {

            lblBillingRunning.Visible = true;
            lblBillingRunning.Text = "Billing Running !!!";
            lblBillingRunning.ForeColor = System.Drawing.Color.Red;

            gdvCountry.Visible = false;
            gdvCustomers.Visible = false;
            gdVendors.Visible = false;

        }
        else
        {
            lblBillingRunning.Visible = false;
            gdvCountry.Visible = true;
            gdvCustomers.Visible = true;
            gdVendors.Visible = true;

            MyDataSetTableAdaptersTableAdapters.trafficTopAdp adpT = new MyDataSetTableAdaptersTableAdapters.trafficTopAdp();
            gdvCustomers.DataSource = adpT.GetDataCM();
            gdvCustomers.DataBind();

            MyDataSetTableAdaptersTableAdapters.trafficVtopAdp adpV = new MyDataSetTableAdaptersTableAdapters.trafficVtopAdp();
            gdVendors.DataSource = adpV.GetDataCM();
            gdVendors.DataBind();

            MyDataSetTableAdaptersTableAdapters.trafficTopCAdp adpC = new MyDataSetTableAdaptersTableAdapters.trafficTopCAdp();
            gdvCountry.DataSource = adpC.GetDataCM();
            gdvCountry.DataBind();

            


        }
    
        
    }   
}
