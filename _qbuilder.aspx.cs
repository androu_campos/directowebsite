using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
public partial class Default3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            //Customer & Vendor

            Session["Table"] = string.Empty;
            txtPageSize.Text = "25";

            dpdCustomer.DataSource = CustomerS;
            dpdCustomer.DataTextField = "ProviderName";
            dpdCustomer.DataValueField = "ProviderName";
            dpdCustomer.DataBind();

            dpdVendor.DataSource = CustomerS;
            dpdVendor.DataTextField = "ProviderName";
            dpdVendor.DataValueField = "ProviderName";
            dpdVendor.DataBind();

            dpdOriginationIP.DataSource = OriginationIPS;
            dpdOriginationIP.DataTextField = "OriginationIP";
            dpdOriginationIP.DataValueField = "OriginationIP";
            dpdOriginationIP.DataBind();

            dpdTerminationIP.DataSource = TerminationIPS;
            dpdTerminationIP.DataTextField = "TerminationIP";
            dpdTerminationIP.DataValueField = "TerminationIP";
            dpdTerminationIP.DataBind();

            dpdCountry.DataSource = CountryS;
            dpdCountry.DataTextField = "Country";
            dpdCountry.DataValueField = "Country";
            dpdCountry.DataBind();         

            dpdType.DataSource = TypeS;
            dpdType.DataTextField = "Type";
            dpdType.DataValueField = "Type";
            dpdType.DataBind();

            SqlCommand sqlSpQuery1 = new SqlCommand();
            sqlSpQuery1.CommandType = System.Data.CommandType.Text;
            sqlSpQuery1.CommandText = "SELECT DISTINCT Class FROM Regions WHERE Class is not NULL UNION SELECT     '** NONE **' AS Class FROM Regions UNION SELECT  '**ALL**' AS Class FROM Regions ORDER BY Class";
            DataSet ResultSet1;
            ResultSet1 = Util.RunQuery(sqlSpQuery1);

            for (int k = 0; k < ResultSet1.Tables[0].Rows.Count; k++)
            {
                dpdClass.Items.Add(ResultSet1.Tables[0].Rows[k]["Class"].ToString());
            }

            dpdSourceMSW.DataSource = SqlDataSource1;
            dpdSourceMSW.DataTextField = "IP";
            dpdSourceMSW.DataValueField = "IP";

        }
        else
        {
            //Session["regionSelected"] = dpdRegions.SelectedValue.ToString();
            //dpdRegions.Items.Clear();
            //string country = dpdCountry.SelectedValue.ToString();
            //country = country.Replace("**ALL**", "%");
            //DataSet dtst =Util.RunQueryByStmntatCDRDB("Select distinct region from regions where country = '" + country + "' UNION SELECT '**ALL**' AS REGION FROM REGIONS UNION SELECT '** NONE **' AS REGION FROM REGIONS ORDER BY REGION ");
            //DataTable tbl = dtst.Tables[0];
            //foreach (DataRow row in tbl.Rows)
            //{
            //    dpdRegions.Items.Add(row[0].ToString());
            //}
        }



    }
    protected void cmdQbuilder_Click(object sender, EventArgs e)
    {

        try
        {
            //string qry; 
            string where = string.Empty;
            string orderby = " Order by ";
            string WHERE = string.Empty;
            StringBuilder newQuery = new StringBuilder();
            StringBuilder newWhere = new StringBuilder();
            StringBuilder newAll = new StringBuilder();
            StringBuilder newGroup = new StringBuilder();

            newQuery.Append("Select ");
            newWhere.Append(" WHERE ");
            newAll.Append(" UNION Select ");
            newGroup.Append(" Group by ");

            if (dpdCustomer.SelectedValue.ToString() != "** NONE **")
            {

                newQuery.Append("Customer,");
                orderby = orderby + "Customer,";
                newGroup.Append("Customer,");
                newWhere.Append("Customer like '" + dpdCustomer.SelectedValue.ToString() + "' AND ");
                newAll.Append("'**ALL**' as Customer,");

            }
            if (dpdVendor.SelectedValue.ToString() != "** NONE **")
            {

                newQuery.Append("Vendor,");
                newGroup.Append("Vendor,");
                orderby = orderby + "Vendor,";
                newWhere.Append("Vendor like '" + dpdVendor.SelectedValue.ToString() + "' AND ");
                newAll.Append(" '**ALL**' as Vendor,");


            }
            if (dpdOriginationIP.SelectedValue.ToString() != "** NONE **")
            {

                newQuery.Append("OriginationIP,");
                newGroup.Append("OriginationIP,");
                newWhere.Append("OriginationIP like '" + dpdOriginationIP.SelectedValue.ToString() + "' AND ");
                newAll.Append("NULL as OriginationIP,");

            }
            if (dpdTerminationIP.SelectedValue.ToString() != "** NONE **")
            {

                newQuery.Append("TerminationIP,");
                newGroup.Append("TerminationIP,");
                newWhere.Append("TerminationIP like '" + dpdTerminationIP.SelectedValue.ToString() + "' AND ");
                newAll.Append("NULL as TerminationIP,");


            }
            if (dpdCountry.SelectedValue.ToString() != "** NONE **")
            {
                newQuery.Append("Country,");
                newGroup.Append("Country,");
                newWhere.Append("Country like '" + dpdCountry.SelectedValue.ToString() + "' AND ");
                newAll.Append("NULL as Country,");
            }

            if (dpdRegions.SelectedValue.ToString() != "** NONE **" && dpdRegions.SelectedValue.ToString() != "")
            {
                string regionS = dpdRegions.SelectedValue.ToString();
                    //Session["regionSelected"].ToString();
                regionS = regionS.Replace("**ALL**","%");
                
                newQuery.Append("Region,");
                newGroup.Append("Region,");
                newWhere.Append("Region like '" + regionS + "' AND ");
                newAll.Append("NULL as Region,");


            }
            if (chkDetail.Checked == true)
            {
                newQuery.Append("RegionPrefix,");
                newGroup.Append("RegionPrefix,");
                newAll.Append("NULL as RegionPrefix,");
            }
            if (dpdType.SelectedValue.ToString() != "** NONE **")
            {

                newQuery.Append("Type,");
                newGroup.Append("Type,");
                newWhere.Append("Type like '" + dpdType.SelectedValue.ToString() + "' AND ");
                newAll.Append("NULL as Type,");

            }
            if (dpdClass.SelectedValue.ToString() != "** NONE **")    //Class
            {

                newQuery.Append("Class,");
                newGroup.Append("Class,");
                newWhere.Append("Class like '" + dpdClass.SelectedValue.ToString() + "' AND ");
                newAll.Append("NULL as Class,");

            }

            if (dpdGateway.SelectedValue.ToString() != "** NONE **")    //Gateway
            {                
                newQuery.Append("VendorID,");
                newGroup.Append("VendorID,");
                newWhere.Append("VendorID like '" + dpdGateway.SelectedValue.ToString() + "' AND ");
                newAll.Append(" '**ALL**' as VendorID,");

            }

            if (dpdSourceMSW.SelectedValue.ToString() != "%")//Source
            {

                newQuery.Append("Source,");
                newGroup.Append("Source,");
                newAll.Append("NULL as Source,");

                if (dpdReportType.SelectedValue.ToString() == "OpSheetInc" || dpdReportType.SelectedValue.ToString() == "OpSheetAll")
                {

                    if (dpdSourceMSW.SelectedValue.ToString() == "**ALL**")
                    {
                        newWhere.Append("Source like '" + dpdSourceMSW.SelectedValue.ToString() + "' AND ");
                    }
                    else
                    {
                        newWhere.Append("Source like '" + dpdSourceMSW.SelectedValue.ToString() + "CTMP" + "' AND ");
                    }


                }

                else if (dpdReportType.SelectedValue.ToString() == "OpSheetY")
                {
                    newWhere.Append("Source like '" + dpdSourceMSW.SelectedValue.ToString() + "' AND ");
                }

            }

            //NEW WHERE
            newWhere.Replace("**ALL**", "%");
            newWhere.Remove(newWhere.Length - 4, 4);
            if (newWhere.Length < 1) newWhere.Replace(newWhere.ToString(), "");

            //Select finalizado
            newQuery.Append("SUM(Minutes)as Minutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls],mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls),SUM(RA)) AS ASR, mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR, mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) AS ACD,ROUND(CAST(AVG(PDD/1000) as float),1) AS PDD ");
            newQuery.Append(" from " + dpdReportType.SelectedValue.ToString() + newWhere.ToString() + " ");
            if (newGroup.Length < 10)
            {
                newGroup.Replace(newGroup.ToString(), " ");
            }
            else
            {
                newGroup.Remove(newGroup.Length - 1, 1);
            }

            newAll.Append("SUM(Minutes)as Minutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls],mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls),SUM(RA)) AS ASR, mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR, mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) AS ACD,ROUND(AVG(PDD)/1000,1) AS PDD ");
            newAll.Append(" from " + dpdReportType.SelectedValue.ToString() + newWhere.ToString() + " ");
            newQuery.Append(newGroup.ToString());


            newQuery.Append(newAll.ToString() + " ORDER BY Minutes DESC ");
            Session["AdQbuilder1"] = newQuery.ToString();
            string tableReport = dpdReportType.SelectedValue.ToString();            
            Response.Redirect("qbuilderV1.aspx?tbl=" + tableReport + "&Pz=" + txtPageSize.Text, false);

        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            

        }

    }


    private System.Data.DataSet RunQuery1(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void dpdCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        dpdRegions.Items.Clear();
        string country = dpdCountry.SelectedValue.ToString();
        country = country.Replace("**ALL**", "%");
        DataSet dtst = Util.RunQueryByStmntatCDRDB("Select distinct region from regions where country = '" + country + "' UNION SELECT '**ALL**' AS REGION FROM REGIONS UNION SELECT '** NONE **' AS REGION FROM REGIONS ORDER BY REGION ");
        DataTable tbl = dtst.Tables[0];
        foreach (DataRow row in tbl.Rows)
        {
            dpdRegions.Items.Add(row[0].ToString());
        }
    }
}
