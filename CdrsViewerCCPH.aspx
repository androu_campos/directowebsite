<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CdrsViewerCCPH.aspx.cs"
    Inherits="CdrsViewer" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="Label7" runat="server" Text="CDR's Current Month Viewer" CssClass="labelTurkH"></asp:Label>
    <br />
    <br />
    <table>
        <tr>
            <td style="width: 71px" valign="top" align="left">
                <asp:Label ID="labelFrom" runat="server" Text="From:" Font-Bold="False" CssClass="labelSteelBlue" />
            </td>
            <td style="width: 100px" valign="top" align="left">
                <asp:TextBox ID="txtFrom" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"
                    AutoPostBack="True" MaxLength="10"></asp:TextBox>
            </td>
            <td style="width: 27px">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" />
            </td>
            <td colspan="4" align="right">
                <asp:HyperLink ID="HyperLink1" runat="server" Font-Bold="True" Font-Names="Arial"
                    Font-Size="8pt" ForeColor="#004D88" NavigateUrl="~/CdrsDownloaderCCPH.aspx">Other Months</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td style="width: 71px;">
                <asp:Label ID="labelTo" runat="server" Text="To:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
            <td style="width: 100px; height: 24px;">
                <asp:TextBox ID="txtTo" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"
                    AutoPostBack="True" MaxLength="10"></asp:TextBox></td>
            <td style="width: 27px;">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:Button ID="btnSearch" runat="server" CssClass="boton" OnClick="btnSearch_Click"
                    Text="Search" />
                <asp:Button ID="btnExport" runat="server" CssClass="boton" OnClick="btnExport_Click"
                    Text="Export" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" valign="top">
                <asp:SqlDataSource ID="SqlCDRSCCPH" runat="server" ConnectionString="<%$ ConnectionStrings:ALLACCESSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>" SelectCommand="select convert(varchar(20),stop,20)as stop,originationip,dialednumber,duration,country,region,CUSTprefix as prefix,str(PRICE,7,6)Price,Custbillminutes as BillMinutes from allaccess.dbo.ratedcdrs_ccph where customer like '%CC-VCIP' and billingdate between @from and @to order by stop">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtFrom" Name="from" PropertyName="Text" />
                        <asp:ControlParameter ControlID="txtTo" Name="to" PropertyName="Text" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
    <asp:GridView ID="gdvCdrs" runat="server" Font-Names="Arial" Font-Size="8pt" AllowPaging="True"
        PageSize="25" OnPageIndexChanging="gdvCdrs_PageIndexChanging" DataSourceID="SqlCDRSCCPH"
        Visible="False" Width="90%" AutoGenerateColumns="False">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
            Font-Size="8pt" HorizontalAlign="center" />
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="stop" HeaderText="Billing" ReadOnly="True" SortExpression="stop"
                ItemStyle-Width="15%" />
            <asp:BoundField DataField="originationip" HeaderText="OriginationIP" SortExpression="originationip" />
            <asp:BoundField DataField="dialednumber" HeaderText="Dialed Number" SortExpression="dialednumber" />
            <asp:BoundField DataField="duration" HeaderText="Duration" SortExpression="duration" />
            <asp:BoundField DataField="country" HeaderText="Country" SortExpression="country" />
            <asp:BoundField DataField="region" HeaderText="Region" SortExpression="region" />
            <asp:BoundField DataField="prefix" HeaderText="Prefix" SortExpression="prefix" />
            <asp:BoundField DataField="Price" HeaderText="Price" ReadOnly="True" SortExpression="Price" />
            <asp:BoundField DataField="BillMinutes" HeaderText="Bill Minutes" SortExpression="BillMinutes" />
        </Columns>
    </asp:GridView>
    <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtFrom" runat="server"
        PopupButtonID="Image1" Format="yyyy-MM-dd">
    </cc1:CalendarExtender>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"
        PopupButtonID="Image2" Format="yyyy-MM-dd">
    </cc1:CalendarExtender>
</asp:Content>
