using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

public partial class AddProvider : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)//Inserta un nuevo registro en la tabla ProviderIp, un nuevo Provider
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {


        try
        {
            //cdrdbDatasetTableAdapters.QueriesTableAdapter adp = new cdrdbDatasetTableAdapters.QueriesTableAdapter();            
            //Validate the Name and Ip...
            int i = 0;
            string name = txtProvider.Text;
            i = name.LastIndexOf(" ") + 1;

            int j = 0;
            string ip = txtIP.Text;
            j = ip.LastIndexOf(" ") + 1;

            bool p = false;

            try
            {
                double x = Convert.ToDouble(txtPrefix.Text);
                p = true;
            }
            catch (Exception)
            {
                p = false;
            }

            //IP BLANK SPACES
            if (j > 0)
            {
                lblIp.Visible = true;
            }
            else
            {
                lblIp.Visible = false;
            }
            //NAMES BLANK SPACES
            if (i > 0)
            {
                lblName.Visible = true;
            }
            else
            {
                lblName.Visible = false;
            }

            //PREFIX NUMBERS
            if (p == false && dpdType.SelectedValue.ToString() != "A")
            {
                lblPrefix.Visible = true;
            }
            else
            {
                lblPrefix.Visible = false;
            }

            if (i > 0 || j > 0 || (p == false && dpdType.SelectedValue.ToString() != "A"))
            {

            }
            else
            {
                //OLD CODE
                //adp.InsertAddIp(txtProvider.Text, txtIP.Text, dpdType.SelectedValue.ToString(), Session["DirectoUser"].ToString(), DateTime.Now);
                //if (dpdType.SelectedValue.ToString() == "C")
                //{

                //    SqlCommand sqlSpQuery = new SqlCommand();
                //    sqlSpQuery.CommandType = CommandType.Text;

                //    sqlSpQuery.CommandText = "INSERT INTO CDRDB.DBO.PROVIDERIP VALUES ('" + txtProvider.Text + "','" + txtIP.Text + "','" + dpdType.SelectedValue.ToString() + "','" + Session["DirectoUser"].ToString() + "',GETDATE())";

                //    RunQuery(sqlSpQuery);                    

                //    loadRates(sender, e);
                //}
                //lblEnd.Text = "New Provider " + txtProvider.Text + " Created Succesfully";
                //HyperLink1.Visible = true;        

                string query = "";
                SqlCommand sqlSpQuery = new SqlCommand();
                sqlSpQuery.CommandType = CommandType.Text;

                if (dpdType.SelectedValue.ToString() == "C")
                {
                    query = "INSERT INTO CDRDB.DBO.PROVIDERIP VALUES ('" + txtProvider.Text + "','" + txtIP.Text + "','C','" + Session["DirectoUser"].ToString() + "',GETDATE()) ";
                    query += "INSERT INTO CDRDB.DBO.RPrefixes VALUES ('C','" + txtIP.Text + "','" + txtPrefix.Text + "') ";

                    if (txtAdj.Text != "")
                    {
                        query += "INSERT INTO CDRDB.DBO.PROVIDERIPMETA VALUES ('" + txtAdj.Text + "','Customer','" + txtIP.Text + "',getdate()) ";
                    }

                    sqlSpQuery.CommandText = query;
                    RunQueryNS(sqlSpQuery);

                    sqlSpQuery.CommandText = "INSERT INTO CDRDB.DBO.PROVIDERIP VALUES ('" + txtProvider.Text + "','" + txtIP.Text + "','C','" + Session["DirectoUser"].ToString() + "',GETDATE()) ";
                    RunQuery(sqlSpQuery);

                    loadRates(sender, e);
                }
                else if (dpdType.SelectedValue.ToString() == "V")
                {
                    query = "INSERT INTO CDRDB.DBO.PROVIDERIP VALUES ('" + txtProvider.Text + "','" + txtIP.Text + "','V','" + Session["DirectoUser"].ToString() + "',GETDATE()) ";
                    query += "INSERT INTO CDRDB.DBO.RPrefixes VALUES ('V','" + txtIP.Text + "'," + txtPrefix.Text + ") ";
                    if (txtAdj.Text != "")
                    {
                        query += "INSERT INTO CDRDB.DBO.PROVIDERIPMETA VALUES ('" + txtAdj.Text + "','Vendor','" + txtIP.Text + "',getdate()) ";
                    }
                    

                    sqlSpQuery.CommandText = query;
                    RunQueryNS(sqlSpQuery);

                    sqlSpQuery.CommandText = "INSERT INTO CDRDB.DBO.PROVIDERIP VALUES ('" + txtProvider.Text + "','" + txtIP.Text + "','V','" + Session["DirectoUser"].ToString() + "',GETDATE()) ";
                    RunQuery(sqlSpQuery);
                }
                else if (dpdType.SelectedValue.ToString() == "G")
                {
                    query = "INSERT INTO CDRDB.DBO.PROVIDERIP VALUES ('" + txtProvider.Text + "','" + txtIP.Text + "','G','" + Session["DirectoUser"].ToString() + "',GETDATE()) ";
                    sqlSpQuery.CommandText = query;

                    RunQueryNS(sqlSpQuery);
                }
                else if (dpdType.SelectedValue.ToString() == "A")
                {
                    string type = txtAdj.Text.Substring(0, 1);

                    if (type == "C")
                    {
                        type = "Customer";
                    }
                    else if (type == "V")
                    {
                        type = "Vendor";
                    }

                    query += "INSERT INTO CDRDB.DBO.PROVIDERIPMETA VALUES ('" + txtAdj.Text + "','" + type + "','" + txtIP.Text + "',getdate()) ";
                    sqlSpQuery.CommandText = query;

                    RunQueryNS(sqlSpQuery);
                }


                lblEnd.Text = "New Provider " + txtProvider.Text + " Created Succesfully";
                HyperLink1.Visible = true;
            }




        }
        catch (Exception ex)
        {
            lblEnd.Text = ex.Message.ToString() + " " + txtProvider.Text.ToString() + " " + txtIP.Text.ToString();
        }


    }

    protected void loadRates(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection();
        string id = string.Empty;
        id = txtIP.Text;
        conn.ConnectionString = "Data Source=170.79.125.55;Initial Catalog=Pricing;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand cmd = new SqlCommand("sp_LoadSTRates", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        conn.Open();
        cmd.Parameters.Add("@Id", SqlDbType.VarChar).Value = id;
        cmd.CommandTimeout = 180;

        try
        {

            cmd.ExecuteNonQuery();
            conn.Close();
        }
        catch (Exception ex)
        {

        }
    }

    private void RunQuery(SqlCommand sqlQuery)
    {
        string connectionString = "Data Source=170.79.125.55;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlConnection DBConnection = new SqlConnection(connectionString);
        DBConnection.Open();
        sqlQuery.Connection = DBConnection;
        sqlQuery.CommandTimeout = 666;
        sqlQuery.ExecuteNonQuery();
    }

    private void RunQueryNS(SqlCommand sqlQuery)
    {
        string connectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlConnection DBConnection = new SqlConnection(connectionString);
        DBConnection.Open();
        sqlQuery.Connection = DBConnection;
        sqlQuery.CommandTimeout = 666;
        sqlQuery.ExecuteNonQuery();
    }

    private System.Data.DataSet RunQueryNS2(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        dbAdapter.SelectCommand.CommandTimeout = 120;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {

        }

        return resultsDataSet;
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        string query = "";
        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.Text;

        query += "SELECT ProviderName FROM CDRDB.dbo.providerip WHERE IP = '" + txtIDSearch.Text + "' ";
        sqlSpQuery.CommandText = query;

        DataSet myDataSet2 = null;
        myDataSet2 = RunQueryNS2(sqlSpQuery);

        if (myDataSet2.Tables[0].Rows.Count > 0)
        {
            lblSearch.Text = "The ID is NOT available.";
            lblSearch.Visible = true;
        }
        else
        {
            lblSearch.Text = "The ID is available.";
            lblSearch.Visible = true;

            txtProvider.Enabled = true;
            dpdType.Enabled = true;
            txtIP.Enabled = true;
            txtPrefix.Enabled = true;
            txtAdj.Enabled = true;
        }
    }
}

