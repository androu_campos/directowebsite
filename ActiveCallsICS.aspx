<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ActiveCallsICS.aspx.cs"
    Inherits="ActiveCalls" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
    
    function dispData(date,calls,graph)
    {
        if (document.getElementById)
	    {
		    document.getElementById('<%=idFrame.ClientID%>').src = "DynamicData.aspx?c=" + calls + "&d=" + date + "&g=" + graph;
	    }
    }    
    
    </script>

    <table width="1024px" height="768px" style="position: absolute; top: 160px; left: 21%;">
        <tr>
            <td valign="top" width="20%" style="height: 20px">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="Active Calls Graph"
                    Width="103px"></asp:Label></td>
            <td valign="top" width="75%" style="height: 20px">
            </td>
            <td valign="top" width="5%" style="height: 20px">
            </td>
        </tr>
        <tr>
            <td valign="top" width="20%">
                &nbsp;</td>
            <td valign="top" width="75%" align="center">
                <iframe src="DynamicData.aspx?c=0&d=0&g=X" runat="server" id="idFrame" frameborder="0"
                    style="width: 190px" height="90"></iframe>
            </td>
            <td valign="top" width="5%">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td align="left" style="height: 65px" valign="top" colspan="2">
                <table align="left" style="width: 177px; height: 120px">
                    <tr>
                        <td align="left" colspan="2" style="height: 43px" valign="top">
                            <table>
                                <tbody>
                                    <tr>
                                        <td style="width: 100px" valign="top">
                                            <cc1:TabContainer ID="TabContainer1" runat="server" Width="177px" Height="330px"
                                                ActiveTabIndex="0">
                                                <cc1:TabPanel ID="tblQuery" runat="server">
                                                    <HeaderTemplate>
                                                        Query Builder
                                                    </HeaderTemplate>
                                                    <ContentTemplate>
                                                        <cc1:CalendarExtender runat="server" TargetControlID="txtFrom" BehaviorID="TabContainer1_tblQuery_CalendarExtender1"
                                                            Enabled="True" PopupButtonID="imgFrom" ID="CalendarExtender1">
                                                        </cc1:CalendarExtender>
                                                        <cc1:CalendarExtender runat="server" TargetControlID="txtTo" BehaviorID="TabContainer1_tblQuery_CalendarExtender2"
                                                            Enabled="True" PopupButtonID="imgTo" ID="CalendarExtender2">
                                                        </cc1:CalendarExtender>
                                                        <table id="tblHorizontal">
                                                            <tbody>
                                                                <tr>
                                                                    <td height="15">
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 19px" valign="top" align="center" bgcolor="#d5e3f0">
                                                                        <asp:Label ID="lblCustomer" runat="server" Text="Customer:" CssClass="labelBlue8"></asp:Label></td>
                                                                    <td style="height: 19px" valign="top">
                                                                        <asp:DropDownList ID="dpdCustomer" runat="server" Font-Size="6pt" Width="101px" CssClass="dropdown"
                                                                            DataTextField="ProviderName" DataValueField="ProviderName">
                                                                        </asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 19px" valign="top" align="center" bgcolor="#d5e3f0">
                                                                        <asp:Label ID="lblVendor" runat="server" Text="Vendor:" CssClass="labelBlue8"></asp:Label></td>
                                                                    <td style="height: 19px" valign="top">
                                                                        <asp:DropDownList ID="dpdVendor" runat="server" Font-Size="6pt" Width="101px" CssClass="dropdown"
                                                                            DataTextField="ProviderName" DataValueField="ProviderName">
                                                                        </asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 19px" valign="top" align="center" bgcolor="#d5e3f0">
                                                                        <asp:Label ID="lblLMC" runat="server" Text="LMC:" CssClass="labelBlue8"></asp:Label></td>
                                                                    <td style="height: 19px" valign="top">
                                                                        <asp:DropDownList ID="dpdLMC" runat="server" Font-Size="6pt" Width="101px" CssClass="dropdown"
                                                                            DataTextField="LMC" DataValueField="LMC">
                                                                        </asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 19px" valign="top" align="center" bgcolor="#d5e3f0">
                                                                        <asp:Label ID="lblCountry" runat="server" Text="Country:" CssClass="labelBlue8"></asp:Label></td>
                                                                    <td style="height: 19px" valign="top">
                                                                        <asp:DropDownList ID="dpdCountry" runat="server" Font-Size="6pt" Width="101px" CssClass="dropdown"
                                                                            DataTextField="Country" DataValueField="Country">
                                                                        </asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 19px" valign="top" align="center" bgcolor="#d5e3f0">
                                                                        <asp:Label ID="lblRegion" runat="server" Text="Region:" CssClass="labelBlue8"></asp:Label></td>
                                                                    <td style="height: 19px" valign="top">
                                                                        <asp:DropDownList ID="dpdRegion" runat="server" Font-Size="6pt" Width="101px" CssClass="dropdown"
                                                                            DataTextField="Region" DataValueField="Region">
                                                                        </asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 19px" valign="top" align="center" bgcolor="#d5e3f0">
                                                                        <asp:Label ID="lblType" runat="server" Text="Type:" CssClass="labelBlue8"></asp:Label></td>
                                                                    <td style="height: 19px" valign="top">
                                                                        <asp:DropDownList ID="dpdOrType" runat="server" Font-Size="7pt" Width="39px" CssClass="dropdown">
                                                                            <asp:ListItem Value="2">==</asp:ListItem>
                                                                            <asp:ListItem Value="1">&lt;&gt;</asp:ListItem>
                                                                        </asp:DropDownList><asp:DropDownList ID="dpdType" runat="server" Font-Size="6pt"
                                                                            Width="62px" CssClass="dropdown" DataTextField="Type" DataValueField="Type">
                                                                        </asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 24px" valign="top" align="center" bgcolor="#d5e3f0">
                                                                        <asp:Label ID="lblClass" runat="server" Text="Class:" CssClass="labelBlue8"></asp:Label></td>
                                                                    <td style="height: 24px" valign="top">
                                                                        <asp:DropDownList ID="dpdOrClass" runat="server" Font-Size="7pt" Width="39px" CssClass="dropdown">
                                                                            <asp:ListItem Value="2">==</asp:ListItem>
                                                                            <asp:ListItem Value="1">&lt;&gt;</asp:ListItem>
                                                                        </asp:DropDownList><asp:DropDownList ID="dpdClass" runat="server" Font-Size="6pt"
                                                                            Width="62px" CssClass="dropdown" DataTextField="Class" DataValueField="Class">
                                                                        </asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 24px" valign="top" align="center" bgcolor="#d5e3f0">
                                                                        <asp:Label ID="lblFrom" runat="server" Text="From:" CssClass="labelBlue8"></asp:Label></td>
                                                                    <td style="height: 24px" valign="top">
                                                                        <asp:TextBox ID="txtFrom" runat="server" Width="56px" CssClass="dropdown"></asp:TextBox>
                                                                        <asp:Image ID="imgFrom" runat="server" ImageUrl="~/Images/calendar1.gif"></asp:Image></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 24px" valign="top" align="center" bgcolor="#d5e3f0">
                                                                        <asp:Label ID="Label3" runat="server" Text="To:" CssClass="labelBlue8"></asp:Label></td>
                                                                    <td style="height: 24px" valign="top">
                                                                        <asp:TextBox ID="txtTo" runat="server" Width="56px" CssClass="dropdown"></asp:TextBox>
                                                                        <asp:Image ID="imgTo" runat="server" ImageUrl="~/Images/calendar1.gif"></asp:Image></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" align="left" height="15">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" colspan="2">
                                                                        <asp:Button ID="cmdGraph" OnClick="cmdGraph_Click" runat="server" Text="View Graph"
                                                                            Font-Size="8pt" Width="74px" Height="17px" CssClass="boton"></asp:Button></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                                <cc1:TabPanel ID="productsTab" runat="server" TabIndex="1" HeaderText="Products">
                                                    <HeaderTemplate>
                                                        <asp:Label runat="server" Text="Products" ID="Label4"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ContentTemplate>
                                                        <cc1:CalendarExtender runat="server" TargetControlID="txtFrom2" BehaviorID="TabContainer1_productsTab_CalendarExtender3"
                                                            Enabled="True" PopupButtonID="imgFrom2" ID="CalendarExtender3">
                                                        </cc1:CalendarExtender>
                                                        <cc1:CalendarExtender runat="server" TargetControlID="txtTo2" BehaviorID="TabContainer1_productsTab_CalendarExtender4"
                                                            Enabled="True" PopupButtonID="imgTo2" ID="CalendarExtender4">
                                                        </cc1:CalendarExtender>
                                                        <table align="left">
                                                            <tbody>
                                                                <tr>
                                                                    <td valign="top" align="left">
                                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" Font-Size="7pt" CssClass="dropdown"
                                                                            AppendDataBoundItems="True">
                                                                            <asp:ListItem Value="0">Mexico CPP</asp:ListItem>
                                                                            <asp:ListItem Value="1">Mexico NEA</asp:ListItem>
                                                                            <asp:ListItem Value="2">Mexico On Net</asp:ListItem>
                                                                            <asp:ListItem Value="3">Philippines Globe</asp:ListItem>
                                                                            <asp:ListItem Value="4">Philippines Smart</asp:ListItem>
                                                                            <asp:ListItem Value="5">Philippines PLDT</asp:ListItem>
                                                                            <asp:ListItem Value="6">Brazil Fijo</asp:ListItem>
                                                                            <asp:ListItem Value="7">Brazil Mobile</asp:ListItem>
                                                                            <asp:ListItem Value="8">Peru ROC</asp:ListItem>
                                                                            <asp:ListItem Value="9">Wholesale vs Retail</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                    <td align="left">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <table align="left">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="left" width="20">
                                                                                        <asp:Label ID="lblFrom2" runat="server" Text="From:" CssClass="labelBlue8"></asp:Label>
                                                                                    </td>
                                                                                    <td align="left" width="20">
                                                                                        <asp:TextBox ID="txtFrom2" runat="server" Width="60px" Height="15px" CssClass="dropdown"></asp:TextBox>
                                                                                    </td>
                                                                                    <td align="left" width="20">
                                                                                        <asp:Image ID="imgFrom2" runat="server" ImageUrl="NeoIma/calendar1.gif"></asp:Image></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" width="20">
                                                                                        <asp:Label ID="lblTo2" runat="server" Text=" To:" CssClass="labelBlue8"></asp:Label></td>
                                                                                    <td align="left" width="20">
                                                                                        <asp:TextBox ID="txtTo2" runat="server" Width="60px" Height="15px" CssClass="dropdown"></asp:TextBox></td>
                                                                                    <td align="left" width="20">
                                                                                        <asp:Image ID="imgTo2" runat="server" ImageUrl="NeoIma/calendar1.gif"></asp:Image></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Button ID="Button1" OnClick="Button1_Click" Font-Size="7pt" runat="server" Text="View Graph"
                                                                            CssClass="boton"></asp:Button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                            </cc1:TabContainer><br />
                                            <asp:Button ID="cmdReset" OnClick="cmdReset_Click" runat="server" Text="reset" Width="46px"
                                                CssClass="boton"></asp:Button>
                                            <table accesskey="tblMaxpoint" id="tblMaxpoint" width="177" border="1" runat="server"
                                                visible="false">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" bgcolor="#d5e3f0" colspan="2">
                                                            <asp:Label ID="lblYmax" runat="server" Text="MAX" CssClass="labelBlue8"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="2" style="height: 26px">
                                                            <asp:Label ID="lblValue" runat="server" Text="Label" ForeColor="Black" Font-Size="7pt"
                                                                Font-Bold="True" Width="100%" CssClass="labelBlue8"></asp:Label><br />
                                                            <asp:Label ID="lblMaxPorts" runat="server" Text="Label" ForeColor="Black" Font-Size="7pt"
                                                                Font-Bold="True" Width="100%" CssClass="labelBlue8"></asp:Label></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="width: 102px" valign="top" rowspan="2">
                                            <asp:Label ID="lblGraphs" runat="server" Text="Under Maintenance..." CssClass="labelBlue8"
                                                Font-Bold="True" Width="117px" Visible="False"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; height: 62px" valign="top">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="30%">
                            <asp:Button ID="btn_excel" runat="server" CssClass="boton" OnClick="btn_excel_Click"
                                Text="Export To EXCEL" Width="83px" /></td>
                        <td align="left" width="30%">
                        </td>
                    </tr>
                </table>
                <DCWC:Chart ID="Chart1" runat="server" Height="449px" Palette="Pastel" Width="627px">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <%--<DCWC:Series ChartType="Area" Color="165, 100, 69, 0" Name="AvailablePorts" ShadowOffset="1"
                            XValueType="DateTime">
                        </DCWC:Series>--%>
                        <DCWC:Series BorderColor="Transparent" Color="252, 255, 165, 0" Name="Ports" ShadowOffset="1"
                            XValueType="DateTime">
                        </DCWC:Series>
                        <DCWC:Series BorderStyle="Dot" ChartType="Line" Color="252, 255, 165, 0" Name="MaxPorts"
                            XValueType="DateTime">
                        </DCWC:Series>
                        <DCWC:Series BorderColor="Transparent" ChartType="Area" Color="165, 255, 69, 0" Name="Calls"
                            ShadowOffset="1" XValueType="DateTime">
                        </DCWC:Series>
                        <DCWC:Series BorderStyle="Dot" ChartType="Line" Color="Red" Name="MaxCalls" XValueType="DateTime">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue" />
                    <ChartAreas>
                        <DCWC:ChartArea BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft" BorderColor=""
                            Name="Default">
                            <AxisX Margin="False" Title="DateTime">
                                <LabelStyle Format="g" />
                            </AxisX>
                            <AxisY Title="Calls">
                                <LabelStyle Format="N0" />
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Alignment="TopLeft" Color="245, 0, 0, 0" Font="Arial, 8.25pt, style=Bold"
                            Name="Title1" Text="">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
            <td style="height: 65px" valign="top" width="5%">
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="1" width="20%">
            </td>
            <td colspan="2" width="80%">
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
