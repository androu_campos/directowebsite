using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UploadFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
   
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            if (FileUpload1.HasFile)
            {
                FileUpload1.SaveAs(@"D:\reports\" + FileUpload1.FileName.ToString());
                lblUpload.Text = "File Uploaded: " + FileUpload1.FileName;

            }
            else
            {
                lblUpload.Text = "No File Uploaded";
            }
        }

        catch(Exception ex)
        {
            string msg = ex.Message.ToString();
        
        }
    }
}
