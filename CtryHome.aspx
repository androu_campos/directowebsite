<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CtryHome.aspx.cs" Inherits="CtryHome" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ScriptManager runat="server" id="ScpMng">
</asp:ScriptManager>

    <table align="left" id="tblMain" cellpadding="6"  style=" height:666PX; position:absolute; top:180px; left:220px;">
        <tr>
            <td rowspan="4" style="width: 100px; height: 800px;" valign="top">
                <img border="0" src="Images/meccasummary.gif" width="264" height="24">
                <table border="0" width="264" id="tblSummary" cellspacing="0" cellpadding="0">
                    <tr height="18px">
                        <td width="30%" height="20">
                            </td>
                        <td width="30%" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">Latest</font></b>
                        </td>
                        <td width="40%" colspan="2" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">Change</font></b></td>
                    </tr>
                    <tr>
                        <td width="30%" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="30%" align="left">
                            <font color="#004D88" style="font-size: 8pt; margin-left: 10px; margin-top: 5px;
                                margin-bottom: 5px; color: #004d88; font-family: Arial">Tot Minutes</font>
                        </td>
                        <td width="30%" align="center">
                            
                                <asp:Label ID="lblMinutesY" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                        <td width="40%" align="center">
                           <asp:Label ID="lblMinutesC" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                            </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image4" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="30%" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="30%">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">Calls</font>
                        </td>
                        <td width="30%" align="center">
                                <asp:Label ID="lblCallsY" CssClass="labelSteelBlue" runat="server"></asp:Label></td>
                        <td width="40%" align="center">
                           <asp:Label ID="lblCallsC" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                            </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image5" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="30%" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="30%">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Gross Profit"></asp:Label>
                        </td>
                        <td width="30%" align="center">
                           <asp:Label ID="lblProfitY" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                        <td width="40%" align="center">
                          <asp:Label ID="lblProfitC" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label></td>
                        <td width="19" align="center">
                            <asp:Image ID="Image6" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="30%" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="30%" style="height: 30px">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">ASR</font>
                        </td>
                        <td width="30%" style="height: 30px" align="center">
                            <asp:Label ID="lblASRY" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                        <td width="40%" style="height: 30px" align="center">
                            <asp:Label ID="lblASRC" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                            </td>
                        <td width="19" align="center" style="height: 30px">
                            <asp:Image ID="Image7" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="30%" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="30%">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">ACD</font>
                        </td>
                        <td width="30%" align="center">
                           <asp:Label ID="lblACDY" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                        <td width="40%" align="center">
                            <asp:Label ID="lblACDC" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image8" runat="server" /></td>
                    </tr>
<tr>
                        <td width="30%" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="30%">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">Total Cost</font>
                        </td>
                        <td width="30%" align="center">
                            <asp:Label ID="LabelTotalCost" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                        <td width="40%" align="center">
                             <asp:Label ID="LabelTotalCostCh" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label></td>
                        <td width="19" align="center">
                            <asp:Image ID="Image13" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="30%" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="30%">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">Total
                                    Sales</font>
                        </td>
                        <td width="30%" align="center">
                            <asp:Label ID="LabelTotalSales" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                        <td width="40%" align="center">
                            
                              <asp:Label ID="LabelTotalSalesCh" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label></td>
                        <td width="19" align="center">
                            <asp:Image ID="Image14" runat="server" /></td>
                    </tr>
                </table>
                <br />
                <img border="0" src="Images/meccastatus.gif" width="264" height="24"><br />

                <ul style="color: #004D88" type="square">
                    <li style="text-align: left;">
                        <asp:Label ID="lblBillingRunning" runat="server" CssClass="labelSteelBlue" Text="Billing Running"
                            Visible="False" Font-Bold="False" Font-Size="8pt" Font-Names="Arial" ForeColor="#666666"
                            Width="193px"></asp:Label>
                    </li>
                    <li style="text-align: left;">
                        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="labelSteelBlue" NavigateUrl="~/unrated.aspx"
                            Font-Size="8pt" Font-Names="Arial" ForeColor="#666666" Width="193px">[HyperLink1]</asp:HyperLink>
                    </li>
                    <li style="text-align: left;">
                        <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Arial" Font-Size="8pt"
                            ForeColor="#666666" NavigateUrl="~/Unknown.aspx" Width="193px">HyperLink</asp:HyperLink></li></ul>
<p>
                    <asp:Image ID="Image12" runat="server" ImageUrl="~/Images/specialc.gif" />
                </p>
                <ul style="color: #004D88; font-size: 8pt" type="square">
                    <li style="text-align: left;">
                        <asp:Label ID="Label12" runat="server" Text="Label" CssClass="labelSteelBlue" ForeColor="#666666"
                            Width="193px"></asp:Label>
                    </li>
                    <li style="text-align: left;">
                        <asp:Label ID="Label13" runat="server" Text="Label" CssClass="labelSteelBlue" ForeColor="#666666"
                            Width="193px"></asp:Label>
                    </li>
                    <li style="text-align: left;">
                        <asp:Label ID="Label14" runat="server" Text="Label" CssClass="labelSteelBlue" ForeColor="#666666"
                            Width="193px"></asp:Label>
                    </li>
                    <li style="text-align: left;">
                        <asp:Label ID="Label15" runat="server" Text="Label" CssClass="labelSteelBlue" ForeColor="#666666"
                            Width="193px"></asp:Label>
                    </li>
                </ul>
                <br />
              
                
            </td>
            <td rowspan="4" style="width: 100px; height: 800px;" valign="top">
                <table border="0" width="264" id="table13" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top">
                            <table border="0" width="264" id="table15" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" bgcolor="#d5e3f0" colspan="3" height="20" style="width: 276px">
                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/top.gif" /></td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="left" height="24" valign="bottom" style="width: 276px"><table style="width: 101%">
                                        <tr>
                                            <td style="width: 33%" valign="bottom">
                                                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Customers"></asp:Label></td>
                                            <td style="width: 37%" align="right" valign="bottom">
                                                <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Minutes"></asp:Label></td>
                                            <td align="right" valign="bottom" width="34%">
                                                <asp:Label ID="lblHeaderProfit1" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Mecca Profit"></asp:Label></td>
                                        </tr>
                                    </table>
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td id="IMG" colspan="3" align="center" valign="top" style="width: 276px">
                                        <asp:Label ID="lblCusN" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5" style="width: 276px">
                                        <asp:GridView RowStyle-Height="18px" ID="gdvCustomers" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Customer" HtmlEncode="False" InsertVisible="False" ShowHeader="False">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="220px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CMinutes" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0.00}"
                                                    ItemStyle-Width="199px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:C}"
                                                    ItemStyle-Width="199px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                            <RowStyle Height="18px" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td colspan="3" style="width: 276px">
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="left" height="24" style="width: 276px">
                                        <table width="100%" align="center">
                                            <tr>
                                                <td style="width: 33%" valign="bottom">
                                                    <asp:Label ID="lblHeaderVendors" runat="server" Text="Vendors" CssClass="labelBlue8" Font-Bold="True"></asp:Label></td>
                                                <td align="right" valign="bottom" width="33%">
                                                    <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Minutes"></asp:Label><td valign="bottom" width="34%" align="right">
                                                    <asp:Label ID="lblHeaderProfit2" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Mecca Profit"></asp:Label></tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center" style="width: 276px">
                                        <img border="0" src="Images/line.gif" width="264" height="1"><br />
                                        <asp:Label ID="lblVenN" runat="server" ForeColor="Red" Text="Not available"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5" style="width: 276px">
                                        <asp:GridView ID="gdVendors" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Vendor">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="220px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="VMinutes" DataFormatString="{0:0,0.00}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="199px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" DataFormatString="{0:C}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="199px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td colspan="3" style="width: 276px">
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="left" height="24" style="width: 276px">
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 33%" valign="bottom">
                                                    <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Countries"></asp:Label></td>
                                                <td style="width: 33%" align="right" valign="bottom">
                                                    <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Minutes"></asp:Label></td>
                                                <td style="width: 34%" valign="bottom" align="right">
                                                    <asp:Label ID="lblHeaderProfit3" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Mecca Profit"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center" style="width: 276px">
                                        <img border="0" src="Images/line.gif" width="264" height="1"><br />
                                        <asp:Label ID="lblCouN" runat="server" ForeColor="Red" Text="Not available"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5" style="width: 276px">
                                        <asp:GridView ID="gdvCountry" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Country" ItemStyle-Width="199px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="VMinutes" DataFormatString="{0:0,0.00}" ItemStyle-Width="220px"
                                                    HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" DataFormatString="{0:C}" ItemStyle-Width="199px"
                                                    HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
            <td rowspan="4" style="width: 26px; height: 800px;" valign="top">
                <table border="0" width="264" id="table14" cellspacing="0" cellpadding="0">
                    
                    
                    <tr>
                        <td align="left">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/noc1.gif" />
                        
                    
                            <asp:GridView ID="gdvRouting" runat="server" Font-Size="8pt" Font-Names="Arial" Width="264px"
                                AutoGenerateColumns="false" CssClass="labelSteelBlue">
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#2461BF" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                    Font-Size="8pt" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="Vendor" HeaderText="Vendor" HeaderStyle-HorizontalAlign="Center" >
                                        <ItemStyle HorizontalAlign="Left" Wrap="true"  />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PenaltyStatus" HeaderText="PenaltyStatus" HeaderStyle-HorizontalAlign="Center" >
                                        <ItemStyle HorizontalAlign="Left" Wrap="true" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Destination" HeaderText="Destination" HeaderStyle-HorizontalAlign="Center" >
                                        <ItemStyle HorizontalAlign="Left" Wrap="true" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="labelSteelBlue" Font-Size="7pt"
                                PostBackUrl="~/MonitoringSearch.aspx">more...</asp:LinkButton></td>
                    </tr>
                    
                    
                    <tr>
                        <td align="left">
                            <br />
                            <asp:Image ID="Image10" runat="server" ImageUrl="~/Images/deploy.gif" />

                           
                            
                            <asp:GridView ID="gdvDeployment" runat="server" Font-Names="Arial" Font-Size="8pt" AutoGenerateColumns="false" Width="264px" CssClass="labelSteelBlue">
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#2461BF" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                    Font-Size="8pt" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="Vendor" HeaderText="Vendor" HeaderStyle-HorizontalAlign="Center" >
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-HorizontalAlign="Center" >
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Capacity" HeaderText="Capacity" HeaderStyle-HorizontalAlign="Center" >
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:HyperLink ID="HyperLink3" runat="server" CssClass="labelSteelBlue" Font-Size="7pt"
                                NavigateUrl="~/MonitoringSearch.aspx">more...</asp:HyperLink></td>
                    </tr>
                   
                </table>
                <br />
                <asp:Panel ID="pnlCtryMan" runat="server" Height="50px" Width="125px" Visible="False">
                    <table id="Commissions">
                        <tr>
                            <td colspan="3" height="9">
                                <asp:Image ID="imgCtryMan5" runat="server" ImageUrl="~/Images/Commissions.gif" /></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" style="height: 100%">
                                <asp:Label ID="Label16" runat="server" ForeColor="Red" Text="Not available" Visible="False"></asp:Label>
                                <table border="0" width="264" id="tblTop5" cellspacing="0" cellpadding="0" height="100%">
                                    <tr height="18px">
                                        <td width="20%">
                                        </td>
                                        <td height="20" width="25%">
                                            <b><font color="orange" style="font-size: 7pt; font-family: Arial;
                                                margin-bottom: 2px; vertical-align: middle;">Calls</font></b>
                                        </td>
                                        <td height="20" valign="middle" width="25%">
                                            <b><font color="orange" style="font-size: 7pt;  font-family: Arial;
                                                margin-bottom: 2px; vertical-align: middle;">Minutes</font></b></td>
                                        <td colspan="2" height="20" valign="middle" style="width: 25%">
                                            <b><font color="orange" style="font-size: 7pt; font-family: Arial;
                                                margin-bottom: 2px; vertical-align: middle;">Gross Profit</font></b></td>
                                        <td width="5%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                    </tr>
                                    <tr>
                                        <td valign="middle" width="20%">
                                            <asp:Label ID="lblCtry1" runat="server" CssClass="labelBlue8" Text="Andre"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall1" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                        <td align="center" width="25%">
                                         
                                         
                                                <asp:Label ID="lblTMin1" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td align="center" colspan="2" style="width: 25%">
                                            
                                                <asp:Label ID="lblTgross1" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                                
                                        </td>
                                        <td width="5%"><asp:Image ID="imgStatus1" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                    </tr>
                                    <tr>
                                        <td valign="middle" width="20%">
                                            <asp:Label ID="lblCtry3" runat="server" CssClass="labelBlue8" Text="Hamit"></asp:Label></td>
                                        <td width="25%">
                                            <asp:Label ID="lblTCall2" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                        <td align="center" width="25%">
                                             
                                                <asp:Label ID="lblTMin2" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td colspan="2" style="width: 25%">
                                            
                                                <asp:Label ID="lblTgross2" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                                
                                        </td>
                                        <td width="5%"><asp:Image ID="imgStatus2" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            
                                                <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <asp:Label ID="lblCtry5" runat="server" CssClass="labelBlue8" Text="Jake"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall3" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                        <td align="center" width="25%">
                                            
                                                
                                                    <asp:Label ID="lblTMin3" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td colspan="2" style="width: 25%">
                                           
                                                <asp:Label ID="lblTgross3" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                                
                                        </td>
                                        <td width="5%"><asp:Image ID="imgStatus3" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                           
                                                <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <asp:Label ID="lblCtry6" runat="server" Text="Jimb" CssClass="labelBlue8"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall4" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                        <td align="center" width="25%">
                                                
                                                    <asp:Label ID="lblTMin4" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td colspan="2" style="width: 25%">
                                                <asp:Label ID="lblTgross4" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                                
                                        </td>
                                        <td width="5%"><asp:Image ID="imgStatus4" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <font color="#004D88">
                                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            <asp:Label ID="lblCtry8" runat="server" Text="Roberto" CssClass="labelBlue8"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall5" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
                                        <td align="center" width="25%">
                                           
                                                
                                                    <asp:Label ID="lblTMin5" runat="server" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td colspan="2" style="width: 25%">
                                          
                                                <asp:Label ID="lblTgross5" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                                
                                        </td>
                                        <td width="5%"><asp:Image ID="imgStatus5" runat="server" /></td>
                                    </tr>
                                    
                                    <tr>
                                        <td width="20%" colspan="5">                                            
                                                <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                            &nbsp;<asp:Label ID="lblCtry9" runat="server" Text="Shi Jie" CssClass="labelBlue8"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall6" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label>
                                        </td>
                                        <td align="center" width="25%">
                                                    <asp:Label ID="lblTMin6" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label></td>
                                        <td colspan="2" style="width: 25%">
                                                <asp:Label ID="lblTgross6" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                                
                                        </td>
                                        <td width="5%"><asp:Image ID="imgStatus6" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <font color="#004D88">
                                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">
                                           <asp:Label ID="lblCtry10" runat="server" Text="Vicente" CssClass="labelBlue8"></asp:Label></td>
                                        <td align="center" width="25%">
                                            <asp:Label ID="lblTCall7" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label>
                                        </td>
                                        <td align="center" width="25%">
                                                    <asp:Label ID="lblTMin7" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                        </td>
                                        <td colspan="2" style="width: 25%">
                                                <asp:Label ID="lblTgross7" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label>
                                                
                                        </td>
                                        <td width="5%"><asp:Image ID="imgStatus7" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <font color="#004D88">
                                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <font color="#004D88">
                                                </font></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" colspan="5">
                                            <font>
                                                </font></td>
                                    </tr>
                                 
                                    
                                    <tr>
                                        <td align="left" width="20%">
                                            <asp:LinkButton ID="LinkButton2" runat="server" CssClass="labelSteelBlue" PostBackUrl="~/CommTry.aspx" Font-Size="7pt">more...</asp:LinkButton></td>
                                        <td align="center" width="25%">
                                            </td>
                                        <td width="25%">
                                                
                                        </td>
                                        <td colspan="2" style="width: 25%">
                                             </td>
                                        <td width="5%"> </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td rowspan="4" style="width: 26px; height: 800px" valign="top">
                <p>
                                <asp:Image ID="Image17" runat="server" ImageUrl="~/Images/wid.gif" />
                    <table>
                        <tr>
                            <td style="width: 606px">
                                <asp:GridView ID="GridView2" runat="server" DataSourceID="SqlDataSource2" Font-Names="Arial"
                                    Font-Size="8pt" CssClass="dropdown" Width="275px" AutoGenerateColumns="False"
                                    AllowPaging="True" PageSize="5">
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle BackColor="#D5E3F0" ForeColor="#004A8F" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                        Font-Size="8pt" Width="25px" />
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField DataField="Country" HeaderText="Country">
                                            <HeaderStyle VerticalAlign="Middle"  HorizontalAlign="Center" Width="25px" />
                                        </asp:BoundField>
                                        <asp:BoundField  DataField="City" HeaderText="City" HtmlEncode="False">
                                            <HeaderStyle VerticalAlign="Middle"  HorizontalAlign="Center" Width="25px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Minimum" HeaderText="Minimum Sell Price" HtmlEncode="False">
                                            <HeaderStyle Width="25px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Average_Cost" HeaderText="Average Cost">
                                            <HeaderStyle Width="25px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Expected_ASR" HeaderText="Expected ASR">
                                            <HeaderStyle Width="25px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Yesterday_ACD" HeaderText="Yesterday ACD">
                                            <HeaderStyle Width="25px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Avail_Capacity" HeaderText="Available Channels">
                                            <HeaderStyle Width="25px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Service_Level" HeaderText="Service Level">
                                            <HeaderStyle Width="25px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Comments" HeaderText="Comments">
                                            <HeaderStyle Width="25px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Vendor_Billing" HeaderText="Vendor Billing">
                                            <HeaderStyle Width="25px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Date_Updated" HeaderText="Date Updated">
                                            <HeaderStyle Width="25px" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                <asp:LinkButton ID="LinkButton4" runat="server" CssClass="labelSteelBlue" Font-Size="7pt"
                                    PostBackUrl="~/PushListEdit.aspx">More...</asp:LinkButton></td>
                        </tr>
                    </table>
                </p>
                <br />
                <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/WIDGET.gif" />
                <ul style="color: #004D88" type="square">
                    <li style="text-align: left;">
                        <asp:Label ID="Label8" runat="server" Text="Label" CssClass="labelSteelBlue" Width="218px"></asp:Label>
                    </li>
                    <%--<li style="text-align: left;">--%>
                        <asp:Label ID="Label9" runat="server" Text="Label" CssClass="labelSteelBlue" Width="218px"></asp:Label>
                    <%--</li>--%>
                    <%--<li style="text-align: left;">--%>
                    <asp:Label ID="Label10" runat="server" Text="Label" CssClass="labelSteelBlue" Width="218px"></asp:Label>
                    <%--</li>--%>
                    <%--<li style="text-align: left;">--%>
                    <asp:Label ID="Label11" runat="server" Text="Label" CssClass="labelSteelBlue" Width="218px"></asp:Label>
                </ul>
                <asp:Image ID="Image15" runat="server" ImageUrl="~/Images/widgetaz.gif" />
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="labelSteelBlue"
                    DataSourceID="SqlDataSource1" Font-Names="Arial" Font-Size="7pt" Width="266px">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="countryLcr" HeaderText="Country" SortExpression="countryLcr"
                            HeaderStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="owner" HeaderText="Responsible" SortExpression="owner" HeaderStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="status" HeaderText="Status" SortExpression="status" HeaderStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="priority" HeaderText="Priority" SortExpression="priority"
                            HeaderStyle-HorizontalAlign="Center" />
                    </Columns>
                </asp:GridView>
                <asp:LinkButton ID="LinkButton3" runat="server" CssClass="labelSteelBlue" Font-Size="7pt" PostBackUrl="~/WidgetLcr.aspx">more...</asp:LinkButton>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>" SelectCommand="SELECT TOP (5) countryLcr, owner, status, priority, Date_received FROM WidgetEntry WHERE (Date_Completed is NULL) ORDER BY Date_received DESC">
                </asp:SqlDataSource>
                
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                SelectCommand="SELECT Country, City, Average_Cost, Expected_ASR, Yesterday_ACD, Avail_Capacity, Service_Level, Comments, Vendor_Billing, Date_Updated, Minimum FROM Push_List">
            </asp:SqlDataSource>
            </td>
        </tr>                    
       
    </table>
    
    
    <table style="position:absolute;top: 950px; left: 230px;">
    
    <tr width="100%">
    <td width="100%" colspan="3"> 
        <asp:Image ID="Image9" runat="server" ImageUrl="Images/footer.gif" /></td>
    </tr>
    
    <tr width="100%">
    <td width="100%">
    
    <table id="tblPowered"> 
    <tr>  
    </tr>
    <tr> 
    
    <td>
        <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                    Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label>
    </td>
    
    <td>
        <asp:Image ID="Image3" runat="server" ImageUrl="Images/computertel.gif" />
    </td>
    
    </tr>
    </table>
    
    </td>
    <td> </td>
    <td> </td>
    </tr>
    
    </table>
    
    
</asp:Content>

