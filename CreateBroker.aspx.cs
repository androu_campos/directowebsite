using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ManageSim : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void cmdCreate_Click(object sender, EventArgs e)
    {
        try
        {
            AccessDatasetTableAdapters.BrokerTableAdapter adp = new AccessDatasetTableAdapters.BrokerTableAdapter();
            adp.Insert(txtName.Text);
            lblInsert.Text = "Broker created succesfull";
            lblInsert.Visible = true;
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            lblInsert.Text = "Error creating Broker";
            lblInsert.Visible = true;
        }
    }
}
