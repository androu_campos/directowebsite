<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="HomeCCPH.aspx.cs"
    Inherits="Home" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>--%>
<%@ OutputCache Location="None" VaryByParam="None" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="x">
    </asp:ScriptManager>

    <script language="javascript" type="text/javascript">
    
     function showImage(imgId,typ) {
                var objImg = document.getElementById(imgId);
                if (objImg) {
                    if (typ == "1")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_2b.gif";
                   else if (typ == "2")
                       objImg.src = "Images/botonesMeccaHome/tabmeccahome_2a.gif";
                   else if (typ == "3")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_3b.gif";
                   else if (typ == "4")
                       objImg.src = "Images/botonesMeccaHome/tabmeccahome_3a.gif";  
               }
         }
         
    </script>

    <table align="left" id="tblMain" cellpadding="6" style="height: 400PX; width: 50%;
        position: absolute; top: 140px; left: 220px;">
        <tr>
            <td style="height: 300px; width: 50%;" valign="top" align="left" width="50%">
                <table id="balanceT" title="Balance" runat="server">
                    <tr>
                        <td style="height: 850px; width: 100%;" valign="top">
                            <asp:Label ID="labelBalance" runat="server" Text="Remaning Balance:" CssClass="labelBal"></asp:Label>
                            <br />
                            <asp:Label ID="lblBal" runat="server" CssClass="labelOrange20"></asp:Label>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <table align="left" id="tblMAinSum" cellpadding="6" style="height: 400PX; width: 50%;
        top: 200px; left: 220px;">
        <tr>
            <td rowspan="2" style="height: 300px;" valign="top" width="50%">
                <%--<img border="0" src="Images/meccasummary.gif" height="24">
                <table border="0" id="tblSummary" cellspacing="0" cellpadding="0">
                    <tr height="18px">
                        <td width="72" height="20">
                        </td>
                        <td width="71" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">&nbsp; &nbsp;&nbsp; Latest</font></b>
                        </td>
                        <td width="113" colspan="2" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">&nbsp; &nbsp; &nbsp;&nbsp; Change</font></b></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72" align="left">
                        <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                            <asp:Label ID="labelAttempt" runat="server" CssClass="labelBlue8" Text="Attempt"></asp:Label>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp;
                                <asp:Label ID="lblAttemptsY" runat="server"></asp:Label>
                            </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblAttemptsC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;
                        </td>
                        <td align="center" style="width: 19px">
                            <asp:Image ID="Image10" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72" align="left">
                        <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                            <asp:Label ID="labelMin" runat="server" CssClass="labelBlue8" Text="Minutes"></asp:Label>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp;
                                <asp:Label ID="lblMinutesY" runat="server"></asp:Label>
                            </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblMinutesC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;</td>
                        <td align="center" style="width: 19px">
                            <asp:Image ID="Image4" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="labelCalls" runat="server" CssClass="labelBlue8" Text="Calls"></asp:Label>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp; &nbsp;
                                <asp:Label ID="lblCallsY" runat="server"></asp:Label></font></td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#FF3300" style="font-size: 8pt; color: red; font-family: Arial">
                                    <asp:Label ID="lblCallsC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td align="center" style="width: 19px">
                            <asp:Image ID="Image5" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="labelProf" runat="server" CssClass="labelBlue8" Text="Profit"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblProfitY" runat="server"></asp:Label></font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblProfitC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td align="center" style="width: 19px">
                            <asp:Image ID="imgProf" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="labelTotS" runat="server" CssClass="labelBlue8" Text="Total Sales"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblTotSalesY" runat="server"></asp:Label></font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblTotSalesC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td align="center" style="width: 19px">
                            <asp:Image ID="imgSales" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="labelASR" runat="server" CssClass="labelBlue8" Text="ASR"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblASRY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblASRC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td align="center" style="width: 19px">
                            <asp:Image ID="Image7" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="labelABR" runat="server" CssClass="labelBlue8" Text="ABR"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblABRY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblABRC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td align="center" style="width: 19px">
                            <asp:Image ID="Image15" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="labelACD" runat="server" CssClass="labelBlue8" Text="ACD"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblACDY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblACDC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td align="center" style="width: 19px">
                            <asp:Image ID="Image8" runat="server" /></td>
                    </tr>
                </table>
            </td>
            <td rowspan="2" style="height: 300px;" valign="top" width="50%">
                <img border="0" src="Images/meccastatus.gif" height="24"><br />
                <ul style="color: #004D88; font-size: 8pt" type="square">
                    <li style="text-align: left;">
                        <asp:Label ID="lblBillingRunning" runat="server" CssClass="labelSteelBlue" Text="Billing is not Running"
                            Font-Bold="False" Font-Size="8pt" Font-Names="Arial" ForeColor="#666666" Width="193px"></asp:Label>
                    </li>
                </ul>
            </td>--%>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <asp:SqlDataSource ID="sqlBal" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="Select Balance From CCPH_Balance where CCName = @Customer">
        <SelectParameters>
            <asp:SessionParameter Name="Customer" SessionField="DirectoUser" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
