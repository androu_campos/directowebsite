using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SignIn_test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            txtUser.Focus();
            if (Request.Cookies["userMecca"] != null && Request.Cookies["passMecca"] != null)
            {
                string UserCooky = Request.Cookies["userMecca"].Value;
                string PassCooky = Request.Cookies["passMecca"].Value;

                //cdrdbDatasetTableAdapters.loginAdp ADPl = new cdrdbDatasetTableAdapters.loginAdp();
                //cdrdbDataset.loginDataTable tbl = ADPl.GetDataByUserPass(UserCooky, PassCooky);
                cdrdbDatasetTableAdapters.loginAdp ADPl = new cdrdbDatasetTableAdapters.loginAdp();
                cdrdbDataset.loginDataTable tbl = ADPl.GetDataByUserPassCaseSens(UserCooky, PassCooky);
                
                
                
                if (tbl.Count > 0)//usuario logeado
                {
                    Session["Rol"] = tbl.Rows[0]["Rol"].ToString();                   
                    Session["DirectoUser"] = tbl.Rows[0]["username"].ToString();
                    Session["BrokerName"] = tbl.Rows[0]["BrokerName"].ToString();
                    Response.Redirect("Home.aspx");
                }
                else
                {
                    lblWrongLog.Visible = true;
                    txtUser.Focus();

                }

            }
            else
            {
                txtUser.Focus();

            }
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }
        
        
    }   
    
    protected void LoginButton_Click(object sender, EventArgs e)
    {
        try
        {
            string UserId;
            string Password;

            UserId = txtUser.Text;
            Password = txtPassword.Text;
            
            cdrdbDatasetTableAdapters.loginAdp ADPl = new cdrdbDatasetTableAdapters.loginAdp();
            cdrdbDataset.loginDataTable tbl = ADPl.GetDataByUserPassCaseSens(UserId, Password);


            if (tbl.Count > 0)//usuario logeado
            {
                Session["Rol"] = tbl.Rows[0]["Rol"].ToString();                
                Session["DirectoUser"] = tbl.Rows[0]["username"].ToString();
                Session["BrokerName"] = tbl.Rows[0]["BrokerName"].ToString();
                                               
		        if (RememberMe.Checked)//quiere recordar contraseņa
                {
                    Response.Cookies["userMecca"].Value = UserId;
                    Response.Cookies["userMecca"].Expires = DateTime.Now.AddDays(365);
                    
                    Response.Cookies["passMecca"].Value = Password;
                    Response.Cookies["passMecca"].Expires = DateTime.Now.AddDays(365);		            
                    
                    Response.Cookies["Rol"].Value = Session["Rol"].ToString();
                    Response.Cookies["Rol"].Expires = DateTime.Now.AddDays(365);

                    Response.Cookies["BrokerName"].Value = Session["BrokerName"].ToString();
                    Response.Cookies["Rol"].Expires = DateTime.Now.AddDays(365);

                    Session["RememberMe"] = "ON";

                    

                }

                if (Session["Rol"].ToString() == "Customer")
                {
                    Response.Redirect("Paccess.aspx", false);
                }
                else if (Session["Rol"].ToString() == "CustomerCDRS")
                {
                    Response.Redirect("CdrsDownloader.aspx", false);
                }
                else if (Session["Rol"].ToString() == "Vendor")
                {
                    Response.Redirect("VendorHome.aspx", false);
                }
                else
                {
                    Response.Redirect("Home.aspx", false);
                }

            }
            else
            {
                lblWrongLog.Visible = true;

            }
        }
        catch(Exception ex)
        {
            string message = ex.Message + " Source: " + ex.Source + " Trace:" + ex.StackTrace + " link:" + ex.HelpLink; 
        
        
        }
    }
}
