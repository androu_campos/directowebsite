using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Net;
using System.Data.SqlClient;

public partial class MECCAReports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = "";
        DataSet ds = null;

        string rol = Session["Rol"].ToString();
        string user = Session["DirectoUser"].ToString();

        if (user == "david" || user == "fcarbia" || user == "rodrigo.andrade" || user == "arturo.marquez")
        {
            cborol.Visible = true;
        }

        if (!Page.IsPostBack)
        {
            if (rol == "Noc" || rol == "Comercial")
            {
                createWSReports(ref sql, ref ds);
                string usr, pw;
                StringBuilder content = new StringBuilder();

                usr = "connectvip";
                pw = "B473cD0FzW9";
                pw.Normalize();

                //try
                //{
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://172.27.27.30:21/DailyReports");

                request.Credentials = new NetworkCredential(usr, pw);

                request.Method = WebRequestMethods.Ftp.ListDirectory;

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                Stream responseStream = response.GetResponseStream();

                StreamReader reader = new StreamReader(responseStream);

                while (!reader.EndOfStream)
                {
                    string fi = reader.ReadLine().ToString();

                    content.Append("<br><a href=ftp://172.27.27.30:21/DailyReports/" + fi + " target=" + "_blank" + ">" + fi + "</a>");
                }

                lblContent.Text = content.ToString();

                reader.Close();
                response.Close();

                //}
                //catch (WebException t)
                //{
                //    String status = ((FtpWebResponse)t.Response).StatusDescription;
                //    lblError.Text = status;
                //}
            }

            if (rol == "Noc" || rol == "CallCenter")
            {
                createCCReports(ref sql, ref ds);
            }
        }
    }

    private void createWSReports(ref string sql, ref DataSet ds)
    {
        panelWS.Visible = true;
        panelCC.Visible = false;

        //DIRECTO UNIQUE
        sql = "SELECT Source, Minutes, Profit, Max, DayBefore, LastWeek FROM DirectoReports.dbo.MailAverage";
        ds = Util.RunQueryByStmnt(sql);

        gvUniqueWS.DataSource = ds.Tables[0];
        gvUniqueWS.DataBind();


        //MEX-ROW LOSS
        sql = "SELECT CONVERT(VARCHAR,BillingDate,105) as BillingDate, Mexico, Row, TotalGeneral [Total General] FROM DirectoReports.dbo.MEXROW_LOSS";
        ds = Util.RunQueryByStmnt(sql);

        gvMRLoss.DataSource = ds.Tables[0];
        gvMRLoss.DataBind();


        //MEXICO LOSS VENDOR-LMC
        sql = "SELECT * FROM DirectoReports.dbo.VL_BillingDateLOSS";
        ds = Util.RunQueryByStmnt(sql);

        gvVL_BillingDateLOSS.DataSource = ds.Tables[0];
        gvVL_BillingDateLOSS.DataBind();

        //MEXICO LOSS LAST DAY
        sql = "SELECT * FROM DirectoReports.dbo.CV_Loss";
        ds = Util.RunQueryByStmnt(sql);

        gvCV_Loss.DataSource = ds.Tables[0];
        gvCV_Loss.DataBind();

        //ROW LOSS
        sql = "SELECT * FROM DirectoReports.dbo.RC_Loss";
        ds = Util.RunQueryByStmnt(sql);

        gvRC_Loss.DataSource = ds.Tables[0];
        gvRC_Loss.DataBind();

        //ROW LOSS LAST DAY 
        sql = "SELECT * FROM DirectoReports.dbo.CV_CLoss";
        ds = Util.RunQueryByStmnt(sql);

        gvCV_CLoss.DataSource = ds.Tables[0];
        gvCV_CLoss.DataBind();

        //TRAFFIC MIN
        sql = "SELECT Country, LMC, MinK as [-1], MinKD as [-2], MinKWD as [-7], AverageMin as MAX  FROM DirectoReports.dbo.FirstSemaphore ";
        ds = Util.RunQueryByStmnt(sql);

        gvTraffic.DataSource = ds.Tables[0];
        gvTraffic.DataBind();

        //TRAFFIC PROFIT
        sql = "SELECT Country, LMC, Profit as [-1], ProfitD as [-2], ProfitWD as [-7], AverageProf as MAX FROM DirectoReports.dbo.FirstSemaphore ";
        ds = Util.RunQueryByStmnt(sql);

        gvProfit.DataSource = ds.Tables[0];
        gvProfit.DataBind();

        //CUSTOMER TELCEL
        sql = "SELECT Customer, MinK as [-1], PVDayK as [-2], PVWeek [-7], Average as MAX, MinDF [DF], ACD, ASR, ABR, PDD, Profit, Margin FROM DirectoReports.dbo.DailyCustTel";
        ds = Util.RunQueryByStmnt(sql);

        gvDailyCustTel.DataSource = ds.Tables[0];
        gvDailyCustTel.DataBind();

        //Vendor TELCEL
        sql = "SELECT Vendor, MinK as [-1], [PVDayK] as [-2], PVWeekK [-7], ACD, ASR, ABR, PDD, Profit, Margin FROM DirectoReports.dbo.DailyVendorTEL";
        ds = Util.RunQueryByStmnt(sql);

        gvDailyVendorTel.DataSource = ds.Tables[0];
        gvDailyVendorTel.DataBind();

        //CUSTOMER TELMEX
        sql = "SELECT Customer, MinK as [-1], PVDayK as [-2], PVWeek [-7], Average as MAX, ACD, ASR, ABR, PDD, Profit, Margin FROM DirectoReports.dbo.DailyCustTELMEX";
        ds = Util.RunQueryByStmnt(sql);

        gvDailyCustTelmex.DataSource = ds.Tables[0];
        gvDailyCustTelmex.DataBind();

        //Vendor TELMEX
        sql = "SELECT Vendor, MinK as [-1], [PVDayK] as [-2], PVWeekK [-7], ACD, ASR, ABR, PDD, Profit, Margin FROM DirectoReports.dbo.DailyVendorTELMEX";
        ds = Util.RunQueryByStmnt(sql);

        gvDailyVendorTelmex.DataSource = ds.Tables[0];
        gvDailyVendorTelmex.DataBind();

        //CUSTOMER IUSA
        sql = "SELECT Customer, MinK as [-1], PVDayK as [-2], PVWeek [-7], Average as MAX, ACD, ASR, ABR, PDD, Profit, Margin FROM DirectoReports.dbo.DailyCustIUSA";
        ds = Util.RunQueryByStmnt(sql);

        gvDailyCustIUSA.DataSource = ds.Tables[0];
        gvDailyCustIUSA.DataBind();

        //Vendor IUSA
        sql = "SELECT Vendor, MinK as [-1], [PVDayK] as [-2], PVWeekK [-7], ACD, ASR, ABR, PDD, Profit, Margin FROM DirectoReports.dbo.DailyVendorIUSA";
        ds = Util.RunQueryByStmnt(sql);

        gvDailyVendorIUSA.DataSource = ds.Tables[0];
        gvDailyVendorIUSA.DataBind();

        //CUSTOMER IUSA
        sql = "SELECT Customer, MinK as [-1], PVDayK as [-2], PVWeek [-7], Average as MAX, ACD, ASR, ABR, PDD, Profit, Margin FROM DirectoReports.dbo.DailyCustMOVI";
        ds = Util.RunQueryByStmnt(sql);

        gvDailyCustMOVI.DataSource = ds.Tables[0];
        gvDailyCustMOVI.DataBind();

        //Vendor IUSA
        sql = "SELECT Vendor, MinK as [-1], [PVDayK] as [-2], PVWeekK [-7], ACD, ASR, ABR, PDD, Profit, Margin FROM DirectoReports.dbo.DailyVendorMOVI";
        ds = Util.RunQueryByStmnt(sql);

        gvDailyVendorMOVI.DataSource = ds.Tables[0];
        gvDailyVendorMOVI.DataBind();

        //ROW
        sql = "SELECT Country, MinK as [-1], PVDayK as [-2], PVWeek [-7], ACD, PDD, Profit, Average as MAX, Margin FROM DirectoReports.dbo.DailyCountry";
        ds = Util.RunQueryByStmnt(sql);

        gvDailyCountry.DataSource = ds.Tables[0];
        gvDailyCountry.DataBind();
    }

    private void createCCReports(ref string sql, ref DataSet ds)
    {
        panelWS.Visible = true;
        panelCC.Visible = false;

        sql = "SELECT Marca, Minutes, Profit, Max, LastWeek FROM DirectoReports.dbo.MailAverageCC ";
        ds = Util.RunQueryByStmnt(sql);

        gvUniqueCC.DataSource = ds.Tables[0];
        gvUniqueCC.DataBind();

        //TRAFFIC Sales
        sql = "select Customer, Mink as [-1], PVDayK as [-2], PV2DayK as [-7], Average as Max, Profit, Margin FROM [DirectoReports]..DailyCCSem  ";
        ds = Util.RunQueryByStmnt(sql);

        gvProfitCC.DataSource = ds.Tables[0];
        gvProfitCC.DataBind();

        //TRAFFIC Min
        sql = "SELECT Customer, Mink as [-1], PVDayK as [-2], PVWeek as [-7], Average as Max, Profit, Margin FROM [DirectoReports]..DailyCCMinSem ";
        ds = Util.RunQueryByStmnt(sql);

        gvTrafficCC.DataSource = ds.Tables[0];
        gvTrafficCC.DataBind();

        //TRAFFIC LMC
        sql = "SELECT LMC, Mink as [-1], MinKD as [-2], MinKWD as [-7], Average as Max, Profit, Margin FROM [DirectoReports]..FirstSemaphoreCC ";
        ds = Util.RunQueryByStmnt(sql);

        gvLMC.DataSource = ds.Tables[0];
        gvLMC.DataBind();

        //TELCEL CLIENT
        sql = "SELECT Customer, MinK as [-1], PVDayK as [-2], PVWeek as [-7], Average as Max, MinDF as DF, ACD, PDD, ASR, Profit, Margin FROM [DirectoReports]..DailyCustTelCC ";
        ds = Util.RunQueryByStmnt(sql);

        gvCTelcelCC.DataSource = ds.Tables[0];
        gvCTelcelCC.DataBind();

        //TELCEL Vendor
        sql = "SELECT Vendor, Mink as [-1], PVDayK as [-2], PVWeekK as [-7], ASR, ACD, PDD, Profit, Margin FROM [DirectoReports]..DailyVendorTelCC ";
        ds = Util.RunQueryByStmnt(sql);

        gvVTelcelCC.DataSource = ds.Tables[0];
        gvVTelcelCC.DataBind();

        //MOVISTAR CLIENT
        sql = "SELECT Customer, Mink as [-1], PVDayK as [-2], PVWeek as [-7], Average as Max, ACD, PDD, ASR, Profit, Margin FROM [DirectoReports]..DailyCustMOVICC ";
        ds = Util.RunQueryByStmnt(sql);

        gvCMoviCC.DataSource = ds.Tables[0];
        gvCMoviCC.DataBind();

        //MOVISTAR Vendor
        sql = "SELECT Vendor, Mink as [-1], PVDayK as [-2], PVWeekK as [-7], ASR, ACD, PDD, Profit, Margin FROM [DirectoReports]..DailyVendorMOVICC ";
        ds = Util.RunQueryByStmnt(sql);

        gvVMoviCC.DataSource = ds.Tables[0];
        gvVMoviCC.DataBind();

        //IUSA CLIENT
        sql = "SELECT Customer, Mink as [-1], PVDayK as [-2], PVWeek as [-7], Average as Max, ACD, PDD, ASR, Profit, Margin FROM [DirectoReports]..DailyCustIUSACC ";
        ds = Util.RunQueryByStmnt(sql);

        gvCIusaCC.DataSource = ds.Tables[0];
        gvCIusaCC.DataBind();

        //IUSA Vendor
        sql = "SELECT Vendor, Mink as [-1], PVDayK as [-2], PVWeekK as [-7], ASR, ACD, PDD, Profit, Margin FROM [DirectoReports]..DailyVendorIUSACC ";
        ds = Util.RunQueryByStmnt(sql);

        gvVIusaCC.DataSource = ds.Tables[0];
        gvVIusaCC.DataBind();

        //TELMEX CLIENT
        sql = "SELECT Customer, Mink as [-1], PVDayK as [-2], PVWeek as [-7], Average as Max, ACD, PDD, ASR, Profit, Margin FROM [DirectoReports]..DailyCustTelmexCC ";
        ds = Util.RunQueryByStmnt(sql);

        gvCTelmexCC.DataSource = ds.Tables[0];
        gvCTelmexCC.DataBind();

        //TELMEX Vendor
        sql = "SELECT Vendor, Mink as [-1], PVDayK as [-2], PVWeekK as [-7], ASR, ACD, PDD, Profit, Margin FROM [DirectoReports]..DailyVendorTelmexCC ";
        ds = Util.RunQueryByStmnt(sql);

        gvVTelmexCC.DataSource = ds.Tables[0];
        gvVTelmexCC.DataBind();
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["DirectoReportsConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

    protected void rol_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cborol.SelectedValue == "1")
        {
            panelWS.Visible = true;
            linkLoss.Visible = true;
            linkSem.Visible = true;
            panelCC.Visible = false;
            linkSemCC.Visible = false;
        }
        else if (cborol.SelectedValue == "2")
        {
            panelWS.Visible = false;
            linkLoss.Visible = false;
            linkSem.Visible = false;
            panelCC.Visible = true;
            linkSemCC.Visible = true;
        }
    }

    protected void linkLoss_Click(object sender, EventArgs e)
    {
        if (panelLoss.Visible == false)
        {
            panelLoss.Visible = true;
        }
        else
        {
            panelLoss.Visible = false;
        }
    }

    protected void linkSem_Click(object sender, EventArgs e)
    {
        linkSem.Visible = true;
        linkSemCC.Visible = false;

        if (panelSem.Visible == false)
        {
            panelSem.Visible = true;
            panelSemCC.Visible = false;
        }
        else
        {
            panelSem.Visible = false;
            panelSemCC.Visible = false;
        }
    }
    protected void linkSemcc_Click(object sender, EventArgs e)
    {
        linkSem.Visible = false;
        linkSemCC.Visible = true;

        if (panelSemCC.Visible == false)
        {
            panelSemCC.Visible = true;
            panelSem.Visible = false;
        }
        else
        {
            panelSemCC.Visible = false;
            panelSem.Visible = false;
        }
    }
}
