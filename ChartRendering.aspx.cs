using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using Dundas.Charting.WebControl;
using RKLib.ExportData;

public partial class ChartRendering : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Chart1.Visible = false;
            btn_excel.Visible = false;

            txtFrom.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();

            txtFrom2.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            txtTo2.Text = DateTime.Now.AddDays(-1).ToShortDateString();

            TabContainer1.ActiveTabIndex = 0;
            this.UpdatePanel1.Controls[0].Controls[5].Visible = false;
            Session["idFrame"] = 0;
            //CUSTOMER
            SqlConnection SqlConn1 = new SqlConnection();
            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
            SqlCommand SqlCommand1 = new SqlCommand("SELECT DISTINCT ProviderName from ProviderIP WHERE Type like 'C' UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT '** NONE **' from ProviderIP as ProviderName", SqlConn1);
            SqlConn1.Open();
            SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
            while (myReader1.Read())
            {
                dpdCustomer.Items.Add(myReader1.GetValue(0).ToString());
            }
            myReader1.Close();
            SqlConn1.Close();
            //VENDOR

            SqlCommand SqlCommand = new SqlCommand("SELECT DISTINCT ProviderName from ProviderIP WHERE Type like 'V' UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT '** NONE **' from ProviderIP as ProviderName ", SqlConn1);
            SqlConn1.Open();
            SqlDataReader myReader = SqlCommand.ExecuteReader();
            while (myReader.Read())
            {
                dpdVendor.Items.Add(myReader.GetValue(0).ToString());
            }
            myReader.Close();
            SqlConn1.Close();
            //COUNTRY
            SqlConnection SqlConn = new SqlConnection();
            SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";

            SqlCommand SqlCommand4 = new SqlCommand("select distinct Country from Regions union  select '** NONE **' as Country from Regions union select '**ALL**' as Country from Regions order by Country ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader4 = SqlCommand4.ExecuteReader();
            while (myReader4.Read())
            {
                dpdCountry.Items.Add(myReader4.GetValue(0).ToString());
            }
            myReader4.Close();
            SqlConn.Close();
            //REGION
            SqlCommand SqlCommand5 = new SqlCommand("select distinct Region from Regions union select '** NONE **' as Region from Regions union select '**ALL**' as Region from Regions order by Region ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader5 = SqlCommand5.ExecuteReader();
            while (myReader5.Read())
            {
                dpdRegion.Items.Add(myReader5.GetValue(0).ToString());
            }
            myReader5.Close();
            SqlConn.Close();
            //TYPE
            SqlCommand SqlCommand6 = new SqlCommand("select distinct Regions.Type from Regions union select '** NONE **' as Type from Regions union select '**ALL**' as Type from Regions order by Type ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader6 = SqlCommand6.ExecuteReader();
            while (myReader6.Read())
            {
                dpdType.Items.Add(myReader6.GetValue(0).ToString());
            }
            myReader6.Close();
            SqlConn.Close();
            //CLASS
            SqlCommand SqlCommand7 = new SqlCommand("SELECT DISTINCT Class FROM Regions WHERE Class is not NULL UNION SELECT '** NONE **' AS Class FROM Regions UNION SELECT '**ALL**' AS Class FROM Regions ORDER BY Class", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader7 = SqlCommand7.ExecuteReader();
            while (myReader7.Read())
            {
                dpdClass.Items.Add(myReader7.GetValue(0).ToString());
            }
            myReader7.Close();
            SqlConn.Close();
        }
        else
        {
            this.UpdatePanel1.Controls[0].Controls[5].Visible = true;
        }


    }
  
    protected void cmdTest_Click(object sender, EventArgs e)
    {

           TabContainer1.ActiveTabIndex = 0;
        
        string queryy = string.Empty;
        string queryyExcel = string.Empty;
        string select = string.Empty;
        string where_ = string.Empty;
        string groupBy = string.Empty;
        StringBuilder tITLE = new StringBuilder();
        StringBuilder query = new StringBuilder();
        query.Append(" ");
        StringBuilder where = new StringBuilder();
        where.Append(" ");
        int counter = 0;

        if (dpdCustomer.SelectedValue.ToString() != "** NONE **")   //Customer
        {
            query.Append(" Customer,");
            if (dpdCustomer.SelectedValue.ToString() != "**ALL**") where.Append(" AND Customer like'" + dpdCustomer.SelectedValue.ToString() + "'");
            tITLE.Append("Customer:" + dpdCustomer.SelectedValue.ToString()+",");
            counter += 1;
        }
        if (dpdVendor.SelectedValue.ToString() != "** NONE **")   //Vendor
        {
            query.Append(" Vendor,");
            if (dpdVendor.SelectedValue.ToString() != "**ALL**")
            {
                if (dpdVendor.SelectedValue.ToString() == "OUTLANDER")
                {
                    where.Append(" AND (Vendor like 'OUTLANDER%' OR Vendor = 'STAR-CITY-SCT1') ");
                }
                else
                {
                    where.Append(" AND Vendor like '" + dpdVendor.SelectedValue.ToString() + "'");
                }
                tITLE.Append("Vendor:" + dpdVendor.SelectedValue.ToString() + ",");
            }
            else
            {
                tITLE.Append("Vendor:" + dpdVendor.SelectedValue.ToString() + ",");
            }
            counter += 1;
            
        }
        if (dpdCountry.SelectedValue.ToString() != "** NONE **")    //Country
        {
            query.Append(" Country,");
            if (dpdCountry.SelectedValue.ToString() != "**ALL**") where.Append(" AND Country like '" + dpdCountry.SelectedValue.ToString() + "'");
            tITLE.Append("Country:" + dpdCountry.SelectedValue.ToString() + ",");
            counter += 1;
        }
        if (dpdRegion.SelectedValue.ToString() != "** NONE **")    //Region
        {
            query.Append(" Region,");
            if (dpdRegion.SelectedValue.ToString() != "**ALL**") where.Append(" AND Region like '" + dpdRegion.SelectedValue.ToString() + "'");
            tITLE.Append("Region:" + dpdRegion.SelectedValue.ToString() + ",");
            counter += 1;
        }
        if (dpdType.SelectedValue.ToString() != "** NONE **")    //Type
        {
            query.Append(" Type,");

            if (dpdType.SelectedValue.ToString() != "**ALL**")
            {
                if (dpdOrType.SelectedValue.ToString() == "2")
                {
                    where.Append(" AND Type like '" + dpdType.SelectedValue.ToString() + "'");
                    tITLE.Append("Type:" + dpdType.SelectedValue.ToString() + ",");
                }
                else
                {
                    where.Append(" AND Type not like '" + dpdType.SelectedValue.ToString() + "'");
                    tITLE.Append("Type not like:" + dpdType.SelectedValue.ToString() + ",");
                }
            }
            else
            {
                tITLE.Append("Type like: **ALL**,");
            }
            counter += 1;
            
        }
        if (dpdClass.SelectedValue.ToString() != "** NONE **")   //Class
        {
            query.Append(" Class,");

            if (dpdClass.SelectedValue.ToString() != "**ALL**")
            {
                if (dpdOrClass.SelectedValue.ToString() == "2")
                {
                    where.Append(" AND Class like '" + dpdClass.SelectedValue.ToString() + "'");
                    tITLE.Append("Class:" + dpdClass.SelectedValue.ToString() + ",");
                }
                else
                {
                    where.Append(" AND Class not like '" + dpdClass.SelectedValue.ToString() + "'");
                    tITLE.Append("Class not like:" + dpdClass.SelectedValue.ToString() + ",");
                }
            }
            else
            {
                tITLE.Append("Class like: **ALL**,");
            }
            counter += 1;

        }

        //Build statement
        where_ = where.ToString();        

        select = "Select [Time],sum(Calls) as Calls, " + query;
        select = select.Substring(0, select.Length);
        groupBy = query.ToString();
        groupBy = groupBy.Substring(0, groupBy.Length - 1);
        queryy = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom.Text + " 00:00:00' and [Time] <= '" + txtTo.Text + " 23:59:59' and Calls > 0 " + where_ + " group by Time ORDER BY Time";

        if (groupBy.Length > 3)
        {
            queryyExcel = "Select [Time],sum(Calls) as Calls, " + groupBy + " from TrafficLogLive Where [Time] >= '" + txtFrom.Text + " 00:00:00' and [Time] <= '" + txtTo.Text + " 23:59:59' and Calls > 0 " + where_ + " group by Time," + groupBy + " ORDER BY Time";
        }
        else
        {
            queryyExcel = queryy;
        }
        Session["qe"] = queryyExcel.ToString();
        
//        DataTable mytbl = Util.RunQueryByStmnt(queryy).Tables[0];
        
        
        //if (mytbl.Rows.Count > 0)
        //{
        //    makeChart(mytbl, 1, tITLE.ToString(),0);        //make Chart
        //    lblGraphs.Visible = false;
        //    btn_excel.Visible = true;
        //}
        //else
        //{
        //    this.Chart1.Visible = false;
        //    lblGraphs.Visible = true;
        //    lblGraphs.Text = "Nothing found !!";
        //}

        //make Puertos Serie
        //if (counter == 1 && dpdCustomer.SelectedIndex != 0)//Customer
        //{
        //    tblPorts = Util.RunQueryByStmntatCDRDB("SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + dpdCustomer.SelectedValue.ToString() + "' and [Type] like 'C%') and [Time] between '" + txtFrom.Text + " 00:00:00.000' and '" + txtTo.Text + " 23:59:59.000' group by [Time] order by [Time]").Tables[0];
        //    makeChart(tblPorts, 2, "Ports",0);
        //}
        //else if (counter == 1 && dpdVendor.SelectedIndex != 0)//Vendor
        //{
        //    tblPorts = Util.RunQueryByStmntatCDRDB("SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'V%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + dpdVendor.SelectedValue.ToString() + "' and [Type] like 'V%') and [Time] between '" + txtFrom.Text + " 00:00:00.000' and '" + txtTo.Text + " 23:59:59.000' group by [Time] order by [Time]").Tables[0];
        //    makeChart(tblPorts, 2, "Ports",0);
        //}

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        SqlConnection myConnection = new SqlConnection("Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg;");


//        SqlCommand myCommand = new SqlCommand("Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '12/05/2007 00:00:00' and [Time] <= '12/10/2007 23:59:59' and Calls > 0   AND Vendor like 'NATIONAL' group by Time ORDER BY Time", myConnection);
        SqlCommand myCommand = new SqlCommand(queryy, myConnection);
        myCommand.Connection.Open();

        SqlDataReader myReader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
        
        Chart2.Series["Calls"].Points.DataBind(
            myReader,
            "Time",
            "Calls",
            "Tooltip=Calls");

        myReader.Close();
        myConnection.Close();
//        this.Chart1.CallbackManager.UpdateClientControl(this.Chart1);
    }

    protected void cmdGraph_Click(object sender, EventArgs e)
    {
        TabContainer1.ActiveTabIndex = 0;
        DataTable tblPorts;
        string queryy = string.Empty;
        string queryyExcel = string.Empty;
        string select = string.Empty;
        string where_ = string.Empty;
        string groupBy = string.Empty;
        StringBuilder tITLE = new StringBuilder();
        StringBuilder query = new StringBuilder();
        query.Append(" ");
        StringBuilder where = new StringBuilder();
        where.Append(" ");
        int counter = 0;

        if (dpdCustomer.SelectedValue.ToString() != "** NONE **")   //Customer
        {
            query.Append(" Customer,");
            if (dpdCustomer.SelectedValue.ToString() != "**ALL**") where.Append(" AND Customer like'" + dpdCustomer.SelectedValue.ToString() + "'");
            tITLE.Append("Customer:" + dpdCustomer.SelectedValue.ToString() + ",");
            counter += 1;
        }
        if (dpdVendor.SelectedValue.ToString() != "** NONE **")   //Vendor
        {
            query.Append(" Vendor,");
            if (dpdVendor.SelectedValue.ToString() != "**ALL**")
            {
                if (dpdVendor.SelectedValue.ToString() == "OUTLANDER")
                {
                    where.Append(" AND (Vendor like 'OUTLANDER%' OR Vendor = 'STAR-CITY-SCT1') ");
                }
                else
                {
                    where.Append(" AND Vendor like '" + dpdVendor.SelectedValue.ToString() + "'");
                }
                tITLE.Append("Vendor:" + dpdVendor.SelectedValue.ToString() + ",");
            }
            else
            {
                tITLE.Append("Vendor:" + dpdVendor.SelectedValue.ToString() + ",");
            }
            counter += 1;

        }
        if (dpdCountry.SelectedValue.ToString() != "** NONE **")    //Country
        {
            query.Append(" Country,");
            if (dpdCountry.SelectedValue.ToString() != "**ALL**") where.Append(" AND Country like '" + dpdCountry.SelectedValue.ToString() + "'");
            tITLE.Append("Country:" + dpdCountry.SelectedValue.ToString() + ",");
            counter += 1;
        }
        if (dpdRegion.SelectedValue.ToString() != "** NONE **")    //Region
        {
            query.Append(" Region,");
            if (dpdRegion.SelectedValue.ToString() != "**ALL**") where.Append(" AND Region like '" + dpdRegion.SelectedValue.ToString() + "'");
            tITLE.Append("Region:" + dpdRegion.SelectedValue.ToString() + ",");
            counter += 1;
        }
        if (dpdType.SelectedValue.ToString() != "** NONE **")    //Type
        {
            query.Append(" Type,");

            if (dpdType.SelectedValue.ToString() != "**ALL**")
            {
                if (dpdOrType.SelectedValue.ToString() == "2")
                {
                    where.Append(" AND Type like '" + dpdType.SelectedValue.ToString() + "'");
                    tITLE.Append("Type:" + dpdType.SelectedValue.ToString() + ",");
                }
                else
                {
                    where.Append(" AND Type not like '" + dpdType.SelectedValue.ToString() + "'");
                    tITLE.Append("Type not like:" + dpdType.SelectedValue.ToString() + ",");
                }
            }
            else
            {
                tITLE.Append("Type like: **ALL**,");
            }
            counter += 1;

        }
        if (dpdClass.SelectedValue.ToString() != "** NONE **")   //Class
        {
            query.Append(" Class,");

            if (dpdClass.SelectedValue.ToString() != "**ALL**")
            {
                if (dpdOrClass.SelectedValue.ToString() == "2")
                {
                    where.Append(" AND Class like '" + dpdClass.SelectedValue.ToString() + "'");
                    tITLE.Append("Class:" + dpdClass.SelectedValue.ToString() + ",");
                }
                else
                {
                    where.Append(" AND Class not like '" + dpdClass.SelectedValue.ToString() + "'");
                    tITLE.Append("Class not like:" + dpdClass.SelectedValue.ToString() + ",");
                }
            }
            else
            {
                tITLE.Append("Class like: **ALL**,");
            }
            counter += 1;

        }

        //Build statement
        where_ = where.ToString();

        select = "Select [Time],sum(Calls) as Calls, " + query;
        select = select.Substring(0, select.Length);
        groupBy = query.ToString();
        groupBy = groupBy.Substring(0, groupBy.Length - 1);
        queryy = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom.Text + " 00:00:00' and [Time] <= '" + txtTo.Text + " 23:59:59' and Calls > 0 " + where_ + " group by Time ORDER BY Time";

        if (groupBy.Length > 3)
        {
            queryyExcel = "Select [Time],sum(Calls) as Calls, " + groupBy + " from TrafficLogLive Where [Time] >= '" + txtFrom.Text + " 00:00:00' and [Time] <= '" + txtTo.Text + " 23:59:59' and Calls > 0 " + where_ + " group by Time," + groupBy + " ORDER BY Time";
        }
        else
        {
            queryyExcel = queryy;
        }
        Session["qe"] = queryyExcel.ToString();

        DataTable mytbl = Util.RunQueryByStmnt(queryy).Tables[0];


        if (mytbl.Rows.Count > 0)
        {
            makeChart(mytbl, 1, tITLE.ToString(), 0);        //make Chart
            lblGraphs.Visible = false;
            btn_excel.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            lblGraphs.Visible = true;
            lblGraphs.Text = "Nothing found !!";
        }

        //make Puertos Serie
        if (counter == 1 && dpdCustomer.SelectedIndex != 0)//Customer
        {
            tblPorts = Util.RunQueryByStmntatCDRDB("SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + dpdCustomer.SelectedValue.ToString() + "' and [Type] like 'C%') and [Time] between '" + txtFrom.Text + " 00:00:00.000' and '" + txtTo.Text + " 23:59:59.000' group by [Time] order by [Time]").Tables[0];
            makeChart(tblPorts, 2, "Ports", 0);
        }
        else if (counter == 1 && dpdVendor.SelectedIndex != 0)//Vendor
        {
            tblPorts = Util.RunQueryByStmntatCDRDB("SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'V%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + dpdVendor.SelectedValue.ToString() + "' and [Type] like 'V%') and [Time] between '" + txtFrom.Text + " 00:00:00.000' and '" + txtTo.Text + " 23:59:59.000' group by [Time] order by [Time]").Tables[0];
            makeChart(tblPorts, 2, "Ports", 0);
        }


    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        TabContainer1.ActiveTabIndex = 1;
        int indx = Convert.ToInt32(RadioButtonList1.SelectedValue);
        string query = string.Empty;
        DataTable tbl = new DataTable();

        if (indx == 0)
        {
            query = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom2.Text + " 00:00:00' and [Time] <= '" + txtTo2.Text + " 23:59:59' and Calls > 0  AND Country like 'MEXICO' AND Type like 'CPP' group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmnt(query).Tables[0];
            makeChart(tbl, 1, "Mexico CPP", 1);
        }
        else if (indx == 1)
        {
            query = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom2.Text + " 00:00:00' and [Time] <= '" + txtTo2.Text + " 23:59:59' and Calls > 0  AND Country = 'MEXICO' AND Type <> 'CPP' and Class like 'OFF-NET' group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmnt(query).Tables[0];
            makeChart(tbl, 1, "Mexico NEA ", 1);
        }
        else if (indx == 2)
        {
            query = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom2.Text + " 00:00:00' and [Time] <= '" + txtTo2.Text + " 23:59:59' and Calls > 0   AND Country = 'MEXICO' AND Type <> 'CPP' and Class like 'ON-NET' group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmnt(query).Tables[0];
            makeChart(tbl, 1, "Mexico On Net", 1);
        }
        else if (indx == 3)
        {
            query = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom2.Text + " 00:00:00' and [Time] <= '" + txtTo2.Text + " 23:59:59' and Vendor = 'GLOBE-TELECOM' and Country = 'PHILIPPINES' and Region like 'GLOBE MOBILE' group by [Time] ORDER BY Time";
            tbl = Util.RunQueryByStmnt(query).Tables[0];
            makeChart(tbl, 1, "Philippines Globe", 1);
        }
        else if (indx == 4)
        {
            query = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom2.Text + " 00:00:00' and [Time] <= '" + txtTo2.Text + " 23:59:59' and Vendor = 'SMART-COMMUNICATIONS' and Country = 'PHILIPPINES' and Region like 'SMART MOBILE' group by [Time] ORDER BY Time";
            tbl = Util.RunQueryByStmnt(query).Tables[0];
            makeChart(tbl, 1, "Philippines Smart", 1);
        }
        else if (indx == 5)
        {
            query = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom2.Text + " 00:00:00' and [Time] <= '" + txtTo2.Text + " 23:59:59' and Vendor = 'PLDT' and Country = 'PHILIPPINES' and Region like 'ON NET (PLDT)' group by [Time] ORDER BY Time";
            tbl = Util.RunQueryByStmnt(query).Tables[0];
            makeChart(tbl, 1, "Philippines PLDT", 1);
        }
        else if (indx == 6)
        {
            query = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom2.Text + " 00:00:00' and [Time] <= '" + txtTo2.Text + " 23:59:59' and Type = 'FIXED' and Country = 'BRAZIL' group by [Time] ORDER BY Time";
            tbl = Util.RunQueryByStmnt(query).Tables[0];
            makeChart(tbl, 1, "Brazil Fijo", 1);
        }
        else if (indx == 7)
        {
            query = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom2.Text + " 00:00:00' and [Time] <= '" + txtTo2.Text + " 23:59:59' and Type = 'MOBILE' and Country = 'BRAZIL' group by [Time] ORDER BY Time";
            tbl = Util.RunQueryByStmnt(query).Tables[0];
            makeChart(tbl, 1, "Brazil Mobile", 1);
        }
        else if (indx == 8)
        {
            query = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom2.Text + " 00:00:00' and [Time] <= '" + txtTo2.Text + " 23:59:59' and Country = 'PERU' and Region like 'ROC' group by [Time] ORDER BY Time";
            tbl = Util.RunQueryByStmnt(query).Tables[0];
            makeChart(tbl, 1, "Peru ROC", 1);
        }

    }


    protected void btn_excel_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable mytblex = Util.RunQueryByStmnt(Session["qe"].ToString()).Tables[0];
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "LiveCallsGraph" + ".xls";
            objExport.ExportDetails(mytblex, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }
    }

    protected void cmdReset_Click(object sender, EventArgs e)
    {
        dpdCustomer.ClearSelection();
        dpdVendor.ClearSelection();
        dpdCountry.ClearSelection();
        dpdRegion.ClearSelection();
        dpdType.ClearSelection();
        dpdOrType.ClearSelection();
        dpdClass.ClearSelection();
        dpdOrClass.ClearSelection();
        txtFrom.Text = DateTime.Now.AddDays(-1).ToShortDateString();
        txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();
        RadioButtonList1.ClearSelection();
        txtFrom2.Text = DateTime.Now.AddDays(-1).ToShortDateString();
        txtTo2.Text = DateTime.Now.AddDays(-1).ToShortDateString();

    }

    private void makeChart(DataTable tbl, int i, string tITLE, int tab)
    {
        int pdx;
        string script = string.Empty;
        if (i == 1)//Calls
        {
            DateTime myStartDate = DateTime.Parse("December 12,2007");

            this.Chart1.ImageType = Dundas.Charting.WebControl.ChartImageType.Png;

            //Zoom
            //            this.Chart1.ChartAreas[0].CursorX.UserEnabled = true;
            //            this.Chart1.ChartAreas[0].CursorY.UserEnabled = true;

            //            this.Chart1.ChartAreas[0].AxisX.View.Zoom(myStartDate.ToOADate(), 90, DateTimeIntervalType.Auto, true);


            foreach (DataRow myRow in tbl.Rows)
            {
                pdx = this.Chart1.Series["Calls"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Calls"]));
                this.Chart1.Series["Calls"].Points[pdx].ToolTip = String.Format("Time:{0},Calls:{1}", myRow["Time"], myRow["Calls"]);
                script = "onmouseover=dispData('" + myRow["Time"].ToString() + "','" + myRow["Calls"] + "')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Calls"].Points[pdx].MapAreaAttributes = script;
            }

            DataPoint maxpoint = this.Chart1.Series["Calls"].Points.FindMaxValue();
            lblValue.Text = maxpoint.ToolTip.ToString();

            string origen = string.Empty;
            string to = string.Empty;
            //set the Title

            this.Chart1.Titles.Add(tITLE);
            //draw the MaxLine            
            DataPoint maxpointY = this.Chart1.Series["Calls"].Points.FindMaxValue("Y");

            if (tab == 0)
            {
                origen = txtFrom.Text + " 00:00:00 AM";
                to = txtTo.Text + " 23:59:59 PM";
            }
            else
            {
                origen = txtFrom2.Text + " 00:00:00 AM";
                to = txtTo2.Text + " 23:59:59 PM";
            }
            this.Chart1.Series["Max"].Points.AddXY(Convert.ToDateTime(to), Convert.ToDouble(maxpointY.YValues[0]));
            this.Chart1.Series["Max"].Points.AddXY(Convert.ToDateTime(origen), Convert.ToDouble(maxpointY.YValues[0]));

        }

        else if (i == 2)//Ports
        {
            string origen = string.Empty;
            string to = string.Empty;
            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                Chart1.Series["Ports"].Points[pdx].ToolTip = String.Format("Time:{0},Ports:{1}", myRow["Time"], myRow["Live_Calls"]);
                script = "onmouseover=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;
            }
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            if (tab == 0)
            {
                origen = txtFrom.Text + " 00:00:00 AM";
                to = txtTo.Text + " 23:59:59 PM";
            }
            else
            {
                origen = txtFrom2.Text + " 00:00:00 AM";
                to = txtTo2.Text + " 23:59:59 PM";
            }
            this.Chart1.Series["MaxPorts"].Points.AddXY(DateTime.Now.Date.AddDays(-1), Convert.ToDouble(maxpointY.YValues[0]));
            this.Chart1.Series["MaxPorts"].Points.AddXY(Convert.ToDateTime(origen), Convert.ToDouble(maxpointY.YValues[0]));
        }

    }
    protected void UpdatePanel1_PreRender(object sender, EventArgs e)
    {
        lblGraphs.Text = "YA est� !";
    }
}
