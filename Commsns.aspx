<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Commsns.aspx.cs" Inherits="Commsns" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<%@ OutputCache Duration="21600" Location="Client" VaryByParam="None" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:ScriptManager runat="server" id="myScript">
    </asp:ScriptManager> 
    
    <table width="100%">
        <tr>
            <td align="left" style="height: 29px" valign="top" width="10%">
                <asp:Label ID="Label4" runat="server" CssClass="labelTurk" Text="Commissions"></asp:Label></td>
            <td style="height: 29px; width: 25%;">
            </td>
        </tr>
        <tr>
            <td width="10%" align="right">
                </td>
            <td style="width: 25%">
                </td>
        </tr>
        <tr>
            <td align="left" colspan="2" valign="bottom">
                <table>
                    <tr>
                        <td align="left" style="width: 100px">
                            <table style="width: 267px; height: 75px">
                                <tr>
                                    <td style="width: 100px">
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Select Country Manager:" Width="120px"></asp:Label></td>
                                    <td colspan="2" style="width: 155px">
                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="dropdown" Width="97px">
                </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="From:"></asp:Label></td>
                                    <td colspan="2" style="width: 155px">
                <asp:TextBox ID="TextBox1" runat="server" CssClass="labelSteelBlue" Width="90px"></asp:TextBox>
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="To:"></asp:Label></td>
                                    <td colspan="2" style="width: 155px">
                <asp:TextBox ID="TextBox2" runat="server" CssClass="labelSteelBlue" Width="90px"></asp:TextBox>
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                    </td>
                                    <td colspan="2" style="width: 155px">
                                        <asp:Button ID="cmdReport" runat="server" CssClass="boton" OnClick="cmdReport_Click"
                                            Text="View report" /></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                <asp:UpdateProgress id="UpdateProgress2" runat="server">
                    <progresstemplate>
<TABLE><TBODY><TR><TD style="WIDTH: 100px"><asp:Label id="Label6" runat="server" Text="Loading..please wait" Width="100px" CssClass="labelBlue8" __designer:wfdid="w42"></asp:Label></TD><TD style="WIDTH: 100px"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/Loading.gif" __designer:wfdid="w43"></asp:Image></TD></TR></TBODY></TABLE>
</progresstemplate>
                </asp:UpdateProgress></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" valign="bottom" colspan="2" rowspan="1">
                </td>
        </tr>
        <tr>
            <td style="height: 21px" width="10%">
            </td>
            <td colspan="1" style="height: 21px" width="20%">
        </td>
        </tr>
        <tr>
            <td style="height: 21px" width="10%">
            </td>
            <td colspan="1" style="height: 21px" width="20%">
                <asp:UpdatePanel id="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <contenttemplate>
<asp:Button id="cmdExport" onclick="cmdExport_Click" runat="server" Text="Export to EXCEL" Visible="False" CssClass="boton" __designer:dtid="13792273858822192" __designer:wfdid="w45"></asp:Button><BR /><asp:GridView id="GridView1" runat="server" Font-Size="8pt" Font-Names="Arial" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="25" AllowPaging="True" __designer:wfdid="w46">
<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="9pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="cmdExport"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel><br />
            </td>
        </tr>
    </table>
                <asp:SqlDataSource ID="SqlDataSource1" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                SelectCommand="SELECT CustBroker AS Broker FROM CBroker WHERE (CustBroker <> 'shi jie') AND (CustBroker <> 'marc') UNION SELECT VendorBroker AS Broker FROM VBroker WHERE (VendorBroker <> 'N/A') AND (VendorBroker <> 'shi jie') AND (VendorBroker <> 'marc')" runat="server"></asp:SqlDataSource>
    &nbsp;&nbsp;<br />
    <cc1:calendarextender id="CalendarExtender1" runat="server" popupbuttonid="Image1"
        targetcontrolid="TextBox1"></cc1:calendarextender>
    <cc1:calendarextender id="CalendarExtender2" runat="server" popupbuttonid="Image2"
        targetcontrolid="TextBox2"></cc1:calendarextender>
    <br />

</asp:Content>

