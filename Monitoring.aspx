<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Monitoring.aspx.cs" Inherits="Monitoring" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%--<script language="javascript" type="text/javascript">
    
    function validTT(){
        
        var number = document.getElementById("txtNumara");
        
        if(isNaN(number))
        {
            alert("The TT# needs to be numeric.");
            txtNumara.value = "";		
		    txtNumara.focus();	
        }
    }

</script>--%>

    <table align="left" id="TABLE1" language="javascript" onclick="return TABLE1_onclick()">
        <tr>
            <td style="width: 188px" align="left">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="New Network Management entry"></asp:Label></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager>&nbsp;</td>
        </tr>
        <tr>
            <td align="left" style="width: 188px">
                
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
                        <TABLE>
                            <TBODY>
                                <TR>
                                    <TD style="WIDTH: 100px; HEIGHT: 21px" align=center bgColor=#d5e3f0>
                                        <asp:Label id="lblnumara" runat="server" Text="TT#" CssClass="labelBlue8">
                                        </asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px" align=left>
                                        <asp:TextBox id="txtNumara" runat="server" >
                                        </asp:TextBox>
                                    </TD>
                                 </TR>
                                <TR>
                                    <TD style="WIDTH: 100px; HEIGHT: 21px" align=center bgColor=#d5e3f0>
                                        <asp:Label id="Label2" runat="server" Text="Type" CssClass="labelBlue8">
                                        </asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px; HEIGHT: 21px" align=left>
                                        <asp:DropDownList id="dpdType" runat="server" CssClass="dropdown" AutoPostBack="True" OnSelectedIndexChanged="dpdType_SelectedIndexChanged">
                                            <asp:ListItem Enabled="False" Selected="True" Value="2">** Select Type **</asp:ListItem>
                                            <%--<asp:ListItem Value="2">Routing/Port Change</asp:ListItem>
                                            <asp:ListItem Value="1">Testing/In-Monitoring</asp:ListItem>--%>
                                            <asp:ListItem Value="0">3rd Strike</asp:ListItem>
                                        </asp:DropDownList> 
                                    </TD>
                                </TR>
                                <TR>
                                    <TD style="WIDTH: 100px" align=center bgColor=#d5e3f0>
                                        <asp:Label id="Label3" runat="server" Text="Vendor" CssClass="labelBlue8">
                                        </asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px" align=left>
                                        <asp:DropDownList id="dpdVendor" runat="server" CssClass="dropdown" OnSelectedIndexChanged="dpdVendor_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </TD>
                                </TR>
                                <%--<TR>
                                    <TD style="WIDTH: 100px" align=center bgColor=#d5e3f0>&nbsp;
                                        <asp:Label id="Label6" runat="server" Text="Nextone" CssClass="labelBlue8" __designer:wfdid="w5">
                                        </asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px" align=left>
                                        <asp:DropDownList id="dpdNextone" runat="server" CssClass="dropdown" __designer:wfdid="w6">
                                            <%--<asp:ListItem Value="NY03">NY03</asp:ListItem>--%>
                                            <%--<asp:ListItem Value="NY04">NY04</asp:ListItem>
                                            <asp:ListItem Value="NY06">NY06</asp:ListItem>
                                            <asp:ListItem Value="NY07">NY07</asp:ListItem>
                                            <asp:ListItem Value="MIA03">MIA03</asp:ListItem>
                                            <asp:ListItem Value="EZLCR">EZLCR</asp:ListItem>
                                            <asp:ListItem Value="EZLCR_MIA">EZLCR_MIA</asp:ListItem>
                                            <asp:ListItem Value="EZLCR_STC">EZLCR_STC</asp:ListItem>
                                            <asp:ListItem Selected="True" Value="ALL">** ALL ** </asp:ListItem>
                                        </asp:DropDownList>
                                    </TD>
                                </TR>--%>
                                <TR>
                                    <TD style="WIDTH: 100px" align=center bgColor=#d5e3f0>&nbsp;
                                        <asp:Label id="Label10" runat="server" Text="Status" CssClass="labelBlue8">
                                        </asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px" align=left>
                                        <asp:DropDownList id="dpdStatus" runat="server" CssClass="dropdown" Enabled="True">
                                            <%--<asp:ListItem Value="Pending">Pending</asp:ListItem>
                                            <asp:ListItem Value="In-Testing">In-Testing</asp:ListItem>
                                            <asp:ListItem Value="Test Approved">Test Approved</asp:ListItem>
                                            <asp:ListItem Value="Test Not Approved">Test Not Approved</asp:ListItem>
                                            <asp:ListItem Value="In Monitoring">In Monitoring</asp:ListItem>
                                            <asp:ListItem Value="In Routing">In Routing</asp:ListItem>--%>
                                            <asp:ListItem Value="Closed">Closed</asp:ListItem>
                                            <asp:ListItem Value="Solved">Solved</asp:ListItem>
                                        </asp:DropDownList>
                                    </TD>
                                </TR>
                                <TR>
                                    <TD style="WIDTH: 100px" bgColor=#d5e3f0>
                                        <asp:Label id="Label4" runat="server" Text="Destination" CssClass="labelBlue8">
                                        </asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px" align=left>
                                        <asp:TextBox id="txtDestination" runat="server">
                                        </asp:TextBox>
                                    </TD>
                                 </TR>
                                 <TR>
                                    <TD style="WIDTH: 100px" align=center bgColor=#d5e3f0>
                                        <asp:Label id="Label5" runat="server" Text="Specific Change" CssClass="labelBlue8">
                                        </asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px" align=left>
                                        <asp:TextBox id="txtSpecific" runat="server"></asp:TextBox>
                                    </TD>
                                 </TR>
                                 <%--<TR>
                                    <TD style="WIDTH: 100px" align=center bgColor=#d5e3f0>
                                        <asp:Label id="Label11" runat="server" Text="Penalty Status" CssClass="labelBlue8" __designer:wfdid="w13">
                                        </asp:Label>
                                    </TD>
                                    <TD style="WIDTH: 100px" align=left>
                                        <asp:DropDownList id="dpdPenalty" runat="server" CssClass="dropdown" __designer:wfdid="w14">
                                            <asp:ListItem Value="Warning">Warning</asp:ListItem>
                                            <asp:ListItem Value="Alert">Alert</asp:ListItem>
                                            <asp:ListItem>Closed</asp:ListItem>
                                            <asp:ListItem>Restored</asp:ListItem>
                                        </asp:DropDownList>
                                    </TD>
                                  </TR>--%>
                                  <%--<TR>
                                      <TD style="WIDTH: 100px" align=center bgColor=#d5e3f0>
                                        <asp:Label id="Label9" runat="server" Text="Capacity" CssClass="labelBlue8" __designer:wfdid="w15">
                                        </asp:Label>
                                      </TD>
                                      <TD style="WIDTH: 100px" align=left>
                                        <asp:TextBox id="txtCapacity" runat="server" __designer:wfdid="w16">
                                        </asp:TextBox>
                                      </TD>
                                   </TR>--%>
                                   <%--<TR>
                                        <TD style="WIDTH: 100px" align=center bgColor=#d5e3f0>
                                            <asp:Label id="lblTarget" runat="server" Text="Target ASR/ACD" Visible="False" CssClass="labelBlue8">
                                            </asp:Label>
                                        </TD>
                                        <TD style="WIDTH: 100px" align=left>
                                            <asp:TextBox id="txtTarget" runat="server" Visible="False">
                                            </asp:TextBox>
                                        </TD>
                                   </TR>
                                   <TR>
                                       <TD style="WIDTH: 100px" align=center bgColor=#d5e3f0>&nbsp;
                                            <asp:Label id="lblProduction" runat="server" Text="Production ASR/ACD" Visible="False" CssClass="labelBlue8">
                                            </asp:Label>
                                       </TD>
                                       <TD style="WIDTH: 100px" align=left>
                                            <asp:TextBox id="txtProduction" runat="server" Visible="False">
                                            </asp:TextBox>
                                       </TD>
                                    </TR>--%>
                                    <TR>
                                        <TD style="WIDTH: 100px; HEIGHT: 42px" align=center bgColor=#d5e3f0>
                                            <asp:Label id="Label12" runat="server" Text="Comments" CssClass="labelBlue8">
                                            </asp:Label>
                                        </TD>
                                        <TD style="WIDTH: 100px; HEIGHT: 42px">
                                            <asp:TextBox id="txtComments" runat="server" Width="150px" CssClass="labelSteelBlue" TextMode="MultiLine">
                                            </asp:TextBox>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD style="WIDTH: 100px; HEIGHT: 20px" align=left></TD>
                                        <TD style="WIDTH: 100px; HEIGHT: 20px" align=right>&nbsp;&nbsp;
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD style="WIDTH: 100px; HEIGHT: 20px" align=left></TD>
                                        <TD style="WIDTH: 100px; HEIGHT: 20px" align=right>&nbsp;
                                        <asp:Button id="cmdInsert" onclick="cmdInsert_Click" runat="server" Text="Save" CssClass="boton">
                                        </asp:Button>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD style="WIDTH: 100px; HEIGHT: 20px" align=left></TD>
                                        <TD style="WIDTH: 100px; HEIGHT: 20px" align=right>
                                            <asp:Label id="lblEnd" runat="server" Text="Entry Registered" Font-Size="9pt" Visible="False" CssClass="labelBlue8">
                                            </asp:Label>
                                        </TD>
                                    </TR>
                          </TBODY>
                </TABLE>
</contenttemplate>
      <Triggers>

<asp:AsyncPostBackTrigger ControlID="cmdInsert"> </asp:AsyncPostBackTrigger>
</Triggers>

                </asp:UpdatePanel></td>
            <td style="width: 100px" align="left" valign="bottom">
                <asp:UpdateProgress id="UpdateProgress1" runat="server">
                    <progresstemplate>
<TABLE><TBODY><TR><TD style="WIDTH: 100px"><asp:Label id="lblLoading" runat="server" Text="Loading please wait..." ForeColor="LimeGreen" Width="110px" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:Image id="Image3" runat="server" ImageUrl="~/Images/Loading.gif"></asp:Image></TD></TR></TBODY></TABLE>
</progresstemplate>
                </asp:UpdateProgress></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td colspan="3" rowspan="2" align="left">
                <br />
                <br />
            </td>
        </tr>
        <tr>
        </tr>
    </table>
</asp:Content>

