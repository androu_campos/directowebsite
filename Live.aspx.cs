using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Live : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = "";
        DataSet ds = null;
        try
        {
            
            getStatus(sender, e);
            getStatusRTSRVMIA(sender, e);
            getStatusRTSRVSTC(sender, e);
            //load_ny03();            
            load_ny06();
            load_mia01();
            load_mia02();
            load_mia08();

            if (Session["DirectoUser"].ToString() != "jmanatlao")
            {
                load_did();
                load_ny05();
            }
            else
            {
                lblicsdid.Visible = false;
                lbldidprovider.Visible = false;
                lblidny05.Visible = false;
                labelNY05.Visible = false;
            }

            
            //load_stc03();

            sql = "select crdate as Modified from [cdrdb].[dbo].sysobjects  where type = 'U' and name like 'cdrnVendors'";
            ds = Util.RunQueryByStmnt(sql);
            lblUpdate.Text = "Last Modified:" + ds.Tables[0].Rows[0][0].ToString();

            //search(sender, e, gdvny03cust);
            //search(sender, e, gdvny03vend);
            //search(sender, e, gdvny07cust);
            //search(sender, e, gdvny07vend);
            search(sender, e, gdvmia02cust);
            search(sender, e, gdvmia02vend);
            search(sender, e, gdvny05cust);
            search(sender, e, gdvny05vend);
            search(sender, e, gdvny06cust);
            search(sender, e, gdvny06vend);
            search(sender, e, gdvMIA01cust);
            search(sender, e, gdvMIA01vend);
            search(sender, e, gdvmia08cust);
            search(sender, e, gdvmia08vend);

        }

        catch (Exception ex)
        {
           

            getStatus(sender, e);
            //load_ny03();
            load_ny06();
            //load_ny07();
            load_mia02();
            sql = "select crdate as Modified from [cdrdb].[dbo].sysobjects  where type = 'U' and name like 'cdrnVendors'";
            ds = Util.RunQueryByStmnt(sql);
            lblUpdate.Text = "Last Modified:" + ds.Tables[0].Rows[0][0].ToString();

            string error = ex.Message.ToString();

        }


    }

    protected void getStatus(object sender, EventArgs e)
    {
        DataSet ds = null;
        string MIA06A = "", MIA06B = "", MIA03A = "", MIA03B = "", MIA02A = "", MIA02B = "", STCB1 = "", STCB2 = "", MIA01A = "", MIA01B = "", MIA08A = "", MIA08B = "", MEXA = "", MEXB = "";
        string sql = "SELECT ID,STATUS FROM cdrdb..NextoneStatus order by id";

        ds = Util.RunQueryByStmntatCDRDB(sql);

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            switch (i)
            {
                case 0: MIA01A = ds.Tables[0].Rows[i][1].ToString(); break;
                case 1: MIA01B = ds.Tables[0].Rows[i][1].ToString(); break;
                case 2: MIA02A = ds.Tables[0].Rows[i][1].ToString(); break;
                case 3: MIA02B = ds.Tables[0].Rows[i][1].ToString(); break;
                case 4: MIA03A = ds.Tables[0].Rows[i][1].ToString(); break;
                case 5: MIA03B = ds.Tables[0].Rows[i][1].ToString(); break;
                case 6: MIA06A = ds.Tables[0].Rows[i][1].ToString(); break;
                case 7: MIA06B = ds.Tables[0].Rows[i][1].ToString(); break;
                case 8: MIA08A = ds.Tables[0].Rows[i][1].ToString(); break;
                case 9: MIA08B = ds.Tables[0].Rows[i][1].ToString(); break;
                case 10: STCB1 = ds.Tables[0].Rows[i][1].ToString(); break;
                case 11: STCB2 = ds.Tables[0].Rows[i][1].ToString(); break;
                //case 10: STC03A = ds.Tables[0].Rows[i][1].ToString(); break;
                //case 11: STC03B = ds.Tables[0].Rows[i][1].ToString(); break;
                case 12: MEXA = ds.Tables[0].Rows[i][1].ToString(); break;
                case 13: MEXB = ds.Tables[0].Rows[i][1].ToString(); break;
            }

        }

        if (MIA06A == "1")
        {
            lblstatusMIA01A.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
            lblstatusMIA01B.BackColor = System.Drawing.Color.White;            
        }
        else
        {
            lblstatusMIA01B.BackColor = System.Drawing.Color.White;
            lblstatusMIA01A.BackColor = System.Drawing.Color.Red;
        }


        if (MIA02A == "1")
        {
            labelstatmia02A.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
            labelstatmia02B.BackColor = System.Drawing.Color.White;
        }
        else
        {
            labelstatmia02B.BackColor = System.Drawing.Color.Red;
            labelstatmia02A.BackColor = System.Drawing.Color.White;
        }

        if (MIA03A == "1")
        {
            labelstatmia03A.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
            labelstatmia03B.BackColor = System.Drawing.Color.White;
        }
        else
        {
            labelstatmia03B.BackColor = System.Drawing.Color.Red;
            labelstatmia03A.BackColor = System.Drawing.Color.White;
        }

        if (STCB2 == "1")
        {
            labelstatusSTC01B.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
            labelstatusSTC01A.BackColor = System.Drawing.Color.White;
        }
        else
        {
            labelstatusSTC01A.BackColor = System.Drawing.Color.Red;
            labelstatusSTC01B.BackColor = System.Drawing.Color.White;
        }
        if (MIA08A == "1")
        {
            
            labelstatmia08A.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
            labelstatmia08B.BackColor = System.Drawing.Color.White;

        }
        else
        {
            labelstatmia08B.BackColor = System.Drawing.Color.Red;
            labelstatmia08A.BackColor = System.Drawing.Color.White;
        }

        if (MEXA == "1")
        {

            labelstatmexA.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
            labelstatmexB.BackColor = System.Drawing.Color.White;

        }
        else
        {
            labelstatmexB.BackColor = System.Drawing.Color.Red;
            labelstatmexA.BackColor = System.Drawing.Color.White;
        }

    }

    protected void getStatusRTSRVMIA(object sender, EventArgs e)
    {
        DataSet ds = null;
        string active = "", alert = "";
        string sql = "select ez_active,alert from ltwez_config where ez_active in (select ezName from dbo.ltwez_names where ezgroup = 'MIA') ";

        ds = Util.RunQueryByStmntatCDRDB(sql);

        if (ds.Tables.Count > 0)
        {
            active = ds.Tables[0].Rows[0][0].ToString();
            alert = ds.Tables[0].Rows[0][1].ToString();
        }

        if (active == "RTSRV_MIA01" && alert == "False")
        {
            lblrtsrvmia01.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
            lblrtsrvmia02.BackColor = System.Drawing.Color.White; 
        }
        else
        {
            lblrtsrvmia02.BackColor = System.Drawing.Color.Red;
            lblrtsrvmia01.BackColor = System.Drawing.Color.White; 
        }
    }

    protected void getStatusRTSRVSTC(object sender, EventArgs e)
    {
        DataSet ds = null;
        string active = "", alert = "";
        string sql = "select ez_active,alert from ltwez_config where ez_active in (select ezName from dbo.ltwez_names where ezgroup = 'STC') ";

        ds = Util.RunQueryByStmntatCDRDB(sql);

        if (ds.Tables.Count > 0)
        {
            active = ds.Tables[0].Rows[0][0].ToString();
            alert = ds.Tables[0].Rows[0][1].ToString();
        }

        if (active == "RTSRV_STC01" && alert == "False")
        {
            lblrtsrvstc01.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
            lblrtsrvstc02.BackColor = System.Drawing.Color.White;
        }
        else
        {
            lblrtsrvstc02.BackColor = System.Drawing.Color.Red;
            lblrtsrvstc01.BackColor = System.Drawing.Color.White;
        }
    }

    //private void load_ny03()
    //{
    //    string sql = "select '**ALL**' as Customer, sum(Live_Calls) as [Total Calls]";
    //           sql += " from cdrdb..cdrn ";
    //           sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%NY03%' and len(Cust_Vendor) = 4 and type like  '%Customer%' and cust_vendor <> ('TRAI') ";
    //           sql += " union";
    //           sql += " select Cust_Vendor , sum(Live_Calls)  ";
    //           sql += " from cdrdb..cdrn ";
    //           sql += " where time = (select max(time) from cdrdb..cdrn) and id like '%NY03%' and len(Cust_Vendor) = 4 and type like  '%Customer%' ";
    //           sql += " group by Cust_Vendor";
    //    DataSet ds = null;

    //    ds = Util.RunQueryByStmnt(sql);
    //    gdvny03cust.DataSource = ds.Tables[0];
    //    gdvny03cust.DataBind();

        

    //    ds = null;
    //    sql = "select '**ALL**' as Vendor, sum(Live_Calls)  as [Total Calls]";
    //    sql += " from cdrdb..cdrnVendors ";
    //    sql += " where id like '%NY03%' and type like  '%Vendor%' and cust_vendor <> ('TRAS') ";
    //    sql += " union";
    //    sql += " select Cust_Vendor, sum(Live_Calls)  ";
    //    sql += " from cdrdb..cdrnVendors  ";
    //    sql += " where id like '%NY03%' and type like  '%Vendor%' ";
    //    sql += " group by Cust_Vendor";
    //    ds = Util.RunQueryByStmnt(sql);
    //    gdvny03vend.DataSource = ds.Tables[0];
    //    gdvny03vend.DataBind();
    //}    

    private void load_ny06()
    {
        string sql = "select '**ALL**' as Customer, sum(Live_Calls) as [Total Calls]";
        sql += " from cdrdb..cdrn ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%STCB%' and len(Cust_Vendor) = 4 and type like  '%Customer%' and cust_vendor not in ('TRAI','ZLCR') ";
        sql += " union";
        sql += " select Cust_Vendor,sum(Live_Calls)  ";
        sql += " from cdrdb..cdrn  ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%STCB%' and len(Cust_Vendor) = 4 and type like  '%Customer%' and cust_vendor not in ('TRAI','ZLCR')";
        sql += " group by Cust_Vendor";
        DataSet ds = null;

        ds = Util.RunQueryByStmnt(sql);
        gdvny06cust.DataSource = ds.Tables[0];
        gdvny06cust.DataBind();

        ds = null;
        sql = "select '**ALL**' as Vendor, sum(Live_Calls)  as [Total Calls]";
        sql += " from cdrdb..cdrnVendors ";
        sql += " where id like '%STCB%' and type like  '%Vendor%'";
        sql += " union";
        sql += " select Cust_Vendor, sum(Live_Calls)  ";
        sql += " from cdrdb..cdrnVendors  ";
        sql += " where id like '%STCB%' and type like  '%Vendor%'";
        sql += " group by Cust_Vendor";
        ds = Util.RunQueryByStmnt(sql);
        gdvny06vend.DataSource = ds.Tables[0];
        gdvny06vend.DataBind();
    }

    //private void load_ny07()
    //{
    //    string sql = "select '**ALL**' as Customer, sum(Live_Calls)  as [Total Calls]";
    //    sql += " from cdrdb..cdrn ";
    //    sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%NY07%' and len(Cust_Vendor) = 4 and type like  '%Customer%' and cust_vendor <> ('TRAI') ";
    //    sql += " union";
    //    sql += " select Cust_Vendor, sum(Live_Calls)  ";
    //    sql += " from cdrdb..cdrn  ";
    //    sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%NY07%' and len(Cust_Vendor) = 4 and type like  '%Customer%' ";
    //    sql += " group by Cust_Vendor";
    //    DataSet ds = null;

    //    ds = Util.RunQueryByStmnt(sql);
    //    gdvny07cust.DataSource = ds.Tables[0];
    //    gdvny07cust.DataBind();

    //    ds = null;
    //    sql = "select '**ALL**' as Vendor, sum(Live_Calls)  as [Total Calls]";
    //    sql += " from cdrdb..cdrnVendors ";
    //    sql += " where id like '%NY07%' and type like  '%Vendor%' and cust_vendor <> ('TRAS') ";
    //    sql += " union";
    //    sql += " select Cust_Vendor, sum(Live_Calls)  ";
    //    sql += " from cdrdb..cdrnVendors  ";
    //    sql += " where id like '%NY07%' and type like  '%Vendor%' ";
    //    sql += " group by Cust_Vendor";
    //    ds = Util.RunQueryByStmnt(sql);
    //    gdvny07vend.DataSource = ds.Tables[0];
    //    gdvny07vend.DataBind();
    //}

    private void load_mia02()
    {
        string sql = "";

        if (Session["DirectoUser"].ToString() == "jmanatlao")
        {
            sql = "select '**ALL**' as Customer, sum(Live_Calls)  as [Total Calls]";
            sql += " from cdrdb..cdrn ";
            sql += " where time = (select max(time) from cdrdb..cdrn ) and id IN ('MIA03','MIA02') AND type like  '%Customer%' AND CUST_VENDOR IN ('ASLS','PHCC','AUPS')";
            sql += " union";
            sql += " select Cust_Vendor, sum(Live_Calls)  ";
            sql += " from cdrdb..cdrn  ";
            sql += " where time = (select max(time) from cdrdb..cdrn ) and id IN ('MIA03','MIA02') and type like  '%Customer%' AND CUST_VENDOR IN ('ASLS','PHCC','AUPS')";
            sql += " group by Cust_Vendor";



            DataSet ds = null;

            ds = Util.RunQueryByStmnt(sql);
            gdvmia02cust.DataSource = ds.Tables[0];
            gdvmia02cust.DataBind();

            ds = null;
            sql = "select '**ALL**' as Vendor, sum(Live_Calls)  as [Total Calls]";
            sql += " from cdrdb..cdrn ";
            sql += " where id IN ('MIA03','MIA02','MIA04')  and type like  '%Vendor%' and cust_vendor IN ('APAS','APBX') ";
            sql += " union";
            sql += " select Cust_Vendor, sum(Live_Calls)  ";
            sql += " from cdrdb..cdrn  ";
            sql += " where id IN ('MIA03','MIA02','MIA04') and type like  '%Vendor%' and cust_vendor IN ('APAS','APBX') ";
            sql += " group by Cust_Vendor";
            ds = Util.RunQueryByStmnt(sql);
            gdvmia02vend.DataSource = ds.Tables[0];
            gdvmia02vend.DataBind();
        }
        else
        {



             sql = "select '** ALL **' as Customer, sum([Total Calls]) as [Total Calls] "
                   + "from ( "
                   + "select sum(Live_Calls)  as [Total Calls] "
                   + "from cdrdb..cdrn "
                   + "where time = (select max(time) from cdrdb..cdrn ) "
                   + "and id like '%MIA02%'and len(Cust_Vendor) = 4 and "
                   + "type like  '%Customer%' and cust_vendor not in ('TRAI','ZLCR') "
                //+ "and cust_vendor not in ('ICS5','ICS2')"
                   + "union "
                   + "select sum(Live_Calls)  as [Total Calls] "
                   + "from cdrdb..cdrn "
                   + "where time = (select max(time) from cdrdb..cdrn ) "
                   + "and id like '%MIA03%'and len(Cust_Vendor) = 4 and "
                   + "type like  '%Customer%' and cust_vendor not in ('TRAI','ZLCR') "
                //+ "and  cust_vendor in ('DIP5','DIP2','DIN5')) t "
                   + "and  cust_vendor in ('DIDP','DIDV','DIDB','INET') ) t "
                   + " union "
                   + "select Cust_Vendor as Customer, [Total Calls] "
                   + "from ( "
                   + "select Cust_Vendor, sum(Live_Calls)  as [Total Calls] "
                   + "from cdrdb..cdrn "
                   + "where time = (select max(time) from cdrdb..cdrn ) "
                   + "and id like '%MIA02%'and len(Cust_Vendor) = 4 and "
                   + "type like  '%Customer%' "
                   + "and cust_vendor not in ('ZLCR')"
                   + "group by Cust_Vendor "
                   + "union "
                   + "select Cust_Vendor, sum(Live_Calls)  as [Total Calls] "
                   + "from cdrdb..cdrn "
                   + "where time = (select max(time) from cdrdb..cdrn ) "
                   + "and id like '%MIA03%'and len(Cust_Vendor) = 4 and "
                   + "type like  '%Customer%' "
                //+ "and  cust_vendor in ('DIP5','DIP2','DIN5') "
                   + "and  cust_vendor in ('DIDP','DIDV','DIDB','INET') "
                   + "group by Cust_Vendor) t2";

            //string sql = "select '**ALL**' as Customer, sum(Live_Calls)  as [Total Calls]";
            //sql += " from cdrdb..cdrn ";
            //sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA02%' and len(Cust_Vendor) = 4 and type like  '%Customer%' and cust_vendor <> ('TRAI') ";
            //sql += " union";
            //sql += " select Cust_Vendor, sum(Live_Calls)  ";
            //sql += " from cdrdb..cdrn  ";
            //sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA02%' and len(Cust_Vendor) = 4 and type like  '%Customer%' ";
            //sql += " group by Cust_Vendor";

            DataSet ds = null;

            ds = Util.RunQueryByStmnt(sql);
            gdvmia02cust.DataSource = ds.Tables[0];
            gdvmia02cust.DataBind();

            ds = null;
            sql = "select '**ALL**' as Vendor, sum(Live_Calls)  as [Total Calls]";
            sql += " from cdrdb..cdrnVendors ";
            sql += " where id like '%MIA03%' and type like  '%Vendor%' and cust_vendor not in  ('TRAS','PLDT') ";
            sql += " union";
            sql += " select Cust_Vendor, sum(Live_Calls)  ";
            sql += " from cdrdb..cdrnVendors  ";
            sql += " where id like '%MIA03%' and type like  '%Vendor%' and cust_vendor not in  ('TRAS','PLDT') ";
            sql += " group by Cust_Vendor";
            ds = Util.RunQueryByStmnt(sql);
            gdvmia02vend.DataSource = ds.Tables[0];
            gdvmia02vend.DataBind();
        }
    }

    private void load_did()
    {
        string sql = "select '**ALL**' as Customer, sum(Live_Calls)  as [Total Calls]";
        sql += " from cdrdb..cdrn ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA03%' and len(Cust_Vendor) = 4 and type like  '%Customer%' and cust_vendor <> ('TRAI') and cust_vendor in ('DIDN','DIDP','DIDB','DIDV','INET','DIDA') ";
        sql += " union";
        sql += " select Cust_Vendor, sum(Live_Calls)  ";
        sql += " from cdrdb..cdrn  ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA03%' and len(Cust_Vendor) = 4 and type like  '%Customer%' and cust_vendor <> ('TRAI') and cust_vendor in ('DIDN','DIDP','DIDB','DIDV','INET','DIDA')";
        sql += " group by Cust_Vendor";

        //string sql = "select '** ALL **' as Customer, sum([Total Calls]) as [Total Calls] "
        //       + "from ( "
        //       + "select sum(Live_Calls)  as [Total Calls] "
        //       + "from cdrdb..cdrn "
        //       + "where time = (select max(time) from cdrdb..cdrn ) "
        //       + "and id like '%MIA02%'and len(Cust_Vendor) = 4 and "
        //       + "type like  '%Customer%' and cust_vendor <> ('TRAI') "
        //       + "and cust_vendor = 'DIDN'"
        //       + "union "
        //       + "select sum(Live_Calls)  as [Total Calls] "
        //       + "from cdrdb..cdrn "
        //       + "where time = (select max(time) from cdrdb..cdrn ) "
        //       + "and id like '%MIA03%'and len(Cust_Vendor) = 4 and "
        //       + "type like  '%Customer%' and cust_vendor <> ('TRAI') "
        //    //+ "and  cust_vendor in ('DIP5','DIP2','DIN5')) t "
        //       + "and  cust_vendor = 'DIDP' ) t "
        //       + " union "
        //       + "select Cust_Vendor as Customer, [Total Calls] "
        //       + "from ( "
        //       + "select Cust_Vendor, sum(Live_Calls)  as [Total Calls] "
        //       + "from cdrdb..cdrn "
        //       + "where time = (select max(time) from cdrdb..cdrn ) "
        //       + "and id like '%MIA02%'and len(Cust_Vendor) = 4 and "
        //       + "type like  '%Customer%' "
        //       + "and cust_vendor  = 'DIDN' "
        //       + "group by Cust_Vendor "
        //       + "union "
        //       + "select Cust_Vendor, sum(Live_Calls)  as [Total Calls] "
        //       + "from cdrdb..cdrn "
        //       + "where time = (select max(time) from cdrdb..cdrn ) "
        //       + "and id like '%MIA03%'and len(Cust_Vendor) = 4 and "
        //       + "type like  '%Customer%' "
        //    //+ "and  cust_vendor in ('DIP5','DIP2','DIN5') "
        //       + "and  cust_vendor = 'DIDP' "
        //       + "group by Cust_Vendor) t2";

        DataSet ds = null;
        ds = Util.RunQueryByStmnt(sql);
        gdvdidIn.DataSource = ds.Tables[0];
        gdvdidIn.DataBind();

        ds = null;

        sql = "select '**ALL**' as Customer, sum(Live_Calls)  as [Total Calls]";
        sql += " from cdrdb..cdrn ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA04%' and len(Cust_Vendor) = 4 and type like  '%Customer%' and cust_vendor <> ('TRAI') and cust_vendor in ('DBTZ','DIDH','DIDK','DIDX','DVOI') ";
        sql += " union";
        sql += " select Cust_Vendor, sum(Live_Calls)  ";
        sql += " from cdrdb..cdrn  ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA04%' and len(Cust_Vendor) = 4 and type like  '%Customer%' and cust_vendor <> ('TRAI') and cust_vendor in ('DBTZ','DIDH','DIDK','DIDX','DVOI')";
        sql += " group by Cust_Vendor";

        //sql = "select '** ALL **' as Customer, sum([Total Calls]) as [Total Calls] "
        //       + "from ( "
        //       + "select sum(Live_Calls)  as [Total Calls] "
        //       + "from cdrdb..cdrn "
        //       + "where time = (select max(time) from cdrdb..cdrn ) "
        //       + "and id like '%MIA02%'and len(Cust_Vendor) = 4 and "
        //       + "type like  '%Customer%' and cust_vendor <> ('TRAI') "
        //       + "and cust_vendor = 'DIDN'"
        //       + "union "
        //       + "select sum(Live_Calls)  as [Total Calls] "
        //       + "from cdrdb..cdrn "
        //       + "where time = (select max(time) from cdrdb..cdrn ) "
        //       + "and id like '%MIA03%'and len(Cust_Vendor) = 4 and "
        //       + "type like  '%Customer%' and cust_vendor <> ('TRAI') "
        //    //+ "and  cust_vendor in ('DIP5','DIP2','DIN5')) t "
        //       + "and  cust_vendor = 'DIDP' ) t "
        //       + " union "
        //       + "select Cust_Vendor as Customer, [Total Calls] "
        //       + "from ( "
        //       + "select Cust_Vendor, sum(Live_Calls)  as [Total Calls] "
        //       + "from cdrdb..cdrn "
        //       + "where time = (select max(time) from cdrdb..cdrn ) "
        //       + "and id like '%MIA02%'and len(Cust_Vendor) = 4 and "
        //       + "type like  '%Customer%' "
        //       + "and cust_vendor  = 'DIDN' "
        //       + "group by Cust_Vendor "
        //       + "union "
        //       + "select Cust_Vendor, sum(Live_Calls)  as [Total Calls] "
        //       + "from cdrdb..cdrn "
        //       + "where time = (select max(time) from cdrdb..cdrn ) "
        //       + "and id like '%MIA03%'and len(Cust_Vendor) = 4 and "
        //       + "type like  '%Customer%' "
        //    //+ "and  cust_vendor in ('DIP5','DIP2','DIN5') "
        //       + "and  cust_vendor = 'DIDP' "
        //       + "group by Cust_Vendor) t2";

        ds = Util.RunQueryByStmnt(sql);

        ds = Util.RunQueryByStmnt(sql);
        gdvdidOut.DataSource = ds.Tables[0];
        gdvdidOut.DataBind();

    }

    private void load_ny05()
    {
        string sql = "select '**ALL**' as Customer, sum(Live_Calls)  as [Total Calls]";
        sql += " from cdrdb..cdrn ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and cust_vendor not like '%ZLCR%' and len(Cust_Vendor) = 4 and id like '%MIA03' and type like  '%Customer%' ";
        sql += " union";
        sql += " select Cust_Vendor, sum(Live_Calls)  ";
        sql += " from cdrdb..cdrn  ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and cust_vendor not like '%ZLCR%' and len(Cust_Vendor) = 4 and id like '%MIA03%' and type like  '%Customer%' ";
        sql += " group by Cust_Vendor";
        DataSet ds = null;

        ds = Util.RunQueryByStmnt(sql);

        gdvny05cust.DataSource = ds.Tables[0];
        gdvny05cust.DataBind();

        ds = null;
        sql = "select '**ALL**' as Vendor, sum(Live_Calls)  as [Total Calls]";
        sql += " from cdrdb..cdrnVendors ";
        sql += " where cust_vendor like '%TRAS%' and type like  '%Vendor%' ";
        sql += " union";
        sql += " select Cust_Vendor, sum(Live_Calls)  ";
        sql += " from cdrdb..cdrnVendors  ";
        sql += " where cust_vendor like '%TRAS%' and type like  '%Vendor%' ";
        sql += " group by Cust_Vendor";
        ds = Util.RunQueryByStmnt(sql);
        gdvny05vend.DataSource = ds.Tables[0];
        gdvny05vend.DataBind();
    }

    private void load_mia01()
    {
        string sql = "";

        if (Session["DirectoUser"].ToString() == "jmanatlao")
        {
            sql = "select '**ALL**' as Customer, sum(Live_Calls)  as [Total Calls]";
            sql += " from cdrdb..cdrn ";
            sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA01%' AND type like  '%Customer%' AND CUST_VENDOR IN ('ASLS','PHCC')";
            sql += " union";
            sql += " select Cust_Vendor, sum(Live_Calls)  ";
            sql += " from cdrdb..cdrn  ";
            sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA01%' and type like  '%Customer%' AND CUST_VENDOR IN ('ASLS','PHCC')";
            sql += " group by Cust_Vendor";
            DataSet ds = null;

            ds = Util.RunQueryByStmnt(sql);

            gdvMIA01cust.DataSource = ds.Tables[0];
            gdvMIA01cust.DataBind();

            ds = null;
            sql = "select '**ALL**' as Vendor, sum(Live_Calls)  as [Total Calls]";
            sql += " from cdrdb..cdrnVendors ";
            sql += " where id like '%MIA06%' and type like  '%Vendor%' and  CUST_VENDOR IN ('APAS','APBX')";
            sql += " union";
            sql += " select Cust_Vendor, sum(Live_Calls)  ";
            sql += " from cdrdb..cdrnVendors  ";
            sql += " where id like '%MIA06%' and type like  '%Vendor%' and  CUST_VENDOR IN ('APAS','APBX')";
            sql += " group by Cust_Vendor";
            ds = Util.RunQueryByStmnt(sql);
            gdvMIA01vend.DataSource = ds.Tables[0];
            gdvMIA01vend.DataBind();
        }
        else
        {


            sql = "select '**ALL**' as Customer, sum(Live_Calls)  as [Total Calls]";
            sql += " from cdrdb..cdrn ";
            sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA06%' AND type like  '%Customer%' AND CUST_VENDOR NOT LIKE '*%' AND CUST_VENDOR <> 'ZLCR'";
            sql += " union";
            sql += " select Cust_Vendor, sum(Live_Calls)  ";
            sql += " from cdrdb..cdrn  ";
            sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA06%' and type like  '%Customer%' AND CUST_VENDOR NOT LIKE '*%' AND CUST_VENDOR <> 'ZLCR'";
            sql += " group by Cust_Vendor";
            DataSet ds = null;

            ds = Util.RunQueryByStmnt(sql);

            gdvMIA01cust.DataSource = ds.Tables[0];
            gdvMIA01cust.DataBind();

            ds = null;
            sql = "select '**ALL**' as Vendor, sum(Live_Calls)  as [Total Calls]";
            sql += " from cdrdb..cdrnVendors ";
            sql += " where id like '%MIA06%' and type like  '%Vendor%' ";
            sql += " union";
            sql += " select Cust_Vendor, sum(Live_Calls)  ";
            sql += " from cdrdb..cdrnVendors  ";
            sql += " where id like '%MIA06%' and type like  '%Vendor%' ";
            sql += " group by Cust_Vendor";
            ds = Util.RunQueryByStmnt(sql);
            gdvMIA01vend.DataSource = ds.Tables[0];
            gdvMIA01vend.DataBind();
        }
    }

    private void load_stc03()
    {
        string sql = "select '**ALL**' as Customer, sum(Live_Calls)  as [Total Calls]";
        sql += " from cdrdb..cdrn ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%STC03%' AND type like  '%Customer%' AND CUST_VENDOR NOT LIKE '*%' AND CUST_VENDOR <> 'ZLCR'";
        sql += " union";
        sql += " select Cust_Vendor, sum(Live_Calls)  ";
        sql += " from cdrdb..cdrn  ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%STC03%' and type like  '%Customer%' AND CUST_VENDOR NOT LIKE '*%' AND CUST_VENDOR <> 'ZLCR'";
        sql += " group by Cust_Vendor";
        DataSet ds = null;

        ds = Util.RunQueryByStmnt(sql);

        //gdvSTC03cust.DataSource = ds.Tables[0];
        //gdvSTC03cust.DataBind();

        ds = null;
    }

    private void load_mia08()
    {
        string sql = "select '**ALL**' as Customer, sum(Live_Calls) as [Total Calls]";
        sql += " from cdrdb..cdrn ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA08%' and len(Cust_Vendor) = 4 and type like  '%Customer%' and cust_vendor not in ('TRAI','ZLCR') ";
        sql += " union";
        sql += " select Cust_Vendor,sum(Live_Calls)  ";
        sql += " from cdrdb..cdrn  ";
        sql += " where time = (select max(time) from cdrdb..cdrn ) and id like '%MIA08%' and len(Cust_Vendor) = 4 and type like  '%Customer%' and cust_vendor not in ('TRAI','ZLCR')";
        sql += " group by Cust_Vendor";
        DataSet ds = null;

        ds = Util.RunQueryByStmnt(sql);

        gdvmia08cust.DataSource = ds.Tables[0];
        gdvmia08cust.DataBind();

        ds = null;
        sql = "select '**ALL**' as Vendor, sum(Live_Calls)  as [Total Calls]";
        sql += " from cdrdb..cdrnVendors ";
        sql += " where id like '%MIA08%' and type like  '%Vendor%'";
        sql += " union";
        sql += " select Cust_Vendor, sum(Live_Calls)  ";
        sql += " from cdrdb..cdrnVendors  ";
        sql += " where id like '%MIA08%' and type like  '%Vendor%'";
        sql += " group by Cust_Vendor";
        ds = Util.RunQueryByStmnt(sql);
        gdvmia08vend.DataSource = ds.Tables[0];
        gdvmia08vend.DataBind();
    }

    protected void search(object sender,EventArgs e ,GridView gv)
    {

        for (int i = 0; i < gv.Rows.Count ; i++)
        {
            string customer =gv.Rows[i].Cells[0].Text;
            customer = customer.Trim();
            if (customer == "TRAI" || customer == "TRAS")
            {
                gv.Rows[i].Cells[0].BackColor = System.Drawing.Color.FromArgb(00,153,255);
                gv.Rows[i].Cells[1].BackColor = System.Drawing.Color.FromArgb(00, 153, 255);
                gv.Rows[i].Font.Bold = true;
            }
            if (customer == "ZLCR")
            {
                gv.Rows[i].Cells[0].BackColor = System.Drawing.Color.FromArgb(51, 204, 204);
                gv.Rows[i].Cells[1].BackColor = System.Drawing.Color.FromArgb(51, 204, 204);
                gv.Rows[i].Font.Bold = true;
            }
        }

        
    }

    

}
