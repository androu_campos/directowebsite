<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="CustVendorNames.aspx.cs" Inherits="CustVendorNames"
    Title="DIRECTO - Connections Worldwide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label1" runat="server" CssClass="labelTurkH" Text="Label" Font-Bold="True"></asp:Label>
    <table style="position:absolute; top:220px; left:230px;" width="100%">
       
        
        <tr>
            <td align="left" >
                <asp:GridView Width="300px" ID="gdvCV" runat="server" Font-Names="Arial" Font-Size="8pt" AllowPaging="true" OnPageIndexChanging="gdvCV_PageIndexChanging" PageSize="50">
                    <FooterStyle BackColor="#507CD1"  ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            
            <td></td>
            <td></td>
        
        </tr>
       
         <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
       
    </table>
 
</asp:Content>
