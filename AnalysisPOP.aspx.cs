using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class AnalysisPOP : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Text = DateTime.Now.Date.AddDays(-1).ToShortDateString();
            txtTo.Text = DateTime.Now.Date.AddDays(-1).ToShortDateString();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string pop, line;
        if (ddlPOP.SelectedItem.ToString() == "** ALL **") pop = "%";
        else pop = ddlPOP.SelectedItem.ToString();

        if (ddlLine.SelectedItem.ToString() == "** ALL **") line = "%";
        else line = ddlLine.SelectedItem.ToString();

        sqldsGv.SelectCommand = "SELECT '****' as BillingDate ,'**Total**' as Pop , '****' as Line, Sum(Duration)/60 as Minutes,Count(*) as TotalCalls,SUM(MECCA2.dbo.ValCompare(Duration,0,1)) as Calls,(CASE WHEN count(*) > 0 THEN 100*sum(MECCA2.dbo.ValCompare(Duration,0,1))/count(*) ELSE 0 END) as ASR,(CASE when sum(MECCA2.dbo.ValCompare(Duration,0,1)) > 0  then round((sum(Duration)/60)/(sum(MECCA2.dbo.ValCompare(Duration,0,1))) ,2) else 0 end) as ACD  FRom CDRSPOP WHERE start >= '" + txtFrom.Text + "' and  start <= '" + txtTo.Text + " 23:59:59' and Pop LIKE '" + pop + "' and Line Like '" + line + "' UNION SELECT Left(Start,11) as BillingDate, Pop, Line, SUM(Duration)/60 as Minutes, Count(*) as TotalCalls,SUM(MECCA2.dbo.ValCompare(Duration,0,1)) as Calls,(CASE WHEN count(*) > 0 THEN 100*sum(MECCA2.dbo.ValCompare(Duration,0,1))/count(*) ELSE 0 END) as ASR,(CASE when sum(MECCA2.dbo.ValCompare(Duration,0,1)) > 0  then round((sum(Duration)/60)/(sum(MECCA2.dbo.ValCompare(Duration,0,1))) ,2) else 0 end) as ACD  FRom CDRSPOP WHERE start >= '" + txtFrom.Text + "' and  start <= '" + txtTo.Text + " 23:59:59' and Pop LIKE '" + pop + "' and Line Like '" + line + "' Group By Left(Start,11), Pop, Line order by pop, line";
      
        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = System.Data.CommandType.Text;
        sqlSpQuery.CommandText = "SELECT '****' as BillingDate ,'**Total**' as Pop , '****' as Line, Sum(Duration)/60 as Minutes,Count(*) as TotalCalls,SUM(MECCA2.dbo.ValCompare(Duration,0,1)) as Calls,(CASE WHEN count(*) > 0 THEN 100*sum(MECCA2.dbo.ValCompare(Duration,0,1))/count(*) ELSE 0 END) as ASR,(CASE when sum(MECCA2.dbo.ValCompare(Duration,0,1)) > 0  then round((sum(Duration)/60)/(sum(MECCA2.dbo.ValCompare(Duration,0,1))) ,2) else 0 end) as ACD  FRom CDRDB..CDRSPOP WHERE start >= '" + txtFrom.Text + "' and  start <= '" + txtTo.Text + " 23:59:59' and Pop LIKE '" + pop + "' and Line Like '" + line + "' UNION SELECT Left(Start,11) as BillingDate, Pop, Line, SUM(Duration)/60 as Minutes, Count(*) as TotalCalls,SUM(MECCA2.dbo.ValCompare(Duration,0,1)) as Calls,(CASE WHEN count(*) > 0 THEN 100*sum(MECCA2.dbo.ValCompare(Duration,0,1))/count(*) ELSE 0 END) as ASR,(CASE when sum(MECCA2.dbo.ValCompare(Duration,0,1)) > 0  then round((sum(Duration)/60)/(sum(MECCA2.dbo.ValCompare(Duration,0,1))) ,2) else 0 end) as ACD  FRom CDRDB..CDRSPOP WHERE start >= '" + txtFrom.Text + "' and  start <= '" + txtTo.Text + " 23:59:59' and Pop LIKE '" + pop + "' and Line Like '" + line + "' Group By Left(Start,11), Pop, Line order by pop, line";
        DataSet ResultSet;
        ResultSet = Util.RunQuery(sqlSpQuery);
        Session["AnalysisPOP"] = ResultSet;
        cmdExport.Visible = true;//Hole

        gvResult.DataSource = ((DataSet)Session["AnalysisPOP"]).Tables[0].Copy();
        gvResult.DataBind();

    }


    protected void cmdExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {
            DataTable dtEmployee = ((DataSet)Session["AnalysisPOP"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "AnalysisPOP" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = "Data Source=localhost;Initial Catalog=CDRDB_LOGS;User ID=steward_of_Gondor;Password=nm@73-mg";
        //string connectionString = ConfigurationManager.ConnectionStrings
        //["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void gvResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvResult.PageIndex = e.NewPageIndex;
        gvResult.DataSource = ((DataSet)Session["AnalysisPOP"]).Tables[0].Copy();
        gvResult.DataBind();
    }
}
