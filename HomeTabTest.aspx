<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="HomeTabTest.aspx.cs" Inherits="HomeTabTest" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript" language="javascript">

function tabLoadInfo(){
    alert("Load Function");    

}
</script>

    <asp:ScriptManager id="ScriptManager1" runat="server"></asp:ScriptManager>
    <ajax:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="1200px" Height="1500px">
        <ajax:TabPanel ID="TabPanel1" runat="server" HeaderText="WholeSale" OnClientClick="tabLoadInfo">
            <ContentTemplate>
                <iframe id="wholesaleframe" src="Home.aspx" name="wholesaleframe" frameborder="0" width="100%" height="100%" visible="true"></iframe>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel ID="TabPanel2" runat="server" HeaderText="ICS">
            <ContentTemplate>
                <iframe id="icsframe" name="icsframe" frameborder="0" width="216px" height="250px"></iframe>
            </ContentTemplate>            
        </ajax:TabPanel>        
    </ajax:TabContainer>
</asp:Content>

