using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Monitoring : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            cdrdbDatasetTableAdapters.ProviderIPTableAdapter adp = new cdrdbDatasetTableAdapters.ProviderIPTableAdapter();
            cdrdbDataset.ProviderIPDataTable tbl = adp.GetNames();

            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                dpdVendor.Items.Add(tbl.Rows[i]["ProviderName"].ToString());
            }
            
            
        }

    }



    protected void cmdInsert_Click(object sender, EventArgs e)
    {
        
        MECCA2TableAdapters.MonitoringAdp adp = new MECCA2TableAdapters.MonitoringAdp();
        adp.InsertQuery(dpdVendor.SelectedValue.ToString(), txtDestination.Text, txtSpecific.Text, dpdStatus.SelectedValue.ToString(), Session["DirectoUser"].ToString(), DateTime.Now, txtComments.Text, "3rd Strike", Convert.ToInt32(txtNumara.Text));
        //adp.Insert(dpdVendor.SelectedValue.ToString(), txtDestination.Text, txtSpecific.Text, "", "", "", "", dpdStatus.SelectedValue.ToString(), Session["DirectoUser"].ToString(), DateTime.Now, "", txtComments.Text, "3rd Strike", Convert.ToInt32(txtNumara.Text));


        //if (dpdType.SelectedValue.ToString() == "0")
        //{
        //    adp.Insert(dpdVendor.SelectedValue.ToString(), txtDestination.Text, txtSpecific.Text, dpdNextone.SelectedValue.ToString(), txtTarget.Text, txtProduction.Text, txtCapacity.Text, dpdStatus.SelectedValue.ToString(), Session["DirectoUser"].ToString(), DateTime.Now, dpdPenalty.SelectedValue.ToString(), txtComments.Text, "Routing / Port Change", Convert.ToInt32(txtNumara.Text));
        //    lblEnd.Visible = true;
            
            
            
        //    this.txtDestination.Text = string.Empty;
        //    this.txtSpecific.Text = string.Empty;
        //    this.txtCapacity.Text = string.Empty;
        //    this.txtComments.Text = string.Empty;

        //}
        //else
        //{
        //    adp.Insert(dpdVendor.SelectedValue.ToString(), txtDestination.Text, "", "", txtTarget.Text, txtProduction.Text, txtCapacity.Text, dpdStatus.SelectedValue.ToString(), Session["DirectoUser"].ToString(), DateTime.Now, dpdPenalty.SelectedValue.ToString(), txtComments.Text, "Testing/In-Monitoring", Convert.ToInt32(txtNumara.Text));
        //    lblEnd.Visible = true;
        //    this.txtDestination.Text = string.Empty;
        //    this.txtCapacity.Text = string.Empty;
        //    this.txtTarget.Text = string.Empty;
        //    this.txtProduction.Text = string.Empty;
        //    this.txtComments.Text = string.Empty;

        //}
        

    }
    protected void dpdType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (dpdType.SelectedItem.Value.ToString() == "0")//Routing
        //{
        //    txtTarget.Visible = false;
        //    lblTarget.Visible = false;
        //    txtProduction.Visible = false;
        //    lblProduction.Visible = false;
        //    dpdNextone.Enabled = true;
        //    Label6.Enabled = true;
        //    Label5.Enabled = true;
        //    Label11.Enabled = false;//penalty status
        //    dpdPenalty.Enabled = false;
        //    txtSpecific.Enabled = true;
        //    dpdStatus.Enabled = true;
        //    Label10.Enabled = false;
        //    lblEnd.Visible = false;
        //    dpdStatus.Enabled = false;
        //}
        //else// Testing/In
        //{
        //    txtTarget.Visible = true;
        //    lblTarget.Visible = true;
        //    txtProduction.Visible = true;
        //    lblProduction.Visible = true;
        //    dpdNextone.Enabled = false;
        //    Label6.Enabled = false;
        //    Label5.Enabled = false;
        //    Label11.Enabled = true;//penalty status
        //    dpdPenalty.Enabled = true;
        //    txtSpecific.Enabled = false;
        //    dpdStatus.Enabled = false;
        //    Label10.Enabled = true;
        //    lblEnd.Visible = false;
        //    dpdStatus.Enabled = true;
        //}
    }
    protected void dpdVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblEnd.Visible = false;
    }
}
