using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;

public partial class RatesUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /*
            if (Request.QueryString["provider"] != null && Request.QueryString["pref"] != null)
            {
                string popupScript = "<script language='JavaScript'>" + "window.open('http://mecca.directo.com/EditRates.aspx?provider=" + Request.QueryString["provider"].ToString() + "&pref=" + Request.QueryString["pref"].ToString() + "', 'CustomPopUp', " + "'width=400, height=250, menubar=yes, resizable=no')" + "</script>";
                ClientScript.RegisterStartupScript(Page.GetType(), "Window", popupScript);
            }                    
    */
        
        
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        string CU, CR, VU, VR;
        bool UP = false;
        CU = "CustUpdate.csv";
        CR = "CustReplace.csv";
        VU = "VendorUpdate.csv";
        VR = "VendorReplace.csv";

        File.Delete(@"E:\Mecca\Files\rates\" + CU);
        File.Delete(@"E:\Mecca\Files\rates\" + CR);
        File.Delete(@"E:\Mecca\Files\rates\" + VU);
        File.Delete(@"E:\Mecca\Files\rates\" + VR);

        try
        {
            string fileName = FU.FileName.ToString();
            Session["fileName"] = fileName;
            if (!FU.HasFile)
            {
                lblUpload.Visible = true;
                lblUpload.Text = "File not loaded";
            }


            if (fileName == "CustUpdate.zip")
            {
                UP = true;
                Session["true"] = 1;
                Session["ww"] = "'CU'";
                //FU.SaveAs(@"E:\Mecca\Files\rates\" + FU.FileName.ToString());
                FU.SaveAs(@"E:\Mecca\Files\rates\Rates.zip");
                lblUpload.Visible = true;
                lblUpload.Text = "File " + FU.FileName.ToString() + " loaded succesfully";

            }
            else if (fileName == "CustReplace.zip")
            {
                UP = true;
                Session["true"] = 1;
                Session["ww"] = "'CR'";
                //FU.SaveAs(@"E:\Mecca\Files\rates\" + FU.FileName.ToString());
                FU.SaveAs(@"E:\Mecca\Files\rates\Rates.zip");
                lblUpload.Visible = true;
                lblUpload.Text = "File " + FU.FileName.ToString() + " loaded succesfully";

            }
            else if (fileName == "VendorUpdate.zip")
            {
                UP = true;
                Session["true"] = 1;
                Session["ww"] = "'VU'";
                //FU.SaveAs(@"E:\Mecca\Files\rates\" + FU.FileName.ToString());
                FU.SaveAs(@"E:\Mecca\Files\rates\Rates.zip");
                lblUpload.Visible = true;
                lblUpload.Text = "File " + FU.FileName.ToString() + " loaded succesfully";

            }
            else if (fileName == "VendorReplace.zip")
            {
                UP = true;
                Session["true"] = 1;
                Session["ww"] = "'VR'";
                //FU.SaveAs(@"E:\Mecca\Files\rates\" + FU.FileName.ToString());
                FU.SaveAs(@"E:\Mecca\Files\rates\Rates.zip");
                lblUpload.Visible = true;
                lblUpload.Text = "File " + FU.FileName.ToString() + " loaded succesfully";

            }
            else if (FU.HasFile)
            {
                lblUpload.Visible = true;
                lblUpload.Text = " This File can't be loaded ";
                Session["true"] = string.Empty;
            }

            if (UP == true)
            {
                
                
                SqlCommand sqlSpQuery = new SqlCommand();
                sqlSpQuery.CommandType = CommandType.StoredProcedure;
                sqlSpQuery.CommandText = "sp_RatesRunDTSX";
                string dd = Session["ww"].ToString();
                sqlSpQuery.Parameters.Add("@who", SqlDbType.VarChar).Value = Session["ww"].ToString();

                DataSet resultSet;
                resultSet = RunQuery(sqlSpQuery);

                if (resultSet.Tables.Count > 0)
                {

                    if (resultSet.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row in resultSet.Tables[1].Rows)
                        {                            
                            string coo;
                            coo = row["corre"].ToString();
                            if (coo != "0") lblUpload.Text = "Failed";
                        }
                    }
                }

                cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter adpD = new cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter();

                
                

                File.Delete(@"E:\Mecca\Files\rates\" + CU);
                File.Delete(@"E:\Mecca\Files\rates\" + CR);
                File.Delete(@"E:\Mecca\Files\rates\" + VU);
                File.Delete(@"E:\Mecca\Files\rates\" + VR);

                cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter adpRR = new cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter();

                GridView1.DataSource = adpRR.GetData();
                GridView1.DataBind();
                if (lblUpload.Text != "Failed")
                {
                    //btnNext.Enabled = true;
                    gvBad.Visible = true;
                    gvGood.Visible = true;
                    gvDupDEl.Visible = true;
                    
                    //Label1.Visible = true;
                    Label2.Visible = true;
                    Label3.Visible = true;
                    Label4.Visible = true;
                    lnknext.Visible = true;

                }
                //btnApply.Visible = false;

            }
            else
            {
                gvBad.Visible = false;
                gvGood.Visible = false;
                gvDupDEl.Visible = false;
                
                //btnNext.Enabled = false;
                lnknext.Visible = false;
                Label1.Visible = false;
                Label2.Visible = false;
                Label3.Visible = false;
                Label4.Visible = false;
            }
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }
    }

    

    protected void next_click(object sender, EventArgs e)
    {
        int i = 0;

        try
        {
            Response.Redirect("http://mecca.directo.com/RatesAssign.aspx");
        }
        catch (Exception ex)
        {
            
        }
    }


    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString = "Data Source=localhost;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
            
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        sqlQuery.CommandTimeout = 666;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception e)
        {
            //labelStatus.Text = "Unable to connect to SQL Server.";
            string exception = e.Message;
        }
        return resultsDataSet;
    }

   
    protected void gvDupDif_RowEditing(object sender, GridViewEditEventArgs e)
    {
        
        UpdatePanel1.UpdateMode = UpdatePanelUpdateMode.Conditional;
        UpdatePanel1.Update();
     
                
    }

    protected void gvDupDif_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        UpdatePanel1.UpdateMode = UpdatePanelUpdateMode.Conditional;
        UpdatePanel1.Update();
        string arguments = e.CommandArgument.ToString();
    }
   
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
        
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string param1 = e.Values[0].ToString();

    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        string providerName = GridView1.DataKeys[e.NewEditIndex].Values[0].ToString();
        Session["providerName"] = providerName;
        
        string prefix = GridView1.DataKeys[e.NewEditIndex].Values[1].ToString();
        Session["prefix"] = prefix;

        string ty = GridView1.DataKeys[e.NewEditIndex].Values[2].ToString();
        Session["ty"] = ty;

        cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter adp = new cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter();
        adp.DeleteQuery(providerName, prefix);

        GridView1.Columns.Clear();
        GridView1.DataSource = adp.GetData();
        GridView1.DataBind();
        UpdatePanel1.Update();

        
        ModalPopupExtender1.Show();                                
    }
    protected void Button2_Click(object sender, EventArgs e)
    {

        cdrdbDatasetTableAdapters.RatesTmpBuenaAdp adp = new cdrdbDatasetTableAdapters.RatesTmpBuenaAdp();
        cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter adp1 = new cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter();

        string newrate = TextBox1.Text;
        string providerName = Session["providerName"].ToString();
        string prefix = Session["prefix"].ToString();
        string ty = Session["ty"].ToString();
        adp.Insert(providerName,prefix,newrate,ty);


        GridView1.DataSource = adp1.GetData();
        GridView1.DataBind();

        gvGood.DataSourceID = "sqlsaGood";
        gvGood.DataBind();
        
    }

    public static void DELETE(string newRate)
    {
        string newrates = newRate;

    }

    protected void Panel1_Load(object sender, EventArgs e)
    {
    }
}

