<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="show.aspx.cs" Inherits="show" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td align="left" colspan="3">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="SteelBlue"
                    Text="Traffic Summary"></asp:Label></td>
            <td align="left" colspan="1" style="width: 85px">
            </td>
            <td align="left" colspan="1" style="width: 86px">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3" style="height: 30px">
            </td>
            <td align="left" colspan="1" style="width: 85px; height: 30px">
            </td>
            <td align="left" colspan="1" style="width: 86px; height: 30px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
               
            </td>
            <td style="width: 84px" valign="top">
            </td>
            <td style="width: 100px" align="left" valign="top">
            
            </td>
            <td align="left" style="width: 85px" valign="top">
            </td>
            <td align="left" style="width: 86px" valign="top">
               
            </td>
        </tr>
    </table>
    <br />
    <br />
    <cc1:TabContainer ID="TabContainer1" runat="server">
    
    
    
    <cc1:TabPanel ID="PanelA" HeaderText=" Live  Traffic " runat="server">
    <ContentTemplate>
    <table>
    <tr>
    
    <td>
    </td>
    
    <td>
    </td>
    
    
    </tr>
    
    <tr>
    
    <td>
    </td>
    
    <td>
    </td>
    
    
    </tr>
    
    
    <tr>
    
    <td valign="top" align="left">
     <asp:GridView ID="gdvCust" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                <Columns>
                
                <asp:BoundField DataField="Cust" HeaderText="Cust" />
                
                <asp:BoundField DataField="Live" HeaderText="Live" />
                
                </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <EditRowStyle BackColor="#999999" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="SteelBlue" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
 
    </td>
    
    <td valign="top" align="right">
        <asp:GridView ID="gdvVend" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                <Columns>
                
                <asp:BoundField DataField="Vend" HeaderText="Vend" />
                <asp:BoundField DataField="Live" HeaderText="Live" />
                
                
                </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <EditRowStyle BackColor="#999999" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="SteelBlue" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
    
    </td>
    
    
    </tr>
    
    
    </table>
    
    
    
    </ContentTemplate>    
    </cc1:TabPanel>
    
    
    
    
    <cc1:TabPanel ID="PanelB" HeaderText=" UK Report " runat="server">
    <ContentTemplate>
    
     <asp:GridView ID="gdvUK" runat="server" CellPadding="4" ForeColor="#333333" AutoGenerateColumns="false" GridLines="None">
                    <Columns>
                    
                    <asp:BoundField DataField="Code" HeaderText="Code" />
                    <asp:BoundField DataField="Ac" HeaderText="AC" />
                    <asp:BoundField DataField="AL" HeaderText="AL" />
                    <asp:BoundField DataField="Rc" HeaderText="Rc" />
                    <asp:BoundField DataField="Min" HeaderText="Min" />
                    <asp:BoundField DataField="ASR" HeaderText="ASR" />
                    <asp:BoundField DataField="ACD" HeaderText="ACD" />
                    
                    
                    
                    
                    
                    </Columns>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <EditRowStyle BackColor="#999999" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="SteelBlue" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
    
    
    </ContentTemplate>    
    </cc1:TabPanel>
    
    
    
    </cc1:TabContainer><br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>

