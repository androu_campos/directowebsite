using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;

public partial class GraphGridV : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String query = null;
        DataTable tbl;
        DataTable tbl1, tbl2;

        string user ;
        
        user = Session["DirectoUser"].ToString();

        if (user == "arturo.marquez")
        {
        }
        else
        {
            Response.Redirect("Home.aspx", false);
        }

        query = "select count(case when lduration > 0 then 1 else 0 end) as Calls, stop [Time] ";
        query += "from cdrdb.dbo.cdrs ";
        query += "where VendorID in (select ip from cdrdb.dbo.providerip where providername like 'TLK-%' AND TYPE = 'V') ";
        query += "and stop >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and stop <= DATEADD(DAY,1,LEFT(GETDATE(),11)) ";
        query += "group by Stop order by stop";
        tbl1 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] ";
        query += "FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Live_Calls] > 0 ";
        query += "and [Cust_Vendor] in (select ip from cdrdb.dbo.providerip where providername like 'TLK-%' AND TYPE = 'V') ";
        query += "and [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) ";
        query += "group by [Time] order by [Time] ";
        tbl2 = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChart1(tbl1,tbl2, "TLK Calls v Ports");

        tbl1.Dispose();
        tbl2.Dispose();

        
    }

    private void makeChart1(DataTable tbl1, DataTable tbl2, string tITLE)
    {
        if (tbl1.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;
            string script2 = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl1.Rows)
            {
                pdx = Chart1.Series["Calls"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Calls"].Points[pdx].MapAreaAttributes = script;
            }

            foreach (DataRow myRow in tbl2.Rows)
            {
                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script2 = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script2 = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script2;
            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Calls"].Points.FindMaxValue("Y");
            DataPoint maxpointY2 = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart1.Titles.Add(tITLE);
            this.Chart1.RenderType = RenderType.ImageMap;

            this.Chart1.Visible = true;
        }
        else
        {
            Session["tblDetail1"] = tbl1;
            Session["tblDetail2"] = tbl2;
        }

    }


}
