<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="AddProvider.aspx.cs" Inherits="AddProvider"
    Title="DIRECTO - Connections Worldwide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td style="width: 160px">
                <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Searh Provider: "
                    Width="106px"></asp:Label>
            </td>
            <td style="width: 103px">
                <asp:TextBox ID="txtIDSearch" runat="server" BorderColor="#E0E0E0" CausesValidation="True"
                    Width="157px" CssClass="labelSteelBlue"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lblSearch" runat="server" CssClass="labelBlue8" Visible="False" Width="219px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td style="width: 103px">
                <asp:Button ID="Button2" runat="server" Text="Search ID" CssClass="boton" 
                    onclick="Button2_Click" />
            </td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td style="width: 160px">
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Create New Provider"
                    Width="106px"></asp:Label>
            </td>
            <td style="width: 103px">
            </td>
            <td style="width: 103px">
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 160px">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Name:"></asp:Label>
            </td>
            <td style="width: 103px">
                <asp:TextBox ID="txtProvider" runat="server" BorderColor="#E0E0E0" CausesValidation="True"
                    Width="157px" CssClass="labelSteelBlue"></asp:TextBox>
            </td>
            <td style="width: 103px">
                <asp:Label ID="lblName" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="The Name contains blank spaces, please verify."
                    Visible="False" Width="244px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 160px; height: 23px">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Type:"></asp:Label>
            </td>
            <td align="left" style="width: 103px; height: 23px">
                <asp:DropDownList ID="dpdType" runat="server" Width="98px" CssClass="dropdown">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="V">Vendor</asp:ListItem>
                    <asp:ListItem Value="N">National</asp:ListItem>
                    <asp:ListItem Value="G">Gateway</asp:ListItem>
                    <asp:ListItem Value="A">Adjacency</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 160px">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="IP:"></asp:Label>
            </td>
            <td style="width: 103px">
                <asp:TextBox ID="txtIP" runat="server" BorderColor="#E0E0E0" Width="156px" CssClass="labelSteelBlue"></asp:TextBox>
            </td>
            <td style="width: 103px">
                <asp:Label ID="lblIp" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="The Ip contains blank spaces, please verify."
                    Visible="False" Width="219px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 160px">
                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Prefix:"></asp:Label>
            </td>
            <td style="width: 103px">
                <asp:TextBox ID="txtPrefix" runat="server" BorderColor="#E0E0E0" Width="156px" CssClass="labelSteelBlue"></asp:TextBox>
            </td>
            <td style="width: 103px">
                <asp:Label ID="lblPrefix" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="The Prefix should be 4 numbers, please verify."
                    Visible="False" Width="219px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 160px">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Adjacency:"></asp:Label>
            </td>
            <td style="width: 103px">
                <asp:TextBox ID="txtAdj" runat="server" BorderColor="#E0E0E0" Width="156px" CssClass="labelSteelBlue"></asp:TextBox>
            </td>
            <td style="width: 103px">
                <asp:Label ID="lblAdj" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="The Adjacency contains blank spaces, please verify."
                    Visible="False" Width="219px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 160px; height: 23px">
            </td>
            <td align="left" style="width: 103px; height: 23px">
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Submit" CssClass="boton" />
            </td>
            <td align="left" style="width: 103px; height: 23px">
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 160px; height: 23px">
            </td>
            <td align="left" style="width: 103px; height: 23px">
                <asp:Label ID="lblEnd" runat="server" CssClass="labelBlue8"></asp:Label>
            </td>
            <td align="left" style="width: 103px; height: 23px">
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="labelBlue8" NavigateUrl="~/AddIp.aspx"
                    Visible="False" Width="210px">Click here to add more Ip's for this Provider</asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>
