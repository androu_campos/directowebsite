using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;

public partial class BlackList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DirectoUser"].ToString() == "santander-demo")
        {

        }
        else
        {
            Response.Redirect("HomeCCCUST.aspx", false);
        }

        if (!IsPostBack)
        {
            DataView dvSql = (DataView)sqldsBlackListCount.Select(DataSourceSelectArguments.Empty);

            foreach (DataRowView drvSql in dvSql)
                txtCount.Text = drvSql["Count"].ToString();
        }
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {

    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        sqldsBlackStatus.DataBind();
        gvGood.Visible = false;
        txtCount.Visible = false;
        gvSearch.Visible = true;
        lblCount.Visible = true;

        if (gvGood.Visible == false || gvSearch.Visible == true)
        {
            DataView dvSql = (DataView)sqldsBlackListCount.Select(DataSourceSelectArguments.Empty);

            foreach (DataRowView drvSql in dvSql)
                txtCount.Text = drvSql["Count"].ToString();
        }

        if (gvSearch.Rows.Count == 0)
        {
            lblError.Visible = true;
            lblError.Text = "Not Found!";
        }
        else
        {
            lblError.Visible = false;
        }

    }
    protected void btnExpCSV_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=BlackList.csv");
            Response.ContentType = "application/text";
            StringBuilder strBr = new StringBuilder();

            if (gvSearch.Visible = true)
            {
                for (int i = 0; i < gvSearch.Columns.Count; i++)
                {
                    strBr.Append(gvSearch.Columns[i].HeaderText + ',');
                }
                strBr.Append("\n");
                for (int j = 0; j < gvSearch.Rows.Count; j++)
                {
                    for (int k = 0; k < gvSearch.Columns.Count; k++)
                    {
                        strBr.Append(gvSearch.Rows[j].Cells[k].Text + ',');
                    }
                    strBr.Append("\n");
                }
                Response.Write(strBr.ToString());
                Response.Flush();
                Response.End();
            }
            else
            {
                for (int i = 0; i < gvGood.Columns.Count; i++)
                {
                    strBr.Append(gvGood.Columns[i].HeaderText + ',');
                }
                strBr.Append("\n");
                for (int j = 0; j < gvGood.Rows.Count; j++)
                {
                    for (int k = 0; k < gvGood.Columns.Count; k++)
                    {
                        strBr.Append(gvGood.Rows[j].Cells[k].Text + ',');
                    }
                    strBr.Append("\n");
                }
                Response.Write(strBr.ToString());
                Response.Flush();
                Response.End();
            }
        }
        catch (Exception ex) { string message = ex.Message.ToString(); }
    }
}


