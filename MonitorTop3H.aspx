<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="MonitorTop3H.aspx.cs" Inherits="MonitorAllAccessUnl" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager id="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <br />
    <%--<asp:UpdatePanel id="UpdatePanel1" runat="server" >
        <contenttemplate>--%>
        <table>
            <TR>
                <TD vAlign=top align=left colspan="3">
                    <asp:Label id="Label1" runat="server" Text="Advanced QMonitor Top3 History" CssClass="labelTurk"></asp:Label>
                </TD>
            </TR>
            <tr>
                <td style="width: 100px" valign="top" align="left">
                    <asp:Label ID="labelFrom" runat="server" Text="From:" Font-Bold="False" CssClass="labelSteelBlue" /></td>
                <td style="width: 300px" valign="top" align="left">
                    <asp:TextBox ID="txtFromV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                    &nbsp;&nbsp;<asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" />
                </td>
            </tr>
            <tr>
                <td style="width: 100px" valign="top" align="left">
                    <asp:Label ID="labelTo" runat="server" Text="To:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                <td style="width: 300px" valign="top" align="left">
                    <asp:TextBox ID="txtToV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                    &nbsp;&nbsp;<asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            </tr>            
        </table>
<TABLE align=left><TBODY><TR><TD vAlign=top align=left>&nbsp;<TABLE id="tblQuery"><TBODY>

<TR><TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0"><asp:Label id="Label5" runat="server" Text="Customer" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:DropDownList id="dpdCustomer" runat="server" CssClass="dropdown"></asp:DropDownList></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0"><asp:Label id="Label6" runat="server" Text="Vendor:" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:DropDownList id="dpdVendor" runat="server" CssClass="dropdown"></asp:DropDownList></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0"><asp:Label id="Label7" runat="server" Text="OriginationIP" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:DropDownList id="dpdOriginationIp" runat="server" CssClass="dropdown"></asp:DropDownList></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0"><asp:Label id="Label8" runat="server" Text="TerminationIP" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:DropDownList id="dpdTermination" runat="server" CssClass="dropdown"></asp:DropDownList></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0"><asp:Label id="Label9" runat="server" Text="Country" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:DropDownList id="dpdCountry" runat="server" CssClass="dropdown"></asp:DropDownList></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0"><asp:Label id="Label10" runat="server" Text="Region" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:DropDownList id="dpdRegion" runat="server" CssClass="dropdown"></asp:DropDownList></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0"><asp:Label id="Label11" runat="server" Text="Type" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:DropDownList id="dpdType" runat="server" CssClass="dropdown"></asp:DropDownList></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0"><asp:Label id="Label12" runat="server" Text="Class" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:DropDownList id="dpdClass" runat="server" CssClass="dropdown"></asp:DropDownList></TD><TD style="WIDTH: 100px"></TD></TR>
    <tr>
        <td align="center" bgcolor="#d5e3f0" style="width: 100px">
            <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="LMC"></asp:Label></td>
        <td style="width: 100px">
            <asp:DropDownList ID="dpdLMC" runat="server" CssClass="dropdown">
            </asp:DropDownList></td>
        <td style="width: 100px">
        </td>
    </tr>
    <TR><TD style="WIDTH: 100px" align="center" bgcolor="#d5e3f0"><asp:Label id="Label13" runat="server" Text="Source MSW" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:DropDownList id="dpdSource" runat="server" CssClass="dropdown"></asp:DropDownList></TD><TD style="WIDTH: 100px"></TD></TR>
    <tr>
        <td align="center" bgcolor="#d5e3f0" style="width: 100px; height: 22px;">
            <asp:Label ID="Label14" runat="server" CssClass="labelBlue8" Text="Gateway"></asp:Label></td>
        <td style="width: 100px; height: 22px;">
            <asp:DropDownList ID="dpdGateway" runat="server" CssClass="dropdown">
            </asp:DropDownList></td>
        <td style="width: 100px; height: 22px;">
        </td>
    </tr>
    <TR><TD style="WIDTH: 100px"><asp:Button id="cmdSearch" onclick="cmdSearch_Click" runat="server" Text="View report" CssClass="boton"></asp:Button></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR></TBODY></TABLE> </TD></TR></TBODY></TABLE>
<%--</contenttemplate>
    </asp:UpdatePanel>--%>
    <asp:HiddenField ID="txtFrom" runat="server" />
    <asp:HiddenField ID="txtTo" runat="server" />
    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromV"
        PopupButtonID="Image1">
    </cc1:CalendarExtender>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToV"
        PopupButtonID="Image2">
    </cc1:CalendarExtender>
</asp:Content>

