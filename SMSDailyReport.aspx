<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="SMSDailyReport.aspx.cs" Inherits="SMSDailyReport" 
Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" EnableEventValidation="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<div>
    <table>
        <tr>
            <td style="width: 71px;">
                <asp:Label ID="labelFrom" runat="server" Text="From:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
            <td style="width: 100px" valign="top" align="left">
                <asp:TextBox ID="txtFromV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"
                    AutoPostBack="True" MaxLength="10" OnTextChanged="txtFromV_TextChanged"></asp:TextBox></td>
            <td style="width: 27px">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
        </tr>
        <tr>
            <td style="width: 71px;">
                <asp:Label ID="labelTo" runat="server" Text="To:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
            <td style="width: 100px; height: 24px;">
                <asp:TextBox ID="txtToV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"
                    AutoPostBack="True" MaxLength="10" OnTextChanged="txtToV_TextChanged"></asp:TextBox></td>
            <td style="width: 27px;">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCustomer" runat="server" Text="Customer:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlCustomer" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblVendor" runat="server" Text="Vendor:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlVendor" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left">                
                <asp:Button ID="btnSubmit" runat="server" Text="View report" OnClick="btnSubmit_Click" Font-Names="Arial" CssClass="boton" />
            </td>
            <td style="width: 100px" align="left">                
                <asp:Button ID="btnExport" runat="server" Text="Export Excel" OnClick="btnExport_Click" Font-Names="Arial" CssClass="boton" Visible=false />
            </td>
        </tr>
    </table>
</div>
<div>
    <table>
        <tr>
            <td>
                <asp:GridView ID="gdvGlobalResume" runat="server" Font-Names="Arial" Font-Size="8pt" Font-Underline="False"
                            Width="100%" >
                <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                            <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                            <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                            <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial"
                                Font-Bold="True" ForeColor="Orange"></HeaderStyle>
                            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                </asp:GridView>
            </td>
        </tr>
    </table>
</div>

<asp:HiddenField ID="txtFrom" runat="server" />
<asp:HiddenField ID="txtTo" runat="server" />

<cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtFromV" runat="server"
    PopupButtonID="Image1">
</cc1:CalendarExtender>
<cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToV"
    PopupButtonID="Image2">
</cc1:CalendarExtender>

</asp:Content>

