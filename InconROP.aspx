<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="InconROP.aspx.cs" Inherits="InconROP" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    
    <table style="position:absolute; top:220px; left:230px;">
    <tr>
    <td>
        <asp:Label ID="Label2" runat="server" CssClass="labelTurk" Text="Provider IP Inconsistencies Viewer" Width="200px"></asp:Label>
    </td>
    </tr>
        <tr>
            <td style="height: 23px">
            </td>
        </tr>
    
    <tr>
        <td align="left" valign="top">
            <asp:GridView ID="gdvRop" runat="server" Font-Names="Arial" Font-Size="8pt">
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                <EditRowStyle BackColor="#2461BF" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                    Font-Size="8pt" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </td>
    </tr>
    </table>
</asp:Content>

