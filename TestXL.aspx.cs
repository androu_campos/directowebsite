using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Reflection;
using Excel;
using System.IO;
using System.Text;
using System.Data.SqlClient;

public partial class TestXL : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {

        this.CreateExcelWorkbook("MYEXCEL");
    }

    private void CreateExcelWorkbook(string spName)
    {
        string strCurrentDir = Server.MapPath(".") + "\\";
//        RemoveFiles(strCurrentDir); // utility method to clean up old files			
        Excel.Application oXL;
        Excel._Workbook oWB;
        Excel._Worksheet oSheet;
        Excel._Worksheet oSheet1;
        Excel._Worksheet oSheet2;
        Excel._Worksheet oSheet3;
        Excel._Worksheet oSheet4;
        Excel._Worksheet oSheet5;
        
        Excel.Range oRng;
        Excel.Range oRng1;
        Excel.Range oRng2;
        Excel.Range oRng3;
        Excel.Range oRng4;
        Excel.Range oRng5;
        
        try
        {
            GC.Collect();// clean up any other excel guys hangin' around...
            oXL = new Excel.Application();
            oXL.Visible = false;
            //Get a new workbook.
            oWB = (Excel._Workbook)(oXL.Workbooks.Add(Missing.Value));
            //agrega la hoja 4
            oWB.Sheets.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            //agrega la hoja 5
            oWB.Sheets.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            //agrega la hoja 6
            oWB.Sheets.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);


            oSheet = (Excel._Worksheet)oWB.ActiveSheet;
            oSheet1 = (Excel._Worksheet)oWB.Sheets[2];
            oSheet2 = (Excel._Worksheet)oWB.Sheets[3];
            oSheet3 = (Excel._Worksheet)oWB.Sheets[4];
            oSheet4 = (Excel._Worksheet)oWB.Sheets[5];
            oSheet5 = (Excel._Worksheet)oWB.Sheets[6];

            //get Data
            DataSet dts1 = (DataSet)Util.RunQueryByStmntatCDRDB("Select * from CDRDB..Diff_1 WHERE Date >= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 00:00:00.000' AND Date <= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 23:59:59.000'");
            System.Data.DataTable tbl = dts1.Tables[0];
                        
            oSheet.Cells[1, 1] = "Date";
            oSheet.Cells[1, 2] = "Attempts";
            oSheet.Cells[1, 3] = "Calls";
            oSheet.Cells[1, 4] = "Minutes";
            oSheet.Cells[1, 5] = "T";

            for (int i = 2; i <= tbl.Rows.Count+1; i++)//por cada renglon
            {
                for (int j = 1; j < tbl.Columns.Count; j++)//por cada celda
                {
                    oSheet.Cells[i, j] = tbl.Rows[i-2][j].ToString(); 
                }
            }
//Table 2
            oSheet1.Cells[1, 1] = "Date_C";
            oSheet1.Cells[1, 2] = "segM4";
            oSheet1.Cells[1, 3] = "segM3";
            oSheet1.Cells[1, 4] = "MinM4";
            oSheet1.Cells[1, 5] = "MinM3";
            oSheet1.Cells[1, 6] = "Match";


            DataSet dts2 = (DataSet)Util.RunQueryByStmntatCDRDB("Select * from CDRDB..Diff_2 WHERE Date_C >= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 00:00:00.000' AND Date_C <= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 23:59:59.000'");
            System.Data.DataTable tbl2 = dts2.Tables[0];
            for (int i = 2; i <= tbl2.Rows.Count+1; i++)//por cada renglon
            {
                for (int j = 1; j < tbl2.Columns.Count; j++)//por cada celda
                {
                    oSheet1.Cells[i, j] = tbl2.Rows[i - 2][j].ToString();
                }
            }
//Table 3

            oSheet2.Cells[1, 1] = "Date_C";
            oSheet2.Cells[1, 2] = "segM4";
            oSheet2.Cells[1, 3] = "segM3";
            oSheet2.Cells[1, 4] = "MinM4";
            oSheet2.Cells[1, 5] = "MinM3";
            oSheet2.Cells[1, 6] = "Match";

            DataSet dts3 = (DataSet)Util.RunQueryByStmntatCDRDB("Select * from CDRDB..Diff_3 WHERE Date_C >= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 00:00:00.000' AND Date_C <= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 23:59:59.000'");
            System.Data.DataTable tbl3 = dts3.Tables[0];
            for (int i = 2; i <= tbl3.Rows.Count+1; i++)//por cada renglon
            {
                for (int j = 1; j < tbl3.Columns.Count; j++)//por cada celda
                {
                    oSheet2.Cells[i, j] = tbl3.Rows[i - 2][j].ToString();
                }
            }
//Table 4

            oSheet3.Cells[1, 1] = "Date_C";
            oSheet3.Cells[1, 2] = "Match";
            oSheet3.Cells[1, 3] = "M3";
            oSheet3.Cells[1, 4] = "M4";

            DataSet dts4 = (DataSet)Util.RunQueryByStmntatCDRDB("Select * from CDRDB..Diff_4 WHERE Date_C >= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 00:00:00.000' AND Date_C <= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 23:59:59.000'");
            System.Data.DataTable tbl4 = dts4.Tables[0];            
            for (int i = 2; i <= tbl3.Rows.Count+1; i++)//por cada renglon
            {
                for (int j = 1; j < tbl4.Columns.Count; j++)//por cada celda
                {
                    oSheet3.Cells[i, j] = tbl3.Rows[i - 2][j].ToString();
                }
            }
//Table 5
            oSheet4.Cells[1,1]="OriginationID";
            oSheet4.Cells[1,2]="m_sIPAddress";
            oSheet4.Cells[1,3]="m_sRemoteIPAddress";
            oSheet4.Cells[1,4]="m_sConnectionId";
            oSheet4.Cells[1,5]="m_sCallingStationId";
            oSheet4.Cells[1,6]="m_sCalledStationID";
            oSheet4.Cells[1,7]="stop";
            oSheet4.Cells[1,8]="start";
            oSheet4.Cells[1,9]="lDuration";
            oSheet4.Cells[1,10]="m_dwDisconnectReason";
            oSheet4.Cells[1,11]="TerminationID";
            oSheet4.Cells[1,12]="VendorID";
            oSheet4.Cells[1,13]="PDD";
            oSheet4.Cells[1, 14] = "Match";


            DataSet dts5 = (DataSet)Util.RunQueryByStmntatCDRDB("Select * from CDRDB..cdrs_Diff_5 WHERE stop >= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 00:00:00.000' AND stop <= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 23:59:59.000'");
            System.Data.DataTable tbl5 = dts5.Tables[0];            
            for (int i = 2; i <= tbl5.Rows.Count; i++)//por cada renglon
            {
                for (int j = 1; j <= tbl5.Columns.Count; j++)//por cada celda
                {
                    oSheet4.Cells[i, j] = tbl5.Rows[i - 1][j-1].ToString();
                }
            }
//Table 6            
            oSheet5.Cells[1,1] = "Gateway";
            oSheet5.Cells[1,2] = "Track";
            oSheet5.Cells[1,3] = "Date_C";
            oSheet5.Cells[1,4] = "Duration";
            oSheet5.Cells[1,5] = "VendorID";

            DataSet dts6 = (DataSet)Util.RunQueryByStmntatCDRDB("Select * from CDRDB..Diff_6 WHERE Date_C >= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 00:00:00.000' AND Date_C <= '" + DateTime.Now.AddDays(-1).ToShortDateString() + " 23:59:59.000'");
            System.Data.DataTable tbl6 = dts6.Tables[0];
            for (int i = 2; i < tbl6.Rows.Count+1; i++)//por cada renglon
            {
                for (int j = 1; j < tbl6.Columns.Count; j++)//por cada columna
                {
                    oSheet5.Cells[i, j] = tbl6.Rows[i - 2][j].ToString();
                }
            }
            
            //Format A1:Z1 as bold, vertical alignment = center.
            oSheet.get_Range("A1", "Z1").Font.Bold = true;
            oSheet.get_Range("A1", "Z1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

            oSheet1.get_Range("A1", "Z1").Font.Bold = true;
            oSheet1.get_Range("A1", "Z1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

            oSheet2.get_Range("A1", "Z1").Font.Bold = true;
            oSheet2.get_Range("A1", "Z1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

            oSheet3.get_Range("A1", "Z1").Font.Bold = true;
            oSheet3.get_Range("A1", "Z1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            
            oSheet4.get_Range("A1", "Z1").Font.Bold = true;
            oSheet4.get_Range("A1", "Z1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

            oSheet5.get_Range("A1", "Z1").Font.Bold = true;
            oSheet5.get_Range("A1", "Z1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

            //AutoFit columns A:Z.
            oRng = oSheet.get_Range("A1", "Z1");
            oRng.EntireColumn.AutoFit();
            //AutoFit columns A:Z.
            oRng1 = oSheet1.get_Range("A1", "Z1");
            oRng1.EntireColumn.AutoFit();
            //AutoFit columns A:Z.
            oRng2 = oSheet1.get_Range("A1", "Z1");
            oRng2.EntireColumn.AutoFit();
            //AutoFit columns A:Z.
            oRng3 = oSheet1.get_Range("A1", "Z1");
            oRng3.EntireColumn.AutoFit();
            //AutoFit columns A:Z.
            oRng4 = oSheet1.get_Range("A1", "Z1");
            oRng4.EntireColumn.AutoFit();
            //AutoFit columns A:Z.
            oRng5 = oSheet1.get_Range("A1", "Z1");
            oRng5.EntireColumn.AutoFit();

            oXL.Visible = false;
            oXL.UserControl = false;
            string strFile = "report_" + System.DateTime.Now.AddDays(-1).ToShortDateString() + ".xls";
            strFile = strFile.Replace("/", "-");

            oWB.SaveAs(strCurrentDir + "Diffs_M3_M4\\" + strFile, Excel.XlFileFormat.xlWorkbookNormal, null, null, false, false, Excel.XlSaveAsAccessMode.xlShared, false, false, null, null, null);
            // Need all following code to clean up and extingush all references!!!
            oWB.Close(null, null, null);
            oXL.Workbooks.Close();

            oXL.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRng);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRng1);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRng2);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRng3);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRng4);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRng5);            
            
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oXL);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oSheet);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oSheet1);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oSheet2);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oSheet3);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oSheet4);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oSheet5);
            
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oWB);
            oSheet = null;
            oWB = null;
            oXL = null;
            GC.Collect();  // force final cleanup!
            string strMachineName = Request.ServerVariables["SERVER_NAME"];
//            errLabel.Text = "<A href=http://" + strMachineName + "/ExcelGen/" + strFile + ">Download Report</a>";
        }
        catch (Exception theException)
        {
            String errorMessage;
            errorMessage = "Error: ";
            errorMessage = String.Concat(errorMessage, theException.Message);
            errorMessage = String.Concat(errorMessage, " Line: ");
            errorMessage = String.Concat(errorMessage, theException.Source);
//            errLabel.Text = errorMessage;
        }
    }
        
}
