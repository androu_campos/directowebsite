<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="CatsEye.aspx.cs" Inherits="CatsEye" Title="DIRECTO - Connections Worldwide" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    Under construction
        <table id="tblMain">
            <tr>
                <td align="left" height="40" valign="top" width="30%">
                    </td>
                <td align="left" style="height: 25px" valign="top" width="30%">
                    <asp:SqlDataSource ID="SqlCountryM" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                        SelectCommand="SELECT CustBroker AS Broker FROM MECCA2..CBroker &#13;&#10;WHERE CustBroker <> 'CT' and CustBroker <> 'N/A' and CustBroker <> 'marc' &#13;&#10;and CustBroker <> 'Fabian' &#13;&#10;UNION &#13;&#10;SELECT VendorBroker AS Broker FROM MECCA2..VBroker &#13;&#10;WHERE VendorBroker <> 'N/A' and VendorBroker <> 'CT' and VendorBroker <> 'marc' &#13;&#10;and VendorBroker <> 'Fabian' &#13;&#10;order by Broker  ">
                    </asp:SqlDataSource>
                </td>
                <td align="left" style="height: 25px" valign="top" width="30%">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
            </tr>
        </table>
    <br />
    <table>
        <tr>
            <td align="left" valign="top" width="100%">
                <table>
                    <tr>
                        <td align="left" style="width: 100px" valign="top">
                            <asp:Label ID="Label23" runat="server" CssClass="labelTurk" Text="Cat's Eye"></asp:Label></td>
                        <td align="left" style="width: 100px" valign="top">
                        </td>
                        <td align="left" style="width: 100px" valign="top">
                        </td>
                        <td align="left" style="width: 100px" valign="top">
                        </td>
                        <td align="left" style="width: 100px" valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px" align="left" valign="top">
                            <asp:Label ID="lblSelectBroker" runat="server" CssClass="labelBlue8" Text="Select Broker:" Width="79px"></asp:Label></td>
                        <td style="width: 100px" align="left" valign="top">
                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="dropdown" DataSourceID="SqlCountryM"
                                DataTextField="Broker" DataValueField="Broker">
                            </asp:DropDownList></td>
                        <td style="width: 100px" align="left" valign="top">
                            <asp:Button ID="cmdReport" runat="server" CssClass="boton" OnClick="cmdReport_Click"
                                Text="View Report" Width="75px" /></td>
                        <td align="left" style="width: 100px" valign="top">
                    <asp:UpdateProgress id="UpdateProgress1" runat="server" DisplayAfter="500">
                        <progresstemplate>
<TABLE><TBODY><TR><TD style="WIDTH: 100px"><asp:Label id="Label10" runat="server" Text="Loading Please Wait..." Width="110px" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:Image id="Image3" runat="server" ImageUrl="~/Images/Loading.gif"></asp:Image></TD></TR></TBODY></TABLE>
</progresstemplate>
                    </asp:UpdateProgress></td>
                        <td align="left" style="width: 100px" valign="top">
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 100px" align="left" valign="top">
                    </td>
        </tr>
        <tr>
            <td align="left" width="100%">

                
    <asp:UpdatePanel ID="UpdParent" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
<TABLE id="Maintbl"><TBODY><TR><TD vAlign=top align=left width=700 colSpan=3 height=30></TD><TD></TD><TD width=300>
    <asp:HyperLink ID="lnkHistory" runat="server" CssClass="labelBlue8" NavigateUrl="~/HistoryCommissions.aspx"
        Width="126px">See History Commissions</asp:HyperLink></TD></TR><TR><TD style="BORDER-RIGHT: #d5e3f0 2px solid; BORDER-TOP: #d5e3f0 2px solid; BORDER-LEFT: #d5e3f0 2px solid; BORDER-BOTTOM: #d5e3f0 2px solid" vAlign=top align=left width=300 height=10><TABLE><TBODY><TR><TD style="WIDTH: 100px"><asp:Label id="Label9" runat="server" Text="DESTINATIONS" CssClass="labelTurk"></asp:Label></TD></TR></TBODY></TABLE></TD><TD style="WIDTH: 100px" vAlign=top align=left height=10></TD><TD vAlign=top align=left width=300 height=10></TD><TD vAlign=top align=left height=10></TD><TD style="WIDTH: 100px" vAlign=top align=left height=10></TD></TR><TR><TD vAlign=top align=center width=450 bgColor=#d5e3f0><asp:Label id="lblMonthTo" runat="server" Text="Month to Date" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" vAlign=top align=left></TD><TD vAlign=top align=center width=450 bgColor=#d5e3f0><asp:Label id="lblYesterday" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD><TD vAlign=top align=left></TD><TD vAlign=top align=center width=450 bgColor=#d5e3f0><asp:Label id="lblBefYesterday" runat="server" Text="Label" CssClass="labelBlue8">
                                    </asp:Label></TD></TR><TR><TD style="WIDTH: 100px" vAlign=top><asp:GridView id="GridView4" runat="server" ForeColor="#333333" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False" CellPadding="4">
<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="Country" HeaderText="DESTINATION">
<ItemStyle Width="25%"></ItemStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="VMinutes" HeaderText="Minutes">
<ItemStyle Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Profit">
<ItemStyle Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="TotalCost" HeaderText="Revenues">
<ItemStyle Width="15%"></ItemStyle>
</asp:BoundField>
</Columns>

<RowStyle BackColor="#EFF3FB"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle ForeColor="Orange" CssClass="titleOrangegrid" Font-Size="8pt" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>&nbsp; </TD><TD style="WIDTH: 100px" vAlign=top align=left></TD><TD style="WIDTH: 100px" vAlign=top align=left><asp:GridView id="GridView5" runat="server" ForeColor="#333333" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False" CellPadding="4">
<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="Country" HeaderText="DESTINATION">
<ItemStyle Width="25%"></ItemStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="VMinutes" HeaderText="Minutes">
<ItemStyle Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Profit">
<ItemStyle Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="TotalCost" HeaderText="Revenues">
<ItemStyle Width="15%"></ItemStyle>
</asp:BoundField>
</Columns>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Left"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle ForeColor="Orange" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>&nbsp; </TD><TD vAlign=top align=left></TD><TD style="WIDTH: 100px" vAlign=top align=left><asp:GridView id="GridView6" runat="server" ForeColor="#333333" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False" CellPadding="4">
<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="Country" HeaderText="DESTINATION">
<ItemStyle Width="25%"></ItemStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="VMinutes" HeaderText="Minutes">
<ItemStyle Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Profit">
<ItemStyle Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="TotalCost" HeaderText="Revenues">
<ItemStyle Width="15%"></ItemStyle>
</asp:BoundField>
</Columns>

<RowStyle BackColor="#EFF3FB"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle ForeColor="Orange" CssClass="titleOrangegrid" Font-Size="8pt" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>&nbsp; </TD></TR><TR><TD width=450 height=25></TD><TD width=200 height=25></TD><TD width=450 height=25></TD><TD height=25></TD><TD width=450 height=25></TD></TR><TR><TD style="BORDER-RIGHT: #d5e3f0 2px solid; BORDER-TOP: #d5e3f0 2px solid; BORDER-LEFT: #d5e3f0 2px solid; WIDTH: 100px; BORDER-BOTTOM: #d5e3f0 2px solid" width=450 height=25><TABLE><TBODY><TR><TD style="WIDTH: 100px" align=left><asp:Label id="Label4" runat="server" Text="HISTORY" CssClass="labelTurk">
                                                    </asp:Label></TD></TR></TBODY></TABLE></TD><TD width=200 height=25></TD><TD width=450 height=25></TD><TD height=25></TD><TD width=450 height=25></TD></TR><TR><TD align=center width=450 bgColor=#d5e3f0 height=25><asp:Label id="Label5" runat="server" Text="Customers" CssClass="labelBlue8">
                                    </asp:Label></TD><TD width=200 height=25></TD><TD align=center width=450 bgColor=#d5e3f0 height=25><asp:Label id="Label6" runat="server" Text="Vendors" CssClass="labelBlue8">
                                    </asp:Label></TD><TD height=25></TD><TD align=center width=450 bgColor=#d5e3f0 height=25><asp:Label id="Label22" runat="server" Text="ALL" CssClass="labelBlue8"></asp:Label></TD></TR><TR><TD width=450 height=25><asp:GridView id="gdvHistCust" runat="server" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False">
<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="MonthN" HeaderText="Month"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="CMinutes" HeaderText="CustomerMinutes"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Profit"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Revenues" HeaderText="Revenues"></asp:BoundField>
</Columns>

<RowStyle BackColor="#D5E3F0" HorizontalAlign="Center"></RowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" CssClass="titleOrangegrid" HorizontalAlign="Center" Font-Size="8pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD width=200 height=25></TD><TD width=450 height=25><asp:GridView id="gdvHistVend" runat="server" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False">
<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="MonthN" HeaderText="Month"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="VMinutes" HeaderText="VendorMinutes"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Profit"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Revenues" HeaderText="Revenues"></asp:BoundField>
</Columns>
<RowStyle BackColor="#D5E3F0" HorizontalAlign="Center"></RowStyle>
<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
<HeaderStyle Height="40px" CssClass="titleOrangegrid" HorizontalAlign="Center" Font-Size="8pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>
<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD height=25></TD><TD vAlign=top align=left width=450 height=25><asp:GridView id="gdvHistALL" runat="server" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False">
<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="MonthN" HeaderText="Month"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="CMinutes" HeaderText="Total Minutes"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Total Profit"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Revenues" HeaderText="Revenues"></asp:BoundField>
</Columns>

<RowStyle BackColor="#D5E3F0" HorizontalAlign="Center"></RowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" CssClass="titleOrangegrid" HorizontalAlign="Center" Font-Size="8pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD width=450 height=25></TD><TD width=200 height=25></TD><TD width=450 height=25></TD><TD height=25></TD><TD width=450 height=25></TD></TR><TR><TD style="BORDER-RIGHT: #d5e3f0 2px solid; BORDER-TOP: #d5e3f0 2px solid; BORDER-LEFT: #d5e3f0 2px solid; BORDER-BOTTOM: #d5e3f0 2px solid" borderColor=#d5e3f0 width=450><TABLE height="100%"><TBODY><TR><TD style="WIDTH: 100px" align=left><asp:Label id="Label2" runat="server" Text="VENDORS" CssClass="labelTurk">
                                                    </asp:Label></TD></TR></TBODY></TABLE></TD><TD width=200></TD><TD width=450></TD><TD></TD><TD width=450></TD></TR><TR><TD vAlign=top align=center width=450 bgColor=#d5e3f0><asp:Label id="Label11" runat="server" Text="Month to Date" CssClass="labelBlue8"></asp:Label></TD><TD vAlign=top align=left width=200></TD><TD vAlign=top align=center width=450 bgColor=#d5e3f0><asp:Label id="lblVndYest" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD><TD vAlign=top align=left></TD><TD vAlign=top align=center width=450 bgColor=#d5e3f0><asp:Label id="lblVndBef" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD></TR><TR><TD vAlign=top align=left width=450><asp:UpdatePanel id="UpdVndMont" runat="server"><ContentTemplate>
<asp:GridView id="gdvVendorsMonth" runat="server" ForeColor="Black" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False" AllowPaging="True" PageSize="1" CellPadding="4">
<PagerSettings Visible="False"></PagerSettings>

<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="Vendor" HeaderText="Vendor"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="VMinutes" HeaderText="Minutes"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Profit"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="TotalCost" HeaderText="Projected Profit"></asp:BoundField>
</Columns>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" ForeColor="Orange" CssClass="titleOrangegrid" HorizontalAlign="Center" Font-Size="8pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Button id="cmdUpdVndMont" onclick="cmdUpdVndMont_Click" runat="server" Text="See Detail" Width="54px" CssClass="boton"></asp:Button> <asp:Button id="cmdUpdVndMontH" onclick="cmdUpdVndMontH_Click" runat="server" Text="Hide Details" Width="54px" Visible="False" CssClass="boton"></asp:Button><BR />
</ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID="cmdUpdVndMont"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="cmdUpdVndMontH"></asp:AsyncPostBackTrigger>
</Triggers>
</asp:UpdatePanel> <BR /></TD><TD vAlign=top align=left width=200></TD><TD vAlign=top align=left width=450><asp:UpdatePanel id="UpdVndYest" runat="server" UpdateMode="Conditional"><ContentTemplate>
<asp:GridView id="gdvVendorsYest" runat="server" ForeColor="Black" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False" AllowPaging="True" PageSize="1" CellPadding="4">
<PagerSettings Visible="False"></PagerSettings>

<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="Vendor" HeaderText="Vendor"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="VMinutes" HeaderText="Minutes"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Profit"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="TotalCost" HeaderText="Delta"></asp:BoundField>
</Columns>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle BackColor="White" Height="40px" ForeColor="Orange" CssClass="titleOrangegrid" HorizontalAlign="Center" Font-Size="8pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Button id="cmdUpdVndYest" onclick="cmdUpdVndYest_Click" runat="server" Text="See Detail" Width="54px" CssClass="boton"></asp:Button>&nbsp;<asp:Button id="cmdUpdVndYestH" onclick="cmdUpdVndYestH_Click" runat="server" Text="Hide Detail" Width="54px" Visible="False" CssClass="boton"></asp:Button> 
</ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID="cmdUpdVndYest"></asp:AsyncPostBackTrigger>
</Triggers>
</asp:UpdatePanel> <BR /></TD><TD vAlign=top align=left></TD><TD vAlign=top align=left width=450><asp:UpdatePanel id="UpdVndPrev" runat="server"><ContentTemplate>
<asp:GridView id="gdvVendorsBeforeY" runat="server" ForeColor="Black" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False" AllowPaging="True" PageSize="1" CellPadding="4">
<PagerSettings Visible="False"></PagerSettings>

<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="Vendor" HeaderText="Vendor"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="VMinutes" HeaderText="Minutes"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Profit"></asp:BoundField>
</Columns>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" ForeColor="Orange" CssClass="titleOrangegrid" HorizontalAlign="Center" Font-Size="8pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Button id="cmdUpdVndPrev" onclick="cmdUpdVndPrev_Click" runat="server" Text="See Detail" Width="54px" CssClass="boton"></asp:Button> <asp:Button id="cmdUpdVndPrevH" onclick="cmdUpdVndPrevH_Click" runat="server" Text="Hide Detail" Width="54px" Visible="False" CssClass="boton"></asp:Button> 
</ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID="cmdUpdVndPrev"></asp:AsyncPostBackTrigger>
</Triggers>
</asp:UpdatePanel><BR /></TD></TR><TR><TD style="HEIGHT: 17px" width=450></TD><TD style="HEIGHT: 17px" width=200></TD><TD style="HEIGHT: 17px" width=450></TD><TD style="HEIGHT: 17px"></TD><TD style="HEIGHT: 17px" width=450></TD></TR><TR><TD width=450></TD><TD width=200></TD><TD width=450></TD><TD></TD><TD width=450></TD></TR><TR><TD style="BORDER-RIGHT: #d5e3f0 2px solid; BORDER-TOP: #d5e3f0 2px solid; BORDER-LEFT: #d5e3f0 2px solid; BORDER-BOTTOM: #d5e3f0 2px solid" width=450><TABLE><TBODY><TR><TD style="WIDTH: 100px" align=left><asp:Label id="Label3" runat="server" Text="CUSTOMERS" CssClass="labelTurk">
                                                    </asp:Label></TD></TR></TBODY></TABLE></TD><TD width=200></TD><TD width=450></TD><TD></TD><TD width=450></TD></TR><TR><TD vAlign=top align=center width=450 bgColor=#d5e3f0><asp:Label id="Label12" runat="server" Text="Month to Date" CssClass="labelBlue8"></asp:Label></TD><TD vAlign=top align=left width=200></TD><TD vAlign=top align=center width=450 bgColor=#d5e3f0><asp:Label id="lblCstYest" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD><TD vAlign=top align=left></TD><TD vAlign=top align=center width=450 bgColor=#d5e3f0><asp:Label id="lblCstBef" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD></TR><TR><TD style="HEIGHT: 91px" vAlign=top align=left width=450><asp:UpdatePanel id="UpdCstMonth" runat="server"><ContentTemplate>
<asp:GridView id="gdvCustMonth" runat="server" ForeColor="#333333" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False" AllowPaging="True" PageSize="1" CellPadding="4">
<PagerSettings Visible="False"></PagerSettings>

<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="Customer" HeaderText="Customer"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="CMinutes" HeaderText="Minutes"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Profit"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="TotalSales" HeaderText="Projected Profit"></asp:BoundField>
</Columns>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" ForeColor="Orange" CssClass="titleOrangegrid" HorizontalAlign="Center" Font-Size="8pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Button id="cmdUpdCstMonth" onclick="cmdUpdCstMonth_Click" runat="server" Text="See Detail" Width="54px" CssClass="boton"></asp:Button> <asp:Button id="cmdUpdCstMonthH" onclick="cmdUpdCstMonthH_Click" runat="server" Text="Hide Detail" Width="54px" Visible="False" CssClass="boton"></asp:Button>
</ContentTemplate>
</asp:UpdatePanel><BR />&nbsp;</TD><TD style="HEIGHT: 91px" vAlign=top align=left width=200></TD><TD style="HEIGHT: 91px" vAlign=top align=left width=450><asp:UpdatePanel id="UpdCstYest" runat="server"><ContentTemplate>
<asp:GridView id="gdvCustYest" runat="server" ForeColor="Black" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False" AllowPaging="True" PageSize="1" CellPadding="4">
<PagerSettings Visible="False"></PagerSettings>

<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="Customer" HeaderText="Customer"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="CMinutes" HeaderText="Minutes"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Profit"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="TotalSales" HeaderText="Delta"></asp:BoundField>
</Columns>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" ForeColor="Orange" CssClass="titleOrangegrid" HorizontalAlign="Center" Font-Size="8pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Button id="cmdUpdCstYest" onclick="cmdUpdCstYest_Click" runat="server" Text="See Detail" Width="54px" CssClass="boton"></asp:Button> <asp:Button id="cmdUpdCstYestH" onclick="cmdUpdCstYestH_Click" runat="server" Text="Hide Detail" Width="54px" Visible="False" CssClass="boton"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><BR />&nbsp;</TD><TD style="HEIGHT: 91px" vAlign=top align=left></TD><TD style="HEIGHT: 91px" vAlign=top align=left width=450><asp:UpdatePanel id="UpdCstBef" runat="server"><ContentTemplate>
<asp:GridView id="gdvCustBeforeY" runat="server" ForeColor="Black" Font-Size="8pt" Font-Names="Arial" Width="300px" AutoGenerateColumns="False" AllowPaging="True" PageSize="1" CellPadding="4">
<PagerSettings Visible="False"></PagerSettings>

<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="Customer" HeaderText="Customer"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0.00}" DataField="CMinutes" HeaderText="Minutes"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:0,0.00}" DataField="Profit" HeaderText="Profit"></asp:BoundField>
</Columns>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" ForeColor="Orange" CssClass="titleOrangegrid" HorizontalAlign="Center" Font-Size="8pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Button id="cmdUpdCstBef" onclick="cmdUpdCstBef_Click" runat="server" Text="See Detail" Width="54px" CssClass="boton"></asp:Button> <asp:Button id="cmdUpdCstBefH" onclick="cmdUpdCstBefH_Click" runat="server" Text="Hide detail" Width="54px" Visible="False" CssClass="boton"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><BR />&nbsp;</TD></TR><TR><TD style="BORDER-TOP-WIDTH: 1px; BORDER-LEFT-WIDTH: 1px; BORDER-LEFT-COLOR: #d5e3f0; BORDER-BOTTOM-WIDTH: 1px; BORDER-BOTTOM-COLOR: #d5e3f0; BORDER-TOP-COLOR: #d5e3f0; BORDER-RIGHT-WIDTH: 1px; BORDER-RIGHT-COLOR: #d5e3f0" vAlign=top align=left width=450></TD><TD vAlign=top align=left width=450></TD><TD vAlign=top align=left width=450></TD><TD></TD><TD width=450></TD></TR><TR><TD vAlign=top align=left width=450><TABLE><TBODY><TR><TD align=center width="20%"><asp:Label id="Label14" runat="server" Text="TOTAL" ForeColor="Orange" CssClass="labelBlue8"></asp:Label></TD><TD align=center width="25%"><asp:Label id="Label15" runat="server" Text="Minutes" ForeColor="Orange" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="Label16" runat="server" Text="Profit" ForeColor="Orange" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="Label17" runat="server" Text="Projected Profit" ForeColor="Orange" CssClass="labelBlue8"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=center><asp:Label id="lblMontCustTot" runat="server" Text="**ALL**" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="lblMontMinTot" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD><TD align=center width="25%"><asp:Label id="lblMontProfTot" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD><TD align=center width="30%"><asp:Label id="lblMontPPTot" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD></TR></TBODY></TABLE></TD><TD vAlign=top align=left width=450 bgColor=#d5e3f0></TD><TD vAlign=top align=left width=450><TABLE><TBODY><TR><TD style="WIDTH: 100px" align=center><asp:Label id="Label13" runat="server" Text="TOTAL" ForeColor="Orange" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="Label18" runat="server" Text="Minutes" ForeColor="Orange" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="Label19" runat="server" Text="Profit" ForeColor="Orange" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="Label20" runat="server" Text="Total Delta" ForeColor="Orange" CssClass="labelBlue8"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=center><asp:Label id="Label21" runat="server" Text="**ALL**" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="lblYestMinTot" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="lblYestProfTot" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="lblYestPPTot" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD></TR></TBODY></TABLE></TD><TD bgColor=#d5e3f0></TD><TD width=450><TABLE><TBODY><TR><TD style="WIDTH: 100px" align=center><asp:Label id="Label25" runat="server" Text="TOTAL" ForeColor="Orange" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="Label26" runat="server" Text="Minutes" ForeColor="Orange" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="Label27" runat="server" Text="Profit" ForeColor="Orange" CssClass="labelBlue8"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=center><asp:Label id="Label29" runat="server" Text="**ALL**" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="lblBefMinTot" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" align=center><asp:Label id="lblBefProfTot" runat="server" Text="Label" CssClass="labelBlue8"></asp:Label></TD></TR></TBODY></TABLE></TD></TR><TR><TD vAlign=top align=left width=450></TD><TD vAlign=top align=left width=450></TD><TD vAlign=top align=left width=450></TD><TD></TD><TD width=450></TD></TR></TBODY></TABLE>
</contenttemplate>
 <Triggers >
<asp:AsyncPostBackTrigger ControlID="cmdReport"></asp:AsyncPostBackTrigger>
</Triggers>
    </asp:UpdatePanel>                
                <br />                                
            </td>
            <td align="left" style="width: 100px" valign="top">
            </td>
        </tr>
    </table>
    <br />
<br />
    <table id="tblFooter" width="100%">
        <tr>
            <td colspan="3">
                    <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" /></td>
        </tr>
        <tr>
            <td style="width: 100px">
                    <table>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label8" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                    Font-Names="Arial" Font-Size="8pt" Width="98%"></asp:Label></td>
                            <td style="width: 100px">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                    Width="146px" Visible="True" /></td>
                        </tr>
                    </table>
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>

</asp:Content>
