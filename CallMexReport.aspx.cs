using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;

public partial class ICSReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = "";
        DataSet ds = null;
        DataTable tbl;

        string user;

        user = Session["DirectoUser"].ToString();

        if (user == "amarquez" || user == "randrade" || user == "jsaenz" || user == "mvieyra")
        {

            if (!Page.IsPostBack)
            {
                txtFromV.Text = DateTime.Now.AddDays(-1).ToShortDateString();
                txtToV.Text = DateTime.Now.AddDays(-1).ToShortDateString();

                sql = "SELECT LMC, SUM(CUSTMINUTES) Minutes, Cost, ";
                sql += "round(SUM(CUSTMINUTES) / (select sum(custminutes) From mecca2.dbo.trafficlog where customer like 'ICS-CALLMEX' and BillingDate BETWEEN DATEADD(DAY,-1,LEFT(GETDATE(),11))AND DATEADD(DAY,1,LEFT(GETDATE(),11))) * 100,1) as 'Porc' ";
	            sql += "FROM mecca2.dbo.trafficlog ";
	            sql += "WHERE customer like 'ICS-CALLMEX' ";
	            sql += "AND BillingDate >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and BillingDate <= DATEADD(DAY,1,LEFT(GETDATE(),11)) ";
	            sql += "GROUP BY LMC, Cost ";
	            sql += "HAVING sum(custminutes) > 0 ";
                sql += "ORDER BY sum(custminutes) desc ";

                tbl = Util.RunQueryByStmntatCDRDB(sql).Tables[0];

                makeChart1(tbl, "ALL CALLMEX");

                tbl.Dispose();


            }
        }
        else
        {
            Response.Redirect("HomeR.aspx", false);
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        string sql = "";

        DataTable tbl;

        string from = txtFromV.Text;
        string to = txtToV.Text;


        if (chkVend.Checked == true)
        {
            sql = "select LMC, Vendor, SUM(CUSTMINUTES) Minutes, Cost, ";
            sql += "round(SUM(CUSTMINUTES) / (select sum(custminutes) From mecca2.dbo.trafficlog where customer like 'ICS-CALLMEX' and BillingDate BETWEEN '" + from + "' AND '" + to + "') * 100,1) as 'Porc' ";
            sql += "From mecca2.dbo.trafficlog ";
            sql += "where customer like 'ICS-CALLMEX' ";
            sql += "AND BillingDate between '" + from + "' and '" + to + "' "; ;
            sql += "group by LMC, Vendor, Cost ";
            sql += "having sum(custminutes) > 0 ";
            sql += "order by sum(custminutes) desc ";

        }
        else
        {
            sql = "SELECT LMC, SUM(CUSTMINUTES) Minutes, Cost, ";
            sql += "round(SUM(CUSTMINUTES) / (select sum(custminutes) From mecca2.dbo.trafficlog where customer like 'ICS-CALLMEX' and BillingDate BETWEEN '" + from + "' AND '" + to + "') * 100,1) as 'Porc' ";
            sql += "FROM mecca2.dbo.trafficlog ";
            sql += "WHERE customer like 'ICS-CALLMEX' ";
            sql += "AND BillingDate between '" + from + "' and '" + to + "' ";
            sql += "GROUP BY LMC, Cost ";
            sql += "HAVING sum(custminutes) > 0 ";
            sql += "ORDER BY sum(custminutes) desc ";
        }

        //lblPrueba.Text = sql;
        //lblPrueba.Visible = true;

        tbl = Util.RunQueryByStmntatCDRDB(sql).Tables[0];

        makeChart1(tbl, "ALL CALLMEX");

        tbl.Dispose();

    }

    private void makeChart1(DataTable tbl, string tITLE)
    {

        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            foreach (DataRow myRow in tbl.Rows)
            {
                if (chkVend.Checked == true)
                {
                    pdx = Chart1.Series["LMC"].Points.AddXY(myRow["Vendor"], Convert.ToDouble(myRow["Porc"]));
                    script = "onclick=dispData('" + myRow["Vendor"].ToString() + "','" + myRow["Porc"] + "','P')";
                    script = script.Replace(" ", "");
                    this.Chart1.Series["LMC"].Points[pdx].MapAreaAttributes = script;

                }
                else
                {
                    pdx = Chart1.Series["LMC"].Points.AddXY(myRow["LMC"], Convert.ToDouble(myRow["Porc"]));
                    script = "onclick=dispData('" + myRow["LMC"].ToString() + "','" + myRow["Porc"] + "','P')";
                    script = script.Replace(" ", "");
                    this.Chart1.Series["LMC"].Points[pdx].MapAreaAttributes = script;
                }
                
            }

            if (chkVend.Checked == true)
            {
                this.Chart1.Titles.Add("TLK-TLMX-TLCL");
            }
            else
            {
                this.Chart1.Titles.Add(tITLE);
            }

            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart1.Visible = true;
            
        }
        else
        {
            this.Chart1.Visible = true;
            Session["tblDetail1"] = tbl;
        }

    }
    protected void cmdDetail_Click(object sender, EventArgs e)
    {
        if (chkVend.Checked == true)
        {
            gdvVendor.DataSource = ((DataTable)Session["tblDetail1"]);
            gdvVendor.DataBind();
            gdvVendor.Visible = true;
            gdvDetail.Visible = false;

        }
        else
        {
            gdvDetail.DataSource = ((DataTable)Session["tblDetail1"]);
            gdvDetail.DataBind();
            gdvDetail.Visible = true;
            gdvVendor.Visible = false;
        }
        

        cmdHide.Visible = true;
        cmdDetail.Visible = true;

    }
    protected void cmdHide_Click(object sender, EventArgs e)
    {
        gdvDetail.Visible = false;
        cmdDetail.Visible = true;
        cmdHide.Visible = false;

    }
}
