using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

public partial class MonitoringSearch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            //VENDOR
            SqlConnection SqlConn = new SqlConnection();
            SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
            SqlCommand SqlCommand = new SqlCommand("SELECT DISTINCT ProviderName from ProviderIP WHERE Type like 'V' UNION SELECT '**ALL**' as ProviderName from ProviderIP",SqlConn);
            SqlConn.Open();
            SqlDataReader myReader = SqlCommand.ExecuteReader();
            while(myReader.Read())
            {
                dpdVendor.Items.Add(myReader.GetValue(0).ToString());
            }
            myReader.Close();
            SqlConn.Close();
            //NOCUSER
            SqlConnection SqlConn1 = new SqlConnection();
            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
            SqlCommand SqlCommand1 = new SqlCommand("SELECT DISTINCT username from login WHERE Rol like 'NOC' UNION SELECT '**ALL**' as username from login", SqlConn1);
            SqlConn1.Open();
            SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
            while (myReader1.Read())
            {
             dpdUser.Items.Add(myReader1.GetValue(0).ToString());
            }
            myReader1.Close();
            SqlConn1.Close();
            //**/
            MECCA2TableAdapters.MonitoringAdp adp = new MECCA2TableAdapters.MonitoringAdp();
            MECCA2.MonitoringDataTable tbl = adp.GetData();

            gdvResults.DataSource = adp.GetDataByDesc();
            Session["SearchMonitoring"] = adp.GetDataByDesc();
            gdvResults.DataBind();
            
            txtDate.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();
        }
        else
        {
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        
        string date1 = txtDate.Text + " 00:00:00";
        DateTime date = Convert.ToDateTime(date1);
        
        
        string date2 = txtTo.Text + " 23:59:59";
        DateTime dat2 = Convert.ToDateTime(date2);


        StringBuilder type = new StringBuilder();
        type.Append(dpdType.SelectedValue.ToString());
        StringBuilder vendor = new StringBuilder();
        vendor.Append(dpdVendor.SelectedValue.ToString());
        //StringBuilder nextone = new StringBuilder();
        //nextone.Append(dpdNextone.SelectedValue.ToString());
        StringBuilder status = new StringBuilder();
        status.Append(dpdStatus.SelectedValue.ToString());
        StringBuilder nocUser = new StringBuilder();
        nocUser.Append(dpdUser.SelectedValue.ToString());
        StringBuilder penaltyStatus = new StringBuilder();
        //penaltyStatus.Append(dpdPenalty.SelectedValue.ToString());


        //Type
            type.Replace("**ALL**","%");
        //Vendor
            vendor.Replace("**ALL**", "%");
        //Nextone
            //nextone.Replace("ALL", "%");
        //Status
            status.Replace("**ALL**", "%");
        //NOCUSER
            nocUser.Replace("**ALL**", "%");
        //Penalty
            penaltyStatus.Replace("**ALL**", "%");
        //date//


        MECCA2TableAdapters.MonitoringAdp adp = new MECCA2TableAdapters.MonitoringAdp();
        //for count if > 0
        MECCA2.MonitoringDataTable tbl = adp.GetDataByAll(vendor.ToString(), "%", "%", "%", "%", "%", "%", status.ToString(), nocUser.ToString(), date, penaltyStatus.ToString(), "%", type.ToString());


        MECCA2.MonitoringDataTable tbl2 = adp.GetDataByDates(vendor.ToString(), "%", "%", "%", "%", "%", "%", status.ToString(), nocUser.ToString(), date,dat2, penaltyStatus.ToString(), "%", type.ToString());

        gdvResults.DataSource = adp.GetDataByDates(vendor.ToString(), "%", "%", "%", "%", "%", "%", status.ToString(), nocUser.ToString(), date, dat2, penaltyStatus.ToString(), "%", type.ToString());

//        gdvResults.DataSource = adp.GetDataByAll(vendor.ToString(), "%", "%", nextone.ToString(), "%", "%", "%", status.ToString(), nocUser.ToString(), date, penaltyStatus.ToString(), "%", type.ToString());
//        Session["SearchMonitoring"] = adp.GetDataByAll(vendor.ToString(), "%", "%", nextone.ToString(), "%", "%", "%", status.ToString(), nocUser.ToString(), date, penaltyStatus.ToString(), "%", type.ToString());
        gdvResults.DataBind();
        

        if (tbl2.Rows.Count < 1)
        {
            lblNothing.Visible = true;
            gdvResults.Visible = false;          
        }
        else
        {
            lblNothing.Visible = false;
            gdvResults.Visible = true;
        }


    }

    private System.Data.DataSet RunQuery(StringBuilder qry, string StringConnection)
    {

        System.Data.SqlClient.SqlCommand sqlQuery = new System.Data.SqlClient.SqlCommand();
        sqlQuery.CommandText = qry.ToString();


        string connectionString = ConfigurationManager.ConnectionStrings
        [StringConnection].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void dpdType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (dpdType.SelectedValue.ToString() == "1")//Routing/Port Change
        //{
        //    lblPenalty.Enabled = false;
        //    dpdPenalty.Enabled = false;
        
        //}
        //else if (dpdType.SelectedValue.ToString() == "2")
        //{
        //    lblPenalty.Enabled = true;
        //    dpdPenalty.Enabled = true;
        //}
        //else
        //{
        //    lblPenalty.Enabled = false;
        //    dpdPenalty.Enabled = false;
        //}

    }

    protected void gdvResults_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        
    }
    protected void gdvResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        
        MECCA2.MonitoringDataTable dtEmployee = ((MECCA2.MonitoringDataTable)Session["SearchMonitoring"]);
        gdvResults.DataSource = dtEmployee;
        gdvResults.PageIndex = e.NewPageIndex;
        gdvResults.DataBind();
    }
}
