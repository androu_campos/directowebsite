using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class GraphDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            Chart1.RenderType = Dundas.Charting.WebControl.RenderType.BinaryStreaming;
            if (Request["Point"] != null)
            {
                // Use the "Point" parameter to load drilled-down data:
                string query = "SELECT * FROM LiveTrafficLog WHERE Updated=" + Request["Point"];
                SqlConnection connection = new SqlConnection("Data Source=172.27.27.30; Initial Catalog=MECCA2; Integrated Security=true");
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader reader = command.ExecuteReader();

                Chart1.Series[0].Points.DataBind(reader, "Time", "Calls", null);
            }
        }
        catch (Exception ex)
        {
            Chart1.Titles.Add(ex.Message);
            
        }


    }    

}
