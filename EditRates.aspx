<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditRates.aspx.cs" Inherits="EditRates" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Edit Rates</title>
    
<script type="text/javascript" language="javascript">
function retornaValor()
{
if(document.getElementById('txtNewRate').value == "")
{
if(dialogArguments == "")
returnValue = "";
else
returnValue = dialogArguments;
}

}
</script>
</head>
<body onunload="retornaValor();">
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="Label4" runat="server" Text="New rate for:"></asp:Label></td>
                <td style="width: 100px">
                    <asp:Label ID="lblProviderName" runat="server" Text="Provider Name"></asp:Label></td>
                <td style="width: 100px">
                    <asp:Label ID="lblPrefix" runat="server" Text="Prefix"></asp:Label></td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtNewRate" runat="server"></asp:TextBox></td>
                <td style="width: 100px">
                    <asp:Button ID="cmdUpdate" runat="server" OnClick="cmdUpdate_Click" Text="Fix" /></td>
            </tr>
            <tr>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:GridView ID="gdvRrateDup" runat="server">
                    </asp:GridView>
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
            </tr>
            <tr>
                <td colspan="3">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
