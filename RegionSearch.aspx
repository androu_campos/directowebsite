<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="RegionSearch.aspx.cs" Inherits="RegionSearch" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="position:absolute; top:220px; left:230px;">
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Code:"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="TextBox1" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 100px">
                <asp:Button ID="cmdSearch" runat="server" Text="Search" CssClass="boton" OnClick="cmdSearch_Click" /></td>
        </tr>
        <tr>
            <td style="width: 100px; height: 21px;">
            </td>
            <td style="width: 100px" rowspan="2">
                <asp:GridView ID="GridView1" AllowPaging="true" PageSize="10" AutoGenerateColumns="false" runat="server" Font-Names="Arial" Font-Size="8pt" RowStyle-Wrap="false" OnPageIndexChanging="GridView1_PageIndexChanging">
                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                    <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                    <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                    <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial"
                        Font-Bold="True"></HeaderStyle>
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                    <Columns>
                        <asp:BoundField DataField="Country" HeaderText="Country" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Region" HeaderText="Region" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Type" HeaderText="Type" ItemStyle-Wrap="false"  ItemStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Code" HeaderText="Code" ItemStyle-Wrap="false"  ItemStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Rounding" HeaderText="Rounding" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="LMC" HeaderText="LMC" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Class" HeaderText="Class" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Left" />
                    </Columns>
                </asp:GridView>
                
            </td>
            <td style="width: 100px; height: 21px;">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
    <asp:Label ID="Label2" runat="server" CssClass="labelTurkH" Text="Region Search"></asp:Label>


</asp:Content>

