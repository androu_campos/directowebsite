using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class DIRECTO_TopNumbers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Value = DateTime.Today.AddDays(-1).ToShortDateString();
            txtTo.Value = DateTime.Today.AddDays(-1).ToShortDateString();


            txtToV.Text = txtTo.Value.ToString();
            txtFromV.Text = txtFrom.Value.ToString();
            SqlConnection SqlConn = new SqlConnection();
            SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";
            SqlCommand SqlCommand7 = new SqlCommand("SELECT DISTINCT Class FROM Regions WHERE Class is not NULL UNION SELECT '** NONE **' AS Class FROM Regions UNION SELECT '**ALL**' AS Class FROM Regions ORDER BY Class", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader7 = SqlCommand7.ExecuteReader();
            while (myReader7.Read())
            {
                dpdClass.Items.Add(myReader7.GetValue(0).ToString());
            }
            myReader7.Close();
            SqlConn.Close();

            TextBox1.Text = "0";
            TextBox2.Text = "0";

        }

    }

    protected void ddlCust_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlCust.SelectedItem.ToString() == "**ALL**") hfCust.Value = "%";
        else hfCust.Value = ddlCust.SelectedItem.ToString();

    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlVendor.SelectedItem.ToString() == "**ALL**") hfVendor.Value = "%";
        else hfVendor.Value = ddlVendor.SelectedItem.ToString();

    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {

        string Coun;

        if (ddlCountry.SelectedItem.ToString() == "**ALL**") Coun = "%";
        else Coun = ddlCountry.SelectedItem.ToString();
        hfCountry.Value = Coun;
        ddlRegion.Enabled = true;
        ddlRegion.Items.Clear();
        sqldsRegion.SelectCommand = "SELECT  '**ALL**' as Region FROM CDRDB..Regions UNION SELECT DISTINCT Region FROM CDRDB..Regions UNION SELECT '** NONE **' as Region FROM CDRDB..Regions WHERE Country like '" + Coun + "' ORDER BY Region";

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
       
        

        



        ///**/
        string SelectString, WhereString, FromString, FromSBro = "", GroupByString, UnionString, QQ, sOrder, BrokerWhere;
        string sF, sW, sS, sSt, sG, sSUM = "";
        bool bCV = false, bVB = false;

        ////checando valores no asignados
        if (ddlRegion.Enabled == false)
        {
            hfRegion.Value = "%";
        }
        else
        {
            if (ddlRegion.SelectedValue.ToString() == "**ALL**") hfRegion.Value = "%";
            else if (ddlRegion.SelectedValue.ToString() == "** NONE **") hfRegion.Value = "%";
            else hfRegion.Value = ddlRegion.SelectedItem.ToString();
        }

        if (ddlCountry.SelectedItem.ToString() == "**ALL**") hfCountry.Value = "%";
        else if (ddlCountry.SelectedItem.ToString() == "** NONE **") hfCountry.Value = "%";
        else hfCountry.Value = ddlCountry.SelectedItem.ToString();

        if (hfCust.Value.ToString() == "") hfCust.Value = "%";
        if (hfVendor.Value.ToString() == "") hfVendor.Value = "%";
        
        ////FIN checando valores no asignados

        SelectString = "";
        BrokerWhere = "";
        FromString = " FM2TL";

        WhereString = "";

        GroupByString = "";
        UnionString = "";

        ////WHERE Builder  
        string from = txtFromV.Text;
        string to = txtToV.Text;


        WhereString = WhereString + "'" + from + "' AND '" + to + "'";
        if (hfCust.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Customer LIKE '" + hfCust.Value.ToString() + "'";
        }
        if (hfVendor.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Vendor LIKE '" + hfVendor.Value.ToString() + "'";
        }
        if (hfCountry.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Country LIKE '" + hfCountry.Value.ToString() + "'";
        }
        if (hfRegion.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Region LIKE '" + hfRegion.Value.ToString() + "'";
        }
        
        if (dpdType.SelectedValue.ToString() != "** NONE **" && dpdType.SelectedValue.ToString() != "**ALL**")
        {
            WhereString = WhereString + " AND T.Type LIKE '" + dpdType.SelectedValue.ToString() + "'";            

        }

        if (TextBox3.Text != "")
        {
            WhereString = WhereString +  " AND T.DialedNumber LIKE '%" + TextBox3.Text + "%'";
        }
       
        ////FIN WHERE Builder

        
        //    SelectString = SelectString + ",CONVERT(VARCHAR(11) ,T.Billingdate,101) as BillingDate";
        //    GroupByString = GroupByString + ",T.BillingDate";
        //    UnionString = UnionString + ",'** ALL **' AS BillingDate";

        if (ddlCust.SelectedItem.ToString() != "** NONE **")
        {
            SelectString = SelectString + ",T.Customer";
            GroupByString = GroupByString + ",T.Customer";
            UnionString = UnionString + ",'****' AS Customer";
        }

        SelectString = SelectString + ",T.DialedNumber";
        GroupByString = GroupByString + ",T.DialedNumber";
        UnionString = UnionString + ",'****' AS DialedNumber";

        if (ddlVendor.SelectedItem.ToString() != "** NONE **")
        {
            SelectString = SelectString + ",T.Vendor";
            GroupByString = GroupByString + ",T.Vendor";
            UnionString = UnionString + ",'****' AS Vendor";
        }

        if (ddlCountry.SelectedItem.ToString() != "** NONE **")
        {
            SelectString = SelectString + ",T.Country";
            GroupByString = GroupByString + ",T.Country";
            UnionString = UnionString + ",'****' as Country";
        }

        if (ddlRegion.SelectedItem.ToString() != "** NONE **")
        {
            SelectString = SelectString + ",T.Region";
            GroupByString = GroupByString + ",T.Region";
            UnionString = UnionString + ",'****' as Region";
        }

        int icount = 0;


        if (dpdType.SelectedValue.ToString() != "** NONE **")//TYPE
        {
            SelectString = SelectString + ",T.Type";
            GroupByString = GroupByString + ",T.Type";
            UnionString = UnionString + ",'****' as [Type]";
            //FromString = FromString + " LJM2R";
            //icount = icount + 1;
        }



        ////Arriba con group
        ////Sin group abajo

       sSUM = sSUM + ",ROUND(SUM(T.Minutes),4) as CMinutes";
       sSUM = sSUM + ",Count(*) as Calls";






       sS = "";
       sOrder = "";
       if (SelectString.Length > 3)
       {
           sS = SelectString.Substring(1);
           sOrder = " ORDER BY " + sS.Replace("CONVERT(VARCHAR(11) ,T.Billingdate,101) as", " ");
       }
       else
       {
           if (sSUM.Length > 3)
           {
               sSUM = sSUM.Substring(1);
           }
       }
        
        string sHaving = "";

       if (TextBox1.Text != "0")
       {
           sHaving = " HAVING SUM(Minutes) < " + TextBox1.Text + " ";
       }
       else if (TextBox2.Text != "0")
       {
           sHaving = " HAVING SUM(Minutes) > " + TextBox2.Text + " ";
       }
       else
           sHaving  = " HAVING SUM(Minutes) > 0 ";

       sG = string.Empty;

       sF = FromString + FromSBro;
       sW = WhereString;
       if (GroupByString.Length > 3)
       {
           sG = " GROUP BY disputes.dbo.getMonth(billingdate), " + GroupByString.Substring(1);
       }
       sSt = "";
       if (UnionString.Length > 1)
       {
           sSt = UnionString.Substring(1);
       }


       sF = sF.Replace("FM2TL", "FROM DISPUTES..TopNumbersReport T");
        //sF = sF.Replace("LJM2CB", "LEFT JOIN MECCA2..CBroker B ON T.Customer = B.Customer");
        //sF = sF.Replace("LJM2VB", "LEFT JOIN MECCA2..VBroker V ON T.Vendor = V.Vendor");
       //sF = sF.Replace("LJM2R", "LEFT JOIN MECCA2.dbo.Regions R ON T.RegionPrefix = R.Code");

       QQ = "sS=" + sS + "&sF=" + sF + "&sW=" + sW + "&sG=" + sG + "&sSt=" + sSt + "&sSUM=" + sSUM;

       string Query = " SELECT disputes.dbo.getMonth(billingdate) as Month, " + sS + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT '******' as Month, " + sSt + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + sHaving + sOrder;


      
       SqlCommand sqlSpQuery = new SqlCommand();
       sqlSpQuery.CommandTimeout = 350;
       sqlSpQuery.CommandType = System.Data.CommandType.Text;

       sqlSpQuery.CommandText = Query;
       DataSet ResultSet;
       ResultSet = Util.RunQuery(sqlSpQuery);

       Session["TopNum"] = ResultSet;

       if (ResultSet.Tables.Count > 0)
       {
           gdvRecent.DataSource = ResultSet.Tables[0];
           gdvRecent.DataBind();
           gdvRecent.Visible = true;
           Button1.Visible = true;
       }
       else
       {
           gdvRecent.Visible = false;
           Button1.Visible = false;
       }


        ////Query = Query.Replace("SELECT ,", "SELECT ");
        //Session["SalesLogS"] = Query;
        ////Response.Cookies["MSLH"].Value = Query;  
        //if ((sS.Length + sSUM.Length) > 6)
        //{
        //    Response.Redirect("~/MSLH.aspx", false);
        //}

    }
    protected void rdbtnlType_SelectedIndexChanged(object sender, EventArgs e)
    {

        //***

    }
    protected void MECCA_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("http://mecca3.computer-tel.com/MECCA/");
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect("MasterSalesLogH.aspx");
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    
    protected void Button1_Click(object sender, EventArgs e)
    {

    }

    protected void gdvRecent_Changing(object sender, GridViewPageEventArgs e)
    {
        gdvRecent.PageIndex = e.NewPageIndex;
        gdvRecent.DataBind();
    }

    protected void btnExcel_Click(object sende, EventArgs e)
    {
        try
        {

            DataTable dtEmployee = ((DataSet)Session["TopNum"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "TopNum" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
}
