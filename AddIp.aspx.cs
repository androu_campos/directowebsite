using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class AddIp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)//Agrega una nueva Ip al Provider seleccionado, en la tabla ProviderIp
    {
        if (!IsPostBack)
        {

            cdrdbDatasetTableAdapters.ProviderAddIpTableAdapter adp = new cdrdbDatasetTableAdapters.ProviderAddIpTableAdapter();
            cdrdbDataset.ProviderAddIpDataTable tbl = adp.GetData();

            int count = tbl.Rows.Count;

            for (int i = 0; i < count; i++)
            {
                dpdProvider.Items.Add(tbl.Rows[i]["providerName"].ToString());

            }

        }

        if (Request.QueryString.Count > 0)
        {
            string ip = Request.QueryString[0].ToString();
            txtIP.Text = ip;
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        cdrdbDatasetTableAdapters.QueriesTableAdapter adp = new cdrdbDatasetTableAdapters.QueriesTableAdapter();
        //inserta la IP en ProviderIp
        adp.InsertAddIp(dpdProvider.SelectedValue.ToString(), txtIP.Text, dpdType.SelectedValue.ToString(),Session["DirectoUser"].ToString(),DateTime.Now);
        //borra el registro de UnknownIDs
        cdrdbDatasetTableAdapters.QueriesTableAdapter adpD = new cdrdbDatasetTableAdapters.QueriesTableAdapter();
        adpD.deleteUnknownQry(txtIP.Text);                
        lblMessage.Visible = true;
        Button1.Enabled = false;
        if (dpdType.SelectedValue == "C")
        {
            loadRates(sender, e);
        }
    }

    protected void loadRates(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=Pricing;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand cmd = new SqlCommand("sp_LoadSTRates", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        conn.Open();
        cmd.Parameters.Add(new SqlParameter("@Id", txtIP.Text));

        try
        {

            cmd.ExecuteNonQuery();
            conn.Close();
        }
        catch (Exception ex)
        {
            
        }


        

    }

    
}
