using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class RatesAssign : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //PopulateOptions(NewNode);
        if (!Page.IsPostBack)
        {

            
        }
    }

    
    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString =
            ConfigurationManager.ConnectionStrings
            ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
            //labelStatus.Text = "Unable to connect to SQL Server.";
        }
        return resultsDataSet;
    }


    
    protected void btnApply_Click(object sender, EventArgs e)
    {
        
        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;
        sqlSpQuery.CommandText = "sp_RateSummaryDetectShedule";      
        sqlSpQuery.Parameters.Add("@from", SqlDbType.SmallDateTime).Value = txtFrom.Text;
        sqlSpQuery.Parameters.Add("@to", SqlDbType.SmallDateTime).Value = txtTo.Text;
        DataSet resultSet;
        resultSet = RunQuery(sqlSpQuery);
        SqlDsratesAlready.SelectCommand = "SELECT [ID],[ProviderName],[Rates],[Applyday],[Uploaded] FROM [CDRDB].[dbo].[RateCSchedules] Order by ApplyDay";
        Sqlvenven.SelectCommand = "SELECT [ID],[ProviderName],[Rates],[Applyday],[Uploaded] FROM [CDRDB].[dbo].[RateVSchedules] Order by ApplyDay";
        SqlClog.SelectCommand = "SELECT [CustName],COUNT(*) as Rates,[ApplyDay],[Modified]  FROM [CDRDB].[dbo].[CustRateSchedulesLog] WHERE ApplyDay BETWEEN '" + txtFrom.Text + "' AND DATEADD(day,-1,LEFT(GETDATE(),11)) GROUP BY [CustName],[ApplyDay],[Modified] ORDER BY ApplyDay,CustName";
        SqlVlog.SelectCommand = "SELECT [VendorName],COUNT(*) as Rates,[ApplyDay],[Modified]   FROM [CDRDB].[dbo].[VendorRateSchedulesLog] WHERE ApplyDay BETWEEN '" + txtFrom.Text + "' AND DATEADD(day,-1,LEFT(GETDATE(),11)) GROUP BY [VendorName],[ApplyDay],[Modified] ORDER BY ApplyDay,VendorName";
        gvAlready.Visible = true;
        GridView1.Visible = true;
        Label3.Visible = true;
        Label5.Visible = true;
        Label6.Visible = true;
        Label7.Visible = true;

    }
        

}
