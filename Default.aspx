﻿<%@ Page Language="C#" AutoEventWireup="true" EnableTheming="true" StylesheetTheme="Theme1" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div>
            <br />
            <br />
            <br />
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>&nbsp;<br />
            <asp:TreeView ID="MeccaTree" runat="server" ExpandDepth="2" Font-Bold="True" Font-Size="Small"
                ForeColor="#000040" NodeIndent="0" PathSeparator="." SkipLinkText="">
                <HoverNodeStyle Font-Bold="True" ForeColor="LightGray" />
                <SelectedNodeStyle CssClass="SelectedNodeStyle" Font-Bold="True" Font-Size="Medium"
                    ForeColor="Gray" />
                <ParentNodeStyle BackColor="White" BorderColor="GrayText" BorderStyle="Dashed" BorderWidth="1px"
                    Width="180px" />
                <RootNodeStyle BackColor="#D5E3F0" BorderColor="GrayText" BorderWidth="1px" ChildNodesPadding="5px"
                    Font-Names="Arial" Font-Size="8pt" ForeColor="#004A8F" HorizontalPadding="10px"
                    VerticalPadding="5px" Width="220px" />
                <LeafNodeStyle BackColor="White" BorderColor="Silver" BorderStyle="Dotted" BorderWidth="1px"
                    ChildNodesPadding="2px" Font-Italic="False" Font-Names="Arial" Font-Size="7.8pt"
                    Font-Strikeout="False" Font-Underline="False" ForeColor="#8EB9DF" HorizontalPadding="12px"
                    ImageUrl="~/Pag/imas/int/f-azul.gif" Width="240px" />
            </asp:TreeView>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" FilterType="Numbers" runat="server" TargetControlID="TextBox1">
            </cc1:FilteredTextBoxExtender>
            </div>
    </form>
</body>
</html>
