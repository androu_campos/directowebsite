using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;


public partial class DIDMonitor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = "";
        DataSet ds = null;



        sql = "Select Customer, sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
        + " SUM(Calls)) as ACD "
        + " from [ICS].[dbo].OpSheetAll "        
        + " group by Customer "
        + "UNION "
        + "SELECT '** ALL **', sum(ISNULL(Minutes,0)) as TotalMinutes,  "
        + "sum(Attempts) as Attempts, "
        + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + "mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + "mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
        + "mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
        + "from [ICS].[dbo].OpSheetAll "        
        + "order by  TotalMinutes DESC ";







        ds = Util.RunQueryByStmnt(sql);
        gdvToday.DataSource = ds.Tables[0];
        gdvToday.DataBind();


        sql = "Select Customer, sum(ISNULL(Minutes,0)) as TotalMinutes, "
       + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
       + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
       + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
       + " SUM(Calls)) as ACD "
       + " from [ICS].[dbo].OpSheetInc "       
       + " group by Customer "
       + "UNION "
       + "SELECT '** ALL **',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
       + "sum(Attempts) as Attempts, "
       + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
       + "mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
       + "mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
       + "mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
       + "from [ICS].[dbo].OpSheetInc "
       + "order by  TotalMinutes DESC ";


        ds = Util.RunQueryByStmnt(sql);
        gdvRecent.DataSource = ds.Tables[0];
        gdvRecent.DataBind();


        sql = "Select Customer, sum(ISNULL(Minutes,0)) as TotalMinutes, "
       + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
       + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
       + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
       + " SUM(Calls)) as ACD "
       + " from [ICS].[dbo].OpSheetY "       
       + " group by Customer "
       + "UNION "
       + "SELECT '** ALL **',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
       + "sum(Attempts) as Attempts, "
       + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
       + "mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
       + "mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
       + "mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
       + "from [ICS].[dbo].OpSheetY "       
       + "order by  TotalMinutes DESC ";


        ds = Util.RunQueryByStmnt(sql);
        gdvYesterday.DataSource = ds.Tables[0];
        gdvYesterday.DataBind();

        //lblRefresh RECENT
        StringBuilder query = new StringBuilder();
        query.Append("select crdate as Modified from [ICS].[dbo].sysobjects  where type = 'U' and name like 'OpSheetInc'");
        DataSet ResultSet1;
        ResultSet1 = RunQuery(query, "ICSConnectionString");
        lblRU.Text = "Last Modified:" + ResultSet1.Tables[0].Rows[0][0].ToString();
        
        //lblRefresh TODAY
        StringBuilder recentupdate = new StringBuilder();
        recentupdate.Append("select crdate as Modified from [ICS].[dbo].sysobjects  where type = 'U' and name like 'OpSheetAll'");
        DataSet ResultSet2;
        ResultSet2 = RunQuery(recentupdate, "ICSConnectionString");
        lblTU.Text = "Last Modified:" + ResultSet2.Tables[0].Rows[0][0].ToString();
        
        //lblRefresh YESTERDAY
        StringBuilder todayupdate = new StringBuilder();
        todayupdate.Append("select crdate as Modified from [ICS].[dbo].sysobjects  where type = 'U' and name like 'OpSheetY'");
        DataSet ResultSet3;
        ResultSet3 = RunQuery(todayupdate, "ICSConnectionString");
        lblYU.Text = "Last Modified:" + ResultSet3.Tables[0].Rows[0][0].ToString();
        


        //sql = " Select CAST(datepart(hour,stop)as varchar) as [Hour], ISNULL([Type],'TBD') as Type, "
        //+ "Customer, "
        //+ "sum(ISNULL(CustBillMinutes,0)) as TotalMinutes, count(*) as Attempts, sum(CASE WHEN duration > 0 then 1 else 0 end) as AnsweredCalls, "
        //+ "sum(RA) as Rejected, mecca2.dbo.fGetASR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end), SUM(RA)) as ASR, "
        //+ "mecca2.dbo.fGetABR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ABR, "
        //+ "mecca2.dbo.fGetACD(SUM(CustBillMinutes), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ACD "
        //+ "from [mecca2].[dbo].Repsheet0_ "
        //+ "where Customer like 'ICS%' and datepart(hour,stop) > 18 and [Type] = 'CPP' "
        //+ "group by datepart(hour,stop), ISNULL([Type],'TBD'), "
        //+ "Customer "
        //+ "UNION "
        //+ "Select '******' as [Hour], "
        //+ "'******' as Type, "
        //+ "'******' [Customer Group], "
        //+ "sum(ISNULL(CustBillMinutes,0)) as TotalMinutes, "
        //+ "count(*) as Attempts, "
        //+ "sum(CASE WHEN duration > 0 then 1 else 0 end) as AnsweredCalls, "
        //+ "sum(RA) as Rejected, "
        //+ "mecca2.dbo.fGetASR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end), SUM(RA)) as ASR, "
        //+ "mecca2.dbo.fGetABR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ABR, "
        //+ "mecca2.dbo.fGetACD(SUM(CustBillMinutes), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ACD "
        //+ "from [mecca2].[dbo].Repsheet0_ "
        //+ "where Customer like 'ICS%' "
        //+ "and datepart(hour,stop) > 18 and [Type] = 'CPP' "
        //+ "order by [Hour], TotalMinutes ";

        //ds = Util.RunQueryByStmnt(sql);
        //gdvYesterdayHour.DataSource = ds.Tables[0];
        //gdvYesterdayHour.DataBind();

    }


    private System.Data.DataSet RunQuery(StringBuilder qry, string StringConnection)
    {

        System.Data.SqlClient.SqlCommand sqlQuery = new System.Data.SqlClient.SqlCommand();
        sqlQuery.CommandText = qry.ToString();


        string connectionString = ConfigurationManager.ConnectionStrings
        [StringConnection].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
