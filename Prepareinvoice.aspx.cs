using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Prepareinvoice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();
        }
        else
        {

        }

    }
    protected void cmdInvoice_Click(object sender, EventArgs e)
    {
        string to = txtTo.Text;
        to = to.Replace("/", "_");
        Response.Redirect("Invoice.aspx?C=" + dpdCust.SelectedValue.ToString() + "&To=" + to+ "&In=" + txtInvoiceNumber.Text, false);


    }
    protected void dpdCust_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable tbl = Util.RunQueryByStmntatCDRDB("Select NumInvoice from Invoice where MeccaName ='" + dpdCust.SelectedValue.ToString() + "'").Tables[0];
        txtInvoiceNumber.Text = Convert.ToString( Convert.ToInt32(tbl.Rows[0][0].ToString()) + 1 );

    }
}
