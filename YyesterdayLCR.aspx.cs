using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class YyesterdayLCR : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string qry = "SELECT T.Country, T.Type, T.LMC, T.Class, T.Region,ROUND(SUM(CustMinutes),0) as Minutes,SUM(BillableCalls) as [Completed Calls],ROUND(Cost,4) Cost,ROUND(mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)),0) as ASR, ROUND(mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)),0) AS ABR, ROUND(mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)),1) as ACD FROM MECCA2..trafficlog T WHERE T.Customer NOT IN (SELECT CUSTOMER FROM mecca2.dbo.ControlCustomer) AND BillingDate = convert(varchar(10),getdate()-1,101) AND Country IS NOT NULL GROUP BY T.Country,T.Type,T.LMC,T.Class, T.Cost, T.Region HAVING SUM(CustMinutes) > 0 ORDER by Minutes DESC OPTION (MAXDOP 1)";

        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = System.Data.CommandType.Text;
        sqlSpQuery.CommandText = qry;
        DataSet ResultSet;
        ResultSet = RunQuery(sqlSpQuery);
        Session["Yestergrid"] = ResultSet;
        gdvYester.DataSource = ResultSet.Tables[0];
        gdvYester.DataBind();
        Label1.Text = "MECCA: YESTERDAY LCR " + DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");

    }
    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        DataSet resultsDataSet = new DataSet();
        try
        {
            string connectionString = ConfigurationManager.ConnectionStrings
            ["MECCA2ConnectionString"].ConnectionString;
            SqlConnection DBConnection = new SqlConnection(connectionString);
            SqlDataAdapter dbAdapter = new SqlDataAdapter();
            dbAdapter.SelectCommand = sqlQuery;
            sqlQuery.Connection = DBConnection;

            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            Label3.Text = ex.ToString();
        }

        return resultsDataSet;
    }
    protected void gdvYester_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvYester.PageIndex = e.NewPageIndex;
        gdvYester.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {

            DataTable dtEmployee = ((DataSet)Session["Yestergrid"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "YesterdayLCR" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
}
