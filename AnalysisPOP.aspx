<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="AnalysisPOP.aspx.cs" Inherits="AnalysisPOP" Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                    <asp:Label ID="Label1" runat="server" CssClass="labelTurkH" Text="MECCA CDRS National Query Builder" Font-Names="Arial" Font-Size="8pt"></asp:Label>
    
    <div>
   
        <table style="position:absolute; top:220px; left:230px;">
            <tr>
                <td align="left" style="width: 500px" valign="top">
                    <strong><em><span style="color: #008000; font-family: Arial"></span></em></strong>

                   </td>
                <td align="left" style="width: 500px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 500px" valign="top">
        <table align="left" style="width: 288px">
            <tr>
                <td align="right" bgcolor="#d5e3f0" style="width: 100px; height: 21px;" valign="top">
                    <asp:Label ID="lPop" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="#004A8F"
                        Text="POP:" Font-Bold="True"></asp:Label></td>
                <td align="left" style="width: 178px" valign="top">
        <asp:DropDownList ID="ddlPOP" runat="server" DataSourceID="sqldsPop" DataTextField="Pop"
            DataValueField="Pop" Width="150px" CssClass="dropdown">
        </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="right" bgcolor="#d5e3f0" style="width: 100px; height: 21px" valign="top">
        <asp:Label ID="lLine" runat="server" Text="LINE:" Font-Bold="True" ForeColor="#004A8F" Font-Names="Arial" Font-Size="8pt"></asp:Label></td>
                <td align="left" style="width: 178px; height: 21px" valign="top">
        <asp:DropDownList ID="ddlLine" runat="server" DataSourceID="sqldsLine" DataTextField="Line"
            DataValueField="Line" Width="150px" CssClass="dropdown">
        </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="right" bgcolor="#d5e3f0" style="width: 100px; height: 21px;" valign="top">
        <asp:Label ID="lFrom" runat="server" Text="FROM:" Font-Bold="True" ForeColor="#004A8F" Font-Names="Arial" Font-Size="8pt"></asp:Label></td>
                <td align="left" style="width: 178px" valign="top">
        <asp:TextBox ID="txtFrom" runat="server" Width="121px" CssClass="labelSteelBlue"></asp:TextBox>&nbsp;
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            </tr>
            <tr>
                <td align="right" bgcolor="#d5e3f0" style="width: 100px; height: 21px;" valign="top">
        <asp:Label ID="lTo" runat="server" Text="TO:" Font-Bold="True" ForeColor="#004A8F" Font-Names="Arial" Font-Size="8pt"></asp:Label></td>
                <td align="left" style="width: 178px" valign="top">
        <asp:TextBox ID="txtTo" runat="server" Width="121px" CssClass="labelSteelBlue"></asp:TextBox>&nbsp;
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            </tr>
            <tr>
                <td align="left" style="width: 100px" valign="top">
                </td>
                <td style="width: 178px" valign="top">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="View report" BorderStyle="Solid" BorderWidth="1px" CssClass="boton" /></td>
            </tr>
        </table>
                </td>
                <td align="left" style="width: 500px" valign="top">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="left" style="width: 500px; height: 50px" valign="bottom">
                    <asp:Button ID="cmdExport" runat="server" BorderStyle="Solid" BorderWidth="1px" CssClass="boton"
                        Font-Names="Arial" Font-Size="8pt" OnClick="cmdExport_Click" Text="Export to EXCEL"
                        Visible="False" /></td>
                <td align="left" style="width: 500px; height: 50px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 500px" valign="top">
        <asp:GridView ID="gvResult" runat="server" ForeColor="#333333" Font-Names="Arial" Font-Size="8pt" Width="400px" AllowPaging="True" OnPageIndexChanging="gvResult_PageIndexChanging" PageSize="100">
            <HeaderStyle CssClass="titleOrangegrid" Font-Names="Arial" Font-Size="8pt" />
            <FooterStyle Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="Navy" ForeColor="White" HorizontalAlign="Center" />
        
        <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
                </td>
                <td align="center" style="width: 500px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 500px" valign="top">
                </td>
                <td align="center" style="width: 500px" valign="top">
                </td>
            </tr>
            
            
        <tr>
            <td width="100%" colspan="2">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        
            
            <tr>
                <td align="left" style="width: 500px" valign="top">
                    &nbsp;&nbsp;<cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="Image1"
                        TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="Image2"
                        TargetControlID="txtTo">
                    </cc1:CalendarExtender>
                    <asp:ScriptManager id="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td align="left" style="width: 500px" valign="top">
                </td>
            </tr>
        </table>
        
    </div>
        <asp:SqlDataSource ID="sqldsPop" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
            SelectCommand="SELECT '** ALL **' as Pop union select Pop from CDRSPOP Order BY POP" ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsLine" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
            SelectCommand="SELECT '** ALL **' as Line union select Line from CDRSPOP Order BY Line" ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsGv" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>" ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>">
        </asp:SqlDataSource>
    


</asp:Content>

