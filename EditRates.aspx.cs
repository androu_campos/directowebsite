using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class EditRates : System.Web.UI.Page
{
    string ty = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        string providerName = Request.QueryString["provider"].ToString();
        string prefix = Request.QueryString["pref"].ToString();

        

        cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter adp = new cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter();
        cdrdbDataset.RRatesDupDif1DataTable tbl = adp.GetDataByNameAndPrefix1(providerName, prefix);
              
        //fill the gridview with rows repeated
        gdvRrateDup.DataSource = adp.GetDataByNameAndPrefix1(providerName, prefix);
        gdvRrateDup.DataBind();



        lblProviderName.Text = tbl.Rows[0]["ProviderName"].ToString();
        lblPrefix.Text = tbl.Rows[0]["Prefix"].ToString();
        ty = tbl.Rows[0]["Ty"].ToString();

    }

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter adpUpdate = new cdrdbDatasetTableAdapters.RRatesDupDif1TableAdapter();

        
        string providerName = lblProviderName.Text;
        string prefix = lblPrefix.Text;

        adpUpdate.DeleteQuery(providerName, prefix);//delete rows repeated

        adpUpdate.Insert(providerName, prefix, txtNewRate.Text, ty);//Insert new Row with data correct
        gdvRrateDup.DataSource = adpUpdate.GetDataByNameAndPrefix1(providerName, prefix);
        gdvRrateDup.DataBind();
        //"returnValue=document.getElementById('txtNewRate').value;"
        string stringreturn = "<script language='JavaScript'>" +             
            "window.close();</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "Return", stringreturn);
        this.Page.ClientScript.RegisterStartupScript(this.GetType(),"closeaw","<script language='JavaScript'> window.close();</script>");
        
    }
}
