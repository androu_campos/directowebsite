using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;

public partial class MonitorMobile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
        }
        else
        {
            try
            {
                //CUSTOMER
                SqlConnection SqlConn1 = new SqlConnection();
                SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
                SqlCommand SqlCommand1 = new SqlCommand("SELECT DISTINCT (CASE WHEN ProviderName LIKE 'ICS%' THEN 'ICS' ELSE ProviderName END) ProviderName from ProviderIP WHERE Type = 'C' UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT '** NONE **' from ProviderIP as ProviderName", SqlConn1);
                SqlConn1.Open();
                SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
                while (myReader1.Read())
                {
                    dpdCustomer.Items.Add(myReader1.GetValue(0).ToString());
                }
                myReader1.Close();
                SqlConn1.Close();

                //VENDOR
                SqlCommand SqlCommand = new SqlCommand("SELECT DISTINCT ProviderName from ProviderIP WHERE Type like 'V' UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT 'NATIONAL ALL' from ProviderIP as ProviderName UNION SELECT '** NONE **' from ProviderIP as ProviderName ", SqlConn1);
                SqlConn1.Open();
                SqlDataReader myReader = SqlCommand.ExecuteReader();
                while (myReader.Read())
                {
                    dpdVendor.Items.Add(myReader.GetValue(0).ToString());
                }
                myReader.Close();
                SqlConn1.Close();

                SqlConnection SqlConn = new SqlConnection();
                SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
                //COUNTRY
                SqlCommand SqlCommand4 = new SqlCommand("select distinct Country from Regions union  select '** NONE **' as Country from Regions union select '**ALL**' as Country from Regions order by Country ASC", SqlConn);
                SqlConn.Open();
                SqlDataReader myReader4 = SqlCommand4.ExecuteReader();
                while (myReader4.Read())
                {
                    dpdCountry.Items.Add(myReader4.GetValue(0).ToString());
                }
                myReader4.Close();
                SqlConn.Close();
                //REGION
                SqlCommand SqlCommand5 = new SqlCommand("select distinct Region from Regions union select '** NONE **' as Region from Regions union select '**ALL**' as Region from Regions order by Region ASC", SqlConn);
                SqlConn.Open();
                SqlDataReader myReader5 = SqlCommand5.ExecuteReader();
                while (myReader5.Read())
                {
                    dpdRegion.Items.Add(myReader5.GetValue(0).ToString());
                }
                myReader5.Close();
                SqlConn.Close();
                //TYPE
                SqlCommand SqlCommand6 = new SqlCommand("select distinct Regions.Type from Regions union select '** NONE **' as Type from Regions union select '**ALL**' as Type from Regions order by Type ASC", SqlConn);
                SqlConn.Open();
                SqlDataReader myReader6 = SqlCommand6.ExecuteReader();
                while (myReader6.Read())
                {
                    dpdType.Items.Add(myReader6.GetValue(0).ToString());
                }
                myReader6.Close();
                SqlConn.Close();
                //CLASS
                SqlCommand SqlCommand7 = new SqlCommand("SELECT DISTINCT Class FROM Regions WHERE Class is not NULL UNION SELECT '** NONE **' AS Class FROM Regions UNION SELECT '**ALL**' AS Class FROM Regions ORDER BY Class", SqlConn);
                SqlConn.Open();
                SqlDataReader myReader7 = SqlCommand7.ExecuteReader();
                while (myReader7.Read())
                {
                    dpdClass.Items.Add(myReader7.GetValue(0).ToString());
                }
                myReader7.Close();
                SqlConn.Close();
                //LMC
                SqlCommand SqlCommand10 = new SqlCommand("SELECT DISTINCT LMC FROM Regions WHERE LMC is not NULL UNION SELECT '** NONE **' AS LMC FROM Regions UNION SELECT '**ALL**' AS LMC FROM Regions ORDER BY LMC", SqlConn);
                SqlConn.Open();
                SqlDataReader myReader10 = SqlCommand10.ExecuteReader();
                while (myReader10.Read())
                {
                    dpdLMC.Items.Add(myReader10.GetValue(0).ToString());
                }
                myReader10.Close();
                SqlConn.Close();
               
                if (Session["hometype"].ToString() == "W")
                {
                    Label1.Text = "Advanced Query Monitor - Wholesale";
                }
                else if (Session["hometype"].ToString() == "R")
                {
                    Label1.Text = "Advanced Query Monitor - Retail";
                }
                else if (Session["hometype"].ToString() == "")
                {
                    Label1.Text = "Advanced Query Monitor";
                }

            }
            catch (Exception ex)
            {
                string errormessage = ex.Message.ToString();
            }
        }
    }

    private System.Data.DataSet RunQuery(StringBuilder qry, string StringConnection)
    {

        System.Data.SqlClient.SqlCommand sqlQuery = new System.Data.SqlClient.SqlCommand();
        sqlQuery.CommandText = qry.ToString();


        string connectionString = ConfigurationManager.ConnectionStrings
        [StringConnection].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }




    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        string Vendor = "";
        string graph = "";

        StringBuilder select = new StringBuilder();
        select.Append("SELECT ");

        StringBuilder where = new StringBuilder();
        where.Append("WHERE ");

        StringBuilder group = new StringBuilder();
        group.Append("GROUP BY ");

        StringBuilder union = new StringBuilder();
        union.Append(" UNION SELECT ");

        StringBuilder qry = new StringBuilder();
        StringBuilder order = new StringBuilder();
        order.Append(" Order by ");
        

        StringBuilder paramUrl = new StringBuilder();

        if (ckboxl.Items[0].Selected == true)
        {
            graph = "gcustomer";
        }
        else
        {
            graph = "gvendor";
        }


        if (dpdCustomer.SelectedValue.ToString() == "A&D-WORLDWIDE")
        {
            string c = dpdCustomer.SelectedValue.ToString();
            c = c.Replace("&", "_");

            paramUrl.Append("Customer=" + c + "&Vendor=" + dpdVendor.SelectedValue.ToString() + "&Country=" + dpdCountry.SelectedValue.ToString() + "&Region=" + dpdRegion.SelectedValue.ToString() + "&Type=" + dpdType.SelectedValue.ToString() + "&Class=" + dpdClass.SelectedValue.ToString() + "&LMC=" + dpdLMC.SelectedValue.ToString() + "&Graph=" + graph);
        }
        else
        {
            paramUrl.Append("Customer=" + dpdCustomer.SelectedValue.ToString() + "&Vendor=" + dpdVendor.SelectedValue.ToString() + "&Country=" + dpdCountry.SelectedValue.ToString() + "&Region=" + dpdRegion.SelectedValue.ToString() + "&Type=" + dpdType.SelectedValue.ToString() + "&Class=" + dpdClass.SelectedValue.ToString() + "&LMC=" + dpdLMC.SelectedValue.ToString() + "&Graph=" + graph);

        }


        
        

        /**/

        //string popupScript = "<script language='JavaScript'>" + "window.open('http://directo.computer-tel.com/AdvancedMonitor.aspx?" + paramUrl.ToString() + "','','')" + "</script>";
        //ClientScript.RegisterStartupScript(Page.GetType(), "Window", popupScript);

        Response.Redirect("~/AdvancedMonitorMobile.aspx?" + paramUrl);

    }

}
