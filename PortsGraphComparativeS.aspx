<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PortsGraphComparativeS.aspx.cs"
    Inherits="PortsGraphComparativeCC" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
    
    function dispData(date,calls,graph)
    {
    
    if (document.getElementById)
	{
									        
    var a = date;
    var i=0;
    var b;
    var c;
    if(a.length > 3)
    {
      
      i = a.lastIndexOf("/");
      b = a.substring(i+5);
      a = a.substring(0,i+5);
      
      c = a + " " + b;
      
      document.getElementById('<%=lblMaxPoint.ClientID%>').innerText = "DateTime: " + c + " , Calls: " + calls;
      
    }
    
     
	}

	
    }    
    
    </script>

    <table>
        <tr>
            <td style="width: 904px; height: 12px;" valign="top">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Active Ports Graph"
                    Width="125px"></asp:Label></td>
        </tr>
        <tr>
            <td valign="top" height="30" style="border-right: 0px solid; border-top: 0px solid;
                border-left: 0px solid; border-bottom: 0px solid; width: 904px;">
                <table style="height: 29px">
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 61px">
                            <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Provider"></asp:Label></td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdProvider" runat="server" CssClass="dropdown" DataSourceID="sqldsCust"
                                DataTextField="Customer" DataValueField="Customer">
                            </asp:DropDownList></td>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Type:"></asp:Label></td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdType" runat="server" CssClass="dropdown">
                                <asp:ListItem Value="C">Customer</asp:ListItem>
                            </asp:DropDownList></td>
                        <td style="width: 117px" align="center" bgcolor="#d5e3f0">
                            <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="From:"></asp:Label></td>
                        <td style="width: 448px">
                            <asp:TextBox ID="txtFrom" runat="server" CssClass="labelSteelBlue" Width="80px"></asp:TextBox>
                            <asp:Image ID="imgFrom" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                        <td style="width: 163px" align="center" bgcolor="#d5e3f0">
                            <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="To:"></asp:Label></td>
                        <td style="width: 452px">
                            <asp:TextBox ID="txtTo" runat="server" CssClass="labelSteelBlue" Width="80px"></asp:TextBox>
                            <asp:Image ID="imgTo" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                        <td align="right" style="width: 108px">
                            <asp:Button ID="cmdReport" runat="server" CssClass="boton" OnClick="cmdReport_Click"
                                Text="View Graph" /></td>
                    </tr>
                    <tr>
                        <td style="width: 163px" align="center" bgcolor="#d5e3f0">
                            <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Source:"></asp:Label></td>
                        <td colspan="7">
                            <asp:CheckBoxList runat="server" ID="chkbSource" DataTextField="Source" AutoPostBack="true"
                                DataValueField="Source" TextAlign="Right" RepeatDirection="Horizontal" CssClass="labelSteelBlue" OnSelectedIndexChanged="chblSource_SelectedIndexChanged">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 321px">
                            <asp:Label ID="lblWarning" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                ForeColor="Red" Text="Nothing found!!" Visible="False" Width="92px"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 904px" align="left" valign="top">
                <DCWC:Chart ID="Chart1" runat="server" Width="905px" Height="445px" Palette="Pastel">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line"
                            Color="DarkOrange">
                        </DCWC:Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="PortsLastWeek" ChartType="Line"
                            Color="Blue">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
            <td style="width: 100px" align="left" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 904px">
                <asp:Label ID="lblMaxPoint" runat="server" CssClass="labelBlue8" Width="317px"></asp:Label><br />
                <table style="width: 178px; height: 68px;">
                    <tr>
                        <td align="right" style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 904px" align="left" valign="top">
                <table>
                    <tr>
                        <td style="width: 179px">
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 179px" valign="top">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button ID="cmdDetail" runat="server" CssClass="boton" OnClick="cmdDetail_Click"
                                        Text="View detail" Visible="False" /><asp:Button ID="cmdHide" runat="server" CssClass="boton"
                                            OnClick="cmdHide_Click" Text="Hide detail" Visible="False" /><br />
                                    <asp:Label ID="lblGrid" runat="server" Text="Label" Visible="False" CssClass="labelBlue8"></asp:Label>
                                    <br />
                                    <asp:GridView ID="gdvDetail" runat="server" Font-Size="8pt" Font-Names="Arial" Visible="False"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:BoundField DataField="Time" HeaderText="Time">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Live_Calls" HeaderText="Live_Calls">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle CssClass="titleOrangegrid"></HeaderStyle>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="cmdDetail" EventName="Click"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="cmdHide" EventName="Click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td align="left" style="width: 100px" valign="top">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 100px">
                                                <asp:Label ID="Label5" runat="server" Text="Loading...Please wait" ForeColor="LimeGreen"
                                                    Width="105px" CssClass="labelBlue8"></asp:Label></td>
                                            <td style="width: 100px">
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Loading.gif"></asp:Image></td>
                                        </tr>
                                    </table>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 179px">
                            &nbsp;</td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 904px">
                <asp:SqlDataSource ID="sqldsCust" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                    SelectCommand="SELECT '**ALL**' AS Customer FROM CDRDB.dbo.ProviderIP UNION SELECT ProviderName FROM CDRDB.dbo.ProviderIP WHERE Type = 'C' AND ProviderName NOT LIKE 'CC-%'"
                    ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>" ></asp:SqlDataSource>
                <asp:SqlDataSource ID="sqldsSource" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                    SelectCommand="SELECT '**ALL**' AS Source UNION SELECT SOURCENAME FROM MECCA2.DBO.SOURCELIST WHERE ACTIVE = 1"
                    ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>" ></asp:SqlDataSource>
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgFrom"
                    TargetControlID="txtFrom">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="imgTo"
                    TargetControlID="txtTo">
                </cc1:CalendarExtender>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 106px; height: 44px;" valign="top">
                <asp:Timer ID="Timer1" runat="server" Interval="300000">
                </asp:Timer>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
