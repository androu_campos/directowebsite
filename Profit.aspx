<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="Profit.aspx.cs" Inherits="Profit" Title="DIRECTO - Connections Worldwide" %>
<%@ OutputCache Duration="21600" Location="Client" VaryByParam="None" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label6" runat="server" CssClass="labelTurkH" Text="MECCA PROFIT ANALYZER"></asp:Label>

    <table align="left" width="100%" style="position: absolute; top: 220px; left: 230px;">    
        <tr>
            <td width="15%"  align="left" rowspan="3" valign="top">
                <table>
                    <tr>
                        <td bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label1" runat="server" Text="Customer" CssClass="labelBlue8"></asp:Label></td>
                        <td style="width: 100px">
                <asp:DropDownList ID="dpdCustomer" runat="server" CssClass="dropdown" Width="110px">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label2" runat="server" Text="Vendor" CssClass="labelBlue8"></asp:Label></td>
                        <td style="width: 100px">
                <asp:DropDownList ID="dpdVendor" runat="server" CssClass="dropdown" Width="110px">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label3" runat="server" Text="Country" CssClass="labelBlue8"></asp:Label></td>
                        <td style="width: 100px">
                <asp:DropDownList ID="dpdCountry" runat="server" CssClass="dropdown" Width="110px">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label4" runat="server" Text="Regions" CssClass="labelBlue8"></asp:Label></td>
                        <td style="width: 100px">
                <asp:DropDownList ID="dpdRegions" runat="server" CssClass="dropdown" Width="110px">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label5" runat="server" Text="Report Type" CssClass="labelBlue8"></asp:Label></td>
                        <td style="width: 100px">
                <asp:DropDownList ID="dpdReport" runat="server" CssClass="dropdown" Width="110px">
                    <asp:ListItem Value="1">Daily</asp:ListItem>
                    <asp:ListItem Value="2">Weekly</asp:ListItem>
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td align="right" style="width: 100px">
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="View report"
                    CssClass="boton" /></td>
                    </tr>
                </table>
            </td>
            <td width="30%" align="left" style="height: 106px">
                </td>            
        </tr>
        <tr>
            <td align="left" style="height: 29px">
                </td>
            
        </tr>
        <tr>
            <td  align="left" style="height: 28px">
                </td>
            
        </tr>
        <tr>
            <td style="height: 26px" >
            </td>
            <td style="height: 26px" >
            </td>
           
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label7" runat="server" CssClass="labelSteelBlue" Text="Report Totals"
                    Visible="False"></asp:Label></td>
            <td  align="left">
            </td>
            
        </tr>
        <tr>
            <td align="left" style="height: 5px" >
            </td>
            <td style="height: 5px" >
            </td>
            
        </tr>
        <tr>
            <td align="left" >
                <asp:Button ID="Button2" runat="server" CssClass="boton" Font-Names="Arial" Font-Size="8pt"
                    OnClick="Button2_Click" Text="Export to EXCEL" Visible="False" /></td>
            <td align="left" >
            </td>
            
        </tr>
        <tr>
            <td align="left" colspan="2" rowspan="2">
                <asp:GridView ID="gdvTotals" runat="server" AutoGenerateColumns="false" CssClass="GridView" Width="606px">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="9pt" Width="100px" />
                    <AlternatingRowStyle BackColor="White" />
                    
                    <Columns>
                    <asp:BoundField DataField="Total Minutes" HeaderText="Total Minutes" HtmlEncode="false" DataFormatString="{0:0,0}" />
                    <asp:BoundField DataField="Total Profit" HeaderText="Total Profit" HtmlEncode="false" DataFormatString="{0,00:C}" />
                    <asp:BoundField DataField="Previous Minutes" HeaderText="Previous Minutes" HtmlEncode="false" DataFormatString="{0:0,0}" />
                    <asp:BoundField DataField="Previous Profit" HeaderText="Previous Profit" HtmlEncode="false" DataFormatString="{0,00:C}" />
                    <asp:BoundField DataField="Total Delta" HeaderText="Total Delta" HtmlEncode="false" DataFormatString="{0:0.00}" />
                    
                    
                    </Columns>
                    
                    
                </asp:GridView>
                <asp:Label ID="lblTotals" runat="server" CssClass="labelSteelBlue" ForeColor="Red"
                    Text="Nothing found" Visible="False"></asp:Label></td>
            
        </tr>
        <tr>
            <td style="width: 100px" colspan="2">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 87px; height: 16px">
            </td>
            <td style="width: 100px; height: 16px">
            </td>
            
        </tr>
        <tr>
            <td style="width: 87px" align="left">
                <asp:Label ID="Label8" runat="server" CssClass="labelSteelBlue" Text="Report Details"
                    Visible="False"></asp:Label></td>
            <td style="width: 100px">
            </td>
            
        </tr>
        <tr>
            <td align="left" style="width: 87px; height: 5px">
            </td>
            <td style="width: 100px; height: 5px">
            </td>
            
        </tr>
        <tr>
            <td align="left" style="width: 87px" valign="bottom">
                <asp:Button ID="Button3" runat="server" CssClass="boton" Font-Names="Arial" Font-Size="8pt"
                    OnClick="Button3_Click" Text="Export to EXCEL" Visible="False" /></td>
            <td style="width: 100px">
            </td>
          
        </tr>
        <tr>
            <td colspan="2" style="height: 129px" align="left" valign="top">
                <asp:GridView ID="gdvProfit" runat="server" CssClass="GridView" AutoGenerateColumns="false">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Left" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="9pt" Width="100px" />
                    <AlternatingRowStyle BackColor="White" />
                    
                    <Columns>
                    
                    
                    
                    
                    
                    </Columns>
                    
                </asp:GridView>
                <asp:Label ID="lblDetails" runat="server" CssClass="labelSteelBlue" ForeColor="Red"
                    Text="Nothing found" Visible="False"></asp:Label></td>
           
        </tr>
        <tr>
            <td width="100%" colspan="2">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" >
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label9" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
