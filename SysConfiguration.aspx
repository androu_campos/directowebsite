<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SysConfiguration.aspx.cs" Inherits="SysConfiguration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
    
        .contenedor{
            display: table;
            
            width: 800px;
            border: 1px solid;
			#position: relative;
			border: 1px solid #CCCCCC;
			margin: auto;
			
			background: #E9E9E9; /* Para exploradores sin css3 */

              /* Para el WebKit (Safari, Google Chrome etc) */
            background: -webkit-gradient(linear, left top, left bottom, from(#E9E9E9), to(#fff));
     
             /* Para Mozilla/Gecko (Firefox etc) */
            background: -moz-linear-gradient(top, #E9E9E9, #FFFFFF);
     
             /* Para Internet Explorer 5.5 - 7 */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#E9E9E9, endColorstr=#FFFFFF, GradientType=0);
                  /* Para Internet Explorer 8 */
                  
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#E9E9E9, endColorstr=#FFFFFF)";
			
        }
        
        .contenido {
        
            display: table-cell;
            vertical-align: middle;
            #position: absolute;
            #top: -50%;
        }
        
        .inner{
            #position: relative;
            #top: -50%; 
            
            
        }
        
        .labelclass{
            padding: 5px;   
                   
        }     
        
        .textarea
        {
            resize: none;
        }   
        
        .tableclass{
            margin: auto;
        }
        
        .buttonclass{
                color: #6e6e6e;
                font: bold 11px Helvetica, Arial, sans-serif;
                text-decoration: none;
                padding: 7px 12px;
                position: relative;
                display: inline-block;
                text-shadow: 0 1px 0 #fff;
                -webkit-transition: border-color .218s;
                -moz-transition: border .218s;
                -o-transition: border-color .218s;
                transition: border-color .218s;
                background: #f3f3f3;
                background: -webkit-gradient(linear,0% 40%,0% 70%,from(#F5F5F5),to(#F1F1F1));
                background: -moz-linear-gradient(linear,0% 40%,0% 70%,from(#F5F5F5),to(#F1F1F1));
                border: solid 1px #dcdcdc;
                border-radius: 2px;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                margin-right: 10px;
                    }
        .buttonclass:hover{
            color: #333;
            border-color: #999;
            -moz-box-shadow: 0 2px 0 rgba(0, 0, 0, 0.2); 
	        -webkit-box-shadow:0 2px 5px rgba(0, 0, 0, 0.2);
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15);
        }         
        
        a.button {
            color: #6e6e6e;
            font: bold 12px Helvetica, Arial, sans-serif;
            text-decoration: none;
            padding: 7px 12px;
            position: relative;
            display: inline-block;
            text-shadow: 0 1px 0 #fff;
            -webkit-transition: border-color .218s;
            -moz-transition: border .218s;
            -o-transition: border-color .218s;
            transition: border-color .218s;
            background: #f3f3f3;
            background: -webkit-gradient(linear,0% 40%,0% 70%,from(#F5F5F5),to(#F1F1F1));
            background: -moz-linear-gradient(linear,0% 40%,0% 70%,from(#F5F5F5),to(#F1F1F1));
            border: solid 1px #dcdcdc;
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            margin-right: 10px;
        }
        a.button:hover {
            color: #333;
            border-color: #999;
            -moz-box-shadow: 0 2px 0 rgba(0, 0, 0, 0.2); 
	        -webkit-box-shadow:0 2px 5px rgba(0, 0, 0, 0.2);
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15);
        }
        a.button:active {
            color: #000;
            border-color: #444;
        }
        a.left {
            -webkit-border-top-right-radius: 0;
            -moz-border-radius-topright: 0;
            border-top-right-radius: 0;
            -webkit-border-bottom-right-radius: 0;
            -moz-border-radius-bottomright: 0;
            border-bottom-right-radius: 0;
            margin: 0;
        }
        a.middle {
            border-radius: 0;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-left: solid 1px #f3f3f3;
            margin: 0;
            border-left: solid 1px rgba(255, 255, 255, 0);
        }
        a.middle:hover,
        a.right:hover { border-left: solid 1px #999 }
        a.right {
            -webkit-border-top-left-radius: 0;
            -moz-border-radius-topleft: 0;
            border-top-left-radius: 0;
            -webkit-border-bottom-left-radius: 0;
            -moz-border-radius-bottomleft: 0;
            border-bottom-left-radius: 0;
            border-left: solid 1px #f3f3f3;
            border-left: solid 1px rgba(255, 255, 255, 0);
        }
        a.big {
            font-size: 16px;
            padding: 10px 15px;
        }
        a.supersize {
            font-size: 20px;
            padding: 15px 20px;
        }
        a.save {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png) 10px 7px no-repeat #f3f3f3;
            padding-left: 30px;
        }
        a.add {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png)  10px -27px no-repeat #f3f3f3;
            padding-left: 30px;
        }
        a.delete {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png)  10px -61px no-repeat #f3f3f3;
            padding-left: 30px;
        }
        a.flag {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png)  10px -96px no-repeat #f3f3f3;
            padding-left: 30px;
        }
        a.up {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png)  13px -133px no-repeat #f3f3f3;
            width: 18px;
        }
        a.down {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png)  13px -168px no-repeat #f3f3f3;
            width: 18px;
        }
        a.save-big {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png) 15px 11px no-repeat #f3f3f3;
            font-size: 16px;
            padding: 10px 15px 10px 35px;
        }
        a.add-big {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png)  15px -23px no-repeat #f3f3f3;
            font-size: 16px;
            padding: 10px 15px 10px 35px;
        }
        a.delete-big {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png)  15px -57px no-repeat #f3f3f3;
            font-size: 16px;
            padding: 10px 15px 10px 35px;
        }
        a.flag-big {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png)  15px -92px no-repeat #f3f3f3;
            font-size: 16px;
            padding: 10px 15px 10px 35px;
        }
        a.up-big {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png)  15px -128px no-repeat #f3f3f3;
            width: 18px;
            font-size: 16px;
            padding: 10px 15px;
        }
        a.down-big {
            background: url(http://cssdeck.com/uploads/media/items/5/5Fm069k.png)  15px -163px no-repeat #f3f3f3;
            width: 18px;
            font-size: 16px;
            padding: 10px 15px;
        }
        
        .imageclass{
            border: 1px solid;			
			border: 1px solid #CCCCCC;
			width: 200px;
			height: 200px;
        }
        
        .HeaderCSS{
            
            font: bold 14px Helvetica, Arial, sans-serif;
            padding-left:20px;
            padding-top: 15px ;
            width: 780px;
            height: 30px;
            border: 1px solid;
			#position: relative;
			border: 1px solid #CCCCCC;
			margin: auto;
            
            background: #E9E9E9; /* Para exploradores sin css3 */

              /* Para el WebKit (Safari, Google Chrome etc) */
            background: -webkit-gradient(linear, left top, left bottom, from(#E9E9E9), to(#fff));
     
             /* Para Mozilla/Gecko (Firefox etc) */
            background: -moz-linear-gradient(top, #E9E9E9, #FFFFFF);
     
             /* Para Internet Explorer 5.5 - 7 */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#E9E9E9, endColorstr=#FFFFFF, GradientType=0);
                  /* Para Internet Explorer 8 */
                  
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#E9E9E9, endColorstr=#FFFFFF)";
        }
    
    </style>
</head>
<body>
    
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   
    <br />    
      
        <cc1:Accordion ID="Accordion1" runat="server" HeaderCssClass="HeaderCSS">
            <Panes>
                <cc1:AccordionPane runat="server" HeaderCssClass="HeaderCSS">
                    <Header>
                        Edit Mail Message
                    </Header>
                    <Content>
                         <div class="contenedor" align="center">
                            <div class="contenido" align="center">
                                <div class="inner" align="center">
                                    <table width="780px">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="TextBox1" runat="server" Height="230px" TextMode="MultiLine" Width="780px" Font-Size="11px"
                                                 Font-Names="Arial" CssClass="textarea">
                                                </asp:TextBox>                    
                                            </td>
                                        </tr>             
                                    </table> 
                                </div>
                            </div>           
                        </div>    
                        <table width="780px" class="tableclass">
                            <tr>
                                <td align="right">
                                    <asp:Button ID="Button1" runat="server" Text="Preview" OnClick="Convert" CssClass="buttonclass" />  
                                </td>
                            </tr>  
                        </table>
                    </Content>
                </cc1:AccordionPane>
                <cc1:AccordionPane ID="AccordionPane1" runat="server" >
                    <Header>
                        Mail Signature
                    </Header>
                    <Content>
                        <div class="contenedor" align="center">
                            <div class="contenido" align="center">
                                <div class="inner" align="center">
                                    <table>
                                        <tr>
                                            <td width= "200px" align="center">
                                                <asp:Label runat="server" ID="lblimg" Text="Select signature to edit:" Font-Bold="true" 
                                                Font-Size="11px" Font-Names="Arial"></asp:Label>
                                            </td>
                                            <td width="250px" align="center">
                                                <asp:FileUpload ID="FU" runat="server" />
                                            </td>
                                            <td width="250px" align="center">                            
                                                <a id="A1" href="#" class="button add" runat="server" onserverclick="Upload">Add Item</a>
                                            </td>                        
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td width= "200px" align="center">
                                                <asp:Label runat="server" ID="lblimg2" Text="New Signature:" Font-Bold="true" 
                                                Font-Size="11px" Font-Names="Arial"></asp:Label>                            
                                            </td>
                                            <td align="center">
                                                <asp:Image ID="Image1" runat="server" CssClass="imgclass" BorderColor="SteelBlue" BorderWidth="2" />
                                            </td>
                                            <td align="center" valign="bottom">
                                                <div id="divCustomersButton" runat="server">
                                                    <a href="#" class="button flag" ID="btnCustomers" >Select Customers to Apply</a>
                                                </div>                            
                                            </td>
                                        </tr>                         
                                    </table> 
                                </div>
                            </div>           
                        </div>
                    </Content>
                </cc1:AccordionPane>
                <cc1:AccordionPane ID="AccordionPane2" runat="server">
                    <Header>
                        Select Customers to Apply
                    </Header>
                    <Content>
                        <div class="contenedor" align="center">
                            <div class="contenido" align="center">
                                <div class="inner" align="center">
                                    <table>
                                    <tr>
                                        <td>                                
                                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" Font-Names="Arial" 
                                                Font-Size="11px" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                                            <asp:ListItem Text="All Customers" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Some Customers" ></asp:ListItem>                                    
                                            <asp:ListItem Text="Just For Customers" ></asp:ListItem>
                                            </asp:RadioButtonList>            
                                        </td>
                                        <td>
                                            <asp:CheckBoxList ID="CheckBoxList1" runat="server" Font-Names="Arial" Font-Size="11px"
                                             RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Standard" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Customized" ></asp:ListItem>                                    
                                                <asp:ListItem Text="Exception" ></asp:ListItem>
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                            <asp:ListBox ID="ListBox1" runat="server" AutoPostBack="True" 
                                                DataTextField="Customer" DataValueField="CustomerID" Height="271px" Width="241px" 
                                                Font-Names="Arial" Font-Size="11px"
                                                OnSelectedIndexChanged="ListBox1_SelectedIndexChanged">
                                            </asp:ListBox>                                
                                        </td>
                                        <td>
                                            <asp:ListBox ID="ListBox2" runat="server" Height="271px" Width="241px" ></asp:ListBox>                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <asp:Label ID="lblcountcustomers" runat="server" Visible="False" Width="239px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <td valign="bottom" style="height: 49px">
                                                        <asp:Button ID="Button2" runat="server" Text="Reset" OnClick="Button1_OnClick" Visible="False"/>
                                                    </td>
                                                    <td valign="bottom" style="height: 49px">
                                                        <asp:Button ID="Button3" runat="server" Text="Delete" OnClick="Button2_OnClick" Visible="False"/>
                                                    </td>
                                                    <td valign="bottom" style="height: 49px">
                                                        <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Text="Continue" CssClass="boton" Visible="false" CausesValidation="true"/>
                                                        <asp:CustomValidator ID="cvListBox2" runat="server" ControlToValidate="ListBox2" ErrorMessage="You must select at least one Customer"
                                                                            Display="Dynamic" SetFocusOnError="true" ValidateEmptyText="true" ClientValidationFunction="Validate"
                                                                            OnServerValidate="cvListbox_ServerValidate" ></asp:CustomValidator>
                                                    </td>
                                                    </tr>
                                                </table>                                
                                            </td>
                                        </tr
                                        <tr>
                                            <td align="right" colspan="2">
                                                <asp:Button ID="Button4" runat="server" Text="Preview" OnClick="Convert" CssClass="buttonclass" />  
                                                <a href="#" class="button save">Save</a>                                    
                                            </td>                                
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </Content>
                </cc1:AccordionPane>
            </Panes>
        </cc1:Accordion>
    </form>   
</body>
</html>
