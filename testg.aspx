<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="testg.aspx.cs" Inherits="testg" Title="Untitled Page" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <table class="sampleTable">
				<tr>
					<td width="412" class="tdchart">
						<DCWC:CHART id="Chart1" runat="server" Palette="Dundas" BackColor="#D3DFF0" Height="296px" Width="412px" BorderLineStyle="Solid" 
						BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105" 
						SoftShadows="False" ImageUrl="~/ChartPic_#SEQ(300,3)">
							<Legends>
								<dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
								EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
								Name="Default"></dcwc:Legend>
							</Legends>
							<BorderSkin SkinStyle="Emboss"></BorderSkin>
							<Series>
								<dcwc:Series ChartArea="Area1" XValueType="Double" Name="Series1" ChartType="Pie" 
								Font="Trebuchet MS, 8.25pt, style=Bold" 
								CustomAttributes="PieDrawingStyle=Concave, CollectedLabel=Other, DoughnutRadius=25,
								MinimumRelativePieSize=20" MarkerStyle="Circle" 
								BorderColor="64, 64, 64, 64" Color="180, 65, 140, 240" YValueType="Double" Label="#PERCENT{P1}">
									<Points>
										<dcwc:DataPoint LegendText="RUS" CustomAttributes="OriginalPointIndex=0" YValues="39"></dcwc:DataPoint>
										<dcwc:DataPoint LegendText="CAN" CustomAttributes="OriginalPointIndex=1" YValues="18"></dcwc:DataPoint>
										<dcwc:DataPoint LegendText="USA" CustomAttributes="OriginalPointIndex=2" YValues="15"></dcwc:DataPoint>
										<dcwc:DataPoint LegendText="PRC" CustomAttributes="OriginalPointIndex=3" YValues="12"></dcwc:DataPoint>
										<dcwc:DataPoint LegendText="DEN" CustomAttributes="OriginalPointIndex=5" YValues="8"></dcwc:DataPoint>
										<dcwc:DataPoint LegendText="AUS" YValues="4.5"></dcwc:DataPoint>
										<dcwc:DataPoint LegendText="IND" CustomAttributes="OriginalPointIndex=4" YValues="3.20000004768372"></dcwc:DataPoint>
										<dcwc:DataPoint LegendText="ARG" YValues="2"></dcwc:DataPoint>
										<dcwc:DataPoint LegendText="FRA" YValues="1"></dcwc:DataPoint>
									</Points>
								</dcwc:Series>
							</Series>
							<ChartAreas>
								<dcwc:ChartArea Name="Area1" BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BackGradientType="TopBottom">
									<AxisY2>
										<MajorGrid Enabled="False"></MajorGrid>
										<MajorTickMark Enabled="False"></MajorTickMark>
									</AxisY2>
									<AxisX2>
										<MajorGrid Enabled="False"></MajorGrid>
										<MajorTickMark Enabled="False"></MajorTickMark>
									</AxisX2>
									<Area3DStyle PointGapDepth="900" YAngle="162" RightAngleAxes="False" WallWidth="25" Clustered="True"></Area3DStyle>
									<AxisY LineColor="64, 64, 64, 64">
										<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
										<MajorGrid LineColor="64, 64, 64, 64" Enabled="False"></MajorGrid>
										<MajorTickMark Enabled="False"></MajorTickMark>
									</AxisY>
									<AxisX LineColor="64, 64, 64, 64">
										<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
										<MajorGrid LineColor="64, 64, 64, 64" Enabled="False"></MajorGrid>
										<MajorTickMark Enabled="False"></MajorTickMark>
									</AxisX>
								</dcwc:ChartArea>
							</ChartAreas>
						</DCWC:CHART>
					</td>
					<td valign="top">
						<table class="controls" cellpadding="4">
							<TR>
								<TD class="label" style="WIDTH: 181px">
									<P>Collect Pie Slices:</P>
								</TD>
								<TD>
									<asp:checkbox id="chk_Pie" runat="server" AutoPostBack="True" Text="" Checked="True"></asp:checkbox></TD>
							</TR>
							<tr>
								<td class="label" style="WIDTH: 181px; HEIGHT: 29px">Collected Threshold (in %):</td>
								<td>
									<asp:dropdownlist id="comboBoxCollectedThreshold" runat="server" Width="120px" AutoPostBack="True" CssClass="spaceright">
										<asp:ListItem Value="5">5</asp:ListItem>
										<asp:ListItem Value="8">8</asp:ListItem>
										<asp:ListItem Value="12">12</asp:ListItem>
										<asp:ListItem Value="15">15</asp:ListItem>
									</asp:dropdownlist>
								</td>
							</tr>
							<tr>
								<td class="label" style="WIDTH: 181px">Collected Color:</td>
								<td><asp:dropdownlist id="comboBoxCollectedColor" runat="server" AutoPostBack="True" CssClass="spaceright" Width="120px">
										<asp:ListItem Value="Green">Green</asp:ListItem>
										<asp:ListItem Value="Gray">Gray</asp:ListItem>
										<asp:ListItem Value="Magenta">Magenta</asp:ListItem>
										<asp:ListItem Value="Gold">Gold</asp:ListItem>
									</asp:dropdownlist>
								</td>
							</tr>
							<tr>
								<td class="label" style="WIDTH: 181px">Collected Label:</td>
								<td>
									<asp:TextBox id="textBoxCollectedLabel" runat="server" Width="120px" CssClass="spaceright" AutoPostBack="True" MaxLength="10"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="label" style="WIDTH: 181px; HEIGHT: 16px">Collected Legend Text:</td>
								<td style="HEIGHT: 16px">
									<asp:TextBox id="textBoxCollectedLegend" runat="server" CssClass="spaceright" Width="120px" AutoPostBack="True" MaxLength="10"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="label" style="WIDTH: 181px; HEIGHT: 20px">Show Exploded:</td>
								<td style="HEIGHT: 20px"><asp:checkbox id="ShowExplode" runat="server" AutoPostBack="True" Text=""></asp:checkbox></td>
							</tr>
							<TR>
								<TD class="label" style="WIDTH: 181px">Chart Type:</TD>
								<TD>
									<asp:dropdownlist id="comboBoxChartType" runat="server" CssClass="spaceright" AutoPostBack="True" Width="120px">
										<asp:ListItem Value="Pie" Selected="True">Pie</asp:ListItem>
										<asp:ListItem Value="Doughnut">Doughnut</asp:ListItem>
									</asp:dropdownlist></TD>
							</TR>
						</table>
					</td>
				</tr>
			</table>
   
</asp:Content>

