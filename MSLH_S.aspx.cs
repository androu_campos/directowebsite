using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class MSLH_S : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        string Query;
        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandTimeout = 350;
        sqlSpQuery.CommandType = System.Data.CommandType.Text;
        Query = Session["SalesLogS"].ToString();

        sqlSpQuery.CommandText = Query;
        DataSet ResultSet;
        ResultSet = Util.RunQuery(sqlSpQuery);

        Session["ResultSetMSLH"] = ResultSet;

        int jr = ResultSet.Tables[0].Rows.Count;
        if (jr <= 0)
        {
            Label1.Visible = true;
            Button1.Visible = false;
        }
        else
        {
            this.GridView1.AutoGenerateColumns = false;
            this.GridView1.DataSource = ResultSet;
            this.GridView1.Columns.Clear();
            for (int c = 0; c < ResultSet.Tables[0].Columns.Count; c++)
            {
                string columnName = ResultSet.Tables[0].Columns[c].ColumnName.ToString();
                BoundField columna = new BoundField();
                columna.HtmlEncode = false;
                if (columnName == "TotalCost" || columnName == "TotalSales" || columnName == "Profit")
                {
                    columna.DataFormatString = "${0:f2}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "CMinutes" || columnName == "VMinutes")
                {
                    columna.DataFormatString = "{0:0,0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "AnsweredCalls")
                {
                    columna.DataFormatString = "{0:0,0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 100;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "ASR")
                {
                    columna.DataFormatString = "{0:0,0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "ACD")
                {
                    columna.DataFormatString = "{0:0.0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "ABR")
                {
                    columna.DataFormatString = "{0:0.0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Attempts")
                {
                    columna.DataFormatString = "{0:0,0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Rejected Calls")
                {
                    columna.DataFormatString = "{0:0,0}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 440;
                    columna.HeaderStyle.Width = 440;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Country")
                {

                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 100;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Cost" || columnName == "Price")
                {
                    columna.DataFormatString = "${0:f4}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;

                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Customer")
                {
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    //columna.ItemStyle.Width = 450;
                    columna.ItemStyle.Wrap = false;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }
                else if (columnName == "Vendor")
                {
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //columna.ItemStyle.Width = 100;
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Wrap = false;
                }
                else if (columnName == "Region")
                {
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Wrap = false;
                }
                else if (columnName == "LMC")
                {
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    //columna.ItemStyle.Width = 190;
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                }


                columna.DataField = columnName;
                columna.HeaderText = columnName;

                GridView1.Columns.Add(columna);
                GridView1.DataBind();
            }

            Label1.Visible = false;
        }



        GridView1.RowStyle.HorizontalAlign = HorizontalAlign.Justify;
        GridView1.RowStyle.BorderWidth = 0;
        GridView1.RowStyle.Font.Name = "Arial";
        GridView1.RowStyle.Font.Size = 8;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {

            DataTable dtEmployee = ((DataSet)Session["ResultSetMSLH"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "MasterSalesLog" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
}
