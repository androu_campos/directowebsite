<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="AldaPreview.aspx.cs" Inherits="OpenSourcePreview" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart1" runat="server" Width="550px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                    <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom" 
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                <DCWC:Chart ID="Chart14" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart6" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart2" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart3" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart11" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart12" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart4" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart5" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
         <tr>
            <td>
                 <DCWC:Chart ID="Chart7" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart8" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
         <tr>
            <td>
                 <DCWC:Chart ID="Chart9" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
         <tr>
            <td>
                 <DCWC:Chart ID="Chart10" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
        <tr>
            <td>
                 <DCWC:Chart ID="Chart13" runat="server" Width="603px" Height="245px" Palette="Pastel" Visible="false">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Alignment="TopLeft" Color="245, 0, 0, 0"
                            Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>            
        </tr>
    </table>
    

</asp:Content>

