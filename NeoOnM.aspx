<%@ Page StylesheetTheme="Theme1" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="NeoOnM.aspx.cs" Inherits="NeoOnM" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table style="position:absolute; top:220px; left:230px;">
<tr> 
<td>
    <asp:Panel ID="Panel1" runat="server" BackColor="Transparent" BorderColor="Transparent"
        Font-Bold="True" Font-Overline="False" ForeColor="White" GroupingText="Parameters"
        Height="100px" Width="736px">
        <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="%Calls"
            Width="50px"></asp:Label>&nbsp; &nbsp;<asp:Label ID="Label2" runat="server" Font-Bold="True"
                ForeColor="White" Text="%Min"></asp:Label>
        &nbsp;&nbsp;
        <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="%ASR"></asp:Label>
        &nbsp;
        <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="White" Text="%ACD"></asp:Label><br />
        <asp:TextBox ID="txtCalls" runat="server" Width="50px" Enabled="False">0.25</asp:TextBox>
        <asp:TextBox ID="txtMin" runat="server" Width="50px" Enabled="False">0.25</asp:TextBox>
        <asp:TextBox ID="txtASR" runat="server" Width="50px" Enabled="False">0.25</asp:TextBox>
        <asp:TextBox ID="txtACD" runat="server" Width="50px" Enabled="False">0.25</asp:TextBox>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Button ID="btnShowGV"
            runat="server" OnClick="btnShowGV_Click" Text="Show Data Details" CssClass="boton" Width="106px" /></asp:Panel>
</td>
</tr>


<tr> 
<td>
    <table>
        <tr>
            <td align="left" style="width: 100px" valign="top">
    <asp:TreeView ID="tvNEO" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="Large"
        ForeColor="LimeGreen" MaxDataBindDepth="10" OnTreeNodePopulate="tvNEO_TreeNodePopulate" EnableClientScript="False" NodeIndent="115" OnTreeNodeExpanded="tvNEO_TreeNodePopulate" PopulateNodesFromClient="False">
        <Nodes>
            <asp:TreeNode Expanded="False" PopulateOnDemand="True" SelectAction="SelectExpand"
                Text="Recent Traffic" Value="OpSheetInc"></asp:TreeNode>
            <asp:TreeNode Expanded="False" PopulateOnDemand="True" SelectAction="SelectExpand"
                Text="Today Traffic" Value="OpSheetAll"></asp:TreeNode>            
        </Nodes>
    </asp:TreeView>
            </td>
            <td align="center" style="width: 100px" valign="middle">
    <asp:GridView ID="gvNeo" runat="server" CellPadding="3" ForeColor="Black" GridLines="Vertical"
        Style="left: 256px; top: 466px" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" Visible="False">
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />

    </asp:GridView>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>
    <asp:Label ID="Label5" runat="server" CssClass="labelTurkH" Text="MECCA Neo ON net"></asp:Label>



</asp:Content>

