<%@ Page Language="C#" MasterPageFile="~/Master.master" StylesheetTheme="Theme1"
    AutoEventWireup="true" CodeFile="CommTry.aspx.cs" Inherits="CommTry" Title="DIRECTO - Connections Worldwide" %>
<%@ OutputCache Duration="21600" Location="Client" VaryByParam="None" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label1" runat="server" Text="MECCA COMMISSIONS" CssClass="labelTurkH"></asp:Label>
       
           
    <table <%--style="position:absolute; top:220px; left:230px;" width="100%"--%>>
    <tr>
    <td style="height: 154px" valign="top" align="left">
        <br />
        <table>
            <tr>
                <td style="width: 100px">
                <asp:Label ID="Label2" runat="server" CssClass="labelSteelBlue" Text="Select Country Manager:" Width="128px"></asp:Label></td>
                <td style="width: 100px">
                <asp:DropDownList ID="dpdManager" runat="server" CssClass="dropdown" DataTextField="Broker" DataValueField="Broker" Visible="False">
                </asp:DropDownList></td>
                <td align="right" style="width: 100px">
                <asp:Label ID="Label5" runat="server" CssClass="labelSteelBlue" Text="From:"></asp:Label></td>
                <td align="left" style="width: 100px">
                <asp:TextBox ID="txtFrom" runat="server" Width="146px" CssClass="labelSteelBlue"></asp:TextBox></td>
                <td style="width: 100px">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            </tr>
            <tr>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
                <td align="right" style="width: 100px">
                <asp:Label ID="Label6" runat="server" CssClass="labelSteelBlue" Text="To:"></asp:Label></td>
                <td align="left" style="width: 100px">
                <asp:TextBox ID="txtTo" runat="server" Width="146px" CssClass="labelSteelBlue"></asp:TextBox></td>
                <td style="width: 100px">
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="Image1"
                        TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                    <cc1:CalendarExtender ID="CalendarExtender4" runat="server" PopupButtonID="Image2"
                        TargetControlID="txtTo">
                    </cc1:CalendarExtender>
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                    <asp:ScriptManager id="ScriptManager2" runat="server">
                    </asp:ScriptManager></td>
            </tr>
        </table>
        <asp:UpdateProgress id="UpdateProgress1" runat="server">
            <progresstemplate>
&nbsp; <TABLE><TBODY><TR><TD style="WIDTH: 100px"><asp:Label id="Label3" runat="server" Text="Loading Please Wait..." ForeColor="LimeGreen" Width="109px" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:Image id="imgLoading" runat="server" ImageUrl="~/Images/Loading.gif"></asp:Image></TD></TR></TBODY></TABLE>
</progresstemplate>
        </asp:UpdateProgress>
        <br />
    <table align="left" >
        <tr>
            <td align="right" colspan="5">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 100%; HEIGHT: 100%"><TBODY><TR><TD style="WIDTH: 100px" align=right><asp:Button id="cmdReport" onclick="cmdReport_Click" runat="server" Text="View Report" CssClass="boton"></asp:Button></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px" align=left><asp:Button id="Button1" onclick="Button1_Click" runat="server" Text="Export to EXCEL" Visible="False" CssClass="boton"></asp:Button></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px" align=left><asp:GridView id="gdvComisions" runat="server" OnPageIndexChanging="gdvComisions_PageIndexChanging" PageSize="25" AutoGenerateColumns="False" AllowPaging="True">
<FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:BoundField DataField="Customer" HeaderText="Customer">
<ItemStyle Width="160px" HorizontalAlign="Left" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle Width="160px" HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Vendor" HeaderText="Vendor">
<ItemStyle Width="160px" HorizontalAlign="Left" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle Width="160px" HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="CustBroker" HeaderText="CustBroker">
<ItemStyle Width="180px" HorizontalAlign="Left" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle Width="160px" HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="VendorBroker" HeaderText="VendorBroker">
<ItemStyle Width="160px" HorizontalAlign="Left" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle Width="160px" HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0}" DataField="AnsweredCalls" HeaderText="AnsweredCalls">
<ItemStyle Width="160px" HorizontalAlign="Left" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle Width="160px" HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0}" DataField="CMinutes" HeaderText="CMinutes">
<ItemStyle Width="180px" HorizontalAlign="Left" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0}" DataField="VMinutes" HeaderText="VMinutes">
<ItemStyle Width="180px" HorizontalAlign="Left" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:f2}" DataField="TotalCost" HeaderText="TotalCost">
<ItemStyle Width="160px" HorizontalAlign="Left" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:f2}" DataField="TotalSales" HeaderText="TotalSales">
<ItemStyle Width="160px" HorizontalAlign="Left" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:f2}" DataField="DirectoProfit" HeaderText="Directo Profit">
<ItemStyle Width="160px" HorizontalAlign="Left" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="${0:f2}" DataField="GrossProfit" HeaderText="Gross Profit">
<ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:0,0}" DataField="Minutes" HeaderText="TotalMinutes">
<ItemStyle HorizontalAlign="Left" Font-Size="9pt" Font-Names="Arial"></ItemStyle>

<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
</Columns>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="9pt" Font-Names="Arial" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR></TBODY></TABLE>
</contenttemplate>

      <Triggers>
<asp:PostBackTrigger ControlID="Button1"></asp:PostBackTrigger>
</Triggers>
                </asp:UpdatePanel>
                </td>
        </tr>
        <tr>
            <td align="right" style="width: 135px">
            </td>
            <td align="right" style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        
        <tr>
            <td align="right" style="width: 135px">
               
            </td>
            <td align="left" style="width: 100px">
                &nbsp;</td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        
    </table>
    </td>
    </tr>
    
    <tr>
    <td>
     
    </td>
    </tr>
    
   
    <tr>
    <td align="left">
    <table <%--style="position: absolute; top: 710; left: 230;"--%> >
        <tr>
        <td>
            &nbsp;</td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" style="height: 60px">
            </td>
        </tr>
        
        
         <tr>
            <td width="100%" >
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="left" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td align="left" style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <asp:SqlDataSource ID="sqlBroker" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                SelectCommand="SELECT CustBroker AS Broker FROM CBroker UNION SELECT VendorBroker AS Broker FROM VBroker WHERE (VendorBroker <> 'N/A')">
            </asp:SqlDataSource>
        </tr>
    </table>
    
    </td>
    </tr>
    </table>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                SelectCommand="SELECT CustBroker AS Broker FROM CBroker WHERE (CustBroker <> 'marc') AND (CustBroker <> 'shi jie') UNION SELECT VendorBroker AS Broker FROM VBroker WHERE (VendorBroker <> 'N/A') AND (VendorBroker <> 'marc') AND (VendorBroker <> 'shi jie')">
            </asp:SqlDataSource>    
</asp:Content>
