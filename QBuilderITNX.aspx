<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="QBuilderITNX.aspx.cs" Inherits="QBuilder" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <table>
        <tr>
            <td width="30%">
                <asp:Label ID="LabelHeader" runat="server" Text="MECCA ADVANCE QUERY BUILDER" Width="250px" Font-Bold="True" CssClass="labelTurk"></asp:Label></td>
            <td width="70%">
               </td>
        </tr>
        <tr>
            <td height="100%" width="30%">
                
    <table align="left" width="100%">
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                <asp:Label ID="lblCustomer" runat="server" Text="Customer" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCustomer" runat="server" DataTextField="ProviderName" DataValueField="ProviderName" Width="130px" CssClass="dropdown">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                <asp:Label ID="lblVendor" runat="server" Text="Vendor" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdVendor" runat="server" DataTextField="ProviderName" DataValueField="ProviderName" Width="130px" CssClass="dropdown">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                <asp:Label ID="lblOriginationIP" runat="server" Text="Origination IP" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdOriginationIP" runat="server" DataTextField="OriginationIP" DataValueField="OriginationIP" Width="130px" CssClass="dropdown">
                    <asp:ListItem>** NONE **</asp:ListItem>
                    <asp:ListItem>**ALL**</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px; height: 28px;" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="lblTerminationIP" runat="server" Text="Termination IP" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px; height: 28px;">
                <asp:DropDownList ID="dpdTerminationIP" runat="server" DataTextField="TerminationIP" DataValueField="TerminationIP" Width="130px" CssClass="dropdown">
                    <asp:ListItem Value="N">**NONE**</asp:ListItem>
                    <asp:ListItem Value="*">**ALL**</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 100px; height: 28px;">
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 84px">
                <asp:Label ID="lblCountry" runat="server" Text="Country" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCountry" runat="server" AutoPostBack="True" CssClass="dropdown"
                    DataTextField="Country" DataValueField="Country" OnSelectedIndexChanged="dpdCountry_SelectedIndexChanged"
                    Width="130px">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 84px">
                <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdRegions" runat="server" CssClass="dropdown" DataTextField="Region"
                    DataValueField="Region" Width="130px">
                </asp:DropDownList></td>
            <td style="width: 100px">
                <asp:CheckBox ID="chkDetail" runat="server" Text="Detail" /></td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="lblType" runat="server" CssClass="labelBlue8" Text="Type"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdType" runat="server" DataTextField="Type" DataValueField="Type" Width="130px" CssClass="dropdown">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px; height: 22px;" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="lblClass" runat="server" Text="Class" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px; height: 22px;">
                <asp:DropDownList ID="dpdClass" runat="server" Width="130px" CssClass="dropdown">
                </asp:DropDownList></td>
            <td style="width: 100px; height: 22px;">
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 84px; height: 22px">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="LMC"></asp:Label></td>
            <td style="width: 100px; height: 22px">
                <asp:DropDownList ID="dpdLmc" runat="server" CssClass="dropdown" Width="130px">
                </asp:DropDownList></td>
            <td style="width: 100px; height: 22px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                &nbsp;<asp:Label ID="lblSourceMSW" runat="server" Text="Source MSW" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdSourceMSW" runat="server" Width="130px" CssClass="dropdown" DataTextField="IP" DataValueField="IP">
                    <asp:ListItem Value="%">** NONE **</asp:ListItem>
                    <asp:ListItem Value="**ALL**">**ALL**</asp:ListItem>                    
                    <%--<asp:ListItem>NY03</asp:ListItem>
                    <asp:ListItem>NY04</asp:ListItem>
                    <asp:ListItem>NY06</asp:ListItem>
                    <asp:ListItem>NY07</asp:ListItem>
                    <asp:ListItem>MIA03</asp:ListItem>                                         
                    <asp:ListItem>EZMIA03</asp:ListItem>                                                        
                    <asp:ListItem>STCB</asp:ListItem> 
                    <asp:ListItem>MIA06</asp:ListItem>
                    <asp:ListItem>MIA08</asp:ListItem>
                    <asp:ListItem>MIA02</asp:ListItem>--%>
                    <asp:ListItem>MEX</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 84px"> &nbsp;<asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdReportType" runat="server" Width="130px" CssClass="dropdown">
                    <asp:ListItem Value="OpSheetInc_ITNX">Recent Traffic</asp:ListItem>
                    <asp:ListItem Value="OpSheetAll_ITNX">Today's Traffic</asp:ListItem>
                    <asp:ListItem Value="OpSheetY_ITNX">Yesterday's Traffic</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 100px">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 84px">
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Gateway"></asp:Label></td>
            <td align="left" style="width: 100px">
                <asp:DropDownList ID="dpdGateway" runat="server" CssClass="dropdown"
                    Width="130px" DataSourceID="sqlGateway" DataTextField="IP" DataValueField="IP">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 84px">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Rows per page"
                    Width="76px"></asp:Label></td>
            <td align="left" style="width: 100px">
                <asp:TextBox ID="txtPageSize" runat="server" CssClass="dropdown" Width="120px"></asp:TextBox></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px">
            </td>
            <td align="left" colspan="2">
                
                <asp:Button ID="cmdQbuilder" runat="server" OnClick="cmdQbuilder_Click" Text="ViewReport" BorderStyle="Solid" BorderWidth="1px" Font-Bold="False" CssClass="boton" /></td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                &nbsp;</td>
            <td align="right" style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="height: 136px" align="left" colspan="2" valign="top">
               <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT DISTINCT IP FROM MECCA2..VportViewer&#13;&#10;UNION&#13;&#10;SELECT '**ALL**' AS IP FROM MECCA2..VportViewer">
    </asp:SqlDataSource>
                <asp:SqlDataSource ID="sqlGateway" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    SelectCommand="SELECT DISTINCT IP FROM ProviderIP WHERE (Type LIKE 'G') AND (IP <> '') UNION SELECT '** NONE **' AS IP FROM ProviderIP AS ProviderIP_1 UNION SELECT '**ALL**' AS IP FROM ProviderIP AS ProviderIP_2">
                </asp:SqlDataSource>
    <asp:SqlDataSource ID="OriginationIPS" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT DISTINCT OriginationIP FROM OpSheetInc_ITNX&#13;&#10;where OriginationIP <>''&#13;&#10;UNION &#13;&#10;SELECT '** NONE **' AS OriginationIP FROM OpSheetInc_ITNX AS OpSheetInc_2 &#13;&#10;UNION &#13;&#10;SELECT '**ALL**' AS OriginationIP FROM OpSheetInc_ITNX AS OpSheetInc_1 ORDER BY OriginationIP">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="TerminationIPS" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="select distinct TerminationIP from OpSheetInc_ITNX where TerminationIP <> ''&#13;&#10;union &#13;&#10;select '** NONE **' as TerminationIP from OpSheetInc_ITNX&#13;&#10;union&#13;&#10;select '**ALL**' as TerminationIP from OpSheetInc_ITNX&#13;&#10;order by TerminationIP ASC">
    </asp:SqlDataSource>
            </td>
            <td align="left" style="width: 100px; height: 136px" valign="top">
                &nbsp; &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
               
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td width="30%">
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
 
 
 
 
 <table visible="false" width="100%" style="position: absolute; top: 950px; left: 230px;">
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="right" colspan="3">
                            <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        
    </table>
</asp:Content>

