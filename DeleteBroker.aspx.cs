using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DeleteBroker : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            dpdBroker.DataSource = SqlDataSource1;
            dpdBroker.DataBind();
        }
        else
        { 
            
        
        }
    }
    protected void cmdDelete_Click(object sender, EventArgs e)
    {

        string broker = dpdBroker.SelectedValue.ToString();
        AccessDatasetTableAdapters.SIMInfoTableAdp adp = new AccessDatasetTableAdapters.SIMInfoTableAdp();
        AccessDataset.SIMInfoDataTable tbl = adp.GetDataByBroker(Convert.ToInt32(broker));

        AccessDatasetTableAdapters.BrokerTableAdapter adpB = new AccessDatasetTableAdapters.BrokerTableAdapter();
        AccessDataset.BrokerDataTable tblB = adpB.GetDataByBroker(broker);
        
        
        if (tbl.Rows.Count > 0)
        { //        Tiene al menos un Sim asignado

            lblDelete.Text = "This Broker can't be deleted";
        }
        else
        { // Borrarlo

            adpB.Delete(  Convert.ToInt32(tblB.Rows[0]["IDBroker"].ToString())  );
            lblDelete.Text = "Broker deleted";
            dpdBroker.DataSource = SqlDataSource1;
            dpdBroker.DataBind();
        }


    }
}
