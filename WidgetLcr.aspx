<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="WidgetLcr.aspx.cs" Inherits="WidgetLcr" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px" valign="top">
    <table>
        <tr>
            <td style="width: 100px; height: 21px;">
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Widget LCR entry" Font-Bold="True" Width="101px"></asp:Label></td>
            <td style="width: 100px; height: 21px;">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" height="20">
            </td>
            <td style="width: 100px" height="20">
            </td>
        </tr>
        <tr>
            <td bgcolor="#d5e3f0" style="width: 100px" align="center">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="CountryLCR"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCountryLcr" runat="server" CssClass="dropdown" DataSourceID="SqlDataSource2" DataTextField="Country" DataValueField="Country">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td bgcolor="#d5e3f0" style="width: 100px" align="center">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Responsible"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdOwner" runat="server" CssClass="dropdown">
                    <asp:ListItem Value="0">Commercial Support</asp:ListItem>
                    <asp:ListItem Value="1">Country Manager</asp:ListItem>
                    <asp:ListItem Value="2">NOC</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Status"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdStatus" runat="server" CssClass="dropdown">
                    <asp:ListItem Value="2">Applied</asp:ListItem>
                    <asp:ListItem Value="0">Completed</asp:ListItem>
                    <asp:ListItem Value="1">Pending Application</asp:ListItem>
                    <asp:ListItem Value="3">Pending-CM</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px; height: 22px;">
                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Priority"></asp:Label></td>
            <td style="width: 100px; height: 22px;">
                <asp:DropDownList ID="dpdPriority" runat="server" CssClass="dropdown">
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Date Received"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtReceived" runat="server" CssClass="dropdown" Width="50px" Wrap="False"></asp:TextBox>
                <asp:Image ID="imgRec" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Requested by"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCtrys" runat="server" CssClass="dropdown" DataSourceID="SqlDataSource3"
                    DataTextField="Broker" DataValueField="Broker">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px; height: 36px">
                <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Text="Type"></asp:Label></td>
            <td style="width: 100px; height: 36px">
                <asp:RadioButtonList ID="rdbTypeOf" runat="server" CssClass="labelSteelBlue" Font-Names="Arial"
                    Font-Size="7pt" RepeatDirection="Horizontal">
                    <asp:ListItem>Retail LCR</asp:ListItem>
                    <asp:ListItem>Wholesale LCR</asp:ListItem>
                    <asp:ListItem>Special</asp:ListItem>
                </asp:RadioButtonList></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px; height: 36px">
                <asp:Label ID="Label9" runat="server" CssClass="labelBlue8" Text="Comments"></asp:Label></td>
            <td style="width: 100px; height: 36px">
                <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="center" style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:Button ID="cmdInsert" runat="server" CssClass="boton" Text="Save" OnClick="cmdInsert_Click" /></td>
        </tr>
    </table>
               
                </td>
            <td style="width: 100px" valign="top">
                <table>
                    <tr>
                        <td style="width: 100px">
                            <br />
                            <br />
                            <asp:GridView ID="GridView1" runat="server" Font-Names="Arial" Font-Size="8pt"
                            AutoGenerateColumns="False" AutoGenerateEditButton="True" DataSourceID="SqlDataSource1" 
                            OnRowEditing="GridView1_RowEditing" OnRowUpdated="GridView1_RowUpdated" DataKeyNames="idEntry" AutoGenerateDeleteButton="True" Width="708px" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowUpdating="GridView1_RowUpdating" OnRowDeleting="GridView1_RowDeleting">
                                <HeaderStyle CssClass="titleOrangegrid" />
                                <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="idEntry" Visible="False" />
                                    <asp:TemplateField HeaderText="Country" SortExpression="countryLcr">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dpdCtrysIn" runat="server" CssClass="dropdown" 
                                            SelectedValue='<%# Bind("countryLcr") %>' DataSourceID="SqlDataSource5" 
                                            DataTextField="Country" DataValueField="Country">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                                                ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>" SelectCommand="SELECT DISTINCT Country FROM Regions">
                                            </asp:SqlDataSource>
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("countryLcr") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Responsible" SortExpression="owner">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dpdOwner2" runat="server" CssClass="dropdown" 
                                            SelectedValue='<%# Bind("owner") %>'>
                                            <asp:ListItem>Commercial Support</asp:ListItem>
                                            <asp:ListItem>Country Manager</asp:ListItem>
                                            <asp:ListItem>NOC</asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("owner") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" SortExpression="status">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dpdStatus2" runat="server" CssClass="dropdown" SelectedValue='<%# Bind("status") %>'>
                                                <asp:ListItem>Applied</asp:ListItem>
                                                <asp:ListItem>Completed</asp:ListItem>
                                                <asp:ListItem>Pending Application</asp:ListItem>
                                                <asp:ListItem>Pending-CM</asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Priority" HeaderText="Priority" SortExpression="priority" >
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Date_Received" SortExpression="Date_Received">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Enabled="false" Text='<%# Bind("Date_Received") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("Date_Received") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date Completed" SortExpression="Date_Completed">
                                        <EditItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Date_Completed") %>' CssClass="dropdown">
                                                        </asp:TextBox></td>
                                                    <td>
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif"/>
                                                    </td>
                                                </tr>
                                            </table>
                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="Image1"
                                                TargetControlID="TextBox2">
                                            </cc1:CalendarExtender>
                                        </EditItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Date_Completed") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comments" SortExpression="Comments">
                                        <EditItemTemplate>
                                        <asp:TextBox ID="txtComments2" runat="server" Text='<%# Bind("Comments") %>'></asp:TextBox>

                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelComments" runat="server" Text='<%# Bind("Comments") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Requested_By" SortExpression="RequestedBy">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="sqlRequested" CssClass="dropdown"
                                                DataTextField="Broker" DataValueField="Broker" SelectedValue='<%# Bind("RequestedBy") %>'>
                                            </asp:DropDownList><asp:SqlDataSource ID="sqlRequested" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                                                SelectCommand="SELECT DISTINCT Broker FROM Accounts_CM"></asp:SqlDataSource>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("RequestedBy") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:BoundField DataField="TypeOf" HeaderText="LCR Type" SortExpression="TypeOf" ReadOnly="True" />
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" 
                SelectCommand="SELECT idEntry, countryLcr, owner, status, priority, Date_received, Date_Completed, RequestedBy, TypeOf, Comments FROM WidgetEntry" UpdateCommand="UPDATE WidgetEntry SET countryLcr = @countryLcr, owner = @owner, status = @status, priority = @priority, Date_received = @Date_received, Date_Completed = @Date_Completed, RequestedBy = @RequestedBy, Comments = @Comments WHERE (idEntry = @idEntry)" DeleteCommand="DELETE FROM WidgetEntry WHERE (idEntry = @idEntry)">
                    <UpdateParameters>
                        <asp:Parameter Name="countryLcr" />
                        <asp:Parameter Name="owner" />
                        <asp:Parameter Name="status" />
                        <asp:Parameter Name="priority" />
                        <asp:Parameter Name="Date_received" />
                        <asp:Parameter Name="Date_Completed" />
                        <asp:Parameter Name="RequestedBy" />
                        <asp:Parameter Name="idEntry" />
                        <asp:Parameter Name="Comments" />
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="idEntry" />
                    </DeleteParameters>
                 
                </asp:SqlDataSource> 
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    SelectCommand="SELECT DISTINCT Country FROM Regions"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                    SelectCommand="SELECT DISTINCT Broker FROM Accounts_CM" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
                </asp:SqlDataSource>
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <cc1:calendarextender id="CalendarExtender1" runat="server" popupbuttonid="imgRec"
                    targetcontrolid="txtReceived"></cc1:calendarextender>

                <cc1:Accordion runat="server" ID="ajxAcc" Visible="false" 
                DataSourceID="SqlDataSource1" TransitionDuration="900" AutoSize="Limit"
                    CssClass="dropdown" Height="150px" Width="300px">
                    <HeaderTemplate>
                        <table>
                        <tr>
                        <td><%# Server.HtmlDecode(Eval("countryLcr").ToString()) %></td>
                        <td><%# Server.HtmlDecode(Eval("Date_received").ToString())%></td>
                        </tr>
                        </table>
                        
                    </HeaderTemplate>
                    <ContentTemplate>
                        <p>
                            <%# Server.HtmlDecode(Eval("countryLcr").ToString()) %>
                        </p>
                    </ContentTemplate>
                </cc1:Accordion>
            </td>
        </tr>
    </table>
    <br />
    
</asp:Content>

