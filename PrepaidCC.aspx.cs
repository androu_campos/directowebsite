using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

public partial class PrepaidCC : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string cmd = string.Empty;

        if (lbCallCenter.SelectedItem != null)
        {

        }
        else
        {
            if (!Page.IsPostBack)
            {
                SqlConnection SqlConn1 = new SqlConnection();
                SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
                SqlCommand SqlCommand1 = null;

                //CallCenter

                SqlCommand1 = new SqlCommand("SELECT DISTINCT RazonSocial from MECCA2.DBO.RAZONSOCIALCC where CallCenter not like '%DID%' UNION SELECT '**SELECT ONE**' ", SqlConn1);

                SqlConn1.Open();
                SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
                while (myReader1.Read())
                {
                    dpdRz.Items.Add(myReader1.GetValue(0).ToString());
                }
                myReader1.Close();
                SqlConn1.Close();


            }
            else
            {

            }
        }
    }
    protected void cmdQbuilder_Click(object sender, EventArgs e)
    {
        string rz, id, cc, user;
        int cont;
        double amount;
        ListItemCollection prepaidCC;

        user = Session["DirectoUser"].ToString();

        id = "";
        cc = "";
        cont = 0;
        rz = dpdRz.SelectedValue.ToString();
        amount = double.Parse(txtAmount.Text.Trim());

        prepaidCC = lbCallCenterPP.Items;
        cont = prepaidCC.Count;

        SqlConnection SqlConn1 = new SqlConnection();
        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand SqlCommand1 = null;

        for (int i = 0; i <= cont; i++)
        {
            cc = prepaidCC[cont - 1].Text.ToString();

            SqlCommand1 = new SqlCommand("SELECT IP from PROVIDERIP WHERE PROVIDERNAME LIKE '" + cc + "' ", SqlConn1);

            SqlConn1.Open();
            SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
            while (myReader1.Read())
            {
                id = myReader1.GetValue(0).ToString();
            }
            myReader1.Close();

            //GET CallCenter
            SqlCommand1 = new SqlCommand("SELECT CallCenter FROM MECCA2.DBO.RAZONSOCIALCC WHERE CALLCENTER LIKE '" + cc + "' ", SqlConn1);
            string CallCenter = "";
            SqlDataReader myReader2 = SqlCommand1.ExecuteReader();
            while (myReader2.Read())
            {
                CallCenter = myReader2.GetValue(0).ToString();
            }
            myReader2.Close();

            //INSERT
            SqlCommand1 = new SqlCommand("INSERT INTO TALKTEL.DBO.PREPAIDCC VALUES ('" + id + "','" + CallCenter + "','" + rz + "',getdate()," + amount + ",'" + user + "' )", SqlConn1);
            SqlDataReader myReader3 = SqlCommand1.ExecuteReader();
            myReader3.Close();

            SqlConn1.Close();

            cont = cont - 1;

        }
        lblNotFound.Text = "Prepaid Inserted";
        lblNotFound.Visible = true;

        txtAmount.Text = "";
        lbCallCenterPP.Items.Clear();
        //    Response.Redirect("~/PrepaidCC.aspx");

    }

    private System.Data.DataSet RunQuery(StringBuilder qry, string StringConnection)
    {

        System.Data.SqlClient.SqlCommand sqlQuery = new System.Data.SqlClient.SqlCommand();
        sqlQuery.CommandText = qry.ToString();


        string connectionString = ConfigurationManager.ConnectionStrings
        [StringConnection].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        gdvPPCCs.Visible = true;

        SqlConnection SqlConn1 = new SqlConnection();
        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=TalkTel;User ID=steward_of_Gondor;Password=nm@73-mg";

        //CallCenter

        StringBuilder SqlCommand1 = new StringBuilder("SELECT CallCenter CallCenter, RazonSocial Company, MontoPrepago Amount, CONVERT(VARCHAR(11),Fecha,101) Date from Talktel.dbo.PrepaidCC ");
        DataSet myDataSet = RunQuery(SqlCommand1, "CDRDBConnectionString");

        try
        {
            gdvPPCCs.DataSource = myDataSet.Tables[0];
            gdvPPCCs.DataBind();
        }
        catch (Exception ex)
        {
            lblNotFound.Visible = true;
            lblNotFound.Text = "Nothing Found!";
            //lblNotFound.Text = SqlCommand1.ToString();
        }
        btnShow.Visible = false;
        btnHide.Visible = true;
    }
    protected void btnHide_Click(object sender, EventArgs e)
    {
        gdvPPCCs.Visible = false;
        btnShow.Visible = true;
        btnHide.Visible = false;
    }
    protected void dpdRz_SelectedIndexChanged(object sender, EventArgs e)
    {
        String rz = dpdRz.SelectedItem.ToString();

        lbCallCenter.Items.Clear();

        SqlConnection SqlConn1 = new SqlConnection();
        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand SqlCommand1 = null;

        //CallCenter

        SqlCommand1 = new SqlCommand("SELECT DISTINCT CallCenter from MECCA2.DBO.RAZONSOCIALCC WHERE RAZONSOCIAL = '" + rz + "' ORDER BY CALLCENTER", SqlConn1);

        SqlConn1.Open();
        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
        while (myReader1.Read())
        {
            lbCallCenter.Items.Add(myReader1.GetValue(0).ToString());
        }

        myReader1.Close();
        SqlConn1.Close();

        lbCallCenterPP.Items.Clear();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string PrepaidCC;

        try
        {
            PrepaidCC = lbCallCenter.SelectedItem.Text.ToString();

            lbCallCenterPP.Items.Add(PrepaidCC);

            lbCallCenter.Items.Remove(lbCallCenter.SelectedItem);
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        try
        {
            lbCallCenter.Items.Add(lbCallCenterPP.SelectedItem);

            lbCallCenterPP.Items.Remove(lbCallCenterPP.SelectedItem);
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnAddAll_Click(object sender, EventArgs e)
    {
        int cont = lbCallCenter.Items.Count - 1;

        for (int i = cont; i >= 0; i--)
        {
            lbCallCenterPP.Items.Add(lbCallCenter.Items[i]);
            lbCallCenter.Items.RemoveAt(i);
        }

    }
    protected void btnRemAll_Click(object sender, EventArgs e)
    {
        int cont = lbCallCenterPP.Items.Count - 1;

        for (int i = cont; i >= 0; i--)
        {
            lbCallCenter.Items.Add(lbCallCenterPP.Items[i]);
            lbCallCenterPP.Items.RemoveAt(i);
        }
    }
}
