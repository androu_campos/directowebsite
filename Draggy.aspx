<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Draggy.aspx.cs" Inherits="Draggy" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Panel ID="pnlA" runat="server" Height="50px" Width="125px">
        <table>
            <tr>
                <td style="width: 100px">
                    DRAG ME</td>
            </tr>
            <tr>
                <td style="width: 100px">
    <asp:GridView ID="myGrid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" >
        <Columns>
            <asp:BoundField DataField="idEntry" HeaderText="idEntry" InsertVisible="False" ReadOnly="True"
                SortExpression="idEntry" />
            <asp:BoundField DataField="countryLcr" HeaderText="countryLcr" SortExpression="countryLcr" />
            <asp:BoundField DataField="owner" HeaderText="owner" SortExpression="owner" />
            <asp:BoundField DataField="status" HeaderText="status" SortExpression="status" />
            <asp:BoundField DataField="priority" HeaderText="priority" SortExpression="priority" />
            <asp:BoundField DataField="meccaUser" HeaderText="meccaUser" SortExpression="meccaUser" />
            <asp:BoundField DataField="Date_received" HeaderText="Date_received" SortExpression="Date_received" />
            <asp:BoundField DataField="Date_Completed" HeaderText="Date_Completed" SortExpression="Date_Completed" />
            <asp:BoundField DataField="RequestedBy" HeaderText="RequestedBy" SortExpression="RequestedBy" />
            <asp:BoundField DataField="TypeOf" HeaderText="TypeOf" SortExpression="TypeOf" />
        </Columns>
    
    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                </td>
            </tr>
        </table>
        <br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
            SelectCommand="SELECT * FROM [WidgetEntry]"></asp:SqlDataSource>
    
    </asp:Panel>
    <asp:ScriptManager id="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <cc1:collapsiblepanelextender id="CollapsiblePanelExtender1" runat="server"></cc1:collapsiblepanelextender>
    <br />

</asp:Content>

