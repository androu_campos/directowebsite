using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class Live_v2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            StreamReader FSOlive = new StreamReader(@"C:\Inetpub\wwwroot\live.html", System.Text.ASCIIEncoding.Default);
            StreamReader FSOqtmon = new StreamReader(@"C:\Inetpub\wwwroot\qtmon.html", System.Text.ASCIIEncoding.Default);
            //StreamReader FSOpremonny2 = new StreamReader(@"D:\upload\premonNY02.htm", System.Text.ASCIIEncoding.Default);
            //StreamReader FSOpremonny = new StreamReader(@"D:\upload\premonNY01.htm", System.Text.ASCIIEncoding.Default);
            //StreamReader FSOntraffic2 = new StreamReader(@"D:\upload\ntrafficNY01.html", System.Text.ASCIIEncoding.Default);
            //StreamReader FSOntraffic3 = new StreamReader(@"D:\upload\ntrafficNY02.html", System.Text.ASCIIEncoding.Default);
            StreamReader FSOntraffic = new StreamReader(@"D:\upload\ntrafficLA01.html", System.Text.ASCIIEncoding.Default);
            StreamReader FSOpremonla = new StreamReader(@"D:\upload\premonLA01.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOpremonla2 = new StreamReader(@"D:\upload\premonLA02.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOpremonla4 = new StreamReader(@"D:\upload\ntrafficLA02.html", System.Text.ASCIIEncoding.Default);
            
            StreamReader FSOpremonNY03 = new StreamReader(@"D:\upload\premonNY03.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOntrafficNY03 = new StreamReader(@"D:\upload\ntrafficNY03.html", System.Text.ASCIIEncoding.Default);
            
            StreamReader FSOpremonNY04 = new StreamReader(@"D:\upload\ny04-premon.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOntrafficNY04 = new StreamReader(@"D:\upload\ny04-network-traffic.html", System.Text.ASCIIEncoding.Default);
            
            //StreamReader FSOpremonmi02 = new StreamReader(@"D:\upload\premonMI02.htm", System.Text.ASCIIEncoding.Default);
            //StreamReader FSOntrafficMI02 = new StreamReader(@"D:\upload\ntrafficMI02.html", System.Text.ASCIIEncoding.Default);

            StreamReader FSOpremonNY07 = new StreamReader(@"D:\upload\premonNY07.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOntrafficNY07 = new StreamReader(@"D:\upload\ntrafficNY07.html", System.Text.ASCIIEncoding.Default);

            StreamReader FSOpremonNY06 = new StreamReader(@"D:\upload\premonNY06.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOntrafficNY06 = new StreamReader(@"D:\upload\ntrafficLA05.html", System.Text.ASCIIEncoding.Default);
            StreamReader FSFSOntrafficNY06V = new StreamReader(@"D:\upload\ntrafficNY06.html", System.Text.ASCIIEncoding.Default);

            StreamReader FSOpremonMIA03 = new StreamReader(@"D:\upload\premonMIA03.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOntrafficMIA03 = new StreamReader(@"D:\upload\ntrafficMIA03.html", System.Text.ASCIIEncoding.Default);


            lblLive.Text = FSOlive.ReadToEnd();
            /**/
            lblQtmon.Text = Util.getLiveTrafficWatch("CCEN_82", "OPRF_19");

            //lblPremonny2.Text = FSOpremonny2.ReadToEnd();
            //lblPremonny.Text = FSOpremonny.ReadToEnd();
            //lblNtraffic2.Text = FSOntraffic2.ReadToEnd();
            //lblNtraffic3.Text = FSOntraffic3.ReadToEnd();
            lblNtraffic.Text = FSOntraffic.ReadToEnd();
            lblPremonla.Text = FSOpremonla.ReadToEnd();
            lblPremonla2.Text = FSOpremonla2.ReadToEnd();
            lblNtraffic4.Text = FSOpremonla4.ReadToEnd();
            lblpremonNY03.Text = FSOpremonNY03.ReadToEnd();
            lblntrafficNY03.Text = FSOntrafficNY03.ReadToEnd();
            //lblPremonnMI02.Text = FSOpremonmi02.ReadToEnd();
            //lblNtrafficMI02.Text = FSOntrafficMI02.ReadToEnd();
            
            lblNtrafficNY4.Text = FSOntrafficNY04.ReadToEnd();
            lblPremonNY4.Text = FSOpremonNY04.ReadToEnd();

            lblPremonNY06.Text = FSOpremonNY06.ReadToEnd();
            lblNtrafficNY6.Text = FSOntrafficNY06.ReadToEnd();
            lblNtrafficNY6.Text=lblNtrafficNY6.Text+FSFSOntrafficNY06V.ReadToEnd();

            lblPremonNY07.Text = FSOpremonNY07.ReadToEnd();
            lblNtrafficNY7.Text = FSOntrafficNY07.ReadToEnd();

            lblPremonMIA03.Text = FSOpremonMIA03.ReadToEnd();
            lblNtrafficMIA03.Text = FSOntrafficMIA03.ReadToEnd();


            FSOlive.Close();
            FSOqtmon.Close();
            //FSOpremonny2.Close();
            //FSOpremonny.Close();
            //FSOntraffic2.Close();
            //FSOntraffic3.Close();
            FSOntraffic.Close();
            FSOpremonla.Close();
            FSOpremonla2.Close();
            FSOpremonla4.Close();
            FSOntrafficNY03.Close();
            FSOpremonNY03.Close();
            //FSOpremonmi02.Close();
            //FSOntrafficMI02.Close();
            
            FSOntrafficNY04.Close();
            FSOpremonNY04.Close();

            FSOpremonNY06.Close();
            FSOntrafficNY06.Close();
            FSFSOntrafficNY06V.Close();

            FSOpremonNY07.Close();
            FSOntrafficNY07.Close();

            FSOpremonMIA03.Close();
            FSOntrafficMIA03.Close();
            getStatus(sender, e);


        }

        catch (Exception ex)
        {         
            //StreamReader FSOpremonny2 = new StreamReader(@"D:\upload\premonNY02.htm", System.Text.ASCIIEncoding.Default);
            //StreamReader FSOpremonny = new StreamReader(@"D:\upload\premonNY01.htm", System.Text.ASCIIEncoding.Default);
            //StreamReader FSOntraffic2 = new StreamReader(@"D:\upload\ntrafficNY01.html", System.Text.ASCIIEncoding.Default);
            //StreamReader FSOntraffic3 = new StreamReader(@"D:\upload\ntrafficNY02.html", System.Text.ASCIIEncoding.Default);

            StreamReader FSOpremonNY03 = new StreamReader(@"D:\upload\premonNY03.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOntrafficNY03 = new StreamReader(@"D:\upload\ntrafficNY03.html", System.Text.ASCIIEncoding.Default);
            //StreamReader FSOpremonmi02 = new StreamReader(@"D:\upload\premonMI02.htm", System.Text.ASCIIEncoding.Default);
            //StreamReader FSOntrafficMI02 = new StreamReader(@"D:\upload\ntrafficMI02.html", System.Text.ASCIIEncoding.Default);

            StreamReader FSOpremonNY04 = new StreamReader(@"D:\upload\premonNY04.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOntrafficNY04 = new StreamReader(@"D:\upload\ntrafficNY04.html", System.Text.ASCIIEncoding.Default);

            StreamReader FSOpremonNY06 = new StreamReader(@"D:\upload\premonNY06.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOntrafficNY06 = new StreamReader(@"D:\upload\ntrafficLA05.html", System.Text.ASCIIEncoding.Default);
            StreamReader FSFSOntrafficNY06V = new StreamReader(@"D:\upload\ntrafficNY06.html", System.Text.ASCIIEncoding.Default);

            StreamReader FSOpremonNY07 = new StreamReader(@"D:\upload\premonNY07.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOntrafficNY07 = new StreamReader(@"D:\upload\ntrafficNY07.html", System.Text.ASCIIEncoding.Default);

            StreamReader FSOpremonMIA03 = new StreamReader(@"D:\upload\premonMIA03.htm", System.Text.ASCIIEncoding.Default);
            StreamReader FSOntrafficMIA03 = new StreamReader(@"D:\upload\ntrafficMIA03.html", System.Text.ASCIIEncoding.Default);


            lblQtmon.Text = "QTMON not available, please wait a few minutes";
            //lblPremonny2.Text = FSOpremonny2.ReadToEnd();
            //lblPremonny.Text = FSOpremonny.ReadToEnd();
            //lblNtraffic2.Text = FSOntraffic2.ReadToEnd();
            //lblNtraffic3.Text = FSOntraffic3.ReadToEnd();
            lblpremonNY03.Text = FSOpremonNY03.ReadToEnd();
            lblntrafficNY03.Text = FSOntrafficNY03.ReadToEnd();
            //lblPremonnMI02.Text = FSOpremonmi02.ReadToEnd();
            //lblNtrafficMI02.Text = FSOntrafficMI02.ReadToEnd();
            lblNtrafficNY4.Text = FSOntrafficNY04.ReadToEnd();
            lblPremonNY4.Text = FSOpremonNY04.ReadToEnd();

            lblPremonNY06.Text = FSOpremonNY06.ReadToEnd();
            lblNtrafficNY6.Text = FSOntrafficNY06.ReadToEnd();
            lblNtrafficNY6.Text = lblNtrafficNY6.Text + FSFSOntrafficNY06V.ReadToEnd();

            lblPremonNY07.Text = FSOpremonNY07.ReadToEnd();
            lblNtrafficNY7.Text = FSOntrafficNY07.ReadToEnd();

            lblPremonMIA03.Text = FSOpremonMIA03.ReadToEnd();
            lblNtrafficMIA03.Text = FSOntrafficMIA03.ReadToEnd();

            //FSOpremonny2.Close();
            //FSOpremonny.Close();
            //FSOntraffic2.Close();
            //FSOntraffic3.Close();
            FSOntrafficNY03.Close();            
            FSOpremonNY03.Close();
            //FSOpremonmi02.Close();
            //FSOntrafficMI02.Close();
            FSOntrafficNY04.Close();
            FSOpremonNY04.Close();

            FSOpremonNY06.Close();
            FSOntrafficNY06.Close();
            FSFSOntrafficNY06V.Close();

            FSOpremonNY07.Close();
            FSOntrafficNY07.Close();

            FSOpremonMIA03.Close();
            FSOntrafficMIA03.Close();

            getStatus(sender, e);


            string error = ex.Message.ToString();

        }


    }

    protected void getStatus(object sender, EventArgs e)
    {
        DataSet ds = null;
        string LA04="", LA05="", MIA03A="", MIA03B="", MIA02A="", MIA02B="";
        string sql = "SELECT ID,STATUS FROM cdrdb..NextoneStatus order by id";

        ds = Util.RunQueryByStmntatCDRDB(sql);

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            switch (i)
            {
                case 0: LA04 = ds.Tables[0].Rows[i][1].ToString(); break;
                case 1: LA05 = ds.Tables[0].Rows[i][1].ToString(); break;
                case 2: MIA02A = ds.Tables[0].Rows[i][1].ToString(); break;
                case 3: MIA02B = ds.Tables[0].Rows[i][1].ToString(); break;
                case 4: MIA03A = ds.Tables[0].Rows[i][1].ToString(); break;
                case 5: MIA03B = ds.Tables[0].Rows[i][1].ToString(); break;
            }

        }


        if (MIA02A == "1")
        {
            labelstatmia02A.BackColor = System.Drawing.Color.FromArgb(0,255,0);
            labelstatmia02B.BackColor = System.Drawing.Color.White;
        }
        else
        {
            labelstatmia02B.BackColor = System.Drawing.Color.Red;
            labelstatmia02A.BackColor = System.Drawing.Color.White;
        }

        if (MIA03A == "1")
        {
            labelstatmia03A.BackColor = System.Drawing.Color.FromArgb(0, 255, 0); 
            labelstatmia03B.BackColor = System.Drawing.Color.White;
        }
        else
        {
            labelstatmia03B.BackColor = System.Drawing.Color.Red;
            labelstatmia03A.BackColor = System.Drawing.Color.White;
        }

        if (LA04 == "1")
        {
            labelstatusLA04.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
            labelstatusLA05.BackColor = System.Drawing.Color.White;
        }
        else
        {
            labelstatusLA05.BackColor = System.Drawing.Color.Red;
            labelstatusLA04.BackColor = System.Drawing.Color.White;
        }


    }
}
