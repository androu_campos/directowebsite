﻿<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="RateViewer2.aspx.cs" Inherits="RateViewer2" Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <asp:Label ID="Label1" runat="server" CssClass="labelTurkH" Text="MECCA RATE VIEWER" Font-Bold="true"></asp:Label>
    <table width="100%" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td align="left">
                <table align="left">
                    <tr>
                        <td valign="top" align="left">
                            <asp:Label ID="Label4" runat="server" CssClass="labelSteelBlue" Text="Select Customer/Vendor"></asp:Label></td>
                        <td valign="top">
                            <asp:RadioButtonList ID="rdbType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                                OnSelectedIndexChanged="rdbType_SelectedIndexChanged" CssClass="labelSteelBlue">
                                <asp:ListItem Value="C" Selected="True">Customer</asp:ListItem>
                                <asp:ListItem Value="V">Vendor</asp:ListItem>
                            </asp:RadioButtonList></td>
                        <td valign="top">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rdbType"
                                ErrorMessage="" Font-Names="Arial" Font-Size="8pt"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label2" runat="server" CssClass="labelSteelBlue" Text="CustName:"></asp:Label></td>
                        <td>
                            <asp:DropDownList ID="dpdCustname" runat="server" Width="179px" CssClass="dropdown">
                            </asp:DropDownList></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 152px; height: 5px" align="left">
                            <asp:Label ID="Label3" runat="server" CssClass="labelSteelBlue" Text="Rate:"></asp:Label></td>
                        <td style="width: 135px; height: 5px" align="left">
                            <asp:TextBox ID="txtRate" runat="server" Width="172px" CssClass="labelSteelBlue"></asp:TextBox></td>
                        <td style="width: 100px; height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 152px; height: 5px" align="left">
                            <asp:Label ID="Label5" runat="server" CssClass="labelSteelBlue" Text="Prefix:"></asp:Label></td>
                        <td style="width: 135px; height: 5px" align="left">
                            <asp:TextBox ID="txtPrefix" runat="server" Width="172px" CssClass="labelSteelBlue"></asp:TextBox></td>
                        <td style="width: 100px; height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 152px; height: 5px">
                        </td>
                        <td align="left" style="width: 135px; height: 5px">
                            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" CssClass="boton" /></td>
                        <td style="width: 100px; height: 5px">
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="75">
            </td>
            
        </tr>
        <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label6" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 152px; height: 10px">
            </td>
            <td align="left" style="width: 135px; height: 10px">
                <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                </cc1:ValidatorCalloutExtender>
            </td>
            <td style="width: 100px; height: 10px">
            </td>
        </tr>
    </table>
</asp:Content>
