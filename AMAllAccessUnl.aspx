<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AMAllAccessUnl.aspx.cs" Inherits="Default7" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td style="width: 200px">
                    <asp:Label ID="lblBilling" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="Label"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 200px">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <asp:Label ID="lblHeader" runat="server" CssClass="labelBlue8" Font-Bold="True" Font-Size="10pt"
                        Text="Label"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px; height: 23px">
                </td>
            </tr>
            <tr>
                <td style="width: 200px">
                    <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Recent Traffic" Width="200px"></asp:Label><br />
                    <asp:GridView ID="gdvRecent" runat="server" Font-Names="Arial" Font-Size="8pt" Font-Underline="False" Width="100%" AllowPaging="True" OnPageIndexChanging="gdvRecent_PageIndexChanging" PageSize="30">
                        <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                        <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                        <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial"
                            Font-Bold="True" ForeColor="Orange"></HeaderStyle>
                        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                        
                    </asp:GridView>
                    <asp:Label ID="lblRecent" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="bexport_recent" runat="server" Text="Excel" CssClass="boton" OnClick="exportRecent"/>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 24px">
                </td>
            </tr>
            <tr>
                <td style="width: 200px">
                    <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Today Traffic"></asp:Label><br />
                    <asp:GridView ID="gdvToday" runat="server" Font-Names="Arial" Font-Size="8pt" Width="100%" AllowPaging="True" OnPageIndexChanging="gdvToday_PageIndexChanging" PageSize="30">
                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial" Font-Bold="True" ForeColor="Orange"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>

                    </asp:GridView>
                    <asp:Label ID="lblToday" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="bexport_today" runat="server" Text="Excel" CssClass="boton" OnClick="exportTodays"/>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 23px">
                </td>
            </tr>
            <tr>
                <td style="width: 200px">
                    <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Yesterday Traffic"></asp:Label><br />
                    <asp:GridView ID="gdvYesterday" runat="server" Font-Names="Arial" Font-Size="8pt" Width="100%" AllowPaging="True" OnPageIndexChanging="gdvYesterday_PageIndexChanging" PageSize="30">
                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>

<RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial" Font-Bold="True" ForeColor="Orange"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>

                    </asp:GridView>
                    <asp:Label ID="lblYesterday" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="bexport_yesterday" runat="server" Text="Excel" CssClass="boton" OnClick="exportYesterdays"/>
                </td>
            </tr>
            <tr>
                <td style="width: 106px; height: 44px;" valign="top">
                    <asp:Timer ID="Timer1" runat="server" Interval="300000">
                    </asp:Timer>
                </td>
                <td style="width: 100px; height: 44px;" valign="top">
                    </td>
            </tr>
            
        </table>
    
    </div>
    </form>
</body>
</html>
