using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Neov3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            tvNEO.Nodes[0].Expanded = true;           
            tvNEO.Nodes[1].Expanded = true;            
        }
    }

    protected void tvNEO_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        e.Node.ChildNodes.Clear();
        switch (e.Node.Depth)
        {
            case 0:
                PopulateOptions(e.Node);
                break;
            case 1:
                PopulateLevels(e.Node);
                break;
            case 2:
                PopulateOptions(e.Node);
                break;
            case 3:
                PopulateLevels(e.Node);
                break;
            case 4:
                PopulateOptions(e.Node);
                break;
            case 5:
                PopulateLevels(e.Node);
                break;
            case 6:
                PopulateOptions(e.Node);
                break;
            case 7:
                PopulateLevels(e.Node);
                break;
            case 8:
                PopulateOptions(e.Node);
                break;
            case 9:
                PopulateLevels(e.Node);
                break;
            case 10:
                PopulateOptions(e.Node);
                break;
        }

    }

    void PopulateLevels(TreeNode node)
    {
        string sWhere = "", sSelectDef = "", sSelect = "SELECT  TOP 10 ", sOrderBy = " ORDER BY Grade", sINselect = "", sFrom, sGroupBy = " GROUP BY ", sSE = "";

        sSelectDef = ",SUM(Calls) as Calls,SUM(CompCalls) as CompCalls,SUM(Minutes) as Minutes,ROUND(100*CAST(SUM(CompCalls) AS FLOAT)/SUM(Calls),2) as ASR,ROUND(CAST(SUM(minutes) as FLOAT)/SUM(CompCalls),2) as ACD";
        SeparaPath(node.ValuePath);
        sFrom = " FROM MECCA2..OpSheet" + Session["Table"].ToString();
        sWhere = BuildWhere(node.Depth) + " AND Calls > 100 ";

        sINselect = node.Value.ToString();
        sSE = sINselect;
        if (sINselect == "Region")
        {
            sINselect = "Region,Country ";
            //sINselect = "Region +' - ' +Country AS Region ";
            sGroupBy = sGroupBy + " Region,Country ";
        }
        else
        {
            sGroupBy = sGroupBy + sINselect;
        }

        sSelect = sSelect + sINselect + sSelectDef + BuildsGrade(sSE);



        SqlCommand sqlQuery = new SqlCommand();
        sqlQuery.CommandText = sSelect + sFrom + sWhere + sGroupBy + sOrderBy;
        DataSet resultSet;
        resultSet = RunQuery(sqlQuery);
        gvNeo.DataSource = resultSet;
        gvNeo.DataBind();
        if (resultSet.Tables.Count > 0)
        {
            foreach (DataRow row in resultSet.Tables[0].Rows)
            {
                //TreeNode NewNode;
                if (sSE == "Region")
                {
                    TreeNode NewNode = new
                    TreeNode(row[sSE].ToString() + " - " + row["Country"].ToString(), row[sSE].ToString());

                    NewNode.Checked = false;
                    NewNode.Expanded = false;
                    NewNode.Selected = false;
                    NewNode.PopulateOnDemand = true;
                    NewNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    NewNode.ImageUrl = getImageLevel(Convert.ToDouble(row["Grade"].ToString()));
                    node.ChildNodes.Add(NewNode);
                }
                else
                {

                    TreeNode NewNode = new
                        TreeNode(row[sSE].ToString(),
                        row[sSE].ToString());
                    NewNode.Checked = false;
                    NewNode.Expanded = false;
                    NewNode.Selected = false;
                    NewNode.PopulateOnDemand = true;
                    NewNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    NewNode.ImageUrl = getImageLevel(Convert.ToDouble(row["Grade"].ToString()));
                    node.ChildNodes.Add(NewNode);
                }

            }
        }
    }

    void PopulateOptions(TreeNode node)
    {
        string sWhere = " WHERE ";

        SeparaPath(node.ValuePath);
        if (Session["Table"].ToString() == "") Session["Table"] = node.Value.ToString();

        switch (node.Depth)
        {
            case 0:
                sWhere = "";
                break;
            case 2:
                sWhere = sWhere + " NameOption <> '" + Session["option0"].ToString() + "'";
                break;
            case 4:
                sWhere = sWhere + " NameOption <> '" + Session["option0"].ToString() + "'";
                sWhere = sWhere + " AND NameOption <> '" + Session["option1"].ToString() + "'";
                break;
            case 6:
                sWhere = sWhere + " NameOption <> '" + Session["option0"].ToString() + "'";
                sWhere = sWhere + " AND NameOption <> '" + Session["option1"].ToString() + "'";
                sWhere = sWhere + " AND NameOption <> '" + Session["option2"].ToString() + "'";
                break;
            case 8:
                sWhere = sWhere + " NameOption <> '" + Session["option0"].ToString() + "'";
                sWhere = sWhere + " AND NameOption <> '" + Session["option1"].ToString() + "'";
                sWhere = sWhere + " AND NameOption <> '" + Session["option2"].ToString() + "'";
                sWhere = sWhere + " AND NameOption <> '" + Session["option3"].ToString() + "'";
                break;

        }
        SqlCommand sqlQuery = new SqlCommand("Select NameOption,Display FROM NeoOptionsTV " + sWhere);
        DataSet resultSet;
        resultSet = RunQuery(sqlQuery);

        if (resultSet.Tables.Count > 0)
        {
            DataRow rowOp;
            DataTable tabOp = new DataTable();
            tabOp.Columns.Add("Options");
            tabOp.Columns.Add("Calls");
            tabOp.Columns.Add("CompCalls");
            tabOp.Columns.Add("Minutes");
            tabOp.Columns.Add("ASR");
            tabOp.Columns.Add("ACD");
            tabOp.Columns.Add("Graded");

            foreach (DataRow row in resultSet.Tables[0].Rows)
            {
                rowOp = getOptionGV(row["NameOption"].ToString(), node.Depth);
                tabOp.Rows.Add(rowOp.ItemArray);
                //tabOp.Rows.Add(rowOp.ItemArray);
                //il = getImageOption(row["NameOption"].ToString(), node.Depth);
                TreeNode NewNode = new
                    TreeNode(row["Display"].ToString(),
                    row["NameOption"].ToString());
                NewNode.Checked = false;
                NewNode.Expanded = false;
                NewNode.Selected = false;
                NewNode.PopulateOnDemand = true;
                NewNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                NewNode.ImageUrl = getImageLevel(Convert.ToDouble(rowOp["Grade"].ToString()));
                node.ChildNodes.Add(NewNode);
            }
            if (tabOp.Rows.Count > 0)
            {
                gvNeo.DataSource = tabOp;
                gvNeo.DataBind();
            }
        }
    }

    string BuildWhere(int iNivel)
    {
        string Where = "";
        switch (iNivel)
        {
            case 1:
                Where = " WHERE ASR <> 0 ";
                break;
            case 3:
                Where = Where + " WHERE " + Session["option0"].ToString() + " LIKE '" + Session["Level1"].ToString() + "'" +
                     " AND ASR <> 0 ";
                break;
            case 5:
                Where = Where + " WHERE " + Session["option0"].ToString() + " LIKE '" + Session["Level1"].ToString() +
                     "' AND " + Session["option1"].ToString() + " LIKE '" + Session["Level2"].ToString() + "'" +
                     " AND ASR <> 0 ";
                break;
            case 7:
                Where = Where + " WHERE " + Session["option0"].ToString() + " LIKE '" + Session["Level1"].ToString() +
                    "' AND " + Session["option1"].ToString() + " LIKE '" + Session["Level2"].ToString() +
                    "' AND " + Session["option2"].ToString() + " LIKE '" + Session["Level3"].ToString() + "'" +
                    " AND ASR <> 0 ";
                break;
            case 9:
                Where = Where + " WHERE " + Session["option0"].ToString() + " LIKE '" + Session["Level1"].ToString() +
                    "' AND " + Session["option1"].ToString() + " LIKE '" + Session["Level2"].ToString() +
                    "' AND " + Session["option2"].ToString() + " LIKE '" + Session["Level3"].ToString() +
                    "' AND " + Session["option3"].ToString() + " LIKE '" + Session["Level4"].ToString() + "'" +
                    " AND ASR <> 0 ";
                break;
        }
        return Where;
    }

    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString =
            ConfigurationManager.ConnectionStrings
            ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
            //labelStatus.Text = "Unable to connect to SQL Server.";
        }
        return resultsDataSet;
    }

    void SeparaPath(string patthh)
    {
        int i = 0, indx;
        string[] niv = new string[11];
        string swap;
        while (patthh.Contains("/") == true)
        {
            indx = patthh.IndexOf("/");
            niv[i] = patthh.Substring(0, indx);
            swap = patthh.Substring(indx + 1);
            patthh = swap;
            i++;
        }
        niv[i] = patthh;
        if (niv[0] != null) Session["Table"] = niv[0];
        if (niv[1] != null) Session["option0"] = niv[1];
        if (niv[2] != null) Session["Level1"] = niv[2];
        if (niv[3] != null) Session["option1"] = niv[3];
        if (niv[4] != null) Session["Level2"] = niv[4];
        if (niv[5] != null) Session["option2"] = niv[5];
        if (niv[6] != null) Session["Level3"] = niv[6];
        if (niv[7] != null) Session["option3"] = niv[7];
        if (niv[8] != null) Session["Level4"] = niv[8];
        if (niv[9] != null) Session["option4"] = niv[9];
        if (niv[10] != null) Session["Level5"] = niv[10];

    }

    string BuildsGrade(string Name)
    {
        //Jugar con la formula de Grade
        string grade;

        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;

        sqlSpQuery.CommandText = "sp_getMaxNeo";

        sqlSpQuery.Parameters.Add("@from", SqlDbType.VarChar).Value = "MECCA2..OpSheet" + Session["Table"].ToString();

        sqlSpQuery.Parameters.Add("@opti", SqlDbType.VarChar).Value = Name;

        DataSet MaxResultSet;
        MaxResultSet = RunQuery(sqlSpQuery);
        DataRow MaxRow = MaxResultSet.Tables[0].Rows[0];

        grade = ",ROUND(CAST(SUM(Calls) as Float)/" + MaxRow["CallsM"].ToString() + " * " + txtCalls.Text + " " +
                "+ SUM(Minutes)/" + MaxRow["MinM"].ToString() + " * " + txtMin.Text + " " +
                "+ ROUND(100*CAST(SUM(CompCalls) AS FLOAT)/SUM(Calls)*" + txtASR.Text + "/" + MaxRow["ASRM"].ToString() + " ,5) " +
                "+ ROUND(CAST(SUM(minutes) as FLOAT)/SUM(CompCalls)*" + txtACD.Text + "/" + MaxRow["ACDM"].ToString() + ",2),3) as Grade";

        return grade;
    }


    private DataRow getOptionGV(string Name, int iNivel)
    {

        string sSelectIn, sSelect, sWhere, sQuery, sFrom, sGrade;


        //Obtiene los MAXIMOS de la opcion Seleccionada--------------------

        sGrade = BuildsGrade(Name);

        //FIN Obtiene MAXIMOS*****************




        //******* FIN Grade



        //SELECT Interno
        sSelectIn = "SELECT " + Name + ", SUM(Calls) as Calls,SUM(CompCalls) as CompCalls,SUM(Minutes) as Minutes" + sGrade;
        //******** FIN SELECT Interno

        //SELECT EXTERNO
        sSelect = "SELECT '" + Name + "' as Options,SUM(Calls) as Calls,SUM(CompCalls) as CompCalls,SUM(Minutes) as Minutes,ROUND(100*CAST(SUM(CompCalls) AS FLOAT)/SUM(Calls),2) as ASR,ROUND(CAST(SUM(minutes) as FLOAT)/SUM(CompCalls),2) as ACD,ROUND(AVG(Grade),2) as Grade FROM (";
        //******** FIN SELECT Externo

        // FROM -----------
        sFrom = " FROM MECCA2..OpSheet" + Session["Table"].ToString();
        // *****FIN  FROM

        // WHERE -----------
        sWhere = BuildWhere(iNivel + 1);
        // *****FIN WHERE 

        //Build Query
        sQuery = sSelect + "SELECT TOP 10 * FROM (" + sSelectIn + sFrom + sWhere + "  GROUP BY " + Name + ") t   WHERE Grade > 0 ORDER BY Grade) f";

        SqlCommand sqlQuery = new SqlCommand();
        sqlQuery.CommandText = sQuery;
        DataSet ResultSett;
        ResultSett = RunQuery(sqlQuery);
        if (ResultSett.Tables.Count > 0)
        {
            return ResultSett.Tables[0].Rows[0];
        }
        else return null;

    }

    string getImageLevel(double ima)
    {
        string imagen = "";
        ima *= 10;
        int i;
        for (i = 1; i <= 10; i++)
        {
            if (ima > i - 1 & ima <= i) imagen = "~/Images/d" + i.ToString() + ".bmp";
        }
        //if (imagen == "") imagen = "~/NeoIma/s1.gif";
        return imagen;
    }
    protected void btnShowGV_Click(object sender, EventArgs e)
    {
        if (gvNeo.Visible == true)
        {
            gvNeo.Visible = false;
            btnShowGV.Text = "Show Data Details";
        }
        else
        {
            gvNeo.Visible = true;
            btnShowGV.Text = "Hide Data Details";
        }
    }

}
