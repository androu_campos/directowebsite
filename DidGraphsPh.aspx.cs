using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;

public partial class DidGraphsPh : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String query = null;
        DataTable tbl;
        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in ('DBTZ')  and [type] like 'C%' and ID IN('STC02','MIA02') group by Time ORDER BY Time";

        //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        //query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        //query += "[Live_Calls] > 0   AND Cust_Vendor in ('OLDR','OLAM','BSTI','OLBR','OLCL','OLC2','OLC3','OLEC','OLFX','OGCL','OGMS','OGTI','OCIC','OLMS','OLPL','OLPH','OLPO','OLPR') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartP(tbl, "DID-BLITZ");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor in ('DIDK')  and [type] like 'C%' and ID IN('STC02','MIA02') group by Time ORDER BY Time";

        //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        //query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        //query += "[Live_Calls] > 0   AND Cust_Vendor = 'OCIC' and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartN(tbl, "DID-KONCEPT");

        tbl.Dispose();
        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in ('DIDI')  and [type] like 'C%' and ID IN('STC02','MIA02') group by Time ORDER BY Time";

        //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        //query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        //query += "[Live_Calls] > 0   AND Cust_Vendor = 'OLC2' and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartB(tbl, "DID-INTELCO");

        tbl.Dispose();
        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in ('DIDH')  and [type] like 'C%' and ID IN('STC02','MIA02') group by Time ORDER BY Time";

        //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        //query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        //query += "[Live_Calls] > 0   AND Cust_Vendor = 'OLMS' and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartV(tbl, "DID-HAWAII");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in ('DVOI')  and [type] like 'C%' and ID IN('STC02','MIA02') group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartNC(tbl, "DID-VOICESTEP");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in ('DIDX')  and [type] like 'C%' and ID IN('STC02','MIA02') group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartA(tbl, "DID-X");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in ('APAS')  and [type] like 'V%' and ID IN('STC02','MIA02') group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartAPAS(tbl, "ALLACCESS-UNL-PASS");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in ('APBX')  and [type] like 'V%' and ID IN('MIA03') group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartAPBX(tbl, "ALLACCESS-UNL-PBX");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in ('ANCM')  and [type] like 'C%' and ID IN('MIA03') group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartANCM(tbl, "ALLACCESS-NETCOMM (800's Traffic)");

        tbl.Dispose();
    }
    private void makeChartP(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();

            //this.Chart1.Titles.Add(tITLE);
            this.Chart1.Titles["Title1"].Text = tITLE;
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetailP"] = tbl;

            this.Chart1.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            Session["tblDetailP"] = tbl;
        }

    }
    private void makeChartN(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart2.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart2.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart2.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart2.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart2.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart2.Titles["Title1"].Text = tITLE;
            this.Chart2.RenderType = RenderType.ImageTag;
            Session["tblDetailN"] = tbl;

            this.Chart1.Visible = true;
        }
        else
        {
            this.Chart2.Visible = false;
            Session["tblDetailN"] = tbl;
        }

    }
    private void makeChartB(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart3.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart3.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart3.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart3.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart3.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart3.Titles["Title1"].Text = tITLE;

            this.Chart3.RenderType = RenderType.ImageTag;
            Session["tblDetailB"] = tbl;

            this.Chart3.Visible = true;
        }
        else
        {
            this.Chart3.Visible = false;
            Session["tblDetailB"] = tbl;
        }

    }
    private void makeChartV(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart4.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart4.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart4.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart4.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart4.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart4.Titles["Title1"].Text = tITLE;
            this.Chart4.RenderType = RenderType.ImageTag;
            Session["tblDetailV"] = tbl;

            this.Chart4.Visible = true;
        }
        else
        {
            this.Chart4.Visible = false;
            Session["tblDetailV"] = tbl;
        }

    }
    private void makeChartNC(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart5.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart5.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart5.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart5.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart5.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart5.Titles["Title1"].Text = tITLE;
            this.Chart5.RenderType = RenderType.ImageTag;
            Session["tblDetailV"] = tbl;

            this.Chart5.Visible = true;
        }
        else
        {
            this.Chart5.Visible = false;
            Session["tblDetailV"] = tbl;
        }
    }
    private void makeChartA(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart6.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart6.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart6.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart6.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart6.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart6.Titles["Title1"].Text = tITLE;
            this.Chart6.RenderType = RenderType.ImageTag;
            Session["tblDetailV"] = tbl;

            this.Chart6.Visible = true;
        }
        else
        {
            this.Chart6.Visible = false;
            Session["tblDetailV"] = tbl;
        }
    }
    private void makeChartAPAS(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart7.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart7.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart7.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart7.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart7.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart7.Titles["Title1"].Text = tITLE;
            this.Chart7.RenderType = RenderType.ImageTag;
            Session["tblDetailV"] = tbl;

            this.Chart7.Visible = true;
        }
        else
        {
            this.Chart7.Visible = false;
            Session["tblDetailV"] = tbl;
        }
    }
    private void makeChartAPBX(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart8.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart8.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart8.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart8.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart8.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart8.Titles["Title1"].Text = tITLE;
            this.Chart8.RenderType = RenderType.ImageTag;
            Session["tblDetailV"] = tbl;

            this.Chart8.Visible = true;
        }
        else
        {
            this.Chart8.Visible = false;
            Session["tblDetailV"] = tbl;
        }
    }
    private void makeChartANCM(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart9.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart9.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart9.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart9.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart9.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart9.Titles["Title1"].Text = tITLE;
            this.Chart9.RenderType = RenderType.ImageTag;
            Session["tblDetailV"] = tbl;

            this.Chart9.Visible = true;
        }
        else
        {
            this.Chart9.Visible = false;
            Session["tblDetailV"] = tbl;
        }
    }
}
