<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CallMexReport.aspx.cs"
    Inherits="ICSReport" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="lblBilling" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="Label"
                    Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <asp:Label ID="lblHeader" runat="server" CssClass="labelBlue8" Font-Bold="True" Font-Size="10pt"
                    Text="CallMex Report"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 100px; height: 23px">
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="width: 100px" valign="top" align="left" bgcolor="#d5e3f0">
                <asp:Label ID="LabelS1" runat="server" Font-Bold="False" Text="" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 300px" valign="top" align="left" bgcolor="#d5e3f0">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Font-Names="Arial" ForeColor="#004A8F"
                    Text="Choose Range Of Days" Width="282px" Font-Size="8pt"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" valign="top" align="left">
                <asp:Label ID="labelFrom" runat="server" Text="From:" Font-Bold="False" CssClass="labelSteelBlue" /></td>
            <td style="width: 300px" valign="top" align="left">
                <asp:TextBox ID="txtFromV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                &nbsp;&nbsp;<asp:Image ID="imgFrom" runat="server" ImageUrl="~/Images/calendar1.gif" />
            </td>
        </tr>
        <tr>
            <td style="width: 100px" valign="top" align="left">
                <asp:Label ID="labelTo" runat="server" Text="To:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
            <td style="width: 300px" valign="top" align="left">
                <asp:TextBox ID="txtToV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                &nbsp;&nbsp;<asp:Image ID="imgTo" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
        </tr>
        <tr>
            <td style="width: 100px" valign="top" align="left">
                <asp:Label ID="lblVendor" runat="server" Text="Group by Vendors:" Font-Bold="False"
                    CssClass="labelSteelBlue"></asp:Label></td>
            <td style="width: 300px" valign="top" align="left">
                <asp:CheckBox ID="chkVend" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue">
                </asp:CheckBox>
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="SubmitClick" Font-Names="Arial"
                    CssClass="boton" />
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <DCWC:Chart ID="Chart1" runat="server" Width="603px" Height="245px" Palette="Pastel">
                    <Legends>
                        <DCWC:Legend Name="Default" Alignment="Center">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="Int" Name="LMC" ChartType="Pie" Color="DarkOrange"
                            BorderColor="64, 64, 64">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <%--<AxisX Title="DateTime">
                                <LabelStyle Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Format="N0"></LabelStyle>
                            </AxisY>--%>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Color="245, 0, 0, 0" Font="Arial, 8.25pt, style=Bold" Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 179px" valign="top">
                <asp:Label ID="lblPrueba" runat="server" Visible="false"></asp:Label>
                <asp:Button ID="cmdDetail" runat="server" CssClass="boton" OnClick="cmdDetail_Click"
                    Text="View detail" Visible="True" />
                <asp:Button ID="cmdHide" runat="server" CssClass="boton" OnClick="cmdHide_Click"
                    Text="Hide detail" Visible="False" /><br />
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <table>
                            <tr>
                                <td style="width: 100px">
                                    <asp:Label ID="Label5" runat="server" Text="Loading...Please wait" ForeColor="LimeGreen"
                                        Width="105px" CssClass="labelBlue8"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Loading.gif"></asp:Image></td>
                            </tr>
                        </table>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td valign="top">
                                    <asp:GridView ID="gdvDetail" runat="server" Font-Size="8pt" Font-Names="Arial" Visible="False"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:BoundField DataField="LMC" HeaderText="LMC">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Minutes" HeaderText="Minutes">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Cost" HeaderText="Cost">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Porc" HeaderText="Porcent">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle CssClass="titleOrangegrid"></HeaderStyle>
                                    </asp:GridView>
                                </td>
                                <td valign="top">
                                    <asp:GridView ID="gdvVendor" runat="server" Font-Size="8pt" Font-Names="Arial" Visible="False"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:BoundField DataField="LMC" HeaderText="LMC">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Vendor" HeaderText="Vendor">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Minutes" HeaderText="Minutes">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Cost" HeaderText="Cost">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Porc" HeaderText="Porcent">
                                                <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle CssClass="titleOrangegrid"></HeaderStyle>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="cmdDetail" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="cmdHide" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="width: 106px; height: 44px;" valign="top">
                <asp:Timer ID="Timer1" runat="server" Interval="300000">
                </asp:Timer>
            </td>
            <td style="width: 100px; height: 44px;" valign="top">
            </td>
        </tr>
    </table>
    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromV"
        PopupButtonID="imgFrom" Format="M/d/yyyy">
    </cc1:CalendarExtender>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToV"
        PopupButtonID="imgTo" Format="M/d/yyyy">
    </cc1:CalendarExtender>
</asp:Content>
