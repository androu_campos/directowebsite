<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="UnratedDid.aspx.cs" Inherits="UnratedDid" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1"  Runat="Server">
   
                    
    <asp:Label ID="Label2" CssClass="labelTurkH" runat="server" Text="UNRATED DID MINUTES" style="left: 229px"></asp:Label>
    <table width="100%" align="left" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td align="left" style="width: 100px">
   
                <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="Button1_Click"
                    Text="Export to EXCEL" Font-Bold="False" Width="104px" /></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px">
    
    
    <asp:GridView ID="GridView1" AutoGenerateColumns="false" runat="server" RowStyle-HorizontalAlign="Center" CellPadding="4" ForeColor="#333333" GridLines="None" Font-Size="9pt" Font-Names="Arial" style="left: 580px; top: 245px" Width="857px" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />
         <Columns>
         <asp:BoundField DataField="BillingDate" HeaderText="BillingDate" >
             <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="160px" />
             <HeaderStyle HorizontalAlign="Left" />
         </asp:BoundField>
         <asp:BoundField  DataField="Customer" HeaderText="Customer">
             <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
             <HeaderStyle HorizontalAlign="Left" />
         </asp:BoundField>
         <asp:BoundField DataField="Vendor" HeaderText="Vendor" >
             <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="130px" Wrap="False" />
             <HeaderStyle HorizontalAlign="Left" />
         </asp:BoundField>
         <asp:BoundField DataField="DID" HeaderText="DID" >
             <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
             <HeaderStyle HorizontalAlign="Left" />
         </asp:BoundField>         
         <asp:BoundField DataField="Minutes" HeaderText="Minutes" >
             <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
             <HeaderStyle HorizontalAlign="Left" />
         </asp:BoundField> 
         </Columns>
    
    </asp:GridView>
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 100px">
   
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Font-Bold="True" Font-Names="Arial"
                    Font-Size="8pt" Text="Nothing Found !!" Visible="False"></asp:Label></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>        
        
        <tr>
        <td height="70" valign="top">
            
            
        </td >  
        </tr>

        <tr>
            <td width="100%">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        
    </table>
</asp:Content>

