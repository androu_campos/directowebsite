<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="OpTc.aspx.cs" Inherits="OpTc" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">

function showDialog()
{
    var x=window.confirm("Are you sure you are ok?")
    
    
    if (x)
    document.getElementById('<%=HiddenField1.ClientID%>').value = "1";
    else
    document.getElementById('<%=HiddenField1.ClientID%>').value = "0";
    
    }

</script>

    <asp:HiddenField ID="HiddenField1" runat="server" />
    &nbsp;
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" DataKeyNames="FullName" OnRowCommand="GridView1_RowCommand" OnRowDeleting="GridView1_RowDeleting">
        <Columns>
            <asp:BoundField DataField="FullName" HeaderText="FullName" SortExpression="FullName" />
            <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname" />
            <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname" />
            <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
            <asp:BoundField DataField="Username" HeaderText="Username" ReadOnly="True" SortExpression="Username" />
            <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password" />
            <asp:BoundField DataField="AccessLevel" HeaderText="AccessLevel" SortExpression="AccessLevel" />
            <asp:TemplateField HeaderText="Eliminar">
                <ItemTemplate>
                    <asp:Button ID="Button1" runat="server" CausesValidation="false" CommandName="Delete"
                        Text="Borrar?" OnClientClick="showDialog()" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        DeleteCommand="DELETE FROM Users WHERE (FullName = @FullName)" SelectCommand="SELECT FullName, Firstname, Lastname, email, Username, Password, AccessLevel FROM Users">
        <DeleteParameters>
            <asp:Parameter Name="FullName" />
        </DeleteParameters>
    </asp:SqlDataSource>
</asp:Content>

