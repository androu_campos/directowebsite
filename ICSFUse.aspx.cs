using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;

public partial class ICSFUse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String query = null;
        DataTable tbl;
        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in (SELECT IP FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME IN (SELECT CUSTOMER FROM CDRDB.DBO.CUSTOMER_BUSINESSUNIT WHERE BUSINESSUNIT = 'ICS-FIRSTU2') AND TYPE = 'C')  and [type] like 'C%' and ID IN('MIA01') group by Time ORDER BY Time";

        //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        //query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        //query += "[Live_Calls] > 0   AND Cust_Vendor in ('OLDR','OLAM','BSTI','OLBR','OLCL','OLC2','OLC3','OLEC','OLFX','OGCL','OGMS','OGTI','OCIC','OLMS','OLPL','OLPH','OLPO','OLPR') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartPWVV(tbl, "ICS-FIRSTU2");

        tbl.Dispose();

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
        query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in (SELECT IP FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME IN (SELECT CUSTOMER FROM CDRDB.DBO.CUSTOMER_BUSINESSUNIT WHERE BUSINESSUNIT = 'ICS-FIRSTU5') AND TYPE = 'C')  and [type] like 'C%' and ID IN('MIA01') group by Time ORDER BY Time";

        //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
        //query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        //query += "[Live_Calls] > 0   AND Cust_Vendor in ('OLDR','OLAM','BSTI','OLBR','OLCL','OLC2','OLC3','OLEC','OLFX','OGCL','OGMS','OGTI','OCIC','OLMS','OLPL','OLPH','OLPO','OLPR') and [type] like 'V%'  group by Time ORDER BY Time";

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartPHSV(tbl, "ICS-FIRSTU5");

        tbl.Dispose();
    }

    private void makeChartPWVV(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();

            //this.Chart1.Titles.Add(tITLE);
            this.Chart1.Titles["Title1"].Text = tITLE;
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetailP"] = tbl;

            this.Chart1.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            Session["tblDetailP"] = tbl;
        }

    }

    private void makeChartPHSV(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart2.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart2.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart2.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart2.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart2.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();

            //this.Chart1.Titles.Add(tITLE);
            this.Chart2.Titles["Title1"].Text = tITLE;
            this.Chart2.RenderType = RenderType.ImageTag;
            Session["tblDetailP"] = tbl;

            this.Chart2.Visible = true;
        }
        else
        {
            this.Chart2.Visible = false;
            Session["tblDetailP"] = tbl;
        }

    }
}
