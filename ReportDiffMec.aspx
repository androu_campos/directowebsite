<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ReportDiffMecca.aspx.cs"
Inherits="ReportDiffMec" Title="DIRECTO - Connections Worldwide" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="ReportLabel" runat="server" Text="REPORT DIFFERENCES" CssClass="labelTurkH"> </asp:Label>
    <table align="left" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td>
                <table width="100%" >
                    <tr>
                        <td align="left" style="width: 204px" valign="top"></td>
                        <td align="left" style="width: 77px" valign="top"></td>
                        <td align="left" style="width: 300px" valign="top"></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 204px; height: 27px;" valign="top"></td>
                        <td align="left" style="width: 77px; height: 27px;" valign="top"></td>
                        <td align="left" style="width: 300px; height: 27px;" valign="top"></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 204px" colspan=2 valign="top" bgcolor="#d5e3f0">
                            <asp:Label ID="LabelS1" runat="server" Font-Bold="True" Font-Names="Arial"
                                Text="Choose Range Of Days" CssClass="labelTurk" Width="132px"></asp:Label></td>
                        
                    </tr>
                    <tr>
                        <td align="right" style="width: 204px; height: 27px;" valign="top">
                                <asp:Label ID="labelFrom" runat="server" Font-Bold="True" Font-Names="Arial" Text="From:" CssClass="labelTurk" Font-Size="8pt"></asp:Label>
                        </td>
                        <td align="left" style="width: 77px; height: 27px" valign="top">
                                <table>
                                    <tr>
                                        <td style="width: 100px">
                                <asp:TextBox ID="txtFromV" runat="server" CssClass="labelSteelBlue" Width="125px"></asp:TextBox></td>
                                        <td style="width: 100px">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                                    </tr>
                                </table>
                         </td>
                         <td align="left" style="width: 300px; height: 27px;" valign="top">&nbsp;</td>            
                    </tr>
                    <tr>
                        <td align="right" style="width: 204px" valign="top">
                            <asp:Label ID="labelTo" runat="server" Font-Bold="True" Font-Names="Arial" Text="To:" CssClass="labelTurk" Font-Size="8pt"></asp:Label></td>
                        <td align="left" style="width: 77px" valign="top">
                            <table>
                                <tr>
                                    <td style="width: 100px">
                            <asp:TextBox ID="txtToV" runat="server" CssClass="labelSteelBlue" OnTextChanged="txtToV_TextChanged" Width="125px"></asp:TextBox></td>
                                    <td style="width: 100px">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" style="width: 300px" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 204px; height: 27px;" valign="top"></td>
                        <td align="left" style="width: 77px; height: 27px" valign="top">
                            <asp:Button ID="btnSubmit" runat="server" Enabled="False" Font-Names="Arial" 
                            OnClick="btnSubmit_Click" Text="Submit" BorderStyle="Solid" BorderWidth="1px"
                            CssClass="boton" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>                
    </table>

</asp:Content>