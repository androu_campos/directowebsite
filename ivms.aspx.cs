using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class ivms : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {

            string report = ddlReport.SelectedIndex.ToString();
            if (report == "0")
            {

                LabelInter.Enabled = true;
                DropDownList1.Enabled = true;

            }
            else
            {
                LabelInter.Enabled = false;
                DropDownList1.Enabled = false;

            }

        }
        else
        {
            txtFrom.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();            
        }

    }
    protected void dgDetail_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        // Set CurrentPageIndex to the page the user clicked.        
        dgDetail.CurrentPageIndex = e.NewPageIndex;

        // Rebind the data. 
        string strCnn = "Data Source=localhost;Initial Catalog=CDRDB_LOGS;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlConnection cnn = new SqlConnection(strCnn);
        SqlDataSource dsDatos = new SqlDataSource();
        SqlCommand x = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet dsG1 = new DataSet();

        SqlParameter psource = new SqlParameter("@source", SqlDbType.VarChar);
        psource.Direction = ParameterDirection.Input;


        //   source
        if (dpdNextone.SelectedValue.ToString() == "ALL")
        {
            psource.Value = "%";
        }
        else
        {
            psource.Value = dpdNextone.SelectedValue.ToString() + "%";
        }


        if (ddlReport.SelectedValue.ToString() == "0")
        {
            x.CommandText = "ivmsDetail";
        }
        else
        {
            x.CommandText = "ivmsDetailGroup";
        }

        x.CommandType = CommandType.StoredProcedure;
        x.Connection = cnn;
        da.SelectCommand = x;

        SqlParameter pi = new SqlParameter("@i", SqlDbType.Int);
        pi.Direction = ParameterDirection.Input;
        SqlParameter pinf = new SqlParameter("@inf", SqlDbType.DateTime);
        pinf.Direction = ParameterDirection.Input;
        SqlParameter pcode = new SqlParameter("@code", SqlDbType.VarChar);
        pcode.Direction = ParameterDirection.Input;
        SqlParameter pcust = new SqlParameter("@cust", SqlDbType.VarChar);
        pcust.Direction = ParameterDirection.Input;
        SqlParameter pvendor = new SqlParameter("@vendor", SqlDbType.VarChar);
        pvendor.Direction = ParameterDirection.Input;
        SqlParameter pprefix = new SqlParameter("@prefix", SqlDbType.VarChar);
        pprefix.Direction = ParameterDirection.Input;

        //   source
        if (dpdNextone.SelectedValue.ToString() == "ALL")
        {
            psource.Value = "%";
        }
        else
        {
            psource.Value = dpdNextone.SelectedValue.ToString() + "%";
        }


        if (ddlReport.SelectedValue.ToString() == "0")
        {

            pi.Value = Convert.ToInt16(hfIncrement.Value);
            pinf.Value = hfHour.Value;
            pcust.Value = hfOri.Value.ToString();
            pvendor.Value = hfDest.Value.ToString();
            pcode.Value = hfDetail.Value;
            pprefix.Value = hfPrefix.Value.ToString();
            x.Parameters.Add(pi);
            x.Parameters.Add(pinf);
            x.Parameters.Add(pcust);
            x.Parameters.Add(pvendor);
            x.Parameters.Add(pcode);
            x.Parameters.Add(pprefix);
            x.Parameters.Add(psource);

        }
        else
        {
            SqlParameter psup = new SqlParameter("@sup", SqlDbType.DateTime);
            psup.Direction = ParameterDirection.Input;
            SqlParameter pselected = new SqlParameter("@selected", SqlDbType.VarChar);
            pselected.Direction = ParameterDirection.Input;
            pi.Value = Convert.ToInt16(hfReport.Value.ToString());
            pinf.Value = hfFrom.Value;
            psup.Value = hfTo.Value;
            pselected.Value = hfHour.Value;
            pcode.Value = hfDetail.Value;
            pcust.Value = hfOri.Value.ToString();
            pvendor.Value = hfDest.Value.ToString();
            pprefix.Value = hfPrefix.Value.ToString();
            x.Parameters.Add(pi);
            x.Parameters.Add(pinf);
            x.Parameters.Add(psup);
            x.Parameters.Add(pcust);
            x.Parameters.Add(pvendor);
            x.Parameters.Add(pselected);
            x.Parameters.Add(pcode);
            x.Parameters.Add(pprefix);
            x.Parameters.Add(psource);
        }
        x.CommandTimeout = 600;
        da.Fill(dsG1);
        dgDetail.DataSource = dsG1;
        dgDetail.DataBind();
    }
    
    protected void dgSemi_SelectedIndexChanged(object sender, EventArgs e)
    {
        dgDetail.Visible = true;
        hfDetail.Value = dgSemi.SelectedItem.Cells[1].Text;


        string strCnn = "Data Source=localhost;Initial Catalog=CDRDB_LOGS;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlConnection cnn = new SqlConnection(strCnn);
        SqlDataSource dsDatos = new SqlDataSource();
        SqlCommand x = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet dsG1 = new DataSet();
        if (ddlReport.SelectedValue.ToString() == "0")
        {
            x.CommandText = "ivmsDetail";
        }
        else
        {
            x.CommandText = "ivmsDetailGroup";
        }

        x.CommandType = CommandType.StoredProcedure;
        x.Connection = cnn;
        da.SelectCommand = x;

        SqlParameter pi = new SqlParameter("@i", SqlDbType.Int);
        pi.Direction = ParameterDirection.Input;
        SqlParameter pinf = new SqlParameter("@inf", SqlDbType.DateTime);
        pinf.Direction = ParameterDirection.Input;
        SqlParameter pcode = new SqlParameter("@code", SqlDbType.VarChar);
        pcode.Direction = ParameterDirection.Input;
        SqlParameter pcust = new SqlParameter("@cust", SqlDbType.VarChar);
        pcust.Direction = ParameterDirection.Input;
        SqlParameter pvendor = new SqlParameter("@vendor", SqlDbType.VarChar);
        pvendor.Direction = ParameterDirection.Input;
        SqlParameter pprefix = new SqlParameter("@prefix", SqlDbType.VarChar);
        pprefix.Direction = ParameterDirection.Input;
        SqlParameter psource = new SqlParameter("@source", SqlDbType.VarChar);
        psource.Direction = ParameterDirection.Input;

        //   source
        if (dpdNextone.SelectedValue.ToString() == "ALL")
        {
            psource.Value = "%";
        }
        else
        {
            psource.Value = dpdNextone.SelectedValue.ToString() + "%";
        }


        if (ddlReport.SelectedValue.ToString() == "0")
        {

            pi.Value = Convert.ToInt16(hfIncrement.Value);
            pinf.Value = hfHour.Value;
            pcust.Value = hfOri.Value.ToString();
            pvendor.Value = hfDest.Value.ToString();
            pcode.Value = hfDetail.Value;
            pprefix.Value = hfPrefix.Value.ToString();
            x.Parameters.Add(pi);
            x.Parameters.Add(pinf);
            x.Parameters.Add(pcust);
            x.Parameters.Add(pvendor);
            x.Parameters.Add(pcode);
            x.Parameters.Add(pprefix);
            x.Parameters.Add(psource);

        }
        else
        {
            SqlParameter psup = new SqlParameter("@sup", SqlDbType.DateTime);
            psup.Direction = ParameterDirection.Input;
            SqlParameter pselected = new SqlParameter("@selected", SqlDbType.VarChar);
            pselected.Direction = ParameterDirection.Input;
            pi.Value = Convert.ToInt16(hfReport.Value.ToString());
            pinf.Value = hfFrom.Value;
            psup.Value = hfTo.Value;
            pselected.Value = hfHour.Value;
            pcode.Value = hfDetail.Value;
            pcust.Value = hfOri.Value.ToString();
            pvendor.Value = hfDest.Value.ToString();
            pprefix.Value = hfPrefix.Value.ToString();
            x.Parameters.Add(pi);
            x.Parameters.Add(pinf);
            x.Parameters.Add(psup);
            x.Parameters.Add(pcust);
            x.Parameters.Add(pvendor);
            x.Parameters.Add(pselected);
            x.Parameters.Add(pcode);
            x.Parameters.Add(pprefix);
            x.Parameters.Add(psource);
        }
        x.CommandTimeout = 600;
        da.Fill(dsG1);
        dgDetail.DataSource = dsG1;
        dgDetail.DataBind();
    }

    protected void dgResum_SelectedIndexChanged(object sender, EventArgs e)
    {
        dgSemi.Visible = true;
        dgDetail.Visible = false;
        hfHour.Value = dgResum.SelectedItem.Cells[1].Text;
        if (hfReport.Value.ToString() == "1")
        {
            txtDest.Text = hfHour.Value.ToString();
            hfDest.Value = hfHour.Value;
        }
        else if (hfReport.Value.ToString() == "2")
        {
            txtOri.Text = hfHour.Value.ToString();
            hfOri.Value = hfHour.Value;

        }

        string strCnn = "Data Source=localhost;Initial Catalog=CDRDB_LOGS;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlConnection cnn = new SqlConnection(strCnn);
        SqlDataSource dsDatos = new SqlDataSource();
        SqlCommand x = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet dsG1 = new DataSet();
        if (hfReport.Value.ToString() == "0")
        {
            x.CommandText = "sp_ivmsSemi";
        }
        else
        {
            x.CommandText = "sp_ivmsSemiGroup";
        }

        x.CommandType = CommandType.StoredProcedure;
        x.Connection = cnn;
        da.SelectCommand = x;

        SqlParameter pi = new SqlParameter("@i", SqlDbType.Int);
        pi.Direction = ParameterDirection.Input;
        SqlParameter pinf = new SqlParameter("@inf", SqlDbType.DateTime);
        pinf.Direction = ParameterDirection.Input;
        SqlParameter pcust = new SqlParameter("@cust", SqlDbType.VarChar);
        pcust.Direction = ParameterDirection.Input;
        SqlParameter pvendor = new SqlParameter("@vendor", SqlDbType.VarChar);
        pvendor.Direction = ParameterDirection.Input;
        SqlParameter pprefix = new SqlParameter("@prefix", SqlDbType.VarChar);
        pprefix.Direction = ParameterDirection.Input;

        SqlParameter psource = new SqlParameter("@source", SqlDbType.VarChar);
        psource.Direction = ParameterDirection.Input;

        //   source
        if (dpdNextone.SelectedValue.ToString() == "ALL")
        {
            psource.Value = "%";
        }
        else
        {
            psource.Value = dpdNextone.SelectedValue.ToString() + "%";
        }

        if (hfReport.Value.ToString() == "0")
        {
            pi.Value = Convert.ToInt16(hfIncrement.Value);
            pinf.Value = hfHour.Value;
            pcust.Value = hfOri.Value.ToString();
            pvendor.Value = hfDest.Value.ToString();
            pprefix.Value = hfPrefix.Value.ToString();
            x.Parameters.Add(pi);
            x.Parameters.Add(pinf);
            x.Parameters.Add(pcust);
            x.Parameters.Add(pvendor);
            x.Parameters.Add(pprefix);
            x.Parameters.Add(psource);
        }
        else
        {
            SqlParameter psup = new SqlParameter("@sup", SqlDbType.VarChar);
            psup.Direction = ParameterDirection.Input;
            SqlParameter pselected = new SqlParameter("@selected", SqlDbType.VarChar);
            pselected.Direction = ParameterDirection.Input;
            pi.Value = ddlReport.SelectedValue.ToString();
            pinf.Value = hfFrom.Value;
            psup.Value = hfTo.Value;
            pcust.Value = hfOri.Value.ToString();
            pvendor.Value = hfDest.Value.ToString();
            pselected.Value = hfHour.Value;
            pprefix.Value = hfPrefix.Value.ToString();
            x.Parameters.Add(pi);
            x.Parameters.Add(pinf);
            x.Parameters.Add(psup);
            x.Parameters.Add(pcust);
            x.Parameters.Add(pvendor);
            x.Parameters.Add(pselected);
            x.Parameters.Add(pprefix);
            x.Parameters.Add(psource);

        }
        x.CommandTimeout = 600;
        da.Fill(dsG1);
        dgSemi.DataSource = dsG1;
        dgSemi.DataBind();

    }
    
    protected void btnShow_Click(object sender, EventArgs e)
    {
              
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        hfReport.Value = ddlReport.SelectedValue.ToString();
        dgDetail.Visible = false;
        dgSemi.Visible = false;
        if (txtOri.Text == "") txtOri.Text = "%";//DateTime.Today.AddDays(-2).ToString();
        if (txtDest.Text == "") txtDest.Text = "%";//DateTime.Today.AddDays(-1).ToString();
        hfPrefix.Value = txtPrefix.Text.ToString() + "%";
        string strCnn = "Data Source=localhost;Initial Catalog=CDRDB_LOGS;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlConnection cnn = new SqlConnection(strCnn);
        SqlDataSource dsDatos = new SqlDataSource();
        SqlCommand x = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet dsG1 = new DataSet();
        if (ddlReport.SelectedValue.ToString() == "0")
        {
            x.CommandText = "sp_ivmsResum";
        }
        else
        {
            x.CommandText = "sp_ivmsResumGroup";
        }
        x.CommandType = CommandType.StoredProcedure;
        x.Connection = cnn;
        da.SelectCommand = x;

        SqlParameter pi = new SqlParameter("@i", SqlDbType.Int);
        pi.Direction = ParameterDirection.Input;
        SqlParameter pinf = new SqlParameter("@inf", SqlDbType.DateTime);
        pinf.Direction = ParameterDirection.Input;
        SqlParameter psup = new SqlParameter("@sup", SqlDbType.DateTime);
        psup.Direction = ParameterDirection.Input;
        SqlParameter pcust = new SqlParameter("@cust", SqlDbType.VarChar);
        pcust.Direction = ParameterDirection.Input;
        SqlParameter pvendor = new SqlParameter("@vendor", SqlDbType.VarChar);
        pvendor.Direction = ParameterDirection.Input;
        SqlParameter pprefix = new SqlParameter("@prefix", SqlDbType.VarChar);
        pprefix.Direction = ParameterDirection.Input;
        SqlParameter psource = new SqlParameter("@source", SqlDbType.VarChar);
        pprefix.Direction = ParameterDirection.Input;

        if (hfReport.Value == "0")
        {
            pi.Value = Convert.ToInt16(DropDownList1.SelectedValue.ToString());
        }
        else
        {
            pi.Value = Convert.ToInt16(hfReport.Value.ToString());

        }

        //source
        if (dpdNextone.SelectedValue.ToString() == "ALL")
        {
            psource.Value = "%";
        }
        else
        {
            psource.Value = dpdNextone.SelectedValue.ToString();
        }



        pinf.Value = txtFrom.Text;
        psup.Value = txtTo.Text + " 23:59:59";
        pcust.Value = txtOri.Text;
        pvendor.Value = txtDest.Text;
        pprefix.Value = hfPrefix.Value.ToString();
        x.Parameters.Add(pi);
        x.Parameters.Add(pinf);
        x.Parameters.Add(psup);
        x.Parameters.Add(pcust);
        x.Parameters.Add(pvendor);
        x.Parameters.Add(pprefix);
        x.Parameters.Add(psource);
        x.CommandTimeout = 600;
        da.Fill(dsG1);
        dgResum.DataSource = dsG1;
        dgResum.DataBind();

        hfDest.Value = txtDest.Text;
        hfFrom.Value = txtFrom.Text;
        hfOri.Value = txtOri.Text;
        hfTo.Value = txtTo.Text + " 23:59:59";
        hfIncrement.Value = DropDownList1.SelectedValue;    
    }
    protected void dgDetail_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
