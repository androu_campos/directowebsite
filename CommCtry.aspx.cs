using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class CommCtry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TextBox1.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            TextBox2.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            
        }
        else
        {

        }
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        sqlQuery.CommandTimeout = 200;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            string erro = ex.Message;
        }

        return resultsDataSet;
    }


    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        DataSet dts = (DataSet)Session["Commsns"];
        GridView1.DataSource = dts.Tables[0];
        GridView1.DataBind();
    }

    protected void cmdReport_Click(object sender, EventArgs e)
    {
        try
        {
            string broker = "";
            cdrdbDatasetTableAdapters.loginAdp adpU = new cdrdbDatasetTableAdapters.loginAdp();
            cdrdbDataset.loginDataTable tblU = adpU.GetDataByUser(Session["DirectoUser"].ToString());
            broker = tblU.Rows[0]["BrokerName"].ToString();
            //            SqlCommand sqlSpQuery = new SqlCommand();
            //            sqlSpQuery.CommandType = System.Data.CommandType.StoredProcedure;
            //            sqlSpQuery.CommandText = "DECLARE @RC int DECLARE @Broker varchar(20) DECLARE @from smalldatetime DECLARE @to smalldatetime EXECUTE @RC = [MECCA2].[dbo].[sp_commisions] @Broker='" + broker + "',@from='" + TextBox1.Text + "',@to='" + TextBox2.Text + "'";

            //sqlSpQuery.CommandText = "sp_commisions";
            //sqlSpQuery.Parameters.Add("@Broker", SqlDbType.VarChar).Value = broker;
            //sqlSpQuery.Parameters.Add("@from", SqlDbType.VarChar).Value = TextBox1.Text.ToString();
            //sqlSpQuery.Parameters.Add("@to", SqlDbType.VarChar).Value = TextBox2.Text.ToString();

            DataSet ResultSetC;
            //            ResultSetC = RunQuery(sqlSpQuery);
            ResultSetC = Util.RunQueryByStmnt("DECLARE @RC int DECLARE @Broker varchar(20) DECLARE @from smalldatetime DECLARE @to smalldatetime EXECUTE @RC = [MECCA2].[dbo].[sp_commisions] @Broker='" + broker + "',@from='" + TextBox1.Text + "',@to='" + TextBox2.Text + "'");


            Session["Commsns"] = ResultSetC;

            int c = ResultSetC.Tables.Count;
            if (c > 0)
            {
                GridView1.DataSource = ResultSetC.Tables[0];
                GridView1.DataBind();
                cmdExport.Visible = true;
            }
            else
            {
                cmdExport.Visible = false;
            }

        }
        catch (Exception ex)
        {
            string messagerror = ex.Message;
        }
    }
    protected void cmdExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {

            DataTable dtEmployee = ((DataSet)Session["Commsns"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "Commissions" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
}
