<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="Showsalesmpop.aspx.cs" Inherits="Showsalesmpop"
    Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label5" runat="server" CssClass="labelTurkH" Text="MECCA OFFNET MINIPOP SALES LOGS"></asp:Label>
    <table style="position: absolute; top: 220px; left: 230px;" width="100%">
        <tr>
            <td align="left" valign="top">
                <table id="Set">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Choose Customer:" CssClass="labelSteelBlue"></asp:Label></td>
                        <td>
                            <asp:DropDownList ID="dpdCustomer" runat="server" Width="158px" CssClass="dropdown">
                            </asp:DropDownList></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text="Choose Vendor:" CssClass="labelSteelBlue"></asp:Label></td>
                        <td>
                            <asp:DropDownList ID="dpdVendor" runat="server" Width="158px" CssClass="dropdown">
                            </asp:DropDownList></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="From:" CssClass="labelSteelBlue"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtFrom" runat="server" Width="153px" CssClass="labelSteelBlue"></asp:TextBox></td>
                        <td>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="To:" CssClass="labelSteelBlue"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtTo" runat="server" Width="153px" CssClass="labelSteelBlue"></asp:TextBox></td>
                        <td>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="cmdShow" runat="server" OnClick="cmdShow_Click" Text="View report"
                                CssClass="boton" Width="88px" /></td>
                        <td>
                        </td>
                    </tr>
                </table>
                <td>
                </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="left">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="left">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="left">
            </td>
            <td>
            </td>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td align="left">
            </td>
            <td>
            </td>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="left">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td height="120">
            </td>
            <td align="left">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label6" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>
            </td>
            <td>
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom"
                    PopupButtonID="Image1">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"
                    PopupButtonID="Image2">
                </cc1:CalendarExtender>
            </td>
        </tr>
    </table>
    <br />
    &nbsp;&nbsp;
</asp:Content>
