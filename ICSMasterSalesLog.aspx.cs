using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class ICSMasterSalesLog : System.Web.UI.Page
{
    int[] indexs = new int[25];

    protected void Page_Load(object sender, EventArgs e)
    {
        string hometype = "";
        if (!Page.IsPostBack)
        {

            txtFrom.Value = DateTime.Today.AddDays(-1).ToShortDateString();
            txtTo.Value = DateTime.Today.AddDays(-1).ToShortDateString();

            txtToV.Text = txtTo.Value.ToString();
            txtFromV.Text = txtFrom.Value.ToString();

            SqlConnection SqlConn1 = new SqlConnection();
            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
            SqlCommand SqlCommand1 = new SqlCommand("SELECT DISTINCT ProviderName from ProviderIP WHERE Type like 'V' AND ProviderName <> 'OUTLANDER-AMIGO' AND ProviderName <> 'OUTLANDER-CALL' AND ProviderName <> 'OUTLANDER-MS' AND ProviderName <> 'OUTLANDER-PLAN' UNION SELECT '**ALL**' from ProviderIP as ProviderName", SqlConn1);
            SqlConn1.Open();
            SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
            while (myReader1.Read())
            {
                ddlVendor.Items.Add(myReader1.GetValue(0).ToString());
            }
            myReader1.Close();
            SqlConn1.Close();

            rdbtnlType.SelectedIndex = Convert.ToInt32(Session["ReportChoosed"]);//7;
            fillRadioButton(Convert.ToInt32(Session["ReportChoosed"]));
            Session["tiempo"] = "0";

            if (Session["hometype"] != "")
            {
                hometype = Session["hometype"].ToString();



                if (hometype == "W")
                    Label3.Text = "MECCA WHOLESALE MASTER SALES LOG";

                else if (hometype == "R")
                    Label3.Text = "MECCA ICS MASTER SALES LOG";


            }
            else
                Label3.Text = "MECCA TOTAL MASTER SALES LOG";

        }
        else
        {
            Session["ReportChoosed"] = rdbtnlType.SelectedIndex;
            //fillRadioButton(Convert.ToInt32(Session["ReportChoosed"]));


            Session["CountrySelected"] = ddlCountry.SelectedValue.ToString();

            Session["regionSelectedMstr"] = ddlRegion.SelectedValue.ToString();

            int selectIndx = ddlRegion.SelectedIndex;


            //string country = ddlCountry.SelectedValue.ToString();
            //country = country.Replace("**ALL**", "%");
            //DataSet dtst = Util.RunQueryByStmntatCDRDB("Select distinct region from regions where country = '" + country + "' UNION SELECT '**ALL**' AS REGION FROM REGIONS ORDER BY REGION ");
            //DataTable tbl = dtst.Tables[0];
            //foreach (DataRow row in tbl.Rows)
            //{
            //    ddlRegion.Items.Add(row[0].ToString());
            //}
            //ddlRegion.Enabled = true;
            //ddlRegion.SelectedIndex = selectIndx;
        }
    }
    protected void ddlCust_SelectedIndexChanged(object sender, EventArgs e)
    {

        //if (ddlCust.SelectedItem.ToString() == "**ALL**") hfCust.Value = "%";
        //else hfCust.Value = ddlCust.SelectedItem.ToString();

    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {


    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlRegion.Items.Clear();
        string country = ddlCountry.SelectedValue.ToString();
        country = country.Replace("**ALL**", "%");
        DataSet dtst = Util.RunQueryByStmntatCDRDB("Select distinct region from regions where country = '" + country + "' UNION SELECT '**ALL**' AS REGION FROM REGIONS ORDER BY REGION ");
        DataTable tbl = dtst.Tables[0];
        foreach (DataRow row in tbl.Rows)
        {
            ddlRegion.Items.Add(row[0].ToString());
        }
        ddlRegion.Enabled = true;
        //ddlRegion.SelectedIndex = selectIndx;


    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string SelectString, WhereString, FromString, FromSBro = "", GroupByString, UnionString, BrokerWhere, QQ, sOrder;
        string sF, sW, sS, sSt, sG, sSUM = "";
        string hometype = "", sourcetable = "", routetype = "";
        string businessunit = "", custunits = "";

        bool bCV = false, bVB = false;

        if (Session["hometype"] != "")
        {
            hometype = Session["hometype"].ToString();

            if (hometype == "W")
                sourcetable = "FROM MECCA2..trafficlogsegments T";

            if (hometype == "W")
            {
                routetype = " routetype = 'W' ";
                Label3.Text = "MECCA WHOLESALE MASTER SALES LOG";
            }
            else if (hometype == "R")
            {
                routetype = " routetype = 'R' ";
                Label3.Text = "MECCA ICS MASTER SALES LOG";
            }

        }
        else
        {
            sourcetable = "FROM MECCA2..trafficlog T";
            Label3.Text = "MECCA TOTAL MASTER SALES LOG";
        }

        if (Session["regionSelectedMstr"].ToString() == "**ALL**") hfRegion.Value = "%";
        else if (Session["regionSelectedMstr"].ToString() == "** NONE **") hfRegion.Value = "%";
        else if (Session["regionSelectedMstr"].ToString() == "") hfRegion.Value = "%";
        else hfRegion.Value = Session["regionSelectedMstr"].ToString();

        if (ddlCountry.SelectedItem.ToString() == "**ALL**") hfCountry.Value = "%";
        //if (hfCust.Value.ToString() == "") hfCust.Value = "%";

        if (ddlCust.SelectedItem.ToString() == "**ALL**") hfCust.Value = "ICS%";
        else hfCust.Value = ddlCust.SelectedItem.ToString();

        //if (hfVendor.Value.ToString() == "") hfVendor.Value = "%";

        if (ddlVendor.SelectedItem.ToString() == "**ALL**") hfVendor.Value = "%";
        else if (ddlVendor.SelectedItem.ToString() == "** NONE **") hfVendor.Value = "%";
        else hfVendor.Value = ddlVendor.SelectedItem.ToString();

        //if (ddlCustBroker.SelectedItem.ToString() == "**ALL**") hfCustBroker.Value = "%";
        //else hfCustBroker.Value = ddlCustBroker.SelectedItem.ToString();

        ////if (hfCustBroker.Value.ToString() == "") hfCustBroker.Value = "%";
        //if (ddlVendorBroker.SelectedItem.ToString() == "**ALL**") hfVendorBroker.Value = "%";
        //else hfVendorBroker.Value = ddlVendorBroker.SelectedItem.ToString();

        //if (hfVendorBroker.Value.ToString() == "") hfVendorBroker.Value = "%";

        //FIN checando valores no asignados

        SelectString = "";
        BrokerWhere = "";
        FromString = " FM2TL";

        WhereString = "";

        GroupByString = "";
        UnionString = "";

        //WHERE Builder

        string from = txtFromV.Text;
        string to = txtToV.Text;
        from = from.Replace("/", "|");
        to = to.Replace("/", "|");


        WhereString = WhereString + "'" + from + "' AND '" + to + "'";
        if (hfCust.Value.ToString() != "%")
        {
            businessunit = hfCust.Value.ToString();
            custunits = getCustomer(businessunit);

            if (custunits != "'-'")
            {
                WhereString = WhereString + " AND T.Customer in ( " + custunits + ") ";
            }
            else
                WhereString = WhereString + " AND T.Customer LIKE '" + hfCust.Value.ToString() + "'";
        }
        if (hfVendor.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Vendor LIKE '" + hfVendor.Value.ToString() + "'";
        }
        if (hfCountry.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Country LIKE '" + ddlCountry.SelectedValue.ToString() + "'";
        }
        if (hfRegion.Value.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Region LIKE '" + hfRegion.Value.ToString() + "'";
        }
        //LMC
        if (dpdLMC.SelectedValue.ToString() != "**Select LMC**")
        {
            WhereString = WhereString + " AND T.LMC ='" + dpdLMC.SelectedValue.ToString() + "'";
        }
        //TYPE
        if (dpdType.SelectedValue.ToString() != "**Select Type**")
        {
            WhereString = WhereString + " AND T.Type ='" + dpdType.SelectedValue.ToString() + "'";
        }
        //CLASS 
        if (dpdClass.SelectedValue.ToString() != "%")
        {
            WhereString = WhereString + " AND T.Class LIKE '" + dpdClass.SelectedValue.ToString() + "'";
        }

        //if (hfCustBroker.Value.ToString() != "%")
        //{
        //    WhereString = WhereString + " AND B.CustBroker LIKE '" + hfCustBroker.Value.ToString() + "'";
        //    FromSBro = FromSBro + " LJM2CB";
        //    bCV = true;
        //}
        //if (hfVendorBroker.Value.ToString() != "%")
        //{
        //    WhereString = WhereString + " AND V.VendorBroker LIKE '" + hfVendorBroker.Value.ToString() + "'";
        //    FromSBro = FromSBro + " LJM2VB";
        //    bVB = true;
        //}

        //FIN WHERE Builder


        if (chkBilling.Items[0].Selected == true)//GROUP BY DATE
        {
            SelectString = SelectString + ",CONVERT(VARCHAR(11) ,T.Billingdate, " + cmbDateFormat.SelectedValue.ToString() + ") as BillingDate";
            GroupByString = GroupByString + ",T.BillingDate";
            UnionString = UnionString + ",'** ALL **' AS BillingDate";
        }

        if (ckboxl.Items[0].Selected == true)
        //if (indexs[0] == 1)
        {
            if (custunits != "'-'" && hfCust.Value.ToString() != "%")
            {
                SelectString = SelectString + ",'" + businessunit + "' AS Customer";
            }
            else
            {
                SelectString = SelectString + ",T.Customer";
                GroupByString = GroupByString + ",T.Customer";
            }

            UnionString = UnionString + ",'****' AS Customer";
        }
        if (ckboxl.Items[1].Selected == true)
        //if (indexs[1] == 1)
        {
            SelectString = SelectString + ",T.Vendor";
            GroupByString = GroupByString + ",T.Vendor";
            UnionString = UnionString + ",'****' AS Vendor";
        }
        //if (ckboxl.Items[2].Selected == true)
        ////if (indexs[2] == 1)
        //{
        //    SelectString = SelectString + ",CustBroker";
        //    GroupByString = GroupByString + ",B.CustBroker";
        //    UnionString = UnionString + ",'****' aS CustBroker";
        //    if (bCV == false) FromSBro = FromSBro + " LJM2CB";
        //}
        //if (ckboxl.Items[3].Selected == true)
        ////if (indexs[3] == 1)
        //{
        //    SelectString = SelectString + ",VendorBroker";
        //    GroupByString = GroupByString + ",V.VendorBroker";
        //    UnionString = UnionString + ",'****' as VendorBroker";
        //    if (bVB == false) FromSBro = FromSBro + " LJM2VB";
        //}
        if (ckboxl.Items[4].Selected == true)
        //if (indexs[4] == 1)
        {
            SelectString = SelectString + ",T.Country";
            GroupByString = GroupByString + ",T.Country";
            UnionString = UnionString + ",'****' as Country";
        }
        if (ckboxl.Items[5].Selected == true)
        //if (indexs[5] == 1)
        {
            SelectString = SelectString + ",T.Region";
            GroupByString = GroupByString + ",T.Region";
            UnionString = UnionString + ",'****' as Region";
        }
        if (ckboxl.Items[6].Selected == true)
        //if (indexs[6] == 1)
        {
            SelectString = SelectString + ",T.RegionPrefix";
            GroupByString = GroupByString + ",T.RegionPrefix";
            UnionString = UnionString + ",'****' as RegionPrefix";
        }
        if (ckboxl.Items[7].Selected == true)
        //if (indexs[7] == 1)
        {
            SelectString = SelectString + ",T.CustPrefix";
            GroupByString = GroupByString + ",T.CustPrefix";
            UnionString = UnionString + ",'****'";
        }
        if (ckboxl.Items[8].Selected == true)
        //if (indexs[8] == 1)
        {
            SelectString = SelectString + ",T.VendorPrefix";
            GroupByString = GroupByString + ",T.VendorPrefix";
            UnionString = UnionString + ",'****' as VendorPrefix";
        }
        if (ckboxl.Items[9].Selected == true || dpdType.SelectedValue.ToString() != "**Select Type**")
        //if (indexs[9] == 1 || dpdType.SelectedValue.ToString() != "**Select Type**")
        {
            SelectString = SelectString + ",T.Type";
            GroupByString = GroupByString + ",T.Type";
            UnionString = UnionString + ",'****' as [Type]";
            //FromString = FromString + " LJM2R";
        }
        if (ckboxl.Items[10].Selected == true || dpdLMC.SelectedValue.ToString() != "**Select LMC**")
        //if (indexs[10] == 1 || dpdLMC.SelectedValue.ToString() != "**Select LMC**")
        {
            SelectString = SelectString + ",T.LMC";
            GroupByString = GroupByString + ",T.LMC";
            UnionString = UnionString + ",'****' as LMC";

            //if (ckboxl.Items[9].Selected == false && dpdType.SelectedValue.ToString() == "**Select Type**")
            //if (indexs[9] == 0)
            //{
            //  FromString = FromString + " LJM2R";
            //}
        }
        //CLASS                 
        //11 es class
        if (ckboxl.Items[11].Selected == true || dpdClass.SelectedValue.ToString() != "%")
        //if (indexs[11] == 1 || dpdClass.SelectedValue.ToString() != "%")
        {
            SelectString = SelectString + ",T.Class";
            GroupByString = GroupByString + ",T.Class";
            UnionString = UnionString + ",'****' as Class";
            //if (ckboxl.Items[10].Selected == false && ckboxl.Items[9].Selected == false && dpdType.SelectedValue.ToString() == "**Select Type**" && dpdLMC.SelectedValue.ToString() == "**Select LMC**")
            //    //if (indexs[10] != 1 && indexs[9] != 1)
            //{
            //    FromString = FromString + " LJM2R";
            //}

        }

        if (ckboxl.Items[12].Selected == true)
        //if (indexs[12] == 1)
        {
            SelectString = SelectString + ",T.Cost";
            GroupByString = GroupByString + ",T.Cost";
            UnionString = UnionString + ",NULL as Cost";
        }
        if (ckboxl.Items[13].Selected == true)
        //if (indexs[13] == 1)
        {
            SelectString = SelectString + ",T.Price";
            GroupByString = GroupByString + ",T.Price";
            UnionString = UnionString + ",NULL AS Price";
        }
        //Arriba con group
        //Sin group abajo

        if (ckboxl.Items[14].Selected == true)
        //if (indexs[14] == 1)
        {
            sSUM = sSUM + ",ROUND(SUM(T.CustMinutes),4) as CMinutes";
        }
        if (ckboxl.Items[15].Selected == true)
        //if (indexs[15] == 1)
        {
            sSUM = sSUM + ",ROUND(SUM(T.VendorMinutes),4) as VMinutes";
        }
        if (ckboxl.Items[16].Selected == true)
        //if (indexs[16] == 1)
        {
            sSUM = sSUM + ",ROUND(SUM(T.TotalCost),4) as TotalCost";
        }
        if (ckboxl.Items[17].Selected == true)
        //if (indexs[17] == 1)
        {
            sSUM = sSUM + ",ROUND(SUM(T.TotalPrice),4) as TotalSales";
        }
        if (ckboxl.Items[18].Selected == true)
        //if (indexs[18] == 1)
        {
            sSUM = sSUM + ",ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit";
        }

        if (ckboxl.Items[19].Selected == true)
        //if (indexs[19] == 1)
        {
            sSUM = sSUM + ",SUM(T.tCalls) as Attempts";
        }

        if (ckboxl.Items[20].Selected == true)
        //if (indexs[20] == 1)
        {
            sSUM = sSUM + ",SUM(T.BillableCalls) as AnsweredCalls";
        }

        if (ckboxl.Items[21].Selected == true)
        //if (indexs[21] == 1)
        {
            //ASR (answer seizure ratio) = Answered calls / (all failed calls � calls rejected with 34) 
            sSUM = sSUM + ",SUM(T.RA) as [Rejected Calls]";
        }

        if (ckboxl.Items[22].Selected == true)
        //if (indexs[22] == 1)
        {
            //ASR (answer seizure ratio) = Answered calls / (all failed calls � calls rejected with 34) 
            sSUM = sSUM + ",mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR";
        }

        if (ckboxl.Items[23].Selected == true)
        //if (indexs[23] == 1)
        {
            //ABR (answer to bid ration, or peg count) = Answered calls / (all failed calls)

            sSUM = sSUM + ",mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR";
        }

        if (ckboxl.Items[24].Selected == true)
        //if (indexs[24] == 1)
        {
            sSUM = sSUM + ",mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD";
        }


        sS = "";
        sOrder = "";
        if (SelectString.Length > 3)
        {
            sS = SelectString.Substring(1);
            sOrder = " ORDER BY " + sS.Replace("CONVERT(VARCHAR(11) ,T.Billingdate, " + cmbDateFormat.SelectedValue.ToString() + ") as", " ");

            if (custunits != "'-'" && hfCust.Value.ToString() != "%")
            {
                sOrder = sOrder.Replace("'" + businessunit + "' AS Customer,", " ");
                sOrder = sOrder.Replace(",'" + businessunit + "' AS Customer", " ");
            }
        }
        else
        {
            if (sSUM.Length > 3)
            {
                sSUM = sSUM.Substring(1);
            }
        }

        string sHaving = " HAVING SUM(CustMinutes) > 0 ";
        sG = string.Empty;

        sF = FromString + FromSBro;
        sW = WhereString;
        if (GroupByString.Length > 3)
        {
            sG = " GROUP BY " + GroupByString.Substring(1);
        }
        sSt = "";
        if (UnionString.Length > 1)
        {
            sSt = UnionString.Substring(1);
        }

        sW = sW.Replace("|", "/");

        if(hfCust.Value.ToString() == "ICS-CTR")
        {
            sF = sF.Replace("FM2TL", "FROM ICS..trafficlogoutbound T");
        }
        else
        {
            sF = sF.Replace("FM2TL", "FROM MECCA2..trafficlog T");
        }
        
        sF = sF.Replace("LJM2CB", "LEFT JOIN MECCA2..CBroker B ON T.Customer = B.Customer");
        sF = sF.Replace("LJM2VB", "LEFT JOIN MECCA2..VBroker V ON T.Vendor = V.Vendor");
        sF = sF.Replace("LJM2R", "LEFT JOIN MECCA2.dbo.Regions R ON T.RegionPrefix = R.Code");

        QQ = "sS=" + sS + "&sF=" + sF + "&sW=" + sW + "&sG=" + sG + "&sSt=" + sSt + "&sSUM=" + sSUM;

        string Query = " SELECT " + sS + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT " + sSt + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + sHaving + sOrder;



        //if (hometype == "")
        Query = " SELECT " + sS + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT " + sSt + sSUM + sF + " WHERE BillingDate BETWEEN " + sW + sHaving + sOrder;
        //else
        //Query = " SELECT " + sS + sSUM + sF + " WHERE " + routetype + " and BillingDate BETWEEN " + sW + " " + sG + sHaving + " UNION SELECT " + sSt + sSUM + sF + " WHERE " + routetype + " and BillingDate BETWEEN " + sW + sHaving + sOrder;

        Session["SalesLogS"] = Query + " option (maxdop 1)";

        string dfrom = from;
        string dto = to;

        dfrom = from.Replace("|", "/");
        dto = to.Replace("|", "/");

        Session["dfrom"] = dfrom.ToString();
        Session["dto"] = dto.ToString();
        //
        Session["ReportChoosed"] = rdbtnlType.SelectedIndex;

        //Response.Write(Session["SalesLogS"]);
        //Response.End();

        if ((sS.Length + sSUM.Length) > 6)
        {
            Response.Redirect("~/MSLH.aspx", false);
        }

    }
    public void fillRadioButton(int index)
    {

        //Session["ReportChoosed"] = index;

        if (index == 0)//by region
        {
            checkList();

            this.ckboxl.ClearSelection();

            this.ckboxl.Items[0].Selected = true;//Cust

            this.ckboxl.Items[1].Selected = true;//Vendor

            this.ckboxl.Items[4].Selected = true;//Country

            this.ckboxl.Items[5].Selected = true;//Region            

            this.ckboxl.Items[20].Selected = true;//Calls

            this.ckboxl.Items[14].Selected = true;//Minutes

            this.ckboxl.Items[15].Selected = true;//Minutes

            this.ckboxl.Items[12].Selected = true;//Buy

            this.ckboxl.Items[13].Selected = true;//Sell           

            this.ckboxl.Items[18].Selected = true;//Profit




        }
        else if (index == 1)//Normal
        {
            checkList();
            this.ckboxl.ClearSelection();

            this.ckboxl.Items[0].Selected = true;//Cust

            this.ckboxl.Items[1].Selected = true;//Vendor

            this.ckboxl.Items[4].Selected = true;//Country

            this.ckboxl.Items[5].Selected = true;//Region            

            this.ckboxl.Items[20].Selected = true;//Calls

            this.ckboxl.Items[14].Selected = true;//MinutesC

            this.ckboxl.Items[15].Selected = true;//MinutesV

            this.ckboxl.Items[12].Selected = true;//Buy

            this.ckboxl.Items[13].Selected = true;//Sell

            this.ckboxl.Items[16].Selected = true;//TotalCost

            this.ckboxl.Items[17].Selected = true;//TotalSales            

            this.ckboxl.Items[18].Selected = true;//Profit



        }
        else if (index == 2)//Prefix
        {
            checkList();

            this.ckboxl.ClearSelection();

            this.ckboxl.Items[0].Selected = true;//Cust

            this.ckboxl.Items[1].Selected = true;//Vendor

            this.ckboxl.Items[4].Selected = true;//Country

            this.ckboxl.Items[5].Selected = true;//Region            

            this.ckboxl.Items[6].Selected = true;//RegionPrefix

            this.ckboxl.Items[7].Selected = true;//CustPrefix            

            this.ckboxl.Items[8].Selected = true;//VendorPrefix

            this.ckboxl.Items[20].Selected = true;//Calls

            this.ckboxl.Items[15].Selected = true;//MinutesC

            this.ckboxl.Items[16].Selected = true;//MinutesV

            this.ckboxl.Items[12].Selected = true;//Buy

            this.ckboxl.Items[13].Selected = true;//Sell

            this.ckboxl.Items[16].Selected = true;//TotalCost

            this.ckboxl.Items[17].Selected = true;//TotalSales

            this.ckboxl.Items[18].Selected = true;//Profit



        }
        else if (index == 3)//Totals
        {
            checkList();

            this.ckboxl.ClearSelection();

            this.ckboxl.Items[0].Selected = true;//Cust   

            this.ckboxl.Items[20].Selected = true;//Calls

            this.ckboxl.Items[14].Selected = true;//MinutesC

            this.ckboxl.Items[15].Selected = true;//MinutesV

            this.ckboxl.Items[16].Selected = true;//TotalCost

            this.ckboxl.Items[17].Selected = true;//TotalSales

            this.ckboxl.Items[18].Selected = true;//Profit


        }
        else if (index == 4)//by region
        {
            checkList();

            this.ckboxl.ClearSelection();
            this.ckboxl.Items[0].Selected = true;//Cust

            this.ckboxl.Items[1].Selected = true;//Vendor

            this.ckboxl.Items[4].Selected = true;//Country

            this.ckboxl.Items[5].Selected = true;//Region            

            this.ckboxl.Items[20].Selected = true;//Calls

            this.ckboxl.Items[14].Selected = true;//MinutesC

            this.ckboxl.Items[15].Selected = true;//MinutesV

            this.ckboxl.Items[9].Selected = true;//Type

            this.ckboxl.Items[12].Selected = true;//Buy

            this.ckboxl.Items[13].Selected = true;//Sell           

            this.ckboxl.Items[18].Selected = true;//Profit




        }
        else if (index == 5)//by region
        {
            checkList();
            this.ckboxl.ClearSelection();
            this.ckboxl.Items[0].Selected = true;//Cust

            this.ckboxl.Items[19].Selected = true;//Calls

            this.ckboxl.Items[13].Selected = true;//Minutes

            this.ckboxl.Items[17].Selected = true;//Profit

        }
        else if (index == 6)//by region
        {
            checkList();

            this.ckboxl.ClearSelection();
            this.ckboxl.Items[1].Selected = true;//Cust

            this.ckboxl.Items[20].Selected = true;//Calls

            this.ckboxl.Items[15].Selected = true;//Minutes

            this.ckboxl.Items[18].Selected = true;//Profit



        }
        else //Personalizado
        {
            checkList();
            if (ckboxl.Items[0].Selected == true)

                this.ckboxl.ClearSelection();
            this.ckboxl.Items[18].Selected = true;//Profit            
            this.ckboxl.Enabled = true;


        }

    }
    public void checkList()
    {
        for (int i = 0; i <= 24; i++)
        {
            if (this.ckboxl.Items[i].Selected == true)
            {
                indexs[i] = 1;
            }
            else
            {
                indexs[i] = 0;
            }
        }
    }
    protected void rdbtnlType_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (rdbtnlType.SelectedIndex == 0)//by region
        {
            ckboxl.ClearSelection();
            ckboxl.Items[0].Selected = true;//Cust
            ckboxl.Items[1].Selected = true;//Vendor
            ckboxl.Items[4].Selected = true;//Country
            ckboxl.Items[5].Selected = true;//Region            
            ckboxl.Items[20].Selected = true;//Calls
            ckboxl.Items[14].Selected = true;//Minutes
            ckboxl.Items[15].Selected = true;//Minutes
            ckboxl.Items[12].Selected = true;//Buy
            ckboxl.Items[13].Selected = true;//Sell           
            ckboxl.Items[18].Selected = true;//Profit

        }
        else if (rdbtnlType.SelectedIndex == 1)//Normal
        {
            ckboxl.ClearSelection();
            ckboxl.Items[0].Selected = true;//Cust
            ckboxl.Items[1].Selected = true;//Vendor
            ckboxl.Items[4].Selected = true;//Country
            ckboxl.Items[5].Selected = true;//Region            
            ckboxl.Items[20].Selected = true;//Calls
            ckboxl.Items[14].Selected = true;//MinutesC
            ckboxl.Items[15].Selected = true;//MinutesV
            ckboxl.Items[12].Selected = true;//Buy
            ckboxl.Items[13].Selected = true;//Sell
            ckboxl.Items[16].Selected = true;//TotalCost
            ckboxl.Items[17].Selected = true;//TotalSales            
            ckboxl.Items[18].Selected = true;//Profit

        }
        else if (rdbtnlType.SelectedIndex == 2)//Prefix
        {
            ckboxl.ClearSelection();
            ckboxl.Items[0].Selected = true;//Cust
            ckboxl.Items[1].Selected = true;//Vendor
            ckboxl.Items[4].Selected = true;//Country
            ckboxl.Items[5].Selected = true;//Region            
            ckboxl.Items[6].Selected = true;//RegionPrefix
            ckboxl.Items[7].Selected = true;//CustPrefix            
            ckboxl.Items[8].Selected = true;//VendorPrefix
            ckboxl.Items[20].Selected = true;//Calls
            ckboxl.Items[15].Selected = true;//MinutesC
            ckboxl.Items[16].Selected = true;//MinutesV
            ckboxl.Items[12].Selected = true;//Buy
            ckboxl.Items[13].Selected = true;//Sell
            ckboxl.Items[16].Selected = true;//TotalCost
            ckboxl.Items[17].Selected = true;//TotalSales
            ckboxl.Items[18].Selected = true;//Profit
        }
        else if (rdbtnlType.SelectedIndex == 3)//Totals
        {
            ckboxl.ClearSelection();
            ckboxl.Items[0].Selected = true;//Cust   
            ckboxl.Items[20].Selected = true;//Calls
            ckboxl.Items[14].Selected = true;//MinutesC
            ckboxl.Items[15].Selected = true;//MinutesV
            ckboxl.Items[16].Selected = true;//TotalCost
            ckboxl.Items[17].Selected = true;//TotalSales
            ckboxl.Items[18].Selected = true;//Profit
        }
        else if (rdbtnlType.SelectedIndex == 4)//by region
        {
            ckboxl.ClearSelection();
            ckboxl.Items[0].Selected = true;//Cust
            ckboxl.Items[1].Selected = true;//Vendor
            ckboxl.Items[4].Selected = true;//Country
            ckboxl.Items[5].Selected = true;//Region            
            ckboxl.Items[20].Selected = true;//Calls
            ckboxl.Items[14].Selected = true;//MinutesC
            ckboxl.Items[15].Selected = true;//MinutesV
            ckboxl.Items[9].Selected = true;//Type
            ckboxl.Items[12].Selected = true;//Buy
            ckboxl.Items[13].Selected = true;//Sell           
            ckboxl.Items[18].Selected = true;//Profit

        }
        else if (rdbtnlType.SelectedIndex == 5)//Yesterday profit by Customer
        {
            ckboxl.ClearSelection();
            ckboxl.Items[0].Selected = true;//Cust
            ckboxl.Items[20].Selected = true;//Answered Calls
            ckboxl.Items[14].Selected = true;//Minutes Customer
            ckboxl.Items[18].Selected = true;//Profit
        }
        else if (rdbtnlType.SelectedIndex == 6)//Yesterday profit by Vendor
        {
            ckboxl.ClearSelection();
            ckboxl.Items[1].Selected = true;//Vendor
            ckboxl.Items[20].Selected = true;//Answered Calls
            ckboxl.Items[15].Selected = true;//Minutes Vendor
            ckboxl.Items[18].Selected = true;//Profit
        }
        else //Personalizado
        {
            ckboxl.ClearSelection();
            ckboxl.Items[18].Selected = true;//Profit
            ckboxl.Enabled = true;
        }

    }
    protected void MECCA_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("http://mecca3.computer-tel.com/MECCA/");
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect("MasterSalesLog.aspx");
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    protected void ddlCustBroker_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlCustBroker.SelectedItem.ToString() == "**ALL**") hfCustBroker.Value = "%";
        //else hfCustBroker.Value = ddlCustBroker.SelectedItem.ToString();

    }
    protected void ddlVendorBroker_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlVendorBroker.SelectedItem.ToString() == "**ALL**") hfVendorBroker.Value = "%";
        //else hfVendorBroker.Value = ddlVendorBroker.SelectedItem.ToString();

    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void cbxLtiempo_SelectedIndexChanged(object sender, EventArgs e)
    {
        DateTime hoy;
        hoy = DateTime.Today.AddDays(-1);
        Session["tiempo"] = cbxLtiempo.SelectedIndex.ToString();
        chkBilling.Items[0].Selected = false;
        if (cbxLtiempo.SelectedIndex == 0)
        {
            txtFromV.Text = DateTime.Today.AddDays(-1).ToShortDateString();
            txtToV.Text = DateTime.Today.AddDays(-1).ToShortDateString();
        }
        else if (cbxLtiempo.SelectedIndex == 1)
        {
            setDays(Convert.ToDateTime(txtFromV.Text));
        }
        else if (cbxLtiempo.SelectedIndex == 2)
        {
            setDays(hoy);
        }
    }
    protected void txtFromV_TextChanged(object sender, EventArgs e)
    {
        setDays(Convert.ToDateTime(txtFromV.Text));
    }
    protected void txtToV_TextChanged(object sender, EventArgs e)
    {
        setDays(Convert.ToDateTime(txtToV.Text));
    }
    protected void setDays(DateTime dia)
    {
        DayOfWeek da;

        if (Session["tiempo"].ToString() == "1")
        {
            dia = dia.AddDays(1 - dia.Day);
            txtFromV.Text = dia.ToShortDateString();

            dia = dia.AddMonths(1);
            dia = dia.AddDays(-dia.Day);
            txtToV.Text = dia.ToShortDateString();
        }
        else if (Session["tiempo"].ToString() == "2")
        {
            da = dia.DayOfWeek;
            if (da == DayOfWeek.Monday)
            {
                txtFromV.Text = dia.AddDays(0).ToShortDateString();
                txtToV.Text = dia.AddDays(6).ToShortDateString();
            }
            else if (da == DayOfWeek.Tuesday)
            {
                txtFromV.Text = dia.AddDays(-1).ToShortDateString();
                txtToV.Text = dia.AddDays(5).ToShortDateString();
            }
            else if (da == DayOfWeek.Wednesday)
            {
                txtFromV.Text = dia.AddDays(-2).ToShortDateString();
                txtToV.Text = dia.AddDays(4).ToShortDateString();
            }
            else if (da == DayOfWeek.Thursday)
            {
                txtFromV.Text = dia.AddDays(-3).ToShortDateString();
                txtToV.Text = dia.AddDays(3).ToShortDateString();
            }
            else if (da == DayOfWeek.Friday)
            {
                txtFromV.Text = dia.AddDays(-4).ToShortDateString();
                txtToV.Text = dia.AddDays(2).ToShortDateString();
            }
            else if (da == DayOfWeek.Saturday)
            {
                txtFromV.Text = dia.AddDays(-5).ToShortDateString();
                txtToV.Text = dia.AddDays(1).ToShortDateString();
            }
            else if (da == DayOfWeek.Sunday)
            {
                txtFromV.Text = dia.AddDays(-6).ToShortDateString();
                txtToV.Text = dia.AddDays(0).ToShortDateString();
            }
        }


    }

    protected string getCustomer(string customer)
    {
        string cust = "";
        String connectionString = ConfigurationManager.ConnectionStrings["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter;
        SqlCommand sQuery = new SqlCommand();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@businessunit", SqlDbType.VarChar);
        param[0].Value = customer;
        sQuery.Parameters.AddRange(param);

        DataSet ResultSet = new DataSet();

        sQuery.CommandType = System.Data.CommandType.StoredProcedure;
        sQuery.CommandText = "sp_getbusinessuc";
        sQuery.Connection = DBConnection;
        dbAdapter = new SqlDataAdapter(sQuery);

        dbAdapter.Fill(ResultSet);

        int index = 0;



        if (ResultSet.Tables.Count > 0)
        {
            while (index < ResultSet.Tables[0].Rows.Count)
            {
                cust += "'" + ResultSet.Tables[0].Rows[index][0].ToString() + "',";
                index++;
            }

            cust = cust.Substring(0, cust.Length - 1);
        }

        return cust;
    }
}
