using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class show : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        cdrdbDatasetTableAdapters.CustTableAdapter adp = new cdrdbDatasetTableAdapters.CustTableAdapter();
        gdvCust.DataSource = adp.GetCustTable();
        gdvCust.DataBind();

        cdrdbDatasetTableAdapters.VendTableAdapter adpV = new cdrdbDatasetTableAdapters.VendTableAdapter();
        gdvVend.DataSource = adpV.GetVendTable();
        gdvVend.DataBind();

        MyDataSetTableAdaptersTableAdapters.MinipopReportTableAdapter adpM = new MyDataSetTableAdaptersTableAdapters.MinipopReportTableAdapter();
        gdvUK.DataSource = adpM.GetData();
        gdvUK.DataBind();
        
        

        

    }
}
