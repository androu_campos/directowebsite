using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class EditGroup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            dpdGroup.DataSource = SqlDistinctNames;
            dpdGroup.DataBind();
        }
        else
        { 
        
        }
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string name = dpdGroup.SelectedValue.ToString();

        AccessDatasetTableAdapters.PlanBreakOutAdp adp = new AccessDatasetTableAdapters.PlanBreakOutAdp();
        AccessDataset.PlanBreakOutDataTable tbl = adp.GetDataByName(name);

        txtNewName.Text = tbl.Rows[0]["Name"].ToString();
        txtNewRate.Text = tbl.Rows[0]["Rate"].ToString();
    }
    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            string name = dpdGroup.SelectedValue.ToString();
            AccessDatasetTableAdapters.PlanBreakOutAdp adp = new AccessDatasetTableAdapters.PlanBreakOutAdp();
            AccessDataset.PlanBreakOutDataTable tbl = adp.GetDataByName(name);
            int id = Convert.ToInt32(tbl.Rows[0]["IDPlan"].ToString());

            string rateS = txtNewRate.Text;

            int lenght = rateS.Length;
            int point = rateS.IndexOf(".", 0);
            rateS = rateS.Substring(point + 1, lenght - point - 1);
            if (rateS.Length > 4)
            {
                lblAfter.Text = "The max length of decimals is 4";
                lblAfter.Visible = true;
            }
            else
            {
                adp.Update(txtNewName.Text, Convert.ToDecimal(txtNewRate.Text), id);
                lblAfter.Text = "Group Updated ";
                lblAfter.Visible = true;
                dpdGroup.DataSource = SqlDistinctNames;
                dpdGroup.DataBind();

            }
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }
    }

    
    protected void dpdGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        string name = dpdGroup.SelectedValue.ToString();

        AccessDatasetTableAdapters.PlanBreakOutAdp adp = new AccessDatasetTableAdapters.PlanBreakOutAdp();
        AccessDataset.PlanBreakOutDataTable tbl = adp.GetDataByName(name);

        txtNewName.Text = tbl.Rows[0]["Name"].ToString();
        txtNewRate.Text = tbl.Rows[0]["Rate"].ToString();
    }
    protected void SqlDistinctNames_Load(object sender, EventArgs e)
    {

    }
}
