using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class TelcelVendorRep : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string sql = "";
        DataSet ds = null;



        sql = "Select Vendor,ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
        + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
        + " SUM(Calls)) as ACD "
        + " from [mecca2].[dbo].OpSheetAll "
        + " where LMC = 'TELCEL' AND COUNTRY = 'MEXICO' "
        + " group by Vendor,ISNULL([Type],'TBD') "
        + "UNION "
        + "SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
        + "sum(Attempts) as Attempts, "
        + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
        + "mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
        + "mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
        + "mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
        + "from [mecca2].[dbo].OpSheetAll "
        + " where LMC = 'TELCEL' AND COUNTRY = 'MEXICO' "
        + "order by  TotalMinutes DESC ";







        ds = Util.RunQueryByStmnt(sql);
        gdvToday.DataSource = ds.Tables[0];
        gdvToday.DataBind();


        sql = "Select Vendor,ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
       + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
       + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
       + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
       + " SUM(Calls)) as ACD "
       + " from [mecca2].[dbo].OpSheetInc "
       + " where LMC = 'TELCEL' AND COUNTRY = 'MEXICO' "
       + " group by Vendor,ISNULL([Type],'TBD') "
       + "UNION "
       + "SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
       + "sum(Attempts) as Attempts, "
       + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
       + "mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
       + "mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
       + "mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
       + "from [mecca2].[dbo].OpSheetInc "
       + " where LMC = 'TELCEL' AND COUNTRY = 'MEXICO' "
       + "order by  TotalMinutes DESC ";


        ds = Util.RunQueryByStmnt(sql);
        gdvRecent.DataSource = ds.Tables[0];
        gdvRecent.DataBind();


        sql = "Select Vendor,ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
       + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
       + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
       + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
       + " SUM(Calls)) as ACD "
       + " from [mecca2].[dbo].OpSheetY "
       + " where LMC = 'TELCEL' AND COUNTRY = 'MEXICO' "
       + " group by Vendor,ISNULL([Type],'TBD') "
       + "UNION "
       + "SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
       + "sum(Attempts) as Attempts, "
       + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
       + "mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
       + "mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
       + "mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
       + "from [mecca2].[dbo].OpSheetY "
       + " where LMC = 'TELCEL' AND COUNTRY = 'MEXICO' "
       + "order by  TotalMinutes DESC ";


        ds = Util.RunQueryByStmnt(sql);
        gdvYesterday.DataSource = ds.Tables[0];
        gdvYesterday.DataBind();
    }
}
