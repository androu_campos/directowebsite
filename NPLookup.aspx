<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="NPLookup.aspx.cs" Inherits="NPLookup" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table>
        <tr>
            <td vAlign=top align=left colspan="2">
                <asp:Label id="Label3" runat="server" Text="Number Portability Lookup" CssClass="labelTurk"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>            
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label id="Label4" runat="server" Text="Another report is running, please try again later." CssClass="labelTurk" Visible=false></asp:Label>
            </td>            
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>            
        </tr>
        <tr>
            <td style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
                <asp:Label id="Label5" runat="server" Text="Customer" CssClass="labelBlue8"></asp:Label>
            </td>
            <td>
                <asp:DropDownList id="dpdCustomer" runat="server" CssClass="dropdown"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
                <asp:Label id="Label1" runat="server" Text="Vendor" CssClass="labelBlue8"></asp:Label>
            </td>
            <td>
                <asp:DropDownList id="dpdVendor" runat="server" CssClass="dropdown"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="WIDTH: 100px" align="center" bgcolor="#d5e3f0">
                <asp:Label id="Label2" runat="server" Text="Report Type" CssClass="labelBlue8"></asp:Label>
            </td>
            <td>
                <asp:DropDownList id="dpdTrafficSource" runat="server" CssClass="dropdown"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="WIDTH: 100px">
                <br />
            </td>
            <td style="WIDTH: 100px">
                <br />
            </td>
        </tr>
        <tr>
            <td style="WIDTH: 100px">
                <asp:Button id="cmdSearch" onclick="cmdSearch_Click" runat="server" Text="Submit report" CssClass="boton"></asp:Button>
            </td>
            <td style="WIDTH: 100px">
               
            </td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td>
                <asp:GridView ID="gvPendingReport" runat="server" Visible=false Font-Names="Arial" Font-Size="8pt" Font-Underline="False" Width="100%">
                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                    <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                    <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                    <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial"
                        Font-Bold="True" ForeColor="Orange"></HeaderStyle>
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

