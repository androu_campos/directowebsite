using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class AlarmTrafficManager : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DirectoUser"].ToString() == "yurema.villa" || Session["DirectoUser"].ToString() == "maria.merlos" || Session["DirectoUser"].ToString() == "angel.hernandez" || Session["DirectoUser"].ToString() == "rodrigo.andrade")
        {
            if (Page.IsPostBack)
            {
            }
            else
            {
                SqlConnection SqlConn = new SqlConnection();
                SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
                SqlCommand SqlCommand = new SqlCommand("select distinct Country from Regions order by Country ASC", SqlConn);
                SqlConn.Open();
                SqlDataReader myReader = SqlCommand.ExecuteReader();
                while (myReader.Read())
                {
                    dpdCountry.Items.Add(myReader.GetValue(0).ToString());
                }
                myReader.Close();
                SqlConn.Close();
            }
        }
        else
        {
            Response.Redirect("Home.aspx", false);
        }
    }

    protected void cmdAddAlarm_Click(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand cmd = new SqlCommand("sp_addTrafficAlarm", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        conn.Open();
        cmd.Parameters.Add(new SqlParameter("@Name",textName.Text));
        cmd.Parameters.Add(new SqlParameter("@Email", textEmail.Text));
        cmd.Parameters.Add(new SqlParameter("@Country", dpdCountry.SelectedValue));
        cmd.Parameters.Add(new SqlParameter("@CallsMin", textCallsMin.Text));
        cmd.Parameters.Add(new SqlParameter("@CallsMax", textCallsMax.Text));
        cmd.Parameters.Add(new SqlParameter("@ASRMin", textASRMin.Text));
        cmd.Parameters.Add(new SqlParameter("@ASRmax", textASRMax.Text));
        cmd.Parameters.Add(new SqlParameter("@ABRMin", textABRMin.Text));
        cmd.Parameters.Add(new SqlParameter("@ABRMax", textABRMax.Text));
        cmd.Parameters.Add(new SqlParameter("@MinutesMin", textMinutesMin.Text));
        cmd.Parameters.Add(new SqlParameter("@MinutesMax", textMinutesMax.Text));

        cmd.ExecuteNonQuery();

        conn.Close();

        cmd.CommandText = "SELECT * FROM [TrafficAlarms]";
        cmd.CommandType = CommandType.Text;
        
        
        DataSet ResultSet;
        ResultSet = RunQuery(cmd);

        
        Sistema.DataBind();



    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
