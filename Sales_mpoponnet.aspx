<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Sales_mpoponnet.aspx.cs" Inherits="Sales_mpoponnet" Title="DIRECTO - Connections Worldwide" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Label ID="Label5" runat="server" CssClass="labelTurkH" Text="MECCA ONNET MINIPOP SALES LOGS" Font-Bold="true"></asp:Label>

    <table align="left" width="100%" style="position:absolute; top:220px; left:230px;">
        
        
        <tr>
            <td align="left" style="width: 149px; height: 32px" valign="top">
            
            <table id="Set">
            
            <tr> <td width="10%"><asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Choose Customer:"></asp:Label></td> <td><asp:DropDownList ID="dpdCustomer" runat="server" Width="158px" CssClass="dropdown">
                </asp:DropDownList></td> <td></td> </tr>
                
            <tr> <td width="10%"><asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Choose Vendor:"></asp:Label></td> <td><asp:DropDownList ID="dpdVendor" runat="server" Width="158px" CssClass="dropdown">
                </asp:DropDownList></td> <td></td> </tr>
                
            <tr> <td width="10%"><asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="From:"></asp:Label></td> <td><asp:TextBox ID="txtFrom" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td> <td><asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td> </tr>
            <tr> <td width="10%"><asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="To:"></asp:Label></td> <td><asp:TextBox ID="txtTo" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td> <td><asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td> </tr>
            <tr> <td width="10%"></td> <td align="left"><asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="View report" CssClass="boton" /></td> <td></td> </tr>
            
            
            </table>
            
            </td>
            <td style="width: 100px; height: 32px">
            </td>
            <td style="width: 100px; height: 32px">
            </td>
        </tr>
        <tr>
            <td style="width: 149px" align="left">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 149px" align="left">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 149px" align="left">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px" align="left">
            </td>
        </tr>
        <tr>
            <td style="width: 149px" align="left">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px" align="left">
            </td>
        </tr>
        <tr>
            <td style="width: 149px">
            </td>
            <td align="left" style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 149px; height: 30px;">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td align="left" style="width: 100px; height: 30px;">
            </td>
            <td style="width: 100px; height: 30px;">
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label6" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
  
   
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom" PopupButtonID="Image1">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo" PopupButtonID="Image2">
                </cc1:CalendarExtender>
</asp:Content>

