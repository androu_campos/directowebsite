<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ShowCDRs.aspx.cs"
    Inherits="ShowCDRs" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label7" runat="server" Text="CDR's Viewer" CssClass="labelTurkH"></asp:Label>
    <br />
    <br />
    <table>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Choose Customer:"
                    Width="122px"></asp:Label>
            </td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdCustCode" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
            <td style="width: 100px" align="left" valign="top" rowspan="5">
                <asp:Label ID="Label12" runat="server" CssClass="labelBlue8" Text="Reason:" Width="122px"></asp:Label>
                <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" MaxLength="144" 
                    Rows="4" style="resize:none;" Width="250px" ></asp:TextBox>
                <asp:Label ID ="lbl14" runat="server" CssClass="lblNotFound" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Choose Vendor:"
                    Width="113px"></asp:Label>
            </td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdVendId" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Choose Country: "></asp:Label>
            </td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdCountry" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Text="Choose LMC: "></asp:Label>
            </td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdLMC" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label9" runat="server" CssClass="labelBlue8" Text="Choose Type: "></asp:Label>
            </td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdType" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label10" runat="server" CssClass="labelBlue8" Text="Choose Cust SIP Cause: "></asp:Label>
            </td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdCSIPCause" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
        </tr>
                <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label13" runat="server" CssClass="labelBlue8" Text="Choose Vend SIP Cause: "></asp:Label>
            </td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdVSIPCause" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Search Number A: "></asp:Label>
            </td>
            <td style="width: 100px" align="left">
                <asp:TextBox ID="txtNumbera" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Search Number B: "></asp:Label>
            </td>
            <td style="width: 100px" align="left">
                <asp:TextBox ID="txtNumberb" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Choose Day:"></asp:Label>
            </td>
            <td style="width: 100px" align="left" valign="top">
                <asp:DropDownList ID="dpdDay" runat="server" CssClass="dropdown">
                    <asp:ListItem Selected="true" Text="Today" Value="CDRS"></asp:ListItem>
                    <asp:ListItem Text="Yesterday" Value="CDRSB"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label11" runat="server" CssClass="labelBlue8" Text="CDRS Count: "></asp:Label>
            </td>
            <td style="width: 100px" align="left">
                <asp:TextBox ID="txtCount" runat="server" Enabled="false" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label14" runat="server" CssClass="labelBlue8" Text="Final LMC: "></asp:Label>
            </td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="ddlFinalLMC" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="Button1_Click"
                    Text="Show" />
            </td>
            <td>
                <asp:Button ID="Button2" runat="server" CssClass="boton" OnClick="Button2_Click"
                    Text="Export" Visible="false" />
            </td>
        </tr>
        <tr>
            <td style="height: 20px;" colspan="2">
                <asp:Label ID="lblError" runat="server" Text="" CssClass="lblNotFound"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:GridView ID="gdvCdrs" runat="server" Font-Names="Arial" Font-Size="8pt" AllowPaging="true"
        OnPageIndexChanging="gdvCdrs_PageIndexChanging" PageSize="25">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
            Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>
