using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class IP : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cdrdbDatasetTableAdapters.ProviderIPTableAdapter adp = new cdrdbDatasetTableAdapters.ProviderIPTableAdapter();
            cdrdbDataset.ProviderIPDataTable tbl = adp.GetProviderName();
            int i = tbl.Rows.Count;

            for (int j = 0; j < i; j++)
            {
                dpdProvider.Items.Add(tbl.Rows[j]["ProviderName"].ToString());

            }
        }


    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string provider = dpdProvider.SelectedValue.ToString();
        cdrdbDatasetTableAdapters.QueriesTableAdapter adp = new cdrdbDatasetTableAdapters.QueriesTableAdapter();


        if (provider == "** ALL **")
        {
            gdvProviders.DataSourceID = "SqlDataSource1";
            gdvProviders.DataBind();
    
        }
        else
        {
            gdvProviders.DataSourceID = "SqlDataSource2";
            gdvProviders.DataBind();
        
        }

    }
}
