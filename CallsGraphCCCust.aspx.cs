using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;


public partial class PortsGraphCCCust : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable tbl;

        

        string username = string.Empty;
        

        username = Session["DirectoUser"].ToString();


        if (username == "santander")//|| username == "bento" || username == "contacto" || username == "eci" || username == "ergon" || username == "megacal" || username == "mkt911" || username == "sto" || username == "targetone" || username == "vcip")
        {
            Response.Redirect("CallsGraphCCCustS.aspx");
        }
        else if (username == "bento" || username == "contacto" || username == "eci" || username == "ergon"
            || username == "megacal" || username == "mkt911" || username == "sto" || username == "targetone"
            || username == "vcip" || username == "santanadmoncall" || username == "santanderdomer" 
            || username == "domercent" || username == "cc-med")
        {
            SqlConnection SqlConn1 = new SqlConnection();
            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
            SqlCommand SqlCommand1 = new SqlCommand();

            if (!Page.IsPostBack)
            {
                txtFrom.Text = DateTime.Now.AddDays(-1).ToShortDateString();
                txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();

                if (username == "vcip")
                {
                    SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE (PROVIDERNAME LIKE 'CC-" + username + "SAN%' or PROVIDERNAME LIKE 'CC-" + username + "-SAN%') AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
                }
                else if (username == "santanadmoncall" || username == "santanderdomer" || username == "domercent")
                {
                    SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-" + username + "%' AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
                }
                else if (username == "cc-med")
                {
                    SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-MED-%' AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
                }
                else
                {
                    SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-" + username + "SAN%' AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
                }

                SqlCommand1.Connection = SqlConn1;
                SqlConn1.Open();
                SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
                while (myReader1.Read())
                {
                    dpdProvider.Items.Add(myReader1.GetValue(0).ToString());
                }
                myReader1.Close();
                SqlConn1.Close();
            }
            if (Request.QueryString.Count > 0)
            {
                string provider = Request.QueryString["P"].ToString();
                string query = string.Empty;
                string title = string.Empty;

                string from = Request.QueryString["dF"].ToString();
                string to = Request.QueryString["dT"].ToString();
                from = from.Replace("_", "/");
                to = to.Replace("_", "/");


                if (Request.QueryString["T"].ToString() == "0") //Customer
                {
                    if (provider == "**ALL**")
                    {
                        if (username == "vcip")
                        {
                            query = "SELECT sum([Calls]) as Live_Calls,[Time] FROM [MECCA2].[dbo].[TrafficLogLive] WHERE (Customer like 'CC-" + username + "SAN%' or Customer like 'CC-" + username + "-SAN%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                        }
                        else if (username == "santanadmoncall" || username == "santanderdomer" || username == "domercent")
                        {
                            query = "SELECT sum([Calls]) as Live_Calls,[Time] FROM [MECCA2].[dbo].[TrafficLogLive] WHERE Customer like 'CC-" + username + "%' and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                        }
                        else if (username == "cc-med")
                        {
                            query = "SELECT sum([Calls]) as Live_Calls,[Time] FROM [MECCA2].[dbo].[TrafficLogLive] WHERE Customer like 'CC-MED-%' and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                        }
                        else
                        {
                            query = "SELECT sum([Calls]) as Live_Calls,[Time] FROM [MECCA2].[dbo].[TrafficLogLive] WHERE Customer like 'CC-" + username + "SAN%' and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                        }
                        title = "Customer: " + username.ToUpper();
                    }
                    else
                    {
                        query = "SELECT sum([Calls]) as Live_Calls,[Time] FROM [MECCA2].[dbo].[TrafficLogLive] WHERE (Customer like 'CC-" + username + "SAN%' or Customer like 'CC-" + username + "-SAN%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                        title = "Customer: " + provider;
                    }

                }

                tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                else cmdDetail.Visible = true;

                makeChart(tbl, title, from, to);
                //gdvDetail.Visible = false;
                lblGrid.Visible = false;

                txtFrom.Text = from;
                txtTo.Text = to;
                dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
                dpdType.SelectedIndex = Convert.ToInt32(Request.QueryString["T"]);
                cmdDetail.Visible = true;

            }
        }
        else
        {
            if (!Page.IsPostBack)
            {
                txtFrom.Text = DateTime.Now.AddDays(-1).ToShortDateString();
                txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();
                SqlConnection SqlConn1 = new SqlConnection();
                SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";

                SqlCommand SqlCommand1;

                if (username.ToUpper() == "CC-BROXEL")
                {
                    SqlCommand1 = new SqlCommand("SELECT ProviderName from CDRDB.dbo.ProviderIP where Providername in ('CC-BROXEL','CC-BROXEL2','CC-BROXEL3')", SqlConn1);
                }
                else
                {
                    SqlCommand1 = new SqlCommand("SELECT '" + username.ToUpper() + "' as ProviderName", SqlConn1);
                }
                SqlConn1.Open();
                SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
                while (myReader1.Read())
                {
                    dpdProvider.Items.Add(myReader1.GetValue(0).ToString());
                }
                myReader1.Close();
                SqlConn1.Close();
            }

            if (Request.QueryString.Count > 0)
            {
                string provider = Request.QueryString["P"].ToString();
                string query = string.Empty;
                string title = string.Empty;

                string from = Request.QueryString["dF"].ToString();
                string to = Request.QueryString["dT"].ToString();
                from = from.Replace("_", "/");
                to = to.Replace("_", "/");

                if (provider == "**ALL**") provider = "%";




                if (Request.QueryString["T"].ToString() == "0") //Customer
                {

                    query = "SELECT sum([Calls]) as Live_Calls,[Time] FROM [MECCA2].[dbo].[TrafficLogLive] WHERE Customer like '" + provider + "' and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                    title = "Customer: " + provider;


                }

                //tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                tbl = RunQuery(new SqlCommand(query)).Tables[0];

                if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                else cmdDetail.Visible = true;

                makeChart(tbl, title, from, to);
                //gdvDetail.Visible = false;
                lblGrid.Visible = false;

                txtFrom.Text = from;
                txtTo.Text = to;
                dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
                dpdType.SelectedIndex = Convert.ToInt32(Request.QueryString["T"]);
                cmdDetail.Visible = true;

            }
        }
    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {
        string username = string.Empty;

        username = Session["DirectoUser"].ToString();

        string from = txtFrom.Text;
        string to = txtTo.Text;

        from = from.Replace("/", "_");
        to = to.Replace("/", "_");

        if (username != "santander")
        {
            if (dpdType.SelectedIndex == 0)//Customer
            {
                if (dpdProvider.SelectedValue.ToString() == "A&D-WORLDWIDE")
                {
                    string c = dpdProvider.SelectedValue.ToString();
                    c = c.Replace("&", "_");
                    Response.Redirect("CallsGraphCCCust.aspx?P=" + c + "&T=0&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
                }
                else
                {
                    Response.Redirect("CallsGraphCCCust.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=0&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
                }
            }
            else                             //Vendor
                Response.Redirect("CallsGraphCCCust.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=1&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
        }
        else
        {
            string a = dpdProvider.SelectedValue.ToString();
            string c = dpdType.SelectedValue.ToString();

            if (dpdType.SelectedValue.ToString() != "**ALL**")
            {
                Response.Redirect("CallsGraphCCCust.aspx?P=0&T=" + a + "&S=" + dpdType.SelectedValue.ToString() + "&dF=" + from + "&dT=" + to);
            }
            else
            {
                Response.Redirect("CallsGraphCCCust.aspx?P=0&T=" + a + "&S=" + dpdType.SelectedValue.ToString() + "&dF=" + from + "&dT=" + to);
            }
        }


    }

    private void makeChart(DataTable tbl, string tITLE, string from, string to)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Calls"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Calls"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Calls"].Points.FindMaxValue("Y");
            DataPoint maxpoint = this.Chart1.Series["Calls"].Points.FindMaxValue();

            string origen = string.Empty;
            string to2 = string.Empty;
            //set the Title

            this.Chart1.Titles.Add(tITLE);
            //draw the MaxLine            

            origen = from + " 00:00:00 AM";
            to2 = to + " 23:59:59 PM";

            this.Chart1.Series["MaxCalls"].Points.AddXY(Convert.ToDateTime(to2), Convert.ToDouble(maxpointY.YValues[0]));
            this.Chart1.Series["MaxCalls"].Points.AddXY(Convert.ToDateTime(origen), Convert.ToDouble(maxpointY.YValues[0]));


            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetail"] = tbl;

            this.Chart1.Visible = true;
            lblWarning.Visible = false;
            this.cmdDetail.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            lblWarning.Visible = true;
            Session["tblDetail"] = tbl;
            this.cmdDetail.Visible = false;

        }

    }
    protected void cmdDetail_Click(object sender, EventArgs e)
    {
        if (((DataTable)Session["tblDetail"]).Rows.Count > 0)
        {

            gdvDetail.DataSource = ((DataTable)Session["tblDetail"]);
            gdvDetail.DataBind();

            gdvDetail.Visible = true;
            lblGrid.Visible = true;
            lblGrid.Text = dpdProvider.SelectedValue.ToString();
            cmdHide.Visible = true;
            cmdDetail.Visible = false;
        }
        else
        {
        }
    }
    protected void cmdHide_Click(object sender, EventArgs e)
    {
        gdvDetail.Visible = false;
        lblGrid.Visible = false;
        cmdDetail.Visible = true;
        cmdHide.Visible = false;

    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
