<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="MonitorHour.aspx.cs"
    Inherits="MonitorHour" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <br />
    <%--<asp:UpdatePanel id="UpdatePanel1" runat="server" >
        <contenttemplate>--%>
    <table align="left">
        <tbody>
            <tr>
                <td valign="top" align="left">
                    <asp:Label ID="Label1" runat="server" Text="Advanced Query Monitor by Hour" CssClass="labelTurk"></asp:Label></td>
            </tr>
            <tr>
                <td valign="top" align="left">
                    &nbsp;<table id="tblQuery">
                        <tbody>
                            <tr>
                                <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                    <asp:Label ID="Label5" runat="server" Text="Customer" CssClass="labelBlue8"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="dpdCustomer" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                    <asp:Label ID="Label6" runat="server" Text="Vendor:" CssClass="labelBlue8"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="dpdVendor" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                    <asp:Label ID="Label7" runat="server" Text="OriginationIP" CssClass="labelBlue8"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="dpdOriginationIp" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                    <asp:Label ID="Label8" runat="server" Text="TerminationIP" CssClass="labelBlue8"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="dpdTermination" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                    <asp:Label ID="Label9" runat="server" Text="Country" CssClass="labelBlue8"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="dpdCountry" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                    <asp:Label ID="Label10" runat="server" Text="Region" CssClass="labelBlue8"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="dpdRegion" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                    <asp:Label ID="Label11" runat="server" Text="Type" CssClass="labelBlue8"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="dpdType" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                    <asp:Label ID="Label12" runat="server" Text="Class" CssClass="labelBlue8"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="dpdClass" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                                    <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="LMC"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="dpdLMC" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                                    <asp:Label ID="Label13" runat="server" Text="Source MSW" CssClass="labelBlue8"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="dpdSource" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#d5e3f0" style="width: 100px; height: 22px;">
                                    <asp:Label ID="Label14" runat="server" CssClass="labelBlue8" Text="Gateway"></asp:Label></td>
                                <td style="width: 100px; height: 22px;">
                                    <asp:DropDownList ID="dpdGateway" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px; height: 22px;">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#d5e3f0" style="width: 100px; height: 22px;">
                                    <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="CustSIPCause"></asp:Label></td>
                                <td style="width: 100px; height: 22px;">
                                    <asp:DropDownList ID="dpdCustSIP" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px; height: 22px;">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#d5e3f0" style="width: 100px; height: 22px;">
                                    <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="VendSIPCause"></asp:Label></td>
                                <td style="width: 100px; height: 22px;">
                                    <asp:DropDownList ID="dpdVendSIP" runat="server" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px; height: 22px;">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#d5e3f0" style="width: 100px; height: 22px;">
                                    <asp:Label ID="lbldiscardCC" runat="server" CssClass="labelBlue8" Text="Remove CC Data"></asp:Label>
                                </td>
                                <td style="width: 100px; height: 22px;">
                                    <asp:CheckBox ID="CheckBox1" runat="server" Width="138px" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px">
                                    <asp:Button ID="cmdSearch" OnClick="cmdSearch_Click" runat="server" Text="View report"
                                        CssClass="boton"></asp:Button></td>
                                <td style="width: 100px">
                                </td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <%--</contenttemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
