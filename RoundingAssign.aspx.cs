using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class RoundingAssign : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {

            cdrdbDatasetTableAdapters.ProvidersIPAdp adp = new cdrdbDatasetTableAdapters.ProvidersIPAdp();
            cdrdbDataset.ProvidersIPDataTable tbl = adp.GetData("C");

            int count = tbl.Rows.Count;
            DropDownList1.Items.Clear();
            for (int i = 0; i < count; i++)
            {
                DropDownList1.Items.Add(tbl.Rows[i]["ProviderName"].ToString());

            }
            /**/
            AccessDatasetTableAdapters.CustRoundingAdp adpC = new AccessDatasetTableAdapters.CustRoundingAdp();
            gdvRounding.DataSource = adpC.GetDatabyCustomer("ACCESSCOM");
            gdvRounding.DataBind();


        }


    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {

        cdrdbDatasetTableAdapters.ProvidersIPAdp adp = new cdrdbDatasetTableAdapters.ProvidersIPAdp();
        cdrdbDataset.ProvidersIPDataTable tbl = adp.GetData(RadioButtonList1.SelectedValue.ToString());

        int count = tbl.Rows.Count;
        DropDownList1.Items.Clear();
        for (int i = 0; i < count; i++)
        {
            DropDownList1.Items.Add(tbl.Rows[i]["ProviderName"].ToString());

        }


    }

    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString =
            ConfigurationManager.ConnectionStrings
            ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
            
        }
        return resultsDataSet;
    }


    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
       

        if (RadioButtonList1.SelectedValue.ToString() == "C")
        {
            AccessDatasetTableAdapters.CustRoundingAdp adpC = new AccessDatasetTableAdapters.CustRoundingAdp();            
            gdvRounding.DataSource = adpC.GetDatabyCustomer(DropDownList1.SelectedValue.ToString());
            gdvRounding.DataBind();
        }
        else
        {
            AccessDatasetTableAdapters.VendorRoundingAdp adpV = new AccessDatasetTableAdapters.VendorRoundingAdp();
            gdvRounding.DataSource = adpV.GetDatabyVendorName(DropDownList1.SelectedValue.ToString());
            gdvRounding.DataBind();                
        }
 

    }
}
