using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RKLib.ExportData;
using System.Data.SqlClient;

public partial class lcr : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
                
        if(!IsPostBack)
        {
            SqlConnection SqlConn1;
            SqlCommand SqlCommand1;

            SqlConn1 = new SqlConnection();
            SqlCommand1 = new SqlCommand();

            DataTable tbl;

            //MyDataSetTableAdaptersTableAdapters.LCRVolumesTableAdapter adp = new MyDataSetTableAdaptersTableAdapters.LCRVolumesTableAdapter();
            //MyDataSetTableAdapters.LCRVolumesDataTable tbl = adp.GetData();

            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";

            SqlCommand1 = new SqlCommand("SELECT Country, SUM(MINUTES) Minutes, SUM(PROFIT) Profit, ROUND(AVG(ASR),2) ASR, round(AVG(ACD),2) ACD FROM [MECCA2].[dbo].[LCRVolumes] GROUP BY Country Order by Country", SqlConn1);

            SqlConn1.Open();
            SqlDataReader myReader1 = SqlCommand1.ExecuteReader();

            gdvLcr.DataSource = myReader1;
            gdvLcr.DataBind();
        }
                        
    }


    protected void download_Click(object sender, EventArgs e)
    {
        MyDataSetTableAdaptersTableAdapters.LCRVolumesAllTableAdapter adp = new MyDataSetTableAdaptersTableAdapters.LCRVolumesAllTableAdapter();
        MyDataSetTableAdapters.LCRVolumesAllDataTable tbl = adp.GetData();

        DataTable dt = adp.GetData();

        // Export all the details
        DataTable dtEmployee = dt;
        // Export all the details to CSV
        RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
        string filename = "LCR Explorer" + ".xls";
        objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
    }
}
