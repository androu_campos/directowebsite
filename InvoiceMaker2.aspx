<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="InvoiceMaker2.aspx.cs" Inherits="InvoiceMaker2" Title="Untitled Page" StylesheetTheme="Theme1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                <asp:Label ID="Label1" runat="server" Text="Customer Name" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCustomer" runat="server" DataSourceID="SqlDataSource1"
                    DataTextField="ProviderName" DataValueField="ProviderName" CssClass="dropdown" Width="165px">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Invoice Number"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtInvoiceNumber" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                <asp:Label ID="Label2" runat="server" Text="Address" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtAddress" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 100px">
            </td>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Text="Due Date"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtDuedate" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                <asp:Label ID="Label3" runat="server" Text="City,Zip,State,Country" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtCityZip" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 100px">
            </td>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label9" runat="server" CssClass="labelBlue8" Text="Adjustment"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtAdjustment" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px; height: 89px;" align="center" bgcolor="#d5e3f0">
                <asp:Label ID="Label4" runat="server" Text="Phone Number" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px; height: 89px;">
                <asp:TextBox ID="txtPhone" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 100px; height: 89px;">
            </td>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px; height: 89px;">
                <asp:Label ID="Label10" runat="server" CssClass="labelBlue8" Text="Previous Balance"></asp:Label></td>
            <td style="width: 100px; height: 89px;">
                <asp:TextBox ID="txtPreviousBalance" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                <asp:Label ID="Label5" runat="server" Text="From" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtFrom" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 100px">
                <asp:Image ID="imgFrom" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label11" runat="server" CssClass="labelBlue8" Text="Previous Payment"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtPreviousPayment" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px; height: 40px;" align="center" bgcolor="#d5e3f0">
                <asp:Label ID="Label6" runat="server" Text="To" CssClass="labelBlue8"></asp:Label></td>
            <td style="width: 100px; height: 40px;">
                <asp:TextBox ID="txtTo" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 100px; height: 40px;">
                <asp:Image ID="imgTo" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px; height: 40px">
                <asp:Label ID="Label12" runat="server" CssClass="labelBlue8" Text="Late Fees"></asp:Label></td>
            <td style="width: 100px; height: 40px">
                <asp:TextBox ID="txtLateFees" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px" align="center" height="30">
                <asp:Button ID="cmdInvoice" runat="server" CssClass="boton" Text="Create Invoice" OnClick="cmdInvoice_Click" /></td>
            <td style="width: 100px" height="30">
            </td>
            <td style="width: 100px" height="30">
            </td>
            <td height="30" style="width: 100px">
            </td>
            <td height="30" style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    SelectCommand="SELECT DISTINCT ProviderName FROM ProviderIP UNION SELECT '**ALL**' AS ProviderName FROM ProviderIP UNION SELECT ' **NONE** ' AS ProviderName FROM ProviderIP" OnSelecting="SqlDataSource1_Selecting">
                </asp:SqlDataSource>
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <cc1:calendarextender id="CalendarExtender1" runat="server" popupbuttonid="imgFrom"
                    targetcontrolid="txtFrom"></cc1:calendarextender>
                <cc1:calendarextender id="CalendarExtender2" runat="server" popupbuttonid="imgTo"
                    targetcontrolid="txtTo"></cc1:calendarextender>
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
</asp:Content>

