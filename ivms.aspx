<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ivms.aspx.cs" Inherits="ivms" Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:Label ID="LabelTitle" runat="server" Font-Bold="True" Font-Size="Medium" Text="!VMS" CssClass="labelTurkH"></asp:Label>


    <table width="100%" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td style="width: 100px; height: 4px" valign="top" align="left">
            </td>
            <td style="height: 4px" valign="top" width="222">
            </td>
            <td align="left" style="width: 200px; height: 4px" valign="top">
            </td>
            <td style="width: 222px; height: 4px" valign="top">
            </td>
            <td style="width: 200px; height: 4px" valign="top">
            </td>
            <td style="width: 200px; height: 4px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 11px;" valign="top">
            </td>
            <td style="height: 11px" valign="top" width="222">
            </td>
            <td align="left" style="width: 200px; height: 11px" valign="top">
            </td>
            <td style="width: 222px; height: 11px" valign="top">
            </td>
            <td style="width: 200px; height: 11px" valign="top">
            </td>
            <td style="width: 200px; height: 11px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 4px" valign="top" align="right">
                <asp:Label ID="LabelReport" runat="server" Font-Bold="True" Text="Report:" CssClass="labelBlue8"></asp:Label></td>
            <td align="left" style="height: 4px" valign="top" width="222">
                <asp:DropDownList ID="ddlReport" runat="server" AutoPostBack="True" Font-Names="Arial"
                    ForeColor="#333333" Width="65%" CssClass="dropdown">
                    <asp:ListItem Value="0">By Time</asp:ListItem>
                    <asp:ListItem Value="2">By Reg ID</asp:ListItem>
                    <asp:ListItem Value="1">By Dest ID</asp:ListItem>
                    <asp:ListItem Value="3">By Origination IP</asp:ListItem>
                    <asp:ListItem Value="4">By Termination IP</asp:ListItem>
                    <asp:ListItem Value="5" Selected="True">** NONE **</asp:ListItem>
                </asp:DropDownList></td>
            <td align="right" style="width: 200px; height: 4px" valign="top">
                <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="Source:" CssClass="labelTurk"></asp:Label></td>
            <td align="left" style="width: 222px; height: 4px" valign="top">
                <asp:DropDownList ID="dpdNextone" runat="server" Font-Names="Arial" Font-Size="8pt"
                    ForeColor="#333333" Width="71px">
                    <asp:ListItem>LA01</asp:ListItem>
                    <asp:ListItem>LA02</asp:ListItem>
                    <asp:ListItem>MI01</asp:ListItem>
                    <asp:ListItem>NY01</asp:ListItem>
                    <asp:ListItem>NY02</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 200px; height: 4px" valign="top" align="right">
                <asp:Label ID="LabelInter" runat="server" Enabled="False" Font-Bold="True" Text="Interval:" CssClass="labelTurk"></asp:Label></td>
            <td align="left" style="width: 200px; height: 4px" valign="top">
                <asp:DropDownList ID="DropDownList1" runat="server" Enabled="False" Font-Names="Arial"
                    Font-Size="8pt" ForeColor="#333333">
                    <asp:ListItem Value="30">30 min</asp:ListItem>
                    <asp:ListItem Selected="True" Value="60">1 hour</asp:ListItem>
                    <asp:ListItem Value="120">2 Hour</asp:ListItem>
                    <asp:ListItem Value="180">3 Hour</asp:ListItem>
                    <asp:ListItem Value="240">4 hour</asp:ListItem>
                    <asp:ListItem Value="1440">By Day</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width: 100px; height: 6px" valign="top">
            </td>
            <td height="20" valign="top" width="222">
            </td>
            <td align="left" style="width: 200px; height: 6px" valign="top">
            </td>
            <td style="width: 222px; height: 6px" valign="top">
            </td>
            <td style="width: 200px; height: 6px" valign="top">
            </td>
            <td style="width: 200px; height: 6px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" valign="top" align="right">
                <asp:Label ID="LabelF" runat="server" Font-Bold="True" Text="Begin Date:" Width="80px" CssClass="labelBlue8"></asp:Label></td>
            <td align="left" valign="top" width="222">
                <asp:TextBox ID="txtFrom" runat="server" Font-Names="Arial" Width="101px" CssClass="labelSteelBlue"></asp:TextBox>
                <asp:Image
                    ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td style="width: 200px" valign="top" align="right">
                <asp:Label ID="LabelT" runat="server" Font-Bold="True" Text="End Date:" Width="64px" CssClass="labelTurk"></asp:Label></td>
            <td align="left" style="width: 222px" valign="top">
                <asp:TextBox ID="txtTo" runat="server" Font-Names="Arial" Width="101px" CssClass="labelSteelBlue"></asp:TextBox>
                <asp:Image
                    ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td style="width: 200px" valign="top">
            </td>
            <td style="width: 200px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 23px" valign="top">
            </td>
            <td height="20" valign="top" width="222">
            </td>
            <td style="width: 200px; height: 23px" valign="top">
            </td>
            <td style="width: 222px; height: 23px" valign="top">
            </td>
            <td style="width: 200px; height: 23px" valign="top">
            </td>
            <td style="width: 200px; height: 23px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" valign="top" align="right">
                <asp:Label ID="LabelOri" runat="server" Font-Bold="True" Text="Orig Reg ID:" Width="80px" CssClass="labelBlue8"></asp:Label></td>
            <td align="left" valign="top" width="222">
                <asp:TextBox ID="txtOri" runat="server" Font-Names="Arial" Width="124px" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td style="width: 200px" valign="top" align="right">
                <asp:Label ID="LabelDest" runat="server" Font-Bold="True" Text="Dest Reg ID:" Width="80px" CssClass="labelTurk"></asp:Label></td>
            <td align="left" style="width: 222px" valign="top">
                <asp:TextBox ID="txtDest" runat="server" Font-Names="Arial" Width="124px" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td style="width: 200px" valign="top" align="right">
                <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="Prefix:" CssClass="labelTurk"></asp:Label></td>
            <td align="left" style="width: 200px" valign="top">
                <asp:TextBox ID="txtPrefix" runat="server" Width="100px" CssClass="labelSteelBlue"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px; height: 39px" valign="top">
            </td>
            <td align="left" style="height: 39px" valign="top" width="222">
            </td>
            <td style="width: 200px; height: 39px" valign="top">
            </td>
            <td align="left" style="width: 222px; height: 39px" valign="top">
            </td>
            <td style="width: 200px; height: 39px" valign="top">
            </td>
            <td align="left" style="width: 200px; height: 39px" valign="top">
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 100px" valign="top">
                <asp:Button ID="Button1" runat="server" BorderStyle="Solid" BorderWidth="1px" CssClass="boton"
                    OnClick="Button1_Click" Text="View report" /></td>
            <td align="left" valign="top" width="222">
            </td>
            <td style="width: 200px" valign="top">
            </td>
            <td align="left" style="width: 222px" valign="top">
            </td>
            <td style="width: 200px" valign="top">
            </td>
            <td align="left" style="width: 200px" valign="top">
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 100px; height: 31px;" valign="top">
            </td>
            <td align="left" valign="top" width="222" style="height: 31px">
            </td>
            <td style="width: 200px; height: 31px;" valign="top">
            </td>
            <td align="left" style="width: 222px; height: 31px;" valign="top">
            </td>
            <td style="width: 200px; height: 31px;" valign="top">
            </td>
            <td align="left" style="width: 200px; height: 31px;" valign="top">
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 100px; height: 144px" valign="top">
            </td>
            <td align="left" colspan="4" style="height: 144px" valign="top" width="222">
            
            <table>
        <tr>
            <td rowspan="2" style="width: 100px" align="left" valign="top">
                <asp:DataGrid ID="dgResum" runat="server" OnSelectedIndexChanged="dgResum_SelectedIndexChanged"
                    Font-Names="Arial" Font-Size="8pt" GridLines="None" Width="250px">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <Columns>
                        <asp:ButtonColumn CommandName="Select" Text="Details"></asp:ButtonColumn>
                    </Columns>
                    <AlternatingItemStyle BackColor="#EFF3FB" />
                </asp:DataGrid></td>
            <td rowspan="2" style="width: 100px" align="left" valign="top">
                <asp:DataGrid ID="dgSemi" runat="server" OnSelectedIndexChanged="dgSemi_SelectedIndexChanged"
                    Font-Names="Arial" Font-Size="8pt" GridLines="None" Width="240px">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <Columns>
                        <asp:ButtonColumn CommandName="Select" Text="Details"></asp:ButtonColumn>
                    </Columns>
                    <AlternatingItemStyle BackColor="#EFF3FB" />
                </asp:DataGrid></td>
            <td align="left" rowspan="2" style="width: 65513px" valign="top">
                &nbsp;</td>
            <td align="left" rowspan="2" style="width: 100px" valign="top">
                <asp:DataGrid ID="dgDetail" runat="server" AllowPaging="True" OnPageIndexChanged="dgDetail_PageIndexChanged"
                    BorderStyle="None" Font-Names="Arial" Font-Size="8pt" GridLines="None" OnSelectedIndexChanged="dgDetail_SelectedIndexChanged"
                    Width="230px">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingItemStyle BackColor="#EFF3FB" />
                </asp:DataGrid></td>
        </tr>
                <tr>
        </tr>
    </table>
            
            
            </td>
            <td align="left" style="width: 200px; height: 144px" valign="top">
            </td>
        </tr>
        
        
                        
        
        <tr>
            <td width="100%" colspan="6">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="6">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="6">
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager></td>
        </tr>
    </table>
   
     
    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom" PopupButtonID="Image1">
    </cc1:CalendarExtender>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo" PopupButtonID="Image2">
    </cc1:CalendarExtender>
    
    <asp:HiddenField ID="hfFrom" runat="server" />
    <asp:HiddenField ID="hfTo" runat="server" />
    <asp:HiddenField ID="hfHour" runat="server" />
    <asp:HiddenField ID="hfIncrement" runat="server" />
    <asp:HiddenField ID="hfDest" runat="server" />
    <asp:HiddenField ID="hfOri" runat="server" />
    <asp:HiddenField ID="hfDetail" runat="server" />
    <asp:HiddenField ID="hfReport" runat="server" />
    <asp:HiddenField ID="hfPrefix" runat="server" />
    
</asp:Content>

