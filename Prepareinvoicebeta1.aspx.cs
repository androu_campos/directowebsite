using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Prepareinvoicebeta1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MyDataSetTableAdaptersTableAdapters.InvoiceTableAdapter adp = new MyDataSetTableAdaptersTableAdapters.InvoiceTableAdapter();
        MyDataSetTableAdapters.InvoiceDataTable tbl = adp.GetOrderByMeccaName();
        int i = tbl.Rows.Count;
        
        for(int j=0;j<i;j++)
        {        
            dpdCustomerN.Items.Add(tbl.Rows[j]["MeccaName"].ToString());            
        }
        

        if(IsPostBack)//selecciona una opcion del dpdCustomerN
        {
            
            MyDataSetTableAdaptersTableAdapters.InvoiceTableAdapter adpI = new MyDataSetTableAdaptersTableAdapters.InvoiceTableAdapter();
            MyDataSetTableAdapters.InvoiceDataTable tblI = adp.GetDataByMeccaName(dpdCustomerN.SelectedValue.ToString());

            txtInvoiceNumber.Text = tblI.Rows[0]["NumInvoice"].ToString();
                                    
        
        }




    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("PrintInvoicebeta1.aspx?Customer=" + dpdCustomerN.SelectedValue.ToString() + "&NumInvoice=" + txtInvoiceNumber.Text + "&To=" + txtCalendar.Text + "&Type=");
    }
}
