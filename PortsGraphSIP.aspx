<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PortsGraphSIP.aspx.cs"
    Inherits="PortsGraph" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">

    function dispDataC(SIPCause, def, atts, graph) {

        if (document.getElementById) {

                document.getElementById('<%=lblMaxPointC.ClientID%>').innerHTML = "C SIPCause: " + SIPCause + " " + def + " , Attempts: " + atts;
            }


        }
        function dispDataV(SIPCause, def, atts, graph) {

            if (document.getElementById) {

                document.getElementById('<%=lblMaxPointV.ClientID%>').innerHTML = "V SIPCause: " + SIPCause + " " + def + " , Attempts: " + atts;
            }


        }


    
    </script>
    <table>
        <tr>
            <td style="width: 904px; height: 12px;" valign="top">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Active Ports Graph by SIP Cause"
                    Width="200px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" height="30" style="border-right: 0px solid; border-top: 0px solid;
                border-left: 0px solid; border-bottom: 0px solid; width: 904px;">
                <table style="height: 29px">
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 61px">
                            <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Customer:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdCustomer" runat="server" CssClass="dropdown" DataTextField="Customer"
                                DataValueField="Customer">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Vendor:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdVendor" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Type:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdType" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="LMC:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdLMC" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="right" style="width: 108px">
                            <asp:Button ID="cmdReport" runat="server" CssClass="boton" OnClick="cmdReport_Click"
                                Text="View Graph" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label9" runat="server" CssClass="labelBlue8" Text="Country:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdCountry" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="70">
                            <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Cust SIP:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdCSIPCause" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Text="Vend SIP:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdVSIPCause" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label10" runat="server" CssClass="labelBlue8" Text="Report Type:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdRtype" runat="server" CssClass="dropdown" >
                                <asp:ListItem Selected="True" Text="Recent" Value="OpSheetINC"></asp:ListItem>
                                <asp:ListItem Text="Todays" Value="OpSheetAll"></asp:ListItem>
                                <asp:ListItem Text="Yesterdays" Value="OpSheetY"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 321px" colspan="4">
                            <asp:Label ID="lblWarning" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                ForeColor="Red" Text="Nothing found!!" Visible="False" Width="300px"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 904px" align="left" valign="top">
                <DCWC:Chart ID="Chart1" runat="server" Width="905px" Height="445px" Palette="Pastel"
                    AutoSize="true">
                    <Legends>
                        <DCWC:Legend Name="Default">
                        </DCWC:Legend>
                    </Legends>
                    <Series>
                        <DCWC:Series Name="C SIP Cause" XValueType="Auto" Color="DarkOrange" BorderColor="NavajoWhite" ChartType="Column">
                        </DCWC:Series>
                        <DCWC:Series Name="V SIP Cause" XValueType="Auto" Color="DarkSlateBlue" BorderColor="NavajoWhite" ChartType="Column">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin PageColor="AliceBlue"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="" BackColor="245, 169, 169, 169" BackGradientType="DiagonalLeft"
                            Name="Default">
                            <AxisX Title="SIPCause" IntervalAutoMode="VariableCount">
                            </AxisX>
                            <AxisY Title="Attemps">
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                        <DCWC:Title Font="Arial, 8.25pt, style=Bold" Color="245, 0, 0, 0" Name="Title1">
                        </DCWC:Title>
                    </Titles>
                </DCWC:Chart>
            </td>
            <td style="width: 100px" align="left" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 904px">
                <asp:Label ID="lblMaxPointC" runat="server" CssClass="labelBlue8" Width="317px"></asp:Label><br />
                <asp:Label ID="lblMaxPointV" runat="server" CssClass="labelBlue8" Width="317px"></asp:Label><br />
                <table style="width: 178px; height: 68px;">
                    <tr>
                        <td align="right" style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 904px" align="left" valign="top">
                <table>
                    <tr>
                        <td style="width: 179px">
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 179px" valign="top">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button ID="cmdDetail" runat="server" CssClass="boton" OnClick="cmdDetail_Click"
                                        Text="View detail" Visible="False" /><asp:Button ID="cmdHide" runat="server" CssClass="boton"
                                            OnClick="cmdHide_Click" Text="Hide detail" Visible="False" /><br />
                                    <asp:Label ID="lblGrid" runat="server" Text="Label" Visible="False" CssClass="labelBlue8"></asp:Label>
                                    <br />
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gdvDetailC" runat="server" Font-Size="8pt" Font-Names="Arial" Visible="False"
                                                    AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="SIPCause" HeaderText="C SIPCause" HeaderStyle-Wrap="false">
                                                            <ItemStyle Wrap="false" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Attempts" HeaderText="Attempts">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Definition" HeaderText="Definition">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="titleOrangegrid"></HeaderStyle>
                                                </asp:GridView>
                                            </td>
                                            <td>
                                                <asp:GridView ID="gdvDetailV" runat="server" Font-Size="8pt" Font-Names="Arial" Visible="False"
                                                    AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="SIPCause" HeaderText="V SIPCause" HeaderStyle-Wrap="false">
                                                            <ItemStyle Wrap="false" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Attempts" HeaderText="Attempts">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Definition" HeaderText="Definition">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="titleOrangegrid"></HeaderStyle>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="cmdDetail" EventName="Click"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="cmdHide" EventName="Click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td align="left" style="width: 100px" valign="top">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 100px">
                                                <asp:Label ID="Label5" runat="server" Text="Loading...Please wait" ForeColor="LimeGreen"
                                                    Width="105px" CssClass="labelBlue8"></asp:Label>
                                            </td>
                                            <td style="width: 100px">
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Loading.gif"></asp:Image>
                                            </td>
                                        </tr>
                                    </table>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 179px">
                            &nbsp;
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <%--<td style="width: 106px; height: 44px;" valign="top">
                <asp:Timer ID="Timer1" runat="server" Interval="300000">
                </asp:Timer>
            </td>--%>
        </tr>
    </table>
    <br />
</asp:Content>
