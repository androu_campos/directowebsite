<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" StylesheetTheme="Theme1"
    CodeFile="lcr.aspx.cs" Inherits="lcr" Title="DIRECTO - Connections Worldwide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label1" runat="server" Text="MECCA LCR EXPLORER BY COUNTRY" Font-Bold="True"
        CssClass="labelTurkH"></asp:Label>
    <table align="left" width="100%" style="position: absolute; top: 220px; left: 230px;">
        <tr>
            <td align="left" valign="top" colspan="2" style="height: 184px">
                <asp:Button ID="download" runat="server" Visible="true" CssClass="boton" Text="CSV Download"
                    OnClick="download_Click" />
                <asp:GridView ID="gdvLcr" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    ForeColor="#333333" GridLines="None">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Country" HeaderText="Country" HtmlEncode="False">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Minutes" HeaderText="Minutes" HtmlEncode="False" DataFormatString="{0:0,0}">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Profit" HeaderText="Profit" HtmlEncode="False" DataFormatString="${0:f2}">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
                        </asp:BoundField>
                       <asp:BoundField DataField="ASR" HeaderText="ASR" HtmlEncode="False">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ACD" HeaderText="ACD" HtmlEncode="False">
                            <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
                        </asp:BoundField>
                        <asp:HyperLinkField DataNavigateUrlFields="Country" DataNavigateUrlFormatString="lcrbycountry.aspx?Mode=R&amp;Country={0}"
                            Text="Yesterday's Traffic">
                            <ControlStyle Font-Size="9pt" Font-Names="Arial" CssClass="labelSteelBlue" Font-Underline="True" />
                        </asp:HyperLinkField>
                    </Columns>
                </asp:GridView>
            </td>
            <td style="height: 184px">
            </td>
            <td style="height: 184px">
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label>
                        </td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
