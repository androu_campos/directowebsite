<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Editing.aspx.cs"
    Inherits="Editing" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label2" runat="server" CssClass="labelTurkH" Text="Edit Rate"></asp:Label>
    <table align="left" style="position: absolute; top: 220px; left: 230px;" width="100%">
        <tr>
            <td align="left" colspan="2">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="CustName" CssClass="labelTurk"></asp:Label></td>
                        <td>
                            <asp:Label ID="Label5" runat="server" Text="Prefix" CssClass="labelTurk"></asp:Label></td>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text="Rate" CssClass="labelTurk"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblName" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblPrefix" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblRate" runat="server" Text="Label" CssClass="labelSteelBlue"></asp:Label></td>
                    </tr>
                </table>
            </td>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label1" runat="server" Text="New rate:" CssClass="labelTurk"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEdit" runat="server" CssClass="labelSteelBlue"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Button ID="btnUpdate" runat="server" CssClass="boton" OnClick="Button1_Click"
                                Text="Update" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td align="left" colspan="2">
                <asp:Label ID="lblUpdated" runat="server" CssClass="labelTurk" Font-Names="Arial"
                    Font-Size="8pt" Text="Rate updated successful" Visible="False" Width="100%"></asp:Label></td>
        </tr>
        
        
        <tr>
        <td colspan="2" height="50px">
        
        </td>        
        </tr>
        
        <tr>
            <td width="100%" colspan="2">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
