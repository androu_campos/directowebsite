<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="HomeW.aspx.cs"
    Inherits="HomeW" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>--%>
<%@ OutputCache Location="None" VaryByParam="None" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <table align="left" id="tblMain" cellpadding="6" style="height: 666PX; width: 100%;
        position: absolute; top: 140px; left: 220px;">
        <tr>
            <td align="center">
                <asp:HyperLink ID="HyperLink9"   runat="server"  Font-Size="Small" CssClass="labelBlue8" NavigateUrl="/Home.aspx">MECCA TOTAL TRAFFIC </asp:HyperLink>                                
            </td>
            <td align="center">
                <asp:HyperLink ID="HyperLink7"  Font-Bold="true" Font-Size="Small" runat="server" CssClass="labelBlue8" NavigateUrl="/HomeW.aspx">MECCA WHOLESALE TRAFFIC</asp:HyperLink>                                
            </td>
            <td align="center">
                <asp:HyperLink ID="HyperLink8" runat="server" Font-Size="Small" CssClass="labelBlue8" NavigateUrl="/HomeR.aspx">MECCA RETAIL TRAFFIC</asp:HyperLink>  
            </td>
        </tr>
        <tr>            
            <td colspan="3">
                <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/lineMenu.gif" />
            </td>  
        </tr>
        <tr>
            <td rowspan="4" style="height: 850px;" valign="top" width="250">
                <img border="0" src="Images/meccasummaryw.gif" height="24">
                <table border="0" width="264" id="tblSummary" cellspacing="0" cellpadding="0">
                    <tr height="18px">
                        <td width="72" height="20">
                        </td>
                        <td width="71" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">&nbsp; &nbsp;&nbsp; Latest</font></b>
                        </td>
                        <td width="113" colspan="2" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">&nbsp; &nbsp; &nbsp;&nbsp; Change</font></b></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72" align="left">
                            <font color="#004D88" style="font-size: 8pt; margin-left: 10px; margin-top: 5px;
                                margin-bottom: 5px; color: #004d88; font-family: Arial">Minutes</font>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp;
                                <asp:Label ID="lblMinutesY" runat="server"></asp:Label>
                            </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblMinutesC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;</td>
                        <td width="19" align="center">
                            <asp:Image ID="Image4" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">Calls</font>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp; &nbsp;
                                <asp:Label ID="lblCallsY" runat="server"></asp:Label></font></td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#FF3300" style="font-size: 8pt; color: red; font-family: Arial">
                                    <asp:Label ID="lblCallsC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image5" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="Label11" runat="server" CssClass="labelBlue8" Text="Profit"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblProfitY" runat="server"></asp:Label></font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblProfitC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image7" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    
                    <tr>
                    <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="Label12" runat="server" CssClass="labelBlue8" Text="Total Sales"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblTotSalesY" runat="server"></asp:Label></font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblTotSalesC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image13" runat="server" /></td>
                     </tr>
                    <tr><td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td></tr>
                    
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">ASR</font>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblASRY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblASRC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image8" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">ACD</font>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblACDY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblACDC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image10" runat="server" /></td>
                    </tr>
                </table>
                <br />
                <p>
                    </p>
                    <p>
                        <table border="0" width="100%" id="Table1" cellspacing="0" cellpadding="0" align="left">
                             <tr>
                                <td valign="top" style="height: 38px">
                                    <table border="0" width="30%" id="table2" cellspacing="0" cellpadding="0">
                                        <tr>
                                             <td align="left" bgcolor="#d5e3f0" colspan="3" height="20">
                                                <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/topcountries.gif" /></td>
                                        </tr>
                                        <tr>
                                            <td width="100%" colspan="3" align="left" height="24" valign="bottom">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="width: 33%" valign="bottom">
                                                            <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Countries"></asp:Label></td>
                                                        <td style="width: 37%" align="right" valign="bottom">
                                                            <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Font-Bold="True">Minutes</asp:Label></td>
                                                        <td align="right" valign="bottom" style="width: 34%">
                                                            <asp:Label ID="lblHeaderProfit3" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                                Text="Profit"></asp:Label></td>
                                                    </tr>
                                                </table>
                                                <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                        </tr>
                                        <tr>
                                            <td id="Td1" width="248" colspan="3" align="center" valign="top" style="height: 14px">
                                                <asp:Label ID="lblCouN" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" rowspan="5">
                                                <asp:GridView ID="gdvCountry" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                                    CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                                    HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                                    <Columns>
                                                        <asp:BoundField DataField="Country" ItemStyle-Width="199px">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Minutes" DataFormatString="{0:0,0}" ItemStyle-Width="220px"
                                                            HtmlEncode="false">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Profit" DataFormatString="{0:C0}" ItemStyle-Width="199px"
                                                            HtmlEncode="false">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                             </tr>
                             <tr>
                                <td>&nbsp;</td>
                             </tr>
                             <tr> 
                                <td>
                                    &nbsp;
                                </td> 
                             </tr>
                             <tr>
                                <td>
                                    &nbsp;
                                </td>
                             </tr>
                             <tr>
                                <td>                                   
                                    &nbsp;
                                </td>
                             </tr>
                        </table>     
                    
            </td>
            
            
            <td rowspan="4" style="height: 850px; width: 168px;" valign="top" align="left">
                <table border="0" width="100%" id="tblCenter" cellspacing="0" cellpadding="0" align="left">
                    <tr>
                        <td valign="top">
                            <table border="0" width="30%" id="table15" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" bgcolor="#d5e3f0" colspan="3" height="20">
                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/topcustvend.gif" /></td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="3" align="left" height="24" valign="bottom">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 33%" valign="bottom">
                                                    <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Customers"></asp:Label></td>
                                                <td style="width: 37%" align="right" valign="bottom">
                                                    <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Font-Bold="True">Minutes</asp:Label></td>
                                                <td align="right" valign="bottom" style="width: 34%">
                                                    <asp:Label ID="lblHeaderProfit1" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="Profit"></asp:Label></td>
                                            </tr>
                                        </table>
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td id="IMG" width="248" colspan="3" align="center" valign="top">
                                        <asp:Label ID="lblCusN" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5">
                                        <asp:GridView RowStyle-Height="18px" ID="gdvCustomers" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Customer" HtmlEncode="False" InsertVisible="False" ShowHeader="False">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="220px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Minutes" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="199px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:C0}"
                                                    ItemStyle-Width="199px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                            <RowStyle Height="18px" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="3">
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="3" align="left" height="24">
                                        <table width="100%" align="center">
                                            <tr>
                                                <td style="width: 33%" valign="bottom">
                                                    <asp:Label ID="lblHeaderVendors" runat="server" Text="Vendors" CssClass="labelBlue8"
                                                        Font-Bold="True"></asp:Label></td>
                                                <td align="right" valign="bottom" width="33%">
                                                    <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Font-Bold="True">Minutes</asp:Label>
                                                <td valign="bottom" width="34%" align="right">
                                                    <asp:Label ID="lblHeaderProfit2" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="Profit"></asp:Label>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="3" align="center">
                                        <img border="0" src="Images/line.gif" width="264" height="1"><br />
                                        <asp:Label ID="lblVenN" runat="server" ForeColor="Red" Text="Not available"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5" width="100%" style="height: 100px">
                                        <asp:GridView ID="gdVendors" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Vendor">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="220px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Minutes" DataFormatString="{0:0,0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="199px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" DataFormatString="{0:C0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="199px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="4" style="height: 850px; width: 168px;" valign="top" align="left">
                <table border="0" width="100%" id="Table3" cellspacing="0" cellpadding="0" align="left">
                    <tr>
                        <td align="left" bgcolor="#d5e3f0" colspan="3" height="20">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/toproutes.png" /></td>
                    </tr>
                    <tr>
                        <td width="100%" colspan="3" align="left" height="24" valign="bottom">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 33%" valign="bottom">
                                        <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Routes"></asp:Label></td>
                                    <td style="width: 37%" align="right" valign="bottom">
                                        <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Font-Bold="True">Minutes</asp:Label></td>
                                    <td align="right" valign="bottom" style="width: 34%">
                                        <asp:Label ID="Label9" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                            Text="Profit"></asp:Label></td>
                                </tr>
                            </table>
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td id="Td2" width="248" colspan="3" align="center" valign="top">
                            <asp:Label ID="Label10" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="3" rowspan="5">
                            <asp:GridView RowStyle-Height="18px" ID="gdvRoutes" runat="server" AutoGenerateColumns="False"
                                CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                <Columns>
                                    <asp:BoundField DataField="RouteName" HtmlEncode="False" InsertVisible="False" ShowHeader="False">
                                        <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="220px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Minutes" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                        ItemStyle-Width="199px">
                                        <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Profit" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:C0}"
                                        ItemStyle-Width="199px">
                                        <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <RowStyle Height="18px" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>      
        
              
              
              </tr>
              
              
              
    <tr>
        <td align="left" valign="top">
            &nbsp;<br />
           
   
                </td>
                
                                

    </tr>
    <tr>
        <td align="left" style="height: 38px">
            <br />
        </td>
    </tr>
    <tr>
        <td style="height: 27px">
            <p style="margin-bottom: 2px">
        </td>
    </tr>
    <tr>
        <td colspan="3" width="250">
            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/footer.gif" /></td>
    </tr>
    <tr>
        <td bgcolor="" width="250">
            <p style="margin-left: 20px; margin-top: 10px; margin-bottom: 10px;" align="left">
                
                    <br>
                    <table>
                        <tr>
                            <td align="right" colspan="3">
                                <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                    Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                            <td style="width: 100px">
                                <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                    Width="146px" Visible="True" /></td>
                        </tr>
                    </table>
                
        </td>
        <td style="width: 168px">
        </td>
        <td style="width: 239px">
        </td>
    </tr>
    </table>
</asp:Content>
