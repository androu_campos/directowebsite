using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

public partial class Default5 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //CUSTOMER
            SqlConnection SqlConn1 = new SqlConnection();
            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
            SqlCommand SqlCommand1 = new SqlCommand("SELECT DISTINCT ProviderName from ProviderIP WHERE Type like 'C' UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT '** NONE **' from ProviderIP as ProviderName", SqlConn1);
            SqlConn1.Open();
            SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
            while (myReader1.Read())
            {
               dpdCustomer.Items.Add(myReader1.GetValue(0).ToString());
            }
            myReader1.Close();
            SqlConn1.Close();

            //VENDOR
            
            SqlCommand SqlCommand = new SqlCommand("SELECT DISTINCT ProviderName from ProviderIP WHERE Type like 'V' UNION SELECT '**ALL**' from ProviderIP as ProviderName UNION SELECT '** NONE **' from ProviderIP as ProviderName ", SqlConn1);
            SqlConn1.Open();
            SqlDataReader myReader = SqlCommand.ExecuteReader();
            while (myReader.Read())
            {
                dpdVendor.Items.Add(myReader.GetValue(0).ToString());
            }
            myReader.Close();
            SqlConn1.Close();

            //ORIGINATION IP
            SqlConnection SqlConn = new SqlConnection();
            SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";
            SqlCommand SqlCommand2 = new SqlCommand("SELECT DISTINCT OriginationIP FROM OpSheetAll UNION SELECT '** NONE **' AS OriginationIP FROM OpSheetAll AS OpSheetAll_2 UNION SELECT '**ALL**' AS OriginationIP FROM OpSheetAll AS OpSheetAll_1 ORDER BY OriginationIP", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader2 = SqlCommand2.ExecuteReader();
            while (myReader2.Read())
            {
                dpdOriginationIp.Items.Add(myReader2.GetValue(0).ToString());
            }
            myReader2.Close();
            SqlConn.Close();
            //TERMINATION IP
            SqlCommand SqlCommand3 = new SqlCommand("select distinct TerminationIP from OpSheetAll union select '** NONE **' as TerminationIP from OpSheetAll union select '**ALL**' as TerminationIP from OpSheetAll order by TerminationIP ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader3 = SqlCommand3.ExecuteReader();
            while (myReader3.Read())
            {
                dpdTermination.Items.Add(myReader3.GetValue(0).ToString());
            }
            myReader3.Close();
            SqlConn.Close();            
            //COUNTRY
            SqlCommand SqlCommand4 = new SqlCommand("select distinct Country from Regions union  select '** NONE **' as Country from Regions union select '**ALL**' as Country from Regions order by Country ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader4 = SqlCommand4.ExecuteReader();
            while (myReader4.Read())
            {
                dpdCountry.Items.Add(myReader4.GetValue(0).ToString());
            }
            myReader4.Close();
            SqlConn.Close();            
            //REGION
            SqlCommand SqlCommand5 = new SqlCommand("select distinct Region from Regions union select '** NONE **' as Region from Regions union select '**ALL**' as Region from Regions order by Region ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader5 = SqlCommand5.ExecuteReader();
            while (myReader5.Read())
            {
                dpdRegion.Items.Add(myReader5.GetValue(0).ToString());
            }
            myReader5.Close();
            SqlConn.Close();          
            //TYPE
            SqlCommand SqlCommand6 = new SqlCommand("select distinct Regions.Type from Regions union select '** NONE **' as Type from Regions union select '**ALL**' as Type from Regions order by Type ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader6 = SqlCommand6.ExecuteReader();
            while (myReader6.Read())
            {
                dpdType.Items.Add(myReader6.GetValue(0).ToString());
            }
            myReader6.Close();
            SqlConn.Close();            
            //CLASS
            SqlCommand SqlCommand7 = new SqlCommand("select distinct Class from Regions union  select '** NONE **' as Class from Regions union select '**ALL**' as Class from Regions order by Class ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader7 = SqlCommand7.ExecuteReader();
            while (myReader7.Read())
            {
                dpdClass.Items.Add(myReader7.GetValue(0).ToString());
            }
            myReader7.Close();
            SqlConn.Close();                        
            //SOURCE MSW
            SqlCommand SqlCommand8 = new SqlCommand("SELECT DISTINCT IP FROM MECCA2..VportViewer UNION SELECT '**ALL**' AS IP FROM MECCA2..VportViewer UNION SELECT '** NONE **' AS IP FROM MECCA2..VportViewer", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader8 = SqlCommand8.ExecuteReader();
            while (myReader8.Read())
            {
                dpdSource.Items.Add(myReader8.GetValue(0).ToString());
            }
            myReader8.Close();
            SqlConn.Close();                        
                        
        }

    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        StringBuilder select = new StringBuilder();
        select.Append("SELECT ");
        
        StringBuilder where = new StringBuilder();
        where.Append("WHERE ");

        StringBuilder group = new StringBuilder();
        group.Append("GROUP BY ");

        StringBuilder union = new StringBuilder();
        union.Append(" UNION SELECT ");

        StringBuilder qry = new StringBuilder();
        StringBuilder order = new StringBuilder();
        order.Append(" Order by ");

        //CUSTOMER
        if (dpdCustomer.SelectedValue.ToString() != "** NONE **")
        {
            
            select = select.Append("Customer,");
            where.Append("Customer like '" + dpdCustomer.SelectedValue.ToString() + "' AND ");
            group.Append("Customer,");
            union.Append("'**ALL**' as Customer,");
            order.Append("Customer,");

        }
        //VENDOR
        if (dpdVendor.SelectedValue.ToString() != "** NONE **")
        {
            select = select.Append("Vendor,");
            where = where.Append("Vendor like '" + dpdVendor.SelectedValue.ToString() + "' AND ");
            group.Append("Vendor,");
            union.Append("'**ALL**' as Vendor,");
            order.Append("Vendor,");
        }
        //OriginationIP
        if (dpdOriginationIp.SelectedValue.ToString() != "** NONE **")
        {
            select = select.Append("OriginationIP,");
            where = where.Append("OriginationIP like '" + dpdOriginationIp.SelectedValue.ToString() + "' AND ");
            group.Append("OriginationIP,");
            union.Append("'' as OriginationIP,");
        }
        //TerminationIP
        if (dpdTermination.SelectedValue.ToString() != "** NONE **")
        {
            select = select.Append("TerminationIP,");
            where = where.Append("TerminationIP like '" + dpdTermination.SelectedValue.ToString() + "' AND ");
            group.Append("TerminationIP,");
            union.Append("'' as TerminationIP,");
        
        }
        //Country
        if (dpdCountry.SelectedValue.ToString() != "** NONE **")
        {
            select = select.Append("Country,");
            where = where.Append("Country like '" + dpdCountry.SelectedValue.ToString() + "' AND ");
            group.Append("Country,");
            union.Append("'' as Country,");
        }
        //Region
        if (dpdRegion.SelectedValue.ToString() != "** NONE **")
        {
            select = select.Append("Region,");
            where = where.Append("Region like '" + dpdRegion.SelectedValue.ToString() + "' AND ");
            group.Append("Region,");
            union.Append("'' as Region,");
        }
        //Type
        if (dpdType.SelectedValue.ToString() != "** NONE **")
        {
            select = select.Append("Type,");
            where = where.Append("Type like '" + dpdType.SelectedValue.ToString() +  "' AND ");
            group.Append("Type,");
            union.Append("'' as Type,");
        }
        //Class
        if (dpdClass.SelectedValue.ToString() != "** NONE **")
        {
            select = select.Append("Class,");
            where = where.Append("Class like '" + dpdClass.SelectedValue.ToString() +  "' AND ");
            group.Append("Class,");
            union.Append("'' as Class,");
           
        }
        //Source MSW
        if (dpdSource.SelectedValue.ToString() != "** NONE **")
        {
            select = select.Append("Source,");
            where = where.Append("Source like '" + dpdSource.SelectedValue.ToString() + "' AND ");
            group.Append("Source,");
            union.Append("'' as Source,");
        }


        select = select.Replace("**ALL**", "%");
        where = where.Replace("**ALL**", "%");
        string selectS = select.ToString();
        string whereW = where.ToString();
        string groupG = group.ToString();
        //string orderO = order.ToString();
        string unionU = union.ToString();


        int lst = whereW.LastIndexOf("AND");//where
        whereW = whereW.Remove(lst, whereW.Length - lst);
        
        /*
        lst = selectS.LastIndexOf(",");//select
        selectS = selectS.Remove(lst,selectS.Length-lst);*/

        lst = groupG.LastIndexOf(",");//group
        groupG = groupG.Remove(lst, groupG.Length - lst);

        lst = unionU.LastIndexOf(",");//union
        unionU = unionU.Remove(lst, unionU.Length - lst);
        /*
        lst = orderO.LastIndexOf(",");//order
        orderO = orderO.Remove(lst, orderO.Length - lst);*/
        //

        select.Replace(select.ToString(), selectS);
        where.Replace(where.ToString(), whereW);
        group.Replace(group.ToString(), groupG);
        union.Replace(union.ToString(), unionU);
        //order.Replace(order.ToString(), orderO);

        select.Append("sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD ");
        union.Append(",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD");

        qry.Append(select.ToString() + " from OpSheetAll " + where.ToString() + group.ToString() + union + " from OpSheetAll " + where.ToString() + order + " TotalMinutes DESC");
        
        
        DataSet myDataset =  RunQuery(qry.ToString());



                      
    }



    private System.Data.DataSet RunQuery(string Query)
    {

        SqlCommand sqlQuery = new SqlCommand();
        sqlQuery.CommandText = Query;
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }


}
