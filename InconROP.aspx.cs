using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class InconROP : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string qry = "select c.ip as [IP Real],c.providername,e.ip as [IP Suggested],e.providername from (select distinct ip, providername from providerip where len(ip) =4 and  providername <> 'national'  and providername <> 'national-telular') c full join (SELECT distinct upper(substring([IP],3,4)) as ip,providername FROM [CDRDB].[dbo].[ProviderIP] where len(ip)>4 and providername <> 'national'  and providername <> 'national-telular') e on c.ip=e.ip where (c.ip is null ) or (c.providername <> e.providername)";
        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = System.Data.CommandType.Text;
        sqlSpQuery.CommandText = qry;
        DataSet ResultSet;
        ResultSet = RunQuery(sqlSpQuery);
        gdvRop.DataSource = ResultSet.Tables[0];
        gdvRop.DataBind();

    }
    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
