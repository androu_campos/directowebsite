using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DeleteGroup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            AccessDatasetTableAdapters.PlanBreakOutAdp adp = new AccessDatasetTableAdapters.PlanBreakOutAdp();
            AccessDataset.PlanBreakOutDataTable tbl = adp.GetData();
            DropDownList1.Items.Clear();
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                DropDownList1.Items.Add(tbl.Rows[i]["Name"].ToString());
            }
        }
        else
        { 
        
        }
        

    }

   
    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try
        {
            
            Session["GROUPDELETED"] = DropDownList1.SelectedValue.ToString();
            AccessDatasetTableAdapters.PlanBreakOutAdp adp = new AccessDatasetTableAdapters.PlanBreakOutAdp();
            AccessDataset.PlanBreakOutDataTable tbl = adp.GetDataByName(DropDownList1.SelectedValue.ToString());
            int idPlan = Convert.ToInt32(tbl.Rows[0]["IDPlan"]);
            
            //have SIM assigned?
            AccessDatasetTableAdapters.SIMInfoTableAdp adpSim = new AccessDatasetTableAdapters.SIMInfoTableAdp();
            AccessDataset.SIMInfoDataTable tblSim = adpSim.GetDataByIdplan(idPlan);
            if (tblSim.Rows.Count > 0)
            {
                // Tiene al menos un SIM asignado, NO se puede borrar
                lblDelete.Text = " This group can't be deleted";
                lblDelete.Visible = true;
            }
            else
            {
                adp.Delete(idPlan);
                lblDelete.Text = " Group deleted";
                lblDelete.Visible = true;
                DropDownList1.DataSource = SqlDataSource1;
                DropDownList1.DataBind();
            }            

        }
        catch(Exception ex)
        {
            string error = ex.Message.ToString();
        }
    }
}
