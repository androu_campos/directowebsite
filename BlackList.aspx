<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    Theme="Theme1" AutoEventWireup="true" CodeFile="BlackList.aspx.cs" Inherits="BlackList"
    Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--<table>
        <tr>
            <td>
                <asp:Button ID="lnkTab1" CssClass="clicked" Text="    Upload" runat="server" OnClick="lnkTab1_Click">
                </asp:Button>
            </td>
            <td>
                <asp:Button ID="lnkTab2" CssClass="unclicked" Text="      Edit" runat="server" >
                </asp:Button>
            </td>
        </tr>
    </table>-->
    <table>
        <!--<tr>
            <td style="width: 100px">
                <asp:FileUpload ID="FU" runat="server" />
            </td>
            <td style="width: 100px">
                <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="boton" />
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="lblUpload" runat="server" CssClass="labelBlue8"></asp:Label>
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>-->
        <tr>
            <td>
                <asp:Label ID="lblsrch" runat="server" CssClass="labelBlue8" Text="Number: "></asp:Label>
                <asp:TextBox ID="txtSearch" runat="server" CssClass="textBox"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnsearch" runat="server" CssClass="boton" Text="Search" OnClick="btnsearch_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblCount" runat="server" CssClass="labelBlue8" Text="Total Records:"></asp:Label>
                <asp:TextBox ID="txtCount" runat="server" CssClass="textBox" Enabled="false" Width="90px"
                    TextAlignment="Right"></asp:TextBox>
                <asp:SqlDataSource ID="sqlsacount" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListConnString %>"
                    ProviderName="<%$ ConnectionStrings:BlackListConnString.ProviderName %>" SelectCommand="SELECT COUNT(NumB) Count FROM fn_blacknumb_BCONNECT_SECCIONAMARILLA">
                </asp:SqlDataSource>
                <asp:TextBox ID="txtCountRes" runat="server" CssClass="textBox" Enabled="false" Width="90px"
                    TextAlignment="Right" Visible="false"></asp:TextBox>
                <asp:SqlDataSource ID="sqlscountres" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListConnString %>"
                    ProviderName="<%$ ConnectionStrings:BlackListConnString.ProviderName %>" SelectCommand="SELECT COUNT(NumB) Count, Numb as Number FROM fn_blacknumb_BCONNECT_SECCIONAMARILLA"
                    FilterExpression="Number like '%{0}%'">
                    <FilterParameters>
                        <asp:ControlParameter ControlID="txtSearch" Name="Number" PropertyName="Text" />
                    </FilterParameters>
                </asp:SqlDataSource>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblError" runat="server" CssClass="lblNotFound" Visible="false"></asp:Label>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="width: 103px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 103px" valign="top" align="left">
                <asp:Label ID="Label1" runat="server" Text="Good Records" Visible="False" CssClass="labelBlue8"></asp:Label>
            </td>
            <td style="width: 100px" valign="top" align="left">
                <asp:Label ID="Label2" runat="server" Text="Bad Records" Visible="False" CssClass="labelBlue8"></asp:Label>
            </td>
            <td style="width: 100px" valign="top" align="left">
                <asp:Label ID="Label3" runat="server" Text="Removed Duplicates" Visible="False" CssClass="labelBlue8"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 103px" valign="top">
                <asp:GridView ID="gvGood" runat="server" Font-Names="Arial" Font-Size="8pt" DataSourceID="sqlsaGood"
                    AutoGenerateColumns="False" AllowPaging="True" PageSize="50" EnableModelValidation="True">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="false" />
                    <Columns>
                        <asp:BoundField DataField="number" HeaderText="Number" SortExpression="Number" />
                        <asp:BoundField DataField="Date" HeaderText="Date" ReadOnly="True" SortExpression="Date" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <asp:SqlDataSource ID="sqlsaGood" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListConnString %>"
                    ProviderName="<%$ ConnectionStrings:BlackListConnString.ProviderName %>" SelectCommand="SELECT NumB as Number, cast(Date_add as char(11))as Date FROM fn_blacknumb_BCONNECT_SECCIONAMARILLA">
                </asp:SqlDataSource>
                <asp:GridView ID="gvSearch" runat="server" Font-Names="Arial" Font-Size="8pt" DataSourceID="SQLSEARCH"
                    AutoGenerateColumns="False" AllowPaging="True" PageSize="50" EnableModelValidation="True"
                    Visible="false">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="false" />
                    <Columns>
                        <asp:BoundField DataField="Number" HeaderText="Number" SortExpression="Number" />
                        <asp:BoundField DataField="Date" HeaderText="Date" ReadOnly="True" SortExpression="Date" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <asp:SqlDataSource ID="SQLSEARCH" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListConnString %>"
                    ProviderName="<%$ ConnectionStrings:BlackListConnString.ProviderName %>" SelectCommand="SELECT NumB as Number, cast(Date_add as char(11))as Date FROM fn_blacknumb_BCONNECT_SECCIONAMARILLA order by Number"
                    FilterExpression="Number like '%{0}%'">
                    <FilterParameters>
                        <asp:ControlParameter ControlID="txtSearch" Name="Number" PropertyName="Text" />
                    </FilterParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
