<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="HomeO.aspx.cs" 
Inherits="HomeO" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<%@ OutputCache Location="None" VaryByParam="None" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
    
        function showImage(imgId,typ) {
                var objImg = document.getElementById(imgId);
                if (objImg) {
                    if (typ == "1")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_1b.gif";
                   else if (typ == "2")
                       objImg.src = "Images/botonesMeccaHome/tabmeccahome_1a.gif";
               }
         }

    
    </script>
    
    
    
    
    <table align="left" id="tblMain" cellpadding="6" style="height: 666PX; width: 100%;
        position: absolute; top: 140px; left: 220px;">
        <tr valign="bottom" height="20px">
            <%--<td align="center">
                &nbsp;</td>--%>
            <%--<td align="center">
                &nbsp;</td>--%>
            <td align="left" valign="bottom" colspan="3" style="height: 20px">
                <asp:ImageButton ID="img2" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_1a.gif" OnClick="buttonhome_click" /><asp:ImageButton ID="img1" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_2b.gif" OnClick="buttonicshome_click" ImageAlign="Bottom" /><br /><asp:Image Width="820px" Height="2px" ID="tabline" runat="server" ImageUrl="~/Images/tabmeccahome2_3.gif" /></td>
        </tr>
        <tr>            
            <td colspan="3">
                <%--<asp:Image ID="Image6" runat="server" ImageUrl="~/Images/lineMenu.gif" />--%>
                &nbsp;
            </td>  
        </tr>
        <tr>
            <td rowspan="4" style="height: 650px;" valign="top" width="250">
                <img border="0" src="Images/meccasummaryo.gif" height="24">
                <table border="0" width="264" id="tblSummary" cellspacing="0" cellpadding="0">
                    <tr height="18px">
                        <td width="72" height="20">
                        </td>
                        <td width="71" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">&nbsp; &nbsp;&nbsp; Latest</font></b>
                        </td>
                        <td width="113" colspan="2" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">&nbsp; &nbsp; &nbsp;&nbsp; Change</font></b></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72" align="left">
                            <font color="#004D88" style="font-size: 8pt; margin-left: 10px; margin-top: 5px;
                                margin-bottom: 5px; color: #004d88; font-family: Arial">Minutes</font>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp;
                                <asp:Label ID="lblMinutesY" runat="server"></asp:Label>
                            </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblMinutesC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;</td>
                        <td width="19" align="center">
                            <asp:Image ID="Image4" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">Calls</font>
                        </td>
                        <td width="71">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp; &nbsp;
                                <asp:Label ID="lblCallsY" runat="server"></asp:Label></font></td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#FF3300" style="font-size: 8pt; color: red; font-family: Arial">
                                    <asp:Label ID="lblCallsC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image5" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="Label11" runat="server" CssClass="labelBlue8" Text="Profit"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblProfitY" runat="server"></asp:Label></font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblProfitC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image7" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    
                    <tr>
                    <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="Label12" runat="server" CssClass="labelBlue8" Text="Total Sales"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblTotSalesY" runat="server"></asp:Label></font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblTotSalesC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image13" runat="server" /></td>
                     </tr>
                    <tr><td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td></tr>
                    
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">ASR</font>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblASRY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblASRC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image8" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">ACD</font>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblACDY" runat="server"></asp:Label>
                                </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblACDC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image10" runat="server" /></td>
                    </tr>
                </table>
                <br />
                <p>
                    </p>
                    <p>
                        <table border="0" width="100%" id="Table1" cellspacing="0" cellpadding="0" align="left">
                             <tr>
                                <td valign="top" style="height: 38px">
                                    <table border="0" width="30%" id="table2" cellspacing="0" cellpadding="0">
                                        <tr>
                                             <td align="left" bgcolor="#d5e3f0" colspan="3" height="20">
                                                <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/topregions.gif" /></td>
                                        </tr>
                                        <tr>
                                            <td width="100%" colspan="3" align="left" height="24" valign="bottom">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="width: 33%" valign="bottom">
                                                            <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Regions"></asp:Label></td>
                                                        <td style="width: 37%" align="right" valign="bottom">
                                                            <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Font-Bold="True">Minutes</asp:Label></td>
                                                        <td align="right" valign="bottom" style="width: 34%">
                                                            <asp:Label ID="lblHeaderProfit3" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                                Text="Calls"></asp:Label></td>
                                                    </tr>
                                                </table>
                                                <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                        </tr>
                                        <tr>
                                            <td id="Td1" width="248" colspan="3" align="center" valign="top" style="height: 14px">
                                                <asp:Label ID="lblCouN" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" rowspan="5">
                                                <asp:GridView ID="gdvCountry" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                                    CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                                    HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                                    <Columns>
                                                        <asp:BoundField DataField="Country" ItemStyle-Width="199px">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Minutes" DataFormatString="{0:0,0}" ItemStyle-Width="220px"
                                                            HtmlEncode="false">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Calls" DataFormatString="{0:0,0}" ItemStyle-Width="199px"
                                                            HtmlEncode="false">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                             </tr>
                             <tr>
                                <td>&nbsp;</td>
                             </tr>
                             <tr> 
                                <td>
                                    &nbsp;
                                </td> 
                             </tr>
                             <tr>
                                <td>
                                    &nbsp;
                                </td>
                             </tr>
                             <tr>
                                <td>                                   
                                    &nbsp;
                                </td>
                             </tr>
                        </table>     
                    
            </td>
            
            
            <td rowspan="4" style="height: 850px; width: 168px;" valign="top" align="left">
                <table border="0" width="100%" id="tblCenter" cellspacing="0" cellpadding="0" align="left">
                    <tr>
                        <td valign="top">
                            <table border="0" width="30%" id="table15" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" bgcolor="#d5e3f0" colspan="3" height="20">
                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/topcustvend.gif" /></td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="3" align="left" height="24" valign="bottom">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 250px" valign="bottom">
                                                    <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Customers"></asp:Label></td>
                                                <td style="width: 199px" align="right" valign="bottom">
                                                    <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Font-Bold="True">Minutes</asp:Label></td>
                                                <td align="right" valign="bottom" style="width: 199px">
                                                    <asp:Label ID="lblHeaderProfit1" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="Calls"></asp:Label></td>
                                                <td style="width:199px" valign="bottom" align="right">
                                                    <asp:Label ID="lblCustomerChange" runat="server" Text="Change" CssClass="labelBlue8"
                                                        Font-Bold="True"></asp:Label></td> 
                                            </tr>
                                        </table>
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td id="IMG" width="248" colspan="3" align="center" valign="top">
                                        <asp:Label ID="lblCusN" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5">
                                        <asp:GridView RowStyle-Height="18px" ID="gdvCustomers" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Customer" HtmlEncode="False" InsertVisible="False" ShowHeader="False">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="220px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Minutes" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="199px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Calls" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="199px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Change" DataFormatString="{0:0,0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="199px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:ImageField DataImageUrlField="ChangeImg" ReadOnly="true">
                                                </asp:ImageField>
                                            </Columns>
                                            <RowStyle Height="18px" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="3">
                                        <img border="0" src="Images/line.gif" width="264" height="1"></td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="3" align="left" height="24">
                                        <table width="100%" align="center">
                                            <tr>
                                                <td style="width: 500px" valign="bottom">
                                                    <asp:Label ID="lblHeaderVendors" runat="server" Text="Vendors" CssClass="labelBlue8"
                                                        Font-Bold="True"></asp:Label></td>
                                                <td align="right" valign="bottom" width="199px">
                                                    <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Font-Bold="True">Minutes</asp:Label>
                                                <td valign="bottom" width="199px" align="right">
                                                    <asp:Label ID="lblHeaderProfit2" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="Calls"></asp:Label>
                                                <td style="width:199px" valign="bottom" align="right">
                                                    <asp:Label ID="lblVendorChange" runat="server" Text="Change" CssClass="labelBlue8"
                                                        Font-Bold="True"></asp:Label></td>        
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="3" align="center">
                                        <img border="0" src="Images/line.gif" width="264" height="1"><br />
                                        <asp:Label ID="lblVenN" runat="server" ForeColor="Red" Text="Not available"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5" width="100%" style="height: 100px">
                                        <asp:GridView ID="gdVendors" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Vendor">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="220px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Minutes" DataFormatString="{0:0,0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="199px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                               <asp:BoundField DataField="Calls" DataFormatString="{0:0,0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="199px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Change" DataFormatString="{0:0,0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="199px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:ImageField DataImageUrlField="ChangeImg" ReadOnly="true">
                                                </asp:ImageField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="4" style="height: 850px; width: 269px;" valign="top" align="left">
                <table border="0" width="100%" id="Table4" cellspacing="0" cellpadding="0" align="left">
                    <%--<tr>
                        <td>
                            <asp:Image ID="Image12" runat="server" ImageUrl="~/Images/didportstatus.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left" height="24" valign="bottom">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 33%" valign="bottom">
                                        <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Provider"></asp:Label></td>                                    
                                    <td align="right" valign="bottom" style="width: 34%">
                                        <asp:Label ID="Label13" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                            Text="Ports">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <img border="0" src="Images/line.gif" width="264" height="1">
                        </td>
                    </tr>                    
                    <tr>
                        <asp:Label ID="Label14" runat="server" Text="Not available" Visible="false" ForeColor="Red"></asp:Label></tr>--%>
                   <%-- <tr>
                        <td>
                        <asp:GridView RowStyle-Height="18px" ID="gdvdidProviders" runat="server" AutoGenerateColumns="False"
                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                            <Columns>
                                <asp:BoundField DataField="Providername"  HeaderText="Provider" HtmlEncode="False" InsertVisible="False" ShowHeader="False">
                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="220px" />
                                </asp:BoundField>                                
                                <asp:BoundField DataField="Ports" ShowHeader="False" HtmlEncode="false"  DataFormatString="{0:0,0}"
                                    ItemStyle-Width="199px">
                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                </asp:BoundField>
                            </Columns>
                            <RowStyle Height="18px" />
                        </asp:GridView>
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td>
                            <asp:Image ID="imgTrafficType" runat="server" ImageUrl="Images/icstraffictype.gif" />
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td width="100%" colspan="3" align="left" height="24" valign="bottom">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 33%" valign="bottom">
                                        <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Customers"></asp:Label></td>
                                    <td style="width: 37%" align="right" valign="bottom">
                                        <asp:Label ID="Label9" runat="server" CssClass="labelBlue8" Font-Bold="True">Type</asp:Label></td>
                                    <td align="right" valign="bottom" style="width: 34%">
                                        <asp:Label ID="Label10" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                            Text="Minutes">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </table>                            
                         </td>
                    </tr>--%>
                    <%--<tr>
                        <td width="100%" colspan="3" align="center">
                            <img border="0" src="Images/line.gif" width="264" height="1"><br />
                            <asp:Label ID="Label15" runat="server" ForeColor="Red" Text="Not available" Visible="False"></asp:Label></td>
                    </tr>--%>
                    <%--<tr>
                        <td>
                             <asp:GridView RowStyle-Height="18px" ID="gvTrafficType" runat="server" AutoGenerateColumns="False"
                                CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                    <Columns>
                                        <asp:BoundField DataField="Customer"  HeaderText="Customers" HtmlEncode="False" InsertVisible="False" ShowHeader="False">
                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="220px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Type" ShowHeader="False" HtmlEncode="false" 
                                            ItemStyle-Width="199px">
                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Minutes" ShowHeader="False" HtmlEncode="false"  DataFormatString="{0:0,0}"
                                            ItemStyle-Width="199px">
                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                    <RowStyle Height="18px" />
                            </asp:GridView>
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            <asp:Image ID="imgTopStates" runat="server" ImageUrl="~/Images/meccasummaryoweek.gif" />
                        </td>
                    </tr>
                     <tr>
                        <td width="100%" colspan="3" align="left" height="24" valign="bottom">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 40%" valign="bottom">
                                        <asp:Label ID="Label16" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Week"></asp:Label></td>
                                    <td style="width: 35%" align="right" valign="bottom">
                                        <asp:Label ID="Label17" runat="server" CssClass="labelBlue8" Font-Bold="True">Minutes</asp:Label></td>
                                    <td align="right" valign="bottom" style="width: 34%">
                                        <asp:Label ID="Label18" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                            Text="Calls">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </table>                            
                         </td>
                    </tr>
                    <tr>
                        <td width="100%" colspan="3" align="center">
                            <img border="0" src="Images/line.gif" width="264" height="1"><br />
                            <asp:Label ID="Label19" runat="server" ForeColor="Red" Text="Not available" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                             <asp:GridView RowStyle-Height="18px" ID="gvTopStates" runat="server" AutoGenerateColumns="False"
                                CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                    <Columns>
                                        <asp:BoundField DataField="WeekText"  HeaderText="Week" HtmlEncode="False" InsertVisible="False" ShowHeader="False">
                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="700px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Minutes" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                            ItemStyle-Width="199px">
                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Calls" ShowHeader="False" HtmlEncode="false"  DataFormatString="{0:0,0}"
                                            ItemStyle-Width="199px">
                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                    <RowStyle Height="18px" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <%--<tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="Image1" runat="server" ImageUrl="Images/meccastatus.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td>
                            <ul style="color: #004D88; font-size: 8pt" type="square">                               
                                <li style="text-align: left;">
                                        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="labelSteelBlue" NavigateUrl="~/UnratedDid.aspx"
                                            Font-Size="10pt" Font-Names="Arial" ForeColor="#666666" Width="193px">[HyperLink1]</asp:HyperLink>
                                </li>
                            </ul>     
                        </td>
                    </tr>--%>
                </table>
            </td>
                
              
              
        </tr>
              
    <tr>
        <td align="left" valign="top">
            &nbsp;<br />
           
   
                </td>
                
                                

    </tr>
    <tr>
        <td align="left" style="height: 38px">
            <br />
        </td>
    </tr>
    <tr>
        <td style="height: 12px">
            <p style="margin-bottom: 2px">
        </td>
    </tr>
    <tr>
        <td colspan="2" width="250">
            <DCWC:Chart ID="Chart1" runat="server" Width="550px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                   <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                         <dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
		                        Name="Title1" Color="26, 59, 105" >
		                 </dcwc:Title>
                    </Titles>
                </DCWC:Chart>
        </td>
        <td colspan="2" width="250">
            <DCWC:Chart ID="Chart2" runat="server" Width="550px" Height="245px" Palette="Dundas" BackColor="#D3DFF0" ImageType="Png" 
                        ImageUrl="~/ChartPic_#SEQ(300,3)" BorderLineStyle="Solid" 
                        BackGradientType="TopBottom" BorderLineWidth="2" BorderLineColor="26, 59, 105">
                   <Legends>
					    <dcwc:Legend TitleFont="Microsoft Sans Serif, 8pt, style=Bold" BackColor="Transparent" 
					    EquallySpacedItems="True" Font="Trebuchet MS, 8pt, style=Bold" AutoFitText="False" 
					    Name="Default"></dcwc:Legend>
				    </Legends>
                    <Series>
                        <DCWC:Series ShadowOffset="1" XValueType="DateTime" Name="Ports" ChartType="Line" 
                           Color="DarkOrange">
                        </DCWC:Series>
                    </Series>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <ChartAreas>
                        <DCWC:ChartArea BorderColor="64, 64, 64, 64" BackGradientEndColor="Transparent"  ShadowColor="Transparent" BackGradientType="TopBottom"
                            Name="Default">
                            <AxisX Title="DateTime" Margin="False">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold" Format="g"></LabelStyle>
                            </AxisX>
                            <AxisY Title="Ports">
                                <LabelStyle Font="Trebuchet MS, 1pt, style=Bold"></LabelStyle>
                            </AxisY>
                        </DCWC:ChartArea>
                    </ChartAreas>
                    <Titles>
                         <dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3"
		                        Name="Title1" Color="26, 59, 105" >
		                 </dcwc:Title>
                    </Titles>
                </DCWC:Chart>
        </td>
    </tr>
    <tr>
        <td colspan="3" width="250">
            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/footer.gif" /></td>
    </tr>
    <tr>
        <td bgcolor="" width="250">
            <p style="margin-left: 20px; margin-top: 10px; margin-bottom: 10px;" align="left">
                
                    <br>
                    <table>
                        <tr>
                            <td align="right" colspan="3">
                                <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                    Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                            <td style="width: 100px">
                                <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                    Width="146px" Visible="True" /></td>
                        </tr>
                    </table>
                
        </td>
        <td style="width: 168px">
        </td>
        <td style="width: 269px">
        </td>
    </tr>
    </table>
</asp:Content>

