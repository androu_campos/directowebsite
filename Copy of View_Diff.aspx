<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="View_Diff.aspx.cs" Inherits="View_Diff" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table height="25%" width="30%">
        <tr>
            <td style="width: 114px; float: right;" align="center" bgcolor="#d5e3f0" height="15" rowspan="1">
                <asp:Label ID="lblTitle" runat="server" CssClass="labelBlue8" Font-Bold="True" Font-Size="7pt"
                    Text="Differences between Meccas" Width="190px"></asp:Label></td>
            <td style="width: 12px" height="15">
            </td>
        </tr>
        <tr>
            <td style="width: 114px; height: 13px;" align="left" valign="bottom">
                <table>
                    <tr>
                        <td style="width: 89px">
                            <asp:Label ID="Label1" runat="server" Text="Select Date:" CssClass="labelBlue8" Width="60px"></asp:Label></td>
                        <td style="width: 71px">
                            <asp:TextBox ID="txtDate" runat="server" CssClass="dropdown" Width="57px">2008/02/03</asp:TextBox></td>
                        <td style="width: 30px">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                        <td style="width: 100px">
                            <asp:Button ID="cmdReport" runat="server" CssClass="boton" Text="View Differences"
                                Width="94px" OnClick="cmdReport_Click" Enabled="False" /></td>
                    </tr>
                </table>
                <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Text="For the moment this Report is Not Available...."></asp:Label></td>
            <td style="width: 12px; height: 13px;" valign="bottom">
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="Image1"
                    TargetControlID="txtDate">
                </cc1:CalendarExtender>
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager>
                </td>
        </tr>
        <tr>
            <td style="width: 114px" rowspan="2" align="left" valign="top">
                <asp:Label ID="lbl_Comments" runat="server" CssClass="labelBlue8" Text="Label"
                    Width="464px"></asp:Label></td>
            <td colspan="1" rowspan="2" valign="top" align="left">
                <asp:Panel ID="pnlComments" runat="server" HorizontalAlign="Justify">
                
                    <table>
                        <tr>
                            <td style="width: 100px">
                <asp:Label ID="lblComments" runat="server" CssClass="labelBlue8" Text="Comments"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 100px" valign="top">
                                <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Height="67px" Width="263px"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 100px">
                <asp:Button ID="cmdAddComment" runat="server" CssClass="boton" Text="Add Comment" OnClick="cmdAddComment_Click" />
                <asp:Button ID="cmdEdit" runat="server" CssClass="boton" OnClick="cmdEdit_Click"
                    Text="Edit Comment" Visible="False" />
                                <asp:Label ID="lblChanges" runat="server" CssClass="labelBlue8" Text="Label" Visible="False"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
                
            </td>
        </tr>
        <tr>
        </tr>
    </table>
    <br />
    <table align="left">
        <tr>
            <td align="left" style="width: 100px" valign="top">
                <asp:Label ID="lblLink" runat="server" CssClass="labelBlue8" Text="Label" Visible="False"></asp:Label></td>
            <td align="left" style="width: 100px" valign="top">
            </td>
            <td align="left" style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px" valign="top">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="RESUME" Width="50px"></asp:Label></td>
            <td align="left" style="width: 100px" valign="top">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="CALLS WITH 1 MIN OF DIFFERENCE" Width="186px"></asp:Label></td>
            <td align="left" style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px" valign="top">
                <asp:GridView ID="gdv1" runat="server" Height="33px" AllowPaging="True" Font-Names="Arial" Font-Size="8pt" OnPageIndexChanging="gdv1_PageIndexChanging" Width="250px">
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td align="left" style="width: 100px" valign="top">
                <asp:GridView ID="gdv2" runat="server" Height="33px" AllowPaging="True" Font-Names="Arial" Font-Size="8pt" OnPageIndexChanging="gdv2_PageIndexChanging" PageSize="5" Width="250px">
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td align="left" style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px; height: 25px" valign="bottom">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="CALLS WITH DIFFERENCE > 1 MIN" Width="293px"></asp:Label></td>
            <td align="left" style="width: 100px; height: 25px" valign="bottom">
                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="DIFFERENCES BETWEEN ID's" Width="146px"></asp:Label></td>
            <td align="left" style="width: 100px; height: 25px" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px" valign="top">
                <asp:GridView ID="gdv3" runat="server" AllowPaging="True" Font-Names="Arial" Font-Size="8pt" OnPageIndexChanging="gdv3_PageIndexChanging" PageSize="5" Width="250px">
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td align="left" style="width: 100px" valign="top">
                <asp:GridView ID="gdv4" runat="server" AllowPaging="True" Font-Names="Arial" Font-Size="8pt" OnPageIndexChanging="gdv4_PageIndexChanging" PageSize="5" Width="250px">
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td align="left" style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px; height: 25px" valign="bottom">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="CDR's ONLY IN NATIONAL" Width="144px"></asp:Label></td>
            <td style="width: 100px; height: 25px">
            </td>
            <td style="width: 100px; height: 25px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px" valign="top">
                <asp:GridView ID="gdv6" runat="server" AllowPaging="True" Font-Names="Arial" Font-Size="8pt" OnPageIndexChanging="gdv6_PageIndexChanging" PageSize="5" Width="250px">
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px; height: 31px" valign="bottom">
                <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="CDR's ONLY IN DIRECTO" Width="129px"></asp:Label></td>
            <td style="width: 100px; height: 31px">
            </td>
            <td style="width: 100px; height: 31px">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3" valign="top">
                <asp:GridView ID="gdv5" runat="server" AllowPaging="True" Font-Names="Arial" Font-Size="8pt" OnPageIndexChanging="gdv5_PageIndexChanging" PageSize="5" Width="250px">
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

