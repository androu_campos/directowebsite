using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class Comisions : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!Page.IsPostBack)
        {
            txtFrom.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            dpdManager.DataSource = sqlBroker;
            dpdManager.DataTextField = "Broker";
            dpdManager.DataValueField = "Broker";
            dpdManager.DataBind();

        }
        else
        { 
        
        }
    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {
        try
        {
            
            string broker = dpdManager.SelectedValue.ToString();

            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = System.Data.CommandType.StoredProcedure;
            sqlSpQuery.CommandText = "sp_commisions";            
            sqlSpQuery.Parameters.Add("@Broker", SqlDbType.VarChar).Value = dpdManager.SelectedValue.ToString();
            sqlSpQuery.Parameters.Add("@from", SqlDbType.VarChar).Value = txtFrom.Text.ToString();
            sqlSpQuery.Parameters.Add("@to", SqlDbType.VarChar).Value = txtTo.Text.ToString();            
            sqlSpQuery.CommandTimeout = 60;
            DataSet ResultSetC;
            ResultSetC = RunQuery(sqlSpQuery);
            Session["ResultSetComisions"] = ResultSetC;

            int c = ResultSetC.Tables[0].Rows.Count;
            if (c > 0)
            {
                Button1.Visible = true;
                gdvComisions.DataSource = ResultSetC.Tables[0];
                gdvComisions.DataBind();
            }
            else
            {
                Button1.Visible = false;
            }

            
        }
        catch (Exception ex)
        {
            string messagerror = ex.Message;
        }
        
        
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {
                     
            DataTable dtEmployee = ((DataSet)Session["ResultSetComisions"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "Commissions" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel,filename);
            
                        
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
   
    protected void gdvComisions_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    {
        gdvComisions.PageIndex = e.NewPageIndex;
        DataSet myDatst=(DataSet)Session["ResultSetComisions"];
        gdvComisions.DataSource = myDatst.Tables[0];
        gdvComisions.DataBind();
    }
}
