using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class RTraffic : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string type = string.Empty;
        string table = string.Empty;
        string sqlqry = string.Empty;
        string hometype = string.Empty;

        if(Request.QueryString.Count > 0)
        {
            type = Request.QueryString["type"].ToString();
            if(Session["hometype"] != string.Empty)
                hometype = Session["hometype"].ToString();

            if (Request.QueryString["table"] != null)
            {
                table = Request.QueryString["table"].ToString();
            }

            if (table == string.Empty)
            {
                if (hometype == "R") //Aplica condición para obtener unicamente los clientes de ICS
                {
                    if (type == "Customer")
                    {
                        //sqlqry = "Select " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo].OpSheetAllR where " + type + " like 'ICS%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' group by " + type + " ";
                        //sqlqry = sqlqry + " UNION Select  '*** ALL ***' as " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo].OpSheetAllR where " + type + " like 'ICS%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' order by  TotalMinutes DESC";

                        sqlqry = "Select " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo].OpSheetAllR where " + type + " like 'ICS%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' group by " + type + " ";
                        sqlqry = sqlqry + " UNION Select CASE WHEN Customer LIKE 'ICS%2' THEN 'ICS-FIRSTU2' WHEN Customer LIKE 'ICS%5' THEN 'ICS-FIRSTU5' ELSE Customer END [Customer],sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo].OpSheetAllR where (" + type + " like 'ICS%2' or " + type + " like 'ICS%5') and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' group by CASE WHEN Customer LIKE 'ICS%2' THEN 'ICS-FIRSTU2' WHEN Customer LIKE 'ICS%5' THEN 'ICS-FIRSTU5' ELSE Customer END ";
                        sqlqry = sqlqry + " UNION Select  '*** ALL ***' as " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo].OpSheetAllR where " + type + " like 'ICS%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' order by  TotalMinutes DESC";
                    }
                    else
                    {
                        sqlqry = "Select " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo].OpSheetAllR where Customer like 'ICS%' and " + type + " like '%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' group by " + type + " ";
                        sqlqry = sqlqry + " UNION Select  '*** ALL ***' as " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo].OpSheetAllR where Customer like 'ICS%' and " + type + " like '%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' order by  TotalMinutes DESC";
                    }                    
                }
                else //Para todos los clientes que no sean ICS
                {

                    sqlqry = "Select * INTO #OpSheetAll from [mecca2].[dbo].OpSheetAll \n";
                    sqlqry += "Update #OpSheetAll Set Customer = 'ICS' WHERE Customer like 'ICS%' \n";
                    sqlqry += "Update #OpSheetAll Set Customer = 'TALKTEL' WHERE Customer in (SELECT CUSTOMER FROM CDRDB.DBO.CUSTBROKERREL WHERE CUSTBROKER = 'CallCenters' UNION SELECT 'STI-800')\n";

                    sqlqry += "SELECT CASE WHEN " + type + " Like 'ICS%' THEN 'ICS' ELSE " + type + " END as " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from #OpSheetAll where " + type + " like '%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' group by CASE WHEN " + type + " Like 'ICS%' THEN 'ICS' ELSE " + type + " END ";
                    sqlqry = sqlqry + " UNION Select  '*** ALL ***' as " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from #OpSheetAll where " + type + " like '%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' order by  TotalMinutes DESC";
                }
            }
            else
            {
                if (hometype == "R")//Aplica condición para obtener unicamente los clientes de ICS
                {
                    if (type == "Customer")
                    {
                        sqlqry = "Select " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo]." + table + "R where " + type + " like 'ICS%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' group by " + type + " ";
                        sqlqry = sqlqry + " UNION Select CASE WHEN Customer LIKE 'ICS%2' THEN 'ICS-FIRSTU2' WHEN Customer LIKE 'ICS%5' THEN 'ICS-FIRSTU5' ELSE Customer END [Customer],sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo]." + table + "R where (" + type + " like 'ICS%2' or " + type + " like 'ICS%5') and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' group by CASE WHEN Customer LIKE 'ICS%2' THEN 'ICS-FIRSTU2' WHEN Customer LIKE 'ICS%5' THEN 'ICS-FIRSTU5' ELSE Customer END ";
                        sqlqry = sqlqry + " UNION Select  '*** ALL ***' as " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo]." + table + "R where " + type + " like 'ICS%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' order by  TotalMinutes DESC";
                    }
                    else
                    {
                        sqlqry = "Select " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo]." + table + "R where Customer like 'ICS%' and " + type + " like '%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' group by " + type + " ";
                        sqlqry = sqlqry + " UNION Select  '*** ALL ***' as " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [ICS].[dbo]." + table + "R where Customer like 'ICS%' and " + type + " like '%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' order by  TotalMinutes DESC";
                    }
                }
                else //Para todos los clientes que no sean ICS
                {
                    sqlqry = "Select * INTO #" + table + " from [mecca2].[dbo]." + table + "\n";
                    sqlqry += "Update #" + table + " Set Customer = 'ICS' WHERE Customer like 'ICS%' \n";
                    sqlqry += "Update #" + table + " Set Customer = 'TALKTEL' WHERE Customer in (SELECT CUSTOMER FROM CDRDB.DBO.CUSTBROKERREL WHERE CUSTBROKER = 'CallCenters' UNION SELECT 'STI-800')\n";

                    //sqlqry += "Select CASE WHEN " + type + " Like 'ICS%' THEN 'ICS' ELSE " + type + " END as " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [mecca2].[dbo]." + table + " where " + type + " like '%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' group by CASE WHEN " + type + " Like 'ICS%' THEN 'ICS' ELSE " + type + " END ";
                    //sqlqry = sqlqry + " UNION Select  '*** ALL ***' as " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from [mecca2].[dbo]." + table + " where " + type + " like '%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' order by  TotalMinutes DESC";

                    sqlqry += "Select CASE WHEN " + type + " Like 'ICS%' THEN 'ICS' ELSE " + type + " END as " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from #" + table + " where " + type + " like '%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' group by CASE WHEN " + type + " Like 'ICS%' THEN 'ICS' ELSE " + type + " END ";
                    sqlqry = sqlqry + " UNION Select  '*** ALL ***' as " + type + ",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) as AnsweredCalls,sum(RA) as Rejected,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD from #" + table + " where " + type + " like '%' and " + type + " is not NULL and OriginationIP like '%' and TerminationIP like '%' and Country like '%' and Region like '%' order by  TotalMinutes DESC";
                }
            }

            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = System.Data.CommandType.Text;
            sqlSpQuery.CommandText = sqlqry;
            DataSet ResultSet;
            ResultSet = RunQuery(sqlSpQuery);

            Session["RTraffic"] = ResultSet;


            BoundField Type = new BoundField();
            Type.DataField = type;
            Type.HeaderText = type;
            gdvRtraffic.Columns.Insert(0,Type);



            gdvRtraffic.Columns[0].ItemStyle.HorizontalAlign = HorizontalAlign.Left;
            gdvRtraffic.Columns[0].ItemStyle.Wrap = false;
            gdvRtraffic.DataSource = ResultSet.Tables[0];
            gdvRtraffic.DataBind();


            if (type == "Customer" & table == "")
            {
                lblHeader.Text = "Today's Customer Traffic";
            }
            else if (type == "Vendor" & table == "")
            {
                lblHeader.Text = "Today's Vendor Traffic";
            }
            else if (type == "Customer" & table == "OpSheetInc")
            {
                lblHeader.Text = "Recent Customers Traffic";
            }
            else if (type == "Vendor" & table == "OpSheetInc")
            {
                lblHeader.Text = "Recent Vendor Traffic";
            }
            else if (type == "Customer" & table == "OpSheetY")
            {
                lblHeader.Text = "Yesterday's Customers Traffic";
            }
            else if (type == "Vendor" & table == "OpSheetY")
            {
                lblHeader.Text = "Yesterday's Vendor Traffic";
            }

  
        }
        
        
    }


    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
        
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

    protected void gdvRtraffic_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }

    protected void cmdExport_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["RTraffic"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "Traffic" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string message = ex.Message.ToString();
        }
    }
}
