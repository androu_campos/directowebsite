using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Graph_NvsCT : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Value = DateTime.Today.AddDays(-1).ToShortDateString();
            txtTo.Value = DateTime.Today.AddDays(-1).ToShortDateString();

            txtFromV.Text = txtFrom.Value.ToString();
        }
        else
        {
            txtFromV.Text = txtFrom.Value.ToString();
        }

    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        ddlRegion.Enabled = true;
        dsRegion.SelectCommand = "select distinct Region from MECCA2.dbo.trafficlog where (vendor ='NATIONAL' OR vendor='NATIONAL-TELULAR') AND VendorMinutes > 1  and billingdate = '" + txtFrom.Value.ToString() + "' order by region";
        btnSubmit.Enabled = true;
        //"SELECT '** ALL **'  as Region UNION select distinct Region from MECCA2.dbo.trafficlog where (vendor ='NATIONAL' OR vendor='NATIONAL-TELULAR') AND Minutes > 1  and billingdate BETWEEN '" + txtFrom.Value.ToString() + " " + txtHf.Text.ToString() + "'  and '" + txtFrom.Value.ToString() + " " + txtHf.Text.ToString() + "' order by region";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {


        string strCnn = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlConnection cnn = new SqlConnection(strCnn);
        SqlDataSource dsDatos = new SqlDataSource();
        SqlCommand x = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet dsG1 = new DataSet();
        x.CommandText = "sp_GraphNvsCTh";
        x.CommandType = CommandType.StoredProcedure;
        x.Connection = cnn;
        da.SelectCommand = x;

        SqlParameter pinf = new SqlParameter("@inf", SqlDbType.DateTime);
        pinf.Direction = ParameterDirection.Input;
        SqlParameter psup = new SqlParameter("@sup", SqlDbType.DateTime);
        psup.Direction = ParameterDirection.Input;
        SqlParameter preg = new SqlParameter("@Region", SqlDbType.VarChar);
        preg.Direction = ParameterDirection.Input;
        SqlParameter pi = new SqlParameter("@i", SqlDbType.Int);
        pi.Direction = ParameterDirection.Input;




        pinf.Value = txtFrom.Value.ToString() + " " + txtHf.Text;
        psup.Value = txtFrom.Value.ToString() + " " + txtHt.Text;
        preg.Value = ddlRegion.SelectedValue.ToString();
        pi.Value = 60;

        x.Parameters.Add(pinf);
        x.Parameters.Add(psup);
        x.Parameters.Add(preg);
        x.Parameters.Add(pi);

        x.CommandTimeout = 600;
        da.Fill(dsG1);
        Response.Redirect("http://mecca3.computer-tel.com/ReportServer/Pages/ReportViewer.aspx?%2fNoc%2fGraph_NvsCT&rs%3aCommand=Render");

    }

}

