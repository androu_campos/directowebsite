using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class CdrPrefix : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            MECCA2TableAdapters.CDR_VendorIDAdp adp = new MECCA2TableAdapters.CDR_VendorIDAdp();
            MECCA2.CDR_VendorIDDataTable tbl = adp.GetData();
            //fill the dropdown of edit
            dpDelete.Items.Clear();
            dpDelete.Items.Add("** select Id **");
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                dpDelete.Items.Add(tbl.Rows[i]["Prefix"].ToString());
            }

            lblDesc.Text = string.Empty;
            lblID.Text = string.Empty;
            lblType.Text = string.Empty;
            lblPrefix.Text = string.Empty;
        
            
        }

    }
    protected void cmdCommand_Click(object sender, EventArgs e)
    {
        try
        {
            MECCA2TableAdapters.CDR_PrefixTableAdapter adp = new MECCA2TableAdapters.CDR_PrefixTableAdapter();                    
            //
            adp.Insert(txtPrefix.Text, txtID.Text, dpdType.SelectedValue.ToString(), txtDescription.Text, Session["DirectoUser"].ToString(), Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            //
            MECCA2.CDR_PrefixDataTable tbl = adp.GetData();
            //fill the dropdown of edit
            dpDelete.Items.Clear();
            dpDelete.Items.Add("** select Id **");
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                dpDelete.Items.Add(tbl.Rows[i]["Prefix"].ToString());
            }
            TabContainer1.ActiveTab.ID = "1";
            Label12.Visible = true;
            Label12.Text = "Create succesfully";
            Label12.Visible = true;

        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            string s = "Cannot insert duplicate key in object 'dbo.CDR_Prefix'.";


            if (error == "Violation of PRIMARY KEY constraint 'PK_CDR_Prefix'. Cannot insert duplicate key in object 'dbo.CDR_Prefix'.\r\nThe statement has been terminated.")
            {
                Label12.Text = s;
                Label12.Visible = true;
            }
        
        }


    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try
        {
            MECCA2TableAdapters.CDR_PrefixTableAdapter adp = new MECCA2TableAdapters.CDR_PrefixTableAdapter();
            if (dpDelete.SelectedValue.ToString() != "** select Id **")
            {                
               
                adp.DeleteQuery(dpDelete.SelectedValue.ToString());
                MECCA2.CDR_PrefixDataTable tbl = adp.GetData();
                //fill the dropdown of edit
                dpDelete.Items.Clear();
                dpDelete.Items.Add("** select Id **");
                for (int i = 0; i < tbl.Rows.Count; i++)
                {
                    dpDelete.Items.Add(tbl.Rows[i]["Prefix"].ToString());
                }
                lblDesc.Text = string.Empty;
                lblID.Text = string.Empty;
                lblType.Text = string.Empty;
                lblPrefix.Text = string.Empty;
                Label13.Text = "Deleted";
                Label13.Visible = true;
            }
            else
            {
                Label13.Text = "";
                Label13.Visible = true;

            }
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }

    }

    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString =
            ConfigurationManager.ConnectionStrings
            ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
            
        }
        catch(Exception ex)
        {
            //labelStatus.Text = "Unable to connect to SQL Server.";
            string errormessage = ex.Message.ToString();
        }
        return resultsDataSet;
    }
    protected void dpDelete_SelectedIndexChanged(object sender, EventArgs e)
    {
        string id = dpDelete.SelectedValue.ToString();
        if(id == "** select Id **")
        {
            lblDesc.Text = string.Empty;
            lblID.Text = string.Empty;
            lblType.Text = string.Empty;
            lblPrefix.Text = string.Empty;
            TabContainer1.ActiveTab.ID =  "1";
            
        
        }
        else
        {
        MECCA2TableAdapters.CDR_PrefixTableAdapter adp = new MECCA2TableAdapters.CDR_PrefixTableAdapter();
        MECCA2.CDR_PrefixDataTable tbl = adp.GetDataByPrefix(dpDelete.SelectedValue.ToString());

        lblDesc.Text = tbl.Rows[0]["description"].ToString();
        lblID.Text = tbl.Rows[0]["ID"].ToString();
        lblType.Text = tbl.Rows[0]["Type"].ToString();
        lblPrefix.Text = tbl.Rows[0]["Prefix"].ToString();
        TabContainer1.ActiveTab.ID = "1";

        }



    }
    protected void cmdView_Click(object sender, EventArgs e)
    {
        string id = dpDelete.SelectedValue.ToString();
        MECCA2TableAdapters.CDR_PrefixTableAdapter adp = new MECCA2TableAdapters.CDR_PrefixTableAdapter();
        MECCA2.CDR_PrefixDataTable tbl = adp.GetDataByPrefix(id);
        lblDesc.Text = tbl.Rows[0]["description"].ToString();
        lblID.Text = tbl.Rows[0]["ID"].ToString();
        lblPrefix.Text = tbl.Rows[0]["Prefix"].ToString();
        lblType.Text = tbl.Rows[0]["Type"].ToString();
        Label13.Visible = false;
    }
}
