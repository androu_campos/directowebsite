using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using System.Net.Sockets;

public partial class CtryHome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {


            if (Session["Rol"].ToString() == "Financial")
            {
                this.Response.Redirect("FinancialHome.aspx", false);
            }


            MECCA2TableAdapters.Special_ConsiderationsAdp adpSc = new MECCA2TableAdapters.Special_ConsiderationsAdp();
            MECCA2.Special_ConsiderationsDataTable tblSc = adpSc.GetData();
            StringBuilder comments = new StringBuilder();

            Label8.Text = tblSc.Rows[1][2].ToString();
            Label9.Text = tblSc.Rows[1][3].ToString();
            Label10.Text = tblSc.Rows[1][4].ToString();
            Label11.Text = tblSc.Rows[1][5].ToString();

            Label12.Text = tblSc.Rows[0][2].ToString();
            Label13.Text = tblSc.Rows[0][3].ToString();
            Label14.Text = tblSc.Rows[0][4].ToString();
            Label15.Text = tblSc.Rows[0][5].ToString();


            MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adp = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
            MyDataSetTableAdapters.Billing_StatusDataTable tbl = adp.GetData();

            /**/
            //TODAY
                        
            /**/

            string broker = string.Empty;
            cdrdbDatasetTableAdapters.loginAdp adpUser = new cdrdbDatasetTableAdapters.loginAdp();
            cdrdbDataset.loginDataTable tblUser = adpUser.GetDataByUser(Session["DirectoUser"].ToString());
            broker = tblUser.Rows[0]["BrokerName"].ToString();

            SqlCommand sqlC = new SqlCommand();
            sqlC.CommandType = System.Data.CommandType.StoredProcedure;
            sqlC.CommandText = "sp_commisions1";
            sqlC.Parameters.Add("@Broker", SqlDbType.VarChar).Value = broker;
            sqlC.Parameters.Add("@from", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();
            sqlC.Parameters.Add("@to", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();
            sqlC.CommandTimeout = 60;
            DataTable tblSummary;
            tblSummary = RunQuery(sqlC).Tables[0];
            
            SqlCommand sqlC2 = new SqlCommand();
            sqlC2.CommandType = System.Data.CommandType.StoredProcedure;
            sqlC2.CommandText = "sp_commisions1";
            sqlC2.Parameters.Add("@Broker", SqlDbType.VarChar).Value = broker;
            sqlC2.Parameters.Add("@from", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-7).ToShortDateString();
            sqlC2.Parameters.Add("@to", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-7).ToShortDateString();
            sqlC2.CommandTimeout = 60;
            DataTable tblSummary2;
            tblSummary2 = RunQuery(sqlC2).Tables[0];



            if (tbl.Rows.Count > 0)//Billing running
            {
                lblBillingRunning.Visible = true;
                lblBillingRunning.Text = "Billing Running !!!";
                lblBillingRunning.ForeColor = System.Drawing.Color.Red;

                HyperLink1.Visible = false;
                HyperLink2.Visible = false;
                //  Get Dates of Yesterday
                string CMinutes = tblSummary.Rows[0]["Minutes"].ToString();
                lblMinutesY.Text = Util.getNumberFormat(CMinutes, 1);
                string Calls = tblSummary.Rows[0]["AnsweredCalls"].ToString();
                lblCallsY.Text = Util.getNumberFormat(Calls, 0);
                string Profit = tblSummary.Rows[0]["GrossProfit"].ToString();
                lblProfitY.Text = Util.getNumberFormat(Profit, 1);
                lblASRY.Text = tblSummary.Rows[0]["ASR"].ToString() + "%";
                lblACDY.Text = tblSummary.Rows[0]["ACD"].ToString();
                LabelTotalCost.Text = tblSummary.Rows[0]["TotalCost"].ToString();
                LabelTotalSales.Text = tblSummary.Rows[0]["TotalSales"].ToString();

                float minutesY = (float)Convert.ToSingle(tblSummary.Rows[0]["Minutes"].ToString());
                int callsY = Convert.ToInt32(tblSummary.Rows[0]["AnsweredCalls"].ToString());
                float profitY = (float)Convert.ToSingle(tblSummary.Rows[0]["GrossProfit"].ToString());
                int asrY = (int)Convert.ToSingle(tblSummary.Rows[0]["ASR"].ToString());
                float acdY = (float)Convert.ToSingle(tblSummary.Rows[0]["ACD"].ToString());
                float totC = (float)Convert.ToSingle(tblSummary.Rows[0]["TotalCost"].ToString());
                float totS = (float)Convert.ToSingle(tblSummary.Rows[0]["TotalSales"].ToString());

                ////Get Dates of PRE-Yesterday
                float minutesA = (float)Convert.ToSingle(tblSummary2.Rows[0]["Minutes"].ToString());
                int callsA = Convert.ToInt32(tblSummary2.Rows[0]["AnsweredCalls"].ToString());
                float profitA = (float)Convert.ToSingle(tblSummary2.Rows[0]["GrossProfit"].ToString());
                int asrA = (int)Convert.ToSingle(tblSummary2.Rows[0]["ASR"].ToString());
                float acdA = (float)Convert.ToSingle(tblSummary2.Rows[0]["ACD"].ToString());
                float totCA = (float)Convert.ToSingle(tblSummary2.Rows[0]["TotalCost"].ToString());
                float totSA = (float)Convert.ToSingle(tblSummary2.Rows[0]["TotalSales"].ToString());


                ////column change
                string MinutesC = Convert.ToString(minutesY - minutesA).Replace("-", "");
                int isfloat = Convert.ToInt32(MinutesC.IndexOf("."));
                if (isfloat < 0)
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 0);
                }
                else
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 1);
                }

                lblMinutesC.Text = Convert.ToString((minutesA - minutesY)).Replace("-", "");
                lblCallsC.Text = Convert.ToString((callsA - callsY)).Replace("-", "");
                lblProfitC.Text = "$" + Convert.ToString((profitA - profitY)).Replace("-", "");
                lblASRC.Text = Convert.ToString((asrA - asrY)).Replace("-", "");
                lblACDC.Text = Convert.ToString((acdA - acdY)).Replace("-", "");
                LabelTotalCostCh.Text = "$" + Convert.ToString((totCA - totC)).Replace("-", "");
                LabelTotalSalesCh.Text =  "$" +Convert.ToString((totSA - totS)).Replace("-", "");

                //// end column change


                /*/ SET FLAGS/*/

                setFlags(minutesY, callsY, profitY, asrY, acdY, totC, totS, minutesA, callsA, profitA, asrA, acdA, totCA, totSA);


                lblBillingRunning.Visible = true;
                lblBillingRunning.Text = "Billing Running";

                MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter adpU = new MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter();
                MyDataSetTableAdapters.unratedMinutes_DataTable tblU = adpU.GetData();
                if (tblU.Rows.Count > 0)//There are unrated minutes?
                {
                    HyperLink1.Text = "There are unrated minutes in MECCA!";
                    HyperLink1.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    HyperLink1.Text = "All minutes are rated in MECCA!";
                }


                MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter adpU2 = new MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter();
                MyDataSetTableAdapters.UnknownIDs2DataTable tblU2 = adpU2.GetData();

                if (tblU2.Rows.Count > 0)
                {
                    HyperLink2.Text = "There are unmatched IDs in MECCA!";
                    HyperLink2.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    HyperLink2.Text = "All IDs are matched";
                }

                //pnlCtryMan.Visible = false;
            }//BILLING not RUNNING
            else
            {

                //RunCtryCmmsn("sp_countryCommissions");//****************************
                //pnlCtryMan.Visible = true;
                /*                                         MECCA  SUMMARY                                       */
                lblCusN.Visible = false;
                lblVenN.Visible = false;
                lblCouN.Visible = false;
                //  Get Dates of Yesterday
                string CMinutes = tblSummary.Rows[0]["Minutes"].ToString();
                lblMinutesY.Text = Util.getNumberFormat(CMinutes, 1);
                string Calls = tblSummary.Rows[0]["AnsweredCalls"].ToString();
                lblCallsY.Text = Util.getNumberFormat(Calls, 0);
                string Profit = tblSummary.Rows[0]["GrossProfit"].ToString();
                lblProfitY.Text = "$" + Util.getNumberFormat(Profit, 1);
                lblASRY.Text = tblSummary.Rows[0]["ASR"].ToString() + "%";
                lblACDY.Text = tblSummary.Rows[0]["ACD"].ToString();
                LabelTotalCost.Text = "$" + tblSummary.Rows[0]["TotalCost"].ToString();
                LabelTotalSales.Text = "$" + tblSummary.Rows[0]["TotalSales"].ToString();

                float minutesY = (float)Convert.ToSingle(tblSummary.Rows[0]["Minutes"].ToString());
                int callsY = Convert.ToInt32(tblSummary.Rows[0]["AnsweredCalls"].ToString());
                float profitY = (float)Convert.ToSingle(tblSummary.Rows[0]["GrossProfit"].ToString());
                int asrY = (int)Convert.ToSingle(tblSummary.Rows[0]["ASR"].ToString());
                float acdY = (float)Convert.ToSingle(tblSummary.Rows[0]["ACD"].ToString());
                float totC = (float)Convert.ToSingle(tblSummary.Rows[0]["TotalCost"].ToString());
                float totS = (float)Convert.ToSingle(tblSummary.Rows[0]["TotalSales"].ToString());

                ////Get Dates of PRE-Yesterday
                float minutesA = (float)Convert.ToSingle(tblSummary2.Rows[0]["Minutes"].ToString());
                int callsA = Convert.ToInt32(tblSummary2.Rows[0]["AnsweredCalls"].ToString());
                float profitA = (float)Convert.ToSingle(tblSummary2.Rows[0]["GrossProfit"].ToString());
                int asrA = (int)Convert.ToSingle(tblSummary2.Rows[0]["ASR"].ToString());
                float acdA = (float)Convert.ToSingle(tblSummary2.Rows[0]["ACD"].ToString());
                float totCA = (float)Convert.ToSingle(tblSummary2.Rows[0]["TotalCost"].ToString());
                float totSA = (float)Convert.ToSingle(tblSummary2.Rows[0]["TotalSales"].ToString());

                    
                ////column change
                string MinutesC = Convert.ToString(minutesY - minutesA).Replace("-", "");
                int isfloat = Convert.ToInt32(MinutesC.IndexOf("."));
                if (isfloat < 0)
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 0);
                }
                else
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 1);
                }

                lblMinutesC.Text = Convert.ToString((minutesA - minutesY)).Replace("-", "");
                lblCallsC.Text = Convert.ToString((callsA - callsY)).Replace("-", "");
                lblProfitC.Text = Convert.ToString((profitA - profitY)).Replace("-", "");
                lblASRC.Text = Convert.ToString((asrA - asrY)).Replace("-", "");
                lblACDC.Text = Convert.ToString((acdA - acdY)).Replace("-", "");
                LabelTotalCostCh.Text = Convert.ToString((totCA - totC)).Replace("-", "");
                LabelTotalSalesCh.Text = Convert.ToString((totSA - totS)).Replace("-", "");

                //// end column change

                /*/ SET FLAGS/*/
                
                setFlags(minutesY, callsY, profitY, asrY, acdY, totC, totS, minutesA, callsA, profitA, asrA, acdA, totCA,totSA);
                
                lblBillingRunning.Visible = true;
                lblBillingRunning.Text = "Billing not Runing";

                MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter adpU = new MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter();
                MyDataSetTableAdapters.unratedMinutes_DataTable tblU = adpU.GetData();
                if (tblU.Rows.Count > 0)//There are unrated minutes?
                {
                    HyperLink1.Text = "There are unrated minutes in MECCA!";
                    HyperLink1.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    HyperLink1.Text = "All minutes are rated in MECCA!";
                }


                MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter adpU2 = new MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter();
                MyDataSetTableAdapters.UnknownIDs2DataTable tblU2 = adpU2.GetData();

                if (tblU2.Rows.Count > 0)
                {
                    HyperLink2.Text = "There are unmatched IDs in MECCA!";
                    HyperLink2.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    HyperLink2.Text = "All IDs are matched";
                }

                
                
//HERE
//TOP 5 Customers

                //                RunTop("sp_TopCountry1", broker, 0);               
                SqlCommand sqlSpQuery4 = new SqlCommand();
                sqlSpQuery4.CommandType = System.Data.CommandType.Text;
                sqlSpQuery4.CommandText = "SELECT TOP 12 Customer,ROUND(SUM(T.CustMinutes),4) as CMinutes ,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Customer in (SELECT [Customer] FROM [MECCA2].[dbo].[CBroker] where Custbroker like '" + broker + "') GROUP BY T.Customer HAVING SUM(CustMinutes) > 0  ORDER BY Profit DESC";
                DataSet DtsCust5 = Util.RunQuery(sqlSpQuery4);
                gdvCustomers.DataSource = DtsCust5.Tables[0];
                gdvCustomers.DataBind();

//TOP 5 Vendors
                //RunTop("sp_TopCountry3",broker,1);               
                SqlCommand sqlSpQuery5 = new SqlCommand();
                sqlSpQuery5.CommandType = System.Data.CommandType.Text;
                sqlSpQuery5.CommandText = "SELECT TOP 12 Vendor,ROUND(SUM(T.VendorMinutes),4) as VMinutes ,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Vendor in (SELECT [Vendor] FROM [MECCA2].[dbo].[VBroker] where VendorBroker like '" + broker + "') GROUP BY T.Vendor HAVING SUM(CustMinutes) > 0  ORDER BY Profit DESC";
                DataSet DtsVndr5 = Util.RunQuery(sqlSpQuery5);
                gdVendors.DataSource = DtsVndr5.Tables[0];
                gdVendors.DataBind();
//TOP 5 Countrys
  //              RunTop("sp_TopCountry2", broker, 2);
                
                SqlCommand sqlCtry = new SqlCommand();
                sqlCtry.CommandType = System.Data.CommandType.Text;
                sqlCtry.CommandText = "SELECT TOP 12 Country,ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit FROM MECCA2..trafficlog T WHERE BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND T.Country in (select country from Countrys_by_Manager where CM='" + broker + "')GROUP BY T.Country  HAVING SUM(CustMinutes) > 0  ORDER BY  Profit desc";
                DataSet DtsCtry = Util.RunQuery(sqlCtry);
                gdvCountry.DataSource = DtsCtry;
                gdvCountry.DataBind();

//
                

            }


            //BOARDS ROUTING AND DEPLOYMENT
            //R
            MECCA2TableAdapters.MonitoringAdp adpBoards = new MECCA2TableAdapters.MonitoringAdp();
            gdvRouting.DataSource = adpBoards.GetTop5Routing();
            gdvRouting.DataBind();
            //D
            gdvDeployment.DataSource = adpBoards.GetTop5Deployment();
            gdvDeployment.DataBind();


//            RunCtryCmmsn("sp_countryCommissions");

        }
        catch (Exception ex)
        {
            string message = ex.Message.ToString();
        }

    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch(Exception ex)
        {
            string messagerror = ex.Message.ToString();
        }

        return resultsDataSet;
    }


    private void RunCtryCmmsn(string storeProcedure)
    {
        try
        {
            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = System.Data.CommandType.StoredProcedure;
            sqlSpQuery.CommandText = storeProcedure;
            sqlSpQuery.Parameters.Add("@fromP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();
            sqlSpQuery.Parameters.Add("@toP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();
            sqlSpQuery.CommandTimeout = 160;
            DataSet ResultSetC;
            ResultSetC = RunQuery(sqlSpQuery);


            DataTable tbl = (DataTable)ResultSetC.Tables[0].Copy();//yesterday


            //tbl.Columns.Clear();
            for (int k = 1; k < ResultSetC.Tables.Count; k++)
            {
                tbl.LoadDataRow(ResultSetC.Tables[k].Rows[0].ItemArray, false);
            }

            SqlCommand sqlSpQuery2 = new SqlCommand();
            sqlSpQuery2.CommandType = System.Data.CommandType.StoredProcedure;
            sqlSpQuery2.CommandText = storeProcedure;
            sqlSpQuery2.Parameters.Add("@fromP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-7).ToShortDateString();
            sqlSpQuery2.Parameters.Add("@toP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-7).ToShortDateString();
            sqlSpQuery2.CommandTimeout = 160;
            DataSet ResultSet2;
            ResultSet2 = RunQuery(sqlSpQuery2);
            DataTable tbl2 = (DataTable)ResultSet2.Tables[0].Copy();//1 month ago

            for (int k = 1; k < ResultSetC.Tables.Count; k++)
            {
                tbl2.LoadDataRow(ResultSet2.Tables[k].Rows[0].ItemArray, false);
            }

            for (int z = 0; z < 7; z++)
            {

                decimal gP = Convert.ToDecimal(tbl.Rows[z]["GrossProfit"].ToString());
                decimal gp2 = Convert.ToDecimal(tbl2.Rows[z]["GrossProfit"].ToString());

                if (z == 0)//top Andre
                {
                    lblTCall1.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin1.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross1.Text = tbl.Rows[z]["GrossProfit"].ToString();
                    if (gP > gp2)
                    {

                        imgStatus1.ImageUrl = "./Images/verde.gif";

                    }
                    else
                    {
                        imgStatus1.ImageUrl = "./Images/roja.gif";

                    }
                }
                else if (z == 1)//Hamit
                {
                    lblTCall2.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin2.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross2.Text = tbl.Rows[z]["GrossProfit"].ToString();
                    if (gP > gp2)
                    {

                        imgStatus2.ImageUrl = "./Images/verde.gif";

                    }
                    else
                    {
                        imgStatus2.ImageUrl = "./Images/roja.gif";

                    }
                }
                else if (z == 2)//Iliana
                {
                    lblTCall3.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin3.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross3.Text = tbl.Rows[z]["GrossProfit"].ToString();

                    if (gP > gp2)
                    {

                        imgStatus3.ImageUrl = "./Images/verde.gif";

                    }
                    else
                    {
                        imgStatus3.ImageUrl = "./Images/roja.gif";

                    }
                }
                else if (z == 3)//Jake
                {
                    lblTCall4.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin4.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross4.Text = tbl.Rows[z]["GrossProfit"].ToString();

                    if (gP > gp2)
                    {

                        imgStatus4.ImageUrl = "./Images/verde.gif";

                    }
                    else
                    {
                        imgStatus4.ImageUrl = "./Images/roja.gif";

                    }
                }
                else if (z == 4)//Jimb
                {
                    lblTCall5.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin5.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross5.Text = tbl.Rows[z]["GrossProfit"].ToString();
                    if (gP > gp2)
                    {

                        imgStatus5.ImageUrl = "./Images/verde.gif";

                    }
                    else
                    {
                        imgStatus5.ImageUrl = "./Images/roja.gif";

                    }
                }
                else if (z == 5)//Michelle
                {
                    lblTCall6.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin6.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross6.Text = tbl.Rows[z]["GrossProfit"].ToString();

                    if (gP > gp2)
                    {
                        imgStatus6.ImageUrl = "./Images/verde.gif";

                    }
                    else
                    {
                        imgStatus6.ImageUrl = "./Images/roja.gif";

                    }
                }
                else if (z == 6)//Roberto
                {
                    lblTCall7.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
                    lblTMin7.Text = tbl.Rows[z]["Minutes"].ToString();
                    lblTgross7.Text = tbl.Rows[z]["GrossProfit"].ToString();

                    if (gP > gp2)
                    {
                        imgStatus7.ImageUrl = "./Images/verde.gif";

                    }
                    else
                    {
                        imgStatus7.ImageUrl = "./Images/roja.gif";

                    }
                }

               
                


            }





        }
        catch (Exception ex)
        {
            string errormessage = ex.Message.ToString();
        }

    }

    private void setFlags(float minutesY, int callsY, float profitY, int asrY, float acdY, float totC, float totS,float minutesA, int callsA, float profitA, int asrA,float acdA,float totCA,float totSA)
    {

        if (minutesY < minutesA)                //MINUTOS
        {
            Image4.ImageUrl = "./Images/roja.gif";
            lblMinutesC.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            Image4.ImageUrl = "./Images/verde.gif";
            lblMinutesC.ForeColor = System.Drawing.Color.Green;
        }

        if (callsY < callsA)                     //LLAMADAS
        {
            Image5.ImageUrl = "./Images/roja.gif";
            lblCallsC.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            Image5.ImageUrl = "./Images/verde.gif";
            lblCallsC.ForeColor = System.Drawing.Color.Green;
        }
        if (profitY < profitA)                      //PROFIT
        {
            Image6.ImageUrl = "./Images/roja.gif";
            lblProfitC.ForeColor = System.Drawing.Color.Red;

        }
        else
        {
            Image6.ImageUrl = "./Images/verde.gif";
            lblProfitC.ForeColor = System.Drawing.Color.Green;

        }
        if (asrY < asrA)                            //ASR
        {
            Image7.ImageUrl = "./Images/roja.gif";
            lblASRC.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            Image7.ImageUrl = "./Images/verde.gif";
            lblASRC.ForeColor = System.Drawing.Color.Green;
        }

        if (acdY < acdA)                            //ACD
        {
            Image8.ImageUrl = "./Images/roja.gif";
            lblACDC.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            Image8.ImageUrl = "./Images/verde.gif";
            lblACDC.ForeColor = System.Drawing.Color.Green;
        }

        if (totC < totCA)                           //TotCost
        {
            Image13.ImageUrl = "./Images/roja.gif";
            LabelTotalCostCh.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            Image13.ImageUrl = "./Images/verde.gif";
            LabelTotalCostCh.ForeColor = System.Drawing.Color.Green;
        }

        if (totS < totSA)                           //TotSales
        {
            Image14.ImageUrl = "./Images/roja.gif";
            LabelTotalSalesCh.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            Image14.ImageUrl = "./Images/verde.gif";
            LabelTotalSalesCh.ForeColor = System.Drawing.Color.Green;
        }
    }
    private void RunTop(string storeProcedure,string broker,int flaGrid)
    {
        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = System.Data.CommandType.StoredProcedure;
        sqlSpQuery.CommandText = storeProcedure;
        sqlSpQuery.Parameters.Add("@Broker", SqlDbType.VarChar).Value = broker;
        sqlSpQuery.Parameters.Add("@from", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();
        sqlSpQuery.Parameters.Add("@to", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();
        sqlSpQuery.CommandTimeout = 60;
        DataSet ResultSetC;
        ResultSetC = RunQuery(sqlSpQuery);

        if (flaGrid == 0)
        {
            gdvCustomers.DataSource = ResultSetC;
            gdvCustomers.DataBind();
        }
        else if (flaGrid == 1)
        {
            gdVendors.DataSource = ResultSetC;
            gdVendors.DataBind();

        }
        else
        {
            gdvCountry.DataSource = ResultSetC;
            gdvCountry.DataBind();
        }
        
    }


}
