using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
public partial class CdrsDownloader : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder content = new StringBuilder();
        StringBuilder texto = new StringBuilder();

        string finalContent = string.Empty;

        string user = Session["DirectoUser"].ToString();

        DirectoryInfo dir;

        if (user == "bento" || user == "contacto" || user == "eci" || user == "ergon" || user == "megacal" || user == "mkt911"
            || user == "sto" || user == "targetone" || user == "vcip" || user == "santadmoncall" || user == "santanderdomer")
        {
            //dir = new DirectoryInfo("E:\\Mecca\\RatedCDRS\\week\\santander\\");
            dir = new DirectoryInfo("E:\\Mecca\\RatedCDRS\\week\\cust\\talktel\\");
        }
        else if (user.ToString().Contains("santanagn") || user.ToString().Contains("vcipsant") || user.ToString().Contains("vcip-sant")
                    || user.ToString().Contains("targetonesant") || user.ToString().Contains("megacalsantan") || user.ToString().Contains("mkt911santan")
                    || user.ToString().Contains("bentosant") || user.ToString().Contains("ecisant") || user.ToString().Contains("santanadmoncall")
                    || user.ToString().Contains("santanderdomer"))
        {
            //dir = new DirectoryInfo("E:\\Mecca\\RatedCDRS\\week\\santander\\");
            dir = new DirectoryInfo("E:\\Mecca\\RatedCDRS\\week\\cust\\talktel\\");
        }
        else if (user == "domer")
        {
            dir = new DirectoryInfo("E:\\Mecca\\RatedCDRS\\week\\start\\");
        }
        else
        {
            dir = new DirectoryInfo("E:\\Mecca\\RatedCDRS\\week\\cust\\talktel\\");
        }

        FileSystemInfo[] dirOn = dir.GetFileSystemInfos();


        if (dirOn.Length > 0)
        {

            foreach (FileSystemInfo fi in dirOn)
            {
                if (user == "bento" || user == "contacto" || user == "eci" || user == "ergon" || user == "megacal"
                    || user == "mkt911" || user == "sto" || user == "targetone" || user == "vcip" || user == "santadmoncall"
                    || user == "santanderdomer")
                {
                    //content.Append("<br><a href=http://170.79.125.55/week/santander/" + fi.Name + " target=" + "_blank" + ">" + fi.Name + "</a>");
                    content.Append("<br><a href=/DownloadTalktel.aspx?name=" + fi.Name + " target=" + "_blank" + ">" + fi.Name + "</a>");
                }
                else if (user.ToString().Contains("santanagn") || user.ToString().Contains("vcipsant") || user.ToString().Contains("vcip-sant")
                        || user.ToString().Contains("targetonesant") || user.ToString().Contains("megacalsantan") || user.ToString().Contains("mkt911santan")
                        || user.ToString().Contains("bentosant") || user.ToString().Contains("ecisant") || user.ToString().Contains("santanadmoncall")
                        || user.ToString().Contains("santanderdomer"))
                {
                    content.Append("<br><a href=http://170.79.125.55/week/santander/" + fi.Name + " target=" + "_blank" + ">" + fi.Name + "</a>");
                }
                else if (user == "domer")
                {
                    content.Append("<br><a href=http://170.79.125.55/week/start/" + fi.Name + " target=" + "_blank" + ">" + fi.Name + "</a>");
                }
                else
                {
                    content.Append("<br><a href=/DownloadTalktel.aspx?name=" + fi.Name + " target=" + "_blank" + ">" + fi.Name + "</a>");
                }

                //texto.Append(fi.FullName);
            }

            Label1.Text = readCdrs(content.ToString());
        }
        else
        {
            Label1.Text = "<h3>No Data Available</h3>";
        }


    }

    private string readCdrs(string allContent)
    {
        string newFile = allContent;
        string user = Session["DirectoUser"].ToString();
        string texto = "";

        if (user == "ALLIANCE")
        {
            user = "ALLIANCE-PAYPHONE";
        }
        else if (user == "santander")
        {
            user = "";
        }
        else if (user == "cc-cyberideas")
        {
            user = "cc-cyberideas_";
        }
        else if (user == "sto")
        {
            user = "cc-sto";
        }

        user = user.ToUpper();
        //if (user == "cc-legaxxi") user = user.ToUpper();
        //if (user == "cc-pentafon") user = user.ToUpper();
        //if (user == "cc-grupo-marketing") user = user.ToUpper();
        //if (user == "ccp-bayental-bpo-sac") user = user.ToUpper();
        //if (user == "cc-fin-independencia") user = user.ToUpper();
        //if (user == "cc-sears") user = user.ToUpper();
        //if (user == "cc-vcip") user = user.ToUpper();
        //if (user == "cc-diim") user = user.ToUpper();
        //if (user == "cc-geea") user = user.ToUpper();
        //if (user == "cc-telecontacto") user = user.ToUpper();
        //if (user == "ccp-grupo-mok") user = user.ToUpper();
        //if (user == "cc-infocredit") u ser = user.ToUpper();
        //if (user == "cc-independencia-ags") user = user.ToUpper();
        //if (user == "cc-nal-monte-piedad") user = user.ToUpper();
        //if (user == "cc-digitex") user = user.ToUpper();
        //if (user == "cc-asecon") user = user.ToUpper();
        //if (user == "cc-conn-center") user = user.ToUpper();
        //if (user == "cc-corpamores") user = user.ToUpper();
        //if (user == "cc-admoncall") user = user.ToUpper();
        //if (user == "cc-simerc") user = user.ToUpper();
        //if (user == "cc-simerc-tamaulipas") user = user.ToUpper();
        //if (user == "cc-simerc-michoacan") user = user.ToUpper();
        //if (user == "cc-atelefonica") user = user.ToUpper();
        //if (user == "cc-atelefonica-cemex") user = user.ToUpper();
        //if (user == "cc-impulse-qro") user = user.ToUpper();
        //if (user == "cc-megadirect") user = user.ToUpper();
        //if (user == "cc-digitex-pgnp") user = user.ToUpper();
        //if (user == "cc-eci-contact") user = user.ToUpper();
        //if (user == "cc-digitex-nextel") user = user.ToUpper();
        //if (user == "cc-seleg") user = user.ToUpper();
        //if (user == "cc-digitex-salvador") user = user.ToUpper();
        //if (user == "cc-eci-geeks") user = user.ToUpper();
        //if (user == "cc-eci-hispano") user = user.ToUpper();
        //if (user == "cc-ecitrademarketing") user = user.ToUpper();
        //if (user == "cc-estadist-aplicada") user = user.ToUpper();
        //if (user == "cc-neocenter") user = user.ToUpper();
        //if (user == "cc-call-city") user = user.ToUpper();
        //if (user == "cc-digitex-demo") user = user.ToUpper();
        //if (user == "cc-digitex-multicamp") user = user.ToUpper();
        //if (user == "cc-digitex-segmty") user = user.ToUpper();
        //if (user == "cc-rm-connecting") user = user.ToUpper();
        //if (user == "cc-rm-connecting-ivr") user = user.ToUpper();
        //if (user == "cc-atencion-banamex") user = user.ToUpper();
        //if (user == "cc-atencionmoviecity") user = user.ToUpper();
        //if (user == "cc-atencionmegacable") user = user.ToUpper();
        //if (user == "cc-atelefonica-rys") user = user.ToUpper();
        //if (user == "cc-atelefonica") user = user.ToUpper();
        //if (user == "cc-atelefonica-cemex") user = user.ToUpper();
        //if (user == "cc-wincall") user = user.ToUpper();
        //if (user == "cc-hersycell-r") user = user.ToUpper();
        //if (user == "cc-hersycell-m") user = user.ToUpper();
        //if (user == "cc-hersycell-c") user = user.ToUpper();
        //if (user == "cc-global-contact") user = user.ToUpper();
        //if (user == "cc-connecta") user = user.ToUpper();
        //if (user == "cc-atelefonica-bbva") user = user.ToUpper();
        //if (user == "cc-digitex-ofix") user = user.ToUpper();
        //if (user == "cc-fcb") user = user.ToUpper();
        //if (user == "cc-seproag") user = user.ToUpper();
        //if (user == "cc-imsol") user = user.ToUpper();
        //if (user == "cc-mediaoutside") user = user.ToUpper();
        //if (user == "cc-atelefonica-oni") user = user.ToUpper();
        //if (user == "cc-oneline") user = user.ToUpper();
        //if (user == "cc-cortizo-nuxiba") user = user.ToUpper();
        //if (user == "cc-valdez-valuarte") user = user.ToUpper();
        //if (user == "cc-valkipro") user = user.ToUpper();
        //if (user == "cc-xcenter") user = user.ToUpper();
        //if (user == "cc-icallcenter") user = user.ToUpper();
        //if (user == "cc-ccom") user = user.ToUpper();
        //if (user == "cc-ccomsmnyl") user = user.ToUpper();
        //if (user == "cc-ccomiprotec") user = user.ToUpper();
        //if (user == "cc-ccom-glaass") user = user.ToUpper();
        //if (user == "cc-ccom-blue2") user = user.ToUpper();
        //if (user == "cc-cco-admon") user = user.ToUpper();
        //if (user == "cc-hablatel-did") user = user.ToUpper();
        //if (user == "cc-lizbethpla") user = user.ToUpper();
        //if (user == "cc-bconnectamex97") user = user.ToUpper();
        //if (user == "cc-bconnectsamarilla") user = user.ToUpper();
        //if (user == "cc-bconnect74amexpdb") user = user.ToUpper();
        //if (user == "cc-bconnect") user = user.ToUpper();
        //if (user == "cc-bconnect-banamex") user = user.ToUpper();
        //if (user == "cc-bconnectamexpdb97") user = user.ToUpper();

        //Response.Write(user);
        //Response.End();

        int first = newFile.IndexOf(user);

        if (first > 0)
        {
            int last = newFile.LastIndexOf(user) + 40;

            int lenght = newFile.Length;

            if (user == "bento" || user == "contacto" || user == "eci" || user == "ergon" || user == "megacal"
                || user == "mkt911" || user == "sto" || user == "targetone" || user == "vcip" || user == "santadmoncall"
                || user == "santanderdomer" || user.ToString().Contains("santanagn") || user.ToString().Contains("vcipsant") || user.ToString().Contains("vcip-sant")
                        || user.ToString().Contains("targetonesant") || user.ToString().Contains("megacalsantan") || user.ToString().Contains("mkt911santan")
                        || user.ToString().Contains("bentosant") || user.ToString().Contains("ecisant") || user.ToString().Contains("stosantan")
                        || user.ToString().Contains("santanadmoncall") || user.ToString().Contains("santanderdomer"))
            {

                texto = "<br><a href=http://170.79.125.55/week/santander/" + newFile.Substring(first, last - first);
            }
            //else if ()
            //{
            //    texto = "<br><a href=http://170.79.125.55/week/santander/" + newFile.Substring(first, last - first);
            //}
            else if (user == "DOMER")
            {
                texto = "<br><a href=http://170.79.125.55/week/start/" + newFile.Substring(first, last - first);
            }
            else
            {
                texto = "<br><a href=/DownloadTalktel.aspx?name=" + newFile.Substring(first, last - first);
            }
        }
        else
        {
            texto = "<h3>No Data Available</h3>";
        }


        return texto;

    }

}
