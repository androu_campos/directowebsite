<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="HomeSMS.aspx.cs" Inherits="Home"
    Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script language="javascript" type="text/javascript">
    
     function showImage(imgId,typ) {
                var objImg = document.getElementById(imgId);
                if (objImg) {
                    if (typ == "1")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_1b.gif";
                   else if (typ == "2")
                       objImg.src = "Images/botonesMeccaHome/tabmeccahome_1a.gif";
                   else if (typ == "3")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_2b.gif";
                   else if (typ == "4")
                       objImg.src = "Images/botonesMeccaHome/tabmeccahome_2a.gif";  
                   else if (typ == "5")
                       objImg.src = "Images/botonesMeccaHome/tabmeccahome_3b.gif";  
                   else if (typ == "6")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_3a.gif";
                    else if (typ == "7")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_4b.png";
                    else if (typ == "8")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_4a.png";
                    else if (typ == "9")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_5b.png";
                    else if (typ == "10")
                        objImg.src = "Images/botonesMeccaHome/tabmeccahome_5a.png";
               }
         }
         
    </script>
    <table align="left" id="tblMain" cellpadding="6" style="height: 666PX; width: 100%;
        position: absolute; top: 140px; left: 220px;">
        <tr valign="bottom" height="20px">
            <td align="left" valign="bottom" colspan="3" style="height: 20px">
                <asp:ImageButton ID="img1" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_1a.gif"
                    OnClick="buttonhome_click" />
                <asp:ImageButton ID="img2" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_2a.gif"
                    OnClick="buttonicshome_click"  />
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_3a.gif"
                    OnClick="buttoncchome_click" />
                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_4b.png"
                    OnClick="buttonsmshome_click" ImageAlign="Bottom"  Enabled="false" />
                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="Images/botonesMeccaHome/tabmeccahome_5a.png"
                    OnClick="buttonaahome_click" /><br />
                <asp:Image Width="1060px" Height="2px" ID="tabline" runat="server" ImageUrl="~/Images/tabmeccahome2_3.gif" />
            </td>
        </tr>
        <tr>            
            <td colspan="3">
                <%--<asp:Image ID="Image6" runat="server" ImageUrl="~/Images/lineMenu.gif" />--%>
                &nbsp;
            </td>  
        </tr>
        <tr>
            <td rowspan="4" style="height: 850px;" valign="top" width="250">
                <img border="0" src="Images/meccasummary.gif" height="24">
                <table border="0" width="264" id="tblSummary" cellspacing="0" cellpadding="0">
                    <tr height="18px">
                        <td width="72" height="20">
                        </td>
                        <td width="71" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">&nbsp; &nbsp;&nbsp; Latest</font></b>
                        </td>
                        <td width="113" colspan="2" height="20" valign="middle">
                            <b><font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial;
                                margin-bottom: 2px; vertical-align: middle;">&nbsp; &nbsp; &nbsp;&nbsp; Change</font></b></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72" align="left">
                            <font color="#004D88" style="font-size: 8pt; margin-left: 10px; margin-top: 5px;
                                margin-bottom: 5px; color: #004d88; font-family: Arial">Total</font>
                        </td >
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp;
                                <asp:Label ID="lblTotalY" runat="server"></asp:Label>
                            </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblTotalC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image10" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72" align="left">
                            <font color="#004D88" style="font-size: 8pt; margin-left: 10px; margin-top: 5px;
                                margin-bottom: 5px; color: #004d88; font-family: Arial">Delivered</font>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp;
                                <asp:Label ID="lblDeliveredY" runat="server"></asp:Label>
                            </font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblDeliveredC" runat="server" Text="Label"></asp:Label></font>
                            &nbsp;</td>
                        <td width="19" align="center">
                            <asp:Image ID="Image4" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <img border="0" src="Images/line.gif" width="264" height="1"></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <font color="#004D88" style="font-size: 8pt; color: #004d88; font-family: Arial">Sales</font>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                            <font color="#666666" style="font-size: 8pt; margin-top: 5px; margin-bottom: 5px;
                                color: #666666; font-family: Arial">&nbsp; &nbsp;
                                <asp:Label ID="lblSalesY" runat="server"></asp:Label></font></td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#FF3300" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblSalesC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image5" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>
                    <tr>
                        <td width="72">
                            <p align="left" style="margin-left: 10px; margin-top: 5px; margin-bottom: 5px">
                                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Profit"></asp:Label>
                        </td>
                        <td width="71">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#666666" style="font-size: 8pt; color: #666666; font-family: Arial">
                                    <asp:Label ID="lblProfitY" runat="server"></asp:Label></font>
                        </td>
                        <td width="85">
                            <p align="center" style="margin-top: 5px; margin-bottom: 5px">
                                <font color="#339933" style="font-size: 8pt; color: #339933; font-family: Arial">
                                    <asp:Label ID="lblProfitC" runat="server" Text="Label"></asp:Label></font>
                        </td>
                        <td width="19" align="center">
                            <asp:Image ID="Image6" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="248" colspan="4">
                            <font color="#004D88">
                                <img border="0" src="Images/line.gif" width="264" height="1"></font></td>
                    </tr>                   
                   
                </table>
                <br />
                <p>
                    </p>
                    <p>
                        <table border="0" width="300px" id="Table1" cellspacing="0" cellpadding="0" align="left">
                             <tr>
                                <td valign="top" style="height: 38px">
                                   <%-- <table border="0" width="300px" id="table2" cellspacing="0" cellpadding="0">
                                        <tr>
                                             <td align="left" bgcolor="#d5e3f0" colspan="3" height="20">
                                                <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/topcountries.gif" /></td>
                                        </tr>
                                        <tr>
                                            <td width="300px" colspan="3" align="left" height="24" valign="bottom">
                                                <table style="width: 300px">
                                                    <tr>
                                                        <td style="width: 150px" valign="bottom">
                                                            <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Countries"></asp:Label></td>
                                                        <td style="width: 50px" align="right" valign="bottom">
                                                            <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Minutes"></asp:Label></td>
                                                        <td align="right" valign="bottom" style="width: 50px">
                                                            <asp:Label ID="lblHeaderProfit3" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                                Text="Profit"></asp:Label></td>
                                                        <td align="center" valign="bottom" style="width: 30px">
                                                            <asp:Label ID="Label18" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                                Text="ASR"></asp:Label>
                                                        </td>
                                                        <td align="center" valign="bottom" style="width: 30px">
                                                            <asp:Label ID="Label19" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                                Text="ABR"></asp:Label>
                                                        </td>
                                                        <td align="center" valign="bottom" style="width: 30px">
                                                            <asp:Label ID="Label20" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                                Text="ACD"></asp:Label>
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                                <img border="0" src="Images/line.gif" width="300px" height="1"></td>
                                        </tr>
                                        <tr>
                                            <td id="Td1" width="248" colspan="3" align="center" valign="top" style="height: 14px">--%>
                                                <asp:Label ID="lblCouN" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label><%--
                                                </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" rowspan="5">
                                                <asp:GridView ID="gdvCountry" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                                    CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                                    HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                                    <Columns>
                                                        <asp:BoundField DataField="Country" ItemStyle-Width="130px">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Minutes" DataFormatString="{0:0,0}" ItemStyle-Width="50px"
                                                            HtmlEncode="false">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Profit" DataFormatString="{0:C0}" ItemStyle-Width="50px"
                                                            HtmlEncode="false">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ASR" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                            ItemStyle-Width="30px">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ABR" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                            ItemStyle-Width="30px">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ACD" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                            ItemStyle-Width="30px">
                                                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>--%>
                                </td>
                             </tr>
                             <tr>
                                <td>&nbsp;</td>
                             </tr>
                             
                             <tr>
                                <td>                                   
                                    <%--<asp:HyperLink ID="hypNational" runat="server" CssClass="labelBlue8" NavigateUrl="http://national.directo.com">Mecca NATIONAL</asp:HyperLink>                                --%>
                                </td>
                             </tr>
                        </table>     
                    
            </td>
            
            
            <td rowspan="4" style="height: 850px; width: 250px;" valign="top" align="left">
                <table border="0" width="250px" id="tblCenter" cellspacing="0" cellpadding="0" align="left">
                    <tr>
                        <td valign="top" style="height: 351px; width: 250px;">
                            <table border="0" width="250px" id="table15" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" bgcolor="#d5e3f0" colspan="3" height="20">
                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/EN-Customers.png" /></td>
                                </tr>
                                <tr>
                                    <td width="400px" colspan="3" align="left" height="24" valign="bottom">
                                        <table style="width: 350px">
                                            <tr>
                                                <td style="width: 190px" valign="bottom">
                                                    <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Customer"></asp:Label></td>
                                                <td align="center"valign="bottom" style="width: 50px">
                                                    <asp:Label ID="Label15" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="Sales"></asp:Label>
                                                </td>    
                                                <td style="width: 50px" align="center" valign="bottom">
                                                    <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="SMS"></asp:Label></td>
                                                <td align="center"valign="bottom" style="width: 50px">
                                                    <asp:Label ID="lblHeaderProfit1" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="Profit"></asp:Label>
                                                </td>                                                
                                            </tr>
                                        </table>
                                        <img border="0" src="Images/line.gif" width="355px" height="1"></td>
                                </tr>
                                <tr>
                                <td id="Td3" width="248" colspan="3" align="center" valign="top" style="height: 14px">
                                                <asp:Label ID="lblCusN" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="5" rowspan="5">
                                        <asp:GridView RowStyle-Height="18px" ID="gdvCustomers" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Customer" HtmlEncode="False" InsertVisible="False" ShowHeader="False">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="190px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Sales" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:C0}"
                                                    ItemStyle-Width="50px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Delivered" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="50px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:C0}"
                                                    ItemStyle-Width="50px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>                                                
                                            </Columns>
                                            <RowStyle Height="18px" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>                                    
                                </tr>                                
                                <tr>
                                    <td id="Td2" width="400px" colspan="3" align="center" valign="top" style="height: 14px">
                                        
                                        <asp:Label ID="lblVenN" runat="server" ForeColor="Red" Text="Not available"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="3" rowspan="5" width="100%" style="height: 100px">
                                       <%-- <asp:GridView ID="gdVendors" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Vendor">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="150px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Cost" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:C0}"
                                                    ItemStyle-Width="50px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Delivered" DataFormatString="{0:0,0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" DataFormatString="{0:C0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>                                                
                                            </Columns>
                                        </asp:GridView>--%>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="4" style="height: 850px; width: 250px;" valign="top" align="left">
                <table border="0" width="250px" id="Table3" cellspacing="0" cellpadding="0" align="left">                
                    <tr>
                        <td valign="top" style="height: 351px; width: 250px;">
                            <table border="0" width="250px" id="table4" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" bgcolor="#d5e3f0" colspan="3" height="20">
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/EN-Vendors.png" /></td>
                                </tr>
                                <tr>
                                    <td width="400px" colspan="3" align="left" height="24" valign="bottom">
                                        <table style="width: 350px">
                                            <tr>
                                                <td style="width: 190px" valign="bottom">
                                                    <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Vendor"></asp:Label></td>                                                
                                                <td style="width: 50px" align="center" valign="bottom">
                                                    <asp:Label ID="Label10" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Cost"></asp:Label></td>
                                                <td align="center"valign="bottom" style="width: 50px">
                                                    <asp:Label ID="Label12" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="SMS"></asp:Label>
                                                </td>
                                                <td align="center" valign="bottom" style="width: 50px">
                                                    <asp:Label ID="Label13" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                                        Text="Profit"></asp:Label>
                                                </td>                                                
                                            </tr>
                                        </table>
                                        <img border="0" src="Images/line.gif" width="355px" height="1"><br /></td>
                                </tr>
                                <tr>
                                    <td id="Td" width="248" colspan="3" align="center" valign="top" style="height: 14px">
                                                <asp:Label ID="Label21" runat="server" Text="Not available" Visible="False" ForeColor="Red"></asp:Label></td>                                               
                                    
                                </tr>
                                            
                                <tr>
                                    <td colspan="5" rowspan="5">
                                        <%--<asp:GridView ID="gdvRegions" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Region" ItemStyle-Width="150px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Minutes" DataFormatString="{0:0,0}" ItemStyle-Width="50px"
                                                    HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" DataFormatString="{0:C0}" ItemStyle-Width="50px"
                                                    HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ASR" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ABR" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ACD" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:0,0}"
                                                    ItemStyle-Width="30px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>--%>
                                        <asp:GridView ID="gdVendors" RowStyle-Height="18px" runat="server" AutoGenerateColumns="False"
                                            CssClass="labelSteelBlue" Font-Bold="False" Font-Names="Courier New" ShowHeader="False"
                                            HorizontalAlign="Left" BorderStyle="Solid" BorderWidth="0px" ForeColor="#666666">
                                            <Columns>
                                                <asp:BoundField DataField="Vendor">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="190px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Cost" ShowHeader="False" HtmlEncode="false" DataFormatString="{0:C0}"
                                                    ItemStyle-Width="50px">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Delivered" DataFormatString="{0:0,0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Profit" DataFormatString="{0:C0}" HtmlEncode="false">
                                                    <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Right" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:BoundField>                                                
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td align="left" valign="top">
                                        &nbsp;<br />
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                                            SelectCommand="SELECT Country, City, Average_Cost, Expected_ASR, Yesterday_ACD, Avail_Capacity, Service_Level, Comments, Vendor_Billing, Date_Updated, Minimum FROM Push_List">
                                        </asp:SqlDataSource>
                               
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            
              
              
              </tr>
              
              
              
    
    <tr>
        <td align="left" style="height: 38px">
            <br />
        </td>
    </tr>
    <tr>
        <td style="height: 27px">
            <p style="margin-bottom: 2px">
        </td>
    </tr>
    <tr>
        <td colspan="3" width="250">
            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/footer.gif" /></td>
    </tr>
    <tr>
        <td bgcolor="" width="250">
            <p style="margin-left: 20px; margin-top: 10px; margin-bottom: 10px;" align="left">
                
                    <br>
                    <table>
                        <tr>
                            <td align="right" colspan="3">
                                <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                    Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                            <td style="width: 100px">
                                <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                    Width="146px" Visible="True" /></td>
                        </tr>
                    </table>
                
        </td>
        <td style="width: 168px">
        </td>
        <td style="width: 239px">
        </td>
    </tr>
    </table>
    
</asp:Content>

