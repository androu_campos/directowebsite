using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Net;
using System.Xml;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Redirect("http://drpmecca.directo.com/MeccaDirecto");
        try
        {
            img1.Attributes.Add("onmouseover", "showImage('" + img1.ClientID + "','1');");
            img1.Attributes.Add("onmouseout", "showImage('" + img1.ClientID + "','2');");
            img2.Attributes.Add("onmouseover", "showImage('" + img2.ClientID + "','3');");
            img2.Attributes.Add("onmouseout", "showImage('" + img2.ClientID + "','4');");
            ImageButton1.Attributes.Add("onmouseover", "showImage('" + ImageButton1.ClientID + "','5');");
            ImageButton1.Attributes.Add("onmouseout", "showImage('" + ImageButton1.ClientID + "','6');");
            ImageButton2.Attributes.Add("onmouseover", "showImage('" + ImageButton2.ClientID + "','7');");
            ImageButton2.Attributes.Add("onmouseout", "showImage('" + ImageButton2.ClientID + "','8');");
            ImageButton3.Attributes.Add("onmouseover", "showImage('" + ImageButton3.ClientID + "','9');");
            ImageButton3.Attributes.Add("onmouseout", "showImage('" + ImageButton3.ClientID + "','10');");

            /*                                         MECCA  SUMMARY                                       */
            lblCusN.Visible = false;
            lblVenN.Visible = false;
            lblCouN.Visible = false;
            string yesterday = System.DateTime.Now.AddDays(-1).ToShortDateString();
            string ayesterday = System.DateTime.Now.AddDays(-2).ToShortDateString();

            Dictionary<string, string> dataBefore = new Dictionary<string, string>();
            Dictionary<string, string> dataLastWeek = new Dictionary<string, string>();

            dataBefore = getSummary();
            dataLastWeek = getLastWeekSummary();

            string ctotalsms = dataBefore["total_sms"].ToString();
            int ctotalsmsint = Convert.ToInt32(ctotalsms);
            string ytotalsms = dataLastWeek["total_sms"].ToString();
            int ytotalsmsint = Convert.ToInt32(ytotalsms);

            string cdeliveredsms = dataBefore["delivered_sms"].ToString();
            int cdeliveredsmsint = Convert.ToInt32(cdeliveredsms);
            string ydeliveredsms = dataLastWeek["delivered_sms"].ToString();
            int ydeliveredsmsint = Convert.ToInt32(ydeliveredsms);

            string csalessms = dataBefore["sales"].ToString();
            int csalessmsint = Convert.ToInt32(csalessms);
            string ysalessms = dataLastWeek["sales"].ToString();
            int ysalessmsint = Convert.ToInt32(ysalessms);

            string cprofitsms = dataBefore["profit"].ToString();
            int cprofitsmsint = Convert.ToInt32(cprofitsms);
            string yprofitsms = dataLastWeek["profit"].ToString();
            int yprofitsmsint = Convert.ToInt32(yprofitsms);

            lblTotalY.Text = Util.getNumberFormat(ctotalsms, 0);
            lblDeliveredY.Text = Util.getNumberFormat(cdeliveredsms, 0);
            lblSalesY.Text = Util.getNumberFormat(csalessms, 0);
            lblProfitY.Text = Util.getNumberFormat(cprofitsms, 0);

            lblTotalC.Text = Util.getNumberFormat((ctotalsmsint - ytotalsmsint).ToString(),0);
            lblDeliveredC.Text = Util.getNumberFormat((cdeliveredsmsint - ydeliveredsmsint).ToString(),0);
            lblSalesC.Text = Util.getNumberFormat((csalessmsint - ysalessmsint).ToString(),0);
            lblProfitC.Text = Util.getNumberFormat((cprofitsmsint - yprofitsmsint).ToString(),0);

            if ( ctotalsmsint < ytotalsmsint)                     
            {
                Image10.ImageUrl = "./Images/roja.gif";
                lblTotalC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image10.ImageUrl = "./Images/verde.gif";
                lblTotalC.ForeColor = System.Drawing.Color.Green;
            }

            if (cdeliveredsmsint < ydeliveredsmsint)
            {
                Image4.ImageUrl = "./Images/roja.gif";
                lblDeliveredC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image4.ImageUrl = "./Images/verde.gif";
                lblDeliveredC.ForeColor = System.Drawing.Color.Green;
            }

            if (csalessmsint < ysalessmsint)
            {
                Image5.ImageUrl = "./Images/roja.gif";
                lblSalesC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image5.ImageUrl = "./Images/verde.gif";
                lblSalesC.ForeColor = System.Drawing.Color.Green;
            }

            if (cprofitsmsint < yprofitsmsint)
            {
                Image6.ImageUrl = "./Images/roja.gif";
                lblProfitC.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                Image6.ImageUrl = "./Images/verde.gif";
                lblProfitC.ForeColor = System.Drawing.Color.Green;
            }

            getTopCustomers();
            getTopVendor();
            

                //string Calls = tblS.Rows[0]["Calls"].ToString();
                //string Attempts = tblS.Rows[0]["Attempts"].ToString();
                //lblSalesY.Text = Util.getNumberFormat(Calls, 0);
                //lblTotalY.Text = Util.getNumberFormat(Attempts, 0);
                //string Profit = tblS.Rows[0]["Profit"].ToString();
                //lblProfitY.Text = Util.getNumberFormat(Profit, 2);
                //lblASRY.Text = tblS.Rows[0]["ASR"].ToString() + ".00%";
                //lblABRY.Text = tblS.Rows[0]["ABR"].ToString() + ".00%";
                //lblACDY.Text = tblS.Rows[0]["CACD"].ToString();

                //lblTotSalesY.Text = Util.getNumberFormat(tblS.Rows[0]["TotalSales"].ToString(), 2);


                //float minutesY = (float)Convert.ToSingle(tblS.Rows[0]["CMinutes"].ToString());
                //int callsY = Convert.ToInt32(tblS.Rows[0]["Calls"].ToString());
                //int attemptsY = Convert.ToInt32(tblS.Rows[0]["Attempts"].ToString());
                //float profitY = (float)Convert.ToSingle(tblS.Rows[0]["Profit"].ToString());
                //int asrY = Convert.ToInt32(tblS.Rows[0]["ASR"].ToString());
                //int abrY = Convert.ToInt32(tblS.Rows[0]["ABR"].ToString());
                //float acdY = (float)Convert.ToSingle(tblS.Rows[0]["CACD"].ToString());
                //float totSalesY = (float)Convert.ToSingle(tblS.Rows[0]["TotalSales"].ToString());
                ////Get Dates of PRE-Yesterday


                ////                lastId2 = 885;

                ////***                Summary.SummaryDataTable tblA = adpS.GetDataById(lastId2);
                //Summary.SummaryCCDataTable tblA = adpS.GetDataById(lastId2);

                //float minutesA = (float)Convert.ToSingle(tblA.Rows[0]["CMinutes"].ToString());
                //int callsA = Convert.ToInt32(tblA.Rows[0]["Calls"].ToString());
                //int attemptsA = Convert.ToInt32(tblA.Rows[0]["Attempts"].ToString());
                //float profitA = (float)Convert.ToSingle(tblA.Rows[0]["Profit"].ToString());
                //int asrA = Convert.ToInt32(tblA.Rows[0]["ASR"].ToString());
                //int abrA = Convert.ToInt32(tblA.Rows[0]["ABR"].ToString());
                //float acdA = (float)Convert.ToSingle(tblA.Rows[0]["CACD"].ToString());
                //float totSalesA = (float)Convert.ToSingle(tblA.Rows[0]["TotalSales"].ToString());
                ////column change
                //string MinutesC = Convert.ToString(minutesY - minutesA).Replace("-", "");
                //int isfloat = Convert.ToInt32(MinutesC.IndexOf("."));
                //if (isfloat < 0)
                //{
                //    lblDeliveredC.Text = Util.getNumberFormat(MinutesC, 0);
                //}
                //else
                //{
                //    //MinutesC = Util.getNumberFormat(MinutesC, 0);
                //    //MinutesC = MinutesC.Substring(0, MinutesC.Length - 3);
                //    //lblDeliveredC.Text = CMinutes;
                //    lblDeliveredC.Text = Util.getNumberFormat(MinutesC, 1);
                //}

                //lblDeliveredC.Text = lblDeliveredC.Text.Replace("-", "");
                //lblSalesC.Text = Util.getNumberFormat(Convert.ToString((callsA - callsY)).Replace("-", ""), 0);
                //lblTotalC.Text = Util.getNumberFormat(Convert.ToString((attemptsA - attemptsY)).Replace("-", ""), 0);
                //lblProfitC.Text = Util.getNumberFormat(Convert.ToString((profitA - profitY)).Replace("-", ""), 2);
                //lblASRC.Text = Convert.ToString((asrA - asrY)).Replace("-", "") + ".00%";
                //lblABRC.Text = Convert.ToString((abrA - abrY)).Replace("-", "") + ".00%";
                //lblACDC.Text = lblACDC.Text = Convert.ToString((acdA - acdY)).Replace("-", "");

                //string sales = Convert.ToString(totSalesY - totSalesA).Replace("-", "");
                //isfloat = Convert.ToInt32(sales.IndexOf("."));
                //if (isfloat < 0)
                //{
                //    lblTotSalesC.Text = Util.getNumberFormat(Convert.ToString((totSalesA - totSalesY)).Replace("-", ""), 0);
                //}
                //else
                //{
                //    lblTotSalesC.Text = Util.getNumberFormat(Convert.ToString((totSalesA - totSalesY)).Replace("-", ""), 2);
                //}
                //// end column change

                //if (minutesY < minutesA)                //MINUTOS
                //{
                //    Image4.ImageUrl = "./Images/roja.gif";
                //    lblDeliveredC.ForeColor = System.Drawing.Color.Red;
                //}
                //else
                //{
                //    Image4.ImageUrl = "./Images/verde.gif";
                //    lblDeliveredC.ForeColor = System.Drawing.Color.Green;
                //}

                //if (callsY < callsA)                     //LLAMADAS
                //{
                //    Image5.ImageUrl = "./Images/roja.gif";
                //    lblSalesC.ForeColor = System.Drawing.Color.Red;
                //}
                //else
                //{
                //    Image5.ImageUrl = "./Images/verde.gif";
                //    lblSalesC.ForeColor = System.Drawing.Color.Green;
                //}
                //if (profitY < profitA)                      //PROFIT
                //{
                //    Image6.ImageUrl = "./Images/roja.gif";
                //    lblProfitC.ForeColor = System.Drawing.Color.Red;

                //}
                //else
                //{
                //    Image6.ImageUrl = "./Images/verde.gif";
                //    lblProfitC.ForeColor = System.Drawing.Color.Green;

                //}
                //if (asrY < asrA)                            //ASR
                //{
                //    Image7.ImageUrl = "./Images/roja.gif";
                //    lblASRC.ForeColor = System.Drawing.Color.Red;
                //}
                //else
                //{
                //    Image7.ImageUrl = "./Images/verde.gif";
                //    lblASRC.ForeColor = System.Drawing.Color.Green;
                //}

                //if (acdY < acdA)                           //ACD
                //{
                //    Image8.ImageUrl = "./Images/roja.gif";
                //    lblACDC.ForeColor = System.Drawing.Color.Red;
                //}
                //else
                //{
                //    Image8.ImageUrl = "./Images/verde.gif";
                //    lblACDC.ForeColor = System.Drawing.Color.Green;
                //}

                //if (attemptsY < attemptsA)                     //Attempts
                //{
                //    Image10.ImageUrl = "./Images/roja.gif";
                //    lblTotalC.ForeColor = System.Drawing.Color.Red;
                //}
                //else
                //{
                //    Image10.ImageUrl = "./Images/verde.gif";
                //    lblTotalC.ForeColor = System.Drawing.Color.Green;
                //}

                //if (totSalesY < totSalesA)                 //TOTAL SALES
                //{
                //    Image13.ImageUrl = "./Images/roja.gif";
                //    lblTotSalesC.ForeColor = System.Drawing.Color.Red;
                //}
                //else
                //{
                //    Image13.ImageUrl = "./Images/verde.gif";
                //    lblTotSalesC.ForeColor = System.Drawing.Color.Green;
                //}

                //if (abrY < abrA)                     //ABR
                //{
                //    Image15.ImageUrl = "./Images/roja.gif";
                //    lblABRC.ForeColor = System.Drawing.Color.Red;
                //}
                //else
                //{
                //    Image15.ImageUrl = "./Images/verde.gif";
                //    lblABRC.ForeColor = System.Drawing.Color.Green;
                //}

            //    //lblBillingRunning.Visible = true;
            //    //lblBillingRunning.Text = "Billing not Runing";

            //    //MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter adpUN = new MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter();
            //    //MyDataSetTableAdapters.unratedMinutes_DataTable tblUN = adpUN.GetData();
            //    //if (tblUN.Rows.Count > 0)//There are unrated minutes?
            //    //{
            //    //    HyperLink1.Text = "There are unrated minutes in MECCA!";
            //    //    HyperLink1.ForeColor = System.Drawing.Color.Red;

            //    //}
            //    //else
            //    //{
            //    //    HyperLink1.Text = "All minutes are rated in MECCA!";
            //    //}


            //    //MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter adpU2 = new MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter();
            //    //MyDataSetTableAdapters.UnknownIDs2DataTable tblU2 = adpU2.GetData();

            //    //if (tblU2.Rows.Count > 0)
            //    //{
            //    //    HyperLink2.Text = "There are unmatched IDs in MECCA!";
            //    //    HyperLink2.ForeColor = System.Drawing.Color.Red;
            //    //}
            //    //else
            //    //{
            //    //    HyperLink2.Text = "All IDs are matched";
            //    //}



            //    string yest = DateTime.Today.AddDays(-1).ToShortDateString();
            //    MyDataSetTableAdaptersTableAdapters.trafficTopAdp adpT = new MyDataSetTableAdaptersTableAdapters.trafficTopAdp();
            //    gdvCustomers.DataSource = adpT.GetDataCC();
            //    gdvCustomers.DataBind();

            //    MyDataSetTableAdaptersTableAdapters.trafficVtopAdp adpV = new MyDataSetTableAdaptersTableAdapters.trafficVtopAdp();
            //    gdVendors.DataSource = adpV.GetDataCC();
            //    gdVendors.DataBind();

            //    MyDataSetTableAdaptersTableAdapters.trafficTopCAdp adpC = new MyDataSetTableAdaptersTableAdapters.trafficTopCAdp();
            //    gdvCountry.DataSource = adpC.GetDataCC(Convert.ToDateTime(yest));
            //    gdvCountry.DataBind();

            //    MyDataSetTableAdaptersTableAdapters.trafficTopRegionsTableAdapter adpR = new MyDataSetTableAdaptersTableAdapters.trafficTopRegionsTableAdapter();
            //    gdvRegions.DataSource = adpR.GetDataCC(Convert.ToDateTime(yest));
            //    gdvRegions.DataBind();

            //    MyDataSetTableAdaptersTableAdapters.UnknownIDs1TableAdapter adpUn = new MyDataSetTableAdaptersTableAdapters.UnknownIDs1TableAdapter();
            //    MyDataSetTableAdapters.UnknownIDs1DataTable tblUn = adpUn.GetData();



            


            //if ((Session["Rol"].ToString() == "NOC" || Session["Rol"].ToString() == "NOC National") && Session["Rol"].ToString() != "Comercial" && Session["Rol"].ToString() != string.Empty)
            //{

            //    lblHeaderProfit1.Visible = false;
            //    lblHeaderProfit2.Visible = false;
            //    lblHeaderProfit3.Visible = false;


            //    TableItemStyle estilo = new TableItemStyle();
            //    estilo.CssClass = "white";

            //    gdvCountry.Columns[2].Visible = false;
            //    lblHeaderProfit1.Text = "Minutes";
            //    lblHeaderProfit1.Visible = true;
            //    Label7.Visible = false;

            //    gdVendors.Columns[2].Visible = false;
            //    lblHeaderProfit2.Text = "Minutes";
            //    lblHeaderProfit2.Visible = true;
            //    Label2.Visible = false;

            //    gdvCustomers.Columns[2].Visible = false;
            //    lblHeaderProfit3.Text = "Minutes";
            //    lblHeaderProfit3.Visible = true;

            //    Label4.Visible = false;


            //    lblProfitY.Visible = false;
            //    lblProfitC.Visible = false;
            //    Image6.Visible = false;
            //    Label5.Visible = false;

            //    Label11.Visible = false;
            //    lblTotSalesY.Visible = false;
            //    lblTotSalesC.Visible = false;
            //    Image13.Visible = false;

            //}
            //else
            //{

            //}
            ////BOARDS ROUTING AND DEPLOYMENT
            ////R
            ////MECCA2TableAdapters.MonitoringAdp adpBoards = new MECCA2TableAdapters.MonitoringAdp();

            ////gdvRouting.DataSource = adpBoards.GetTop5Routing();
            ////gdvRouting.DataBind();
            ////D
            ////gdvDeployment.DataSource = adpBoards.GetTop5Deployment();
            ////gdvDeployment.DataBind();

            ////
            //if (Session["DirectoUser"].ToString() == "djassan" || Session["DirectoUser"].ToString() == "hvalle")
            //{
            //    hypNational.Visible = true;
            //}
            //else
            //{
            //    hypNational.Visible = false;
            //}

        }
        catch (Exception ex)
        {
            string message = ex.Message.ToString();
        }

    }
    protected void buttoncchome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeCC.aspx");
    }

    protected void buttonaahome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeAA.aspx");
    }

    protected void buttonicshome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeR.aspx");
    }

    protected void buttonhome_click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }
    protected void buttonsmshome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeSMS.aspx");
    }


    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            string messagerror = ex.Message.ToString();
        }

        return resultsDataSet;
    }

    protected Dictionary<string,string> getSummary()
    {
        HttpWebRequest request = WebRequest.Create("http://sms.directo.com:1366/dayBefoResume") as HttpWebRequest;
        HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(response.GetResponseStream());

        XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/tabla/fila/columna");
        List<ColumnaDayBefore> daybefore = new List<ColumnaDayBefore>();
        Dictionary<string, string> dataDayBefore = new Dictionary<string, string>();

        foreach (XmlNode node in nodes)
        {
            
            dataDayBefore.Add("total_sms", node.SelectSingleNode("total_sms").InnerText);
            dataDayBefore.Add("delivered_sms", node.SelectSingleNode("entregados_sms").InnerText);
            dataDayBefore.Add("profit", node.SelectSingleNode("profit").InnerText);
            dataDayBefore.Add("sales", node.SelectSingleNode("sales").InnerText);
        }

        return dataDayBefore;
    }

    protected Dictionary<string,string> getLastWeekSummary()
    {
        HttpWebRequest request = WebRequest.Create("http://sms.directo.com:1366/dayBefoResumeWeekCompare") as HttpWebRequest;
        HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(response.GetResponseStream());

        XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/tabla/fila/columna");
        List<ColumnaDayBefore> daybefore = new List<ColumnaDayBefore>();
        Dictionary<string, string> dataLastWeek = new Dictionary<string, string>();

        foreach (XmlNode node in nodes)
        {

            dataLastWeek.Add("total_sms", node.SelectSingleNode("total_sms").InnerText);
            dataLastWeek.Add("delivered_sms", node.SelectSingleNode("entregados_sms").InnerText);
            dataLastWeek.Add("profit", node.SelectSingleNode("profit").InnerText);
            dataLastWeek.Add("sales", node.SelectSingleNode("sales").InnerText);
        }

        return dataLastWeek;
    }

    protected void getTopCustomers()
    {
        HttpWebRequest request = WebRequest.Create("http://sms.directo.com:1366/dayBefoResumeClient") as HttpWebRequest;
        HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(response.GetResponseStream());
                
        //using (DataSet ds = new DataSet())
        //{
        //    ds.ReadXml(xmlReader);
        //    xmlReader.Close();
        //    gdvCustomers.DataSource = ds;
        //    gdvCustomers.DataBind();
        //}

        XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/tabla/fila/columna");

        DataTable table1 = new DataTable("customers");
        table1.Columns.Add("Customer");
        table1.Columns.Add("Sales").DataType = System.Type.GetType("System.Double");
        table1.Columns.Add("Delivered").DataType = System.Type.GetType("System.Double");
        table1.Columns.Add("Profit").DataType = System.Type.GetType("System.Double");

        List<ColumnDayBeforeTopCustomers> topCustomers = new List<ColumnDayBeforeTopCustomers>();

        foreach (XmlNode node in nodes)
        {

            table1.Rows.Add(node.SelectSingleNode("client").InnerText.ToUpper(),
                            node.SelectSingleNode("sales").InnerText,
                            node.SelectSingleNode("entregados_sms").InnerText, 
                            node.SelectSingleNode("profit").InnerText);                        
        }

        DataSet set = new DataSet();
        set.Tables.Add(table1);

        gdvCustomers.DataSource = set.Tables[0];
        gdvCustomers.DataBind();
        gdvCustomers.Visible = true;

    }

    protected void getTopVendor()
    {
        HttpWebRequest request = WebRequest.Create("http://sms.directo.com:1366/dayBefoResumeProvider") as HttpWebRequest;
        HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(response.GetResponseStream());        

        XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/tabla/fila/columna");

        DataTable table1 = new DataTable("vendors");
        table1.Columns.Add("Vendor");
        table1.Columns.Add("Cost").DataType = System.Type.GetType("System.Double");
        table1.Columns.Add("Delivered").DataType = System.Type.GetType("System.Double");
        table1.Columns.Add("Profit").DataType = System.Type.GetType("System.Double");        

        foreach (XmlNode node in nodes)
        {

            table1.Rows.Add(node.SelectSingleNode("provider").InnerText.ToUpper(),
                            node.SelectSingleNode("sales").InnerText,
                            node.SelectSingleNode("entregados_sms").InnerText,
                            node.SelectSingleNode("profit").InnerText);
        }

        DataSet set = new DataSet();
        set.Tables.Add(table1);

        gdVendors.DataSource = set.Tables[0];
        gdVendors.DataBind();
        gdVendors.Visible = true;

    }

    class ColumnaDayBefore
    {
        public string totalSms;
        public string deliveredSms;
        public string sales;
        public string profit;
    }

    public class ColumnDayBeforeTopCustomers
    {
        public string customer;
        public string totalSms;
        public string deliveredSms;
        public string sales;
        public string profit;
    }
   
}
