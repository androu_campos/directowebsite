using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class qbprofit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            cdrdbDatasetTableAdapters.ProviderIP1TableAdapter adp = new cdrdbDatasetTableAdapters.ProviderIP1TableAdapter();
            cdrdbDataset.ProviderIP1DataTable tbl = adp.GetProviderName();

            int c = tbl.Rows.Count;

            for (int i = 0; i < c; i++)
            {
                dpdCustomer.Items.Add(tbl.Rows[i]["ProviderName"].ToString());
                dpdVendor.Items.Add(tbl.Rows[i]["ProviderName"].ToString());
            }

            cdrdbDatasetTableAdapters.RegionsTableAdapter adpR = new cdrdbDatasetTableAdapters.RegionsTableAdapter();
            cdrdbDataset.RegionsDataTable tblC = adpR.GetCountry();
            cdrdbDataset.RegionsDataTable tblR = adpR.GetRegion();

            int cR = tblR.Rows.Count;

            for (int j = 0; j < cR; j++)
            {
                dpdCountry.Items.Add(tblC.Rows[j]["Country"].ToString());
                dpdRegions.Items.Add(tblR.Rows[j]["Region"].ToString());
            }
        }

        

    }
    protected void Button1_Click(object sender, EventArgs e)
    {/*
        string qry = string.Empty;
        string select = string.Empty;
        string where = string.Empty;
        string groupby = string.Empty;
        string having = string.Empty;
        string table = string.Empty;
        string customer, vendor, country, regions;
        
        
        //SELECT 

        if (dpdCustomer.SelectedValue.ToString() != "**DONT SHOW**")
        {
            select = select + dpdCustomer.SelectedValue.ToString() + ",";
            customer = dpdCustomer.SelectedValue.ToString();
            where = where + "Customer like '" + customer + "' AND";
            groupby = groupby + "Customer,";
        }

        if (dpdVendor.SelectedValue.ToString() != "**DONT SHOW**")
        {
            select = select + dpdVendor.SelectedValue.ToString() + ",";
            vendor = dpdVendor.SelectedValue.ToString();
            where = where + "Vendor like '" + vendor + "' AND";
            groupby = groupby + "Vendor,";
        }
        if (dpdCountry.SelectedValue.ToString() != "**DONT SHOW**")
        {
            select = select + dpdCountry.SelectedValue.ToString() + ",";
            country = dpdCountry.SelectedValue.ToString();
            where = where + "Country like '" + country + "' AND";
            groupby = groupby + "Country,";
        }
        if (dpdRegions.SelectedValue.ToString() != "**DONT SHOW**")
        {
            select = select + dpdRegions.SelectedValue.ToString() + ",";
            regions = dpdRegions.SelectedValue.ToString();
            where = where + "Region like '" + regions + "' AND";
            groupby = groupby + "Regions,";
        }
        if (dpdTypeReport.SelectedValue.ToString() != "**DONT SHOW**")
        {
            select = select + dpdTypeReport.SelectedValue.ToString() + ",";
        }


        select = select + "sum(TotalMinutes) as  Minutes ,round(sum(Profit),1)  as  [ Profit ], sum(PrevMinutes) as  PreviousMinutes,round(sum(PrevProfit),1) as  PreviousProfit, round(sum(0+Profit)-sum(0+PrevProfit),1) as Delta ";
                        
        //from...Table
        if (dpdTypeReport.SelectedValue.ToString() == "1")
        { table = "ProfitTrendsDaily"; }
        else { table = "ProfitTrendsWeekly"; }

        qry = select + " from " + table;

        //where
        int i = where.LastIndexOf("AND");
        where = where.Substring(0, i);        
        qry = qry + where;

        //group by
        int g = groupby.LastIndexOf(",");
        groupby = groupby.Substring(0, g);
        qry = qry + groupby;

        //having
        having = " having (sum(TotalMinutes)+sum(PrevMinutes)) >0  order by Delta ";
        qry = qry + having;
        */

    }
    protected void Button2_Click(object sender, EventArgs e)
    {

        //
    }
}
