using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterAccess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblAfter.Text = string.Empty;
        lblDelete.Visible = false;
        txtName.Focus();
        if (!Page.IsPostBack)
        {
            DropDownList2.DataSource = SqlDataSource1;
            DropDownList2.DataBind();

            DropDownList1.DataSource = SqlDataSource1;
            DropDownList1.DataBind();
         
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string name = DropDownList2.SelectedValue.ToString();

        AccessDatasetTableAdapters.PlanBreakOutAdp adp = new AccessDatasetTableAdapters.PlanBreakOutAdp();
        AccessDataset.PlanBreakOutDataTable tbl = adp.GetDataByName(name);

        txtNewName.Text = tbl.Rows[0]["Name"].ToString();
        txtNewRate.Text = tbl.Rows[0]["Rate"].ToString();
    }
    protected void cmdInsert_Click(object sender, EventArgs e)
    {
        try
        {
            lblDelete.Visible = false;

            string name = txtName.Text;
            decimal rate = Convert.ToDecimal(txtRate.Text);
            string rateS = txtRate.Text;

            int lenght = rateS.Length;
            int point = rateS.IndexOf(".", 0);
            rateS = rateS.Substring(point + 1, lenght-point-1);
            if (rateS.Length > 4)
            {
                lblAfter.Text = "The max length of decimals is 4";
            }
            else
            {
                AccessDatasetTableAdapters.PlanBreakOutAdp adp = new AccessDatasetTableAdapters.PlanBreakOutAdp();
                adp.Insert(name, rate);
                lblAfter.Text = "Group "+ name + " created successfully";
                txtName.Text = "";
                txtRate.Text = "";

/////
                DropDownList2.DataSource = SqlDistinctNames;
                DropDownList2.DataBind();
                DropDownList1.DataSource = SqlDataSource1;
                DropDownList1.DataBind();

            }

            
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            lblAfter.Text = " The group " + txtName.Text + " can't be created, the group already exists";
        }
    }
    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try
        {

            string  s = DropDownList1.SelectedValue.ToString();
            AccessDatasetTableAdapters.PlanBreakOutAdp adp = new AccessDatasetTableAdapters.PlanBreakOutAdp();
            AccessDataset.PlanBreakOutDataTable tbl = adp.GetDataByName(s);
            int idPlan = Convert.ToInt32(tbl.Rows[0]["IDPlan"]);

            //have SIM assigned?
            AccessDatasetTableAdapters.SIMInfoTableAdp adpSim = new AccessDatasetTableAdapters.SIMInfoTableAdp();
            AccessDataset.SIMInfoDataTable tblSim = adpSim.GetDataByIdplan(idPlan);
            if (tblSim.Rows.Count > 0)
            {
                // Tiene al menos un SIM asignado, NO se puede borrar
                lblDelete.Text = " This group can't be deleted";
                lblDelete.Visible = true;
            }
            else
            {
                adp.Delete(idPlan);
                lblDelete.Text = " Group " + s + " deleted";
                lblDelete.Visible = true;
                DropDownList1.DataSource = SqlDataSource1;
                DropDownList1.DataBind();

                DropDownList2.DataSource = SqlDistinctNames;
                DropDownList2.DataBind();

                Label4.Visible = false;
            }

        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }
    }
    protected void dpdGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        AccessDatasetTableAdapters.PlanBreakOutAdp adp = new AccessDatasetTableAdapters.PlanBreakOutAdp();
        string name = DropDownList2.SelectedValue.ToString();
        AccessDataset.PlanBreakOutDataTable tbl = adp.GetDataByName(name);
        txtNewName.Text =  tbl.Rows[0]["Name"].ToString();
        txtNewRate.Text = tbl.Rows[0]["Rate"].ToString();                
    }
    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            string name = DropDownList2.SelectedValue.ToString();
            AccessDatasetTableAdapters.PlanBreakOutAdp adp = new AccessDatasetTableAdapters.PlanBreakOutAdp();
            AccessDataset.PlanBreakOutDataTable tbl = adp.GetDataByName(name);
            int id = Convert.ToInt32(tbl.Rows[0]["IDPlan"].ToString());
            string rateS = txtNewRate.Text;
            int lenght = rateS.Length;
            int point = rateS.IndexOf(".", 0);
            rateS = rateS.Substring(point + 1, lenght - point - 1);
            if (rateS.Length > 4)
            {
                Label4.Text = "The max length of decimals is 4";
                Label4.Visible = true;
            }
            else
            {
             
                adp.Update(txtNewName.Text, Convert.ToDecimal(txtNewRate.Text), id);
                Label4.Text = "Group " +  name +  " Updated ";
                Label4.Visible = true;
                txtNewName.Text = string.Empty;
                txtNewRate.Text = string.Empty;
                lblDelete.Visible = false;
                
            }
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }
    }
    protected void SqlDistinctNames_Load(object sender, EventArgs e)
    {

    }

    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        AccessDatasetTableAdapters.PlanBreakOutAdp adp = new AccessDatasetTableAdapters.PlanBreakOutAdp();
        string name = DropDownList2.SelectedValue.ToString();
        AccessDataset.PlanBreakOutDataTable tbl = adp.GetDataByName(name);
        txtNewName.Text = tbl.Rows[0]["Name"].ToString();
        txtNewRate.Text = tbl.Rows[0]["Rate"].ToString();
    }
}
