using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class HistoryProfit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            //Chart1.Visible = false;
            //Chart2.Visible = false;
        }
        else
        {
            //Chart1.Visible = true;
            //Chart2.Visible = true;
        }            
    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {
        string provider = dpdProvider.SelectedValue.ToString();
        DataTable myTbl;
        if (provider == "**ALL**")
        {
            provider = "%";
        }                  
            myTbl = Util.RunQueryByStmnt("Select * from mecca2..ProfitR where BillingDate between '" + txtFrom.Text + "' and '" + txtTo.Text + "' and Vendor like '" + provider + "'" ).Tables[0];                            
        Session["tblProfit"] = myTbl;
        gdvProfit.DataSource = myTbl;
        gdvProfit.DataBind();                
    }
    protected void gdvProfit_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvProfit.PageIndex = e.NewPageIndex;
        gdvProfit.DataSource = (DataTable)Session["tblProfit"];
        gdvProfit.DataBind();
    }
}
