using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

public partial class AddDID : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
            string sql = string.Empty;

        

            sql = "SELECT distinct Country from Regions order by Country ASC";

            SqlCommand SqlC = new SqlCommand();
            SqlC.CommandText = sql;

            DataSet ds = RunQuery(SqlC);

            if (ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    dpdCountry.Items.Add(ds.Tables[0].Rows[i][0].ToString());
                }
            }

            sql = "Select distinct StateAbb from ics.dbo.Regions UNION Select '' as StateAbb ";
            
            SqlC.CommandText = sql;

            ds = RunQuery(SqlC);

            if (ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    dpdstprefix.Items.Add(ds.Tables[0].Rows[i][0].ToString());
                }
            }
        
    }


    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

        return resultsDataSet;
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string sql = string.Empty;
        string did = string.Empty;
        string country = string.Empty;
        string stPrefix = string.Empty;
        string state = string.Empty;
        string provider = string.Empty;
        string framework = string.Empty;
        string city = string.Empty;

        did = txtdid.Text;
        country = dpdCountry.SelectedValue;
        city = txtCity.Text;
        stPrefix = dpdstprefix.SelectedValue;
        state = txtstate.Text;
        provider = dpdProvider.SelectedValue;
        framework = dpdFramework.SelectedValue;

        city = city.ToUpper();

        sql = "INSERT INTO ICS.dbo.sourceDID VALUES ('" 
            + did + "','" + country + "','" + city + "','" + stPrefix + "','" + state + "','"
            + provider + "','" + framework + "')";


        SqlCommand sqlC = new SqlCommand();
        sqlC.CommandText = sql;
        DataSet ds = RunQuery(sqlC);

        Response.Redirect("AddDid.aspx");        

    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddDid.aspx");
    }

    protected void dpdStPrefix_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtstate.Text = "";
        string sql = string.Empty;
        SqlCommand SqlC = new SqlCommand();
        DataSet ds;
        DataSet dsaux;
        if (dpdstprefix.SelectedValue != "")
        {
            

            sql = "if exists (SELECT 1 from ics.dbo.sourceDID where did = '" + txtdid.Text +  "') ";
            sql += "BEGIN ";
            sql += " SELECT 1 ";
            sql += " END ";
            sql += "ELSE ";
            sql += "BEGIN ";
            sql += " SELECT 0 ";
            sql += " END ";
            SqlC.CommandText = sql;
            ds = RunQuery(SqlC);

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows[0][0].ToString() == "1")
                    Response.Redirect("AddDid.aspx");
                else
                {
                    sql = "select [State] from ics.dbo.Regions where StateAbb = '" + dpdstprefix.SelectedValue + "' ";

                    SqlC.CommandText = sql;
                    dsaux = RunQuery(SqlC);

                    if (dsaux.Tables.Count > 0)
                    {
                        txtstate.Text = dsaux.Tables[0].Rows[0][0].ToString();
                    }

                    dsaux.Dispose();
                }
            }

            ds.Dispose();            
        }
                
    }

}
