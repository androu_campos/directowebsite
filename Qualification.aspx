<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    Theme="Theme1" AutoEventWireup="true" CodeFile="Qualification.aspx.cs" Inherits="BlackList"
    Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td style="height: 22px">
                <asp:Label ID="lblagency" runat="server" Text="Agency: " CssClass="labelBlue8"></asp:Label>
                <asp:DropDownList ID="ddlAgency" runat="server" CssClass="labelBlue8" DataSourceID="sqldsAgency"
                    DataTextField="agencia" DataValueField="agencia">
                </asp:DropDownList>
                <asp:Button ID="btnSearch" runat="Server" Text="Search" CssClass="boton" />
                <asp:Label ID="lblError" runat="server" CssClass="lblNotFound" Visible="false"></asp:Label>
                <asp:Button ID="btnExpCSV" runat="server" CssClass="boton" Text="Export to CSV" OnClick="btnExpCSV_Click" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 103px" valign="top">
                <asp:GridView ID="gvQualif" runat="server" Font-Names="Arial" Font-Size="8pt" DataSourceID="sqldsQualification"
                    AutoGenerateColumns="False" AllowPaging="True" PageSize="50" EnableModelValidation="True"
                    Visible="true">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" VerticalAlign="middle" HorizontalAlign="Center" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="numero" HeaderText="Number" ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="fecha" HeaderText="Date" ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="agente" HeaderText="Agent" ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="callcenter" HeaderText="CallCenter" ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="duracion_llamada" HeaderText="Duration" ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="resultado_gestion" HeaderText="Result" ItemStyle-Wrap="false" />
                        <asp:BoundField DataField="producto" HeaderText="Product" ItemStyle-Wrap="false" />
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="gvSearch" runat="server" Font-Names="Arial" Font-Size="8pt" DataSourceID="sqlsQualifSearch"
                    AutoGenerateColumns="False" AllowPaging="True" PageSize="50" EnableModelValidation="True"
                    Visible="False">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" VerticalAlign="Middle" HorizontalAlign="Center" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="numero" HeaderText="Number" ReadOnly="True" SortExpression="numero" />
                        <asp:BoundField DataField="fecha" HeaderText="Date" SortExpression="fecha" />
                        <asp:BoundField DataField="agente" HeaderText="Agent" SortExpression="agente" />
                        <asp:BoundField DataField="callcenter" HeaderText="CallCenter" SortExpression="callcenter" />
                        <asp:BoundField DataField="duracion_llamada" HeaderText="Duration" SortExpression="duracion_llamada" />
                        <asp:BoundField DataField="Resultado_Gestion" HeaderText="Result" ReadOnly="True"
                            SortExpression="Resultado_Gestion" />
                        <asp:BoundField DataField="producto" HeaderText="Product" SortExpression="producto" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sqlsQualifSearch" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListSantander %>"
                    ProviderName="<%$ ConnectionStrings:BlackListSantander.ProviderName %>" SelectCommand="select right(numero,10) numero, fecha, agente, callcenter, duracion_llamada, &#13;&#10;case when resultado_gestion = 1 then 'Venta' &#13;&#10;        when resultado_gestion = 2 then 'Rechazo Producto'&#13;&#10;        when resultado_gestion = 3 then 'Pendiente' end as Resultado_Gestion, &#13;&#10;producto &#13;&#10;from tipificacion;"
                    FilterExpression="agencia = '{0}'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlAgency" Name="agencia" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sqldsQualification" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListSantander %>"
                    ProviderName="<%$ ConnectionStrings:BlackListSantander.ProviderName %>" SelectCommand="select right(numero,10) numero, fecha, agente, callcenter, duracion_llamada, &#13;&#10;case when resultado_gestion = 1 then 'Venta' &#13;&#10;        when resultado_gestion = 2 then 'Rechazo Producto'&#13;&#10;        when resultado_gestion = 3 then 'Pendiente' end as Resultado_Gestion, &#13;&#10;producto &#13;&#10;from tipificacion;">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sqldsAgency" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListSantander %>"
                    ProviderName="<%$ ConnectionStrings:BlackListSantander.ProviderName %>" SelectCommand="select distinct agencia from tipificacion;"
                    CancelSelectOnNullParameter="false"></asp:SqlDataSource>
            </td>
        </tr>
    </table>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
</asp:Content>
