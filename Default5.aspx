<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Default5.aspx.cs" Inherits="Default5" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="tblQuery">
        <tr>
            <td style="width: 100px">
                Customer</td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCustomer" runat="server">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                Vendor</td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdVendor" runat="server">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                OriginationIP</td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdOriginationIp" runat="server">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                TerminationIP</td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdTermination" runat="server">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                Country</td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCountry" runat="server">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                Region</td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdRegion" runat="server">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                Type</td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdType" runat="server">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                Class</td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdClass" runat="server">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                Source MSW</td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdSource" runat="server">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Button ID="cmdSearch" runat="server" OnClick="cmdSearch_Click" Text="Button" /></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
</asp:Content>

