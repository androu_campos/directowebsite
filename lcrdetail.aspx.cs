using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RKLib.ExportData;
using System.Data.SqlClient;

public partial class lcrdetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["Country"].ToString() == "Mexico")
        {
            lblNothingV.Visible = true;
            cmdXport.Visible = false;
        }
        else if (Request.QueryString["Country"].ToString() != null && Request.QueryString["Region"].ToString() != null && Request.QueryString["CCountry"].ToString() != null)
        {

            string country = Request.QueryString["Country"].ToString();
            string region = Request.QueryString["Region"].ToString();
            string ccountry = Request.QueryString["CCountry"].ToString();

            SqlConnection SqlConn1;
            SqlCommand SqlCommand1;

            SqlConn1 = new SqlConnection();
            SqlCommand1 = new SqlCommand();

            //MyDataSetTableAdaptersTableAdapters.trafficlogAdp adp = new MyDataSetTableAdaptersTableAdapters.trafficlogAdp();
            //MyDataSetTableAdapters.trafficlogDataTable tblA = adp.GetVolumeby(country, region);

            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";

            SqlCommand1 = new SqlCommand("SELECT Vendor, cost, SUM(CustMinutes) AS TotalMinutes, SUM(BillableCalls) AS BillableCalls FROM         trafficlog WHERE     (CustMinutes > 0) AND (BillingDate = LEFT(DATEADD(day, - 1, GETDATE()), 11)) AND (Country = '"+ country +"') AND (Region LIKE '"+ region +"') GROUP BY Vendor, cost ORDER BY TotalMinutes DESC", SqlConn1);
            
            SqlConn1.Open();
            SqlDataReader myReader1 = SqlCommand1.ExecuteReader();

            if (myReadeer1.Rows.Count > 0)
            {
                grdVolumes.DataSource = myReader1;
                grdVolumes.DataBind();
                Session["TableVolumes"] = tblA;
                lblNothingV.Visible = false;
                cmdXport.Visible = true;
            }
            else
            {
                lblNothingV.Visible = true;
                cmdXport.Visible = false;
            }

            

            MyDataSetTableAdaptersTableAdapters.LCRBASEAdp adp1 = new MyDataSetTableAdaptersTableAdapters.LCRBASEAdp();
            
            if (ccountry == "N")
            {//By Region
                grdLCR.DataSource = adp1.GetLCRby(country, region);
                MyDataSetTableAdapters.LCRBASEDataTable tbl = adp1.GetLCRby(country, region);

                if (tbl.Rows.Count > 0)
                {
                    Session["TableLCR"] = tbl;
                    grdLCR.DataBind();

                    lblNothingLCR.Visible = false;
                    cmdExport2.Visible = true;
                }
                else
                {
                    lblNothingLCR.Visible = true;
                    cmdExport2.Visible = false;
                }

            }
            else
            { //By Country
                grdLCR.DataSource = adp1.GetLCRby(country, "%");
                MyDataSetTableAdapters.LCRBASEDataTable tbl = adp1.GetLCRby(country, "%");
                Session["TableLCR"] = tbl;
                grdLCR.DataBind();


            }

        }


    }
    protected void grdVolumes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdLCR.PageIndex = e.NewPageIndex;
        grdLCR.DataBind();
    }
    protected void grdLCR_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdLCR.PageIndex = e.NewPageIndex;
        grdLCR.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {

            DataTable dtEmployee = ((DataTable)Session["TableLCR"]).Copy();
            // Export all the details to EXCEL
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "LCR" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }

    
    protected void cmdXport_Click(object sender, EventArgs e)
    {
       
        try
        {

            DataTable dtEmployee = ((DataTable)Session["TableVolumes"]).Copy();
            // Export all the details to EXCEL
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "VolumesLCR" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

    }
}
