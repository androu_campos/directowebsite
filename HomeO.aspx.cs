using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;
using Dundas.Charting.WebControl;

public partial class HomeO : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (Session["Rol"].ToString() == "Country Managers" && Session["DirectoUser"].ToString() != "vicente" && Session["DirectoUser"].ToString() != "mmurray" && Session["DirectoUser"].ToString() != "mmurray" && Session["DirectoUser"].ToString() != "vicente")
            {
                Response.Redirect("CtryHome.aspx", false);
            }

            img2.Attributes.Add("onmouseover", "showImage('" + img2.ClientID + "','1');");
            img2.Attributes.Add("onmouseout", "showImage('" + img2.ClientID + "','2');");



            //MECCA2TableAdapters.Special_ConsiderationsAdp adpSc = new MECCA2TableAdapters.Special_ConsiderationsAdp();
            //MECCA2.Special_ConsiderationsDataTable tblSc = adpSc.GetData();
            //StringBuilder comments = new StringBuilder();

            //    Label12.Text = tblSc.Rows[1][2].ToString();
            //    Label13.Text = tblSc.Rows[1][3].ToString();
            //    Label14.Text = tblSc.Rows[1][4].ToString();                           
            //    Label8.Text = tblSc.Rows[0][2].ToString();
            //    Label9.Text = tblSc.Rows[0][3].ToString();
            //    Label10.Text = tblSc.Rows[0][4].ToString();



            MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adp = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
            MyDataSetTableAdapters.Billing_StatusDataTable tbl = adp.GetData();

            SummaryTableAdapters.QrysAdp adpL = new SummaryTableAdapters.QrysAdp();
            int lastId = Convert.ToInt32(adpL.LastIdSummarySegments());
            int lastId1 = 0, lastId2 = 0;
            /**/
            //TODAY
            
            
            SummaryTableAdapters.SummaryOTableAdapter adpD = new SummaryTableAdapters.SummaryOTableAdapter();
            Summary.SummaryODataTable tblD = adpD.GetDataByDate(Convert.ToDateTime(DateTime.Now.ToString()));//Today
            
            if (tblD.Rows.Count > 0)
            {
                lastId1 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            else
            {
                tblD = adpD.GetDataByDate(Convert.ToDateTime(DateTime.Now.AddDays(-1).ToShortDateString()));//Today
                lastId1 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            /**/

            /**/
            //7 DAY BEFORE

            tblD = adpD.GetDataByDate(Convert.ToDateTime(DateTime.Now.AddDays(-8).ToShortDateString()));//Today
            if (tblD.Rows.Count > 0)
            {
                lastId2 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            else if (tblD.Rows.Count != 0)
            {
                tblD = adpD.GetDataByDate(Convert.ToDateTime(DateTime.Now.AddDays(-8).ToShortDateString()));//Today
                lastId2 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
            }
            /**/
            //            lastId2 = 885;

            if (tbl.Rows.Count > 0)//Billing running
            {

                //lblBillingRunning.Visible = true;
                //lblBillingRunning.Text = "Billing Running !!!";
                //lblBillingRunning.ForeColor = System.Drawing.Color.Red;

                //HyperLink1.Visible = false;
                //HyperLink2.Visible = false;

                string yesterday = System.DateTime.Now.AddDays(-1).ToShortDateString();
                string ayesterday = System.DateTime.Now.AddDays(-2).ToShortDateString();
                //  Get Dates of Yesterday//Today

                //SummaryTableAdapters.SummaryAdp adpS = new SummaryTableAdapters.SummaryAdp();
                //SummaryTableAdapters.Summary_Adp adpS = new SummaryTableAdapters.Summary_Adp();
                //Summary.Summary_DataTable tblS = adpS.GetDataById(lastId1);

                SummaryTableAdapters.SummaryOTableAdapter adpS = new SummaryTableAdapters.SummaryOTableAdapter();
                Summary.SummaryODataTable tblS = adpS.GetDataById(lastId1);                

                //Summary.SummaryDataTable tblS = adpS.GetDataById(lastId1);

                string CMinutes = tblS.Rows[0]["CMinutes"].ToString();
                int isFloatCminutes = Convert.ToInt32(CMinutes.IndexOf("."));
                if (isFloatCminutes < 0)
                {
                    lblMinutesY.Text = Util.getNumberFormat(CMinutes, 0);
                }
                else
                {
                    CMinutes = Util.getNumberFormat(CMinutes, 1);
                    CMinutes = CMinutes.Substring(0, CMinutes.Length - 3);
                    lblMinutesY.Text = CMinutes;
                    lblMinutesY.Text = Util.getNumberFormat(CMinutes, 1);
                }




                string Calls = tblS.Rows[0]["Calls"].ToString();
                lblCallsY.Text = Util.getNumberFormat(Calls, 0);

                string Profit = tblS.Rows[0]["Profit"].ToString();
                lblProfitY.Text = Util.getNumberFormat(Profit, 2);

                lblTotSalesY.Text = Util.getNumberFormat(tblS.Rows[0]["TotalSales"].ToString(), 2);



                lblASRY.Text = tblS.Rows[0]["ASR"].ToString() + ".00%";
                lblACDY.Text = tblS.Rows[0]["CACD"].ToString();


                float minutesY = (float)Convert.ToSingle(tblS.Rows[0]["CMinutes"].ToString());
                int callsY = Convert.ToInt32(tblS.Rows[0]["Calls"].ToString());
                float profitY = (float)Convert.ToSingle(tblS.Rows[0]["Profit"].ToString());
                int asrY = Convert.ToInt32(tblS.Rows[0]["ASR"].ToString());
                float acdY = (float)Convert.ToSingle(tblS.Rows[0]["CACD"].ToString());
                float totSalesY = (float)Convert.ToSingle(tblS.Rows[0]["TotalSales"].ToString());

                //Get Dates of PRE-Yesterday
                //*               Summary.SummaryDataTable tblA = adpS.GetDataById(lastId2);
                Summary.SummaryODataTable tblA = adpS.GetDataById(lastId2);

                float minutesA = (float)Convert.ToSingle(tblA.Rows[0]["CMinutes"].ToString());
                int callsA = Convert.ToInt32(tblA.Rows[0]["Calls"].ToString());
                float profitA = (float)Convert.ToSingle(tblA.Rows[0]["Profit"].ToString());
                int asrA = Convert.ToInt32(tblA.Rows[0]["ASR"].ToString());
                float acdA = (float)Convert.ToSingle(tblA.Rows[0]["CACD"].ToString());
                float totSalesA = (float)Convert.ToSingle(tblA.Rows[0]["TotalSales"].ToString());

                //column change
                //
                string MinutesC = Convert.ToString(minutesY - minutesA).Replace("-", "");
                int isfloat = Convert.ToInt32(MinutesC.IndexOf("."));
                if (isfloat < 0)
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 0);
                }
                else
                {
                    lblMinutesC.Text = Util.getNumberFormat(MinutesC, 1);
                }

                lblMinutesC.Text = lblMinutesC.Text.Replace("-", "");
                lblCallsC.Text = Util.getNumberFormat(Convert.ToString((callsA - callsY)).Replace("-", ""), 0);
                lblProfitC.Text = Util.getNumberFormat(Convert.ToString((profitA - profitY)).Replace("-", ""), 2);
                lblASRC.Text = Convert.ToString((asrA - asrY)).Replace("-", "") + ".00%";
                lblACDC.Text = lblACDC.Text = Convert.ToString((acdA - acdY)).Replace("-", "");

                string sales = Convert.ToString(totSalesY - totSalesA).Replace("-", "");
                isfloat = Convert.ToInt32(sales.IndexOf("."));
                if (isfloat < 0)
                {
                    lblTotSalesC.Text = Util.getNumberFormat(Convert.ToString((totSalesA - totSalesY)).Replace("-", ""), 0);
                }
                else
                {
                    lblTotSalesC.Text = Util.getNumberFormat(Convert.ToString((totSalesA - totSalesY)).Replace("-", ""), 2);
                }


                // end column change

                if (minutesY < minutesA)                //MINUTOS
                {
                    Image4.ImageUrl = "./Images/roja.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image4.ImageUrl = "./Images/verde.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Green;
                }

                if (callsY < callsA)                     //CALLS
                {
                    Image5.ImageUrl = "./Images/roja.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image5.ImageUrl = "./Images/verde.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Green;
                }
                //if (profitY < profitA)                      //PROFIT
                //{
                //    Image6.ImageUrl = "./Images/roja.gif";
                //    lblProfitC.ForeColor = System.Drawing.Color.Red;

                //}
                //else
                //{
                //    Image6.ImageUrl = "./Images/verde.gif";
                //    lblProfitC.ForeColor = System.Drawing.Color.Green;

                //}
                if (asrY < asrA)                            //ASR
                {
                    Image7.ImageUrl = "./Images/roja.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image7.ImageUrl = "./Images/verde.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Green;
                }

                if (acdY < acdA)                           //ACD
                {
                    Image8.ImageUrl = "./Images/roja.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image8.ImageUrl = "./Images/verde.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Green;
                }

                if (totSalesY < totSalesA)                 //TOTAL SALES
                {
                    Image13.ImageUrl = "./Images/roja.gif";
                    lblTotSalesC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image13.ImageUrl = "./Images/verde.gif";
                    lblTotSalesC.ForeColor = System.Drawing.Color.Green;
                }

                lblCusN.Visible = true;
                lblVenN.Visible = true;
                lblCouN.Visible = true;
                //Label15.Visible = true;
                Label18.Visible = true;

                //**//
                if (Session["Rol"].ToString() == "NOC" || Session["Rol"].ToString() == "NOC National")
                {

                    lblHeaderProfit1.Visible = false;
                    lblHeaderProfit2.Visible = false;
                    lblHeaderProfit3.Visible = false;


                    TableItemStyle estilo = new TableItemStyle();
                    estilo.CssClass = "white";

                    gdvCountry.Columns[2].Visible = false;
                    lblHeaderProfit1.Text = "Minutes";
                    lblHeaderProfit1.Visible = true;
                    Label7.Visible = false;

                    gdVendors.Columns[2].Visible = false;
                    lblHeaderProfit2.Text = "Minutes";
                    lblHeaderProfit2.Visible = true;
                    Label2.Visible = false;

                    gdvCustomers.Columns[2].Visible = false;
                    lblHeaderProfit3.Text = "Minutes";
                    lblHeaderProfit3.Visible = true;
                    Label4.Visible = false;

                    //gvTrafficType.Columns[2].Visible = false;
                    //Label9.Text = "Minutes";
                    //Label9.Visible = true;
                    //Label9.Visible = false;

                    gvTopStates.Columns[2].Visible = false;
                    Label17.Text = "Minutes";
                    Label17.Visible = true;
                    Label19.Visible = false;

                    //lblProfitY.Visible = false;
                    //lblProfitC.Visible = false;
                    //Image6.Visible = false;
                    //Label5.Visible = false;

                    //Label11.Visible = false;
                    //lblTotSalesY.Visible = false;
                    //lblTotSalesC.Visible = false;
                    //Image13.Visible = false;
                }

                Chart1.Visible = false;
                Chart2.Visible = false;

            }//FIN DEL BILLING RUNNING
            else
            {

                //if (Session["DirectoUser"].ToString() == "hvalle" || Session["DirectoUser"].ToString() == "jai")
                //{
                //    Label16.Visible = false;
                //    imgCtryMan5.Visible = true;
                //    pnlCtryMan.Visible = true;    /****************/

                //    //REPORT CTRY MANAGERS
                //    RunCtryCmmsn("sp_countryCommissionsH");

                //}
                //else
                //{
                //    Label16.Visible = false;
                //    imgCtryMan5.Visible = false;
                //    pnlCtryMan.Visible = false;
                //}

                /*                                         MECCA  SUMMARY                                       */
                lblCusN.Visible = false;
                lblVenN.Visible = false;
                lblCouN.Visible = false;
                string yesterday = System.DateTime.Now.AddDays(-1).ToShortDateString();
                string ayesterday = System.DateTime.Now.AddDays(-2).ToShortDateString();
                //  Get Dates of Yesterday
                //SummaryTableAdapters.SummarySegmentsTableAdapter adpS = new SummaryTableAdapters.SummarySegmentsTableAdapter();
                //Summary.SummarySegmentsDataTable tblS = adpS.GetDataById(lastId1);

                SummaryTableAdapters.SummaryOTableAdapter adpS = new SummaryTableAdapters.SummaryOTableAdapter();
                Summary.SummaryODataTable tblS = adpS.GetDataById(lastId1);

                string CMinutes = tblS.Rows[0]["CMinutes"].ToString();
                int isFloatCminutes = Convert.ToInt32(CMinutes.IndexOf("."));
                if (isFloatCminutes < 0)
                {
                    lblMinutesY.Text = Util.getNumberFormat(CMinutes, 0);
                }
                else
                {
                    CMinutes = Util.getNumberFormat(CMinutes, 1);

                    CMinutes = CMinutes.Substring(0, (CMinutes.Length - (CMinutes.Length - CMinutes.IndexOf("."))));

                    lblMinutesY.Text = CMinutes;
                    //lblMinutesY.Text = Util.getNumberFormat(CMinutes, 1);
                }

                string Calls = tblS.Rows[0]["Calls"].ToString();
                lblCallsY.Text = Util.getNumberFormat(Calls, 0);
                string Profit = tblS.Rows[0]["Profit"].ToString();
                lblProfitY.Text = Util.getNumberFormat(Profit, 2);
                lblASRY.Text = tblS.Rows[0]["ASR"].ToString() + ".00%";
                lblACDY.Text = tblS.Rows[0]["CACD"].ToString();

                lblTotSalesY.Text = Util.getNumberFormat(tblS.Rows[0]["TotalSales"].ToString(), 2);


                float minutesY = (float)Convert.ToSingle(tblS.Rows[0]["CMinutes"].ToString());
                int callsY = Convert.ToInt32(tblS.Rows[0]["Calls"].ToString());
                float profitY = (float)Convert.ToSingle(tblS.Rows[0]["Profit"].ToString());
                int asrY = Convert.ToInt32(tblS.Rows[0]["ASR"].ToString());
                float acdY = (float)Convert.ToSingle(tblS.Rows[0]["CACD"].ToString());
                float totSalesY = (float)Convert.ToSingle(tblS.Rows[0]["TotalSales"].ToString());
                //Get Dates of PRE-Yesterday


                //                lastId2 = 885;

                //***                Summary.SummaryDataTable tblA = adpS.GetDataById(lastId2);


                Summary.SummaryODataTable tblA = adpS.GetDataById(lastId2);
                float minutesA = 0;
                int callsA = 0;
                int asrA = 0;
                float acdA = 0;
                string MinutesC = "0.00";
                float totSalesA = 0;
                int isfloat = 0;
                float profitA = 0;

                if (tblA.Rows.Count != 0)
                {
                    minutesA = (float)Convert.ToSingle(tblA.Rows[0]["CMinutes"].ToString());
                    callsA = Convert.ToInt32(tblA.Rows[0]["Calls"].ToString());
                    profitA = (float)Convert.ToSingle(tblA.Rows[0]["Profit"].ToString());
                    asrA = Convert.ToInt32(tblA.Rows[0]["ASR"].ToString());
                    acdA = (float)Convert.ToSingle(tblA.Rows[0]["CACD"].ToString());
                    totSalesA = (float)Convert.ToSingle(tblA.Rows[0]["TotalSales"].ToString());
                    //column change
                    MinutesC = Convert.ToString(minutesY - minutesA).Replace("-", "");
                    isfloat = Convert.ToInt32(MinutesC.IndexOf("."));
                    if (isfloat < 0)
                    {
                        lblMinutesC.Text = Util.getNumberFormat(MinutesC, 0);
                    }
                    else
                    {
                        //MinutesC = Util.getNumberFormat(MinutesC, 0);
                        //MinutesC = MinutesC.Substring(0, MinutesC.Length - 3);
                        //lblMinutesC.Text = CMinutes;
                        lblMinutesC.Text = Util.getNumberFormat(MinutesC, 1);
                    }

                    lblMinutesC.Text = lblMinutesC.Text.Replace("-", "");
                    lblCallsC.Text = Util.getNumberFormat(Convert.ToString((callsA - callsY)).Replace("-", ""), 0);
                    lblProfitC.Text = Util.getNumberFormat(Convert.ToString((profitA - profitY)).Replace("-", ""), 2);
                    lblASRC.Text = Convert.ToString((asrA - asrY)).Replace("-", "") + ".00%";
                    lblACDC.Text = lblACDC.Text = Convert.ToString((acdA - acdY)).Replace("-", "");

                    string sales = Convert.ToString(totSalesY - totSalesA).Replace("-", "");
                    isfloat = Convert.ToInt32(sales.IndexOf("."));
                    if (isfloat < 0)
                    {
                        lblTotSalesC.Text = Util.getNumberFormat(Convert.ToString((totSalesA - totSalesY)).Replace("-", ""), 0);
                    }
                    else
                    {
                        lblTotSalesC.Text = Util.getNumberFormat(Convert.ToString((totSalesA - totSalesY)).Replace("-", ""), 2);
                    }
                }
                else
                {
                    lblMinutesC.Text = "0.00";
                    lblCallsC.Text = "0.00";
                    lblASRC.Text = "0.00%";
                    lblACDC.Text = "0.00";
                    lblTotSalesC.Text = "0.00";
                    lblProfitC.Text = "0.00";
                }

                //// end column change

                if (minutesY < minutesA)                //MINUTOS
                {
                    Image4.ImageUrl = "./Images/roja.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image4.ImageUrl = "./Images/verde.gif";
                    lblMinutesC.ForeColor = System.Drawing.Color.Green;
                }

                if (callsY < callsA)                     //LLAMADAS
                {
                    Image5.ImageUrl = "./Images/roja.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image5.ImageUrl = "./Images/verde.gif";
                    lblCallsC.ForeColor = System.Drawing.Color.Green;
                }
                if (profitY < profitA)                      //PROFIT
                {
                    Image7.ImageUrl = "./Images/roja.gif";
                    lblProfitC.ForeColor = System.Drawing.Color.Red;

                }
                else
                {
                    Image7.ImageUrl = "./Images/verde.gif";
                    lblProfitC.ForeColor = System.Drawing.Color.Green;

                }
                if (asrY < asrA)                            //ASR
                {
                    Image8.ImageUrl = "./Images/roja.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image8.ImageUrl = "./Images/verde.gif";
                    lblASRC.ForeColor = System.Drawing.Color.Green;
                }

                if (acdY < acdA)                           //ACD
                {
                    Image10.ImageUrl = "./Images/roja.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image10.ImageUrl = "./Images/verde.gif";
                    lblACDC.ForeColor = System.Drawing.Color.Green;
                }

                if (totSalesY < totSalesA)                 //TOTAL SALES
                {
                    Image13.ImageUrl = "./Images/roja.gif";
                    lblTotSalesC.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    Image13.ImageUrl = "./Images/verde.gif";
                    lblTotSalesC.ForeColor = System.Drawing.Color.Green;
                }


                //lblBillingRunning.Visible = true;
                //lblBillingRunning.Text = "Billing not Runing";

                //MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter adpUN = new MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter();
                //MyDataSetTableAdapters.unratedMinutes_DataTable tblUN = adpUN.GetData();
                //if (tblUN.Rows.Count > 0)//There are unrated minutes?
                //{
                //    HyperLink1.Text = "There are unrated minutes in MECCA!";
                //    HyperLink1.ForeColor = System.Drawing.Color.Red;

                //}
                //else
                //{
                //    HyperLink1.Text = "All minutes are rated in MECCA!";
                //}


                //MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter adpU2 = new MyDataSetTableAdaptersTableAdapters.UnknownIDs2TableAdapter();
                //MyDataSetTableAdapters.UnknownIDs2DataTable tblU2 = adpU2.GetData();

                //if (tblU2.Rows.Count > 0)
                //{
                //    HyperLink2.Text = "There are unmatched IDs in MECCA!";
                //    HyperLink2.ForeColor = System.Drawing.Color.Red;
                //}
                //else
                //{
                //    HyperLink2.Text = "All IDs are matched";
                //}

                //MyDataSetTableAdaptersTableAdapters.unratedMinutesDIDTableAdapter adpUN = new MyDataSetTableAdaptersTableAdapters.unratedMinutesDIDTableAdapter();
                //MyDataSetTableAdapters.unratedMinutesDIDDataTable tblUN = adpUN.GetData();
                //if (tblUN.Rows.Count > 0)//There are unrated minutes?
                //{
                //    HyperLink1.Text = "There are Unmatched DID's!!!";
                //    HyperLink1.ForeColor = System.Drawing.Color.Red;

                //}
                //else
                //{
                //    HyperLink1.Text = "All DID's are Matched!!!";
                //}

                string yest = DateTime.Today.AddDays(-1).ToShortDateString();
                MyDataSetTableAdaptersTableAdapters.trafficTopAdp adpT = new MyDataSetTableAdaptersTableAdapters.trafficTopAdp();
                gdvCustomers.DataSource = adpT.GetDataO(Convert.ToDateTime(yest));
                gdvCustomers.DataBind();

                MyDataSetTableAdaptersTableAdapters.trafficVtopAdp adpV = new MyDataSetTableAdaptersTableAdapters.trafficVtopAdp();
                gdVendors.DataSource = adpV.GetDataO(Convert.ToDateTime(yest));
                gdVendors.DataBind();

                MyDataSetTableAdaptersTableAdapters.trafficTopCAdp adpC = new MyDataSetTableAdaptersTableAdapters.trafficTopCAdp();
                gdvCountry.DataSource = adpC.GetDataO(Convert.ToDateTime(yest));
                gdvCountry.DataBind();

                //MyDataSetTableAdaptersTableAdapters.TopDIDProvidersTableAdapter adpR = new MyDataSetTableAdaptersTableAdapters.TopDIDProvidersTableAdapter();
                //gdvdidProviders.DataSource = adpR.GetData();
                //gdvdidProviders.DataBind();

                //MyDataSetTableAdaptersTableAdapters.didPortStatusTableAdapter adpR = new MyDataSetTableAdaptersTableAdapters.didPortStatusTableAdapter();
                //gdvdidProviders.DataSource = adpR.GetData();
                //gdvdidProviders.DataBind();

                //MyDataSetTableAdaptersTableAdapters.trafficICSTypeTableAdapter adpType = new MyDataSetTableAdaptersTableAdapters.trafficICSTypeTableAdapter();
                //gvTrafficType.DataSource = adpType.GetData(Convert.ToDateTime(yest));
                //gvTrafficType.DataBind();

                SummaryTableAdapters.SummaryOWeekTableAdapter adpSt = new SummaryTableAdapters.SummaryOWeekTableAdapter();
                gvTopStates.DataSource = adpSt.GetData();
                //MyDataSetTableAdaptersTableAdapters.trafficStateTopTableAdapter adpSt = new MyDataSetTableAdaptersTableAdapters.trafficStateTopTableAdapter();
                //gvTopStates.DataSource = adpSt.GetData(Convert.ToDateTime(yest));
                gvTopStates.DataBind();

                //MyDataSetTableAdaptersTableAdapters.UnknownIDs1TableAdapter adpUn = new MyDataSetTableAdaptersTableAdapters.UnknownIDs1TableAdapter();
                //MyDataSetTableAdapters.UnknownIDs1DataTable tblUn = adpUn.GetData();

                String query = null;
                DataTable tblg;
                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
                query += "WHERE [Time] >= DATEADD(DAY,0,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in ('OLFD','OLCL','OL50','OLWH')  and [type] like 'V%'  group by Time ORDER BY Time";

                //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdnVendorsDaily] ";
                //query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
                //query += "[Live_Calls] > 0   AND Cust_Vendor in ('OLDR','OLAM','BSTI','OLBR','OLCL','OLC2','OLC3','OLEC','OLFX','OGCL','OGMS','OGTI','OCIC','OLMS','OLPL','OLPH','OLPO','OLPR') and [type] like 'V%'  group by Time ORDER BY Time";

                tblg = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                makeChartICS(tblg, "OUTLANDER-Today");

                tblg.Dispose();

                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] ";
                query += "WHERE [Time] >= DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,0,LEFT(GETDATE(),11)) and [Live_Calls] > 0   AND Cust_Vendor in ('OLFD','OLCL','OL50','OLWH') and [type] like 'V%'  group by Time ORDER BY Time";

                tblg = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                makeChartICSY(tblg, "OUTLANDER-Yesterday");

                tblg.Dispose();


            }


            if ((Session["Rol"].ToString() == "NOC" || Session["Rol"].ToString() == "NOC National") && Session["Rol"].ToString() != "Comercial" && Session["Rol"].ToString() != string.Empty)
            {

                lblHeaderProfit1.Visible = false;
                lblHeaderProfit2.Visible = false;
                lblHeaderProfit3.Visible = false;


                TableItemStyle estilo = new TableItemStyle();
                estilo.CssClass = "white";

                gdvCountry.Columns[2].Visible = false;
                lblHeaderProfit1.Text = "Minutes";
                lblHeaderProfit1.Visible = true;
                Label7.Visible = false;

                gdVendors.Columns[2].Visible = false;
                lblHeaderProfit2.Text = "Minutes";
                lblHeaderProfit2.Visible = true;
                Label2.Visible = false;

                gdvCustomers.Columns[2].Visible = false;
                lblHeaderProfit3.Text = "Minutes";
                lblHeaderProfit3.Visible = true;

                Label4.Visible = false;


                //lblProfitY.Visible = false;
                //lblProfitC.Visible = false;
                //Image6.Visible = false;
                //Label5.Visible = false;

                //Label11.Visible = false;
                //lblTotSalesY.Visible = false;
                //lblTotSalesC.Visible = false;
                //Image13.Visible = false;

            }
            else
            {

            }
            //BOARDS ROUTING AND DEPLOYMENT
            //R
            //MECCA2TableAdapters.MonitoringAdp adpBoards = new MECCA2TableAdapters.MonitoringAdp();          

            //gdvRouting.DataSource = adpBoards.GetTop5Routing();
            //gdvRouting.DataBind();
            ////D
            //gdvDeployment.DataSource = adpBoards.GetTop5Deployment();
            //gdvDeployment.DataBind();

            ////
            //if (Session["DirectoUser"].ToString() == "david" || Session["DirectoUser"].ToString() == "hvalle")
            //{
            //    hypNational.Visible = true;
            //}
            //else
            //{
            //    hypNational.Visible = false;
            //}

        }
        catch (Exception ex)
        {
            string message = ex.Message.ToString();
        }

    }
    //private void RunCtryCmmsn(string storeProcedure)
    //{
    //    try
    //    {
    //        SqlCommand sqlSpQuery = new SqlCommand();
    //        sqlSpQuery.CommandType = System.Data.CommandType.StoredProcedure;
    //        sqlSpQuery.CommandText = storeProcedure;
    //        sqlSpQuery.Parameters.Add("@fromP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();
    //        sqlSpQuery.Parameters.Add("@toP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-1).ToShortDateString();
    //        sqlSpQuery.CommandTimeout = 160;
    //        DataSet ResultSetC;
    //        //ResultSetC = Util.RunQuery(sqlSpQuery);
    //        ResultSetC = Util.RunQueryByStmnt("select isnull(minutes,0) Minutes,isnull(answeredcalls,0) AnsweredCalls,isnull(grossprofit,0) GrossProfit from Widgethvalle where Id=1");
    //        DataTable tbl = (DataTable)ResultSetC.Tables[0].Copy();//yesterday

    //        for (int k = 1; k < ResultSetC.Tables.Count; k++)
    //        {
    //            tbl.LoadDataRow(ResultSetC.Tables[k].Rows[0].ItemArray, false);
    //        }

    //        SqlCommand sqlSpQuery2 = new SqlCommand();
    //        sqlSpQuery2.CommandType = System.Data.CommandType.StoredProcedure;
    //        sqlSpQuery2.CommandText = storeProcedure;
    //        sqlSpQuery2.Parameters.Add("@fromP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-7).ToShortDateString();
    //        sqlSpQuery2.Parameters.Add("@toP", SqlDbType.VarChar).Value = DateTime.Now.AddDays(-7).ToShortDateString();
    //        sqlSpQuery2.CommandTimeout = 160;
    //        DataSet ResultSet2;
    //        //ResultSet2 = RunQuery(sqlSpQuery2);
    //        ResultSet2 = Util.RunQueryByStmnt("select isnull(minutes,0) Minutes,isnull(answeredcalls,0) AnsweredCalls,isnull(grossprofit,0) GrossProfit from Widgethvalle where Id=2");
    //        DataTable tbl2 = (DataTable)ResultSet2.Tables[0].Copy();//1 month ago

    //        for (int k = 1; k < ResultSetC.Tables.Count; k++)
    //        {
    //           tbl2.LoadDataRow(ResultSet2.Tables[k].Rows[0].ItemArray, false);
    //        }

    //        for (int z = 0; z < 8; z++)
    //        {

    //            decimal gP = Convert.ToDecimal( tbl.Rows[z]["GrossProfit"].ToString() );
    //            decimal gp2 = Convert.ToDecimal( tbl2.Rows[z]["GrossProfit"].ToString() );

    //            if (z == 0)//top Andre
    //            {
    //                lblTCall1.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
    //                lblTMin1.Text = tbl.Rows[z]["Minutes"].ToString();
    //                lblTgross1.Text = tbl.Rows[z]["GrossProfit"].ToString();
    //                if (gP > gp2)
    //                {
    //                    imgStatus1.ImageUrl = "./Images/verde.gif";
    //                }
    //                else
    //                {
    //                    imgStatus1.ImageUrl = "./Images/roja.gif";
    //                }
    //            }
    //            else if (z == 1)//Hamit
    //            {
    //                lblTCall2.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
    //                lblTMin2.Text = tbl.Rows[z]["Minutes"].ToString();
    //                lblTgross2.Text = tbl.Rows[z]["GrossProfit"].ToString();
    //                if (gP > gp2)
    //                {
    //                    imgStatus2.ImageUrl = "./Images/verde.gif";
    //                }
    //                else
    //                {
    //                    imgStatus2.ImageUrl = "./Images/roja.gif";
    //                }
    //            }
    //            else if (z == 2)//Iliana
    //            {
    //                lblTCall3.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
    //                lblTMin3.Text = tbl.Rows[z]["Minutes"].ToString();
    //                lblTgross3.Text = tbl.Rows[z]["GrossProfit"].ToString();

    //                if (gP > gp2)
    //                {
    //                    imgStatus3.ImageUrl = "./Images/verde.gif";
    //                }
    //                else
    //                {
    //                    imgStatus3.ImageUrl = "./Images/roja.gif";
    //                }
    //            }
    //            else if (z == 3)//Jake
    //            {
    //                lblTCall4.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
    //                lblTMin4.Text = tbl.Rows[z]["Minutes"].ToString();
    //                lblTgross4.Text = tbl.Rows[z]["GrossProfit"].ToString();

    //                if (gP > gp2)
    //                {
    //                    imgStatus4.ImageUrl = "./Images/verde.gif";
    //                }
    //                else
    //                {
    //                    imgStatus4.ImageUrl = "./Images/roja.gif";
    //                }
    //            }
    //            else if (z == 9)//Jimb
    //            {
    //                //if (tbl.Rows[z]["AnsweredCalls"] == System.DBNull.Value)
    //                //{
    //                //    lblTCall5.Text = "0";
    //                //    lblTMin5.Text = "0";
    //                //    lblTgross5.Text = "0";
    //                //    imgStatus5.ImageUrl = "./Images/roja.gif";
    //                //}
    //                //else
    //                //{


    //                //    lblTCall5.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
    //                //    lblTMin5.Text = tbl.Rows[z]["Minutes"].ToString();
    //                //    lblTgross5.Text = tbl.Rows[z]["GrossProfit"].ToString();
    //                //    if (gP > gp2)
    //                //    {
    //                //        imgStatus5.ImageUrl = "./Images/verde.gif";
    //                //    }
    //                //    else
    //                //    {
    //                //        imgStatus5.ImageUrl = "./Images/roja.gif";
    //                //    }
    //                //}

    //            }
    //            else if (z == 4)//Martha
    //            {
    //                lblTCall6.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
    //                lblTMin6.Text = tbl.Rows[z]["Minutes"].ToString();
    //                lblTgross6.Text = tbl.Rows[z]["GrossProfit"].ToString();

    //                if (gP > gp2)
    //                {
    //                    imgStatus6.ImageUrl = "./Images/verde.gif";
    //                }
    //                else
    //                {
    //                    imgStatus6.ImageUrl = "./Images/roja.gif";
    //                }
    //            }
    //            else if (z == 5)//Michelle
    //            {
    //                lblTCall7.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
    //                lblTMin7.Text = tbl.Rows[z]["Minutes"].ToString();
    //                lblTgross7.Text = tbl.Rows[z]["GrossProfit"].ToString();

    //                if (gP > gp2)
    //                {
    //                    imgStatus7.ImageUrl = "./Images/verde.gif";
    //                }
    //                else
    //                {
    //                    imgStatus7.ImageUrl = "./Images/roja.gif";
    //                }
    //            }
    //            else if (z == 6)//Roberto
    //            {
    //                lblTCall8.Text = tbl.Rows[z]["AnsweredCalls"].ToString();
    //                lblTMin8.Text = tbl.Rows[z]["Minutes"].ToString();
    //                lblTgross8.Text = tbl.Rows[z]["GrossProfit"].ToString();

    //                if (gP > gp2)
    //                {
    //                    imgStatus8.ImageUrl = "./Images/verde.gif";
    //                }
    //                else
    //                {
    //                    imgStatus8.ImageUrl = "./Images/roja.gif";
    //                }
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        string errormessage = ex.Message.ToString();
    //    }
    //}

    protected void buttonicshome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeR.aspx");
    }

    protected void buttonhome_click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }

    private void makeChartICS(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();

            //this.Chart1.Titles.Add(tITLE);
            this.Chart1.Titles["Title1"].Text = tITLE;
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetailICS"] = tbl;

            this.Chart1.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            Session["tblDetailICS"] = tbl;
        }

    }

    private void makeChartICSY(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart2.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart2.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart2.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart2.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart2.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();

            //this.Chart1.Titles.Add(tITLE);
            this.Chart2.Titles["Title1"].Text = tITLE;
            this.Chart2.RenderType = RenderType.ImageTag;
            Session["tblDetailICSY"] = tbl;

            this.Chart2.Visible = true;
        }
        else
        {
            this.Chart2.Visible = false;
            Session["tblDetailICSY"] = tbl;
        }

    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            string messagerror = ex.Message.ToString();
        }

        return resultsDataSet;
    }
}
