using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;


public partial class PortsGraphComparativeAA : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Text = DateTime.Now.ToShortDateString();
            txtTo.Text = DateTime.Now.ToShortDateString();
            if (Request.QueryString.Count > 0)
            {
                string provider = Request.QueryString["P"].ToString();
                string query = string.Empty;
                string queryb = string.Empty;
                string title = string.Empty;
                DataTable tbl = null;
                DataTable tblb = null;
                string from = Request.QueryString["dF"].ToString();
                string to = Request.QueryString["dT"].ToString();
                from = from.Replace("_", "/");
                to = to.Replace("_", "/");

                if (provider == "**ALL**") provider = "%";

                if (Request.QueryString["T"].ToString() == "0") //Customer
                {

                    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and providername in (select customer from cdrdb.dbo.custbrokerrel where custbroker = 'CallCenters') and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' and ID IN('NY03','MIA03','MIA02','STC01','STC03','MIA06','MIA08') group by [Time] order by [Time]";
                    queryb = "SELECT sum([Live_Calls]) as Live_Calls,dateadd(day,7,[Time]) 'Time' FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and providername in (select customer from cdrdb.dbo.custbrokerrel where custbroker = 'CallCenters') and [Type] like 'C%') and [Time] between dateadd(day,-7,'" + from + " 00:00:00.000') and dateadd(day,-7,'" + to + " 23:59:59.000') and ID IN('NY03','MIA03','MIA02','STC01','STC03','MIA06','MIA08') group by dateadd(day,7,[Time]) order by dateadd(day,7,[Time])";
                    tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                    tblb = Util.RunQueryByStmntatCDRDB(queryb).Tables[0];

                    if (provider == "%") provider = "ALL";
                    title = "Customer: " + provider;
                    
                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;
                }
                

                makeChart(tbl, 1,title);
                makeChart(tblb, 2, title);  
                lblGrid.Visible = false;

                txtFrom.Text = from;
                txtTo.Text = to;
                dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
                dpdType.SelectedIndex = Convert.ToInt32(Request.QueryString["T"]);
                cmdDetail.Visible = true;

            }
        
        }
        else
        {
            
            if (Request.QueryString.Count > 0)
            {
                string provider = Request.QueryString["P"].ToString();
                string query = string.Empty;
                string queryb = string.Empty;
                string title = string.Empty;
                DataTable tbl = null;
                DataTable tblb = null;
                string from = txtFrom.Text;//Request.QueryString["dF"].ToString();
                string to = txtTo.Text;//Request.QueryString["dT"].ToString();
                from = from.Replace("_", "/");
                to = to.Replace("_", "/");

                if (provider == "**ALL**") provider = "%";

                

                if (Request.QueryString["T"].ToString() == "0") //Customer
                {
                    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and providername in (select customer from cdrdb.dbo.custbrokerrel where custbroker = 'CallCenters') and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' and ID IN('NY03','NY07','MIA02','STC01','STC03','MIA03','MIA06','MIA08') group by [Time] order by [Time]";
                    queryb = "SELECT sum([Live_Calls]) as Live_Calls, dateadd(day,7,[Time]) 'Time' FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and providername in (select customer from cdrdb.dbo.custbrokerrel where custbroker = 'CallCenters') and [Type] like 'C%') and [Time] between dateadd(day,-7,'" + from + " 00:00:00.000') and dateadd(day,-7,'" + to + " 23:59:59.000') and ID IN('NY03','MIA03','MIA02','STC01','STC03','MIA06','MIA08') group by dateadd(day,7,[Time]) order by dateadd(day,7,[Time])";                 
                    tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                    tblb = Util.RunQueryByStmntatCDRDB(queryb).Tables[0];

                    if (provider == "%") provider = "ALL";
                    title = "Customer: " + provider;

                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;
                }
               

                makeChart(tbl,1, title);
                makeChart(tblb, 2, title); 
                lblGrid.Visible = false;

                txtFrom.Text = from;
                txtTo.Text = to;

                
            }
        
        }

    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {

        string from = txtFrom.Text;
        string to = txtTo.Text;

        from = from.Replace("/", "_");
        to = to.Replace("/", "_");

        if (dpdType.SelectedIndex == 0)//Customer
        {
            if (dpdProvider.SelectedValue.ToString() == "A&D-WORLDWIDE")
            {
                string c = dpdProvider.SelectedValue.ToString();
                c = c.Replace("&", "_");
                Response.Redirect("PortsGraphComparativeCC.aspx?P=" + c + "&T=0&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
            }
            else
            {
                Response.Redirect("PortsGraphComparativeCC.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=0&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
            }
        }
        else                             //Vendor
            Response.Redirect("PortsGraphComparativeCC.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=1&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
        
        
    }

    private void makeChart(DataTable tbl, int i ,string tITLE)
    {
        if (i == 1) //Current Ports
        {
            if (tbl.Rows.Count > 0)
            {
                int pdx;
                string script = string.Empty;

                this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

                foreach (DataRow myRow in tbl.Rows)
                {

                    pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                    script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                    script = script.Replace(" ", "");
                    this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

                }


                this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
                DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
                //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
                this.Chart1.Titles.Add(tITLE);
                this.Chart1.RenderType = RenderType.ImageTag;
                Session["tblDetail"] = tbl;

                this.Chart1.Visible = true;
                lblWarning.Visible = false;
                this.cmdDetail.Visible = true;
            }
            else
            {
                this.Chart1.Visible = false;
                lblWarning.Visible = true;
                Session["tblDetail"] = tbl;
                this.cmdDetail.Visible = false;

            }
        }
        else //PortsLastWeek
        {
            if (tbl.Rows.Count > 0)
            {
                int pdx;
                string script = string.Empty;

                this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

                foreach (DataRow myRow in tbl.Rows)
                {

                    pdx = Chart1.Series["PortsLastWeek"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                    script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                    script = script.Replace(" ", "");
                    this.Chart1.Series["PortsLastWeek"].Points[pdx].MapAreaAttributes = script;

                }


                this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
                DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
                //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
                this.Chart1.Titles.Add(tITLE);
                this.Chart1.RenderType = RenderType.ImageTag;
                Session["tblDetail"] = tbl;

                this.Chart1.Visible = true;
                lblWarning.Visible = false;
                this.cmdDetail.Visible = true;
            }
            else
            {
                this.Chart1.Visible = false;
                lblWarning.Visible = true;
                Session["tblDetail"] = tbl;
                this.cmdDetail.Visible = false;

            }
        }        

    }
    protected void cmdDetail_Click(object sender, EventArgs e)
    {
        if (((DataTable)Session["tblDetail"]).Rows.Count > 0)
        {

            gdvDetail.DataSource = ((DataTable)Session["tblDetail"]);
            gdvDetail.DataBind();

            gdvDetail.Visible = true;
            lblGrid.Visible = true;
            lblGrid.Text = dpdProvider.SelectedValue.ToString();
            cmdHide.Visible = true;
            cmdDetail.Visible = false;
        }
        else
        {
        }
    }
    protected void cmdHide_Click(object sender, EventArgs e)
    {
        gdvDetail.Visible = false;
        lblGrid.Visible = false;
        cmdDetail.Visible = true;
        cmdHide.Visible = false;

    }
}
