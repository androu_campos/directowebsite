using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;

public partial class TMXGraph : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String query = null;
        DataTable tbl;

        query = "select [Time], sum(Live_Calls) 'Live_Calls' ";
        query += "from cdrdb.dbo.cdrn ";
        query += "where cust_vendor in ('TCDF','TCNL','TUNL','TMEX','TNAL') ";
        query += "and time >= dateadd(day,0,left(getdate(),11)) ";
        query += "and time < dateadd(day,1,left(getdate(),11)) ";
        query += "group by [Time] ";
        query += "order by time ";
        
        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChart1(tbl, "TELMEX");

        tbl.Dispose();
    }

    private void makeChart1(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart1.Titles.Add(tITLE);
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart1.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            Session["tblDetail1"] = tbl;
        }

    }
}
