using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class QbuilderbydatePh : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Value = DateTime.Today.AddDays(-1).ToShortDateString();
            txtTo.Value = DateTime.Today.AddDays(-1).ToShortDateString();


            txtToV.Text = txtTo.Value.ToString();
            txtFromV.Text = txtFrom.Value.ToString();
            //SqlConnection SqlConn = new SqlConnection();
            //SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";
            //SqlCommand SqlCommand7 = new SqlCommand("SELECT DISTINCT Class FROM Regions WHERE Class is not NULL UNION SELECT '** NONE **' AS Class FROM Regions UNION SELECT '**ALL**' AS Class FROM Regions ORDER BY Class", SqlConn);
            //SqlConn.Open();
            //SqlDataReader myReader7 = SqlCommand7.ExecuteReader();
            //while (myReader7.Read())
            //{
            //    dpdClass.Items.Add(myReader7.GetValue(0).ToString());
            //}
            //myReader7.Close();
            //SqlConn.Close();

        }

    }

    protected void ddlCust_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlCust.SelectedItem.ToString() == "**ALL**") hfCust.Value = "%";
        else hfCust.Value = ddlCust.SelectedItem.ToString();

    }
    //protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (ddlVendor.SelectedItem.ToString() == "**ALL**") hfVendor.Value = "%";
    //    else hfVendor.Value = ddlVendor.SelectedItem.ToString();

    //}
    //protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //    string Coun;

    //    if (ddlCountry.SelectedItem.ToString() == "**ALL**") Coun = "%";
    //    else Coun = ddlCountry.SelectedItem.ToString();
    //    hfCountry.Value = Coun;
    //    ddlRegion.Enabled = true;
    //    ddlRegion.Items.Clear();
    //    sqldsRegion.SelectCommand = "SELECT  '**ALL**' as Region FROM CDRDB..Regions UNION SELECT DISTINCT Region FROM CDRDB..Regions UNION SELECT '** NONE **' as Region FROM CDRDB..Regions WHERE Country like '" + Coun + "' ORDER BY Region";

    //}
    protected void btnSubmit_Click(object sender, EventArgs e)
    {


        string customer = ddlCust.SelectedValue;
        string Query = string.Empty;

        if (customer != null)
        {
            if (customer == "**ALL**")
            {
                Query = " SELECT BillingDate, Customer,Country,SUM(CUSTMINUTES) CMinutes, ";
                Query += "SUM(BILLABLECALLS) AnsweredCalls,SUM(tCalls) Attempts,SUM(RA) RejectedCalls ";
                Query += ",mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR ";
                Query += ",mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR ";
                Query += ",mecca2.dbo.fGETACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD ";
                Query += "FROM MECCA2.DBO.TRAFFICLOG ";
                Query += "WHERE BILLINGDATE BETWEEN '" + txtFromV.Text + "' AND '" + txtToV.Text + "' ";
                Query += "AND VENDOR = 'ALLACCESS-UNL-PASS' ";
                Query += "GROUP BY BillingDate, Customer,Country ";
                Query += "HAVING SUM(CUSTMINUTES) > 0 ORDER BY BillingDate ";
            }
            else
            {
                Query = " SELECT BillingDate, Customer,Country,SUM(CUSTMINUTES) CMinutes, ";
                Query += "SUM(BILLABLECALLS) AnsweredCalls,SUM(tCalls) Attempts,SUM(RA) RejectedCalls ";
                Query += ",mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR ";
                Query += ",mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR ";
                Query += ",mecca2.dbo.fGETACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD ";
                Query += "FROM MECCA2.DBO.TRAFFICLOG ";
                Query += "WHERE BILLINGDATE BETWEEN '" + txtFromV.Text + "' AND '" + txtToV.Text + "' ";
                Query += "AND VENDOR = 'ALLACCESS-UNL-PASS' ";
                Query += "AND CUSTOMER LIKE '" + customer + "' ";
                Query += "GROUP BY BillingDate, Customer,Country ";
                Query += "HAVING SUM(CUSTMINUTES) > 0 ORDER BY BillingDate ";
            }
        }

         


        //Query = Query.Replace("SELECT ,", "SELECT ");
        Session["SalesLogS"] = Query;
        //Response.Cookies["MSLH"].Value = Query;  
        if (Query.Length > 6)
        {
            Response.Redirect("~/MSLHPH.aspx", false);
        }

    }
    
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect("QbuilderbydatePh.aspx");
    }
    
}
