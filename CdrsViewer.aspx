<%@ Page Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="CdrsViewer.aspx.cs" Inherits="CdrsViewer" Title="DIRECTO - Connections Worldwide"  StylesheetTheme="Theme1"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 <asp:Label ID="Label7" runat="server" Text="CDR's Viewer" CssClass="labelTurkH"></asp:Label>
    <br />
    <br />

    <table >
        <tr>
            <td style="width: 100px" align="right">
                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Choose Customer Code:"
                    Width="122px"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdCustCode" runat="server" CssClass="dropdown" DataSourceID="SqlCustCode" DataTextField="Provider" DataValueField="Provider">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width: 100px" align="right">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Choose Vendor regId:" Width="113px"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdVendId" runat="server" CssClass="dropdown" DataSourceID="SqlVendId" DataTextField="Provider" DataValueField="Provider">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width: 100px" align="right" valign="top">
                &nbsp;<asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Display Last "></asp:Label></td>
            <td style="width: 100px" valign="top" align="left">
                <table>
                    <tr>
                        <td style="width: 100px">
                <asp:TextBox ID="txtMinutes" runat="server" Width="50px" CssClass="labelSteelBlue"></asp:TextBox></td>
                        <td style="width: 100px">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Minutes"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="right" valign="top">
                &nbsp;<asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Display Last"></asp:Label></td>
            <td style="width: 100px" valign="top" align="left">
                <table>
                    <tr>
                        <td style="width: 100px">
                <asp:TextBox ID="txtRecords" runat="server" Width="50px" CssClass="labelSteelBlue"></asp:TextBox></td>
                        <td style="width: 100px">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Records"></asp:Label></td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="Button1_Click"
                    Text="Button" /></td>
        </tr>
        <tr>
            <td style="height: 66px;" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" valign="top">
                
                <asp:SqlDataSource ID="SqlCustCode" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>" SelectCommand="SELECT DISTINCT IP AS Provider FROM ProviderIP WHERE (ASCII(IP) > 64) AND (ProviderName = @providerName) ORDER BY Provider">
                    <SelectParameters>
                        <asp:SessionParameter ConvertEmptyStringToNull="False" DefaultValue="0" Name="providerName"
                            SessionField="DirectoUser" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlVendId" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>" SelectCommand="SELECT DISTINCT IP AS Provider &#13;&#10;FROM ProviderIP &#13;&#10;WHERE (ASCII(IP) > 96) &#13;&#10;AND ProviderName = @providerName&#13;&#10;ORDER BY IP">
                    <SelectParameters>
                        <asp:Parameter ConvertEmptyStringToNull="False" DefaultValue="0" Name="providerName"
                            Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                </td>
        </tr>
    </table>
    <asp:GridView ID="gdvCdrs" runat="server" Font-Names="Arial" Font-Size="8pt" AllowPaging="true" PageSize="25" OnPageIndexChanging="gdvCdrs_PageIndexChanging">
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />
                
                </asp:GridView>
</asp:Content>

