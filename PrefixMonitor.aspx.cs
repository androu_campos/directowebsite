using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class PrefixMonitor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtACD.Text = "100";
            txtASR.Text = "10";
            txtABR.Text = "10";
            txtAttempts.Text = "100";
            txtPrefix.Text = "52";
        }
        else
        {
            DataSet resultSet;
            if (Session["PrefixMonitor"].ToString() != string.Empty)
            {

                resultSet = ((DataSet)Session["PrefixMonitor"]);

                if (resultSet.Tables.Count > 0)
                {
                    if (resultSet.Tables[0].Rows.Count > 0)
                    {
                        gvData.DataSource = resultSet.Tables[0];
                        gvData.DataBind();
                        Label6.Visible = false;
                        gvData.Visible = true;
                    }
                    else
                    {
                        Label6.Visible = true;
                        gvData.Visible = false;
                    }

                }


            }
        }

    }
    protected void btnViewReport_Click(object sender, EventArgs e)
    {

        string query, where, having = "", havatt = "";

        query = "SELECT RegionPrefix,Customer,Vendor,mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls),SUM(RA)) AS ASR,mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR,mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) AS ACD,SUM(Attempts) as Attempts,SUM(Calls) as Calls,SUM(Minutes) as Minutes FROM mecca2..OpSheetInc ";

        where = " WHERE RegionPrefix LIKE '" + txtPrefix.Text + "%'";

        if (chAttempts.Checked == true)
        {
            havatt = havatt + " AND SUM(Attempts) > " + txtAttempts.Text;
        }
        if(chASR.Checked==true)
        {
            having = having + " OR  mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls),SUM(RA)) < " + txtASR.Text;
        }
        if (chABR.Checked == true)
        {
            having = having + " OR  mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) < " + txtABR.Text;
        }
        
        if (chACD.Checked == true)
        {
            having = having + " OR mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) < " + txtACD.Text;
        }
        if (having.Length > 1)
        {
            having = "HAVING (" + having.Substring(4)+")";
        }        
        if (havatt.Length > 1 & having.Length < 1)
        {
            havatt = "HAVING " + havatt.Substring(5);
        }
        

        query = query + where + " GROUP BY RegionPrefix,Customer,Vendor " + having + havatt;


        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.Text;
        sqlSpQuery.CommandText = query;
        



        DataSet resultSet;
        resultSet = RunQuery(sqlSpQuery);
        
        if (resultSet.Tables.Count > 0)
        {
            if (resultSet.Tables[0].Rows.Count > 0)
            {
                gvData.DataSource = resultSet.Tables[0];
                gvData.DataBind();
                Label6.Visible = false;
                Session["PrefixMonitor"] = resultSet;
                gvData.Visible = true;
            }
            else
            {
                Label6.Visible = true;
                Session["PrefixMonitor"] = string.Empty;
                gvData.Visible = false;
            }

        }
        
    }
    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString =
            ConfigurationManager.ConnectionStrings
            ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
            
        }
        return resultsDataSet;
    }


}

