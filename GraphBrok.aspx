<%@ Page StylesheetTheme="Theme1" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="GraphBrok.aspx.cs" Inherits="GraphBrok" Title="Untitled Page" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" style="position: absolute; top: 132px; left: 203px;">
<tr>
<td colspan="3">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1750px" Width="100%" ProcessingMode="Remote">
        <ServerReport ReportPath="/financial/Graph_Broker" />
    </rsweb:ReportViewer>
    </td>
</tr>
</table> 

    <table visible="false" width="100%" style="position: absolute; top: 750px; left: 230px;">
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td align="right" colspan="3">
                            <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        
    </table>

</asp:Content>

