<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="EditGroup.aspx.cs" Inherits="EditGroup" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table align="left">
                <tr>
                    <td style="width: 168px" align="left">
                        <asp:Label ID="lblHeaderEdit" runat="server" CssClass="labelTurk" Text="Edit Group"></asp:Label></td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 168px; height: 25px">
                    </td>
                    <td style="width: 100px; height: 25px">
                    </td>
                    <td style="width: 100px; height: 25px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 168px; height: 22px;" bgcolor="#d5e3f0">
                        <asp:Label ID="lblSelectGroup" runat="server" CssClass="labelTurk" Text="Select Group:"></asp:Label></td>
                    <td style="width: 100px; height: 22px;">
                        <asp:DropDownList ID="dpdGroup" runat="server" CssClass="dropdown" DataTextField="Name"
                            OnSelectedIndexChanged="dpdGroup_SelectedIndexChanged">
                        </asp:DropDownList></td>
                    <td style="width: 100px; height: 22px;" align="left">
                        <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="Button1_Click"
                            Text="Show data" /></td>
                </tr>
                <tr>
                    <td style="width: 168px">
                    </td>
                    <td style="width: 100px" align="right">
                        </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 168px" bgcolor="#d5e3f0">
                        <asp:Label ID="lblEditName" runat="server" CssClass="labelTurk" Text="Name:"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtNewName" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 168px" bgcolor="#d5e3f0">
                        <asp:Label ID="lblEditRate" runat="server" CssClass="labelTurk" Text="Rate:"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtNewRate" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 168px">
                    </td>
                    <td align="right" style="width: 100px">
                        &nbsp;<asp:Button ID="cmdUpdate" runat="server" CssClass="boton" OnClick="cmdUpdate_Click"
                            Text="Update" /></td>
                    <td style="width: 100px">
                    </td>
                </tr>
        <tr>
            <td style="width: 168px">
            </td>
            <td align="right" style="width: 100px">
                <asp:Label ID="lblAfter" runat="server" Text="Label" CssClass="labelTurk" Visible="False"></asp:Label></td>
            <td style="width: 100px">
            </td>
        </tr>
            </table>    
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    
      <asp:SqlDataSource ID="SqlDistinctNames" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
                OnLoad="SqlDistinctNames_Load" SelectCommand="SELECT DISTINCT Name FROM PlanBreakOut&#13;&#10;UNION&#13;&#10;SELECT '**NULL**' as Name FROM PlanBreakOut">
            </asp:SqlDataSource>
    <br />

</asp:Content>

