using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class CdrsViewer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (!Page.IsPostBack)
        {
            txtMinutes.Text = "60";
            txtRecords.Text = "100";

        }
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        string codeCustomer = dpdCustCode.SelectedValue.ToString();
        string vendorId = dpdVendId.SelectedValue.ToString();

        if (codeCustomer == string.Empty) codeCustomer = "%";
        if (vendorId == string.Empty) vendorId = "%";

        string query = "select top  " + txtRecords.Text + " * from cdrdb.dbo.cdrs where m_sRemoteIPAddress like '" + codeCustomer.Replace("** ALL **", "%") + "' and m_sIPAddress like '" + vendorId.Replace("** ALL **", "%") + "' and stop > dateadd(minute,-" + txtMinutes.Text + ",GetDate())";
        SqlCommand qryCommand = new SqlCommand();
        qryCommand.CommandText = query;
        DataSet myDataSet = RunQuery(qryCommand);
        Session["CdrsViewer"] = myDataSet;
        gdvCdrs.DataSource = myDataSet.Tables[0];
        gdvCdrs.DataBind();
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

    
    protected void gdvCdrs_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvCdrs.PageIndex = e.NewPageIndex;
        gdvCdrs.DataSource = ((DataSet)Session["CdrsViewer"]).Tables[0];
        gdvCdrs.DataBind();

    }
}
