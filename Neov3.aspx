<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Neov3.aspx.cs" Inherits="Neov3" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
&nbsp;
    <asp:Panel ID="Panel1" runat="server" BackColor="Transparent" BorderColor="Transparent"
        Font-Bold="True" Font-Overline="False" ForeColor="SteelBlue" GroupingText="Parameters"
        Height="100px" Width="736px">
        <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="SteelBlue" Text="%Calls"
            Width="50px"></asp:Label>&nbsp; &nbsp;<asp:Label ID="Label2" runat="server" Font-Bold="True"
                ForeColor="SteelBlue" Text="%Min"></asp:Label>
        &nbsp;&nbsp;
        <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="SteelBlue" Text="%ASR"></asp:Label>
        &nbsp;
        <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="SteelBlue" Text="%ACD"></asp:Label><br />
        <asp:TextBox ID="txtCalls" runat="server" Width="50px">0.25</asp:TextBox>
        <asp:TextBox ID="txtMin" runat="server" Width="50px">0.25</asp:TextBox>
        <asp:TextBox ID="txtASR" runat="server" Width="50px">0.25</asp:TextBox>
        <asp:TextBox ID="txtACD" runat="server" Width="50px">0.25</asp:TextBox>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Button ID="btnShowGV"
            runat="server" OnClick="btnShowGV_Click" Text="Show Data Details" /></asp:Panel>
    <table>
        <tr>
            <td align="left" style="width: 100px" valign="top">
    <asp:TreeView ID="tvNEO" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="Large"
        ForeColor="LimeGreen" MaxDataBindDepth="10" OnTreeNodePopulate="tvNEO_TreeNodePopulate" EnableClientScript="False" NodeIndent="115" OnTreeNodeExpanded="tvNEO_TreeNodePopulate" PopulateNodesFromClient="False">
        <Nodes>
            <asp:TreeNode Expanded="False" PopulateOnDemand="True" SelectAction="SelectExpand"
                Text="Recent Traffic" Value="Inc"></asp:TreeNode>
            <asp:TreeNode Expanded="False" PopulateOnDemand="True" SelectAction="SelectExpand"
                Text="Today Traffic" Value="All"></asp:TreeNode>            
        </Nodes>
    </asp:TreeView>
            </td>
            <td align="center" style="width: 100px" valign="middle">
    <asp:GridView ID="gvNeo" runat="server" CellPadding="3" ForeColor="Black" GridLines="Vertical"
        Style="left: 256px; top: 466px" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" Visible="False">
        <FooterStyle BackColor="#CCCCCC" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="#CCCCCC" />
    </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

