<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="EditSim.aspx.cs" Inherits="EditSim" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="left">
        <tr>
            <td align="left" style="width: 116px; height: 25px">
            </td>
            <td style="width: 100px; height: 25px">
            </td>
            <td style="width: 100px; height: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 116px" align="left">
                <asp:Label ID="Label1" runat="server" Text="EDIT BROKER" CssClass="labelTurk"></asp:Label></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 116px; height: 26px;">
            </td>
            <td style="width: 100px; height: 26px;">
            </td>
            <td style="width: 100px; height: 26px;">
            </td>
        </tr>
        <tr>
            <td style="width: 116px" align="right" bgcolor="#d5e3f0">
                <asp:Label ID="Label2" runat="server" Text="Select Broker:" CssClass="labelTurk"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdBrokerName" runat="server" DataTextField="Broker" CssClass="dropdown">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 116px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 116px" align="right" bgcolor="#d5e3f0">
                <asp:Label ID="Label3" runat="server" Text="New Broker name:" CssClass="labelTurk"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:TextBox ID="txtNewName" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 116px">
            </td>
            <td style="width: 100px" align="right">
                <asp:Button ID="cmdUpdate" runat="server" OnClick="cmdUpdate_Click" Text="Update Broker" CssClass="boton" /></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 116px">
            </td>
            <td colspan="2">
                <asp:Label ID="lblUpdated" runat="server" Text="Label" CssClass="labelTurk" Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
                    SelectCommand="SELECT DISTINCT Broker FROM Broker UNION SELECT '**NULL**' AS Broker FROM Broker AS Broker_1">
                </asp:SqlDataSource>
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
    </table>
</asp:Content>

