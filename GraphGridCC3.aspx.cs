using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;

public partial class GraphGrid : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Session["DirectoUser"] as string))//Usuario Logeado            
        {
            String query = null;
            DataTable tbl;

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('SDFM') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart1(tbl, "CC-VCIPSANTDFMANUAL");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('CCIA') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart2(tbl, "CC-INDEPENDENCIA-AGS");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('CCSN') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart3(tbl, "CC-CORTIZO-SANTANDER");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('CBAD') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart4(tbl, "CC-BCONNECTADMON");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('CFCB') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart5(tbl, "CC-FCB");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('CDOM') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart6(tbl, "CC-DOMER");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('CMED') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart7(tbl, "CC-COMER-MEDC");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('CME2') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart8(tbl, "CC-COMER-MEDC2");

            tbl.Dispose();

            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
            query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
            query += "[Live_Calls] > 0   AND Cust_Vendor in ('CNEW') and [type] like 'C%'  group by Time ORDER BY Time";
            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

            makeChart9(tbl, "CC-CONTACTO-NEW");

            tbl.Dispose();
        }
        else
        {
            Response.Redirect("~/SignIn.aspx");
        }
    }

    private void makeChart1(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart1.Titles.Add(tITLE);
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetail1"] = tbl;

            this.Chart1.Visible = true;
        }
        else
        {
            this.Chart1.Visible = true;
            Session["tblDetail1"] = tbl;
        }

    }

    private void makeChart2(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart2.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart2.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart2.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart2.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart2.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart2.Titles.Add(tITLE);
            this.Chart2.RenderType = RenderType.ImageTag;
            Session["tblDetail2"] = tbl;

            this.Chart2.Visible = true;
        }
        else
        {
            this.Chart2.Visible = true;
            Session["tblDetail2"] = tbl;
        }

    }

    private void makeChart3(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart3.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart3.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart3.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart3.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart3.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart3.Titles.Add(tITLE);
            this.Chart3.RenderType = RenderType.ImageTag;
            Session["tblDetail3"] = tbl;

            this.Chart3.Visible = true;
        }
        else
        {
            this.Chart3.Visible = true;
            Session["tblDetail3"] = tbl;
        }

    }

    private void makeChart4(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart4.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart4.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart4.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart4.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart4.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart4.Titles.Add(tITLE);
            this.Chart4.RenderType = RenderType.ImageTag;
            Session["tblDetail4"] = tbl;

            this.Chart4.Visible = true;
        }
        else
        {
            this.Chart4.Visible = true;
            Session["tblDetail4"] = tbl;
        }

    }

    private void makeChart5(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart5.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart5.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart5.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart5.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart5.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart5.Titles.Add(tITLE);
            this.Chart5.RenderType = RenderType.ImageTag;
            Session["tblDetail5"] = tbl;

            this.Chart5.Visible = true;
        }
        else
        {
            this.Chart5.Visible = true;
            Session["tblDetail5"] = tbl;
        }

    }

    private void makeChart6(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart6.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart6.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart6.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart6.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart6.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart6.Titles.Add(tITLE);
            this.Chart6.RenderType = RenderType.ImageTag;
            Session["tblDetail6"] = tbl;

            this.Chart6.Visible = true;
        }
        else
        {
            this.Chart6.Visible = true;
            Session["tblDetail6"] = tbl;
        }
    }

    private void makeChart7(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart7.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart7.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart7.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart7.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart7.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart7.Titles.Add(tITLE);
            this.Chart7.RenderType = RenderType.ImageTag;
            Session["tblDetail7"] = tbl;

            this.Chart7.Visible = true;
        }
        else
        {
            this.Chart7.Visible = true;
            Session["tblDetail7"] = tbl;
        }
    }

    private void makeChart8(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart8.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart8.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart8.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart8.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart8.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart8.Titles.Add(tITLE);
            this.Chart8.RenderType = RenderType.ImageTag;
            Session["tblDetail8"] = tbl;

            this.Chart8.Visible = true;
        }
        else
        {
            this.Chart8.Visible = true;
            Session["tblDetail8"] = tbl;
        }

    }

    private void makeChart9(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart9.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart9.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart9.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart9.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart9.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart9.Titles.Add(tITLE);
            this.Chart9.RenderType = RenderType.ImageTag;
            Session["tblDetail9"] = tbl;

            this.Chart9.Visible = true;
        }
        else
        {
            this.Chart9.Visible = true;
            Session["tblDetail9"] = tbl;
        }

    }
}
