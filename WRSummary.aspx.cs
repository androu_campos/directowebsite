using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;


public partial class WRSummary : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string from = "";
        string to = "";
        string query = "";
        DataTable tbl = new DataTable();

        from = Request.QueryString["dF"].ToString();
        to = Request.QueryString["dT"].ToString();

        from = from.Replace("_", "/");
        to = to.Replace("_", "/");

        query = "Select [Proc_date],sum(WholeSale_tot) as WholeSaleCalls, sum(Retail_tot) as RetailCalls from WRHISTORY Where [Proc_date] >= '" + from + " 00:00:00' and [Proc_date] <= '" + to + " 23:59:59' group by [Proc_date] ORDER BY Proc_date";
        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
        makeChart(tbl, "# of Calls per Product", from , to);
        makeChartStackedArea(tbl, "Distribution of Calls over 100% of Total Calls", from, to);


        query = "Select [Proc_date], sum(WholeSale_tot/(WholeSale_tot+Retail_tot)*100) as WholeSaleCalls, sum(Retail_tot/(WholeSale_tot+Retail_tot)*100) as RetailCalls from WRHISTORY Where [Proc_date] >= '" + from + " 00:00:00' and [Proc_date] <= '" + to + " 23:59:59' group by [Proc_date] ORDER BY Proc_date ";
        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
        makeChartStackedArea100(tbl, "Percetage of Distribution of Total Calls over 100%", from, to);
    }



    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    if (!Page.IsPostBack)
    //    {
    //        txtFrom.Text = DateTime.Now.ToShortDateString();
    //        txtTo.Text = DateTime.Now.ToShortDateString();
    //        if (Request.QueryString.Count > 0)
    //        {
    //            string provider = Request.QueryString["P"].ToString();
    //            string query = string.Empty;
    //            string title = string.Empty;
    //            DataTable tbl;
    //            string from = Request.QueryString["dF"].ToString();
    //            string to = Request.QueryString["dT"].ToString();
    //            from = from.Replace("_", "/");
    //            to = to.Replace("_", "/");

    //            if (provider == "**ALL**") provider = "%";

    //            if (Request.QueryString["T"].ToString() == "0") //Customer
    //            {
    //                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
    //                tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

    //                if (provider == "%") provider = "ALL";
    //                title = "Customer: " + provider;
                    
    //                if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
    //                else cmdDetail.Visible = true;
    //            }
    //            else//Vendor
    //            {
    //                if (provider == "OUTLANDER")
    //                {
    //                    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in( 'OLAM','OLCL','OLPL','OLMS','OLPH','OLDR','OLC2','OCNW')  and [type] like 'V%' group by Time ORDER BY Time";
    //                }
    //                else
    //                {
    //                    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'V%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'V%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
    //                }
                    
    //                tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

    //                if (provider == "%") provider = "ALL";
    //                title = "Vendor: " + provider;
                    
    //                if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
    //                else cmdDetail.Visible = true;
    //            }

    //            makeChart(tbl, title);
    //            //gdvDetail.Visible = false;
    //            lblGrid.Visible = false;

    //            txtFrom.Text = from;
    //            txtTo.Text = to;
    //            dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
    //            dpdType.SelectedIndex = Convert.ToInt32(Request.QueryString["T"]);
    //            cmdDetail.Visible = true;

    //        }
        
    //    }
    //    else
    //    {
            
    //        if (Request.QueryString.Count > 0)
    //        {
    //            string provider = Request.QueryString["P"].ToString();
    //            string query = string.Empty;
    //            string title = string.Empty;
    //            DataTable tbl;
    //            string from = txtFrom.Text;//Request.QueryString["dF"].ToString();
    //            string to = txtTo.Text;//Request.QueryString["dT"].ToString();
    //            from = from.Replace("_", "/");
    //            to = to.Replace("_", "/");

    //            if (provider == "**ALL**") provider = "%";

    //            if (Request.QueryString["T"].ToString() == "0") //Customer
    //            {
    //                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
    //                tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

    //                if (provider == "%") provider = "ALL";
    //                title = "Customer: " + provider;

    //                if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
    //                else cmdDetail.Visible = true;
    //            }
    //            else//Vendor
    //            {
    //                if (dpdProvider.SelectedValue.ToString() == "OUTLANDER")
    //                {
    //                    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + txtFrom.Text + " 00:00:00' and [Time] <= '" + txtTo.Text + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in( 'OLAM','OLCL','OLPL','OLMS','OLPH','OLDR','OCNW')  and [type] like 'V%' group by Time ORDER BY Time";
    //                }
    //                else
    //                {
    //                    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'V%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'V%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
    //                }


    //                tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

    //                if (provider == "%") provider = "ALL";
    //                title = "Vendor: " + provider;

    //                if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
    //                else cmdDetail.Visible = true;
    //            }

    //            makeChart(tbl, title);
    //            //gdvDetail.Visible = false;
    //            lblGrid.Visible = false;

    //            txtFrom.Text = from;
    //            txtTo.Text = to;

    //            //dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
    //        }
        
    //    }

    //}
    protected void cmdReport_Click(object sender, EventArgs e)
    {

        //string from = txtFrom.Text;
        //string to = txtTo.Text;

        //from = from.Replace("/", "_");
        //to = to.Replace("/", "_");
        //Session["dF"] = from;
        //Session["dT"] = to;

        //Response.Redirect("WRSummary.aspx");

        //if(dpdType.SelectedIndex == 0)   //Customer
        //Response.Redirect("PortsGraph.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=0&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
        //else                             //Vendor
        //Response.Redirect("PortsGraph.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=1&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
        
        
    }

    private void makeChart(DataTable tbl, string tITLE,string f, string t)
    {
        //string origen = string.Empty;
        //string to = string.Empty;

        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ImageType = Dundas.Charting.WebControl.ChartImageType.Png;

            foreach (DataRow myRow in tbl.Rows)
            {
                pdx = this.Chart1.Series["WholeSaleCalls"].Points.AddXY(myRow["Proc_date"], Convert.ToDouble(myRow["WholeSaleCalls"]));
               
                script = "onclick=dispData('" + myRow["Proc_date"].ToString() + "','" + myRow["WholeSaleCalls"] + "','C')";
                script = script.Replace(" ", "");
                this.Chart1.Series["WholeSaleCalls"].Points[pdx].MapAreaAttributes = script;

                pdx = this.Chart1.Series["RetailCalls"].Points.AddXY(myRow["Proc_date"], Convert.ToDouble(myRow["RetailCalls"]));
                script = "onclick=dispData('" + myRow["Proc_date"].ToString() + "','" + myRow["RetailCalls"] + "','C')";
                script = script.Replace(" ", "");
                this.Chart1.Series["RetailCalls"].Points[pdx].MapAreaAttributes = script;
            }
            DataPoint maxpoint = this.Chart1.Series["WholeSaleCalls"].Points.FindMaxValue();
            DataPoint maxretailpointx = this.Chart1.Series["RetailCalls"].Points.FindMaxValue();
            //lblValue.Text = maxpoint.Label.ToString();

            string origen = string.Empty;
            string to = string.Empty;
            //set the Title

            this.Chart1.Titles.Add(tITLE);
            //draw the MaxLine            
            DataPoint maxpointY = this.Chart1.Series["WholeSaleCalls"].Points.FindMaxValue("Y");
            DataPoint maxpointretailY = this.Chart1.Series["RetailCalls"].Points.FindMaxValue("Y");
                
            origen = f + " 00:00:00 AM";
            to = t + " 23:59:59 PM";            
            
            this.Chart1.Series["MaxWholeSaleCalls"].Points.AddXY(Convert.ToDateTime(to), Convert.ToDouble(maxpointY.YValues[0]));
            this.Chart1.Series["MaxWholeSaleCalls"].Points.AddXY(Convert.ToDateTime(origen), Convert.ToDouble(maxpointY.YValues[0]));

            this.Chart1.Series["MaxRetailCalls"].Points.AddXY(Convert.ToDateTime(to), Convert.ToDouble(maxpointretailY.YValues[0]));
            this.Chart1.Series["MaxRetailCalls"].Points.AddXY(Convert.ToDateTime(origen), Convert.ToDouble(maxpointretailY.YValues[0]));            

        }
        
    }

    private void makeChartStackedArea(DataTable tbl, string tITLE, string f, string t)
    {
        //string origen = string.Empty;
        //string to = string.Empty;

        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart2.ImageType = Dundas.Charting.WebControl.ChartImageType.Png;

            foreach (DataRow myRow in tbl.Rows)
            {
                pdx = this.Chart2.Series["WholeSaleCalls"].Points.AddXY(myRow["Proc_date"], Convert.ToDouble(myRow["WholeSaleCalls"]));

                script = "onclick=dispData('" + myRow["Proc_date"].ToString() + "','" + myRow["WholeSaleCalls"] + "','C')";
                script = script.Replace(" ", "");
                this.Chart2.Series["WholeSaleCalls"].Points[pdx].MapAreaAttributes = script;

                pdx = this.Chart2.Series["RetailCalls"].Points.AddXY(myRow["Proc_date"], Convert.ToDouble(myRow["RetailCalls"]));
                script = "onclick=dispData('" + myRow["Proc_date"].ToString() + "','" + myRow["RetailCalls"] + "','C')";
                script = script.Replace(" ", "");
                this.Chart2.Series["RetailCalls"].Points[pdx].MapAreaAttributes = script;
            }
            DataPoint maxpoint = this.Chart2.Series["WholeSaleCalls"].Points.FindMaxValue();
            DataPoint maxretailpointx = this.Chart2.Series["RetailCalls"].Points.FindMaxValue();
            //lblValue.Text = maxpoint.Label.ToString();

            string origen = string.Empty;
            string to = string.Empty;
            //set the Title

            this.Chart2.Titles.Add(tITLE);
            //draw the MaxLine            
            DataPoint maxpointY = this.Chart2.Series["WholeSaleCalls"].Points.FindMaxValue("Y");
            DataPoint maxpointretailY = this.Chart2.Series["RetailCalls"].Points.FindMaxValue("Y");

            origen = f + " 00:00:00 AM";
            to = t + " 23:59:59 PM";

            this.Chart2.Series["MaxWholeSaleCalls"].Points.AddXY(Convert.ToDateTime(to), Convert.ToDouble(maxpointY.YValues[0]));
            this.Chart2.Series["MaxWholeSaleCalls"].Points.AddXY(Convert.ToDateTime(origen), Convert.ToDouble(maxpointY.YValues[0]));

            this.Chart2.Series["MaxRetailCalls"].Points.AddXY(Convert.ToDateTime(to), Convert.ToDouble(maxpointretailY.YValues[0]));
            this.Chart2.Series["MaxRetailCalls"].Points.AddXY(Convert.ToDateTime(origen), Convert.ToDouble(maxpointretailY.YValues[0]));

        }

    }

    private void makeChartStackedArea100(DataTable tbl, string tITLE, string f, string t)
    {
        //string origen = string.Empty;
        //string to = string.Empty;

        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart3.ImageType = Dundas.Charting.WebControl.ChartImageType.Png;

            foreach (DataRow myRow in tbl.Rows)
            {
                pdx = this.Chart3.Series["WholeSaleCalls"].Points.AddXY(myRow["Proc_date"], Convert.ToDouble(myRow["WholeSaleCalls"]));

                script = "onclick=dispData('" + myRow["Proc_date"].ToString() + "','" + myRow["WholeSaleCalls"] + "','C')";
                script = script.Replace(" ", "");
                this.Chart3.Series["WholeSaleCalls"].Points[pdx].MapAreaAttributes = script;

                pdx = this.Chart3.Series["RetailCalls"].Points.AddXY(myRow["Proc_date"], Convert.ToDouble(myRow["RetailCalls"]));
                script = "onclick=dispData('" + myRow["Proc_date"].ToString() + "','" + myRow["RetailCalls"] + "','C')";
                script = script.Replace(" ", "");
                this.Chart3.Series["RetailCalls"].Points[pdx].MapAreaAttributes = script;
            }
            DataPoint maxpoint = this.Chart2.Series["WholeSaleCalls"].Points.FindMaxValue();
            DataPoint maxretailpointx = this.Chart2.Series["RetailCalls"].Points.FindMaxValue();
            //lblValue.Text = maxpoint.Label.ToString();

            string origen = string.Empty;
            string to = string.Empty;
            //set the Title

            this.Chart3.Titles.Add(tITLE);
            //draw the MaxLine            
            DataPoint maxpointY = this.Chart3.Series["WholeSaleCalls"].Points.FindMaxValue("Y");
            DataPoint maxpointretailY = this.Chart3.Series["RetailCalls"].Points.FindMaxValue("Y");

            origen = f + " 00:00:00 AM";
            to = t + " 23:59:59 PM";

            this.Chart3.Series["MaxWholeSaleCalls"].Points.AddXY(Convert.ToDateTime(to), Convert.ToDouble(maxpointY.YValues[0]));
            this.Chart3.Series["MaxWholeSaleCalls"].Points.AddXY(Convert.ToDateTime(origen), Convert.ToDouble(maxpointY.YValues[0]));

            this.Chart3.Series["MaxRetailCalls"].Points.AddXY(Convert.ToDateTime(to), Convert.ToDouble(maxpointretailY.YValues[0]));
            this.Chart3.Series["MaxRetailCalls"].Points.AddXY(Convert.ToDateTime(origen), Convert.ToDouble(maxpointretailY.YValues[0]));

        }

    }

    protected void cmdDetail_Click(object sender, EventArgs e)
    {
    //    if (((DataTable)Session["tblDetail"]).Rows.Count > 0)
    //    {

    //        gdvDetail.DataSource = ((DataTable)Session["tblDetail"]);
    //        gdvDetail.DataBind();

    //        gdvDetail.Visible = true;
    //        lblGrid.Visible = true;
    //        lblGrid.Text = dpdProvider.SelectedValue.ToString();
    //        cmdHide.Visible = true;
    //        cmdDetail.Visible = false;
    //    }
    //    else
    //    {
    //    }
    }
    protected void cmdHide_Click(object sender, EventArgs e)
    {
        //gdvDetail.Visible = false;
        //lblGrid.Visible = false;
        //cmdDetail.Visible = true;
        //cmdHide.Visible = false;

    }
}
