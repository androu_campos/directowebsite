using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class MasReportLog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DirectoUser"].ToString() == "djassan" || Session["DirectoUser"].ToString() == "hvalle" || Session["DirectoUser"].ToString() == "fguelfi" || Session["DirectoUser"].ToString() == "jgomez" || Session["DirectoUser"].ToString() == "jalbornoz" || Session["DirectoUser"].ToString() == "icarballo" || Session["DirectoUser"].ToString() == "randrade" || Session["DirectoUser"].ToString() == "mrobles" || Session["DirectoUser"].ToString() == "emeade" || Session["DirectoUser"].ToString() == "laguilar")
        {
            if (!IsPostBack)
            {
                TextBox1.Text = DateTime.Now.AddDays(-1).ToShortDateString();
                TextBox2.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            }
        }
        else
        {
            Response.Redirect("Home.aspx", false);
        }
    }
    protected void cmdShow_Click(object sender, EventArgs e)
    {
        string sql = "";
        SqlCommand sqlQry = new SqlCommand();
        sqlQry.CommandType = CommandType.Text;

        sql = "SELECT CONVERT(VARCHAR(11) ,Billingdate,103) as BDATE, VENDOR ,substring(CONVERT(varchar(15),cast(Minutes as money),1) , 1 , charindex ('.',(CONVERT(varchar(15),cast(Minutes as money),1)))-1) as Minutes " +
              ",'$ ' + substring(CONVERT(varchar(15),cast(Profit as money),1) , 1 , charindex ('.',(CONVERT(varchar(15),cast(Profit as money),1)))-1) as Profit " +
              " FROM MECCA2..PROFITR WHERE BILLINGDATE BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' ORDER BY BillingDate ";

        sqlQry.CommandText = sql;

        DataSet myDataset = RunQuery(sqlQry);
        Session["MaxReport"] = myDataset;
        if (myDataset.Tables.Count > 0)
        {
            
            gdv1.DataSource = myDataset.Tables[0];
            gdv1.DataBind();
        }
        else
        {
            //cmdExport.Visible = false;            
        }
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

    protected void gdv1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdv1.PageIndex = e.NewPageIndex;
        gdv1.DataBind();
    }
    protected void cmdExport(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["MaxReport"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "MaxReport" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string message = ex.Message.ToString();
        }
    }
}
