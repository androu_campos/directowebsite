using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class CdrsViewer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Text = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
            txtTo.Text = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");

            btnExport.Visible = false;
        }
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        gdvCdrs.Visible = true;
        //Session["CDRSReport"] = resultSet;
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

    
    protected void gdvCdrs_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvCdrs.PageIndex = e.NewPageIndex;
        gdvCdrs.DataSource = ((DataSet)Session["CdrsViewer"]).Tables[0];
        gdvCdrs.DataBind();

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {
            Session["ResultSetCDRS"] = gdvCdrs;

            DataTable dtCDRS = ((DataSet)Session["ResultSetCDRS"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "CDRS_"+ DateTime.Now.Year + ".xls";
            objExport.ExportDetails(dtCDRS, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
}
