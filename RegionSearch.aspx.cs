using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class RegionSearch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        string code = TextBox1.Text;
        MECCA2TableAdapters.RegionsTableAdapter adp = new MECCA2TableAdapters.RegionsTableAdapter();
        GridView1.DataSource = adp.GetDataByCode(code + "%");        
        GridView1.DataBind();
        

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        string code = TextBox1.Text;
        MECCA2TableAdapters.RegionsTableAdapter adp = new MECCA2TableAdapters.RegionsTableAdapter();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = adp.GetDataByCode(code + "%");
        GridView1.DataBind();
    }
}
