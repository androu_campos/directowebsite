using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Aqb : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                //string qry = "Select Customer,SUM(Minutes)as Minutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls],mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls),SUM(RA)) AS ASR, mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR, mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) AS ACD from OpSheetInc WHERE Customer like 'WAVECREST'   Group by Customer UNION Select '**ALL**' as Customer,SUM(Minutes)as Minutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls],mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls),SUM(RA)) AS ASR, mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR, mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) AS ACD from OpSheetInc WHERE Customer like 'WAVECREST'   ORDER BY Minutes DESC ";

               
                GridView1.DataSourceID = "SqlDataSource1";
               
                ///**/
            
                

            }


        }
        catch (Exception ex)
        {
            string messageerror = ex.Message.ToString();
            Response.Redirect("Qbuilder.aspx", false);

        }



    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
