<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ReportMPop.aspx.cs" Inherits="ReportMPop" Title="DIRECTO - Connections Worldwide" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

   <asp:Label ID="Label3" runat="server" Text="REPORT MPOP" CssClass="labelTurkH"></asp:Label>
       <table align="left" style="position:absolute; top:220px; left:230px;">
       <tr>
       <td>       
       
        <table width="100%" >
            <tr>
                <td align="left" style="width: 105px" valign="top">
                    </td>
                <td align="left" style="width: 77px" valign="top">
                </td>
                <td align="left" style="width: 300px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 105px; height: 27px;" valign="top">
                </td>
                <td align="left" style="width: 77px; height: 27px;" valign="top">
                </td>
                <td align="left" style="width: 300px; height: 27px;" valign="top">
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 105px" valign="top" bgcolor="#d5e3f0">
                    <asp:Label ID="LabelS1" runat="server" Font-Bold="True" Font-Names="Arial"
                        Text="Step 1 " CssClass="labelTurk"></asp:Label></td>
                <td align="left" style="width: 77px" valign="top" bgcolor="#d5e3f0">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial"
                        Text="Choose Range Of   Days" Width="282px" CssClass="labelTurk"></asp:Label></td>
                <td align="left" style="width: 300px" valign="top">
                    </td>
            </tr>
            <tr>
                <td align="left" style="width: 105px; height: 23px" valign="top">
                </td>
                <td align="left" style="width: 77px; height: 23px" valign="top">
                </td>
                <td align="left" style="width: 300px; height: 23px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 105px; height: 27px;" valign="top">
                    <asp:Label ID="labelFrom" runat="server" Font-Bold="True" Font-Names="Arial" Text="From:" CssClass="labelTurk" Font-Size="8pt"></asp:Label></td>
                <td align="left" style="width: 77px; height: 27px" valign="top">
                    <table>
                        <tr>
                            <td style="width: 100px">
                    <asp:TextBox ID="txtFromV" runat="server" CssClass="labelSteelBlue" Width="125px"></asp:TextBox></td>
                            <td style="width: 100px">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                        </tr>
                    </table>
                </td>
                <td align="left" style="width: 300px; height: 27px;" valign="top">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right" style="width: 105px" valign="top">
                    <asp:Label ID="labelTo" runat="server" Font-Bold="True" Font-Names="Arial" Text="To:" CssClass="labelTurk" Font-Size="8pt"></asp:Label></td>
                <td align="left" style="width: 77px" valign="top">
                    <table>
                        <tr>
                            <td style="width: 100px">
                    <asp:TextBox ID="txtToV" runat="server" CssClass="labelSteelBlue" OnTextChanged="txtToV_TextChanged" Width="125px"></asp:TextBox></td>
                            <td style="width: 100px">
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                        </tr>
                    </table>
                </td>
                <td align="left" style="width: 300px" valign="top">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right" style="width: 105px; height: 24px" valign="top">
                </td>
                <td align="right" style="width: 77px; height: 24px" valign="top">
                </td>
                <td align="left" style="width: 300px; height: 24px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 105px" valign="top" bgcolor="#d5e3f0">
                    <asp:Label ID="LabelS2" runat="server" Font-Bold="True" Font-Names="Arial"
                        Text="Step 2 " CssClass="labelTurk"></asp:Label></td>
                <td align="left" style="width: 77px" valign="top" bgcolor="#d5e3f0">
                    <asp:Label ID="Label4" runat="server" CssClass="labelTurk" Text="Click Button"></asp:Label></td>
                <td style="width: 300px" valign="top">
                    </td>
            </tr>
            <tr>
                <td align="left" style="width: 105px; height: 24px" valign="top">
                </td>
                <td align="left" style="width: 77px; height: 24px" valign="top">
                                        <asp:Button ID="Button1" runat="server" BorderStyle="Solid" BorderWidth="1px" CssClass="boton"
                                            OnClick="Button1_Click" Text="Click here" Width="56px" /></td>
                <td style="width: 300px; height: 24px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 105px; height: 22px" valign="top">
                </td>
                <td align="left" style="width: 77px; height: 22px" valign="top">
                </td>
                <td style="width: 300px; height: 22px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 105px" valign="top" bgcolor="#d5e3f0">
                    <asp:Label ID="LabelS4" runat="server" Font-Bold="True" Font-Names="Arial"
                        Text="Step 3" CssClass="labelTurk"></asp:Label></td>
                <td align="left" style="width: 77px" valign="top" bgcolor="#d5e3f0">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Arial"
                        Text="Choose    Region" Width="282px" CssClass="labelTurk"></asp:Label></td>
                <td align="left" style="width: 300px" valign="top">
                    </td>
            </tr>
            <tr>
                <td align="right" style="width: 105px" valign="top">
                    <asp:Label ID="labelRegion" runat="server" Font-Bold="True" Font-Names="Arial" Text="Region:" CssClass="labelTurk" Font-Size="8pt"></asp:Label></td>
                <td align="right" style="width: 77px" valign="top">
                    <asp:DropDownList ID="ddlRegion" runat="server" Font-Names="Arial" Width="300px" CssClass="dropdown">
                    </asp:DropDownList></td>
                <td align="left" style="width: 300px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 105px; height: 25px" valign="top">
                </td>
                <td align="right" style="width: 77px; height: 25px" valign="top">
                </td>
                <td align="left" style="width: 300px; height: 25px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 105px" valign="top">
                    <asp:CheckBox ID="cbGroup" runat="server" Font-Bold="True" Font-Italic="False" Font-Names="Arial"
                        Text="Group Result" TextAlign="Left" CssClass="labelTurk" Font-Size="8pt" /></td>
                <td align="left" style="width: 77px" valign="top">
                    <asp:Button ID="btnSubmit"
            runat="server" Enabled="False" Font-Names="Arial" OnClick="btnSubmit_Click" Text="Submit" BorderStyle="Solid" BorderWidth="1px" CssClass="boton" /></td>
                <td align="left" style="width: 300px" valign="top">
                    </td>
            </tr>
            <tr>
                <td align="left" style="width: 105px; height: 32px" valign="top">
                </td>
                <td align="left" style="width: 77px; height: 32px" valign="top">
                </td>
                <td align="left" style="width: 300px; height: 32px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 105px; height: 27px" valign="top">
                    </td>
                <td align="left" style="width: 77px; height: 27px" valign="top">
                </td>
                <td align="left" style="width: 300px; height: 27px" valign="top">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="3" valign="top" style="height: 21px">
                   
                </td>
            </tr>
        
        
        
    
        
        
        </table>
       
       </td>
       
       <td>
       
       </td>
       </tr>
       
       
       <tr>
       
       <td align="left">
       <%--//boton--%>
       
       <asp:Button ID="exportMpop" runat="server" CssClass="boton" Font-Names="Arial" Font-Size="8pt"
                        OnClick="exportMpop_Click" Text="Export to EXCEL"  Visible="False" />
       </td>
       
       
       <td>
       
       
       </td>
       
       </tr>
       
       <tr>
       <td>
        <asp:GridView ID="gvRePOP" runat="server" Font-Names="Arial" Font-Overline="False"
                        Font-Size="8pt" AutoGenerateColumns="false" Width="750px" AllowPaging="True" OnPageIndexChanging="gvRePOP_PageIndexChanging" PageSize="25">
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                        <EditRowStyle BackColor="#2461BF" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                            Font-Size="8pt" />
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField HeaderText="BillingDate" DataField="BillingDate" >
                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Vendor" DataField="Vendor" >
                                <ItemStyle Width="200px" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Region" DataField="Region" >
                                <ItemStyle Width="150px" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Fixed Min" DataField="Fixed Min">
                                <HeaderStyle HorizontalAlign="Center" Width="80px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="National Min" DataField="National Min">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Fixed Calls" DataField="Fixed Calls">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="National Calls" DataField="National Calls" >
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ACD" DataField="ACD" >
                                <ItemStyle Width="100px" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="%ASR" DataField="ASR" HtmlEncode="False" DataFormatString="{0:0,0.00}">
                                <ItemStyle Width="100px" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="%Overflow" DataField="%Overflow">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                    
                    </asp:GridView>
       
       </td>
       
       <td> </td>
       
       </tr>
       
       
       
           
        <tr>
            <td width="100%" colspan="2">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label5" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
           <tr>
               <td align="center" colspan="2">
                   <asp:ScriptManager id="ScriptManager1" runat="server">
                   </asp:ScriptManager></td>
           </tr>
        
       </table>
       
       
       
        <asp:SqlDataSource ID="dsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
        </asp:SqlDataSource>
        <asp:HiddenField ID="txtFrom" runat="server" />
        <asp:HiddenField ID="txtTo" runat="server" />
        <br />
        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromV" PopupButtonID="Image1">
        </cc1:CalendarExtender>
        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToV" PopupButtonID="Image2">
        </cc1:CalendarExtender>
        



</asp:Content>

