using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class EditSim : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {            
            dpdBrokerName.DataSource = SqlDataSource1;
            dpdBrokerName.DataBind();
        }
        else
        { 
        
        }
    }
    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            AccessDatasetTableAdapters.BrokerTableAdapter adp = new AccessDatasetTableAdapters.BrokerTableAdapter();
            AccessDataset.BrokerDataTable tbl = adp.GetDataByBroker(dpdBrokerName.SelectedValue.ToString());
            int id = Convert.ToInt32(tbl.Rows[0]["IDBroker"].ToString());
            adp.Update(txtNewName.Text, id);
            lblUpdated.Text = "Broker Updated Succesfully";
            lblUpdated.Visible = true;
            dpdBrokerName.DataSource = SqlDataSource1;
            dpdBrokerName.DataBind();
            txtNewName.Text = string.Empty;


        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            lblUpdated.Text = "Error in Updated Broker";
            lblUpdated.Visible = true;
        }
    }
}
