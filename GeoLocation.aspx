<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="GeoLocation.aspx.cs" Inherits="GeoLocation" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGpffoYyZqE58-EOYgrladei-GIGxvKLQ"></script>
<script type="text/javascript">
    function setText(val, e) {
    document.getElementById(e).value = val;
    }

    function insertText(val, e) {
        document.getElementById(e).value += val;
    }

    var nav = null; 

    function requestPosition() {
      if (nav == null) {
          nav = window.navigator;
      }
      if (nav != null) {
          var geoloc = nav.geolocation;
          if (geoloc != null) {
              geoloc.getCurrentPosition(successCallback);
          }
          else {
              alert("geolocation not supported");
          }
      }
      else {
          alert("Navigator not found");
      }
    }
    
    function successCallback(position)
    {
       setText(position.coords.latitude, "latitude");
       setText(position.coords.longitude, "longitude");
       ready.Value = "true"
    }
</script>

    <asp:HiddenField ID="latitude" runat="server" />
    <asp:HiddenField ID="longitude" runat="server" />    
    <asp:HiddenField ID="ready" runat="server" Value="false"/> 
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
</asp:Content>

