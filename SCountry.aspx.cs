using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SCountry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DirectoUser"].ToString() !="vicente")
        {
            Response.Redirect("Home.aspx", false);
        }

        if (!Page.IsPostBack)
        {

            MECCA2TableAdapters.Special_ConsiderationsAdp adpSc = new MECCA2TableAdapters.Special_ConsiderationsAdp();
            MECCA2.Special_ConsiderationsDataTable tblSc = adpSc.GetData();            
            TextBox1.Text = tblSc.Rows[1][2].ToString();
            TextBox2.Text = tblSc.Rows[1][3].ToString();
            TextBox3.Text = tblSc.Rows[1][4].ToString();
            


        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Label6.Visible = true;
        MECCA2TableAdapters.Special_ConsiderationsAdp adp = new MECCA2TableAdapters.Special_ConsiderationsAdp();
        adp.Delete(2);
        adp.Insert(2, DateTime.Now, TextBox1.Text, TextBox2.Text, TextBox3.Text,"",Session["DirectoUser"].ToString());
    }
}
