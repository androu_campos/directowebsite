using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Office.Interop;
using System.Data.SqlClient;
using ADODB;
using System.Net.Mail;
using System.Net;
using System.Diagnostics;
using System.Net.Mime;
using System.Globalization;
using System.Windows.Forms;

public partial class WSReportsBuilder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //string from = Request.QueryString["from"].ToString();
        //string to = Request.QueryString["to"].ToString();            
        createExcelFile("","");
    }

    protected void createExcelFile(string from,string to)
    {
        object missValue = System.Reflection.Missing.Value;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        char weekLimitData;
        char weekLimitDataB;
        char weekLimitDataC;
        char weekLimitDataD;
        char weekLimitDataE;
        char weekLimitDataF;
        char weekLimitDataG;
        char weekLimitDataH;
        char weekLimitDataK;
        char weekLimitDataN;
        char weekLimitDataFormulas;
        int endColumn = 0;
        int endColumnB = 0;
        int endColumnC = 0;
        int endColumnD = 0;
        int endColumnK = 0;
        int endColumnN = 0;
        int endColumnFormulas = 0;

        DateTime weekdate;
        string wdate;
        weekdate = DateTime.Now.AddDays(((int)(DateTime.Today.DayOfWeek) * -1) - 6);
        wdate = weekdate.ToString("yyyy-MM-dd");
        string ydate = weekdate.Year.ToString();
        string mdate = weekdate.Month.ToString();
        string ddate = weekdate.Day.ToString();
        //MessageBox.Show(wdate);
        //MessageBox.Show(ydate);
        //MessageBox.Show(mdate);
        //MessageBox.Show(ddate);
        
        Microsoft.Office.Interop.Excel.Application xlApp = default(Microsoft.Office.Interop.Excel.Application);
        Microsoft.Office.Interop.Excel.Workbook xlWorkBook = default(Microsoft.Office.Interop.Excel.Workbook);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetA = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetB = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetC = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetD = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetE = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetF = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetG = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetH = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetI = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetJ = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetK = default(Microsoft.Office.Interop.Excel.Worksheet);

        CultureInfo ciCurr = CultureInfo.CurrentCulture;
        int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

        String sToday = DateTime.Now.ToString("MM.dd.yyyy.HH.mm");
        String sPathFiles = "E:\\Mecca\\Reports\\";
        String sStandardFile = sPathFiles + "WeeklyResultsReport.xls";

        String connectionString = ConfigurationManager.ConnectionStrings["MECCA2ConnectionString"].ConnectionString;

        xlApp = new Microsoft.Office.Interop.Excel.ApplicationClass();

        ADODB.Connection DBConnection = new ADODB.Connection();
        ADODB.Recordset rsDataA = new ADODB.Recordset();
        ADODB.Recordset rsDataB = new ADODB.Recordset();
        ADODB.Recordset rsDataC = new ADODB.Recordset();
        ADODB.Recordset rsDataD = new ADODB.Recordset();

        ADODB.Recordset rsDataE = new ADODB.Recordset();
        ADODB.Recordset rsDataF = new ADODB.Recordset();
        ADODB.Recordset rsDataG = new ADODB.Recordset();
        ADODB.Recordset rsDataH = new ADODB.Recordset();
        ADODB.Recordset rsDataI = new ADODB.Recordset();
        ADODB.Recordset rsDataJ = new ADODB.Recordset();
        ADODB.Recordset rsDataK = new ADODB.Recordset();
        ADODB.Recordset rsDataL = new ADODB.Recordset();
        ADODB.Recordset rsDataM = new ADODB.Recordset();
        ADODB.Recordset rsDataN = new ADODB.Recordset(); 

        DBConnection.Open("Provider='SQLOLEDB.1';" + connectionString, "", "", -1);
        DBConnection.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
        DBConnection.CommandTimeout = 300;

        rsDataA.Open("EXEC Mecca2.dbo.sp_CCReportWeeklyResults WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataB.Open("EXEC Mecca2.dbo.sp_CCReportWeeklyProductResults WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataC.Open("EXEC Mecca2.dbo.sp_CCReportMonthlyResults WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataD.Open("EXEC Mecca2.dbo.sp_CCReportMonthlyProductResults WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataE.Open("EXEC Mecca2.dbo.sp_CCReportWeeklyResults_Margin WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataF.Open("EXEC Mecca2.dbo.sp_CCReportWeeklyProductResults_Margin WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataG.Open("EXEC Mecca2.dbo.sp_CCReportMonthlyResults_Margin WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataH.Open("EXEC Mecca2.dbo.sp_CCReportMonthlyProductResults_Margin WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataI.Open("SELECT AccountManager,SUM(Profit) 'Profit' FROM mecca2.dbo.CC_AMReport WHERE WeekNumber = case when datepart(week,dateadd(day,0,left(getdate(),11))) = 1 then 52 else datepart(week,dateadd(day,0,left(getdate(),11))) - 1 end AND Type = 'Custbroker' AND datepart(year,startdate)= '" + ydate + "' and datepart(month,startdate)= '" + mdate + "' and datepart(day,startdate)= '" + ddate + "' GROUP BY AccountManager", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        MessageBox.Show(rsDataI.Source.ToString());
        rsDataJ.Open("SELECT AccountManager,SUM(Profit) 'Profit' FROM mecca2.dbo.CC_AMReport WHERE WeekNumber = case when datepart(week,dateadd(day,0,left(getdate(),11))) = 1 then 52 else datepart(week,dateadd(day,0,left(getdate(),11))) - 1 end AND Type = 'Vendorbroker' AND datepart(year,startdate)= '" + ydate + "' and datepart(month,startdate)= '" + mdate + "' and datepart(day,startdate)= '" + ddate + "'  GROUP BY AccountManager", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataK.Open("EXEC Mecca2.dbo.sp_CCReportMonthlyResultsCC WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataL.Open("SELECT AccountManager, SUM(Profit) 'Profit' FROM mecca2.dbo.CC_AMReportMonth WHERE [Month] = case when datepart(month,dateadd(day,0,left(getdate(),11))) = 1 then 12 else datepart(month,dateadd(day,0,left(getdate(),11))) - 1 end AND [Year] = case when datepart(month,dateadd(day,0,left(getdate(),11))) = 1 then datepart(year,dateadd(day,0,left(getdate(),11))) - 1 else datepart(year,dateadd(day,0,left(getdate(),11))) end AND [Type] = 'Custbroker' AND AccountManager <> 'N/A' AND datepart(year,startdate)= '" + ydate + "' and datepart(month,startdate)= '" + mdate + "' and datepart(day,startdate)= '" + ddate + "'  GROUP BY AccountManager ", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataM.Open("SELECT AccountManager, SUM(Profit) 'Profit' FROM mecca2.dbo.CC_AMReportMonth WHERE [Month] = case when datepart(month,dateadd(day,0,left(getdate(),11))) = 1 then 12 else datepart(month,dateadd(day,0,left(getdate(),11))) - 1 end AND [Year] = case when datepart(month,dateadd(day,0,left(getdate(),11))) = 1 then datepart(year,dateadd(day,0,left(getdate(),11))) - 1 else datepart(year,dateadd(day,0,left(getdate(),11))) end AND [Type] = 'Vendorbroker' AND AccountManager <> 'N/A' AND datepart(year,startdate)= '" + ydate + "' and datepart(month,startdate)= '" + mdate + "' and datepart(day,startdate)= '" + ddate + "'  GROUP BY AccountManager ", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataN.Open("EXEC Mecca2.dbo.sp_CCWeeklyProfitIncrease WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);

        xlWorkBook = xlApp.Workbooks.Open(sStandardFile, 0, false, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", false, false, 0, false, false, 0);
        xlWorkSheetA = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        xlWorkSheetB = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
        xlWorkSheetC = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(3);
        xlWorkSheetD = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(4);
        xlWorkSheetE = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(5);
        xlWorkSheetF = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(6);
        xlWorkSheetG = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(7);
        xlWorkSheetH = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(8);
        xlWorkSheetI = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(9);
        xlWorkSheetK = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(10);

        Microsoft.Office.Interop.Excel.Range tblRangeA = xlWorkSheetA.get_Range("A2", "A2");
        Microsoft.Office.Interop.Excel.Range tblRangeB = xlWorkSheetA.get_Range("A51", "A51");
        Microsoft.Office.Interop.Excel.Range tblRangeC = xlWorkSheetB.get_Range("A2", "A2");
        Microsoft.Office.Interop.Excel.Range tblRangeD = xlWorkSheetB.get_Range("A51", "A51");

        Microsoft.Office.Interop.Excel.Range tblRangeI = xlWorkSheetE.get_Range("A2", "A2");
        Microsoft.Office.Interop.Excel.Range tblRangeJ = xlWorkSheetF.get_Range("A2", "A2");
        Microsoft.Office.Interop.Excel.Range tblRangeK = xlWorkSheetG.get_Range("A2", "A2");
        Microsoft.Office.Interop.Excel.Range tblRangeL = xlWorkSheetH.get_Range("A2", "A2");
        Microsoft.Office.Interop.Excel.Range tblRangeM = xlWorkSheetI.get_Range("A2", "A2");
        Microsoft.Office.Interop.Excel.Range tblRangeN = xlWorkSheetK.get_Range("A2", "A2"); 

        int iDataA = tblRangeA.CopyFromRecordset(rsDataA, missValue, missValue);
        int iDataB = tblRangeB.CopyFromRecordset(rsDataB, missValue, missValue);
        int iDataC = tblRangeC.CopyFromRecordset(rsDataC, missValue, missValue);
        int iDataD = tblRangeD.CopyFromRecordset(rsDataD, missValue, missValue);

        int iDataI = tblRangeI.CopyFromRecordset(rsDataI, missValue, missValue);
        int iDataJ = tblRangeJ.CopyFromRecordset(rsDataJ, missValue, missValue);
        int iDataK = tblRangeK.CopyFromRecordset(rsDataK, missValue, missValue);
        int iDataL = tblRangeL.CopyFromRecordset(rsDataL, missValue, missValue);
        int iDataM = tblRangeM.CopyFromRecordset(rsDataM, missValue, missValue);
        int iDataN = tblRangeN.CopyFromRecordset(rsDataN, missValue, missValue);

        tblRangeB.Columns.AutoFit();

        int headersCountA = rsDataA.Fields.Count;
        int headersCountB = rsDataB.Fields.Count;
        int headersCountC = rsDataC.Fields.Count;
        int headersCountD = rsDataD.Fields.Count;
        int headersCountE = rsDataE.Fields.Count;
        int headersCountF = rsDataF.Fields.Count;
        int headersCountG = rsDataG.Fields.Count;
        int headersCountH = rsDataH.Fields.Count;
        int headersCountI = rsDataI.Fields.Count;
        int headersCountJ = rsDataJ.Fields.Count;
        int headersCountK = rsDataK.Fields.Count;
        int headersCountN = rsDataN.Fields.Count;

        

        endColumn = 65 + headersCountA - 1;
        endColumnB = 65 + headersCountB - 1;
        endColumnC = 65 + headersCountC - 1;
        endColumnD = 65 + headersCountD - 1;
        endColumnFormulas = endColumn + 4;
        endColumnK = 65 + headersCountK - 1;
        endColumnN = 65 + headersCountN - 1;

        weekLimitData = Convert.ToChar(endColumn);
        weekLimitDataB = Convert.ToChar(endColumnB);
        weekLimitDataC = Convert.ToChar(endColumnC);
        weekLimitDataD = Convert.ToChar(endColumnD);
        weekLimitDataE = Convert.ToChar(endColumn-4);
        weekLimitDataF = Convert.ToChar(endColumnB - 4);
        weekLimitDataG = Convert.ToChar(endColumnC - 3);
        weekLimitDataH = Convert.ToChar(endColumnD - 3);
        weekLimitDataK = Convert.ToChar(endColumnK);
        weekLimitDataN = Convert.ToChar(endColumnN);
        weekLimitDataFormulas = Convert.ToChar(endColumnFormulas);


        Microsoft.Office.Interop.Excel.Range tblRangeE = xlWorkSheetA.get_Range("A"+(3 + iDataA), (weekLimitDataE.ToString()) + (3 + iDataA));
        int iDataE = tblRangeE.CopyFromRecordset(rsDataE, missValue, missValue);

        Microsoft.Office.Interop.Excel.Range tblRangeF = xlWorkSheetA.get_Range("A" + (3 + iDataB + 49), (weekLimitDataF.ToString()) + (3 + iDataB + 49));
        int iDataF = tblRangeF.CopyFromRecordset(rsDataF, missValue, missValue);

        Microsoft.Office.Interop.Excel.Range tblRangeG = xlWorkSheetB.get_Range("A" + (3 + iDataC), (weekLimitDataG.ToString()) + (3 + iDataC));
        int iDataG = tblRangeG.CopyFromRecordset(rsDataG, missValue, missValue);

        Microsoft.Office.Interop.Excel.Range tblRangeH = xlWorkSheetB.get_Range("A" + (3 + iDataD + 49), (weekLimitDataH.ToString()) + (3 + iDataD + 49));
        int iDataH = tblRangeH.CopyFromRecordset(rsDataH, missValue, missValue);

        //Microsoft.Office.Interop.Excel.Range tblRangeI = xlWorkSheetE.get_Range("A1", "B" + (iDataI));
        //int iDataI = tblRangeI.CopyFromRecordset(rsDataI, missValue, missValue);

        //Microsoft.Office.Interop.Excel.Range tblRangeJ = xlWorkSheetE.get_Range("A1", "B" + (iDataJ));
        //int iDataJ = tblRangeJ.CopyFromRecordset(rsDataJ, missValue, missValue);



        xlApp.get_Range("A2", sb.ToString() + weekLimitData.ToString() + "2").Copy(missValue);

        Microsoft.Office.Interop.Excel.Range tblDataA = xlWorkSheetA.get_Range("A2", weekLimitData.ToString() + (1 + iDataA));
        Microsoft.Office.Interop.Excel.Range tblDataB = xlWorkSheetB.get_Range("A2", weekLimitDataB.ToString() + (1 + iDataB));
        Microsoft.Office.Interop.Excel.Range tblDataC = xlWorkSheetC.get_Range("A2", weekLimitDataC.ToString() + (1 + iDataC));
        Microsoft.Office.Interop.Excel.Range tblDataD = xlWorkSheetD.get_Range("A2", weekLimitDataD.ToString() + (1 + iDataD));
        //Microsoft.Office.Interop.Excel.Range tblDataD = xlWorkSheetD.get_Range("A2", "AM" + (1 + iDataD));
        //Microsoft.Office.Interop.Excel.Range tblDataE = xlWorkSheetE.get_Range("A2", "AM" + (1 + iDataE));
        //Microsoft.Office.Interop.Excel.Range tblDataF = xlWorkSheetF.get_Range("A2", "L" + (1 + iDataF));

        tblDataA.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        tblDataB.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        tblDataC.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        tblDataD.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        //tblDataE.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        //tblDataF.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);


        xlWorkSheetA.get_Range("A1", weekLimitData.ToString() + "1").Font.Bold = true;
        xlWorkSheetA.get_Range("A50", weekLimitDataB.ToString() + "50").Font.Bold = true;        
        xlWorkSheetB.get_Range("A1", weekLimitDataC.ToString() + "1").Font.Bold = true;
        xlWorkSheetB.get_Range("A50", weekLimitDataD.ToString() + "50").Font.Bold = true;

        xlWorkSheetA.get_Range("A" + (iDataA + 2), weekLimitData.ToString() + (iDataA + 5)).Font.Bold = true;
        xlWorkSheetA.get_Range("A" + (49 + iDataB + 2), weekLimitData.ToString() + (49 + iDataB + 3)).Font.Bold = true;

        //xlWorkSheetD.get_Range("A1", "AM1").Font.Bold = true;
        //xlWorkSheetE.get_Range("A1", "AM1").Font.Bold = true;
        //xlWorkSheetF.get_Range("A1", "L1").Font.Bold = true;

        xlWorkSheetA.get_Range("A1", weekLimitData.ToString() + "1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheetA.get_Range("A1", weekLimitData.ToString() + "1").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

        xlWorkSheetA.get_Range("A50", weekLimitDataB.ToString() + "50").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheetA.get_Range("A50", weekLimitDataB.ToString() + "50").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

        xlWorkSheetB.get_Range("A1", weekLimitDataC.ToString() + "1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheetB.get_Range("A1", weekLimitDataC.ToString() + "1").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;


        xlWorkSheetB.get_Range("A50", weekLimitDataD.ToString() + "50").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheetB.get_Range("A50", weekLimitDataD.ToString() + "50").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //xlWorkSheetB.get_Range("A1", "AJ1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        //xlWorkSheetC.get_Range("A1", "L1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        //xlWorkSheetD.get_Range("A1", "AM1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        //xlWorkSheetE.get_Range("A1", "AM1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        //xlWorkSheetF.get_Range("A1", "L1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

        xlWorkSheetA.get_Range("B2", weekLimitData.ToString() + (5 + iDataA)).NumberFormat = "#,##0;[Red]-#,##0";
        xlWorkSheetA.get_Range("B51", weekLimitDataB.ToString() + (2 + iDataB + 50)).NumberFormat = "#,##0;[Red]-#,##0";
        xlWorkSheetB.get_Range("B2", weekLimitDataC.ToString() + (2 + iDataC)).NumberFormat = "#,##0;[Red]-#,##0";
        xlWorkSheetB.get_Range("B51", weekLimitDataD.ToString() + (2 + iDataD + 50)).NumberFormat = "#,##0;[Red]-#,##0";
        //xlWorkSheet.get_Range("K2", "N" + (2 + iData)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheet.get_Range("U2", "X" + (2 + iData)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheet.get_Range("AE2", "AH" + (2 + iData)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheet.get_Range("F2", "F" + (1 + iData)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheet.get_Range("O2", "P" + (1 + iData)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheet.get_Range("Y2", "Z" + (1 + iData)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheet.get_Range("AI2", "AJ" + (1 + iData)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheet.get_Range("I2", "J" + (1 + iData)).NumberFormat = "0.0000";
        //xlWorkSheet.get_Range("S2", "T" + (1 + iData)).NumberFormat = "0.0000";
        //xlWorkSheet.get_Range("AC2", "AD" + (1 + iData)).NumberFormat = "0.0000";

        
        /************* Total Profit *********************/

        char n = 'B';

        xlWorkSheetA.get_Range("A" + (2 + iDataA), "A" + (2 + iDataA)).Value2 = "Total Profit";
        xlWorkSheetA.get_Range("A" + (1 + iDataB + 50), "A" + (1 + iDataB + 50)).Value2 = "Total Profit";

        for (int y = 0; y < headersCountA - 3 ; y++)
        {
            xlWorkSheetA.get_Range(n.ToString() + (2 + iDataA), n.ToString() + (2 + iDataA)).Formula = "=SUBTOTAL(109,"+n.ToString()+"2:"+n.ToString() + (1 + iDataA) + ")";
            xlWorkSheetA.get_Range(n.ToString() + (3 + iDataA), n.ToString() + (3 + iDataA)).NumberFormat = "0.0%_);[Red](0.0%)";

            xlWorkSheetA.get_Range(n.ToString() + (1 + iDataB + 50), n.ToString() + (1 + iDataB + 50)).Formula = "=SUBTOTAL(109," + n.ToString() + "51:" + n.ToString() + (iDataB + 50) + ")";
            xlWorkSheetA.get_Range(n.ToString() + (3 + iDataB + 49), n.ToString() + (3 + iDataB + 49)).NumberFormat = "0.0%_);[Red](0.0%)";
            n++;
        }

        //n = 'B';

        //xlWorkSheetB.get_Range("A" + (2 + iDataB), "A" + (2 + iDataB)).Value2 = "Total Profit";

        //for (int y = 0; y < headersCountB - 1; y++)
        //{
        //    xlWorkSheetB.get_Range(n.ToString() + (2 + iDataB), n.ToString() + (2 + iDataB)).Formula = "=SUBTOTAL(109," + n.ToString() + "2:" + n.ToString() + (1 + iDataB) + ")";
        //    n++;
        //}

        n = 'B';

        xlWorkSheetB.get_Range("A" + (2 + iDataC), "A" + (2 + iDataC)).Value2 = "Total Profit";
        xlWorkSheetB.get_Range("A" + (1 + iDataD+ 50), "A" + (1 + iDataD + 50)).Value2 = "Total Profit";

        for (int y = 0; y < headersCountC - 1; y++)
        {
            if ((y%2) == 0 || y == 0)
            {
                xlWorkSheetB.get_Range(n.ToString() + (2 + iDataC), n.ToString() + (2 + iDataC)).Formula = "=SUBTOTAL(109," + n.ToString() + "2:" + n.ToString() + (1 + iDataC) + ")";
                xlWorkSheetB.get_Range(n.ToString() + (3 + iDataC), n.ToString() + (3 + iDataC)).NumberFormat = "0.0%_);[Red](0.0%)";

                xlWorkSheetB.get_Range(n.ToString() + (2 + iDataD + 49), n.ToString() + (2 + iDataD + 49)).Formula = "=SUBTOTAL(109," + n.ToString() + "51:" + n.ToString() + (1 + iDataD + 49) + ")";
                xlWorkSheetB.get_Range(n.ToString() + (3 + iDataD + 49), n.ToString() + (3 + iDataD + 49)).NumberFormat = "0.0%_);[Red](0.0%)";
            }            
            n++;
        }
        n = 'B';

        xlWorkSheetD.get_Range("A" + (2 + iDataD), "A" + (2 + iDataD)).Value2 = "Total Profit";

        for (int y = 0; y < headersCountD - 1; y++)
        {
            xlWorkSheetD.get_Range(n.ToString() + (2 + iDataD), n.ToString() + (2 + iDataD)).Formula = "=SUBTOTAL(109," + n.ToString() + "2:" + n.ToString() + (1 + iDataD) + ")";
            n++;
        }


        for (int x = 0; x < iDataA + 4 ; x++)
        {
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString(x + 2) + "-" + Convert.ToChar(endColumn - 5).ToString() + Convert.ToString(x + 2);
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2) + "/" + Convert.ToChar(endColumn - 5).ToString() + Convert.ToString(x + 2);


            xlWorkSheetA.get_Range(Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 1).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 1).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            
        }

        for (int x = 0; x < iDataA + 1; x++)
        {
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString(x + 2) + "/" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString((2 + iDataA));
        }

        for (int x = 49; x < (iDataB + 51); x++)
        {
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString(x + 2) + "-" + Convert.ToChar(endColumn - 5).ToString() + Convert.ToString(x + 2);
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 3).ToString() + Convert.ToString(x + 2) + "/" + Convert.ToChar(endColumn - 5).ToString() + Convert.ToString(x + 2);
            //xlWorkSheetB.get_Range(Convert.ToChar(endColumnB).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumnB).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumnB - 4).ToString() + Convert.ToString(x + 2) + "/" + Convert.ToChar(endColumnB - 4).ToString() + Convert.ToString((2 + iDataB));

            xlWorkSheetA.get_Range(Convert.ToChar(endColumnB).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumnB).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            xlWorkSheetA.get_Range(Convert.ToChar(endColumnB - 1).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumnB - 1).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            xlWorkSheetA.get_Range(Convert.ToChar(endColumnB - 2).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumnB - 2).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";

        }

        for (int x = 49; x < iDataB + 50; x++)
        {
            xlWorkSheetA.get_Range(Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString(x + 2) + "/" + Convert.ToChar(endColumn - 4).ToString() + Convert.ToString((2 + iDataA));
        }

        n = 'C';

        for (int v = 0; v < headersCountC - 3; v++)
        {
            for (int w = 0; w < iDataC; w++)
            {
                xlWorkSheetB.get_Range(n.ToString() + Convert.ToString(w + 2), n.ToString() + Convert.ToString(w + 2)).Formula = "=" + Convert.ToChar(Convert.ToInt32(n) - 1).ToString() + Convert.ToString(w + 2) + "/" + Convert.ToChar(Convert.ToInt32(n) - 1).ToString() + Convert.ToString((2 + iDataC));
                xlWorkSheetB.get_Range(n.ToString() + Convert.ToString(w + 2), n.ToString() + Convert.ToString(w + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
                                
                //xlWorkSheetC.get_Range(Convert.ToChar(endColumn - 1).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 1).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
                //xlWorkSheetC.get_Range(Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            }
            v = v + 2 ;
            n = Convert.ToChar(v + 67);
            v--;

        }

        //for (int v = 0; v < iDataC + 1; v++)
        //{
        //    xlWorkSheetB.get_Range(Convert.ToChar(endColumnC).ToString() + Convert.ToString(v + 2), Convert.ToChar(endColumnC).ToString() + Convert.ToString(v + 2)).Formula = "=" + Convert.ToChar(endColumnC - 1).ToString() + Convert.ToString(v + 2) + "/" + Convert.ToChar(endColumnC - 5).ToString() + Convert.ToString(v + 2);
        //    xlWorkSheetB.get_Range(Convert.ToChar(endColumnC).ToString() + Convert.ToString(v + 2), Convert.ToChar(endColumnC).ToString() + Convert.ToString(v + 2)).NumberFormat = "0.0%_);[Red](0.0%)";            
        //}

        for (int v = 0; v < headersCountK; v++)
        {
            xlWorkSheetG.get_Range(Convert.ToChar(66+v).ToString()+ (iDataK+2), Convert.ToChar(66+v).ToString()+ (iDataK+2)).Formula = "=SUBTOTAL(109," + Convert.ToChar(66+v).ToString()+ "2:" + Convert.ToChar(66+v).ToString()+ (iDataK+1);            
        }

        n = 'C';

        for (int v = 0; v < headersCountD - 3; v++)
        {
            for (int w = 49; w < (iDataD + 49); w++)
            {
                xlWorkSheetB.get_Range(n.ToString() + Convert.ToString(w + 2), n.ToString() + Convert.ToString(w + 2)).Formula = "=" + Convert.ToChar(Convert.ToInt32(n) - 1).ToString() + Convert.ToString(w + 2) + "/" + Convert.ToChar(Convert.ToInt32(n) - 1).ToString() + Convert.ToString((2 + iDataD+ 49));
                xlWorkSheetB.get_Range(n.ToString() + Convert.ToString(w + 2), n.ToString() + Convert.ToString(w + 2)).NumberFormat = "0.0%_);[Red](0.0%)";

                //xlWorkSheetC.get_Range(Convert.ToChar(endColumn - 1).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 1).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
                //xlWorkSheetC.get_Range(Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumn - 2).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
            }
            v = v + 2;
            n = Convert.ToChar(v + 67);
            v--;

        }

        //for (int x = 49; x < iDataD + 50; x++)
        //{
        //    xlWorkSheetB.get_Range(Convert.ToChar(endColumnD).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumnD).ToString() + Convert.ToString(x + 2)).Formula = "=" + Convert.ToChar(endColumnD - 1).ToString() + Convert.ToString(x + 2) + "/" + Convert.ToChar(endColumnD - 5).ToString() + Convert.ToString((x+2));
        //    xlWorkSheetB.get_Range(Convert.ToChar(endColumnD).ToString() + Convert.ToString(x + 2), Convert.ToChar(endColumnD).ToString() + Convert.ToString(x + 2)).NumberFormat = "0.0%_);[Red](0.0%)";
        //}

        xlWorkSheetB.get_Range(n.ToString()+"2", n.ToString() + (2 + iDataC)).NumberFormat = "0.0%_);[Red](0.0%)";
        xlWorkSheetB.get_Range("A" + (iDataC + 2), Convert.ToChar(endColumnC).ToString() + (iDataC + 3)).Font.Bold = true;

        xlWorkSheetB.get_Range("A" + (iDataD + 51), Convert.ToChar(endColumnD).ToString() + (iDataD + 52)).Font.Bold = true;

        //xlWorkSheet.get_Range("C" + (2 + iData), "C" + (2 + iData)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iData) + ")";
        //xlWorkSheet.get_Range("D" + (2 + iData), "D" + (2 + iData)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iData) + ")";
        //xlWorkSheet.get_Range("E" + (2 + iData), "E" + (2 + iData)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iData) + ")";

        //xlWorkSheet.get_Range("K" + (2 + iData), "K" + (2 + iData)).Formula = "=SUBTOTAL(109,K2:K" + (1 + iData) + ")";
        //xlWorkSheet.get_Range("L" + (2 + iData), "L" + (2 + iData)).Formula = "=SUBTOTAL(109,L2:L" + (1 + iData) + ")";
        //xlWorkSheet.get_Range("M" + (2 + iData), "M" + (2 + iData)).Formula = "=SUBTOTAL(109,M2:M" + (1 + iData) + ")";
        //xlWorkSheet.get_Range("N" + (2 + iData), "N" + (2 + iData)).Formula = "=SUBTOTAL(109,N2:N" + (1 + iData) + ")";

        //xlWorkSheet.get_Range("U" + (2 + iData), "U" + (2 + iData)).Formula = "=SUBTOTAL(109,U2:U" + (1 + iData) + ")";
        //xlWorkSheet.get_Range("V" + (2 + iData), "V" + (2 + iData)).Formula = "=SUBTOTAL(109,V2:V" + (1 + iData) + ")";
        //xlWorkSheet.get_Range("W" + (2 + iData), "W" + (2 + iData)).Formula = "=SUBTOTAL(109,W2:W" + (1 + iData) + ")";
        //xlWorkSheet.get_Range("X" + (2 + iData), "X" + (2 + iData)).Formula = "=SUBTOTAL(109,X2:X" + (1 + iData) + ")";

        //xlWorkSheet.get_Range("AE" + (2 + iData), "AE" + (2 + iData)).Formula = "=SUBTOTAL(109,AE2:AE" + (1 + iData) + ")";
        //xlWorkSheet.get_Range("AF" + (2 + iData), "AF" + (2 + iData)).Formula = "=SUBTOTAL(109,AF2:AF" + (1 + iData) + ")";
        //xlWorkSheet.get_Range("AG" + (2 + iData), "AG" + (2 + iData)).Formula = "=SUBTOTAL(109,AG2:AG" + (1 + iData) + ")";
        //xlWorkSheet.get_Range("AH" + (2 + iData), "AH" + (2 + iData)).Formula = "=SUBTOTAL(109,AH2:AH" + (1 + iData) + ")";
        
        //xlWorkSheetB.get_Range("C2", "E" + (2 + iDataB)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetB.get_Range("K2", "N" + (2 + iDataB)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetB.get_Range("U2", "X" + (2 + iDataB)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetB.get_Range("AE2", "AH" + (2 + iDataB)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetB.get_Range("F2", "F" + (1 + iDataB)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetB.get_Range("O2", "P" + (1 + iDataB)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetB.get_Range("Y2", "Z" + (1 + iDataB)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetB.get_Range("AI2", "AJ" + (1 + iDataB)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetB.get_Range("I2", "J" + (1 + iDataB)).NumberFormat = "0.0000";
        //xlWorkSheetB.get_Range("S2", "T" + (1 + iDataB)).NumberFormat = "0.0000";
        //xlWorkSheetB.get_Range("AC2", "AD" + (1 + iDataB)).NumberFormat = "0.0000";

        //xlWorkSheetB.get_Range("C" + (2 + iDataB), "C" + (2 + iDataB)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iDataB) + ")";
        //xlWorkSheetB.get_Range("D" + (2 + iDataB), "D" + (2 + iDataB)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iDataB) + ")";
        //xlWorkSheetB.get_Range("E" + (2 + iDataB), "E" + (2 + iDataB)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iDataB) + ")";

        //xlWorkSheetB.get_Range("K" + (2 + iDataB), "K" + (2 + iDataB)).Formula = "=SUBTOTAL(109,K2:K" + (1 + iDataB) + ")";
        //xlWorkSheetB.get_Range("L" + (2 + iDataB), "L" + (2 + iDataB)).Formula = "=SUBTOTAL(109,L2:L" + (1 + iDataB) + ")";
        //xlWorkSheetB.get_Range("M" + (2 + iDataB), "M" + (2 + iDataB)).Formula = "=SUBTOTAL(109,M2:M" + (1 + iDataB) + ")";
        //xlWorkSheetB.get_Range("N" + (2 + iDataB), "N" + (2 + iDataB)).Formula = "=SUBTOTAL(109,N2:N" + (1 + iDataB) + ")";

        //xlWorkSheetB.get_Range("U" + (2 + iDataB), "U" + (2 + iDataB)).Formula = "=SUBTOTAL(109,U2:U" + (1 + iDataB) + ")";
        //xlWorkSheetB.get_Range("V" + (2 + iDataB), "V" + (2 + iDataB)).Formula = "=SUBTOTAL(109,V2:V" + (1 + iDataB) + ")";
        //xlWorkSheetB.get_Range("W" + (2 + iDataB), "W" + (2 + iDataB)).Formula = "=SUBTOTAL(109,W2:W" + (1 + iDataB) + ")";
        //xlWorkSheetB.get_Range("X" + (2 + iDataB), "X" + (2 + iDataB)).Formula = "=SUBTOTAL(109,X2:X" + (1 + iDataB) + ")";

        //xlWorkSheetB.get_Range("AE" + (2 + iDataB), "AE" + (2 + iDataB)).Formula = "=SUBTOTAL(109,AE2:AE" + (1 + iDataB) + ")";
        //xlWorkSheetB.get_Range("AF" + (2 + iDataB), "AF" + (2 + iDataB)).Formula = "=SUBTOTAL(109,AF2:AF" + (1 + iDataB) + ")";
        //xlWorkSheetB.get_Range("AG" + (2 + iDataB), "AG" + (2 + iDataB)).Formula = "=SUBTOTAL(109,AG2:AG" + (1 + iDataB) + ")";
        //xlWorkSheetB.get_Range("AH" + (2 + iDataB), "AH" + (2 + iDataB)).Formula = "=SUBTOTAL(109,AH2:AH" + (1 + iDataB) + ")";        


        //xlWorkSheetC.get_Range("C2", "F" + (2 + iDataC)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetC.get_Range("K2", "L" + (1 + iDataC)).NumberFormat = "0.0000";
        //xlWorkSheetC.get_Range("H2", "J" + (1 + iDataC)).NumberFormat = "0.00";
        //xlWorkSheetC.get_Range("G2", "G" + (1 + iDataC)).NumberFormat = "0.0%_);[Red](0.0%)";

        //xlWorkSheetC.get_Range("C" + (2 + iDataC), "C" + (2 + iDataC)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iDataC) + ")";
        //xlWorkSheetC.get_Range("D" + (2 + iDataC), "D" + (2 + iDataC)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iDataC) + ")";
        //xlWorkSheetC.get_Range("E" + (2 + iDataC), "E" + (2 + iDataC)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iDataC) + ")";
        //xlWorkSheetC.get_Range("F" + (2 + iDataC), "F" + (2 + iDataC)).Formula = "=SUBTOTAL(109,F2:F" + (1 + iDataC) + ")";


        //xlWorkSheetD.get_Range("C2", "E" + (2 + iDataD)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetD.get_Range("L2", "O" + (2 + iDataD)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetD.get_Range("W2", "Z" + (2 + iDataD)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetD.get_Range("AH2", "AK" + (2 + iDataD)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetD.get_Range("F2", "F" + (1 + iDataD)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetD.get_Range("P2", "Q" + (1 + iDataD)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetD.get_Range("AA2", "AB" + (1 + iDataD)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetD.get_Range("AL2", "AM" + (1 + iDataD)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetD.get_Range("J2", "K" + (1 + iDataD)).NumberFormat = "0.0000";
        //xlWorkSheetD.get_Range("U2", "V" + (1 + iDataD)).NumberFormat = "0.0000";
        //xlWorkSheetD.get_Range("AF2", "AG" + (1 + iDataD)).NumberFormat = "0.0000";

        //xlWorkSheetD.get_Range("C" + (2 + iDataD), "C" + (2 + iDataD)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iDataD) + ")";
        //xlWorkSheetD.get_Range("D" + (2 + iDataD), "D" + (2 + iDataD)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iDataD) + ")";
        //xlWorkSheetD.get_Range("E" + (2 + iDataD), "E" + (2 + iDataD)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iDataD) + ")";

        //xlWorkSheetD.get_Range("L" + (2 + iDataD), "L" + (2 + iDataD)).Formula = "=SUBTOTAL(109,L2:L" + (1 + iDataD) + ")";
        //xlWorkSheetD.get_Range("M" + (2 + iDataD), "M" + (2 + iDataD)).Formula = "=SUBTOTAL(109,M2:M" + (1 + iDataD) + ")";
        //xlWorkSheetD.get_Range("N" + (2 + iDataD), "N" + (2 + iDataD)).Formula = "=SUBTOTAL(109,N2:N" + (1 + iDataD) + ")";
        //xlWorkSheetD.get_Range("O" + (2 + iDataD), "O" + (2 + iDataD)).Formula = "=SUBTOTAL(109,O2:O" + (1 + iDataD) + ")";

        //xlWorkSheetD.get_Range("W" + (2 + iDataD), "W" + (2 + iDataD)).Formula = "=SUBTOTAL(109,W2:W" + (1 + iDataD) + ")";
        //xlWorkSheetD.get_Range("X" + (2 + iDataD), "X" + (2 + iDataD)).Formula = "=SUBTOTAL(109,X2:X" + (1 + iDataD) + ")";
        //xlWorkSheetD.get_Range("Y" + (2 + iDataD), "Y" + (2 + iDataD)).Formula = "=SUBTOTAL(109,Y2:Y" + (1 + iDataD) + ")";
        //xlWorkSheetD.get_Range("Z" + (2 + iDataD), "Z" + (2 + iDataD)).Formula = "=SUBTOTAL(109,Z2:Z" + (1 + iDataD) + ")";

        //xlWorkSheetD.get_Range("AH" + (2 + iDataD), "AH" + (2 + iDataD)).Formula = "=SUBTOTAL(109,AH2:AH" + (1 + iDataD) + ")";
        //xlWorkSheetD.get_Range("AI" + (2 + iDataD), "AI" + (2 + iDataD)).Formula = "=SUBTOTAL(109,AI2:AI" + (1 + iDataD) + ")";
        //xlWorkSheetD.get_Range("AJ" + (2 + iDataD), "AJ" + (2 + iDataD)).Formula = "=SUBTOTAL(109,AJ2:AJ" + (1 + iDataD) + ")";
        //xlWorkSheetD.get_Range("AK" + (2 + iDataD), "AK" + (2 + iDataD)).Formula = "=SUBTOTAL(109,AK2:AK" + (1 + iDataD) + ")";        


        //xlWorkSheetE.get_Range("C2", "E" + (2 + iDataE)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetE.get_Range("L2", "O" + (2 + iDataE)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetE.get_Range("W2", "Z" + (2 + iDataE)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetE.get_Range("AH2", "AK" + (2 + iDataE)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetE.get_Range("F2", "F" + (1 + iDataE)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetE.get_Range("P2", "Q" + (1 + iDataE)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetE.get_Range("AA2", "AB" + (1 + iDataE)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetE.get_Range("AL2", "AM" + (1 + iDataE)).NumberFormat = "0.0%_);[Red](0.0%)";
        //xlWorkSheetE.get_Range("J2", "K" + (1 + iDataE)).NumberFormat = "0.0000";
        //xlWorkSheetE.get_Range("U2", "V" + (1 + iDataE)).NumberFormat = "0.0000";
        //xlWorkSheetE.get_Range("AF2", "AG" + (1 + iDataE)).NumberFormat = "0.0000";

        //xlWorkSheetE.get_Range("C" + (2 + iDataE), "C" + (2 + iDataE)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iDataE) + ")";
        //xlWorkSheetE.get_Range("D" + (2 + iDataE), "D" + (2 + iDataE)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iDataE) + ")";
        //xlWorkSheetE.get_Range("E" + (2 + iDataE), "E" + (2 + iDataE)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iDataE) + ")";

        //xlWorkSheetE.get_Range("L" + (2 + iDataE), "L" + (2 + iDataE)).Formula = "=SUBTOTAL(109,L2:L" + (1 + iDataE) + ")";
        //xlWorkSheetE.get_Range("M" + (2 + iDataE), "M" + (2 + iDataE)).Formula = "=SUBTOTAL(109,M2:M" + (1 + iDataE) + ")";
        //xlWorkSheetE.get_Range("N" + (2 + iDataE), "N" + (2 + iDataE)).Formula = "=SUBTOTAL(109,N2:N" + (1 + iDataE) + ")";
        //xlWorkSheetE.get_Range("O" + (2 + iDataE), "O" + (2 + iDataE)).Formula = "=SUBTOTAL(109,O2:O" + (1 + iDataE) + ")";

        //xlWorkSheetE.get_Range("W" + (2 + iDataE), "W" + (2 + iDataE)).Formula = "=SUBTOTAL(109,W2:W" + (1 + iDataE) + ")";
        //xlWorkSheetE.get_Range("X" + (2 + iDataE), "X" + (2 + iDataE)).Formula = "=SUBTOTAL(109,X2:X" + (1 + iDataE) + ")";
        //xlWorkSheetE.get_Range("Y" + (2 + iDataE), "Y" + (2 + iDataE)).Formula = "=SUBTOTAL(109,Y2:Y" + (1 + iDataE) + ")";
        //xlWorkSheetE.get_Range("Z" + (2 + iDataE), "Z" + (2 + iDataE)).Formula = "=SUBTOTAL(109,Z2:Z" + (1 + iDataE) + ")";

        //xlWorkSheetE.get_Range("AH" + (2 + iDataE), "AH" + (2 + iDataE)).Formula = "=SUBTOTAL(109,AH2:AH" + (1 + iDataE) + ")";
        //xlWorkSheetE.get_Range("AI" + (2 + iDataE), "AI" + (2 + iDataE)).Formula = "=SUBTOTAL(109,AI2:AI" + (1 + iDataE) + ")";
        //xlWorkSheetE.get_Range("AJ" + (2 + iDataE), "AJ" + (2 + iDataE)).Formula = "=SUBTOTAL(109,AJ2:AJ" + (1 + iDataE) + ")";
        //xlWorkSheetE.get_Range("AK" + (2 + iDataE), "AK" + (2 + iDataE)).Formula = "=SUBTOTAL(109,AK2:AK" + (1 + iDataE) + ")";

        //xlWorkSheetF.get_Range("C2", "F" + (2 + iDataF)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //xlWorkSheetF.get_Range("K2", "L" + (1 + iDataF)).NumberFormat = "0.0000";
        //xlWorkSheetF.get_Range("H2", "J" + (1 + iDataF)).NumberFormat = "0.00";
        //xlWorkSheetF.get_Range("G2", "G" + (1 + iDataF)).NumberFormat = "0.0%_);[Red](0.0%)";

        //xlWorkSheetF.get_Range("C" + (2 + iDataF), "C" + (2 + iDataF)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iDataF) + ")";
        //xlWorkSheetF.get_Range("D" + (2 + iDataF), "D" + (2 + iDataF)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iDataF) + ")";
        //xlWorkSheetF.get_Range("E" + (2 + iDataF), "E" + (2 + iDataF)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iDataF) + ")";
        //xlWorkSheetF.get_Range("F" + (2 + iDataF), "F" + (2 + iDataF)).Formula = "=SUBTOTAL(109,F2:F" + (1 + iDataF) + ")";

        

        string[] hnamesa = new string[headersCountA];
        string[] hnamesb = new string[headersCountB];
        string[] hnamesc = new string[headersCountC];
        string[] hnamesd = new string[headersCountD];
        string[] hnamesk = new string[headersCountK];
        string[] hnamesn = new string[headersCountN];
        //string[] hnamese = new string[headersCountE];
        //string[] hnamesf = new string[headersCountF]; 

        int i = 0;
        int h = 0;

        foreach (ADODB.Field adoField in rsDataA.Fields)
        {
            hnamesa[i] = adoField.Name;
            i++;
        }

        foreach (ADODB.Field adoField in rsDataB.Fields)
        {
            hnamesb[h] = adoField.Name;
            h++;
        }

        i = 0;
        h = 0;

        foreach (ADODB.Field adoField in rsDataC.Fields)
        {
            hnamesc[i] = adoField.Name;
            i++;
        }

        foreach (ADODB.Field adoField in rsDataD.Fields)
        {
            hnamesd[h] = adoField.Name;
            h++;
        }

        i = 0;
        h = 0;

        foreach (ADODB.Field adoField in rsDataK.Fields)
        {
            hnamesk[i] = adoField.Name;
            i++;
        }

        foreach (ADODB.Field adoField in rsDataN.Fields)
        {
            hnamesn[h] = adoField.Name;
            h++;
        }

        xlWorkSheetA.get_Range("A1", weekLimitData.ToString() + "1").Value2 = hnamesa;
        xlWorkSheetA.get_Range("A50", weekLimitDataB.ToString() + "50").Value2 = hnamesb;
        xlWorkSheetB.get_Range("A1", weekLimitDataC.ToString() + "1").Value2 = hnamesc;
        xlWorkSheetB.get_Range("A50", weekLimitDataD.ToString() + "50").Value2 = hnamesd;
        xlWorkSheetG.get_Range("A1", weekLimitDataK.ToString() + "1").Value2 = hnamesk;
        xlWorkSheetK.get_Range("A1", weekLimitDataN.ToString() + "1").Value2 = hnamesn;
        //xlWorkSheetD.get_Range("A1", "AM1").Value2 = hnamesd;
        //xlWorkSheetE.get_Range("A1", "AM1").Value2 = hnamese;
        //xlWorkSheetF.get_Range("A1", "L1").Value2 = hnamesf;

        Microsoft.Office.Interop.Excel.Range chartRange;
        Microsoft.Office.Interop.Excel.ChartObjects xlCharts = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChart = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(10, 180, 450, 250);
        
        Microsoft.Office.Interop.Excel.Chart chartPage = myChart.Chart;


        chartRange = xlWorkSheetA.get_Range("A1", weekLimitDataE.ToString() + (1 + iDataA));
        chartPage.SetSourceData(chartRange, Excel.XlRowCol.xlRows);
        chartPage.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        chartPage.ApplyLayout(3, Microsoft.Office.Interop.Excel.XlChartType.xlLine);
        chartPage.ChartTitle.Text = "Profit Per AM Per Week";
       
        


        
        Microsoft.Office.Interop.Excel.ChartObjects xlChartsA2 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartA2 = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(470, 180, 450, 250);

        Microsoft.Office.Interop.Excel.Chart chartPageA2 = myChartA2.Chart;


        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollection = (Microsoft.Office.Interop.Excel.SeriesCollection)myChartA2.Chart.SeriesCollection(missValue);
        Microsoft.Office.Interop.Excel.Series seriesA21 = seriesCollection.NewSeries();
        Microsoft.Office.Interop.Excel.Series seriesA22 = seriesCollection.NewSeries();
        //Microsoft.Office.Interop.Excel.Series seriesA23 = seriesCollection.NewSeries();

        Microsoft.Office.Interop.Excel.Range seriesA21_range = xlWorkSheetA.get_Range("B" + (iDataA + 2),weekLimitDataE.ToString() + (iDataA + 2));
        Microsoft.Office.Interop.Excel.Range seriesA22_range = xlWorkSheetA.get_Range("B" + (iDataA + 4),weekLimitDataE.ToString() + (iDataA + 4));
        //Microsoft.Office.Interop.Excel.Range seriesA23_range = xlWorkSheetA.get_Range("B" + (iDataA + 5), weekLimitDataE.ToString() + (iDataA + 5)); 

        seriesA21.XValues = xlWorkSheetA.get_Range("B1",  weekLimitDataE.ToString() + "1");
        seriesA22.XValues = xlWorkSheetA.get_Range("B1",  weekLimitDataE.ToString() + "1");
        //seriesA23.XValues = xlWorkSheetA.get_Range("B1", weekLimitDataE.ToString() + "1");

        seriesA21.Name = "Profit";
        seriesA22.Name = "Sales";
        //seriesA23.Name = "Minutes";

        seriesA21.Values = seriesA21_range;
        seriesA22.Values = seriesA22_range;
        //seriesA23.Values = seriesA23_range;



        seriesA21.AxisGroup = Microsoft.Office.Interop.Excel.XlAxisGroup.xlSecondary;
        seriesA22.AxisGroup = Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary;
        //seriesA23.AxisGroup = Microsoft.Office.Interop.Excel.XlAxisGroup.xlSecondary;
        

        seriesA21.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
        seriesA22.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        //seriesA23.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlAreaStacked;
        myChartA2.Chart.HasTitle = true;
        myChartA2.Chart.ChartTitle.Text = "Sales & Profit Per Week";
        myChartA2.Chart.Legend.Position = Microsoft.Office.Interop.Excel.XlLegendPosition.xlLegendPositionBottom;

        Microsoft.Office.Interop.Excel.Axis pAxis = (Microsoft.Office.Interop.Excel.Axis) myChartA2.Chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        pAxis.HasTitle = true;
        pAxis.AxisTitle.Text = "Sales";
        //pAxis.MinimumScaleIsAuto = false;
        //pAxis.MinimumScale = ;

        Microsoft.Office.Interop.Excel.Axis sAxis = (Microsoft.Office.Interop.Excel.Axis) myChartA2.Chart.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlSecondary);
        sAxis.HasTitle = true;
        sAxis.AxisTitle.Text = "Profit";
        //sAxis.MinimumScaleIsAuto = false;
        //sAxis.MajorUnitIsAuto = false;
        //sAxis.MajorUnit = 250;
        //sAxis.MinimumScale = 1000;


        Microsoft.Office.Interop.Excel.Range chartRangeA3;
        Microsoft.Office.Interop.Excel.ChartObjects xlChartsA3 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartA3 = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(10, 440, 450, 250);

        Microsoft.Office.Interop.Excel.Chart chartPageA3 = myChartA3.Chart;

        chartRangeA3 = xlWorkSheetE.get_Range("A2", "B" + (iDataI + 1));

        chartPageA3.SetSourceData(chartRangeA3, missValue);
        chartPageA3.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded;
        chartPageA3.ApplyLayout(1, Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded);
        chartPageA3.ChartTitle.Text = "AM Profit Week ";
        chartPageA3.Elevation = 30;



        Microsoft.Office.Interop.Excel.Range chartRangeA4;
        Microsoft.Office.Interop.Excel.ChartObjects xlChartsA4 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartA4 = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(470, 440, 450, 250);


        Microsoft.Office.Interop.Excel.Chart chartPageA4 = myChartA4.Chart;


        chartRangeA4 = xlWorkSheetK.get_Range("A1", weekLimitDataN.ToString() + (1 + iDataN));
        chartPageA4.SetSourceData(chartRangeA4, Excel.XlRowCol.xlRows);

        //Microsoft.Office.Interop.Excel.Range chartRangeA4x;
        //Microsoft.Office.Interop.Excel.Range chartRangeA4y;

        //chartRangeA4x = xlWorkSheetK.get_Range("A1", weekLimitDataN.ToString() + "1"); 
        //chartRangeA4y = xlWorkSheetK.get_Range("A2", weekLimitDataN.ToString() + "4");

        //Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionA4 = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPageA4.SeriesCollection(missValue);
        //Microsoft.Office.Interop.Excel.Series seriesA41 = seriesCollectionA4.NewSeries();

        //seriesA41.XValues = chartRangeA4x;
        //seriesA41.Values = chartRangeA4y;

        //chartPageA4.SetSourceData(chartRangeA4, missValue);
        chartPageA4.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnStacked;
        chartPageA4.HasTitle = true;
        chartPageA4.ChartTitle.Text = "Profit Increase Per Week";
        chartPageA4.Legend.Position = Microsoft.Office.Interop.Excel.XlLegendPosition.xlLegendPositionBottom;



        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollection4 = (Microsoft.Office.Interop.Excel.SeriesCollection)myChartA4.Chart.SeriesCollection(missValue);
        Microsoft.Office.Interop.Excel.Series seriesA43 = (Microsoft.Office.Interop.Excel.Series)myChartA4.Chart.SeriesCollection(3);
        seriesA43.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;

        //seriesA42.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnStacked;
        //seriesA43.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnStacked;

        Microsoft.Office.Interop.Excel.Axis sAxisA4 = (Microsoft.Office.Interop.Excel.Axis)chartPageA4.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        sAxisA4.TickLabels.NumberFormat = "#,##0;[Red]-#,##0";

        Microsoft.Office.Interop.Excel.Axis sAxisA5 = (Microsoft.Office.Interop.Excel.Axis)chartPageA4.Axes(Microsoft.Office.Interop.Excel.XlAxisType.xlCategory, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary);
        sAxisA5.TickLabelPosition = Microsoft.Office.Interop.Excel.XlTickLabelPosition.xlTickLabelPositionNone;
        sAxisA4.HasTitle = true;
        sAxisA4.AxisTitle.Text = "Minutes";
        //chartPageA4.Elevation = 30;

        Microsoft.Office.Interop.Excel.ChartObjects xlChartsA5 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartA5 = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(10, 1100, 455, 350);
        Microsoft.Office.Interop.Excel.Chart chartPageA5 = myChartA5.Chart;

        Microsoft.Office.Interop.Excel.Range chartRangeA5x;
        Microsoft.Office.Interop.Excel.Range chartRangeA5y;

        //chartRangeA5y = xlWorkSheetA.get_Range("R51", "R65");
        chartRangeA5y = xlWorkSheetA.get_Range(weekLimitDataB.ToString() + "51", weekLimitDataB.ToString() + (iDataB + 50));
        //chartRangeA5x = xlWorkSheetA.get_Range("A51", "A65");
        chartRangeA5x = xlWorkSheetA.get_Range("A51", "A" + (iDataB + 50));

        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionA5 = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPageA5.SeriesCollection(missValue);
        Microsoft.Office.Interop.Excel.Series seriesA51 = seriesCollectionA5.NewSeries();

        seriesA51.XValues = chartRangeA5x;
        seriesA51.Values = chartRangeA5y;
        
        chartPageA5.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPieExploded;
        chartPageA5.ApplyLayout(1, Microsoft.Office.Interop.Excel.XlChartType.xlPieExploded);
        chartPageA5.ChartTitle.Text = "Product Share Week";



        Microsoft.Office.Interop.Excel.ChartObjects xlChartsA6 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartA6 = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(475, 1100, 455, 350);
        Microsoft.Office.Interop.Excel.Chart chartPageA6 = myChartA6.Chart;

        Microsoft.Office.Interop.Excel.Range chartRangeA6x;
        Microsoft.Office.Interop.Excel.Range chartRangeA6y;

        chartRangeA6y = xlWorkSheetA.get_Range(Convert.ToChar(endColumnB-1).ToString() + "51", Convert.ToChar(endColumnB-1).ToString() + (iDataB + 50));
        chartRangeA6x = xlWorkSheetA.get_Range("A51", "A" + (iDataB + 50));

        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionA6 = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPageA6.SeriesCollection(missValue);
        Microsoft.Office.Interop.Excel.Series seriesA61 = seriesCollectionA6.NewSeries();

        seriesA61.XValues = chartRangeA6x;
        seriesA61.Values = chartRangeA6y;

        chartPageA6.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
        chartPageA6.HasTitle = true;
        chartPageA6.ChartTitle.Text = "Margin Product Week";



        Microsoft.Office.Interop.Excel.Range chartRangeB1;
        Microsoft.Office.Interop.Excel.ChartObjects xlChartsB1 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetB.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartB1 = (Microsoft.Office.Interop.Excel.ChartObject)xlChartsB1.Add(10, 180, 300, 250);

        Microsoft.Office.Interop.Excel.Chart chartPageB1 = myChartB1.Chart;


        chartRangeB1 = xlWorkSheetG.get_Range("A1", weekLimitDataK.ToString() + (1 + iDataK));
        chartPageB1.SetSourceData(chartRangeB1, Microsoft.Office.Interop.Excel.XlRowCol.xlRows);
        chartPageB1.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlLine;
        chartPageB1.ApplyLayout(3, Microsoft.Office.Interop.Excel.XlChartType.xlLine);
        chartPageB1.ChartTitle.Text = "Profit Per AM Per Month";



        Microsoft.Office.Interop.Excel.ChartObjects xlChartsB2 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetB.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartB2 = (Microsoft.Office.Interop.Excel.ChartObject)xlChartsB2.Add(320, 180, 300, 250);
        Microsoft.Office.Interop.Excel.Chart chartPageB2 = myChartB2.Chart;

        Microsoft.Office.Interop.Excel.Range chartRangeB2x;
        Microsoft.Office.Interop.Excel.Range chartRangeB2y;

        //chartRangeB2y = xlWorkSheetG.get_Range(Convert.ToChar(endColumnB - 1).ToString() + "51", Convert.ToChar(endColumnB - 1).ToString() + (iDataB + 50));
        //chartRangeB2x = xlWorkSheetG.get_Range("A51", "A" + (iDataB + 50));

        chartRangeB2y = xlWorkSheetG.get_Range("B10", "L10");
        chartRangeB2x = xlWorkSheetG.get_Range("B1", "L1");

        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionB2 = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPageB2.SeriesCollection(missValue);
        Microsoft.Office.Interop.Excel.Series seriesB21 = seriesCollectionB2.NewSeries();

        seriesB21.XValues = chartRangeB2x;
        seriesB21.Values = chartRangeB2y;

        chartPageB2.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
        chartPageB2.HasTitle = true;
        chartPageB2.ChartTitle.Text = "Profit Product Month";


        Microsoft.Office.Interop.Excel.Range chartRangeB3;
        Microsoft.Office.Interop.Excel.ChartObjects xlChartsB3 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetB.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartB3 = (Microsoft.Office.Interop.Excel.ChartObject)xlChartsB3.Add(10, 440, 300, 250);

        Microsoft.Office.Interop.Excel.Chart chartPageB3 = myChartB3.Chart;

        chartRangeB3 = xlWorkSheetH.get_Range("A2", "B" + (iDataL + 1));

        chartPageB3.SetSourceData(chartRangeB3, missValue);
        chartPageB3.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded;
        chartPageB3.ApplyLayout(1, Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded);
        chartPageB3.ChartTitle.Text = "AMf Profit Month ";
        chartPageB3.Elevation = 30;



        //Microsoft.Office.Interop.Excel.Range chartRangeB4;
        //Microsoft.Office.Interop.Excel.ChartObjects xlChartsB4 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetB.ChartObjects(Type.Missing);
        //Microsoft.Office.Interop.Excel.ChartObject myChartB4 = (Microsoft.Office.Interop.Excel.ChartObject)xlChartsB4.Add(320, 440, 300, 250);

        //Microsoft.Office.Interop.Excel.Chart chartPageB4 = myChartB4.Chart;

        //chartRangeB4 = xlWorkSheetI.get_Range("A2", "B" + (iDataM + 1));

        //chartPageB4.SetSourceData(chartRangeB4, missValue);
        //chartPageB4.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded;
        //chartPageB4.ApplyLayout(1, Microsoft.Office.Interop.Excel.XlChartType.xl3DPieExploded);
        //chartPageB4.ChartTitle.Text = "Vendor Profit Month ";

        //chartPageB4.Elevation = 30;

        

        Microsoft.Office.Interop.Excel.ChartObjects xlChartsB5 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetB.ChartObjects(Type.Missing);
        Microsoft.Office.Interop.Excel.ChartObject myChartB5 = (Microsoft.Office.Interop.Excel.ChartObject)xlChartsB5.Add(50, 1100, 455, 350);
        Microsoft.Office.Interop.Excel.Chart chartPageB5 = myChartB5.Chart;

        Microsoft.Office.Interop.Excel.Range chartRangeB5x;
        Microsoft.Office.Interop.Excel.Range chartRangeB5y;

        chartRangeB5y = xlWorkSheetB.get_Range(Convert.ToChar(endColumnD - 2).ToString() + "51", Convert.ToChar(endColumnD - 2).ToString() + (iDataD + 50));
        chartRangeB5x = xlWorkSheetB.get_Range("A51", "A" + (iDataB + 50));

        Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionB5 = (Microsoft.Office.Interop.Excel.SeriesCollection)chartPageB5.SeriesCollection(missValue);
        Microsoft.Office.Interop.Excel.Series seriesB51 = seriesCollectionB5.NewSeries();

        seriesB51.XValues = chartRangeB5x;
        seriesB51.Values = chartRangeB5y;

        chartPageB5.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPieExploded;
        chartPageB5.ApplyLayout(1, Microsoft.Office.Interop.Excel.XlChartType.xlPieExploded);
        chartPageB5.HasTitle = true;
        chartPageB5.ChartTitle.Text = "Customer Share Month";




        //Microsoft.Office.Interop.Excel.ChartObjects xlChartsA5 = (Microsoft.Office.Interop.Excel.ChartObjects)xlWorkSheetA.ChartObjects(Type.Missing);
        //Microsoft.Office.Interop.Excel.ChartObject myChartA5 = (Microsoft.Office.Interop.Excel.ChartObject)xlCharts.Add(10, 1000, 450, 250);

        //Microsoft.Office.Interop.Excel.Chart chartPageA5 = myChartA5.Chart;


        //Microsoft.Office.Interop.Excel.SeriesCollection seriesCollectionA5 = (Microsoft.Office.Interop.Excel.SeriesCollection)myChartA5.Chart.SeriesCollection(missValue);
        //Microsoft.Office.Interop.Excel.Series seriesA51 = seriesCollection.NewSeries();


        ////Microsoft.Office.Interop.Excel.Range seriesA51_range = xlWorkSheetA.get_Range(weekLimitDataB.ToString() + "51", weekLimitDataB.ToString() + (iDataB + 50));
        //Microsoft.Office.Interop.Excel.Range seriesA51_range = xlWorkSheetA.get_Range("R51","R65");

        ////seriesA51.XValues = xlWorkSheetA.get_Range("A51", "A" + (iDataB + 50));
        //seriesA51.XValues = xlWorkSheetA.get_Range("A51", "A65");
        
        //seriesA51.Values = seriesA51_range;
        
        //seriesA51.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPieExploded;
        
        //myChartA2.Chart.HasTitle = true;
        //myChartA2.Chart.ChartTitle.Text = "Product Share Week";
        
        rsDataA.Close();
        rsDataB.Close();
        rsDataC.Close();
        rsDataD.Close();
        rsDataE.Close();
        rsDataF.Close();
        rsDataG.Close();
        rsDataH.Close();
        rsDataI.Close();
        DBConnection.Close();

        xlApp.DisplayAlerts = false;
        xlApp.AlertBeforeOverwriting = false;



        xlWorkBook.SaveAs(sPathFiles + "WeeklyCCResultsReport_Week"+ (weekNum-1) + "_" + sToday + ".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, missValue, missValue, missValue, missValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, missValue, missValue, missValue, missValue);
        xlWorkBook.Close(false, missValue, missValue);

        xlApp.Quit();
        //xlApp.Visible = true;

        releaseObject(xlWorkSheetA);
        releaseObject(xlWorkSheetB);
        releaseObject(xlWorkSheetC);
        releaseObject(xlWorkSheetD);
        releaseObject(xlWorkSheetE);
        //releaseObject(xlWorkSheetF);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        while (xlApp != null)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
            xlApp = null;
        }

        GC.Collect();
        GC.WaitForPendingFinalizers();


        //HyperLink1.NavigateUrl = "http://mecca.directo.com/ws/op-leverage/" + "OperativeLeverage_" + sToday + ".xls";
        //HyperLink1.Visible = true;

        Response.Redirect("http://mecca.directo.com/reports-mecca/WeeklyCCResultsReport_Week" + (weekNum - 1) + "_" + sToday + ".xls");

        

    }

    private void releaseObject(object obj)
    {
        if (obj != null)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //lblError.Text = "Exception Occured while releasing object " + ex.ToString();
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}

