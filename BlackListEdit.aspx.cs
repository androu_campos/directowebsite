using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data.Odbc;

public partial class BlackListEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ArrayList CheckBoxArray;
        if (ViewState["CheckBoxArray"] != null)
        {
            CheckBoxArray = (ArrayList)ViewState["CheckBoxArray"];
        }
        else
        {
            CheckBoxArray = new ArrayList();
        }

        if (IsPostBack)
        {
            int CheckBoxIndex;
            bool CheckAllWasChecked = false;
            CheckBox chkAll = (CheckBox)gvGood.HeaderRow.Cells[0].FindControl("chbxselAll");
            string checkAllIndex = "chbxselAll-" + gvGood.PageIndex;
            if (chkAll.Checked)
            {
                if (CheckBoxArray.IndexOf(checkAllIndex) == -1)
                {
                    CheckBoxArray.Add(checkAllIndex);
                }
            }
            else
            {
                if (CheckBoxArray.IndexOf(checkAllIndex) != -1)
                {
                    CheckBoxArray.Remove(checkAllIndex);
                    CheckAllWasChecked = true;
                }
            }
            for (int i = 0; i < gvGood.Rows.Count; i++)
            {
                if (gvGood.Rows[i].RowType == DataControlRowType.DataRow)
                {
                    CheckBox chk = (CheckBox)gvGood.Rows[i].Cells[0].FindControl("chbxsel");
                    CheckBoxIndex = gvGood.PageSize * gvGood.PageIndex + (i + 1);
                    if (chk.Checked)
                    {
                        if (CheckBoxArray.IndexOf(CheckBoxIndex) == -1 && !CheckAllWasChecked)
                        {
                            CheckBoxArray.Add(CheckBoxIndex);
                        }
                    }
                    else
                    {
                        if (CheckBoxArray.IndexOf(CheckBoxIndex) != -1 || CheckAllWasChecked)
                        {
                            CheckBoxArray.Remove(CheckBoxIndex);
                        }
                    }
                }
            }

        }
        else if (!IsPostBack)
        {
            //CountDB();
            lnkTab1.CssClass = "unclicked";
            lnkTab2.CssClass = "clicked";
            gvGood.Visible = true;

        }
        ViewState["CheckBoxArray"] = CheckBoxArray;
    }

    

    //protected void gvDupDif_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    UpdatePanel1.UpdateMode = UpdatePanelUpdateMode.Conditional;
    //    UpdatePanel1.Update();
    //    string arguments = e.CommandArgument.ToString();
    //}

    //protected void gvGood_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    string param1 = e.Values[0].ToString();

    //}
    //protected void Button2_Click(object sender, EventArgs e)
    //{
    //    cdrdbDatasetTableAdapters.ValidarTmpBuenaTableAdapter adp = new cdrdbDatasetTableAdapters.ValidarTmpBuenaTableAdapter();
    //    cdrdbDatasetTableAdapters.RNumberDupDifTableAdapter adp1 = new cdrdbDatasetTableAdapters.RNumberDupDifTableAdapter();

    //    string newrate = TextBox1.Text;
    //    string Number = Session["Number"].ToString();
    //    adp.Insert(Number);


    //    gvGood.DataSource = adp1.GetData();
    //    gvGood.DataBind();

    //    gvGood.DataSourceID = "sqlsaGood";
    //    gvGood.DataBind();

    //}

    protected void lnkTab1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/BlackList.aspx");
    }
    protected void lnkTab2_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/BlackListEdit.aspx");
    }

    //protected void gvGood_RowEditing(object sender, GridViewEditEventArgs e)
    //{
    //    gvGood.EditIndex = e.NewEditIndex;

    //    cdrdbDatasetTableAdapters.ValidarTmpBuenaTableAdapter adp = new cdrdbDatasetTableAdapters.ValidarTmpBuenaTableAdapter();

    //    int id = Convert.ToInt32(gvGood.DataKeys[e.NewEditIndex].Value);
    //    DataTable dt1 = new DataTable();


    //    string sql = @"SELECT NumVal FROM ValidarTmpBuena";


    //    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CDRDB"].ToString()))
    //    {
    //        conn.Open();

    //        SqlCommand cmd = new SqlCommand(sql, conn);

    //        SqlDataAdapter da = new SqlDataAdapter(cmd);

    //        da.Fill(dt1);
    //    }

    //    DataBind();

    //}


    //protected void gvGood_RowUpdating(object sender, GridViewUpdateEventArgs e)
    //{
    //    int id = Convert.ToInt32(gvGood.DataKeys[e.RowIndex].Value);

    //    TextBox text = gvGood.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox;
    //    string number = text.Text;

    //    cdrdbDatasetTableAdapters.ValidarTmpBuenaTableAdapter adp1 = new cdrdbDatasetTableAdapters.ValidarTmpBuenaTableAdapter();

    //    DataTable dt = new DataTable();


    //    string sql = @"UPDATE ValidarTmpBuena SET NumVal = @num where id = @id";


    //    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
    //    {
    //        conn.Open();

    //        SqlCommand cmd = new SqlCommand(sql, conn);
    //        cmd.Parameters.AddWithValue("@num", number);
    //        cmd.Parameters.AddWithValue("@id", id);

    //        cmd.ExecuteNonQuery();
    //    }

    //    gvGood.EditIndex = -1;
    //    DataBind();

    //}

    protected void OnPaging(object sender, GridViewPageEventArgs e)
    {
        gvGood.PageIndex = e.NewPageIndex;
        gvGood.DataBind();
        if (ViewState["CheckBoxArray"] != null)
        {
            ArrayList CheckBoxArray = (ArrayList)ViewState["CheckBoxArray"];
            string checkAllIndex = "chbxselAll-" + gvGood.PageIndex;

            if (CheckBoxArray.IndexOf(checkAllIndex) != -1)
            {
                CheckBox chbAll = (CheckBox)gvGood.HeaderRow.Cells[0].FindControl("chbxselAll");
                chbAll.Checked = true;
            }
            for (int i = 0; i < gvGood.Rows.Count; i++)
            {

                if (gvGood.Rows[i].RowType == DataControlRowType.DataRow)
                {
                    if (CheckBoxArray.IndexOf(checkAllIndex) != -1)
                    {
                        CheckBox chk = (CheckBox)gvGood.Rows[i].Cells[0].FindControl("chbxsel");
                        chk.Checked = true;
                    }
                    else
                    {
                        int CheckBoxIndex = gvGood.PageSize * (gvGood.PageIndex) + (i + 1);
                        if (CheckBoxArray.IndexOf(CheckBoxIndex) != -1)
                        {
                            CheckBox chk = (CheckBox)gvGood.Rows[i].Cells[0].FindControl("chbxsel");
                            chk.Checked = true;
                        }
                    }
                }
            }
        }
    }
}
