<%@ Page Language="C#" MasterPageFile="~/Master.master" StylesheetTheme="Theme1" AutoEventWireup="true" CodeFile="RateSchedule.aspx.cs" Inherits="RatesAssign" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table style="position:absolute; top:220px; left:230px;">
        <tr>
            <td style="width: 100px" valign="top" align="left">
            </td>
            <td style="width: 100px" valign="top">
                </td>
            <td style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 109px;" valign="top" align="left">
                <table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="From:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtFrom" runat="server" CssClass="labelSteelBlue" Width="81px"></asp:TextBox></td>
                        <td style="width: 100px">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                    </tr>
                </table>
                <br />
                &nbsp;
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="Image1"
                    TargetControlID="txtFrom">
                </cc1:CalendarExtender>
            </td>
            <td style="width: 100px; height: 109px;" valign="top" align="left">
                <table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="To:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtTo" runat="server" CssClass="labelSteelBlue" Width="81px"></asp:TextBox></td>
                        <td style="width: 100px">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                        <td style="width: 100px">
                <asp:Button ID="btnApply" runat="server" OnClick="btnApply_Click"
                    Text="Check Schedules" CssClass="boton" Width="88px" /></td>
                    </tr>
                </table>
                <br />
                &nbsp;
                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="Image2"
                    TargetControlID="txtTo">
                </cc1:CalendarExtender>
            </td>
            <td align="right" style="width: 100px; height: 109px;" valign="top">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/RatesUpload.aspx" Width="87px">Upload Rates</asp:HyperLink>&nbsp;
             </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px" valign="top">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Customer:" Visible="False"></asp:Label></td>
            <td align="left" style="width: 100px" valign="top">
                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Vendor:" Visible="False"></asp:Label></td>
            <td align="left" style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" valign="top">
                <asp:GridView ID="gvAlready" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                    DataSourceID="SqlDsratesAlready" Visible="False" Font-Names="Arial" Font-Size="8pt">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" DeleteText="Deleted" />
                        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                            SortExpression="ID" />
                        <asp:BoundField DataField="ProviderName" HeaderText="ProviderName" SortExpression="ProviderName" />
                        <asp:BoundField DataField="Rates" HeaderText="Rates" SortExpression="Rates" />
                        <asp:BoundField DataField="Applyday" HeaderText="Applyday" SortExpression="Applyday" />
                        <asp:BoundField DataField="Uploaded" HeaderText="Uploaded" SortExpression="Uploaded" />
                    </Columns><RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td style="width: 100px" valign="top">                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                    DataSourceID="Sqlvenven" Visible="False" Font-Names="Arial" Font-Size="8pt">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" DeleteText="Deleted" />
                        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                            SortExpression="ID" />
                        <asp:BoundField DataField="ProviderName" HeaderText="ProviderName" SortExpression="ProviderName" />
                        <asp:BoundField DataField="Rates" HeaderText="Rates" SortExpression="Rates" />
                        <asp:BoundField DataField="Applyday" HeaderText="Applyday" SortExpression="Applyday" />
                        <asp:BoundField DataField="Uploaded" HeaderText="Uploaded" SortExpression="Uploaded" />
                    </Columns>
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px; height: 50px" valign="bottom">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Customer Already Applied:"
                    Visible="False" Width="291px"></asp:Label></td>
            <td align="left" style="width: 100px; height: 50px" valign="bottom">
                <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Vendor Already Applied:"
                    Visible="False" Width="291px"></asp:Label></td>
            <td style="width: 100px; height: 50px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" valign="top">
                <asp:GridView ID="GridView2" runat="server"
                    DataSourceID="SqlClog" Font-Names="Arial" Font-Size="8pt">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlClog" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>"></asp:SqlDataSource>
            </td>
            <td style="width: 100px" valign="top">
                <asp:GridView ID="GridView3" runat="server"
                    DataSourceID="SqlVlog" Font-Names="Arial" Font-Size="8pt">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlVlog" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>"></asp:SqlDataSource>
            </td>
            <td style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" valign="top">
                &nbsp;<asp:ScriptManager id="ScriptManager1" runat="server"></asp:ScriptManager>
                </td>
            <td style="width: 100px" valign="top">
                </td>
            <td style="width: 100px" valign="top">
                </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="SqlDsratesAlready" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
        DeleteCommand="sp_RateSummaryDeleteSchedule" ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>"
        SelectCommand="SELECT [ID]&#13;&#10;      ,[ProviderName]&#13;&#10;      ,[Rates]&#13;&#10;      ,[Applyday]&#13;&#10;      ,[Uploaded]&#13;&#10;  FROM [CDRDB].[dbo].[RateCSchedules]&#13;&#10;Order by applyday,providername" DeleteCommandType="StoredProcedure">
        <DeleteParameters>
            <asp:ControlParameter ControlID="gvAlready" Name="ID" PropertyName="SelectedValue"
                Type="Int32" />
            <asp:Parameter DefaultValue="C" Name="ty" Type="String" />
        </DeleteParameters>
    </asp:SqlDataSource>
                <asp:Label ID="Label1" runat="server" Text="Summary" CssClass="labelTurkH"></asp:Label>
    <asp:SqlDataSource ID="Sqlvenven" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
        DeleteCommand="sp_RateSummaryDeleteSchedule" ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>"
        SelectCommand="SELECT [ID]&#13;&#10;      ,[ProviderName]&#13;&#10;      ,[Rates]&#13;&#10;      ,[Applyday]&#13;&#10;      ,[Uploaded]&#13;&#10;  FROM [CDRDB].[dbo].[RateVSchedules]&#13;&#10;Order by ApplyDay,ProviderName" DeleteCommandType="StoredProcedure">
        <DeleteParameters>
            <asp:ControlParameter ControlID="gvAlready" Name="ID" PropertyName="SelectedValue"
                Type="Int32" />
            <asp:Parameter DefaultValue="V" Name="ty" Type="String" />
        </DeleteParameters>
    </asp:SqlDataSource>



</asp:Content>

