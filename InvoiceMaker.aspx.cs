using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class InvoiceMaker : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFrom.Text = DateTime.Now.AddDays(-1).ToShortDateString() + " 00:00:00 AM";

            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            //FILL DROPDOWN CUSTOMER
            SqlConnection SqlConn1 = new SqlConnection();
            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
            SqlCommand SqlCommand1 = new SqlCommand("SELECT '**ALL**' AS Customer FROM CDRDB.dbo.ProviderIP UNION SELECT DISTINCT ProviderName AS Customer FROM CDRDB.dbo.ProviderIP AS ProviderIP_2 WHERE (Type = 'C') UNION SELECT 'NIP-CALLING-CARD' AS Customer", SqlConn1);
            SqlConn1.Open();
            SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
            while (myReader1.Read())
            {
                dpdCustomer.Items.Add(myReader1.GetValue(0).ToString());
            }
            myReader1.Close();
            SqlConn1.Close();

            //COUNTRY
            SqlConnection SqlConn = new SqlConnection();
            SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";

            SqlCommand SqlCommand4 = new SqlCommand("select distinct Country from Regions union  select '** NONE **' as Country from Regions union select '**ALL**' as Country from Regions order by Country ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader4 = SqlCommand4.ExecuteReader();
            while (myReader4.Read())
            {
                dpdCountry.Items.Add(myReader4.GetValue(0).ToString());
            }
            myReader4.Close();
            SqlConn.Close();
        }
        else
        {

        }
    }

    protected void cmdReport_Click(object sender, EventArgs e)
    {
        string customer = dpdCustomer.SelectedValue.ToString();
        
        string country = dpdCountry.SelectedValue.ToString();
        country = country.Replace("**ALL**", "%");
        country = country.Replace("** NONE **", "%");
        string from = txtFrom.Text;
        string to = txtTo.Text;

        int rounding = rblRounding.SelectedIndex;

        int gmt = Convert.ToInt32(dpdGmt.SelectedValue) * 3600;
        DataSet myDataSet = new DataSet();

        if (rounding == 0)                // MEX 60 ROW 1
        {
            myDataSet = Util.RunQryVoidatDISPUTES("DECLARE @RC int DECLARE @cust varchar(20) DECLARE @ctry varchar(20) DECLARE @inf datetime DECLARE @sup datetime EXECUTE @RC = [DISPUTES].[dbo].[sp_InvoiceMaker_601]   @cust='" + customer + "',@ctry='" + country + "' ,@inf='" + from + "',@sup='" + to + "',@gmt='" + gmt +"'");
        }
        else                          // MEX 60 ROW 306
        {
            myDataSet = Util.RunQryVoidatDISPUTES("DECLARE @RC int DECLARE @cust varchar(20) DECLARE @ctry varchar(20) DECLARE @inf datetime DECLARE @sup datetime EXECUTE @RC = [DISPUTES].[dbo].[sp_InvoiceMaker]   @cust='" + customer + "',@ctry='" + country + "' ,@inf='" + from + "',@sup='" + to + "',@gmt='" + gmt + "'");
        }

        Session["InvoiceSpecial"] = myDataSet;
        DataSet detailsDataSet = new DataSet();
        detailsDataSet = Util.RunQryVoidatDISPUTES("select * from pseudoatt");
        Session["InvoiceDetails"] = detailsDataSet;

        

        gdvPart1.DataSource = myDataSet.Tables[0];
        gdvPart1.DataBind();
        gdvPart2.DataSource = myDataSet.Tables[1];
        gdvPart2.DataBind();
        gdvPart3.DataSource = myDataSet.Tables[2];
        gdvPart3.DataBind();



    }
    protected void cmdExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {

            DataTable tblMain = new DataTable();            
            DataRow myRowy = tblMain.NewRow();
            DataRow myRowy2 = tblMain.NewRow();
           
            tblMain.Columns.Add("Completed_Calls");
            tblMain.Columns.Add("Billed_Minutes");
            tblMain.Columns.Add("Price");
            tblMain.Columns.Add("Amount");
                        
            foreach (DataRow aRow in ((DataSet)Session["InvoiceSpecial"]).Tables[0].Rows)
            {
                tblMain.AcceptChanges();
                tblMain.LoadDataRow(aRow.ItemArray, true);
            }
            DataTable dtEmployee = ((DataSet)Session["InvoiceSpecial"]).Tables[0].Copy();

            //Add Total Grid
            myRowy["Completed_Calls"] = "Total_Calls";
            myRowy["Billed_Minutes"] = "Total_Minutes";
            myRowy["Price"] = "Price";
            myRowy["Amount"] = "Amount";
            tblMain.Rows.Add(myRowy);

            foreach(DataRow myRow in ((DataSet)Session["InvoiceSpecial"]).Tables[1].Rows)
            {                             
                tblMain.AcceptChanges();                
                tblMain.Rows.Add(myRow.ItemArray);
            }

            myRowy2["Completed_Calls"] = "Date";
            myRowy2["Billed_Minutes"] = "Completed_Calls";
            myRowy2["Price"] = "Billed_Minutes";
            myRowy2["Amount"] = "Amount";
            tblMain.Rows.Add(myRowy2);

            foreach (DataRow myRow2 in ((DataSet)Session["InvoiceSpecial"]).Tables[2].Rows)
            {                
                tblMain.AcceptChanges();
                tblMain.LoadDataRow(myRow2.ItemArray, false);
            }
            
            // Export all the details to EXCEL
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");            
            string filename = "Invoice" + dpdCustomer.SelectedValue.ToString() +  ".xls";
            objExport.ExportDetails(tblMain, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
    protected void cmdExportDetails_Click(object sender, EventArgs e)
    {

        try
        {
            DataTable myTbl = ((DataSet)Session["InvoiceDetails"]).Tables[0];
            // Export all the details to EXCEL
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "Details" + dpdCustomer.SelectedValue.ToString() + ".xls";
            objExport.ExportDetails(myTbl, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }
        
    }
}
