<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Regions_OffNet.aspx.cs" Inherits="Regions_OffNet" Title="DIRECTO - Connections Worldwide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="Label1" runat="server" CssClass="labelTurkH" Text="MECCA Regions OFFNET"></asp:Label>
    <table align="left" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td align="left" valign="top" width="1800">
                <table>
                    <tr>
                        <td style="width: 100px">
                <asp:Label ID="Label2" runat="server" Text="Step 1" CssClass="labelTurk"></asp:Label></td>
                        <td style="width: 100px">
                <asp:Label ID="Label4" runat="server" Text="Filter By Region:" CssClass="labelTurk"></asp:Label></td>
                        <td style="width: 100px">
                <asp:DropDownList ID="DropDownList1" runat="server" Width="128px" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True" DataTextField="Region" DataValueField="Region" CssClass="dropdown">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                <asp:Label ID="Label3" runat="server" Text="Step 2" CssClass="labelTurk"></asp:Label></td>
                        <td style="width: 100px">
                <asp:Label ID="Label5" runat="server" Text="Filter By Vendor:" CssClass="labelTurk"></asp:Label></td>
                        <td style="width: 100px">
                <asp:DropDownList ID="DropDownList2" runat="server" Width="128px" DataSourceID="SqlDataSource2" DataTextField="Vendor" DataValueField="Vendor" CssClass="dropdown">
                </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px" >
                <asp:Button ID="Button1" runat="server" CssClass="boton" Text="View report" OnClick="Button1_Click" BorderStyle="Solid" BorderWidth="1px" EnableViewState="False" />
                </td>
                
                    </tr>
                    
                    <tr>
                    
                    <td colspan="3" height="25">
                    
                    </td>
                    
                    </tr>
                </table>
            </td>
            <td style="width: 100px" align="right" valign="top">
                <asp:GridView  ID="GridView1" runat="server" Width="150px" GridLines="None" Font-Names="Arial" AutoGenerateColumns="false"
                    Font-Size="8pt" HorizontalAlign="Right">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" HorizontalAlign="Right" />
                    <AlternatingRowStyle BackColor="White" />
                <Columns>
                
                <asp:BoundField HeaderText="Last Refresh" DataField="Last Refresh" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="1800">
                <table align="left">
                    <tr>
                        <td align="left" style="width: 100px">
                <asp:Label ID="Label6" runat="server" CssClass="labelTurk" Text="Recent Traffic"></asp:Label></td>
                        <td align="left" style="width: 100px">
                <asp:Label ID="Label7" runat="server" CssClass="labelTurk" Text="Today's Traffic"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px">
                            <asp:Button ID="exportRecent" runat="server" CssClass="boton" Font-Names="Arial"
                                Font-Size="6pt" OnClick="exportRecent_Click" Text="Export to EXCEL" Width="69px" /></td>
                        <td align="left" style="width: 100px">
                            <asp:Button ID="exportToday" runat="server" CssClass="boton" Font-Names="Arial" Font-Size="6pt"
                                OnClick="exportToday_Click" Text="Export to EXCEL" Width="69px" /></td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top">
                <asp:GridView ID="gvRegionsOff" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="dsInc" GridLines="None" Font-Names="Arial" Font-Size="8pt" Width="600px" OnLoad="gvRegionsOff_Load">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle Font-Bold="False" Height="40px" Font-Size="8pt" Font-Italic="False" ForeColor="#F38330" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Vendor" HeaderText="Vendor" SortExpression="Vendor" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/>
                        <asp:BoundField DataField="TerminationIP" HeaderText="TerminationIP" SortExpression="TerminationIP" />
                        <asp:BoundField DataField="Minutes" HeaderText="Minutes" SortExpression="Minutes" HtmlEncode="false" DataFormatString="{0:0,0}" />
                        <asp:BoundField DataField="Calls" HeaderText="Calls" SortExpression="Calls" HtmlEncode="false" DataFormatString="{0:0,0}" />
                        <asp:BoundField DataField="%ASR" HeaderText="%ASR" SortExpression="%ASR" HtmlEncode="false" DataFormatString="{0:0,0}"  />
                        <asp:BoundField DataField="ACD" HeaderText="ACD" SortExpression="ACD" HtmlEncode="false" DataFormatString="{0:0.00}"  />
                        <asp:BoundField DataField="Code" HeaderText="Code" SortExpression="Code" />
                        <asp:BoundField DataField="Region" HeaderText="Region" SortExpression="Region" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/>
                    </Columns>
                </asp:GridView>
                            <asp:Label ID="lblNothR" runat="server" ForeColor="Red" Text="Nothing Found !!" Visible="False"></asp:Label></td>
                        <td style="width: 100px" valign="top">
                <asp:GridView ID="gdvToday" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="dsAll" Font-Names="Arial" Font-Size="8pt" GridLines="None" Width="600px">
                     <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle Font-Bold="False" Height="40px" Font-Size="8pt" Font-Italic="False" ForeColor="#F38330" />
                    <AlternatingRowStyle BackColor="White" />
                    
                    <Columns>
                        <asp:BoundField DataField="Vendor" HeaderText="Vendor" SortExpression="Vendor" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="TerminationIP" HeaderText="TerminationIP" SortExpression="TerminationIP" />
                        <asp:BoundField DataField="Minutes" HeaderText="Minutes" SortExpression="Minutes" HtmlEncode="false" DataFormatString="{0:0,0}" />
                        <asp:BoundField DataField="Calls" HeaderText="Calls" SortExpression="Calls" HtmlEncode="false" DataFormatString="{0:0,0}" />
                        <asp:BoundField DataField="%ASR" HeaderText="%ASR" SortExpression="%ASR" HtmlEncode="false" DataFormatString="{0:0,0}" />
                        <asp:BoundField DataField="ACD" HeaderText="ACD" SortExpression="ACD" HtmlEncode="false" DataFormatString="{0:0.0}" />
                        <asp:BoundField DataField="Code" HeaderText="Code" SortExpression="Code" />
                        <asp:BoundField DataField="Region" HeaderText="Region" SortExpression="Region" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/>
                    </Columns>
                </asp:GridView>
                            <asp:Label ID="lblNothT" runat="server" ForeColor="Red" Text="Nothing Found !!" Visible="False"></asp:Label></td>
                    </tr>
                </table>
            </td>
            <td style="width: 100px; height: 28px">
            </td>
        </tr>
        
        
        
        <tr>
            <td width="100%" colspan="2">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label8" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager></td>
        </tr>
    </table>
    
    <asp:Timer id="Timer1" runat="server">
    </asp:Timer>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT '%' AS Region UNION SELECT DISTINCT Region FROM Regions_OFF_NET">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT '%' as Vendor UNION SELECT DISTINCT [Vendor] FROM [Regions_OFF_NET] WHERE Calls > 25 UNION SELECT DISTINCT [Vendor] FROM [Regions_OFF_NETT] WHERE Calls > 100 ">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg"
        ProviderName="System.Data.SqlClient"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsInc" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT *  FROM [MECCA2].[dbo].[Regions_OFF_NET] WHERE Calls > 25 order by [%ASR], ACD">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsAll" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT *  FROM [MECCA2].[dbo].[Regions_OFF_NETT] WHERE Calls > 100 order by [%ASR], ACD">
    </asp:SqlDataSource>
   
</asp:Content>

