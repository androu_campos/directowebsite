using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;


public partial class LtwGrouped : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["DirectoUser"].ToString() != string.Empty)
        //{
            lblCustomer.Text = makeHtml("Customer%");
            lblVendor.Text = makeHtml("Vendor%");
        //}


        
    }
    private string makeHtml(string type)
    {
        try
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "sp_LtwGrouped";
            sqlCommand.Parameters.Add("type", SqlDbType.Char).Value = type;
            StringBuilder myString = new StringBuilder();
            myString.Append("<TABLE><TR><TD>Provider</TD><TD>TotalCalls</TD></TR>");
            DataTable tbl = Util.RunQueryatCDRDB(sqlCommand).Tables[0];
            foreach (DataRow myRow in tbl.Rows)
            {
                myString.Append("<TR><TD>" + myRow["ProviderName"].ToString() + "</TD><TD>" + myRow["TotalCalls"].ToString() + "</TD></TR>");

            }
            myString.Append("</TABLE>");
            string table = myString.ToString();
            //lblCustomer.Text = table;
            return table;
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            string table = "This table is not available, please try later";
            return table;
            
        }
    }
}
