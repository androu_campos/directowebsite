using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using RKLib.ExportData;

public partial class LiveCallsGraph : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Chart1.Visible = false;
            btn_excel.Visible = false;
            //string idd = this.FindControl("myIframe").ClientID;
            txtFrom.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            
            
        }
        else
        {
            
        }

    }
    protected void cmdGraph_Click(object sender, EventArgs e)
    {
        string queryy = string.Empty;
        string queryyExcel = string.Empty;
        string select = string.Empty;
        string where_ = string.Empty;
        string groupBy = string.Empty;
        
        StringBuilder query = new StringBuilder();
        query.Append(" ");
        StringBuilder where = new StringBuilder();
        where.Append(" ");
        
        if (dpdCustomer.SelectedValue.ToString() != "** NONE **")   //Customer
        {
            query.Append(" Customer,");
            if (dpdCustomer.SelectedValue.ToString() != "**ALL**") where.Append(" AND Customer like'" + dpdCustomer.SelectedValue.ToString() + "'");
            
        }
        if (dpdVendor.SelectedValue.ToString() != "** NONE **")   //Vendor
        {
            query.Append(" Vendor,");
            if (dpdVendor.SelectedValue.ToString() != "**ALL**")
            {
                if (dpdVendor.SelectedValue.ToString() == "OUTLANDER")
                {
                    where.Append(" AND (Vendor like 'OUTLANDER%' OR Vendor = 'STAR-CITY-SCT1') ");
                }
                else
                {
                    where.Append(" AND Vendor like '" + dpdVendor.SelectedValue.ToString() + "'");
                }
            }
        }
        if (dpdCountry.SelectedValue.ToString() != "** NONE **")    //Country
        {
            query.Append(" Country,");
            if (dpdCountry.SelectedValue.ToString() != "**ALL**") where.Append(" AND Country like '" + dpdCountry.SelectedValue.ToString() + "'");
        }
        if (dpdRegion.SelectedValue.ToString() != "** NONE **")    //Region
        {
            query.Append(" Region,");
            if (dpdRegion.SelectedValue.ToString() != "**ALL**") where.Append(" AND Region like '" + dpdRegion.SelectedValue.ToString() + "'");
        }
        if (dpdType.SelectedValue.ToString() != "** NONE **")    //Type
        {
            query.Append(" Type,");

            if (dpdType.SelectedValue.ToString() != "**ALL**")
            {
                if (dpdOrType.SelectedValue.ToString() == "2")
                    where.Append(" AND Type like '" + dpdType.SelectedValue.ToString() + "'");
                else
                    where.Append(" AND Type not like '" + dpdType.SelectedValue.ToString() + "'");
            }
        }
        if(dpdClass.SelectedValue.ToString() != "** NONE **")   //Class
        {
            query.Append(" Class,");

            if (dpdClass.SelectedValue.ToString() != "**ALL**")
            {
                if (dpdOrClass.SelectedValue.ToString() == "2")
                    where.Append(" AND Class like '" + dpdClass.SelectedValue.ToString() + "'");
                else
                    where.Append(" AND Class not like '" + dpdClass.SelectedValue.ToString() + "'");
            }
        }

//Build statement
        where_ = where.ToString();
        //where_ = where_.Substring(0, where_.Length - 3);

        select = "Select [Time],sum(Calls) as Calls, " + query;
        select = select.Substring(0, select.Length);
        groupBy = query.ToString();
        groupBy = groupBy.Substring(0,groupBy.Length-1);
        queryy = "Select [Time],sum(Calls) as Calls from TrafficLogLive Where [Time] >= '" + txtFrom.Text + " 00:00:00' and [Time] <= '" + txtTo.Text + " 23:59:59' and Calls > 0 " + where_ + " group by Time ORDER BY Time";

        if (groupBy.Length > 3)
        {
            queryyExcel = "Select [Time],sum(Calls) as Calls, " + groupBy + " from TrafficLogLive Where [Time] >= '" + txtFrom.Text + " 00:00:00' and [Time] <= '" + txtTo.Text + " 23:59:59' and Calls > 0 " + where_ + " group by Time," + groupBy + " ORDER BY Time";
        }
        else
        {
            queryyExcel = queryy;
        }
        DataTable mytbl = Util.RunQueryByStmnt(queryy).Tables[0];
           
        Session["qe"] = queryyExcel.ToString();
        //make Chart
        makeChart(mytbl, 1);

        //gdvLiveCalls.DataSource = mytbl;
        //gdvLiveCalls.DataBind();

        btn_excel.Visible = true;

    }

    private void makeChart(DataTable tbl,int i)
    {
        int pdx;
        if (i == 1)
        {
            Chart1.RenderType = Dundas.Charting.WebControl.RenderType.ImageTag;
            foreach (DataRow myRow in tbl.Rows)
            {
                pdx = Chart1.Series["Series1"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Calls"]));
                Chart1.Series["Series1"].Points[pdx].ToolTip = String.Format("Time:{0},Calls:{1}", myRow["Time"], myRow["Calls"]);

            }
        }
        else if (i == 2)
        {
            //foreach (DataRow myRow in tbl.Rows)
            //{
            //    pdx = Chart2.Series["Series1"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Calls"]));
            //    Chart2.Series["Series1"].Points[pdx].ToolTip = String.Format("Time:{0},Calls:{1}", myRow["Time"], myRow["Calls"]);
            //}
        }
        else if (i == 3)
        {
            //foreach (DataRow myRow in tbl.Rows)
            //{
            //    pdx = Chart3.Series["Series1"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Calls"]));
            //    Chart3.Series["Series1"].Points[pdx].ToolTip = String.Format("Time:{0},Calls:{1}", myRow["Time"], myRow["Calls"]);
            //}
        }
        else if (i == 4)
        {
            
        }
        else if (i == 5)
        {
            
        }

    }
    protected void btn_excel_Click(object sender, EventArgs e)
    {
        try
        {
        DataTable mytblex = Util.RunQueryByStmnt(Session["qe"].ToString()).Tables[0];
        // Export all the details to CSV
        RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");        
        string filename = "LiveCallsGraph" + ".xls";
        objExport.ExportDetails(mytblex, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string erro = ex.Message.ToString();
        }
    }
    protected void gdvGraph_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
}
