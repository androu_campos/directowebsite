using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Office.Interop;
using System.Data.SqlClient;
using ADODB;
using System.Net.Mail;
using System.Net;
using System.Diagnostics;
using System.Net.Mime;
using System.Globalization;

public partial class WSReportsBuilder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string from = Request.QueryString["from"].ToString();
        string to = Request.QueryString["to"].ToString();            
        createExcelFile(from,to);
    }

    protected void createExcelFile(string from,string to)
    {
        object missValue = System.Reflection.Missing.Value;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        Microsoft.Office.Interop.Excel.Application xlApp = default(Microsoft.Office.Interop.Excel.Application);
        Microsoft.Office.Interop.Excel.Workbook xlWorkBook = default(Microsoft.Office.Interop.Excel.Workbook);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetB = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetC = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetD = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetE = default(Microsoft.Office.Interop.Excel.Worksheet);
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheetF = default(Microsoft.Office.Interop.Excel.Worksheet);

        CultureInfo ciCurr = CultureInfo.CurrentCulture;
        int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

        String sToday = DateTime.Now.ToString("MM.dd.yyyy.HH.mm");
        String sPathFiles = "E:\\Mecca\\Reports\\";
        String sStandardFile = sPathFiles + "WeeklyReport.xls";

        String connectionString = ConfigurationManager.ConnectionStrings["MECCA2ConnectionString"].ConnectionString;

        xlApp = new Microsoft.Office.Interop.Excel.ApplicationClass();

        ADODB.Connection DBConnection = new ADODB.Connection();
        ADODB.Recordset rsData = new ADODB.Recordset();
        ADODB.Recordset rsDataB = new ADODB.Recordset();
        ADODB.Recordset rsDataC = new ADODB.Recordset();
        ADODB.Recordset rsDataD = new ADODB.Recordset();
        ADODB.Recordset rsDataE = new ADODB.Recordset();
        ADODB.Recordset rsDataF = new ADODB.Recordset();

        DBConnection.Open("Provider='SQLOLEDB.1';" + connectionString, "", "", -1);
        DBConnection.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
        DBConnection.CommandTimeout = 3600;
        rsData.Open("EXEC Mecca2.dbo.sp_WSReportCustomer '" + from + "', '" + to + "' WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataB.Open("EXEC Mecca2.dbo.sp_WSReportVendor '" + from + "', '" + to + "' WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataC.Open("EXEC Mecca2.dbo.sp_WSReportLmc '" + from + "', '" + to + "' WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataD.Open("EXEC Mecca2.dbo.sp_WSReportCustomerVendor '" + from + "', '" + to + "' WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataE.Open("EXEC Mecca2.dbo.sp_WSReportVendorCustomer '" + from + "', '" + to + "' WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataF.Open("EXEC Mecca2.dbo.sp_WSReportLmcCC '" + from + "', '" + to + "' WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);


        xlWorkBook = xlApp.Workbooks.Open(sStandardFile, 0, false, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", false, false, 0, false, false, 0);
        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        xlWorkSheetB = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
        xlWorkSheetC = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(3);
        xlWorkSheetF = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(4);
        xlWorkSheetD = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(5);
        xlWorkSheetE = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(6);

        Microsoft.Office.Interop.Excel.Range tblRange = xlWorkSheet.get_Range("A2","A2");
        Microsoft.Office.Interop.Excel.Range tblRangeB = xlWorkSheetB.get_Range("A2", "A2");
        Microsoft.Office.Interop.Excel.Range tblRangeC = xlWorkSheetC.get_Range("A2", "A2");
        Microsoft.Office.Interop.Excel.Range tblRangeD = xlWorkSheetD.get_Range("A2", "A2");
        Microsoft.Office.Interop.Excel.Range tblRangeE = xlWorkSheetE.get_Range("A2", "A2");
        Microsoft.Office.Interop.Excel.Range tblRangeF = xlWorkSheetF.get_Range("A2", "A2");

        //Microsoft.Office.Interop.Excel.Range tblRange = xlApp.get_Range("A2", "A2");
        //Microsoft.Office.Interop.Excel.Range tblRangeB = xlApp.get_Range("A2", "A2");
        //Microsoft.Office.Interop.Excel.Range tblRangeC = xlApp.get_Range("A2", "A2");
        //Microsoft.Office.Interop.Excel.Range tblRangeD = xlApp.get_Range("A2", "A2");
        //Microsoft.Office.Interop.Excel.Range tblRangeE = xlApp.get_Range("A2", "A2");

        int iData = tblRange.CopyFromRecordset(rsData, missValue, missValue);
        int iDataB = tblRangeB.CopyFromRecordset(rsDataB, missValue, missValue);
        int iDataC = tblRangeC.CopyFromRecordset(rsDataC, missValue, missValue);
        int iDataD = tblRangeD.CopyFromRecordset(rsDataD, missValue, missValue);
        int iDataE = tblRangeE.CopyFromRecordset(rsDataE, missValue, missValue);
        int iDataF = tblRangeF.CopyFromRecordset(rsDataF, missValue, missValue);

        int headersCount = rsData.Fields.Count;
        int headersCountB = rsDataB.Fields.Count;
        int headersCountC = rsDataC.Fields.Count;
        int headersCountD = rsDataD.Fields.Count;
        int headersCountE = rsDataE.Fields.Count;
        int headersCountF = rsDataF.Fields.Count;


        xlApp.get_Range("A2", sb.ToString() + "AJ2").Copy(missValue);

        Microsoft.Office.Interop.Excel.Range tblData = xlWorkSheet.get_Range("A2", "AJ"+(1+iData));
        Microsoft.Office.Interop.Excel.Range tblDataB = xlWorkSheetB.get_Range("A2", "AJ" + (1 + iDataB));
        Microsoft.Office.Interop.Excel.Range tblDataC = xlWorkSheetC.get_Range("A2", "L" + (1 + iDataC));
        Microsoft.Office.Interop.Excel.Range tblDataD = xlWorkSheetD.get_Range("A2", "AM" + (1 + iDataD));
        Microsoft.Office.Interop.Excel.Range tblDataE = xlWorkSheetE.get_Range("A2", "AM" + (1 + iDataE));
        Microsoft.Office.Interop.Excel.Range tblDataF = xlWorkSheetF.get_Range("A2", "L" + (1 + iDataF));

        tblData.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        tblDataB.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        tblDataC.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        tblDataD.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        tblDataE.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        tblDataF.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        
        //Microsoft.Office.Interop.Excel.Range tblData = xlApp.get_Range("A3", sb.ToString() + (2 + iData));
        //Microsoft.Office.Interop.Excel.Range tblDataB = xlApp.get_Range("A17", sb.ToString() + (6 + iData + iDataB));
        //tblData.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        //tblDataB.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        //tblData.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //tblDataB.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";

        

        xlWorkSheet.get_Range("A1", "AJ1").Font.Bold = true;
        xlWorkSheetB.get_Range("A1", "AJ1").Font.Bold = true;
        xlWorkSheetC.get_Range("A1", "L1").Font.Bold = true;
        xlWorkSheetD.get_Range("A1", "AM1").Font.Bold = true;
        xlWorkSheetE.get_Range("A1", "AM1").Font.Bold = true;
        xlWorkSheetF.get_Range("A1", "L1").Font.Bold = true;

        xlWorkSheet.get_Range("A1", "AJ1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheetB.get_Range("A1", "AJ1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheetC.get_Range("A1", "L1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheetD.get_Range("A1", "AM1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheetE.get_Range("A1", "AM1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheetF.get_Range("A1", "L1").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

        xlWorkSheet.get_Range("C2", "E"+(2+iData)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheet.get_Range("K2", "N" + (2 + iData)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheet.get_Range("U2", "X" + (2 + iData)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheet.get_Range("AE2", "AH" + (2 + iData)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheet.get_Range("F2", "F" + (1 + iData)).NumberFormat = "0.0%";
        xlWorkSheet.get_Range("O2", "P" + (1 + iData)).NumberFormat = "0.0%";
        xlWorkSheet.get_Range("Y2", "Z" + (1 + iData)).NumberFormat = "0.0%";
        xlWorkSheet.get_Range("AI2", "AJ" + (1 + iData)).NumberFormat = "0.0%";
        xlWorkSheet.get_Range("I2", "J" + (1 + iData)).NumberFormat = "0.0000";
        xlWorkSheet.get_Range("S2", "T" + (1 + iData)).NumberFormat = "0.0000";
        xlWorkSheet.get_Range("AC2", "AD" + (1 + iData)).NumberFormat = "0.0000";

        xlWorkSheet.get_Range("C" + (2 + iData), "C" + (2 + iData)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iData) + ")";
        xlWorkSheet.get_Range("D" + (2 + iData), "D" + (2 + iData)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iData) + ")";
        xlWorkSheet.get_Range("E" + (2 + iData), "E" + (2 + iData)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iData) + ")";

        xlWorkSheet.get_Range("K" + (2 + iData), "K" + (2 + iData)).Formula = "=SUBTOTAL(109,K2:K" + (1 + iData) + ")";
        xlWorkSheet.get_Range("L" + (2 + iData), "L" + (2 + iData)).Formula = "=SUBTOTAL(109,L2:L" + (1 + iData) + ")";
        xlWorkSheet.get_Range("M" + (2 + iData), "M" + (2 + iData)).Formula = "=SUBTOTAL(109,M2:M" + (1 + iData) + ")";
        xlWorkSheet.get_Range("N" + (2 + iData), "N" + (2 + iData)).Formula = "=SUBTOTAL(109,N2:N" + (1 + iData) + ")";

        xlWorkSheet.get_Range("U" + (2 + iData), "U" + (2 + iData)).Formula = "=SUBTOTAL(109,U2:U" + (1 + iData) + ")";
        xlWorkSheet.get_Range("V" + (2 + iData), "V" + (2 + iData)).Formula = "=SUBTOTAL(109,V2:V" + (1 + iData) + ")";
        xlWorkSheet.get_Range("W" + (2 + iData), "W" + (2 + iData)).Formula = "=SUBTOTAL(109,W2:W" + (1 + iData) + ")";
        xlWorkSheet.get_Range("X" + (2 + iData), "X" + (2 + iData)).Formula = "=SUBTOTAL(109,X2:X" + (1 + iData) + ")";

        xlWorkSheet.get_Range("AE" + (2 + iData), "AE" + (2 + iData)).Formula = "=SUBTOTAL(109,AE2:AE" + (1 + iData) + ")";
        xlWorkSheet.get_Range("AF" + (2 + iData), "AF" + (2 + iData)).Formula = "=SUBTOTAL(109,AF2:AF" + (1 + iData) + ")";
        xlWorkSheet.get_Range("AG" + (2 + iData), "AG" + (2 + iData)).Formula = "=SUBTOTAL(109,AG2:AG" + (1 + iData) + ")";
        xlWorkSheet.get_Range("AH" + (2 + iData), "AH" + (2 + iData)).Formula = "=SUBTOTAL(109,AH2:AH" + (1 + iData) + ")";
        
        xlWorkSheetB.get_Range("C2", "E" + (2 + iDataB)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetB.get_Range("K2", "N" + (2 + iDataB)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetB.get_Range("U2", "X" + (2 + iDataB)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetB.get_Range("AE2", "AH" + (2 + iDataB)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetB.get_Range("F2", "F" + (1 + iDataB)).NumberFormat = "0.0%";
        xlWorkSheetB.get_Range("O2", "P" + (1 + iDataB)).NumberFormat = "0.0%";
        xlWorkSheetB.get_Range("Y2", "Z" + (1 + iDataB)).NumberFormat = "0.0%";
        xlWorkSheetB.get_Range("AI2", "AJ" + (1 + iDataB)).NumberFormat = "0.0%";
        xlWorkSheetB.get_Range("I2", "J" + (1 + iDataB)).NumberFormat = "0.0000";
        xlWorkSheetB.get_Range("S2", "T" + (1 + iDataB)).NumberFormat = "0.0000";
        xlWorkSheetB.get_Range("AC2", "AD" + (1 + iDataB)).NumberFormat = "0.0000";

        xlWorkSheetB.get_Range("C" + (2 + iDataB), "C" + (2 + iDataB)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iDataB) + ")";
        xlWorkSheetB.get_Range("D" + (2 + iDataB), "D" + (2 + iDataB)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iDataB) + ")";
        xlWorkSheetB.get_Range("E" + (2 + iDataB), "E" + (2 + iDataB)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iDataB) + ")";

        xlWorkSheetB.get_Range("K" + (2 + iDataB), "K" + (2 + iDataB)).Formula = "=SUBTOTAL(109,K2:K" + (1 + iDataB) + ")";
        xlWorkSheetB.get_Range("L" + (2 + iDataB), "L" + (2 + iDataB)).Formula = "=SUBTOTAL(109,L2:L" + (1 + iDataB) + ")";
        xlWorkSheetB.get_Range("M" + (2 + iDataB), "M" + (2 + iDataB)).Formula = "=SUBTOTAL(109,M2:M" + (1 + iDataB) + ")";
        xlWorkSheetB.get_Range("N" + (2 + iDataB), "N" + (2 + iDataB)).Formula = "=SUBTOTAL(109,N2:N" + (1 + iDataB) + ")";

        xlWorkSheetB.get_Range("U" + (2 + iDataB), "U" + (2 + iDataB)).Formula = "=SUBTOTAL(109,U2:U" + (1 + iDataB) + ")";
        xlWorkSheetB.get_Range("V" + (2 + iDataB), "V" + (2 + iDataB)).Formula = "=SUBTOTAL(109,V2:V" + (1 + iDataB) + ")";
        xlWorkSheetB.get_Range("W" + (2 + iDataB), "W" + (2 + iDataB)).Formula = "=SUBTOTAL(109,W2:W" + (1 + iDataB) + ")";
        xlWorkSheetB.get_Range("X" + (2 + iDataB), "X" + (2 + iDataB)).Formula = "=SUBTOTAL(109,X2:X" + (1 + iDataB) + ")";

        xlWorkSheetB.get_Range("AE" + (2 + iDataB), "AE" + (2 + iDataB)).Formula = "=SUBTOTAL(109,AE2:AE" + (1 + iDataB) + ")";
        xlWorkSheetB.get_Range("AF" + (2 + iDataB), "AF" + (2 + iDataB)).Formula = "=SUBTOTAL(109,AF2:AF" + (1 + iDataB) + ")";
        xlWorkSheetB.get_Range("AG" + (2 + iDataB), "AG" + (2 + iDataB)).Formula = "=SUBTOTAL(109,AG2:AG" + (1 + iDataB) + ")";
        xlWorkSheetB.get_Range("AH" + (2 + iDataB), "AH" + (2 + iDataB)).Formula = "=SUBTOTAL(109,AH2:AH" + (1 + iDataB) + ")";        


        xlWorkSheetC.get_Range("C2", "F" + (2 + iDataC)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetC.get_Range("K2", "L" + (1 + iDataC)).NumberFormat = "0.0000";
        xlWorkSheetC.get_Range("H2", "J" + (1 + iDataC)).NumberFormat = "0.00";
        xlWorkSheetC.get_Range("G2", "G" + (1 + iDataC)).NumberFormat = "0.0%";

        xlWorkSheetC.get_Range("C" + (2 + iDataC), "C" + (2 + iDataC)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iDataC) + ")";
        xlWorkSheetC.get_Range("D" + (2 + iDataC), "D" + (2 + iDataC)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iDataC) + ")";
        xlWorkSheetC.get_Range("E" + (2 + iDataC), "E" + (2 + iDataC)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iDataC) + ")";
        xlWorkSheetC.get_Range("F" + (2 + iDataC), "F" + (2 + iDataC)).Formula = "=SUBTOTAL(109,F2:F" + (1 + iDataC) + ")";


        xlWorkSheetD.get_Range("C2", "E" + (2 + iDataD)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetD.get_Range("L2", "O" + (2 + iDataD)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetD.get_Range("W2", "Z" + (2 + iDataD)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetD.get_Range("AH2", "AK" + (2 + iDataD)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetD.get_Range("F2", "F" + (1 + iDataD)).NumberFormat = "0.0%";
        xlWorkSheetD.get_Range("P2", "Q" + (1 + iDataD)).NumberFormat = "0.0%";
        xlWorkSheetD.get_Range("AA2", "AB" + (1 + iDataD)).NumberFormat = "0.0%";
        xlWorkSheetD.get_Range("AL2", "AM" + (1 + iDataD)).NumberFormat = "0.0%";
        xlWorkSheetD.get_Range("J2", "K" + (1 + iDataD)).NumberFormat = "0.0000";
        xlWorkSheetD.get_Range("U2", "V" + (1 + iDataD)).NumberFormat = "0.0000";
        xlWorkSheetD.get_Range("AF2", "AG" + (1 + iDataD)).NumberFormat = "0.0000";

        xlWorkSheetD.get_Range("C" + (2 + iDataD), "C" + (2 + iDataD)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iDataD) + ")";
        xlWorkSheetD.get_Range("D" + (2 + iDataD), "D" + (2 + iDataD)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iDataD) + ")";
        xlWorkSheetD.get_Range("E" + (2 + iDataD), "E" + (2 + iDataD)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iDataD) + ")";

        xlWorkSheetD.get_Range("L" + (2 + iDataD), "L" + (2 + iDataD)).Formula = "=SUBTOTAL(109,L2:L" + (1 + iDataD) + ")";
        xlWorkSheetD.get_Range("M" + (2 + iDataD), "M" + (2 + iDataD)).Formula = "=SUBTOTAL(109,M2:M" + (1 + iDataD) + ")";
        xlWorkSheetD.get_Range("N" + (2 + iDataD), "N" + (2 + iDataD)).Formula = "=SUBTOTAL(109,N2:N" + (1 + iDataD) + ")";
        xlWorkSheetD.get_Range("O" + (2 + iDataD), "O" + (2 + iDataD)).Formula = "=SUBTOTAL(109,O2:O" + (1 + iDataD) + ")";

        xlWorkSheetD.get_Range("W" + (2 + iDataD), "W" + (2 + iDataD)).Formula = "=SUBTOTAL(109,W2:W" + (1 + iDataD) + ")";
        xlWorkSheetD.get_Range("X" + (2 + iDataD), "X" + (2 + iDataD)).Formula = "=SUBTOTAL(109,X2:X" + (1 + iDataD) + ")";
        xlWorkSheetD.get_Range("Y" + (2 + iDataD), "Y" + (2 + iDataD)).Formula = "=SUBTOTAL(109,Y2:Y" + (1 + iDataD) + ")";
        xlWorkSheetD.get_Range("Z" + (2 + iDataD), "Z" + (2 + iDataD)).Formula = "=SUBTOTAL(109,Z2:Z" + (1 + iDataD) + ")";

        xlWorkSheetD.get_Range("AH" + (2 + iDataD), "AH" + (2 + iDataD)).Formula = "=SUBTOTAL(109,AH2:AH" + (1 + iDataD) + ")";
        xlWorkSheetD.get_Range("AI" + (2 + iDataD), "AI" + (2 + iDataD)).Formula = "=SUBTOTAL(109,AI2:AI" + (1 + iDataD) + ")";
        xlWorkSheetD.get_Range("AJ" + (2 + iDataD), "AJ" + (2 + iDataD)).Formula = "=SUBTOTAL(109,AJ2:AJ" + (1 + iDataD) + ")";
        xlWorkSheetD.get_Range("AK" + (2 + iDataD), "AK" + (2 + iDataD)).Formula = "=SUBTOTAL(109,AK2:AK" + (1 + iDataD) + ")";        


        xlWorkSheetE.get_Range("C2", "E" + (2 + iDataE)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetE.get_Range("L2", "O" + (2 + iDataE)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetE.get_Range("W2", "Z" + (2 + iDataE)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetE.get_Range("AH2", "AK" + (2 + iDataE)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetE.get_Range("F2", "F" + (1 + iDataE)).NumberFormat = "0.0%";
        xlWorkSheetE.get_Range("P2", "Q" + (1 + iDataE)).NumberFormat = "0.0%";
        xlWorkSheetE.get_Range("AA2", "AB" + (1 + iDataE)).NumberFormat = "0.0%";
        xlWorkSheetE.get_Range("AL2", "AM" + (1 + iDataE)).NumberFormat = "0.0%";
        xlWorkSheetE.get_Range("J2", "K" + (1 + iDataE)).NumberFormat = "0.0000";
        xlWorkSheetE.get_Range("U2", "V" + (1 + iDataE)).NumberFormat = "0.0000";
        xlWorkSheetE.get_Range("AF2", "AG" + (1 + iDataE)).NumberFormat = "0.0000";

        xlWorkSheetE.get_Range("C" + (2 + iDataE), "C" + (2 + iDataE)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iDataE) + ")";
        xlWorkSheetE.get_Range("D" + (2 + iDataE), "D" + (2 + iDataE)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iDataE) + ")";
        xlWorkSheetE.get_Range("E" + (2 + iDataE), "E" + (2 + iDataE)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iDataE) + ")";

        xlWorkSheetE.get_Range("L" + (2 + iDataE), "L" + (2 + iDataE)).Formula = "=SUBTOTAL(109,L2:L" + (1 + iDataE) + ")";
        xlWorkSheetE.get_Range("M" + (2 + iDataE), "M" + (2 + iDataE)).Formula = "=SUBTOTAL(109,M2:M" + (1 + iDataE) + ")";
        xlWorkSheetE.get_Range("N" + (2 + iDataE), "N" + (2 + iDataE)).Formula = "=SUBTOTAL(109,N2:N" + (1 + iDataE) + ")";
        xlWorkSheetE.get_Range("O" + (2 + iDataE), "O" + (2 + iDataE)).Formula = "=SUBTOTAL(109,O2:O" + (1 + iDataE) + ")";

        xlWorkSheetE.get_Range("W" + (2 + iDataE), "W" + (2 + iDataE)).Formula = "=SUBTOTAL(109,W2:W" + (1 + iDataE) + ")";
        xlWorkSheetE.get_Range("X" + (2 + iDataE), "X" + (2 + iDataE)).Formula = "=SUBTOTAL(109,X2:X" + (1 + iDataE) + ")";
        xlWorkSheetE.get_Range("Y" + (2 + iDataE), "Y" + (2 + iDataE)).Formula = "=SUBTOTAL(109,Y2:Y" + (1 + iDataE) + ")";
        xlWorkSheetE.get_Range("Z" + (2 + iDataE), "Z" + (2 + iDataE)).Formula = "=SUBTOTAL(109,Z2:Z" + (1 + iDataE) + ")";

        xlWorkSheetE.get_Range("AH" + (2 + iDataE), "AH" + (2 + iDataE)).Formula = "=SUBTOTAL(109,AH2:AH" + (1 + iDataE) + ")";
        xlWorkSheetE.get_Range("AI" + (2 + iDataE), "AI" + (2 + iDataE)).Formula = "=SUBTOTAL(109,AI2:AI" + (1 + iDataE) + ")";
        xlWorkSheetE.get_Range("AJ" + (2 + iDataE), "AJ" + (2 + iDataE)).Formula = "=SUBTOTAL(109,AJ2:AJ" + (1 + iDataE) + ")";
        xlWorkSheetE.get_Range("AK" + (2 + iDataE), "AK" + (2 + iDataE)).Formula = "=SUBTOTAL(109,AK2:AK" + (1 + iDataE) + ")";

        xlWorkSheetF.get_Range("C2", "F" + (2 + iDataF)).NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        xlWorkSheetF.get_Range("K2", "L" + (1 + iDataF)).NumberFormat = "0.0000";
        xlWorkSheetF.get_Range("H2", "J" + (1 + iDataF)).NumberFormat = "0.00";
        xlWorkSheetF.get_Range("G2", "G" + (1 + iDataF)).NumberFormat = "0.0%";

        xlWorkSheetF.get_Range("C" + (2 + iDataF), "C" + (2 + iDataF)).Formula = "=SUBTOTAL(109,C2:C" + (1 + iDataF) + ")";
        xlWorkSheetF.get_Range("D" + (2 + iDataF), "D" + (2 + iDataF)).Formula = "=SUBTOTAL(109,D2:D" + (1 + iDataF) + ")";
        xlWorkSheetF.get_Range("E" + (2 + iDataF), "E" + (2 + iDataF)).Formula = "=SUBTOTAL(109,E2:E" + (1 + iDataF) + ")";
        xlWorkSheetF.get_Range("F" + (2 + iDataF), "F" + (2 + iDataF)).Formula = "=SUBTOTAL(109,F2:F" + (1 + iDataF) + ")";

        //xlWorkSheet.get_Range("A16", sb.ToString() + "16").Font.Bold = true;
        //xlWorkSheet.get_Range("A2", sb.ToString() + "2").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        //xlWorkSheet.get_Range("A16", sb.ToString() + "16").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

        string[] hnames = new string[headersCount];
        string[] hnamesb = new string[headersCountB];
        string[] hnamesc = new string[headersCountC];
        string[] hnamesd = new string[headersCountD];
        string[] hnamese = new string[headersCountE];
        string[] hnamesf = new string[headersCountF]; 

        int i = 0;
        int h = 0;

        foreach (ADODB.Field adoField in rsData.Fields)
        {
            hnames[i] = adoField.Name;
            i++;
        }

        foreach (ADODB.Field adoField in rsDataB.Fields)
        {
            hnamesb[h] = adoField.Name;
            h++;
        }

        i = 0;
        h = 0;

        foreach (ADODB.Field adoField in rsDataC.Fields)
        {
            hnamesc[i] = adoField.Name;
            i++;
        }

        foreach (ADODB.Field adoField in rsDataD.Fields)
        {
            hnamesd[h] = adoField.Name;
            h++;
        }

        i = 0;
        h = 0;

        foreach (ADODB.Field adoField in rsDataE.Fields)
        {
            hnamese[i] = adoField.Name;
            i++;
        }

        foreach (ADODB.Field adoField in rsDataF.Fields)
        {
            hnamesf[h] = adoField.Name;
            h++;
        }

        xlWorkSheet.get_Range("A1", "AJ1").Value2 = hnames;
        xlWorkSheetB.get_Range("A1", "AJ1").Value2 = hnamesb;
        xlWorkSheetC.get_Range("A1", "L1").Value2 = hnamesc;
        xlWorkSheetD.get_Range("A1", "AM1").Value2 = hnamesd;
        xlWorkSheetE.get_Range("A1", "AM1").Value2 = hnamese;
        xlWorkSheetF.get_Range("A1", "L1").Value2 = hnamesf;


        //xlWorkSheet.get_Range("A16", sb.ToString() + "16").Value2 = hnamesb;
        //xlWorkSheet.get_Range("A13", "A13").Value2 = "VENTA TOTAL";
        //xlWorkSheet.get_Range("A14", "A14").Value2 = "AVANCE META";

        //xlWorkSheet.get_Range("A13", "A14").Font.Bold = true;
        //xlWorkSheet.get_Range("A25", "A26").Font.Bold = true;

        //xlWorkSheet.get_Range("A13", "A14").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        //xlWorkSheet.get_Range("A25", "A26").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

        //xlWorkSheet.get_Range("A25", "A25").Value2 = "PROFIT";
        //xlWorkSheet.get_Range("A26", "A26").Value2 = "PROFIT %";

        //i = 0;
        //h = 0;
        //char l = 'B';
        //char m = 'B';


        //while (i < (headersCount - 1))
        //{
        //    Microsoft.Office.Interop.Excel.Range oRng = xlWorkSheet.get_Range(l.ToString() + "13", l.ToString() + "13");
        //    Microsoft.Office.Interop.Excel.Range oRngB = xlWorkSheet.get_Range(l.ToString() + "14", l.ToString() + "14");
        //    oRng.Formula = "=SUM(" + l.ToString() + "3:" + l.ToString() + "12)";
        //    oRngB.Formula = "=" + l.ToString() + "13/35000";
        //    oRng.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        //    oRngB.NumberFormat = "0.0%";
        //    l++;
        //    i++;
        //}

        //while (h < (headersCountB - 1))
        //{
        //    Microsoft.Office.Interop.Excel.Range oRngC = xlWorkSheet.get_Range(m.ToString() + "23", m.ToString() + "23");
        //    Microsoft.Office.Interop.Excel.Range oRngD = xlWorkSheet.get_Range(m.ToString() + "25", m.ToString() + "25");
        //    Microsoft.Office.Interop.Excel.Range oRngE = xlWorkSheet.get_Range(m.ToString() + "26", m.ToString() + "26");

        //    oRngC.Formula = "=SUM(" + m.ToString() + "17:" + m.ToString() + "22)";
        //    oRngC.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";

        //    oRngD.Formula = "=" + m.ToString() + "13 - " + m.ToString() + "23";
        //    oRngD.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";

        //    oRngE.Formula = "=" + m.ToString() + "25 / " + m.ToString() + "13";
        //    oRngE.NumberFormat = "0.0%";
        //    m++;
        //    h++;
        //}





        //tblData = xlApp.get_Range("D12", "F" + (11 + iRates));
        //tblData.Formula = tblRates.Value2;

        //tblRange = xlApp.get_Range("A11", missValue);
        //tblRange.Activate();

        rsData.Close();
        rsDataB.Close();
        rsDataC.Close();
        rsDataD.Close();
        rsDataE.Close();
        rsDataF.Close();
        DBConnection.Close();

        xlApp.DisplayAlerts = false;
        xlApp.AlertBeforeOverwriting = false;



        xlWorkBook.SaveAs(sPathFiles + "WeeklyReport_Week"+ (weekNum-1) + "_" + sToday + ".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, missValue, missValue, missValue, missValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, missValue, missValue, missValue, missValue);
        xlWorkBook.Close(false, missValue, missValue);

        xlApp.Quit();
        //xlApp.Visible = true;

        releaseObject(xlWorkSheet);
        releaseObject(xlWorkSheetB);
        releaseObject(xlWorkSheetC);
        releaseObject(xlWorkSheetD);
        releaseObject(xlWorkSheetE);
        releaseObject(xlWorkSheetF);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        while (xlApp != null)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
            xlApp = null;
        }

        GC.Collect();
        GC.WaitForPendingFinalizers();


        //HyperLink1.NavigateUrl = "http://mecca.directo.com/ws/op-leverage/" + "OperativeLeverage_" + sToday + ".xls";
        //HyperLink1.Visible = true;

        Response.Redirect("http://mecca.directo.com/reports-mecca/WeeklyReport_Week" + (weekNum - 1) + "_" + sToday + ".xls");


    }

    private void releaseObject(object obj)
    {
        if (obj != null)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //lblError.Text = "Exception Occured while releasing object " + ex.ToString();
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
