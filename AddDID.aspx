<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="AddDID.aspx.cs" Inherits="AddDID" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script language="javascript" type="text/javascript">

    function vdid()
    {
       var did;
       did = document.getElementById('<%=txtdid.ClientID%>').value;
       
       if(isNaN(did))
       {
            alert("The DID needs to be numeric.");
		    document.getElementById('<%=txtdid.ClientID%>').value = "";
		    document.getElementById('<%=txtdid.ClientID%>').focus();
       }
    }
    
    function validFields()
    {
        var city;
        var state;
        
        
        city = document.getElementById('<%=txtCity.ClientID%>').value;
        state = document.getElementById('<%=txtstate.ClientID%>').value;
        
        
        if(city == "") 
        {
            alert("Please Insert a City Name.");
            return false;
        }
        else if (state == "")
        {
            alert("Please Insert the State Name.");
            return false;
        }             
        return true;
    }

</script>

    <table>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Font-Bold="True" Font-Size="10pt" Text="Add DID"></asp:Label>
            </td>            
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">                
                <asp:Label ID="lbldid" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="DID #"></asp:Label>
            </td>
            <td style="width: 100px">
                <asp:TextBox ID="txtdid" runat="server" CssClass="dropdown" Width="120px" OnBlur="vdid()"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                <asp:Label ID="lblcountry" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Country"></asp:Label>                
            </td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCountry" runat="server" AutoPostBack="True" CssClass="dropdown"
                    DataTextField="Country" DataValueField="Country" 
                    Width="130px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                <asp:Label ID="lblcity" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="City"></asp:Label>
            </td>
            <td style="width: 100px">
                <asp:TextBox ID="txtCity" runat="server" CssClass="dropdown" Width="120px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                <asp:Label ID="lblstprefix" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="StPrefix"></asp:Label>
            </td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdstprefix" runat="server" AutoPostBack="True" CssClass="dropdown"
                    DataTextField="StateAbb" DataValueField="StateAbb" OnSelectedIndexChanged="dpdStPrefix_SelectedIndexChanged"
                    Width="130px">
                </asp:DropDownList>                
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                <asp:Label ID="lblstate" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="State"></asp:Label>
            </td>
            <td style="width: 100px">
                <asp:TextBox ID="txtstate" runat="server" CssClass="dropdown" Width="120px" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                <asp:Label ID="lblprovider" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Provider"></asp:Label>
            </td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdProvider" runat="server" Width="130px" CssClass="dropdown" DataTextField="provider" DataValueField="provider">                                        
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem>BLITZ</asp:ListItem>
                    <asp:ListItem>NUVOX</asp:ListItem>
                    <asp:ListItem>PAC-WEST</asp:ListItem>
                    <asp:ListItem>VOICE-STEP</asp:ListItem>
                    <asp:ListItem>NETCOMM</asp:ListItem>
                    <asp:ListItem>ETOLLFREE</asp:ListItem>  
                    <asp:ListItem>MAXCOM</asp:ListItem>
                    <asp:ListItem>AVALON</asp:ListItem>
                    <asp:ListItem>RNK</asp:ListItem>   
                    <asp:ListItem>NETTOWN</asp:ListItem>                                                                 
                    <asp:ListItem>TOKTELECOM</asp:ListItem> 
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 84px" bgcolor="#d5e3f0">
                <asp:Label ID="lblframework" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Framework"></asp:Label>
            </td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdFramework" runat="server" Width="130px" CssClass="dropdown" DataTextField="provider" DataValueField="provider">                                        
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem>NACT</asp:ListItem>
                    <asp:ListItem>PHOENIX SOFT</asp:ListItem>                                       
                    <asp:ListItem>WEBVOIP</asp:ListItem> 
                    <asp:ListItem>A2BILLING</asp:ListItem> 
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="cmdSave" runat="server" OnClientClick="return validFields();" OnClick="cmdSave_Click" Text="Save" BorderStyle="Solid" BorderWidth="1px" Font-Bold="False" CssClass="boton" />
            </td>
            <td align="right">
                <asp:Button ID="cmdCancel" runat="server" OnClick="cmdCancel_Click" Text="Cancel" BorderStyle="Solid" BorderWidth="1px" Font-Bold="False" CssClass="boton" onclientclick="if(!confirm('Are you sure you want to cancel the operation?')) return;" UseSubmitBehavior="False" />
            </td>
        </tr>
    </table>
</asp:Content>

