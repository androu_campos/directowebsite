using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class PrintInvoice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int i = Request.Form.Count;
        lblHeader.Text = "";
        string query1 = string.Empty;
        string query2 = string.Empty;
        string query3 = string.Empty;
       
        string[] invoiceArray1 = new string[12];

        invoiceArray1 = (string[])Session["invoiceParams"];

        query1 = "select CONVERT(VARCHAR(11) ,Billingdate,101) as [Date],sum(Calls) as [Completed Calls], Sum(CustMinutes) as [Billed Minutes], sum(TotalSales) as Amount from Mecca2.dbo.SalesLog where Customer like '" + invoiceArray1[0].ToString() + "'  and BillingDate >= '" + invoiceArray1[4].ToString() + "'  and BillingDate <= '" + invoiceArray1[5].ToString() + "' group by Billingdate, Customer order by Billingdate";
        query2 = "select sum(Calls) as [Completed Calls], Sum(CustMinutes) as [Billed Minutes], Price, sum(TotalSales) as Amount from Mecca2.dbo.SalesLog where Customer like '" + invoiceArray1[0].ToString() + "'  and BillingDate >= '" + invoiceArray1[4].ToString() + "' and BillingDate <= '" + invoiceArray1[5].ToString() + "' group by Customer, Price having sum(CustMinutes)>0";
        query3 = "select sum(Calls) as [Total Calls], Sum(CustMinutes) as [Total Minutes], '******' as Price, sum(TotalSales) as [Total Amount] from Mecca2.dbo.SalesLog where Customer like '" + invoiceArray1[0].ToString() + "'  and BillingDate >= '" + invoiceArray1[4].ToString() + "' and BillingDate <= '" + invoiceArray1[5].ToString() + "' group by Customer order by sum(TotalSales)";

        GridView1.DataSource = Util.RunQueryByStmnt(query1).Tables[0];
        GridView1.DataBind();

        GridView2.DataSource = Util.RunQueryByStmnt(query2).Tables[0];
        GridView2.DataBind();

        GridView3.DataSource = Util.RunQueryByStmnt(query3).Tables[0];
        GridView3.DataBind();


        lblCustomer.Text = invoiceArray1[0].ToString();
        lblAddress.Text = invoiceArray1[1].ToString();
        lblPhone.Text = invoiceArray1[2].ToString();
        lblBillDate.Text = DateTime.Now.ToShortDateString();
        lblFrom.Text = invoiceArray1[4].ToString();
        lblTo.Text = invoiceArray1[5].ToString();
        lblAmound.Text = GridView3.Rows[0].Cells[3].Text;
        lblPreviousBalance.Text = invoiceArray1[9].ToString();
        lblPayments.Text = invoiceArray1[10].ToString();
        lblLateFees.Text = invoiceArray1[11].ToString();
        lblAdjustments.Text = invoiceArray1[8].ToString();
        lblCurrentCharges.Text = GridView3.Rows[0].Cells[3].Text;
        lblDueDate.Text = invoiceArray1[7].ToString();
        

        

        lblPastDueBalance.Text = Convert.ToString( Convert.ToDouble(invoiceArray1[9]) + Convert.ToDouble(invoiceArray1[8]) + Convert.ToDouble(invoiceArray1[10]) );
        lblTotalDue.Text = Convert.ToString( Convert.ToDouble(GridView3.Rows[0].Cells[3].Text) + Convert.ToDouble(lblPastDueBalance.Text) );

        this.Page.Title = invoiceArray1[0].ToString() + " Invoice: From " + invoiceArray1[4].ToString() + " - " + invoiceArray1[5].ToString(); ;


    }
}
