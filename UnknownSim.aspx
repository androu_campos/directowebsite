<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"  AutoEventWireup="true" CodeFile="UnknownSim.aspx.cs" Inherits="Unknown" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="left">
        <tr>
            <td style="width: 100px; height: 22px;" align="left">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="Unknown SIMs" Font-Bold="True"></asp:Label></td>
            <td style="width: 100px; height: 22px;">
                <asp:Label ID="lblmessage" runat="server" Font-Overline="False" Font-Size="8pt" ForeColor="Red"
                    Text="No unknown SIMs found !!" Visible="False" Width="194px" Font-Bold="True" Font-Names="Arial"></asp:Label></td>
            <td style="width: 100px; height: 22px;">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" rowspan="2">
            </td>
            <td colspan="2" rowspan="2">
                <asp:GridView ID="gdvUnknown" runat="server" AutoGenerateColumns="False" Font-Names="Arial" Font-Size="8pt" DataSourceID="SqldsUnknownSim">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <Columns>
                        <asp:HyperLinkField DataNavigateUrlFields="SIM" DataNavigateUrlFormatString="AddSIM.aspx?SIM={0}"
                            DataTextField="SIM" HeaderText="SIM" />
                        <asp:BoundField DataField="Calls" HeaderText="Calls" SortExpression="Calls" />
                        <asp:BoundField DataField="Minutes" HeaderText="Minutes" SortExpression="Minutes" />
                
                </Columns>
                
                
                </asp:GridView>
                &nbsp;
            </td>
        </tr>
        <tr>
        </tr>
    </table>
    <asp:SqlDataSource ID="SqldsUnknownSim" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
        ProviderName="<%$ ConnectionStrings:QUESCOMConnectionString.ProviderName %>"
        SelectCommand="SELECT * FROM UnKnownSim"></asp:SqlDataSource>
</asp:Content>

