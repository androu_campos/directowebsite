<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PrintInvoicebeta1.aspx.cs" Inherits="PrintInvoicebeta1" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="center" style="width: 388px">
        <tr>
            <td align="center" style="width: 100px" valign="middle">
                <table style="width: 155px">
                    <tr>
                        <td style="width: 154px">
                            <asp:Label ID="Label1" runat="server" Text="Computer-Tel, Inc"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 154px">
                            <asp:Label ID="Label2" runat="server" Text="2050 Russett Way"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 154px">
                            <asp:Label ID="Label3" runat="server" Text="Carson City, NV 89703"></asp:Label></td>
                    </tr>
                </table>
            </td>
            <td valign="middle" width="70">
            </td>
            <td style="width: 120px" valign="middle">
                <table width="100%">
                    <tr>
                        <td style="width: 107px">
                            <asp:Label ID="Label4" runat="server" Text="Invoice Number:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblPrefix" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 107px">
                            <asp:Label ID="Label5" runat="server" Text="Customer:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblCustomer" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 107px">
                            <asp:Label ID="Label6" runat="server" Text="Address:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblAddress" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 107px">
                        </td>
                        <td style="width: 100px">
                            <asp:Label ID="lblCity" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 107px">
                            <asp:Label ID="Label7" runat="server" Text="Phone:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblPhone" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td rowspan="2" style="width: 127px">
                <table width="100%">
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="Label8" runat="server" Text="Invoice Summary" Width="110px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                            <asp:Label ID="Label9" runat="server" Text="Bill Date:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblBillDate" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                            <asp:Label ID="Label10" runat="server" Text="Billing Period:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblBillinPeriod" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                            <asp:Label ID="Label11" runat="server" Text="Amound Due:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblAmoundDue" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                            <asp:Label ID="Label12" runat="server" Text="Due Date:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblDueDate" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="Label13" runat="server" Text="Bank Wiring Instructions"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label14" runat="server" Text="Computer-Tel, Inc."></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label15" runat="server" Text="The Laredo National Bank"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label16" runat="server" Text="P.O. Box 59"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label17" runat="server" Text="700 San Bernardo"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label18" runat="server" Text="Laredo, TX, 78042"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label19" runat="server" Text="ABA: 1 1 4 9 0 0 3 1 3"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label20" runat="server" Text="SWIFT: L B L T U S 4 L"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label21" runat="server" Text="Account #: 0 6 6 5 2 5 6"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label22" runat="server" Text="Account Status"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                            <asp:Label ID="Label23" runat="server" Text="Previous Balance:" Width="113px"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblPreviousBalance" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                            <asp:Label ID="Label25" runat="server" Text="Payments:" Width="113px"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="Label32" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                            <asp:Label ID="Label28" runat="server" Text="Adjustments:" Width="113px"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblAdjustments" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                            <asp:Label ID="Label27" runat="server" Text="Past Due Balance:" Width="113px"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblPastDueBalance" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                            <asp:Label ID="Label26" runat="server" Text="Late Fees:" Width="113px"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblLateFees" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                            <asp:Label ID="Label29" runat="server" Text="Current Charges:" Width="113px"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblCurrentCharges" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 122px">
                            <asp:Label ID="Label30" runat="server" Font-Bold="True" ForeColor="#000040" Text="Total Due:"
                                Width="113px"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="lblTotalDue" runat="server" Text="Label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 100px" valign="top">
                <asp:GridView ID="GridView1" runat="server" Width="100%" CellPadding="4" ForeColor="#333333" GridLines="None">
                <Columns>
                
                
                <asp:BoundField DataField="Date" HeaderText="Date" />
                <asp:BoundField DataField="Completed Calls" HeaderText="Completed Calls" />
                <asp:BoundField DataField="Billed Minutes" HeaderText="Billed Minutes" />
                <asp:BoundField DataField="Amount" HeaderText="Amount" />
                
                
                
                
                </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <EditRowStyle BackColor="#999999" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="SteelBlue" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" valign="top">
                <asp:GridView ID="GridView2" runat="server" Width="100%" CellPadding="4" ForeColor="#333333" GridLines="None">
                <Columns>
                
                <asp:BoundField HeaderText="Completed Calls" />
                <asp:BoundField HeaderText="Billed Minutes" />
                <asp:BoundField HeaderText="Price" />
                <asp:BoundField HeaderText="Amount" />
                
                
                
                </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <EditRowStyle BackColor="#999999" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="SteelBlue" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td rowspan="1" style="width: 127px">
            </td>
            <td style="width: 100px" valign="top">
                <asp:GridView ID="GridView3" runat="server" Width="100%" CellPadding="4" ForeColor="#333333" GridLines="None">
                <Columns>
                
                <asp:BoundField HeaderText="Total Calls" />
                <asp:BoundField HeaderText="Total Minutes" />
                <asp:BoundField HeaderText="Price" />
                <asp:BoundField HeaderText="Total Amount" />
                
                
                </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <EditRowStyle BackColor="#999999" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="SteelBlue" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    &nbsp;<br />
</asp:Content>

