using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class AssignBroker : System.Web.UI.Page
{
    Label lblnxtid;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DirectoUser"].ToString() == "rodrigo.andrade" || Session["DirectoUser"].ToString() == "amarquez")
        {

            CustBindData();
        }
        else
        {
            Response.Redirect("ContactUs.aspx", false);
        }
    }

    private void CustBindData()
    {
        SqlCommand sqlquery = new SqlCommand();
        sqlquery.CommandType = CommandType.Text;

        sqlquery.CommandText = "SELECT ID, Name, Gateway, VPort, IP, Prefix, CallingPlan, NextoneID FROM [CDRDB].[dbo].[CustVendCatalog] where Type = 'C' ";

        DataSet resultSet;
        resultSet = Util.RunQueryatCDRDB(sqlquery);

        gvCust.DataSource = resultSet;

        gvCust.DataBind();
    }

    private void VendBindData()
    {
        SqlCommand sqlquery = new SqlCommand();
        sqlquery.CommandType = CommandType.Text;

        sqlquery.CommandText = "SELECT ID, Name, Gateway, VPort, IP, Prefix, CallingPlan, NextoneID FROM [CDRDB].[dbo].[CustVendCatalog] where Type = 'V' ";

        DataSet resultSet;
        resultSet = Util.RunQueryatCDRDB(sqlquery);

        gvVend.DataSource = resultSet;

        gvVend.DataBind();
    }
  
    protected void btnShowC_Click(object sender, EventArgs e)
    {
        gvCust.Visible = true ;
        //sqldsCustAs.SelectCommand = "SELECT ID, Name, Gateway, VPort, IP, Prefix, CallingPlan, NextoneID FROM [CDRDB].[dbo].[CustVendCatalog] where Type = 'C' ";
    }
    protected void btnShowV_Click(object sender, EventArgs e)
    {
        gvVend.Visible = true;
        //sqldsVenAs.SelectCommand = "SELECT ID, Name, Gateway, VPort, IP, Prefix, CallingPlan, NextoneID FROM [CDRDB].[dbo].[CustVendCatalog] where Type = 'V' ";
    }


    protected void gvCust_RowEditing(object sender, GridViewEditEventArgs e)
    {
        lblnxtid = (Label)gvCust.Rows[e.NewEditIndex].FindControl("lblNxtID");

        gvCust.EditIndex = e.NewEditIndex;
        CustBindData();

    }

    protected void gvCust_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataRowView dRowView = (DataRowView)e.Row.DataItem;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //check if is in edit mode
            if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            {

                ddledNIDBind(e);

            }
        }
    }

    private void ddledNIDBind(GridViewRowEventArgs e)
    {
        DropDownList ddledNID = (DropDownList)e.Row.FindControl("ddledNID");
        string user = e.Row.Cells[1].Text.ToString();

        DataRowView dr = e.Row.DataItem as DataRowView;

        ddledNID.DataBind();

        ddledNID.SelectedValue = dr.Row[1].ToString();

    }   

    protected void gvCust_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        //switch back to default mode
        gvCust.EditIndex = -1;
        //Bind the grid

        gvCust.DataBind();
    }

    protected void gvCust_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        TextBox txtname = gvCust.Rows[e.RowIndex].FindControl("txtGVName") as TextBox;
        TextBox txtgateway = gvCust.Rows[e.RowIndex].FindControl("txtGVGateway") as TextBox;
        TextBox txtvport = gvCust.Rows[e.RowIndex].FindControl("txtGVVPort") as TextBox;
        TextBox txtip = gvCust.Rows[e.RowIndex].FindControl("txtGVIP") as TextBox;
        TextBox txtprefix = gvCust.Rows[e.RowIndex].FindControl("txtGVPrefix") as TextBox;
        TextBox txtcallingplan = gvCust.Rows[e.RowIndex].FindControl("txtGVCallingPlan") as TextBox;
        DropDownList ddledNID = gvCust.Rows[e.RowIndex].FindControl("ddledNID") as DropDownList;

        string ID = gvCust.Rows[e.RowIndex].Cells[1].Text.ToString();
        string name = txtname.Text.ToString();
        string gateway = txtgateway.Text.ToString();
        string vport = txtvport.Text.ToString();
        string ip = txtip.Text.ToString();
        string prefix = txtprefix.Text.ToString();
        string callingplan = txtcallingplan.Text.ToString();
        string nextid = ddledNID.SelectedValue.ToString();


        SqlCommand sqlup = new SqlCommand();
        sqlup.CommandType = CommandType.Text;
        sqlup.CommandText = "Update [CDRDB].[DBO].[CustVendCatalog] set Name = '" + name + "' , gateway = '" + gateway + "' , vport = '" + vport + "' , ip = '" + ip + "' , prefix = '" + prefix + "' , callingplan= '" + callingplan + "' , nextoneid= '" + nextid + "' where ID ='" + ID + "' ";

            Util.RunQuery(sqlup);

        //MySqlDataReader readerapi = RunQueryNS(sqlapi);

        gvCust.EditIndex = -1;
        
        gvCust.DataBind();
    }

    protected void btnAcep_Click(object sender, EventArgs e)
    {
        SqlCommand sqlins = new SqlCommand();
        sqlins.CommandType = CommandType.Text;
        sqlins.CommandText = "INSERT INTO [CDRDB].[DBO].[CustVendCatalog] VALUES ('" + txtID.Text.ToString() + "' , '" + txtName.Text.ToString() + "','" + ddlType.SelectedValue.ToString() +"','"+ txtGateway.Text.ToString() +"','"+ txtVport.Text.ToString() +"','"+ txtIP.Text.ToString() +"','"+ txtPrefix.Text.ToString() +"','" + txtCP.Text.ToString() +"','"+ ddlNextoneID.SelectedValue.ToString() +"')";

        Util.RunQuery(sqlins);

        Response.Redirect("CustVendCatalog.aspx", false);
    }
}
