<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="Live.aspx.cs" Inherits="Live" Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label Font-Bold="true" ID="lblHeader" runat="server" Text="MECCA Live Calls Traffic Watch"
        CssClass="labelBlue8" Width="200px"></asp:Label><br />
    <br />
    <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="(Calls finished in the last 10 minutes)"
        Width="250px"></asp:Label>
    <table align="left" id="mainTable">
        <tr>
            <td style="width: 30px; height: 10px;" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 106px" valign="top">
                <%--<table>
                                <tr>
                                    <td style="width: 100px" valign="top">
                                        <asp:Label ID="lblLive" runat="server" Text="Label" Font-Names="Arial" Font-Size="7pt"></asp:Label></td>
                                    <td style="width: 100px" valign="top">
                                        <asp:Label ID="lblQtmon" runat="server" Text="Label" Font-Size="8pt"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                </tr>
                            </table>--%>
            </td>
            <td style="width: 100px" valign="top">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblUpdate" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Notes" runat="server" CssClass="labelBlue8" Text="Color Blue - SYSTEM USE"
                                            Font-Bold="True" Width="162px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <%--<td style="width: 100px" valign="top" align = "center">
                                                    <table border="1">
                                                        <tr>
                                                            <td style="width: 100px" valign="top">
                                                                <asp:Label ID="labelstatusSTC03A" runat="server" Text="STC03A"></asp:Label>
                                                            </td> 
                                                            <td style="width: 100px" valign="top">
                                                                <asp:Label ID="labelstatusSTC03B" runat="server" Text="STC03B"></asp:Label>
                                                            </td>                                                        
                                                        </tr>                                                        
                                                    </table>
                                                </td>--%>
                                    <td style="width: 100px" valign="top" align="center">
                                        <table border="1">
                                            <tr>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="labelstatusSTC01A" runat="server" Text="STC01"></asp:Label>
                                                </td>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="labelstatusSTC01B" runat="server" Text="STC02"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px" valign="top" align="center">
                                        <table border="1">
                                            <tr>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="lblstatusMIA01A" runat="server" Text="MIA06A"></asp:Label>
                                                </td>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="lblstatusMIA01B" runat="server" Text="MIA06B"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px" valign="top" align="center">
                                        <table border="1">
                                            <tr>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="labelstatmia02A" runat="server" Text="MIA02A"></asp:Label>
                                                </td>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="labelstatmia02B" runat="server" Text="MIA02B"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px" valign="top" align="center">
                                        <table border="1">
                                            <tr>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="labelstatmia08A" runat="server" Text="MIA08A"></asp:Label>
                                                </td>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="labelstatmia08B" runat="server" Text="MIA08B"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px" valign="top" align="center">
                                        <asp:Label ID="lblicsdid" runat="server" Text="ICS - DID"></asp:Label>
                                    </td>
                                    <td style="width: 100px" valign="top" align="center">
                                        <asp:Label ID="lblidny05" runat="server" Text="MIA03"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <%--<td style="width: 100px">                                                    
                                                </td>--%>
                                    <td style="width: 100px; text-align: center;" valign="top">
                                        <%--<table border="1">                                                        
                                                        <tr>
                                                            <td style="width: 100px" valign="top">
                                                                <asp:Label ID="lblidstc02a" runat="server" Text="STC02A"></asp:Label>
                                                            </td> 
                                                            <td style="width: 100px" valign="top">
                                                                <asp:Label ID="lblidstc02b" runat="server" Text="STC02B"></asp:Label>
                                                            </td>                                                       
                                                        </tr>
                                                    </table>--%>
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                    <td style="width: 100px" align="center">
                                        <table border="1">
                                            <tr>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="labelstatmia03A" runat="server" Text="MIA03A"></asp:Label>
                                                </td>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="labelstatmia03B" runat="server" Text="MIA03B"></asp:Label>
                                                </td>
                                               
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px">
                                                                            <table border="1">
                                            <tr>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="labelstatmexA" runat="server" Text="MEX-A"></asp:Label>
                                                </td>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="labelstatmexB" runat="server" Text="MEX-B"></asp:Label>
                                                </td>
                                               
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px" align="center">
                                        <asp:Label ID="lbldidprovider" runat="server" Text="PROVIDER" Font-Size="Small"></asp:Label>
                                    </td>
                                    <td style="width: 100px" align="center">
                                        <asp:Label ID="labelNY05" runat="server" Text="INBOUND" Font-Size="Small"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <%--<td>
                                                </td>--%>
                                    <td>
                                        <table border="1">
                                            <tr>
                                                <td style="width: 100px" valign="top" align="center">
                                                    <asp:Label ID="lblrtsrvstc01" runat="server" Text="RTSRV STC01"></asp:Label>
                                                </td>
                                                <td style="width: 100px" valign="top" align="center">
                                                    <asp:Label ID="lblrtsrvstc02" runat="server" Text="RTSRV STC02"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <table border="1">
                                            <tr>
                                                <td style="width: 100px" valign="top" align="center">
                                                    <asp:Label ID="lblrtsrvmia01" runat="server" Text="RTSRV MIA01"></asp:Label>
                                                </td>
                                                <td style="width: 100px" valign="top" align="center">
                                                    <asp:Label ID="lblrtsrvmia02" runat="server" Text="RTSRV MIA02"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <%--<td style="width: 100px" valign="top" align = "center">
                                                <table>
                                                    <tr>
                                                        <td style="width: 3px">
                                                             <asp:GridView ID="gdvSTC03cust" runat="server" Height="33px"
                                                                Font-Names="Arial" Font-Size="8pt" Width="100px" AutoGenerateColumns="true">
                                                                <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                                <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                                <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                                    Font-Size="8pt" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 3px">
                                                             <asp:GridView ID="gdvny03vend" runat="server" Height="33px" 
                                                                Font-Names="Arial" Font-Size="8pt" Width="100px" AutoGenerateColumns="true">
                                                                <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                                <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                                <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                                    Font-Size="8pt" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>                                                                                                       
                                                </td>--%>
                                    <td style="width: 100px" valign="top" align="center">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gdvny06cust" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gdvny06vend" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px" valign="top" align="center">
                                        <table>
                                            <tr>
                                                <td style="width: 3px">
                                                    <asp:GridView ID="gdvMIA01cust" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 3px">
                                                    <asp:GridView ID="gdvMIA01vend" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px" valign="top" align="center">
                                        <table>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gdvmia02cust" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gdvmia02vend" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px" valign="top" align="center">
                                        <table>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gdvmia08cust" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gdvmia08vend" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px" valign="top" align="center">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gdvdidIn" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gdvdidOut" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px" valign="top" align="center">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gdvny05cust" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gdvny05vend" runat="server" Height="33px" Font-Names="Arial" Font-Size="8pt"
                                                        Width="100px" AutoGenerateColumns="true">
                                                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                                        <PagerStyle BackColor="#004A8F" ForeColor="White" HorizontalAlign="Center" />
                                                        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                                            Font-Size="8pt" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%-- <tr>
                                    <td style="width: 100px">
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                </tr>--%>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 106px; height: 44px;" valign="top">
                <asp:Timer ID="Timer1" runat="server" Interval="90000">
                </asp:Timer>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td style="width: 100px; height: 44px;" valign="top">
            </td>
        </tr>
    </table>
    <table visible="false" width="1200" style="position: absolute; top: 5850px; left: 230px;">
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <%--<tr>
            <td width="100%">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label3" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>--%>
    </table>
</asp:Content>
