using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using RKLib.ExportData;


public partial class DIDGuide : System.Web.UI.Page
{
    private const string ASCENDING = " ASC";
    private const string DESCENDING = " DESC";

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Page.IsPostBack)
        //{
        //    gvResult.DataBind();
        //}
    }

    protected void gvResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //DataSet myDtst = (DataSet)Session["spc"];
        gvResult.PageIndex = e.NewPageIndex;
        //GridView1.DataSource = myDtst.Tables[0];
        gvResult.DataBind();
    }

    private string ConvertSortDirectionToSql(SortDirection sortDirection)
    {
        string newSortDirection = String.Empty;

        switch (sortDirection)
        {
            case SortDirection.Ascending:
                newSortDirection = "ASC";
                break;

            case SortDirection.Descending:
                newSortDirection = "DESC";
                break;
        }

        return newSortDirection;
    }

    protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = gvResult.DataSource as DataTable;

        if (dataTable != null)
        {
            try
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

                gvResult.DataSource = dataView;
                gvResult.DataBind();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/MDevices.aspx");
            }
        }
    }

    protected void cmdExport(object sender, EventArgs e)
    {
        string sql = "";

        try
        {
            sql = "SELECT DID, COUNTRY, CITY, STATEPREFIX, STATE, PROVIDERNAME, FRAMEWORK FROM ICS.DBO.SOURCEDID ";

            DataSet ds = Util.RunQueryByStmnt(sql);
            DataTable dtEmployee = ds.Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "DIDList" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

    }
   
}
