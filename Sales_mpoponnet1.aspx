<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Sales_mpoponnet1.aspx.cs" Inherits="Sales_mpoponnet1" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="Label1" runat="server" CssClass="labelTurkH" Text="ON NET MINIPOP RESULTS"></asp:Label>
    <table align="left" width="100%" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td align="right" style="width: 100px">
                <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="Button1_Click"
                    Text="Export to EXCEL" Width="94px" /></td>
        </tr>
        <tr>
            <td style="width: 100px; height: 39px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
    <asp:GridView ID="GridView1" AutoGenerateColumns="false" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Font-Names="Arial" Font-Size="9pt" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="100">
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#999999" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt"/>
        <AlternatingRowStyle BackColor="White" />
        
        <Columns>
        <asp:BoundField DataField="BillingDate" HeaderText="BillingDate" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="Customer" HeaderText="Customer" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="Vendor" HeaderText="Vendor" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="150px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="Region" HeaderText="Region" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="130px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="RegionPrefix" HeaderText="RegionPrefix" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="Calls" HeaderText="Calls"  HtmlEncode="False"  DataFormatString="{0:0,0}" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="CMinutes" HeaderText="Cminutes"  HtmlEncode="False" DataFormatString="{0:0,0.00}" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="cost" HeaderText="Cost"  >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="price" HeaderText="Price"  HtmlEncode="false" DataFormatString="{0:C}" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="TotalCost" HeaderText="TotalCost"  HtmlEncode="false" DataFormatString="{0:C}" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="150px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="Totalsales" HeaderText="TotalSales" HtmlEncode="false" DataFormatString="{0:C}" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="150px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="Profit" HeaderText="Profit" HtmlEncode="false" DataFormatString="{0:C}" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="130px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        
        
        
        </Columns>
        
        
        
    </asp:GridView>
                <asp:Label ID="lblSales" runat="server" ForeColor="Red" Text="Nothing found" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td height="30" style="width: 100px">
            </td>
        </tr>
        
          <tr>
            <td width="100%">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>

