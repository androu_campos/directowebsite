using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class WeekProfit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    GridView1.Visible = false;
        //}
    }

    protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlCVendor_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        String inidate = string.Empty;
        String enddate = string.Empty;
        String customer = string.Empty;
        String vendor = string.Empty;
        String type = string.Empty;
        SqlCommand sQuery = new SqlCommand();
        DataSet ds;
        SqlDataAdapter da;
        

        inidate = txtfrom.Text;
        enddate = txtTo.Text;

        customer = ddlCustomer.SelectedValue;
        vendor = ddlVendor.SelectedValue;

        sQuery.CommandTimeout = 350;
        sQuery.Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["MECCA2ConnectionString"].ConnectionString);

        if (rboption.SelectedValue == "Profit")
        {
            type = "P";
        }
        else if (rboption.SelectedValue == "Minutes")
        {
            type = "M";
        }
        else if (rboption.SelectedValue == "Sales")
        {
            type = "S";
        }
        else if (rboption.SelectedValue == "Cost")
        {
            type = "C";
        }

        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@startdate", SqlDbType.SmallDateTime);
        param[0].Value = inidate;
        param[1] = new SqlParameter("@enddate", SqlDbType.SmallDateTime);
        param[1].Value = enddate;
        param[2] = new SqlParameter("@customer",SqlDbType.VarChar);
        param[2].Value = customer;
        param[3] = new SqlParameter("@vendor", SqlDbType.VarChar);
        param[3].Value = vendor;
        param[4] = new SqlParameter("@type", SqlDbType.VarChar);
        param[4].Value = type;

        sQuery.CommandType = CommandType.StoredProcedure;
        sQuery.CommandText = "sp_WeekProfitReport";
        sQuery.Parameters.AddRange(param);


        try
        {
            ds = new DataSet();
            da = new SqlDataAdapter(sQuery);
            da.Fill(ds);
            GridView1.Visible = true;

            
            GridView1.DataSource = ds.Tables[0];

            this.GridView1.Columns.Clear();
            for (int c = 0; c < ds.Tables[0].Columns.Count; c++)
            {
                string columnName = ds.Tables[0].Columns[c].ColumnName.ToString();
                BoundField columna = new BoundField();
                columna.HtmlEncode = false;
                if (columnName == "MIN_PROFIT" || columnName == "MAX_PROFIT" || columnName == "AVG_PROFIT"
                    || columnName == "MIN_SALES" || columnName == "MAX_SALES" || columnName == "AVG_SALES"
                    || columnName == "MIN_COST" || columnName == "MAX_COST" || columnName == "AVG_COST")
                {
                    columna.DataFormatString = "${0:f2}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }

                if (columnName.Substring(0, 2) == "WE" && rboption.SelectedValue != "Minutes")
                {
                    columna.DataFormatString = "${0:f2}";
                    columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    columna.ItemStyle.Width = 75;
                    columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                }

                columna.DataField = columnName;
                columna.HeaderText = columnName;

                GridView1.Columns.Add(columna);
                GridView1.DataBind();
            }
            Session["weekProfit"] = ds;
        }
                
            
        
        catch (Exception ex)
        {
            
        }
              
       
    }


    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.DataSource = ((DataSet)Session["weekProfit"]).Tables[0].Copy();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }

    protected void btnExp_Click(object sender, EventArgs e)
    {
        try
        {

            DataTable dtEmployee = ((DataSet)Session["weekProfit"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "weekProfit" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }

}
