<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="IP.aspx.cs" Inherits="IP" Title="DIRECTO - Connections Worldwide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    &nbsp; &nbsp;&nbsp;
    <table>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label1" runat="server" Text="Provider:" CssClass="labelBlue8"></asp:Label>
            </td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdProvider" runat="server" Width="128px" CssClass="dropdown">
                </asp:DropDownList>
            </td>
            <td style="width: 100px" align="right">
                <asp:Button ID="Button1" runat="server" Text="Show IPs" OnClick="Button1_Click" CssClass="boton" />
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td align="center" colspan="2" rowspan="6">
                <asp:GridView ID="gdvProviders" runat="server" CellPadding="4" ForeColor="#333333"
                    AllowPaging="true" PageSize="50" GridLines="None" AutoGenerateColumns="False"
                    DataKeyNames="IP" DataSourceID="SqlDataSource2" Font-Names="Arial" Font-Size="8pt">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="ProviderName" HeaderText="ProviderName" SortExpression="ProviderName">
                            <ItemStyle HorizontalAlign="Left" Wrap="false" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IP" HeaderText="IP" ReadOnly="True" SortExpression="IP"> 
                        <ItemStyle Wrap="false" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
                        <asp:BoundField DataField="AdjacencyName" HeaderText="AdjacencyName" SortExpression="AdjacencyName" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    SelectCommand="SELECT ProviderName, IP, ip.Type, AdjacencyName FROM CDRDB.dbo.ProviderIP ip LEFT JOIN CDRDB.dbo.ProviderIPMeta ipm ON ip.IP = ipm.MeccaID WHERE ([ProviderName] = @ProviderName) ORDER BY ip.Type">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="dpdProvider" DefaultValue="All Providers" Name="ProviderName"
                            PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br />
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    SelectCommand="SELECT ProviderName, IP, ip.Type, AdjacencyName FROM CDRDB.dbo.ProviderIP ip LEFT JOIN CDRDB.dbo.ProviderIPMeta ipm ON ip.IP = ipm.MeccaID ORDER BY ip.Type"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
</asp:Content>
