<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Prepareinvoice.aspx.cs" Inherits="Prepareinvoice" Title="Untitled Page" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="Printable Invoice Maker" Width="141px"></asp:Label></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Customer Name"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCust" runat="server" AutoPostBack="True" CssClass="dropdown"
                    DataSourceID="SqlCustName" DataTextField="MeccaName" DataValueField="MeccaName"
                    OnSelectedIndexChanged="dpdCust_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td bgcolor="#d5e3f0" style="width: 100px">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Invoice Number"
                    Width="78px"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtInvoiceNumber" runat="server" CssClass="dropdown"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="To"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtTo" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" align="center" bgcolor="#d5e3f0">
                <asp:Button ID="cmdInvoice" runat="server" CssClass="boton" OnClick="cmdInvoice_Click"
                    Text="Create Invoice" /></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:SqlDataSource ID="SqlCustName" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    SelectCommand="SELECT DISTINCT MeccaName FROM Invoice"></asp:SqlDataSource>
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
</asp:Content>

