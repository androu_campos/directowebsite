using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;


public partial class PortsGraphCCCust : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        DataTable tbl;

        if (!Page.IsPostBack)
        {
            txtFrom.Text = DateTime.Now.ToShortDateString();
            txtTo.Text = DateTime.Now.ToShortDateString();

            string username = string.Empty;

            username = Session["DirectoUser"].ToString();

            if (username == "santander")
            {
                Response.Redirect("PortsGraphCCCustS.aspx");
            }

            else if (username == "bento" || username == "contacto" || username == "eci" || username == "ergon"
                || username == "megacal" || username == "mkt911" || username == "sto" || username == "targetone"
                || username == "vcip" || username == "santanadmoncall" || username == "santanderdomer"
                || username == "domercent" || username =="cc-med")
            {
                SqlConnection SqlConn1 = new SqlConnection();
                SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
                SqlCommand SqlCommand1 = new SqlCommand();

                if (username == "vcip")
                {
                    SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE (PROVIDERNAME LIKE 'CC-" + username + "SAN%' or PROVIDERNAME LIKE 'CC-" + username + "-SAN%') AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
                }
                else if (username == "santanadmoncall" || username == "santanderdomer" || username == "domercent")
                {
                    SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-" + username + "%' AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
                }
                else if (username == "cc-med")
                {
                    SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-MED-%' AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
                }
                else
                {
                    SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-" + username + "SAN%' AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
                }

                SqlCommand1.Connection = SqlConn1;
                SqlConn1.Open();
                SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
                while (myReader1.Read())
                {
                    dpdProvider.Items.Add(myReader1.GetValue(0).ToString());
                }
                myReader1.Close();
                SqlConn1.Close();

                if (Request.QueryString.Count > 0)
                {
                    string provider = Request.QueryString["P"].ToString();
                    string query = string.Empty;
                    string title = string.Empty;

                    string from = Request.QueryString["dF"].ToString();
                    string to = Request.QueryString["dT"].ToString();
                    from = from.Replace("_", "/");
                    to = to.Replace("_", "/");


                    if (Request.QueryString["T"].ToString() == "0") //Customer
                    {
                        if (provider == "**ALL**")
                        {
                            if (username == "vcip")
                            {
                                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where (ProviderName like 'CC-" + username + "SAN%' or Providername like 'CC-" + username + "-SAN%') and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000'  group by [Time] order by [Time]";
                            }
                            else if (username == "santanadmoncall" || username == "santanderdomer" || username == "domercent")
                            {
                                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like 'CC-" + username + "%'  and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000'  group by [Time] order by [Time]";
                            }
                            else if (username == "cc-med")
                            {
                                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like 'CC-MED-%'  and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000'  group by [Time] order by [Time]";
                            }
                            else 
                            {
                                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like 'CC-" + username + "SAN%'  and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000'  group by [Time] order by [Time]";
                            }
                            title = "Customer: " + username.ToUpper();
                        }
                        else
                        {
                            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000'  group by [Time] order by [Time]";
                            title = "Customer: " + provider;
                        }

                    }

                    tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;

                    makeChart(tbl, title);
                    //gdvDetail.Visible = false;
                    lblGrid.Visible = false;

                    txtFrom.Text = from;
                    txtTo.Text = to;
                    dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
                    dpdType.SelectedIndex = Convert.ToInt32(Request.QueryString["T"]);
                    cmdDetail.Visible = true;

                }
            }

            else
            {
                SqlConnection SqlConn1 = new SqlConnection();
                SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
                SqlCommand SqlCommand1 = new SqlCommand("SELECT '" + username.ToUpper() + "' as ProviderName", SqlConn1);
                SqlConn1.Open();
                SqlDataReader myReader1 = SqlCommand1.ExecuteReader();
                while (myReader1.Read())
                {
                    dpdProvider.Items.Add(myReader1.GetValue(0).ToString());
                }
                myReader1.Close();
                SqlConn1.Close();

                if (Request.QueryString.Count > 0)
                {
                    string provider = Request.QueryString["P"].ToString();
                    string query = string.Empty;
                    string title = string.Empty;

                    string from = Request.QueryString["dF"].ToString();
                    string to = Request.QueryString["dT"].ToString();
                    from = from.Replace("_", "/");
                    to = to.Replace("_", "/");

                    if (provider == "**ALL**") provider = "%";




                    if (Request.QueryString["T"].ToString() == "0") //Customer
                    {

                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' and ID IN('NY03','NY07','MIA02','STC01','STC03','MIA01','MIA08','MIA06','PERIMETAMI','PERIMETAMX') group by [Time] order by [Time]";
                        title = "Customer: " + provider;


                    }

                    tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;

                    makeChart(tbl, title);
                    //gdvDetail.Visible = false;
                    lblGrid.Visible = false;

                    txtFrom.Text = from;
                    txtTo.Text = to;
                    dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
                    dpdType.SelectedIndex = Convert.ToInt32(Request.QueryString["T"]);
                    cmdDetail.Visible = true;

                }
            }
        }
        else
        {


            if (Request.QueryString.Count > 0)
            {
                string provider = Request.QueryString["P"].ToString();
                string query = string.Empty;
                string title = string.Empty;
                string from = txtFrom.Text;//Request.QueryString["dF"].ToString();
                string to = txtTo.Text;//Request.QueryString["dT"].ToString();
                from = from.Replace("_", "/");
                to = to.Replace("_", "/");


                if (Request.QueryString["T"].ToString() == "0") //Customer
                {

                    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' and ID IN('NY03','NY07','MIA02','STC01','STC03','MIA01','MIA08','PERIMETAMI','PERIMETAMX') group by [Time] order by [Time]";




                    title = "Customer: " + provider;


                }

                tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                else cmdDetail.Visible = true;

                makeChart(tbl, title);
                //gdvDetail.Visible = false;
                lblGrid.Visible = false;

                txtFrom.Text = from;
                txtTo.Text = to;

                //dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
            }

        }

    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {

        string from = txtFrom.Text;
        string to = txtTo.Text;

        from = from.Replace("/", "_");
        to = to.Replace("/", "_");

        if (dpdType.SelectedIndex == 0)//Customer
        {
            if (dpdProvider.SelectedValue.ToString() == "A&D-WORLDWIDE")
            {
                string c = dpdProvider.SelectedValue.ToString();
                c = c.Replace("&", "_");
                Response.Redirect("PortsGraphCCCust.aspx?P=" + c + "&T=0&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
            }
            else
            {
                Response.Redirect("PortsGraphCCCust.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=0&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
            }
        }
        else                             //Vendor
            Response.Redirect("PortsGraphCCCust.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=1&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);


    }

    private void makeChart(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart1.Titles.Add(tITLE);
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetail"] = tbl;

            this.Chart1.Visible = true;
            lblWarning.Visible = false;
            this.cmdDetail.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            lblWarning.Visible = true;
            Session["tblDetail"] = tbl;
            this.cmdDetail.Visible = false;

        }

    }
    protected void cmdDetail_Click(object sender, EventArgs e)
    {
        if (((DataTable)Session["tblDetail"]).Rows.Count > 0)
        {

            gdvDetail.DataSource = ((DataTable)Session["tblDetail"]);
            gdvDetail.DataBind();

            gdvDetail.Visible = true;
            lblGrid.Visible = true;
            lblGrid.Text = dpdProvider.SelectedValue.ToString();
            cmdHide.Visible = true;
            cmdDetail.Visible = false;
        }
        else
        {
        }
    }
    protected void cmdHide_Click(object sender, EventArgs e)
    {
        gdvDetail.Visible = false;
        lblGrid.Visible = false;
        cmdDetail.Visible = true;
        cmdHide.Visible = false;

    }
}
