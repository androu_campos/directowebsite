<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestNewGraphs.aspx.cs" Inherits="TestNewGraphs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <DCWC:Chart ID="Chart1" runat="server" Height="449px" Width="627px">
            <Series>
                <DCWC:Series Name="Series1" Color="255, 255, 165, 0" ShadowOffset="1">
                </DCWC:Series>
                <DCWC:Series Name="Series2" Color="165, 255, 69, 0" ShadowOffset="1">
                </DCWC:Series>
                <DCWC:Series Name="Series3" Color="200, 255, 255, 0" ShadowOffset="1">
                </DCWC:Series>
                <DCWC:Series Name="Series4" Color="255, 139, 0, 0" ShadowOffset="1">
                </DCWC:Series>                
            </Series>
            <ChartAreas>
                <DCWC:ChartArea Name="Default">
                </DCWC:ChartArea>
            </ChartAreas>
        </DCWC:Chart>
    </div>
    </form>
</body>
</html>
