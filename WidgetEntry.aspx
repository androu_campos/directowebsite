<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="WidgetEntry.aspx.cs" Inherits="WidgetEntry" Title="Untitled Page" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px">
    <table>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label11" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="INSERT NEW ROW AT WIDGET PUSH LIST"
                    Width="214px"></asp:Label></td>
            <td colspan="1">
            </td>
        </tr>
        <tr>
            <td colspan="2" height="25">
            </td>
            <td colspan="1" height="25">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Country"></asp:Label></td>
            <td style="width: 25px">
                <asp:TextBox ID="txtCountry" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Region"></asp:Label></td>
            <td style="width: 25px">
                <asp:TextBox ID="txtRegion" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Average Cost Per Minute"></asp:Label></td>
            <td style="width: 25px">
                <asp:TextBox ID="txtAvg" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Expected ASR ABR"></asp:Label></td>
            <td style="width: 25px">
                <asp:TextBox ID="txtASR" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Yesterday ACD"></asp:Label></td>
            <td style="width: 25px">
                <asp:TextBox ID="txtACD" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Addt'l Avail. Capacity/Day"></asp:Label></td>
            <td style="width: 25px">
                <asp:TextBox ID="txtAvailC" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
                <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Service Level"></asp:Label></td>
            <td style="width: 25px">
                <asp:DropDownList ID="dpdServiceL" runat="server" CssClass="dropdown" Width="165px">
                    <asp:ListItem>WholeSale</asp:ListItem>
                    <asp:ListItem>Retail</asp:ListItem>
                    <asp:ListItem>Premium CLI</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
                <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Text="Comments"></asp:Label></td>
            <td style="width: 25px">
                <asp:TextBox ID="txtComments" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
                <asp:Label ID="Label9" runat="server" CssClass="labelBlue8" Text="Vendor Billing Increments"></asp:Label></td>
            <td style="width: 25px">
                <asp:DropDownList ID="DropDownList2" runat="server" CssClass="dropdown" Width="165px">
                    <asp:ListItem>1/1</asp:ListItem>
                    <asp:ListItem>60/60</asp:ListItem>
                    <asp:ListItem>30/6</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
                <asp:Label ID="Label10" runat="server" CssClass="labelBlue8" Text="Minimum Sell Rate"></asp:Label></td>
            <td style="width: 25px" nowrap="noWrap">
                <asp:TextBox ID="txtMinimum" runat="server" CssClass="dropdown"></asp:TextBox></td>
            <td nowrap="nowrap" style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
            </td>
            <td style="width: 25px">
                <asp:Button ID="cmdInsert" runat="server" Text="Update Widget" CssClass="boton" OnClick="cmdInsert_Click" /></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager>&nbsp;
            </td>
            <td style="width: 25px">
                <asp:Label ID="lblUpdated" runat="server" CssClass="labelBlue8" Font-Bold="True"
                    Font-Size="9pt" ForeColor="Red" Text="Widget Updated !!!" Visible="False"></asp:Label></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td style="width: 81px">
            </td>
            <td style="width: 25px">
            </td>
            <td style="width: 25px">
            </td>
        </tr>
    </table>
            </td>
            <td style="width: 100px" valign="top">
                <table>
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:GridView ID="gdvEntrys" runat="server" Visible="False">
                            </asp:GridView>
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>

