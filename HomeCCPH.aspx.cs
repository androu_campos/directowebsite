using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;


public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string username = Session["DirectoUser"].ToString();
        string bal = "";

        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "show", rol, true);

        //Response.Redirect("http://drpmecca.directo.com/MeccaDirecto");
        try
        {
            if (Session["Rol"].ToString() == "CallCenterPH" || Session["Rol"].ToString() == "NOC")
            {
                MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adp = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
                MyDataSetTableAdapters.Billing_StatusDataTable tbl = adp.GetData();

                DataView dvSQL = (DataView)sqlBal.Select(DataSourceSelectArguments.Empty);

                foreach (DataRowView drvSQL in dvSQL)
                    bal = drvSQL["Balance"].ToString();

                bal = string.Concat("$", bal.ToString(), " USD");

                lblBal.Text = bal;
               
                

                SummaryTableAdapters.QrysAdp adpL = new SummaryTableAdapters.QrysAdp();
                int lastId = Convert.ToInt32(adpL.LastIdSummaryCC());
                int lastId1 = 0, lastId2 = 0;
                /**/
                //TODAY
                SummaryTableAdapters.SummaryCCPHTableAdapter adpD = new SummaryTableAdapters.SummaryCCPHTableAdapter();
                Summary.SummaryCCPHDataTable tblD = adpD.GetDataByDate(Convert.ToDateTime(DateTime.Now.ToString()));//Today
                if (tblD.Rows.Count > 0)
                {
                    lastId1 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
                }
                else
                {
                    tblD = adpD.GetDataByDate(Convert.ToDateTime(DateTime.Now.AddDays(-1).ToShortDateString()));//Today
                    lastId1 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
                }
                /**/

                /**/
                //7 DAY BEFORE

                tblD = adpD.GetDataByDate(DateTime.Now.AddDays(-7));//Today
                if (tblD.Rows.Count > 0)
                {
                    lastId2 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
                }
                else
                {
                    tblD = adpD.GetDataByDate(Convert.ToDateTime(DateTime.Now.AddDays(-8).ToShortDateString()));//Today
                    lastId2 = Convert.ToInt32(tblD.Rows[0]["Id"].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            string message = ex.Message.ToString();
        }
    }

    protected void buttoncchome_click(object sender, EventArgs e)
    {
        Response.Redirect("HomeCCPH.aspx");
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            string messagerror = ex.Message.ToString();
        }

        return resultsDataSet;
    }
}


