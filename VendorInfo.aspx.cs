using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class VendorInfo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TextBox1.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            TextBox2.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            CheckBox1.Visible = false;

            if (Session["DirectoUser"].ToString() == "pldtretail" || Session["DirectoUser"].ToString() == "olbrasilnat" || Session["DirectoUser"].ToString() == "ALPI")
            {
                this.Label1.Text = "Show older traffic";
            }
        }


    }
    protected void cmdShow_Click(object sender, EventArgs e)
    {        
        string customer = Session["DirectoUser"].ToString();
        string vendor = Session["DirectoUser"].ToString();
        SqlCommand sqlQry = new SqlCommand();        
        sqlQry.CommandType = CommandType.Text;
        


        DateTime dt = Convert.ToDateTime(TextBox1.Text);
        DateTime dtaux = DateTime.Now.AddDays(-8);

        TimeSpan tm = dtaux-dt;

        if (tm.TotalDays >= 1 && vendor != "pldtretail" && vendor != "24-TRANSPORT" && vendor != "olbrasilnat" && vendor != "VETECSURMIN" && vendor != "COSMORED" && vendor != "ALPI" && vendor != "OUTLANDER-NEW-NET")
        {
            TextBox1.Text = dtaux.ToShortDateString();
        }

        if (vendor == "pldtretail")
        {
            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE,CUSTOMER, REGION, VENDORPREFIX AS PREFIX ,TYPE, sum(ISNULL(VENDORMINUTES,0)) MINUTES, sum(tCalls) ATTEMPTS From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR = 'PLDT-ALLACCESSPH'  group by BillingDate,Customer, Region, VENDORPREFIX, TYPE";
            
        }
        else if (vendor == "ALPI")
        {
            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as [DATE], sum(ISNULL(VENDORMINUTES,0)) MINUTES From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR IN ('OUTLANDER-ALPI-ICS','OUTLANDER-ALPI') group by BillingDate UNION SELECT '***ALL***' as [DATE], sum(ISNULL(VENDORMINUTES,0)) MINUTES From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR IN ('OUTLANDER-ALPI-ICS','OUTLANDER-ALPI')";
        }
        else if (vendor == "olbrasilnat")
        {
            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE,VENDOR,  sum(ISNULL(VENDORMINUTES,0)) MINUTES, mecca2.dbo.fGetASR(SUM(tcalls),SUM(billablecalls), SUM(RA)) as ASR, mecca2.dbo.fGetABR(SUM(tcalls),SUM(billablecalls)) as ABR,mecca2.dbo.fGetACD(SUM(vendorMinutes), SUM(billablecalls)) as ACD From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR = 'OL-BRASIL-NAT'  group by BillingDate,Vendor";
        }
        else if (vendor == "24-TRANSPORT")
        {
            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE,VENDOR,  sum(ISNULL(BillableCalls,0)) as ANSWEREDCALLS, sum(ISNULL(VENDORMINUTES,0)) MINUTES, mecca2.dbo.fGetASR(SUM(tcalls),SUM(billablecalls), SUM(RA)) as ASR, mecca2.dbo.fGetABR(SUM(tcalls),SUM(billablecalls)) as ABR,mecca2.dbo.fGetACD(SUM(vendorMinutes), SUM(billablecalls)) as ACD From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR = '24-TRANSPORT'  group by BillingDate,Vendor";
        }
        else if (vendor == "TELCELPOST")
        {
            //            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE, sum(ISNULL(BillableCalls,0)) as ANSWEREDCALLS, sum(ISNULL(VENDORMINUTES,0)) MINUTES, mecca2.dbo.fGetASR(SUM(tcalls),SUM(billablecalls), SUM(RA)) as ASR, mecca2.dbo.fGetABR(SUM(tcalls),SUM(billablecalls)) as ABR,mecca2.dbo.fGetACD(SUM(vendorMinutes), SUM(billablecalls)) as ACD From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR = 'OUTLANDER-POSPAY'  group by BillingDate";
            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE, LMC, sum(ISNULL(BillableCalls,0)) as ANSWEREDCALLS, sum(ISNULL(VENDORMINUTES,0)) MINUTES From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR = 'OUTLANDER-POSPAY' AND LMC is not null group by BillingDate,LMC";
        }
        else if (vendor == "MAS-COMUNICACION")
        {
            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE, sum(ISNULL(BillableCalls,0)) as ANSWEREDCALLS, sum(ISNULL(VENDORMINUTES,0)) MINUTES, mecca2.dbo.fGetASR(SUM(tcalls),SUM(billablecalls), SUM(RA)) as ASR, mecca2.dbo.fGetABR(SUM(tcalls),SUM(billablecalls)) as ABR,mecca2.dbo.fGetACD(SUM(vendorMinutes), SUM(billablecalls)) as ACD From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR = 'MAS-COMUNICACION'  group by BillingDate";
            //sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE, VENDOR, ROUND(SUM(ISNULL(VendorMinutes,0)),4) MINUTES From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR in ('VETECSUR','VETECSUR-ICS') AND LMC is not null group by BillingDate, Vendor";
        }
        else if (vendor == "OUTLANDER-NEW-NET")
        {
            //            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE, sum(ISNULL(BillableCalls,0)) as ANSWEREDCALLS, sum(ISNULL(VENDORMINUTES,0)) MINUTES, mecca2.dbo.fGetASR(SUM(tcalls),SUM(billablecalls), SUM(RA)) as ASR, mecca2.dbo.fGetABR(SUM(tcalls),SUM(billablecalls)) as ABR,mecca2.dbo.fGetACD(SUM(vendorMinutes), SUM(billablecalls)) as ACD From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR = 'OUTLANDER-POSPAY'  group by BillingDate";
            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE, ROUND(SUM(ISNULL(VendorMinutes,0)),4) MINUTES From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' AND VENDOR IN ('OUTLANDER-NEWNET-ICS', 'OUTLANDER-NEW-NET') AND LMC is not null group by BillingDate ORDER BY BillingDate";
        }
        else if (vendor == "COSMORED")
        {
            //            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE, sum(ISNULL(BillableCalls,0)) as ANSWEREDCALLS, sum(ISNULL(VENDORMINUTES,0)) MINUTES, mecca2.dbo.fGetASR(SUM(tcalls),SUM(billablecalls), SUM(RA)) as ASR, mecca2.dbo.fGetABR(SUM(tcalls),SUM(billablecalls)) as ABR,mecca2.dbo.fGetACD(SUM(vendorMinutes), SUM(billablecalls)) as ACD From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR = 'OUTLANDER-POSPAY'  group by BillingDate";
            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE, VENDOR, ROUND(SUM(ISNULL(VendorMinutes,0)),4) MINUTES From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR = 'COSMORED' AND LMC is not null group by BillingDate, Vendor";
        }
        else if (vendor == "CIAO-TELECOM")
        {
            sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE,VENDOR,COUNTRY,REGION, TYPE, sum(tcalls) as ATTEMPTS, sum(ISNULL(BillableCalls,0)) as ANSWEREDCALLS, sum(ISNULL(VENDORMINUTES,0)) MINUTES FROM mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and VENDOR IN ('CIAO-TELECOM-MOBILE','CIAO-TELECOM-FIXED')  group by BillingDate,Vendor,COUNTRY,REGION, TYPE";
        }
        else
            sqlQry.CommandText = "Select CONVERT(VARCHAR(11) ,Billingdate,101) as BILLINGDATE, Customer, Region, Type, Price as SellingRate ,sum(ISNULL(CustMinutes,0)) CMinutes, sum(tCalls) Attempts From mecca2.dbo.TrafficLog Where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and TYPE = 'TBD'and region in ('MANILA (PLDT)','ON NET (PAPTELCO - PLDT)','ON NET (PLDT)')  group by BillingDate,Customer, Region, Type, Price";
            
        //sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Origination' as Direction, sum(CustMinutes)as TotalMinutes,sum(BillableCalls) as CompletedCalls from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Customer = '" + customer + "' group by BillingDate having sum(CustMinutes) > 0 union select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate,'Termination' as Direction, Cost as Rate, sum(CustMinutes)as TotalMinutes,sum(BillableCalls) as CompletedCalls from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Vendor = '" + vendor + "' group by BillingDate   having sum(CustMinutes) > 0 order by Direction";
        // Price as Rate
        //group by Price,
        
        DataSet myDataset = RunQuery(sqlQry);
        Session["VendorInfo"] = myDataset;
        if (myDataset.Tables.Count > 0)
        {
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            cmdExport.Visible = true;
            Label4.Visible = false;

            //GridViewRow headerRow = GridView1.HeaderRow;

            //int index = 0;

            //for (int i = 0; i++; i < headerRow.Cells.Count)
            //{
            //    if (headerRow.Cells[i].Text == "MINUTES")
            //    {
            //        index = i;
            //        break;
            //    }
            //}

           
        }
        else
        {
            cmdExport.Visible = false;
            Label4.Visible = true;
        }
    }
    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void cmdExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {
         DataTable dtEmployee = ((DataSet)Session["VendorInfo"]).Tables[0].Copy();
         // Export all the details to CSV
         RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");                  
         string filename = "OlderTraffic" + ".xls";
         objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = Session["VendorInfo"];
        GridView1.DataBind();
    }
}
