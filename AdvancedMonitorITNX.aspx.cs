using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;


public partial class Default7 : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
        //Muestra los datos del Recent,Today y Yesterday segun los parametros de la URL enviados por Monitor.aspx
    {
        try
        {
            string customer = Request.QueryString[0];
            string vendor = Request.QueryString[1];
            string originationIP = Request.QueryString[2];
            string terminationIP = Request.QueryString[3];
            string country = Request.QueryString[4];
            string region = Request.QueryString[5];
            string type = Request.QueryString[6];
            string clas = Request.QueryString[7];
            string lmc = Request.QueryString[8];
            string source = Request.QueryString[9];
            string gateway = Request.QueryString[10];
            string ccdata = Request.QueryString[11];

            //
            //
            StringBuilder select = new StringBuilder();
            select.Append("SELECT ");

            StringBuilder where = new StringBuilder();
            where.Append("WHERE ");

            string whereR = "WHERE ";

            string whereT = "WHERE";

            StringBuilder group = new StringBuilder();
            group.Append("GROUP BY ");

            StringBuilder union = new StringBuilder();
            union.Append(" UNION SELECT ");

            StringBuilder qry = new StringBuilder();
            StringBuilder order = new StringBuilder();
            order.Append(" Order by ");
            int ord = 0;

            //CUSTOMER
            if (customer != "** NONE **")
            {
                if (customer == "ICS-ALL-CARDS")
                {
                    select = select.Append("'ICS-ALLCARDS' as Customer,");
                    where.Append("Customer in ('ICS-800-CARDS','ICS-WEB-DID','ICS-WEB-UNL','ICS-800-WEB','ICS-800-PARAMOUNT','ICS-PARAMOUNT','ICS-800-TSI','ICS','ICS-TSI','ICS-PHONETIME','ICS-800-TITAN') AND ");
                    union.Append("'**ALL**' as Customer,");

                }

                else if (customer == "ICS")
                {
                    select = select.Append("'ICS' as Customer,");
                    where.Append("Customer in (SELECT PROVIDERNAME FROM CDRDB..PROVIDERIP WHERE PROVIDERNAME LIKE 'ICS%') AND ");
                    union.Append("'**ALL**' as Customer,");
                }

                else if (customer == "TALKTEL")
                {
                    select = select.Append("'TALKTEL' as Customer,");
                    where.Append("Customer in (SELECT CUSTOMER FROM CDRDB.DBO.CUSTBROKERREL WHERE CUSTBROKER = 'CallCenters' AND CUSTOMER NOT LIKE 'CCPH%' AND CUSTOMER NOT LIKE 'CC-PHIP%' ) AND ");
                    union.Append("'**ALL**' as Customer,");
                }

                else if (customer == "ICS-RELOADABLE")
                {
                    select = select.Append("'ICS-RELOADABLE' as Customer,");
                    where.Append("Customer in ('ICS-PREMIUM','ICS-PREMIUM-UNL','ICS-PAC-WEST-5','ICS-NUVOX-5') AND ");
                    union.Append("'**ALL**' as Customer,");
                }

                else if (customer == "ICS-RELOADABLE-2")
                {
                    select = select.Append("'ICS-RELOADABLE-2' as Customer,");
                    where.Append("Customer in ('ICS-PAC-WEST-2','ICS-NUVOX-2','ICS-PREMIUM-UNL-2') AND ");
                    union.Append("'**ALL**' as Customer,");
                }
                else if (customer == "A_D-WORLDWIDE")
                {
                    select = select.Append("'A&D-WORLDWIDE' as Customer,");
                    where.Append("Customer like 'A&D-WORLDWIDE' AND ");
                    union.Append("'**ALL**' as Customer,");
                }
                else
                {
                    if (customer == "**ALL**")
                    {
                        select = select.Append("Case When (Customer like 'CC-%' or Customer like 'CCP-%' or Customer like 'VINTALK-OUT') THEN 'TALKTEL' ELSE Customer END as Customer,");
                        where.Append("Customer like '" + customer + "' AND ");
                        group.Append("Case When (Customer like 'CC-%' or Customer like 'CCP-%' or Customer like 'VINTALK-OUT') THEN 'TALKTEL' ELSE Customer END,");
                        union.Append("'**ALL**' as Customer,");
                        ord = ord + 1;
                    }
                    else
                    {
                        select = select.Append("Customer,");
                        if (Session["DirectoUser"].ToString() == "jmanatlao" && customer == "**ALL**")
                        {
                            where.Append("Customer IN (SELECT DISTINCT ProviderName from cdrdb.dbo.ProviderIP  WHERE Type like 'C' AND Providername IN ('ALLACCESSPH-PINLESS','PHIP-CALLCENTER','ALLACCESS-UNL-PASSIP','ALLACCESS-UNL-USASMA','ALLACCESSPH-UNL-FLAT','ALLACCESS-NETCOMM') UNION SELECT Providername FROM cdrdb.dbo.ProviderIP  WHERE Providername like 'DID-%' AND [Type] = 'C') AND ");
                        }
                        else
                        {                            
                            where.Append("Customer like '" + customer + "' AND ");
                        }


                        group.Append("Customer,");
                        union.Append("'**ALL**' as Customer,");
                        ord = ord + 1;
                    }
                }

            }
            //VENDOR
            if (vendor != "** NONE **")
            {
                select = select.Append("Vendor,");

                if (Session["DirectoUser"].ToString() == "jmanatlao" && vendor == "**ALL**")
                {
                    where.Append("Vendor IN ('ALLACCESS-UNL-PASS','ALLACCESS-UNL-PBX') AND ");
                }
                else
                {
                    where.Append("Vendor like '" + vendor + "' AND ");
                }
                group.Append("Vendor,");
                union.Append("'**ALL**' as Vendor,");                
            }
            //OriginationIP
            if (originationIP != "** NONE **")
            {
                select = select.Append("OriginationIP,");
                where = where.Append("OriginationIP like '" + originationIP + "' AND ");
                group.Append("OriginationIP,");
                union.Append("'' as OriginationIP,");
            }
            //TerminationIP
            if (terminationIP != "** NONE **")
            {
                select = select.Append("TerminationIP,");
                where = where.Append("TerminationIP like '" + terminationIP + "' AND ");
                group.Append("TerminationIP,");
                union.Append("'' as TerminationIP,");

            }
            //Country
            if (country != "** NONE **")
            {
                select = select.Append("Country,");
                where = where.Append("Country like '" + country + "' AND ");
                group.Append("Country,");
                union.Append("'' as Country,");
            }
            //Region
            if (region != "** NONE **")
            {
                select = select.Append("Region,");
                where = where.Append("Region like '" + region + "' AND ");
                group.Append("Region,");
                union.Append("'' as Region,");
            }
            //Type
            if (type != "** NONE **")
            {
                select = select.Append("Type,");
                where = where.Append("Type like '" + type + "' AND ");
                group.Append("Type,");
                union.Append("'' as Type,");
            }
            //Class
            if (clas != "** NONE **")
            {
                select = select.Append("Class,");
                where = where.Append("Class like '" + clas + "' AND ");
                group.Append("Class,");
                union.Append("'' as Class,");

            }
            //LMC
            if (lmc != "** NONE **" && lmc != "**ALL**")
            {
                select = select.Append("LMC,");
                //select = select.Append("'" + lmc + "' as LMC, ");
                //where = where.Append("LMC in (SELECT LMC FROM MECCA2..LMC WHERE ENTERPRISE = '" + lmc + "') AND ");
                where = where.Append("LMC like '" + lmc + "' AND ");
                group.Append("LMC,");
                union.Append("'' as LMC,");

            }

           
            
            //Source MSW
            if (source != "** NONE **")
            {
                string tmp = string.Empty;
                tmp = where.ToString();
                select = select.Append("Source,");

                whereR = where.ToString() + "Source like '" + source + "CTMP%' AND ";
                whereT = where.ToString() + "Source like '" + source + "CTMP%' AND ";
                
                where = where.Append("Source like '" + source + "' AND ");
                                                                
                group.Append("Source,");
                union.Append("'' as Source,");
            }
            //Gateway
            if (gateway != "** NONE **")
            {
                select = select.Append("VendorID,");
                where = where.Append("VendorID like '" + gateway + "' AND ");
                group.Append("VendorID,");
                union.Append("'' as VendorID,");
            }


            if (lmc == "**ALL**")
            {
                select = select.Append("LMC,");
                group.Append("LMC,");
                union.Append("'' as LMC,");
            }


            select = select.Replace("**ALL**", "%");
            where = where.Replace("**ALL**", "%");
            string selectS = select.ToString();
            string whereW = where.ToString();
            string groupG = group.ToString();
            //string orderO = order.ToString();
            string unionU = union.ToString();


            int lst = whereW.LastIndexOf("AND");//where
            whereW = whereW.Remove(lst, whereW.Length - lst);

            if (groupG.Length < 10)
                groupG = " ";
            else
            {
                lst = groupG.LastIndexOf(",");//group
                groupG = groupG.Remove(lst, groupG.Length - lst);
            }

            lst = unionU.LastIndexOf(",");//union
            unionU = unionU.Remove(lst, unionU.Length - lst);

            select.Replace(select.ToString(), selectS);
            where.Replace(where.ToString(), whereW);
            group.Replace(group.ToString(), groupG);
            union.Replace(union.ToString(), unionU);

            select.Append("sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,CAST(AVG(PDD) as int) AS PDD,ROUND(SUM(CAST(ShortCalls as float)*100)/SUM(case when Calls = 0 then 1 else Calls end),2) as PSC,ROUND(SUM(CAST(Fas as float) * 100)/SUM(case when Calls = 0 then 1 else Calls end),2) as PFAS");
            union.Append(",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,CAST(AVG(PDD) as int) AS PDD,ROUND(SUM(CAST(ShortCalls as float)*100)/SUM(case when Calls = 0 then 1 else Calls end),2) as PSC,ROUND(SUM(CAST(Fas as float) * 100)/SUM(case when Calls = 0 then 1 else Calls end),2) as PFAS");

            //select.Append("sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,CAST(AVG(PDD) as int) AS PDD,ROUND(SUM(CAST(ShortCalls as float)*100)/SUM(case when Calls = 0 then 1 else Calls end),2) as PSC");
            //union.Append(",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,CAST(AVG(PDD) as int) AS PDD,ROUND(SUM(CAST(ShortCalls as float)*100)/SUM(case when Calls = 0 then 1 else Calls end),2) as PSC");

            //Recent
            whereR = whereR.Replace("**ALL**CTMP%", "%CTMP");
            whereR = whereR.Replace("**ALL**", "%");
            lst = whereR.LastIndexOf("AND");

            if (lst < 0)
                whereR = where.ToString();
            else
                whereR = whereR.Remove(lst, whereR.Length - lst);

            if (Session["hometype"] != null)
            {

                if (Session["hometype"].ToString() == "W")
                {
                    whereR += " AND Routetype = 'W' ";
                }
                else if (Session["hometype"].ToString() == "R")
                {
                    whereR += " AND Routetype = 'R' ";
                }
            }

            if (whereR.Length > 6 && ccdata == "True")
            {
                whereR += " AND Customer not in (Select Customer from cdrdb.dbo.custbrokerrelcc) ";
            }

            qry.Append(select.ToString() + " from OpSheetInc_ITNX " + whereR + group.ToString() + union + " from OpSheetInc_ITNX " + whereR.ToString() + " ORDER BY TotalMinutes DESC");
 //new
            //Response.Write(qry);
            //Response.End();


            DataSet mydataSet1 = RunQuery(qry, "MECCA2ConnectionString");
            gdvRecent.DataSource = mydataSet1.Tables[0];
            gdvRecent.DataBind();
 //Todays
            whereT = whereT.Replace("**ALL**CTMP%", "%CTMP");
            whereT = whereT.Replace("**ALL**", "%");

            lst = whereT.LastIndexOf("AND");
            if (lst < 0)
                whereT = where.ToString();
            else
                whereT = whereT.Remove(lst, whereT.Length - lst);

            if (Session["hometype"] != null)
            {

                if (Session["hometype"].ToString() == "W")
                {
                    whereT += " AND Routetype = 'W' ";
                }
                else if (Session["hometype"].ToString() == "R")
                {
                    whereT += " AND Routetype = 'R' ";
                }
            }
            
            qry.Remove(0, qry.Length);

            if (whereT.Length > 6 && ccdata == "True")
            {
                whereT += " AND Customer not in (Select Customer from cdrdb.dbo.custbrokerrelcc) ";
            }

            qry.Append(select.ToString() + " from OpSheetInc_ITNX " + whereT + group.ToString() + union + " from OpSheetInc_ITNX " + whereT.ToString() + " ORDER BY TotalMinutes DESC");            
            qry.Replace("OpSheetInc_ITNX", "OpSheetAll_ITNX");
            string qry2 = qry.ToString();

            DataSet myDataSet2 = RunQuery(qry, "MECCA2ConnectionString");
            gdvToday.DataSource = myDataSet2.Tables[0];
            gdvToday.DataBind();
 //Yesterday
            qry.Remove(0, qry.Length);

            if (Session["hometype"] != null)
            {

                if (Session["hometype"].ToString() == "W")
                {
                    where.Append(" AND Routetype = 'W' ");
                }
                else if (Session["hometype"].ToString() == "R")
                {
                    where.Append(" AND Routetype = 'R' ");
                }
            }

            if (where.Length > 6 && ccdata == "True")
            {
                where.Append(" AND Customer not in (Select Customer from cdrdb.dbo.custbrokerrelcc) ");
            }


            qry.Append(select.ToString() + " from OpSheetInc_ITNX " + where.ToString() + group.ToString() + union + " from OpSheetInc_ITNX " + where.ToString() + " ORDER BY TotalMinutes DESC");
            qry.Replace("OpSheetInc_ITNX", "OpSheetAll_ITNX");            
            qry.Replace("OpSheetAll_ITNX", "OpSheetY_ITNX");
            string qry3 = qry.ToString();

            DataSet myDataSet3 = RunQuery(qry, "MECCA2ConnectionString");
            gdvYesterday.DataSource = myDataSet3.Tables[0];
            gdvYesterday.DataBind();

            //new

            //lblRefresh YESTERDAY
            StringBuilder query = new StringBuilder();
            query.Append("select crdate as Modified from [mecca2].[dbo].sysobjects  where type = 'U' and name like 'OpSheetY_ITNX'");
            DataSet ResultSet1;
            ResultSet1 = RunQuery(query, "MECCA2ConnectionString");
            lblYesterday.Text = "Last Modified:" + ResultSet1.Tables[0].Rows[0][0].ToString();
            ////lblRefresh RECENT
            StringBuilder recentupdate = new StringBuilder();
            recentupdate.Append("select crdate as Modified from [mecca2].[dbo].sysobjects  where type = 'U' and name like 'OpSheetInc_ITNX'");
            DataSet ResultSet2;
            ResultSet2 = RunQuery(recentupdate, "MECCA2ConnectionString");
            lblRecent.Text = "Last Modified:" + ResultSet2.Tables[0].Rows[0][0].ToString();
            lblRecent.Visible = true;
            //lblRefresh TODAY
            StringBuilder todayupdate = new StringBuilder();
            todayupdate.Append("select crdate as Modified from [mecca2].[dbo].sysobjects  where type = 'U' and name like 'OpSheetAll_ITNX'");
            DataSet ResultSet3;
            ResultSet3 = RunQuery(todayupdate, "MECCA2ConnectionString");
            lblToday.Text = "Last Modified:" + ResultSet3.Tables[0].Rows[0][0].ToString();
            lblToday.Visible = true;

            if (customer == "NEW-WORLD-BRANDS")
            {
                Label1.Text = "<strong><font color=\"red\">Recent Traffic - 60 Minutes Back</font></strong>" ;                
            }

            lblHeader.Text = customer;
            lblBilling.Visible = false;
        }
        catch (Exception ex)
        {
            lblBilling.Visible = true;
            string error = ex.Message.ToString();

        }
    }


    private System.Data.DataSet RunQuery(StringBuilder qry, string StringConnection)
    {

        System.Data.SqlClient.SqlCommand sqlQuery = new System.Data.SqlClient.SqlCommand();
        sqlQuery.CommandText = qry.ToString();


        string connectionString = ConfigurationManager.ConnectionStrings
        [StringConnection].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

}
