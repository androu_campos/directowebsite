<%@ Page Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="Prates.aspx.cs" Inherits="Prates" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="left">
        <tr>
            <td style="width: 185px">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="Show older traffic"
                    Width="108px"></asp:Label></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 185px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 185px" align="right" valign="top">
                &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="from:"></asp:Label></td>
            <td style="width: 100px" align="left" valign="top">
                <asp:TextBox ID="TextBox1" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td style="width: 100px" valign="top">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td style="width: 100px" valign="top">
                <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="to:"></asp:Label></td>
            <td align="left" style="width: 100px" valign="top">
                <asp:TextBox ID="TextBox2" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td align="left" style="width: 100px" valign="top">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
            <td align="left" style="width: 100px" valign="top">
                <asp:Button ID="cmdShow" runat="server" CssClass="boton" OnClick="cmdShow_Click"
                    Text="View report" /></td>
        </tr>
        <tr>
            <td style="width: 185px" valign="top">
            </td>
            <td align="left" style="width: 100px" valign="top">
                <asp:CheckBox ID="CheckBox1" runat="server" CssClass="labelBlue8" Text="Show by region" /></td>
            <td style="width: 100px" valign="top">
            </td>
            <td style="width: 100px" valign="top">
            </td>
            <td align="left" style="width: 100px" valign="top">
            </td>
            <td align="left" style="width: 100px" valign="top">
            </td>
            <td align="left" style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 185px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 185px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="Nothing found"
                    Visible="False"></asp:Label></td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 185px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
                <asp:Button ID="cmdExport" runat="server" CssClass="boton" OnClick="cmdExport_Click"
                    Text="Export to EXCEL" Visible="False" Width="86px" /></td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 27px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 185px">
            </td>
            <td colspan="4" valign="top">
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" Font-Names="Arial" Font-Size="8pt" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="25">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 185px">
            </td>
            <td colspan="4" valign="top">
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager><br />
                <cc1:calendarextender id="CalendarExtender1" runat="server" popupbuttonid="Image1"
                    targetcontrolid="TextBox1"></cc1:calendarextender>
                <cc1:calendarextender id="CalendarExtender2" runat="server" popupbuttonid="Image2"
                    targetcontrolid="TextBox2"></cc1:calendarextender>
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
</asp:Content>

