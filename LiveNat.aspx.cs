using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class LiveNat : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {

            lblQtmon.Text = Util.getLiveTrafficWatch("NTCQ_1", "NTCQ_4");//NATIONAL TELQUES
            lblAlfa.Text = Util.getLiveTrafficWatch("VSGO_A2", "EZQL_A16");//ALFA
            lblDirectoV.Text = Util.getLiveTrafficWatch("VSGO_D1", "SNMS_S1");//DIRECTOV            
            //lblGuatemala.Text = Util.getLiveTrafficWatch("GNAT_1", "GNAT_5");//NATIONAL GUATE
            Label6.Text = Util.getLiveTrafficWatch("SNMS_S2", "CRDL_S39");//STARCITY
            lblNatOff.Text = Util.getLiveTrafficWatch("NOFF_1", "NOFF_21");//NATIONAL OFF
            StreamReader FSOqtmon = new StreamReader(@"C:\Inetpub\wwwroot\qtmon.html", System.Text.ASCIIEncoding.Default);
            lblAll.Text = FSOqtmon.ReadToEnd();
            FSOqtmon.Close();


        }
        catch (Exception ex)
        {
            string errormessage = ex.Message.ToString();
        }
    }
   
}
