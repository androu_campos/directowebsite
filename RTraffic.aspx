<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="RTraffic.aspx.cs" Inherits="RTraffic" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:Label ID="lblHeader" runat="server" CssClass="labelTurkH" Text="Label"></asp:Label>
    <table  width="100%" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td align="left" style="width: 100px">
                <asp:Button ID="cmdExport" runat="server" CssClass="boton" OnClick="cmdExport_Click"
                    Text="Export to EXCEL" /></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 100px">
    <asp:GridView ID="gdvRtraffic" Width="654px" AutoGenerateColumns="False" runat="server" OnSelectedIndexChanging="gdvRtraffic_SelectedIndexChanging" Font-Names="Arial" Font-Size="8pt" ForeColor="#333333" GridLines="None" Height="170px">
    <HeaderStyle CssClass="titleOrangegrid" Height="40px" Width="150px" Font-Names="Arial" Font-Size="8pt" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <AlternatingRowStyle BackColor="White" />
    
    <Columns>
    
    
    
    
    <asp:BoundField DataField="TotalMinutes" HeaderText="TotalMinutes" HtmlEncode="False" DataFormatString="{0:f2}" >
    <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
    <HeaderStyle HorizontalAlign="Left" />
    </asp:BoundField>
    
    
    <asp:BoundField DataField="Attempts" HeaderText="Attempts">
    <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
    <HeaderStyle HorizontalAlign="Left" />
    </asp:BoundField>
    
    <asp:BoundField DataField="AnsweredCalls" HeaderText="AnsweredCalls">
    <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Center" Width="100px" />
    <HeaderStyle HorizontalAlign="Left" />
    </asp:BoundField>
    
    
    <asp:BoundField DataField="Rejected" HeaderText="RejectedCalls" >
    <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Center" Width="100px" />
    <HeaderStyle HorizontalAlign="Left" />
    </asp:BoundField>
    
    
    <asp:BoundField DataField="ASR" HeaderText="ASR" HeaderStyle-HorizontalAlign="Center" >
    <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Center" Width="100px" />
    
    </asp:BoundField>
       
    <asp:BoundField DataField="ABR" HeaderText="ABR" >
    <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
    <HeaderStyle HorizontalAlign="Left" />
    </asp:BoundField>
        
     <asp:BoundField DataField="ACD" HeaderText="ACD" >
    <ItemStyle Font-Names="Arial" Font-Size="9pt" HorizontalAlign="Left" Width="100px" />
    <HeaderStyle HorizontalAlign="Left" />
    </asp:BoundField>
    
    </Columns>
    
    
    </asp:GridView>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        
        
        
        
        <tr>
            <td width="100%" colspan="2">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <table>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
   
    
</asp:Content>

