<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="DIDGuide.aspx.cs" Inherits="DIDGuide" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <table>
        
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="simperc" runat="server" CssClass="labelTurk" Text="DID Guide" Width="219px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 21px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="cmdExcelExport" runat="server" CssClass="boton" OnClick="cmdExport" Text="Export to Excel" Width="91px" />
                        </td>
                    </tr>                    
                </table>                
            </td>
        </tr>         
        <tr>
            <td>
                &nbsp;
                <%--<asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" DataSourceID="sqldidGuide" PageSize="500">
                    <Columns>
                        <asp:CommandField ShowEditButton="True" />
                        <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" />
                        <asp:BoundField DataField="did" HeaderText="did" SortExpression="did" />
                        <asp:BoundField DataField="country" HeaderText="country" SortExpression="country" />
                        <asp:BoundField DataField="city" HeaderText="city" SortExpression="city" />
                        <asp:BoundField DataField="stateprefix" HeaderText="stateprefix" SortExpression="stateprefix" />
                        <asp:BoundField DataField="state" HeaderText="state" SortExpression="state" />
                        <asp:BoundField DataField="providername" HeaderText="providername" SortExpression="providername" />
                        <asp:BoundField DataField="framework" HeaderText="framework" SortExpression="framework" />
                    </Columns>
                </asp:GridView>--%>
                <asp:GridView ID="gvResult" runat="server" AllowSorting="true" Font-Size="8pt" Font-Names="Arial" 
                     AllowPaging="True" OnSorting="gridView_Sorting" DataKeyNames="id"
                     OnPageIndexChanging="gvResult_PageIndexChanging"  PageSize="300" 
                     AutoGenerateColumns="False" 
                     AutoGenerateEditButton="True" DataSourceID="sqldidGuide" Enabled="true"> 
                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                    <EditRowStyle BackColor="#E0E0E0"></EditRowStyle>
                    <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                    <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial" 
                    Font-Bold="True" HorizontalAlign = "Center"></HeaderStyle>
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                    <Columns>
                        <asp:BoundField DataField="Did" HeaderText="Did" SortExpression="Did" >
                            <HeaderStyle Width="100px"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country">
                            <HeaderStyle Width="100px"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="City" HeaderText="City" SortExpression="City">
                            <HeaderStyle Width="100px"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="StatePrefix" HeaderText="StatePrefix" SortExpression="StatePrefix">
                            <HeaderStyle Width="100px"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="State" HeaderText="State" SortExpression="State">
                            <HeaderStyle Width="100px"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="ProviderName" HeaderText="ProviderName" SortExpression="ProviderName" >
                            <HeaderStyle Width="100px"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="Framework" HeaderText="Framework" SortExpression="Framework">
                            <HeaderStyle Width="100px"/>
                        </asp:BoundField>                        
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
               <%-- <asp:Timer ID="Timer1" runat="server" Interval="900000">
            </asp:Timer><asp:Scriptmanager id="Scriptmanager1" runat="server"></asp:Scriptmanager>--%>
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="sqldidGuide" runat="server" ConnectionString="<%$ ConnectionStrings:ICSConnectionString %>"
        SelectCommand="SELECT id, did, country, city, stateprefix, state, providername, framework FROM sourceDID ORDER BY did"
        UpdateCommand="UPDATE [ICS].[dbo].[sourceDID] SET [country] = @country, [city] = @city, [stateprefix] = @stateprefix, [state]=@state, [providername]=@providername, [framework]=@framework WHERE [id]=@id"
        ProviderName="<%$ ConnectionStrings:ICSConnectionString.ProviderName %>">       
    </asp:SqlDataSource>
</asp:Content>

