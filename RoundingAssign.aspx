<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="RoundingAssign.aspx.cs" Inherits="RoundingAssign"
    Title="DIRECTO - Connections Worldwide" %>
<%@ OutputCache Duration="21600" Location="Client" VaryByParam="None" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label3" runat="server" Text="MECCA ROUNDING CONDITIONS" CssClass="labelTurkH"></asp:Label>
    <table width="100%" align="left" style="position: absolute; top: 220px; left: 230px;">
        <tr>
            <td width="20%" valign="top" align="left" style="height: 96px">
            <table>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Choose Type of Provider" CssClass="labelBlue8"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged"
                                RepeatDirection="Horizontal" CssClass="labelSteelBlue">
                                <asp:ListItem Value="C" Selected="True">Customer</asp:ListItem>
                                <asp:ListItem Value="V">Vendor</asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text="Choose Provider" CssClass="labelBlue8"></asp:Label></td>
                        <td>
                            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"
                                Width="154px" CssClass="dropdown">
                            </asp:DropDownList></td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="20%" style="height: 96px">
                <table align="left">
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Rounding Options"></asp:Label></td>
                    </tr>
                    <tr>
                        <td rowspan="2" style="width: 100px">
                            <table>
                                <tr>
                                    <td bgcolor="#d5e3f0" colspan="2" style="height: 16px">
                                        <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Rounding Types"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 16px;">
                                        <asp:Label ID="Label12" runat="server" CssClass="labelBlue8" Text="1"></asp:Label></td>
                                    <td style="width: 100px; height: 18px;">
                                        <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="1 / 1"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 16px;">
                                        <asp:Label ID="Label9" runat="server" CssClass="labelBlue8" Text="6"></asp:Label></td>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Text="6/6"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 16px;">
                                        <asp:Label ID="Label10" runat="server" CssClass="labelBlue8" Text="306"></asp:Label></td>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label11" runat="server" CssClass="labelBlue8" Text="30/6"></asp:Label></td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td style="width: 100px; height: 16px;">
                                        <asp:Label ID="Label25" runat="server" CssClass="labelBlue8" Text="60"></asp:Label></td>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label26" runat="server" CssClass="labelBlue8" Text="60/60"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                        <td rowspan="2" style="width: 18px">
                        </td>
                        <td rowspan="2" style="width: 150px"><table>
                            <tr>
                                <td bgcolor="#d5e3f0" colspan="2" style="height: 16px">
                                    <asp:Label ID="Label13" runat="server" CssClass="labelBlue8" Text="Default Rounding"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 141px; height: 16px;">
                                    <asp:Label ID="Label14" runat="server" CssClass="labelBlue8" Text="India"></asp:Label></td>
                                <td style="width: 100px; height: 18px;">
                                    <asp:Label ID="Label15" runat="server" CssClass="labelBlue8" Text="1 / 1"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 141px; height: 16px;">
                                    <asp:Label ID="Label16" runat="server" CssClass="labelBlue8" Text="Mexico"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:Label ID="Label17" runat="server" CssClass="labelBlue8" Text="60/60"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 141px; height: 16px;">
                                    <asp:Label ID="Label18" runat="server" CssClass="labelBlue8" Text="ROW"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:Label ID="Label19" runat="server" CssClass="labelBlue8" Text="30/6"></asp:Label></td>
                            </tr>
                        </table>
                            <table>
                                <tr>
                                    <td style="width: 141px; height: 16px;">
                                        <asp:Label ID="Label20" runat="server" CssClass="labelBlue8"></asp:Label></td>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label21" runat="server" CssClass="labelBlue8"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>
                
            </td>
        </tr>
        <tr>
            <td  align="left" background="#d5e3f0" style="height: 21px">
               
            <td   align="left" style="height: 21px">
                <asp:Label ID="Label22" runat="server" CssClass="labelBlue8" Font-Bold="False" Text="Note: The Default Roundings will NOT be displayed."></asp:Label></td>
            
        </tr>
        <tr>
            <td >
            </td>
            <td >
            </td>
            
        </tr>
        <tr>
            <td align="left"colspan="2" valign="bottom">
                <br />
                <asp:GridView ID="gdvRounding" runat="server" CellPadding="4" ForeColor="#333333"
                    GridLines="None" Font-Size="9pt" Font-Names="Arial" AutoGenerateColumns="true" Height="100%">
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <EditRowStyle BackColor="#999999" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                       <%-- <asp:BoundField DataField="Provider" HeaderText="Provider">
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Country" HeaderText="Country">
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Rounding" HeaderText="Rounding">
                            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>--%>
                    </Columns>
                </asp:GridView>
                </td>
        </tr>
        <tr>
            <td width="100%" colspan="2">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="left"colspan="2">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
