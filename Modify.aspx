<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Modify.aspx.cs" Inherits="Modify" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                <asp:Label ID="Label2" runat="server" CssClass="labelTurkH" Text="Rates"></asp:Label>
 
    <table width="100%" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td align="left" style="width: 100px" valign="top">
                <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="Button1_Click"
                    Text="Export to Excel" /></td>
        </tr>
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="Label1" runat="server" Text="Nothing found"
        Visible="False" ForeColor="Red"></asp:Label><br />
    <asp:GridView ID="gdvModify" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="#333333" AutoGenerateColumns="true" AutoGenerateEditButton="true" DataKeyNames="Prefix" OnRowEditing="gdvModify_RowEditing" Width="300px" AllowPaging="True" PageSize="50">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
            Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />
    
       
    </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                &nbsp;</td>
        </tr>
           <tr>
            <td width="100%">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>

