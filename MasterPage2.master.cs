using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPage2 : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Session["DirectoUser"] as string))//Usuario Logeado            
            {

                if (Session["Rol"].ToString() == "Customer")
                {

                    Session["hometype"] = "";
                    if (Session["DirectoUser"].ToString() == "acuenca")
                    {
                        Session["DirectoUser"] = "NIP-CALLING-CARD";
                    }
                    if (Session["DirectoUser"].ToString() == "Andres")
                    {
                        Session["DirectoUser"] = "ATSI";
                    }

                    MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adpBR = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
                    MyDataSetTableAdapters.Billing_StatusDataTable tblBR = adpBR.GetData();

                    if (Session["Rol"].ToString() != string.Empty || Request.Cookies["userMecca"] != null)//Usuario Logeado
                    {
                        if (Request.Cookies["link"] != null)
                        {
                            lnkCloseSesion.Visible = true;
                        }
                        else
                        {
                            lnkCloseSesion.Visible = true;
                        }


                        if (Request.Cookies["Rol"] != null)
                        {
                            Session["Rol"] = Request.Cookies["Rol"].Value.ToString();
                            Session["DirectoUser"] = Request.Cookies["userMecca"].Value.ToString();
                        }


                        lblUserName.Text = Session["DirectoUser"].ToString();
                        string urlrequested = Request.RawUrl.ToString();
                        int urlLength = urlrequested.Length;
                        int index = urlrequested.LastIndexOf("/");
                        int sufix = urlrequested.LastIndexOf("x");
                        string url = urlrequested.Substring(index, sufix - index + 1);
                        url = "~" + url;


                        MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpS = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                        MyDataSetTableAdapters.MainNodesDataTable tblS = adpS.GetDataByUrlnRol(url, Session["Rol"].ToString());

                        if (tblS.Count == 0 && Session["Rol"].ToString() != "Admins")//Verifica si el usuario tiene acceso a esta pagina
                        {

                            Response.Redirect("ContactUs.aspx", false);

                        }
                        else
                        {
                            MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adp = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
                            MyDataSetTableAdapters.Billing_StatusDataTable tbl = adp.GetData();


                            if (!Page.IsPostBack)//Construye el Treeview                        
                            {

                                //obtiene el rol del usuario
                                string rol = Session["Rol"].ToString();
                                //Make tree new                 
                                //query qe regresa los nodos principales
                                MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter adpN = new MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter();
                                MyDataSetTableAdapters.Roles_Nodes1DataTable tblN = adpN.GetDataByRole(rol, rol);
                                int jN = tblN.Count;
                                string[] selected = new string[jN];
                                for (int i = 0; i < jN; i++)
                                {
                                    string rolName = tblN.Rows[i]["NodeName"].ToString();
                                    //Nodos primarios   nivel 1                        
                                    Menu1.Items.Add(new MenuItem(rolName, i.ToString()));
                                    int idNodeFhater = Convert.ToInt32(tblN.Rows[i]["Id"]);
                                    Menu1.Items[i].Selectable = false;

                                    //Llenar los nodos secundarios  nivel 2
                                    MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpBN = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                                    MyDataSetTableAdapters.MainNodesDataTable tblBN = adpBN.GetDataByFatherId(idNodeFhater);
                                    int rbN = tblBN.Rows.Count;
                                    for (int n = 0; n < rbN; n++)
                                    {
                                        string nameNode = tblBN.Rows[n]["Name"].ToString();
                                        string urlPath = tblBN.Rows[n]["Url"].ToString();
                                        //int idNodeFhater2 = Convert.ToInt32(tblBN.Rows[n]["IdNode"].ToString());//Id del nodo actual
                                        Menu1.Items[i].ChildItems.Add(new MenuItem(nameNode, nameNode, "./Pag/imas/int/f-azul.gif", urlPath));
                                    }



                                }
                                //end Make

                                if (Session["DirectoUser"].ToString() == "NIP-CALLING-CARD" || Session["DirectoUser"].ToString() == "acuenca")
                                {
                                    Menu1.Items[0].ChildItems.Add(new MenuItem("CDR's Viewer", "CDR's Viewer", "./Pag/imas/int/f-azul.gif", "CdrsViewer.aspx"));
                                }
                                else if (Session["DirectoUser"].ToString() == "AFFINITY-VOIP" || Session["DirectoUser"].ToString() == "csolares")
                                {
                                    Menu1.Items[0].ChildItems.RemoveAt(2);
                                }
                                Session["ElMenu"] = Menu1;
                            }
                        }

                    }
                    else
                    {

                        Response.Redirect("SignIn.aspx", false);
                    }

                }

                else if (Session["Rol"].ToString() == "CustomerCDRS")
                {
                    if (!Page.IsPostBack)//Construye el Treeview                        
                    {

                        //obtiene el rol del usuario
                        string rol = Session["Rol"].ToString();
                        lnkCloseSesion.Visible = true;
                        lblUserName.Text = Session["DirectoUser"].ToString();
                        string urlrequested = Request.RawUrl.ToString();
                        int urlLength = urlrequested.Length;
                        int index = urlrequested.LastIndexOf("/");
                        int sufix = urlrequested.LastIndexOf("x");
                        string url = urlrequested.Substring(index, sufix - index + 1);
                        url = "~" + url;


                        MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpS = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                        MyDataSetTableAdapters.MainNodesDataTable tblS = adpS.GetDataByUrlnRol(url, Session["Rol"].ToString());

                        if (tblS.Count == 0 && Session["Rol"].ToString() != "Admins")//Verifica si el usuario tiene acceso a esta pagina
                        {

                            Response.Redirect("Home.aspx", false);

                        }
                        else
                        {
                            //Make tree new                 
                            //query qe regresa los nodos principales
                            MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter adpN = new MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter();
                            MyDataSetTableAdapters.Roles_Nodes1DataTable tblN = adpN.GetDataByRole(rol, rol);
                            int jN = tblN.Count;
                            string[] selected = new string[jN];
                            for (int i = 0; i < jN; i++)
                            {
                                string rolName = tblN.Rows[i]["NodeName"].ToString();
                                //Nodos primarios   nivel 1                        
                                Menu1.Items.Add(new MenuItem(rolName, i.ToString()));
                                int idNodeFhater = Convert.ToInt32(tblN.Rows[i]["Id"]);
                                Menu1.Items[i].Selectable = false;

                                //Llenar los nodos secundarios  nivel 2
                                MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpBN = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                                MyDataSetTableAdapters.MainNodesDataTable tblBN = adpBN.GetDataByFatherId(idNodeFhater);
                                int rbN = tblBN.Rows.Count;
                                for (int n = 0; n < rbN; n++)
                                {
                                    string nameNode = tblBN.Rows[n]["Name"].ToString();
                                    string urlPath = tblBN.Rows[n]["Url"].ToString();
                                    //int idNodeFhater2 = Convert.ToInt32(tblBN.Rows[n]["IdNode"].ToString());//Id del nodo actual
                                    Menu1.Items[i].ChildItems.Add(new MenuItem(nameNode, nameNode, "./Pag/imas/int/f-azul.gif", urlPath));
                                }



                            }
                            //end Make           
                        }
                    }
                }
                else if (Session["Rol"].ToString() == "VendorCDRS")
                {
                    if (!Page.IsPostBack)//Construye el Treeview                        
                    {

                        //obtiene el rol del usuario
                        string rol = Session["Rol"].ToString();
                        lnkCloseSesion.Visible = true;
                        lblUserName.Text = Session["DirectoUser"].ToString();
                        string urlrequested = Request.RawUrl.ToString();
                        int urlLength = urlrequested.Length;
                        int index = urlrequested.LastIndexOf("/");
                        int sufix = urlrequested.LastIndexOf("x");
                        string url = urlrequested.Substring(index, sufix - index + 1);
                        url = "~" + url;


                        MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpS = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                        MyDataSetTableAdapters.MainNodesDataTable tblS = adpS.GetDataByUrlnRol(url, Session["Rol"].ToString());

                        if (tblS.Count == 0 && Session["Rol"].ToString() != "Admins")//Verifica si el usuario tiene acceso a esta pagina
                        {

                            Response.Redirect("Home.aspx", false);

                        }
                        else
                        {
                            //Make tree new                 
                            //query qe regresa los nodos principales
                            MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter adpN = new MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter();
                            MyDataSetTableAdapters.Roles_Nodes1DataTable tblN = adpN.GetDataByRole(rol, rol);
                            int jN = tblN.Count;
                            string[] selected = new string[jN];
                            for (int i = 0; i < jN; i++)
                            {
                                string rolName = tblN.Rows[i]["NodeName"].ToString();
                                //Nodos primarios   nivel 1                        
                                Menu1.Items.Add(new MenuItem(rolName, i.ToString()));
                                int idNodeFhater = Convert.ToInt32(tblN.Rows[i]["Id"]);
                                Menu1.Items[i].Selectable = false;

                                //Llenar los nodos secundarios  nivel 2
                                MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpBN = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                                MyDataSetTableAdapters.MainNodesDataTable tblBN = adpBN.GetDataByFatherId(idNodeFhater);
                                int rbN = tblBN.Rows.Count;
                                for (int n = 0; n < rbN; n++)
                                {
                                    string nameNode = tblBN.Rows[n]["Name"].ToString();
                                    string urlPath = tblBN.Rows[n]["Url"].ToString();
                                    //int idNodeFhater2 = Convert.ToInt32(tblBN.Rows[n]["IdNode"].ToString());//Id del nodo actual
                                    Menu1.Items[i].ChildItems.Add(new MenuItem(nameNode, nameNode, "./Pag/imas/int/f-azul.gif", urlPath));
                                }



                            }
                            //end Make           
                        }
                    }
                }
                else if (Session["Rol"].ToString() == "Vendor")
                {
                    MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp adp = new MyDataSetTableAdaptersTableAdapters.Billing_StatusAdp();
                    MyDataSetTableAdapters.Billing_StatusDataTable tbl = adp.GetData();

                    lnkCloseSesion.Visible = true;
                    lblUserName.Text = Session["DirectoUser"].ToString();

                    if (!Page.IsPostBack)//Construye el Treeview                        
                    {

                        //obtiene el rol del usuario
                        string rol = Session["Rol"].ToString();
                        //Make tree new                 
                        //query qe regresa los nodos principales
                        MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter adpN = new MyDataSetTableAdaptersTableAdapters.Roles_Nodes1TableAdapter();
                        MyDataSetTableAdapters.Roles_Nodes1DataTable tblN = adpN.GetDataByRole(rol, rol);
                        int jN = tblN.Count;
                        string[] selected = new string[jN];
                        for (int i = 0; i < jN; i++)
                        {
                            string rolName = tblN.Rows[i]["NodeName"].ToString();
                            //Nodos primarios   nivel 1                        
                            Menu1.Items.Add(new MenuItem(rolName, i.ToString()));
                            int idNodeFhater = Convert.ToInt32(tblN.Rows[i]["Id"]);
                            Menu1.Items[i].Selectable = false;

                            //Llenar los nodos secundarios  nivel 2
                            MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter adpBN = new MyDataSetTableAdaptersTableAdapters.MainNodesTableAdapter();
                            MyDataSetTableAdapters.MainNodesDataTable tblBN = adpBN.GetDataByFatherId(idNodeFhater);
                            int rbN = tblBN.Rows.Count;
                            for (int n = 0; n < rbN; n++)
                            {
                                string nameNode = tblBN.Rows[n]["Name"].ToString();
                                string urlPath = tblBN.Rows[n]["Url"].ToString();
                                //int idNodeFhater2 = Convert.ToInt32(tblBN.Rows[n]["IdNode"].ToString());//Id del nodo actual
                                Menu1.Items[i].ChildItems.Add(new MenuItem(nameNode, nameNode, "./Pag/imas/int/f-azul.gif", urlPath));
                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect("Home.aspx", false);
                }

            }
        }

        catch (Exception ex)
        {
            string errormessage = ex.Message.ToString();

        }
    }

    protected void lnkCloseSesion_Click(object sender, EventArgs e)
    {

        if (Session["RememberMe"].ToString() == "ON" || Request.Cookies["userMecca"] != null)
        {//clear cookies
            Response.Cookies["userMecca"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["passMecca"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["link"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["Rol"].Expires = DateTime.Now.AddDays(-1);
            Session["RememberMe"] = string.Empty;
            Response.Redirect("~/SignIn.aspx");
        }
        else

            Session["Rol"] = string.Empty;
        Session["DirectoUser"] = string.Empty;
        Response.Redirect("~/SignIn.aspx");
    }
}
