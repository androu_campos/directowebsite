using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class InvoiceMaker2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    
    protected void cmdInvoice_Click(object sender, EventArgs e)
    {
        

        string[] invoiceArray = new string[12];

        invoiceArray[0] = dpdCustomer.SelectedValue.ToString();
        invoiceArray[1]= txtAddress.Text;
        invoiceArray[2]= txtCityZip.Text;
        invoiceArray[3]= txtPhone.Text;
        invoiceArray[4]= txtFrom.Text;
        invoiceArray[5]= txtTo.Text;
        invoiceArray[6]=txtInvoiceNumber.Text;
        invoiceArray[7]=txtDuedate.Text;
        invoiceArray[8]=txtAdjustment.Text;
        invoiceArray[9]=txtPreviousBalance.Text;
        invoiceArray[10] = txtPreviousPayment.Text;
        invoiceArray[11] = txtLateFees.Text;

        if (txtDuedate.Text == string.Empty)
            invoiceArray[7] = "0";
        if (txtAdjustment.Text == string.Empty)
            invoiceArray[8] = "0";
        if (txtPreviousBalance.Text == string.Empty)
            invoiceArray[9] = "0";
        if(txtPreviousPayment.Text == string.Empty)
            invoiceArray[10] ="0";
        if (txtPreviousPayment.Text == string.Empty)
            invoiceArray[11] = "0";

        Session["invoiceParams"] = invoiceArray;
        Response.Redirect("./PrintInvoice.aspx");



    }
    protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
}
