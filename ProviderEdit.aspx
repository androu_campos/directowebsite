<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ProviderEdit.aspx.cs" Inherits="ProviderEdit" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="left">
        <tr>
            <td align="left" style="width: 72px" valign="top">
                <asp:Label ID="Label13" runat="server" CssClass="labelTurkH" Text="CUSTOMER EDIT"
                    Width="137px"></asp:Label></td>
            <td align="left" style="width: 100px" valign="top">
            </td>
            <td align="left" style="width: 100px" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 72px; height: 25px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 25px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 25px" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 72px; height: 86px;" align="left" valign="top">
                <table style="width: 319px; height: 26px">
                    <tr>
                        <td style="width: 100px; height: 22px">
                        </td>
                        <td style="width: 113px; height: 22px">
                            <asp:Label ID="Label17" runat="server" CssClass="labelTurk" Text="Update Customer:"
                                Width="120px"></asp:Label></td>
                        <td style="width: 113px; height: 22px">
                            <asp:Label ID="Label18" runat="server" CssClass="labelTurk" Text="Create Customer:"
                                Width="120px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 22px">
                <asp:Label ID="Label1" runat="server" Text="Choose Customer:" CssClass="labelTurk" Width="120px"></asp:Label></td>
                        <td style="width: 113px; height: 22px">
                <asp:DropDownList ID="dpdProvider" runat="server" Width="182px" CssClass="dropdown" AutoPostBack="True" OnSelectedIndexChanged="dpdProvider_SelectedIndexChanged" DataSourceID="sqldsUpdate" DataTextField="MeccaName" DataValueField="MeccaName">
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList></td>
                        <td style="width: 113px; height: 22px">
                            <asp:DropDownList ID="ddlNew" runat="server" Width="182px" CssClass="dropdown" AutoPostBack="True" OnSelectedIndexChanged="ddlNew_SelectedIndexChanged" DataSourceID="sqldsNew" DataTextField="MeccaName" DataValueField="MeccaName">
                                <asp:ListItem></asp:ListItem>
                           </asp:DropDownList></td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="sqldsUpdate" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>" SelectCommand="SELECT ' Choose Customer' AS MeccaName UNION SELECT MeccaName from invoice">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sqldsNew" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:CDRDBConnectionString.ProviderName %>" SelectCommand="SELECT ' Choose Customer' AS MeccaName UNION SELECT DISTINCT ProviderName as MeccaName FROM ProviderIp WHERE ProviderName  NOT IN (SELECT MeccaName from invoice) AND [Type] = 'C' Order By MeccaName">
                </asp:SqlDataSource>
            </td>
            <td align="left" style="width: 100px; height: 86px;" valign="top">
            </td>
            <td style="width: 100px; height: 86px;" align="left" valign="top">
                </td>
        </tr>
        <tr>
            <td align="left" style="width: 72px; height: 64px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 64px" valign="top">
            </td>
            <td align="left" style="width: 100px; height: 64px" valign="top">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table>
                    <tr>
                        <td align="center" style="width: 100px">
                            <asp:Label ID="Label2" runat="server" CssClass="labelSteelBlue" Text="BussinesName"
                                Visible="False"></asp:Label></td>
                        <td align="center" style="width: 100px">
                            <asp:Label ID="Label3" runat="server" CssClass="labelSteelBlue" Text="MeccaName"
                                Visible="False"></asp:Label></td>
                        <td align="center" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" CssClass="labelSteelBlue" Text="Address" Visible="False"></asp:Label></td>
                        <td align="center" style="width: 100px">
                            <asp:Label ID="Label8" runat="server" CssClass="labelSteelBlue" Text="Phone" Visible="False"></asp:Label></td>
                        <td align="center" style="width: 100px">
                            <asp:Label ID="Label10" runat="server" CssClass="labelSteelBlue" Text="Prefix Invoice"
                                Visible="False"></asp:Label></td>
                        <td align="center" style="width: 100px">
                        </td>
                        <td align="center" style="width: 100px">
                        </td>
                        <td align="center" style="width: 100px">
                            <asp:Label ID="Label12" runat="server" CssClass="labelSteelBlue" Text="Tip" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtBussines" runat="server" CssClass="labelSteelBlue" Visible="False"></asp:TextBox></td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtMecca" runat="server" CssClass="labelSteelBlue" Visible="False"></asp:TextBox></td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtAddress" runat="server" CssClass="labelSteelBlue" Visible="False"></asp:TextBox></td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtPhone" runat="server" CssClass="labelSteelBlue" Visible="False"></asp:TextBox></td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtPrefix" runat="server" CssClass="labelSteelBlue" Visible="False"></asp:TextBox></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="dropdown" Visible="False" Width="70px">
                    <asp:ListItem Value="1">Weekly</asp:ListItem>
                    <asp:ListItem Value="2">Biweekly</asp:ListItem>
                    <asp:ListItem Value="3">Monthly</asp:ListItem>
                </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="Label5" runat="server" CssClass="labelSteelBlue" Text="City" Visible="False"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="Label6" runat="server" CssClass="labelSteelBlue" Text="ZipCode" Visible="False"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="Label7" runat="server" CssClass="labelSteelBlue" Text="ST" Visible="False"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="Label9" runat="server" CssClass="labelSteelBlue" Text="DueDate" Visible="False"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Label ID="Label11" runat="server" CssClass="labelSteelBlue" Text="NumInvoice"
                                Visible="False"></asp:Label></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtCity" runat="server" CssClass="labelSteelBlue" Visible="False"></asp:TextBox></td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtZipCode" runat="server" CssClass="labelSteelBlue" Visible="False"></asp:TextBox></td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtST" runat="server" CssClass="labelSteelBlue" Visible="False"></asp:TextBox></td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtDueDate" runat="server" CssClass="labelSteelBlue" Visible="False"></asp:TextBox></td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNum" runat="server" CssClass="labelSteelBlue" Visible="False"></asp:TextBox></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
                <br />
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblUpdate" runat="server" ForeColor="Red" Text="Customer Updated !!"
                    Visible="False"></asp:Label></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 72px" align="left">
                <br />
                <br />
                <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="Button1_Click1"
                    Text="Update" Visible="False" /></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 72px">
            </td>
            <td style="width: 100px">
                &nbsp;</td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
    <br />
</asp:Content>

