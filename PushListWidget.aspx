<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PushListWidget.aspx.cs" Inherits="PushListWidget" Title="Untitled Page" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Push List "
                    Width="134px" Font-Bold="True"></asp:Label></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="Select Country:"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdCtry" runat="server" CssClass="dropdown" AutoPostBack="True" OnSelectedIndexChanged="dpdCtry_SelectedIndexChanged" Width="183px">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Select Region:"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdRegions" runat="server" CssClass="dropdown" Width="183px">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label5" runat="server" CssClass="labelBlue8" Text="Select Type:"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdType" runat="server" CssClass="dropdown" Width="183px">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:Button ID="cmdReport" runat="server" OnClick="cmdReport_Click" Text="View" CssClass="boton" /></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:Label ID="Label2" runat="server" Text="Label" Visible="False"></asp:Label></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:GridView ID="gdvWidget" runat="server" 
                 
                Font-Names="Arial" Font-Size="8pt" OnRowCommand="gdvWidget_RowCommand" CssClass="dropdown" >
                    <Columns>
                        <asp:ButtonField ButtonType="Button" Text="Insert row" >
                            <ControlStyle CssClass="boton" />
                        </asp:ButtonField>
                    </Columns>
                    <HeaderStyle CssClass="titleOrangegrid" />
                </asp:GridView>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 35px;">
            </td>
            <td style="width: 100px; height: 35px;">
                <br />
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>" SelectCommand="SELECT Id, Country, City, Average_Cost, Expected_ASR, Yesterday_ACD, Avail_Capacity, Service_Level, Comments, Vendor_Billing, Date_Updated FROM Push_List"
                    UpdateCommand="UPDATE Push_List SET Country =@Country , City =@City , Average_Cost =@Average_Cost , Expected_ASR =@Expected_ASR , Yesterday_ACD =@Yesterday_ACD , Avail_Capacity =@Avail_Capacity , Service_Level =@Service_Level , Comments =@Comments , Vendor_Billing =@Vendor_Billing , Date_Updated =@Date_Updated ">
                    <UpdateParameters>
                        <asp:Parameter Name="Country" />
                        <asp:Parameter Name="City" />
                        <asp:Parameter Name="Average_Cost" />
                        <asp:Parameter Name="Expected_ASR" />
                        <asp:Parameter Name="Yesterday_ACD" />
                        <asp:Parameter Name="Avail_Capacity" />
                        <asp:Parameter Name="Service_Level" />
                        <asp:Parameter Name="Comments" />
                        <asp:Parameter Name="Vendor_Billing" />
                        <asp:Parameter Name="Date_Updated" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </td>
            <td style="width: 100px; height: 35px;">
            </td>
        </tr>
    </table>

</asp:Content>

