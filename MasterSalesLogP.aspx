<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="MasterSalesLogP.aspx.cs" Inherits="MasterSalesLogP" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" EnableEventValidation="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager id="ScriptManager1" runat="server">
                    </asp:ScriptManager>

<div style="text-align: left" align="left">    
<asp:Label ID="Label3" runat="server" Text="MECCA MASTER SALES LOG" CssClass="labelTurkH"></asp:Label>

        <table align="left" style="position:absolute; top:220px; left:230px;">
            <tr>
                <td style="width: 430px; height: 306px;" valign="top">
                    <table align="left" style="height: 100%;">
                        <tr>
                            <td style="width: 100px" valign=top align=left bgcolor="#d5e3f0">
                                <asp:Label ID="LabelS1" runat="server" Font-Bold="False"
                                    Text="Step 1 " CssClass="labelBlue8"></asp:Label></td>
                            <td style="width: 300px" valign=top align=left bgcolor="#d5e3f0">
                                <asp:Label ID="Label1" runat="server" Font-Bold="False" Font-Names="Arial" ForeColor="#004A8F"
                                    Text="Choose Range Of   Days" Width="282px" Font-Size="8pt"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2" style="width: 430px" valign="top">
                                <table>
                        <tr>
                            <td align="left" style="width: 170px" valign="top" rowspan="2">
                            <asp:RadioButtonList ID="cbxLtiempo" runat="server" CssClass="labelSteelBlue" OnSelectedIndexChanged="cbxLtiempo_SelectedIndexChanged"
                                    RepeatDirection="Horizontal" Width="120px" RepeatLayout="Flow" AutoPostBack="True">
                                <asp:ListItem Selected="True" Value="0">Personalized</asp:ListItem>
                                    <asp:ListItem Value="1">Monthly</asp:ListItem>
                                    <asp:ListItem Value="2">Weekly</asp:ListItem>
                                </asp:RadioButtonList></td>
                            <td style="width: 71px" valign=top align=left>
                            <asp:Label ID="labelFrom" runat="server" Text="From:" Font-Bold="False" CssClass="labelSteelBlue" />
                            </td>
                            <td style="width: 100px" valign=top align=left>
                                <asp:TextBox ID="txtFromV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue" AutoPostBack="True" MaxLength="10" OnTextChanged="txtFromV_TextChanged"></asp:TextBox></td>
                            <td style="width: 27px">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                        </tr>
                        <tr>
                            <td style="width: 71px;">
                                <asp:Label ID="labelTo" runat="server" Text="To:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                            <td style="width: 100px; height: 24px;">
                                <asp:TextBox ID="txtToV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue" AutoPostBack="True" MaxLength="10" OnTextChanged="txtToV_TextChanged"></asp:TextBox></td>
                            <td style="width: 27px;">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>                           
                            
                        </tr>
                    </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px; margin-top: 30px; margin-left: 0px; position: relative;" valign=top align=left bgcolor="#d5e3f0">
                                <asp:Label ID="LabelS2" runat="server" Font-Bold="False"
                                    Text="Step 2 " CssClass="labelBlue8"></asp:Label></td>
                            <td style="width: 300px" valign=top align=center bgcolor="#d5e3f0">
                                <asp:Label ID="Label9" runat="server" Font-Bold="False"
                                    Text="Optional" Width="282px" CssClass="labelBlue8"></asp:Label>
        </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 100px; height: 21px" valign="top">
                                <asp:Label ID="Label7" runat="server" Font-Bold="False" Text="CustBroker:" CssClass="labelSteelBlue"></asp:Label></td>
                            <td align="right" style="width: 300px; height: 21px" valign="top">
                                <asp:DropDownList ID="ddlCustBroker" runat="server" DataSourceID="sdldsCustBroker"
                                    DataTextField="CustBroker" DataValueField="CustBroker" OnSelectedIndexChanged="ddlCustBroker_SelectedIndexChanged"
                                    Width="300px" CssClass="dropdown">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 100px; height: 21px" valign="top">
                                <asp:Label ID="Label8" runat="server" Font-Bold="False" Text="VendorBroker:" CssClass="labelSteelBlue"></asp:Label></td>
                            <td align="left" style="width: 300px; height: 21px" valign="top">
                                <asp:DropDownList ID="ddlVendorBroker" runat="server" DataSourceID="sqldsVendorBroker"
                                    DataTextField="VendorBroker" DataValueField="VendorBroker" OnSelectedIndexChanged="ddlVendorBroker_SelectedIndexChanged"
                                    Width="300px" CssClass="dropdown">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px; height: 21px;" valign=top align=left bgcolor="#d5e3f0">
                                <asp:Label ID="LabelS3" runat="server" Font-Bold="False"
                                    Text="Step 3" CssClass="labelBlue8"></asp:Label></td>
                            <td style="width: 300px; height: 21px;" valign=top align=left bgcolor="#d5e3f0">
                                <asp:Label ID="Label4" runat="server" Font-Bold="False"
                                    Text="Choose Customer and Vendor" Width="281px" CssClass="labelBlue8"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 100px; height: 22px;" valign=top align=left>
        <asp:Label ID="labelCustomer" runat="server" Text="Customer:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                            <td style="width: 300px; height: 22px;" valign=top align=left>
                            <asp:DropDownList ID="ddlCust" runat="server" OnSelectedIndexChanged="ddlCust_SelectedIndexChanged" Width="300px" Font-Names="Arial" CssClass="dropdown" DataSourceID="sqldsCust" DataTextField="Customer" DataValueField="Customer"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px" valign=top align=left>
                            <asp:Label ID="labelVendor" runat="server" Text="Vendor:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                            <td style="width: 300px" valign=top align=left>
                            <asp:DropDownList ID="ddlVendor" runat="server" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" Width="300px" Font-Names="Arial" CssClass="dropdown" DataSourceID="sqldsVendor" DataTextField="Vendor" DataValueField="Vendor"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px" valign=top align=left bgcolor="#d5e3f0">
                                <asp:Label ID="LabelS4" runat="server" Font-Bold="False"
                                    Text="Step 4" CssClass="labelBlue8"></asp:Label></td>
                            <td style="width: 300px" valign=top align=left bgcolor="#d5e3f0">
                                <asp:Label ID="Label2" runat="server" Font-Bold="False"
                                    Text="Choose Country and Region" Width="281px" CssClass="labelBlue8"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 100px" valign=top align=left>
        <asp:Label ID="labelCountry" runat="server" Text="Country:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                            <td align="left" rowspan="2" style="width: 300px" valign="top">
                                <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" CssClass="dropdown"
                                    DataSourceID="sqldsCountry" DataTextField="Country" DataValueField="Country"
                                    Font-Names="Arial" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" Width="300px">
                                </asp:DropDownList><br />
                                &nbsp;<asp:DropDownList ID="ddlRegion" runat="server" CssClass="dropdown" Enabled="False"
                                    Font-Names="Arial" Width="300px">
                                </asp:DropDownList><br />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" valign=top align=left>
        <asp:Label ID="labelRegion" runat="server" Text="Region:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" bgcolor="#d5e3f0" style="width: 100px" valign="top">
                                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Step 5"></asp:Label></td>
                            <td align="left" bgcolor="#d5e3f0" style="width: 300px" valign="top">
                                <asp:Label ID="Label13" runat="server" CssClass="labelBlue8" Text="Choose LMC (optional)"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 100px" valign="top">
                                <asp:Label ID="Label14" runat="server" CssClass="labelSteelBlue" Text="LMC:"></asp:Label></td>
                            <td align="left" style="width: 300px" valign="top">
                                <asp:DropDownList ID="dpdLMC" runat="server" CssClass="dropdown" DataSourceID="sqldsLMC"
                                    DataTextField="LMC" DataValueField="LMC" Width="300px">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" bgcolor="#d5e3f0" style="width: 100px" valign="top">
                                <asp:Label ID="Label15" runat="server" CssClass="labelBlue8" Text="Step 6:"></asp:Label></td>
                            <td align="left" bgcolor="#d5e3f0" style="width: 300px" valign="top">
                                <asp:Label ID="Label16" runat="server" CssClass="labelBlue8" Text="Choose Type (optional)"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 100px" valign="top">
                                <asp:Label ID="Label17" runat="server" CssClass="labelSteelBlue" Text="Type:"></asp:Label></td>
                            <td align="left" style="width: 300px" valign="top">
                                <asp:DropDownList ID="dpdType" runat="server" CssClass="dropdown" DataSourceID="sqldsType"
                                    DataTextField="Type" DataValueField="Type" Width="300px">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" bgcolor="#d5e3f0" style="width: 100px" valign="top">
                                <asp:Label ID="Label18" runat="server" CssClass="labelBlue8" Text="Step 7"></asp:Label></td>
                            <td align="left" bgcolor="#d5e3f0" style="width: 300px" valign="top">
                                <asp:Label ID="Label19" runat="server" CssClass="labelBlue8" Text="Choose Class (optional)"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 100px" valign="top">
                                <asp:Label ID="Label20" runat="server" CssClass="labelSteelBlue" Text="Class:"></asp:Label></td>
                            <td align="left" style="width: 300px" valign="top">
                                <asp:DropDownList ID="dpdClass" runat="server" CssClass="dropdown" Width="300px">
                                    <asp:ListItem Selected="True" Value="%">**Select Class**</asp:ListItem>
                                    <asp:ListItem>OFF-NET</asp:ListItem>
                                    <asp:ListItem>ON-NET</asp:ListItem>
                                    <asp:ListItem>TBD</asp:ListItem>
                                    <asp:ListItem>UNIQUE</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                    <br />
                    
                    
                    <br />
                    <br />
                    <br />      
                    <br />
                    
                </td>
                <td bgcolor="white" colspan="2" style="height: 306px; text-align: left" valign="top">
<TABLE id="tblAjax"><TBODY><TR><TD style="WIDTH: 100px" vAlign=top>
<TABLE width="95%"><TBODY><TR><TD style="WIDTH: 100px" bgColor=#d5e3f0>
<asp:Label id="LabelS6" runat="server" Text="Step 8" Font-Bold="False" Width="100px" CssClass="labelBlue8"></asp:Label>
</TD>
<TD style="WIDTH: 150px" bgColor=#d5e3f0><asp:Label id="Label5" runat="server" Text="Choose Report" Font-Bold="False" Width="150px" CssClass="labelBlue8"></asp:Label></TD></TR></TBODY></TABLE>
<asp:RadioButtonList id="rdbtnlType" runat="server" Font-Bold="False" Width="260px" CssClass="labelSteelBlue" OnSelectedIndexChanged="rdbtnlType_SelectedIndexChanged" AutoPostBack="True">
<asp:ListItem Value="0">Sales Log By Region</asp:ListItem>
<asp:ListItem Value="1">Sales Log</asp:ListItem>
<asp:ListItem Value="2">Sales Log By Prefix</asp:ListItem>
<asp:ListItem Value="3">Sales Log Totals</asp:ListItem>
<asp:ListItem Value="4">Sales Log With Type</asp:ListItem>
<asp:ListItem Value="5">Yesterday's Profit By Customer</asp:ListItem>
<asp:ListItem Value="6">Yesterday's Profit By Vendor</asp:ListItem>
<asp:ListItem Value="18">Personalized</asp:ListItem>
</asp:RadioButtonList> 
<TABLE style="width: 156%"><TBODY><TR><TD style="WIDTH: 100px" bgColor=#d5e3f0><asp:Label id="Label10" runat="server" Text="Step 9" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px" bgColor=#d5e3f0><asp:Label id="Label11" runat="server" Text="Grouped  by date" CssClass="labelBlue8" Width="86px"></asp:Label></TD></TR><TR><TD colSpan=2><asp:CheckBoxList id="chkBilling" runat="server" CssClass="labelSteelBlue">
                                    <asp:ListItem Selected="True">Billing Date</asp:ListItem>
                                </asp:CheckBoxList></TD></TR></TBODY></TABLE><asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<TABLE><TBODY><TR><TD style="WIDTH: 100px"><asp:Label id="Label21" runat="server" Text="Loading, please wait..." Width="111px" CssClass="labelBlue8"></asp:Label></TD><TD style="WIDTH: 100px"><asp:Image id="Image5" runat="server" ImageUrl="~/Images/Loading.gif"></asp:Image></TD></TR></TBODY></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> </TD><TD style="WIDTH: 100px" vAlign=top><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 140px" bgColor=#d5e3f0><asp:Label id="LabelS5" runat="server" Text="Step 10" Font-Bold="False" Width="80px" CssClass="labelBlue8"></asp:Label></TD></TR></TBODY></TABLE>
    <asp:CheckBoxList ID="ckboxl" runat="server" AccesKey="myCheckBox" CssClass="labelSteelBlue"
        Font-Bold="False" Width="172px">
        <asp:ListItem Value="0">Customer</asp:ListItem>
        <asp:ListItem Value="1">Vendor</asp:ListItem>
        <asp:ListItem Value="2">CustBroker</asp:ListItem>
        <asp:ListItem Value="3">VendorBroker</asp:ListItem>
        <asp:ListItem Value="4">Country</asp:ListItem>
        <asp:ListItem Value="5">Region</asp:ListItem>
        <asp:ListItem Value="6">RegionPrefix</asp:ListItem>
        <asp:ListItem Value="7">CustPrefix</asp:ListItem>
        <asp:ListItem Value="8">VendorPrefix</asp:ListItem>
        <asp:ListItem Value="9">Type</asp:ListItem>
        <asp:ListItem Value="10">LMC</asp:ListItem>
        <asp:ListItem Value="25">Class</asp:ListItem>
        <asp:ListItem Value="11">Buy</asp:ListItem>
        <asp:ListItem Value="12">Sell</asp:ListItem>
        <asp:ListItem Value="14">Minutes (Customer)</asp:ListItem>
        <asp:ListItem Value="15">Minutes (Vendor)</asp:ListItem>
        <asp:ListItem Value="16">TotalCost</asp:ListItem>
        <asp:ListItem Value="17">TotalSales</asp:ListItem>
        <asp:ListItem Selected="True" Value="18">Profit</asp:ListItem>
        <asp:ListItem Value="21">Attempts</asp:ListItem>
        <asp:ListItem Value="13">Answered Calls</asp:ListItem>
        <asp:ListItem Value="23">Rejected calls</asp:ListItem>
        <asp:ListItem Value="22">ASR</asp:ListItem>
        <asp:ListItem Value="19">ABR</asp:ListItem>
        <asp:ListItem Value="20">ACD</asp:ListItem>
    </asp:CheckBoxList>
<%--<triggers>
<ASP:ASYNCPOSTBACKTRIGGER ControlID="rdbtnlType"></ASP:ASYNCPOSTBACKTRIGGER>
</triggers>--%>
</TD></TR></TBODY></TABLE>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table align="left" style="height: 100%;">
                        <tr>
                            <td style="width: 100px" valign=top align=left bgcolor="#d5e3f0">
                                <asp:Label ID="Label22" runat="server" Font-Bold="False"
                                    Text="Step 11 " CssClass="labelBlue8"></asp:Label></td>
                            <td style="width: 300px" valign=top align=left bgcolor="#d5e3f0">
                                <asp:Label ID="Label23" runat="server" Font-Bold="False" Font-Names="Arial" ForeColor="#004A8F"
                                    Text="Choose Date Format" Width="282px" Font-Size="8pt"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 100px; height: 21px" valign="top">
                                <asp:Label ID="Label24" runat="server" Font-Bold="False" Text="Date Format:" CssClass="labelSteelBlue"></asp:Label></td>
                            <td align="right" style="width: 300px; height: 21px" valign="top">
                                <asp:DropDownList ID="cmbDateFormat" runat="server" Width="300px" CssClass="dropdown">
                                    <asp:ListItem Value="101" Selected>mm/dd/yyyy</asp:ListItem>
                                    <asp:ListItem Value="103">dd/mm/yyyy</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 306px" valign="top" colspan="3" align="center">
                    <table>
                        <tr>
                            <td style="width: 100px" align="left">
                                <asp:CheckBox ID="ToExcel" runat="server" Text="To Excel" Visible="False" /></td>
                            <td style="width: 100px" align="left">
                    <asp:Button ID="btnSubmit" runat="server" Text="View report" OnClick="btnSubmit_Click" Font-Names="Arial" CssClass="boton" /></td>
                            <td style="width: 100px" align="left">
                                <asp:Button ID="btnReset" runat="server" OnClick="btnReset_Click"
                        Text="Reset" CssClass="boton" /></td>
                        </tr>
                    </table>                                                    
                    </td>
            </tr>
            <tr>
                <td width="100%" colspan="3">
                    <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
                </td>
            </tr>
            <tr>
                <td align="left" colspan="3">
                    <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label12" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
               </td>
            
            </tr>
            <tr>
                <td align="left" colspan="3">
                    </td>
            </tr>
            
        </table>            
       
        <asp:HiddenField ID="hfCustBroker" runat="server" />
        <asp:HiddenField ID="hfVendorBroker" runat="server" />
    
        <asp:SqlDataSource ID="sqldsFill" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
        </asp:SqlDataSource>
        <asp:HiddenField ID="hfRegion" runat="server" />
        <asp:HiddenField ID="txtFrom" runat="server" />
        <asp:HiddenField ID="txtTo" runat="server" />
        <asp:HiddenField ID="hfCust" runat="server" />
        <asp:HiddenField ID="hfVendor" runat="server" />
        <asp:HiddenField ID="hfCountry" runat="server" />
        <asp:SqlDataSource ID="sqldsCust" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT '**ALL**' AS Customer FROM CDRDB.dbo.ProviderIP UNION SELECT DISTINCT ProviderName AS Customer FROM CDRDB.dbo.ProviderIP AS ProviderIP_2 WHERE (Type = 'C') UNION SELECT 'NIP-CALLING-CARD' AS Customer  " ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT DISTINCT ProviderName AS Vendor FROM CDRDB.dbo.ProviderIP WHERE (Type LIKE 'V') AND (ProviderName <> 'OUTLANDER-AMIGO') AND (ProviderName <> 'OUTLANDER-CALL') AND (ProviderName <> 'OUTLANDER-MS') AND (ProviderName <> 'OUTLANDER-PLAN') UNION SELECT '**ALL**' AS Vendor UNION SELECT 'NIP-CALLING-CARD' AS Vendor">
        </asp:SqlDataSource>
    
    </div>
        <asp:SqlDataSource ID="sqldsCountry" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT '**ALL**' as Country UNION SELECT DISTINCT Country FROM CDRDB..Regions ORDER BY Country" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>
        <asp:SqlDataSource ID="sdldsCustBroker" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT  '**ALL**' as CustBroker UNION SELECT DISTINCT CustBroker FROM MECCA2..CBroker" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="sqldsVendorBroker" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" SelectCommand="SELECT  '**ALL**' as VendorBroker UNION SELECT DISTINCT VendorBroker  FROM MECCA2..VBroker" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
        </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsLMC" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT DISTINCT CASE WHEN ENTERPRISE IS NULL THEN LMC ELSE ENTERPRISE END LMC FROM MECCA2..LMC AS LMC_1 UNION SELECT '**Select LMC**' AS LMC FROM LMC AS LMC_1 ORDER BY LMC">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsType" runat="server" ConnectionString="<%$ ConnectionStrings:CDRDBConnectionString %>"
        SelectCommand="SELECT DISTINCT Type FROM Regions UNION SELECT '**Select Type**' AS Type FROM Regions AS Regions_1 ORDER BY Type">
    </asp:SqlDataSource>
    <br />
    <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtFromV" runat="server" PopupButtonID="Image1">
    </cc1:CalendarExtender>

    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToV" PopupButtonID="Image2">
    </cc1:CalendarExtender>
</asp:Content>

