<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="OperativeLeverage.aspx.cs" Inherits="OperativeLeverage" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Label ID="lblHeader" runat="server" CssClass="labelTurkH" Text="Operative Leverage"></asp:Label>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
        <table>
            <tr>
                <td style="width: 100px" valign="top" align="left">
                    <asp:Label ID="labelFrom" runat="server" Text="From:" Font-Bold="False" CssClass="labelSteelBlue" /></td>
                <td style="width: 300px" valign="top" align="left">
                    <asp:TextBox ID="txtFromV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                    &nbsp;&nbsp;<asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" />
                </td>
            </tr>
            <tr>
               <td style="width: 100px" valign="top" align="left">
                    <asp:Label ID="labelTo" runat="server" Text="To:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                <td style="width: 300px" valign="top" align="left">
                    <asp:TextBox ID="txtToV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                    &nbsp;&nbsp;<asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" />
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>                
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                Font-Names="Arial" CssClass="boton" />
                </td>
            </tr>
        </table>
        <br />        
        
        <asp:HyperLink ID="HyperLink1" runat="server" Visible="False" Font-Bold="True" Font-Names="Arial" Font-Size="11pt" ForeColor="#003399">Click to Download</asp:HyperLink>
    
    
    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromV"
        PopupButtonID="Image1">
    </cc1:CalendarExtender>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToV"
        PopupButtonID="Image2">
    </cc1:CalendarExtender>
</asp:Content>

