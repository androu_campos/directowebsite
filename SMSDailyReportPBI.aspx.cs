﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.IO;

public partial class SMSDailyReportPBI : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string postData = "";
        string year = "";
        string month = "";
        string day = "";
        int aux = 0;
        string responseText = "";

        string from = "";
        string to = "";
        year = DateTime.Now.AddDays(-1).Year.ToString();
        month = DateTime.Now.AddDays(-1).Month.ToString();
        day = DateTime.Now.AddDays(-1).Day.ToString();

        if (month.Length == 1)
            month = "0" + month;

        if (day.Length == 1)
            day = "0" + day;

        from = year + "-" + month + "-" + day;
        to = from;
       

        postData = HttpUtility.UrlEncode("fecha_inicio") + "="
                + HttpUtility.UrlEncode(from) + "&"
                + HttpUtility.UrlEncode("fecha_fin") + "="
                + HttpUtility.UrlEncode(to);

        
            postData += "&" + HttpUtility.UrlEncode("client") + "="
                     + HttpUtility.UrlEncode("VentaDirecta");
        

        byte[] data = Encoding.ASCII.GetBytes(postData);


        HttpWebRequest request = WebRequest.Create("http://sms.directo.com:1366/globalResume") as HttpWebRequest;
        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";
        request.ContentLength = data.Length;

        Stream requestStream = request.GetRequestStream();
        requestStream.Write(data, 0, data.Length);
        requestStream.Close();

        HttpWebResponse response = request.GetResponse() as HttpWebResponse;

        encoding = ASCIIEncoding.ASCII;
        using ( reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
        {
            responseText = reader.ReadToEnd();
        }

        Context.Response.Write(responseText);
        
    }

    private Encoding _encoding;
    private StreamReader reader;

    public Encoding encoding
    {
        get { return _encoding; }
        set { _encoding = value; }
    }
}