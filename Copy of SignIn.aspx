<%@ Page Language="C#" StylesheetTheme="Theme1" AutoEventWireup="true" CodeFile="SignIn.aspx.cs" Debug="true" Inherits="SignIn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>DIRECTO LOGIN</title>
</head>
<body background="Pag/bg.gif">
    <form id="form1" runat="server">
    <div align="center">
        <br />
        <br />
        <br />
        
        &nbsp;<br />                      
        <table border="7" id="loginTbl" cellspacing="0" cellpadding="0" style="width: 27%" bordercolor="white">
						<tr >
                            <td width="205" background="Pag/bg-log.gif">
                                <img border="0" src="Pag/imas/login/logo_talktel.png" width="221" height="253"></td>
                            <td valign="top" background="Pag/bg-log.gif">
                                <table border="0" width="100%" id="table4" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2" align="left">
                                            <img border="0" src="Pag/imas/login/dlogin.gif" width="277" height="59"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 109px; height: 29px;">
                                            <p style="margin-left: 23px; margin-top: 15px">
                                                <font color="#666666">
                                                    <asp:Label ID="Label2" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="#666666"
                                                        Text="Mecca Username:" Width="94px"></asp:Label></font></td>
                                        <td width="67%" style="height: 29px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="100%" colspan="2">
                                            <p style="margin-left: 23px; margin-top: 5px; margin-bottom: 10px" align="left">
                                                <asp:TextBox ID="txtUser" runat="server" Height="15px" Width="97px" BackColor="WhiteSmoke"
                                                    BorderColor="#E0E0E0" Font-Names="Arial" Font-Size="8pt"></asp:TextBox>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px; width: 109px;">
                                            <p style="margin-left: 23px">
                                                <font color="#666666">
                                                    <asp:Label ID="Label3" runat="server" Text="Mecca Password: " Font-Names="Arial" Font-Size="8pt" Width="94px"></asp:Label></font></td>
                                        <td width="67%" style="height: 19px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="100%" colspan="2">
                                            <p style="margin-left: 23px; margin-top: 5px; margin-bottom: 5px" align="left">
                                                <asp:TextBox ID="txtPassword" runat="server" Height="15px" Width="97px" TextMode="Password"
                                                    BackColor="WhiteSmoke" BorderColor="#E0E0E0" Font-Names="Arial" Font-Size="8pt"></asp:TextBox>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="100%" colspan="2" align="left">
                                            &nbsp; &nbsp;&nbsp;
                                            <asp:CheckBox ID="RememberMe" runat="server" Text="Remember me on this computer" Font-Size="8pt"
                                                ForeColor="SteelBlue" Font-Names="Arial" /></td>
                                    </tr>
                                    <tr>
                                        <td width="100%" colspan="2">
                                            <p style="margin-left: 23px; margin-top: 10px; margin-bottom: 10px" align="left">
                                                &nbsp;<asp:Label ID="lblWrongLog" runat="server" ForeColor="Red" Text="User/Password wrong"
                                                    Visible="False" Font-Size="Smaller" Font-Names="Arial"></asp:Label>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                    <td style="width: 109px">
                                    <p style="margin-left: 23px; margin-top: 10px; margin-bottom: 10px" align="left">
                                <asp:Button ID="LoginButton" runat="server" BackColor="White" BorderColor="#507CD1"
                                    BorderStyle="Solid" BorderWidth="1px" CommandName="Login" Font-Names="Arial"
                                    Font-Size="10pt" ForeColor="#284E98" OnClick="LoginButton_Click" Text="Login"
                                    ValidationGroup="Login1" /></td>
                                    </tr>
                                </table>
                                </td>
                        </tr>
					</table>
        &nbsp; &nbsp;&nbsp;&nbsp;<br />
    
    </div>
    </form>
</body>
</html>
