using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;

public partial class UpdateDialPlan : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        bool isLoaded = false;
        string dialPlan = string.Empty;
        string filePath = string.Empty;

        dialPlan = "DialPlan";

        File.Delete(@"E:\Mecca\Files\dialplan\" + dialPlan + ".zip");
        File.Delete(@"E:\Mecca\Files\dialplan\" + dialPlan + ".rar");

        try
        {
            filePath = FU.FileName.ToString();
            Session["filePath"] = filePath;

            if (!FU.HasFile)
            {
                lblUpload.Visible = true;
                lblUpload.Text = "File not loaded! Please add a File";
            }

            if (filePath == "DialPlan.rar" || filePath == "DialPlan.zip")
            {
                isLoaded = true;
                FU.SaveAs(@"E:\Mecca\Files\dialplan\" + FU.FileName.ToString());
                lblUpload.Visible = true;
                lblUpload.Text = "File " + FU.FileName.ToString() + " loaded succesfully";
            }
            else if (FU.HasFile)
            {
                lblUpload.Visible = true;
                lblUpload.Text = " This File can't be loaded! Please check File Name or File Extension ";                
            }

            if (isLoaded)
            {
                SqlCommand sqlSpQuery = new SqlCommand();
                sqlSpQuery.CommandType = CommandType.StoredProcedure;
                sqlSpQuery.CommandText = "sp_DialPlanRunDTSX";

                DataSet resultSet;
                resultSet = RunQuery(sqlSpQuery);

                if (resultSet.Tables.Count > 0)
                {

                    if (resultSet.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row in resultSet.Tables[1].Rows)
                        {
                            string coo;
                            coo = row["corre"].ToString();
                            if (coo != "0") lblUpload.Text = "Failed!!! Please contact IT";
                            else
                                lblUpload.Text = "<b>Completed, the LCR will be send to your inbox soon as possible</b>";
                        }
                    }
                }
                
            }


        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }

    }

    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString =
            ConfigurationManager.ConnectionStrings
            ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        sqlQuery.CommandTimeout = 666;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
            //labelStatus.Text = "Unable to connect to SQL Server.";
        }
        return resultsDataSet;
    }
}
