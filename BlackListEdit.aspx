<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    Theme="Theme1" AutoEventWireup="true" CodeFile="BlackListEdit.aspx.cs" Inherits="BlackListEdit"
    Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
<!--
    function Check_Click(objRef) {
        //Get the Row based on checkbox
        var row = objRef.parentNode.parentNode;

        //Get the reference of GridView
        var GridView = row.parentNode;

        //Get all input elements in Gridview
        var inputList = GridView.getElementsByTagName("input");

        for (var i = 0; i < inputList.length; i++) {
            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];

            //Based on all or none checkboxes
            //are checked check/uncheck Header Checkbox
            var checked = true;
            if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                if (!inputList[i].checked) {
                    checked = false;
                    break;
                }
            }
        }
        headerCheckBox.checked = checked;

    }
    function checkAll(objRef) {
        var GridView = objRef.parentNode.parentNode.parentNode;
        var inputList = GridView.getElementsByTagName("input");
        for (var i = 0; i < inputList.length; i++) {
            //Get the Cell To find out ColumnIndex
            var row = inputList[i].parentNode.parentNode;
            if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                if (objRef.checked) {
                    //If the header checkbox is checked
                    //check all checkboxes
                    //and highlight all rows
                    inputList[i].checked = true;
                }
                else {
                    //If the header checkbox is checked
                    //uncheck all checkboxes
                    //and change rowcolor back to original 
                    inputList[i].checked = false;
                }
            }
        }
    }
    -->
</script>
    <table>
        <tr>
            <td>
                <asp:Button ID="lnkTab1" CssClass="clicked" Text="    Upload" runat="server" OnClick="lnkTab1_Click" />
            </td>
            <td>
                <asp:Button ID="lnkTab2" CssClass="unclicked" Text="      Edit" runat="server" OnClick="lnkTab2_Click" />
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="width: 200px">
                <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Number:" />
                <asp:TextBox ID="txtSearch" runat="server" CssClass="textBox" Width="80px" />
                <asp:Button ID="btnSearch" runat="server" Width="50px" CssClass="boton" Text="Search" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnDelete" runat="server" Width="90px" CssClass="boton" Text="Delete Selected" />
            </td>
            <td style="width: 50px">
            </td>
            <td style="width: 200px">
                <asp:Label ID="lbl1" runat="server" CssClass="labelBlue8" Text="Total Records:"></asp:Label>
                <asp:TextBox ID="txtCount" runat="server" CssClass="textBox" Enabled="false" Width="20px"
                    TextAlignment="Center"></asp:TextBox>
                <asp:SqlDataSource ID="sqlsacount" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListConnString %>"
                    ProviderName="<%$ ConnectionStrings:BlackListConnString.ProviderName %>" SelectCommand="SELECT COUNT(NumB)Count FROM fn_blacknumb_BCONNECT_SECCIONAMARILLA">
                </asp:SqlDataSource>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="width: 103px" valign="top">
                
                        <asp:GridView ID="gvGood" runat="server" Visible="False" Font-Names="Arial" Font-Size="8pt"
                            AllowPaging="true" PageSize="25" DataSourceID="sqlsaGood" OnPageIndexChanging = "OnPaging">
                            <Columns>
                                <asp:TemplateField>
                                <HeaderTemplate>
                                <asp:CheckBox ID="chbxselAll" runat="server" onclick="checkAll(this);"/>
                                </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chbxsel" runat="server" onclick="Check_Click(this);" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="false" />
                            <EditRowStyle BackColor="#2461BF" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                                Font-Size="8pt" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    
                <asp:SqlDataSource ID="sqlsaGood" runat="server" ConnectionString="<%$ ConnectionStrings:BlackListConnString %>"
                    ProviderName="<%$ ConnectionStrings:BlackListConnString.ProviderName %>" SelectCommand="SELECT NumB as Number, cast(Date_add as VARCHAR(11))as Date FROM fn_blacknumb_BCONNECT_SECCIONAMARILLA"
                    FilterExpression="NumB = {0}">
                    <FilterParameters>
                        <asp:ControlParameter ControlID="txtSearch" Name="NumB" PropertyName="Text" />
                    </FilterParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
