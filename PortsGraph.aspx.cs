using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;
using System.Data.SqlClient;


public partial class PortsGraph : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string user = "";

        user = Session["DirectoUser"].ToString();

        
        
        if (!Page.IsPostBack)
        {
            fillddlProvider(user);

            txtFrom.Text = DateTime.Now.ToShortDateString();
            txtTo.Text = DateTime.Now.ToShortDateString();
            if (Request.QueryString.Count > 0)
            {
                string provider = Request.QueryString["P"].ToString();
                string query = string.Empty;
                string title = string.Empty;
                DataTable tbl;
                string from = Request.QueryString["dF"].ToString();
                string to = Request.QueryString["dT"].ToString();
                from = from.Replace("_", "/");
                to = to.Replace("_", "/");

                if (provider == "**ALL**")
                        provider = "%";


                if (provider == "A_D-WORLDWIDE")
                {
                    provider = "A&D-WORLDWIDE";
                }


                if (Request.QueryString["T"].ToString() == "0") //Customer
                {
                    if (provider == "TALKTEL")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in (select ip from cdrdb.dbo.providerip where providername in (select customer from cdrdb.dbo.custbrokerrel where custbroker = 'CallCenters') and type = 'C')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-ALL" || provider == "ICS")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in (select ip from cdrdb..providerip where providername like 'ICS%' and len(ip) = 4 and type = 'C')  and [type] like 'C%' AND Cust_Vendor in ('DIDB','DIDN','DIDP','DIDV','INET','DIDA') group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-RELOADABLE")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('IPRE','IPRU','DIP5','DIN5')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-RELOADABLE-2")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('IPUC','DIP2','DIN2')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-ALL-CARDS")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('ICSI','I800','800P','800T','800S','IWEB','ICSP','IPHO','ITSI','ICSL','IUNL')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-PAC-WEST")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIDP')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-PAC-WEST-5")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIP5')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-PAC-WEST-2")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIP2')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-NUVOX-5")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIN5')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-NUVOX")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIDN')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-CARDS-2DLS")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('ICS2')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-CARDS-5DLS")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('ICS5')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-VOICE-STEP")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIDV')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-BLITZ")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIDB')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-NETCOMM")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('INET')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "WEB-VOIP")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('PWVV')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS020") // "ALLACCESS-NETCOMM")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('ANCM')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS032") // "ALLACCESS-UNL-PASSIP")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUPS')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS034") // "ALLACCESS-UNLPASSTDM")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUPT')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    //else if (provider == "ALLACCESS-UNL-SMART")
                    //{
                    //    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUSM')  and [type] like 'C%' group by Time ORDER BY Time";
                    //}
                    else if (provider == "ALLACCESS026" ) // "ALLACCESS-UNL-CANSMA")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUSM')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS036" ) //"ALLACCESS-UNL-USASMA")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUSA')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS037" ) // "ALLACCESS-UNL-USASMT")
                     {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUMT')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS033") // "ALLACCESS-UNLPASSIPB")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUPB')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "RC-GUATEMALA")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('RCGT')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS029") // "ALLACCESS-UNLI-SMART")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('ASMA')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS028") // "ALLACCESS-UNLI-PLDT")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('APLD')  and [type] like 'C%' group by Time ORDER BY Time";
                    }

                    else
                    {
                        if ( provider != "%")
                        {
                            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' AND [Live_Calls] > 0 group by [Time] order by [Time]";
                        }
                        else
                        {
                            //if (user != "jim.baumhart")
                            //{
                                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Cust_Vendor] <> 'CTEL' and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' AND [Live_Calls] > 0 group by [Time] order by [Time]";
                            //}
                            //else
                            //{
                            //    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName in (select customer from cdrdb.dbo.custbrokerrel where custbroker = 'Jim' ) and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                            //}
                        }
                    }

                    //Response.Write(query);
                    //Response.End();

                    tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                    if (provider == "%") provider = "ALL";
                    title = "Customer: " + provider;
                    
                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;
                }
                else//Vendor
                {
                   
                    if (provider == "AMERICATEL" || provider == "AMERICATEL-PERU" || provider == "CELL-TERMINATION" ||
                             provider == "CONVERGIA" || provider == "IBASIS" || provider == "INTER-CONNECTION-LLC" ||
                             provider == "KEEN-OCEAN" || provider == "NETSERVICE" || provider == "NEXTCOM" ||
                             provider == "NEXTCOM-OFFNET" || provider == "TELEFONICA-BRASIL" || provider == "TELWISE" ||
                             provider == "TRANSITAR" || provider == "NATIONAL" || provider == "NATIONAL-NEXT" ||
                             provider == "NATIONAL-OFF" || provider == "NATIONAL-TELULAR" || provider == "OUTLANDER" || provider == "OUTLANDER-CALL" ||
                             provider == "OUTLANDER-CALL2" || provider == "OUTLANDER-CALL3" || provider == "OUTLANDER-GUATE-CLAR" ||
                             provider == "OUTLANDER-ICS" || provider == "OUTLANDER-MS" || provider == "OUTLANDER-POSPAY-ICS" || provider == "OUTLANDER-POSPREMIUM" || provider == "OUTLANDER-POSPRE-ICS")
                    {

                        if (provider == "OUTLANDER")
                        {

                            //query = "SELECT SUM(CALLS) as Live_Calls,[Time] FROM (SELECT SUM(CALLS) AS CALLS, [TIME] FROM tmpLiveCallsTraffic WHERE VENDORTAG IN  ( 'OLDR', 'OLAM', 'BSTI', 'OLBR','OLCL','OLC2','OLC3','OLEC','OLFX','OGCL','OGMS','OGTI','OCIC','OLMS','OLPL','OLPH','OLPO','OLPR') AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] UNION SELECT SUM(CALLS) AS CALLS, [TIME] FROM TrafficLogLive WHERE VENDOR in ( select PROVIDERNAME from cdrdb..providerip where ProviderName like 'OUTLANDER%' and [Type] like 'V%') AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] ) A group by [Time] order by [Time] ";
                            query = "select isnull(SUM(Live_Calls),0)  as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername like 'OUTLANDER%' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time] order by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                        }
                        else if(provider == "OUTLANDER-POSPAY-ICS")
                        {
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('OUTLANDER-POSPAY','OUTLANDER-POSPAY-ICS') and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else if (provider == "OUTLANDER-POSPREMIUM" || provider == "OUTLANDER-POSPRE-ICS")
                        {
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('OUTLANDER-POSPREMIUM','OUTLANDER-POSPRE-ICS') and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else if(provider == "OUTLANDER-CALL" || provider == "OUTLANDER-CALL2" || provider == "OUTLANDER-CALL3" || provider == "OUTLANDER-GUATE-CLAR" || provider == "OUTLANDER-ICS" || provider == "OUTLANDER-MS" )
                        {
                            //query = "select SUM(Live_Calls) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername like '" + provider + "' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdrn where Cust_Vendor in (select ip from cdrdb..providerip where providername like '" + provider + "' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else if (provider == "CONVERGIA")
                        {
                            //query = "SELECT SUM(CALLS) as Live_Calls,[Time] FROM (SELECT SUM(CALLS) AS CALLS, [TIME] FROM cdrdb..cdrn WHERE VENDOR LIKE '" + provider + "' AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] UNION SELECT SUM(CALLS) AS CALLS, [TIME] FROM TrafficLogLive WHERE VENDOR LIKE '" + provider + "' AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] ) A group by [Time] order by [Time] ";
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdrn where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('" + provider + "') and len(ip) = 4 and type like 'V') and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmnt(query).Tables[0];
                        }
                        else
                        {
                            //query = "SELECT SUM(CALLS) as Live_Calls,[Time] FROM (SELECT SUM(CALLS) AS CALLS, [TIME] FROM cdrdb..cdrn WHERE VENDOR LIKE '" + provider + "' AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] UNION SELECT SUM(CALLS) AS CALLS, [TIME] FROM TrafficLogLive WHERE VENDOR LIKE '" + provider + "' AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] ) A group by [Time] order by [Time] ";
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdrn where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('" + provider + "') and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmnt(query).Tables[0];
                        }

                    }
                    else if (provider == "TLK-ALL")
                    {
                        query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdrn where Cust_Vendor in (select ip from cdrdb..providerip where providername like 'TLK%' and len(ip) = 4 and type = 'V' UNION SELECT 'DGTK' AS IP) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                    }
                    else if (provider == "TLK-TELCEL-DF" || provider == "TLK-TELCEL-DF-CAP" || provider == "TLK-TELCEL-DF-CAP2" || provider == "TLK-TELCEL-ROC" || provider == "TLK-TELCEL-ROC-CAP" || provider == "TLK-TELCEL-ROC-CAP-2" || provider == "TLK-TELCEL-ROC-UNLI" || provider == "TLK-TELMEX-DF" || provider == "TLK-TELMEX-ROC" || provider == "TLK-TEMPORAL-ROC" || provider == "TLKTLC-TELCEL-IP")
                    {
                        query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdrn where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('" + provider + "') and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                        tbl = Util.RunQueryByStmnt(query).Tables[0];
                    }
                    else if (provider == "TLK-TMX-ALL")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Live_Calls] > 0 AND Cust_Vendor in (select ip from cdrdb.dbo.providerip where providername like 'TLK-%' AND TYPE = 'V' UNION SELECT 'DGTK' AS IP) AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by Time ORDER BY Time";
                        tbl = Util.RunQueryByStmnt(query).Tables[0];
                    }
                    else
                    {
                        if (provider != "%")
                        {
                            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'V%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'V%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else
                        {
                            //if (user != "jim.baumhart")
                            //{
                                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'V%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'V%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                                tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                            //}
                            //else
                            //{
                            //    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'V%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName in (select vendor from cdrdb.dbo.vendorbrokerrel where vendorbroker = 'Jim' ) and [Type] like 'V%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' and ID IN('NY03','MIA04','MIA02','STC01','MIA08','MIA01','STCB','MIA06','MIA03','MEX') group by [Time] order by [Time]";
                            //    tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                            //}
                        }
                        
                        
                    }
                    
                    

                    if (provider == "%") provider = "ALL";
                    title = "Vendor: " + provider;
                    
                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;
                }

                makeChart(tbl, title);
                //gdvDetail.Visible = false;
                lblGrid.Visible = false;

                txtFrom.Text = from;
                txtTo.Text = to;
                dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
                dpdType.SelectedIndex = Convert.ToInt32(Request.QueryString["T"]);
                cmdDetail.Visible = true;

            }
        
        }
        else
        {
            
            if (Request.QueryString.Count > 0)
            {
                string provider = Request.QueryString["P"].ToString();
                string query = string.Empty;
                string title = string.Empty;
                DataTable tbl;
                string from = txtFrom.Text;//Request.QueryString["dF"].ToString();
                string to = txtTo.Text;//Request.QueryString["dT"].ToString();
                from = from.Replace("_", "/");
                to = to.Replace("_", "/");

                if (provider == "**ALL**") provider = "%";

                if (provider == "A_D-WORLDWIDE")
                {
                    provider = "A&D-WORLDWIDE";
                }

                if (Request.QueryString["T"].ToString() == "0") //Customer
                {
                    //query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";

                    if (provider == "TALKTEL")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in (select ip from cdrdb.dbo.providerip where providername in (select customer from cdrdb.dbo.custbrokerrel where custbroker = 'CallCenters') and type = 'C')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-ALL" || provider == "ICS")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in (select ip from cdrdb..providerip where providername like 'ICS%' and len(ip) = 4 and type = 'C')  and [type] like 'C%' AND Cust_Vendor in ('DIDB','DIDN','DIDP','DIDV','INET','DIDA') group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-RELOADABLE")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('IPRE','IPRU','DIP5','DIN5')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-RELOADABLE-2")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('IPUC','DIP2','DIN2')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-ALL-CARDS")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('ICSI','I800','800P','800T','800S','IWEB','ICSP','IPHO','ITSI','ICSL','IUNL')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-PAC-WEST")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIDP')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-PAC-WEST-5")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIP5')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-PAC-WEST-2")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIP2')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-NUVOX-5")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIN5')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-NUVOX")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIDN')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-CARDS-2DLS")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('ICS2')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-CARDS-5DLS")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('ICS5')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-VOICE-STEP")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIDV')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-BLITZ")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DIDB')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ICS-NETCOMM")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('INET')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS020") //"ALLACCESS-NETCOMM")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('ANCM')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS032") //"ALLACCESS-UNL-PASSIP")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUPS')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS034") // "ALLACCESS-UNLPASSTDM")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUPT')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    //else if (provider == "ALLACCESS-UNL-SMART")
                    //{
                    //    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUSM')  and [type] like 'C%' group by Time ORDER BY Time";
                    //}
                    else if (provider == "ALLACCESS026") //"ALLACCESS-UNL-CANSMA")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUSM')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS036") // "ALLACCESS-UNL-USASMA")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUSA')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS037") //"ALLACCESS-UNL-USASMT")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUMT')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS033") //"ALLACCESS-UNLPASSIPB")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('AUPB')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "RC-GUATEMALA")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('RCGT')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS029") //"ALLACCESS-UNLI-SMART")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('ASMA')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "ALLACCESS028") //"ALLACCESS-UNLI-PLDT")
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('APLD')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else if (provider == "DID-VOICESTEP-HAWAII") //Agrego AJCM
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' and [Live_Calls] > 0   AND Cust_Vendor in ('DVHS')  and [type] like 'C%' group by Time ORDER BY Time";
                    }
                    else
                    {
                        if (provider != "%")
                        {
                            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' AND [Live_Calls] > 0  group by [Time] order by [Time]";
                        }
                        else
                        {
                            //if (user != "jim.baumhart")
                            //{
                                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'C%') and [Cust_Vendor] <> 'CTEL' and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' AND [Live_Calls] > 0 group by [Time] order by [Time]";
                            //}
                            //else
                            //{
                            //    query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName in (select customer from cdrdb.dbo.custbrokerrel where custbroker = 'Jim' ) and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                            //}
                        }
                    }
                    

                    tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                    if (provider == "%") provider = "ALL";
                    title = "Customer: " + provider;

                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;
                }
                else//Vendor
                {
                    if (provider == "AMERICATEL" || provider == "AMERICATEL-PERU" || provider == "CELL-TERMINATION" ||
                             provider == "CONVERGIA" || provider == "IBASIS" || provider == "INTER-CONNECTION-LLC" ||
                             provider == "KEEN-OCEAN" || provider == "NETSERVICE" || provider == "NEXTCOM" ||
                             provider == "NEXTCOM-OFFNET" || provider == "TELEFONICA-BRASIL" || provider == "TELWISE" ||
                             provider == "TRANSITAR" || provider == "NATIONAL" || provider == "NATIONAL-NEXT" ||
                             provider == "NATIONAL-OFF" || provider == "NATIONAL-TELULAR" || provider == "OUTLANDER" || provider == "OUTLANDER-CALL" ||
                             provider == "OUTLANDER-CALL2" || provider == "OUTLANDER-CALL3" || provider == "OUTLANDER-GUATE-CLAR" ||
                             provider == "OUTLANDER-ICS" || provider == "OUTLANDER-MS" || provider == "OUTLANDER-POSPAY-ICS" || provider == "OUTLANDER-POSPREMIUM" || provider == "OUTLANDER-POSPRE-ICS")
                    {

                        if (provider == "OUTLANDER")
                        {

                            //query = "SELECT SUM(CALLS) as Live_Calls,[Time] FROM (SELECT SUM(CALLS) AS CALLS, [TIME] FROM tmpLiveCallsTraffic WHERE VENDORTAG IN  ( 'OLDR', 'OLAM', 'BSTI', 'OLBR','OLCL','OLC2','OLC3','OLEC','OLFX','OGCL','OGMS','OGTI','OCIC','OLMS','OLPL','OLPH','OLPO','OLPR') AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] UNION SELECT SUM(CALLS) AS CALLS, [TIME] FROM TrafficLogLive WHERE VENDOR in ( select PROVIDERNAME from cdrdb..providerip where ProviderName like 'OUTLANDER%' and [Type] like 'V%') AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] ) A group by [Time] order by [Time] ";
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername like 'OUTLANDER%' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

                        }
                        else if (provider == "OUTLANDER-POSPAY-ICS")
                        {
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('OUTLANDER-POSPAY','OUTLANDER-POSPAY-ICS') and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else if (provider == "OUTLANDER-POSPREMIUM" || provider == "OUTLANDER-POSPRE-ICS")
                        {
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername in ('OUTLANDER-POSPREMIUM','OUTLANDER-POSPRE-ICS') and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else if (provider == "OUTLANDER-CALL" || provider == "OUTLANDER-GUATE-CLAR" || provider == "OUTLANDER-ICS" || provider == "OUTLANDER-MS" || provider == "OUTLANDER-CALL2" || provider == "OUTLANDER-CALL3")
                        {
                            //query = "select SUM(Live_Calls) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername like '" + provider + "' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            query = "select isnull(SUM(Live_Calls),0) as Live_Calls, [Time] from cdrdb..cdnVendorsDaily where Cust_Vendor in (select ip from cdrdb..providerip where providername like '" + provider + "' and len(ip) = 4) and [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' group by [Time]";
                            tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                        }
                        else
                        {
                            query = "SELECT SUM(CALLS) as Live_Calls,[Time] FROM (SELECT SUM(CALLS) AS CALLS, [TIME] FROM tmpLiveCallsTraffic WHERE VENDOR LIKE '" + provider + "' AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] UNION SELECT SUM(CALLS) AS CALLS, [TIME] FROM TrafficLogLive WHERE VENDOR LIKE '" + provider + "' AND [Time] >= '" + from + " 00:00:00' and [Time] <= '" + to + " 23:59:59' GROUP BY [TIME] ) A group by [Time] order by [Time] ";
                            tbl = Util.RunQueryByStmnt(query).Tables[0];
                        }


                    }
                    else
                    {
                        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'V%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + provider + "' and [Type] like 'V%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];
                    }

                    if (provider == "%") provider = "ALL";
                    title = "Vendor: " + provider;

                    if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
                    else cmdDetail.Visible = true;
                }

                makeChart(tbl, title);
                //gdvDetail.Visible = false;
                lblGrid.Visible = false;

                txtFrom.Text = from;
                txtTo.Text = to;

                //dpdProvider.SelectedIndex = Convert.ToInt32(Request.QueryString["S"]);
            }
        
        }

    }

    private void fillddlProvider(string user)
    {
        dpdProvider.Items.Clear();

        string cmd = string.Empty;

        //if (user != "jim.baumhart")
        //{
            cmd = "SELECT DISTINCT PROVIDERNAME AS Customer INTO #CustomerList FROM CDRDB.DBO.PROVIDERIP WHERE [TYPE] IN ('C','V') \n ";
            cmd += "INSERT INTO #CustomerList SELECT '**ALL**' \n ";
            cmd += "INSERT INTO #CustomerList SELECT '** NONE **' \n ";
            cmd += "INSERT INTO #CustomerList SELECT 'TLK-ALL' \n ";
            cmd += "INSERT INTO #CustomerList SELECT 'TLK-TMX-ALL' \n ";
            cmd += "UPDATE #CustomerList SET Customer = 'ICS' WHERE Customer like 'ICS%' \n ";
            cmd += "UPDATE #CustomerList SET Customer = 'TALKTEL' WHERE Customer IN (SELECT CUSTOMER FROM CDRDB.DBO.CUSTBROKERREL WHERE CUSTBROKER = 'CallCenters' AND CUSTOMER NOT LIKE 'CCPH%' AND CUSTOMER NOT LIKE 'CC-PHIP%' AND CUSTOMER NOT IN ('ALLACCESS-CCPH06','DIRECTO-TK-DIGITAL') ) \n ";
            cmd += "SELECT DISTINCT Customer FROM #CustomerList \n ";
        //}
        //else
        //{
        //    cmd = "SELECT DISTINCT PROVIDERNAME AS Customer FROM CDRDB.DBO.PROVIDERIP ";
        //    cmd += "join cdrdb.dbo.custbrokerrel on providername = Customer ";
        //    cmd += "WHERE [TYPE] IN ('C') AND custbroker = 'Jim' ";
        //    cmd += "UNION SELECT DISTINCT PROVIDERNAME AS Customer FROM CDRDB.DBO.PROVIDERIP ";
        //    cmd += "join cdrdb.dbo.vendorbrokerrel on providername = Vendor ";
        //    cmd += "WHERE [TYPE] IN ('V') And vendorbroker = 'Jim' ";
        //    cmd += "UNION SELECT '**ALL**' ";
        //    cmd += "UNION SELECT '** NONE **' ";
        //    cmd += "UNION SELECT 'KDDI-GLOBAL' ";
        //    cmd += "UNION SELECT 'QWEST' ";
        //    cmd += "UNION SELECT 'ULTRA-MOBILE' ";
        //    cmd += "UNION SELECT 'VONAGE' ";
        //    cmd += "UNION SELECT 'NOBEL' ";
        //    cmd += "UNION SELECT 'ALKA-IP' ";
        //}

        SqlCommand SqlCommand1 = new SqlCommand();
        SqlCommand1.CommandText = cmd;
        DataSet dataSet = RunQuery(SqlCommand1);

        DataTable dataTable = dataSet.Tables[0];

        for (int i = 0; i < dataTable.Rows.Count; i++)
        {
            dpdProvider.Items.Add(dataTable.Rows[i][0].ToString());
        }
    }

    protected void cmdReport_Click(object sender, EventArgs e)
    {

        string from = txtFrom.Text;
        string to = txtTo.Text;

        from = from.Replace("/", "_");
        to = to.Replace("/", "_");

        if (dpdType.SelectedIndex == 0)//Customer
        {
            if (dpdProvider.SelectedValue.ToString() == "A&D-WORLDWIDE")
            {
                string c = dpdProvider.SelectedValue.ToString();
                c = c.Replace("&", "_");
                Response.Redirect("PortsGraph.aspx?P=" + c + "&T=0&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
            }
            else
            {
                Response.Redirect("PortsGraph.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=0&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);
            }
        }
        else                             //Vendor
            Response.Redirect("PortsGraph.aspx?P=" + dpdProvider.SelectedValue.ToString() + "&T=1&S=" + dpdProvider.SelectedIndex + "&dF=" + from + "&dT=" + to);


    }

    private void makeChart(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart1.Titles.Add(tITLE);
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetail"] = tbl;

            this.Chart1.Visible = true;
            lblWarning.Visible = false;
            this.cmdDetail.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            lblWarning.Visible = true;
            Session["tblDetail"] = tbl;
            this.cmdDetail.Visible = false;

        }

    }

    protected void cmdDetail_Click(object sender, EventArgs e)
    {
        gdvDetail.DataSource = ((DataTable)Session["tblDetail"]);
        gdvDetail.DataBind();

        gdvDetail.Visible = true;
        lblGrid.Visible = true;
        lblGrid.Text = dpdProvider.SelectedValue.ToString();
        cmdHide.Visible = true;
        cmdDetail.Visible = false;

    }

    protected void cmdHide_Click(object sender, EventArgs e)
    {
        gdvDetail.Visible = false;
        lblGrid.Visible = false;
        cmdDetail.Visible = true;
        cmdHide.Visible = false;

    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
