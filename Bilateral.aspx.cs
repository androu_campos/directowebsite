using System;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;

public partial class SWAP : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFrom.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();

            SqlConnection SqlConn1 = new SqlConnection();
            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";

            string sql = "SELECT Provider, Period, InRevenue, OutRevenue, EXPECPROFIT [Expected Profit], REALPROFIT [Real Profit], convert(varchar(11),Date) Date, Status, Comments from [MECCA2].[dbo].[BilateralAgreements] ";
                   sql += "union ";
                   sql += "SELECT 'TOTAL', '', sum(INREVENUE), sum(OUTREVENUE), sum(EXPECPROFIT), sum(REALPROFIT), '', '','' from [MECCA2].[dbo].[BilateralAgreements] ";

           SqlCommand sqlSpQuery = new SqlCommand();
           sqlSpQuery.CommandType = System.Data.CommandType.Text;
           sqlSpQuery.CommandText = sql;
           DataSet ResultSet;
           ResultSet = Util.RunQuery(sqlSpQuery);

           Session["qbuildercc"] = ResultSet;

           int jr = ResultSet.Tables[0].Rows.Count;

           if (jr <= 0)
           {
               //btnExcel.Visible = false;
           }
           else
           {
               this.gdvData.AutoGenerateColumns = false;
               this.gdvData.DataSource = ResultSet;
               this.gdvData.Columns.Clear();
               for (int c = 0; c < ResultSet.Tables[0].Columns.Count; c++)
               {
                   string columnName = ResultSet.Tables[0].Columns[c].ColumnName.ToString();
                   BoundField columna = new BoundField();
                   columna.HtmlEncode = false;
                   if (columnName == "Period")
                   {
                       columna.ItemStyle.Width = 120;
                       columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                   }
                   else if (columnName == "Comments")
                   {
                       columna.ItemStyle.Width = 300;
                       columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
                   }
                   else if (columnName == "Date")
                   {
                       columna.ItemStyle.Width = 100;
                       columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                   }
                   else if (columnName == "Status")
                   {
                       columna.ItemStyle.Width = 75;
                       columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                   }
                   else if (columnName == "InRevenue")
                   {
                       columna.DataFormatString = "{0:c0}";
                       columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                       columna.ItemStyle.Width = 75;
                       columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                   }
                   else if (columnName == "OutRevenue")
                   {
                       columna.DataFormatString = "{0:c0}";
                       columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                       columna.ItemStyle.Width = 75;
                       columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                   }
                   else if (columnName == "Expected Profit")
                   {
                       columna.DataFormatString = "{0:c0}";
                       columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                       columna.ItemStyle.Width = 75;
                       columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                   }
                   else if (columnName == "Real Profit")
                   {
                       columna.DataFormatString = "{0:c0}";
                       columna.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                       columna.ItemStyle.Width = 75;
                       columna.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                   }

                   columna.DataField = columnName;
                   columna.HeaderText = columnName;

                   gdvData.Columns.Add(columna);
                   gdvData.DataBind();
               }
           }

           //gdvData.DataSource = ResultSet;
           //gdvData.DataBind();
           gdvData.Visible = true;


           sqlSpQuery.CommandText = "SELECT Provider, Period from [MECCA2].[dbo].[BilateralAgreements] ";

           sqlSpQuery.Connection = SqlConn1;
           SqlConn1.Open();
           SqlDataReader myReader1 = sqlSpQuery.ExecuteReader();
           while (myReader1.Read())
           {
               ddlCust.Items.Add(myReader1.GetValue(0).ToString());
               ddlPeriod.Items.Add(myReader1.GetValue(1).ToString());
           }
           myReader1.Close();
           SqlConn1.Close();

        }
    }
    protected void SubmitClick(object sender, EventArgs e)
    {
        SqlConnection SqlConn1 = new SqlConnection();
        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";

        //string sql = "select ProviderName from cdrdb.dbo.providerip where type = '" + type + "' and providername not in (select customer from mecca2.dbo.controlcustomer) and providername not like 'ICS-%' and providername not like 'CC-%' order by ProviderName; ";
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlType.SelectedValue != "Choose One")
        {
            ddlClient.Items.Clear();
            ddlClient.Enabled = true;
            string type = ddlType.Text.ToString();

            SqlConnection SqlConn1 = new SqlConnection();
            SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";

            string sql = "select ProviderName from cdrdb.dbo.providerip where type = '" + type + "' and providername not in (select customer from mecca2.dbo.controlcustomer) and providername not like 'ICS-%' and providername not like 'CC-%' order by ProviderName; ";

            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = System.Data.CommandType.Text;
            sqlSpQuery.CommandText = sql;
            sqlSpQuery.Connection = SqlConn1;

            SqlConn1.Open();
            SqlDataReader myReader1 = sqlSpQuery.ExecuteReader();

            while (myReader1.Read())
            {
                ddlClient.Items.Add(myReader1.GetValue(0).ToString());
            }
            myReader1.Close();
            SqlConn1.Close();
        }
        else
        {
        //    ddlClient.Items.Clear();
        //    ddlClient.Items.Add("Choose Type");
            ddlClient.Enabled = false;
        }
    }
    protected void gdvData_PageIndexChanging (object sender, EventArgs e)
    {

    }
}
