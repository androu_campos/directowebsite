<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Showsalesmpop1.aspx.cs" Inherits="Showsalesmpop1" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="Label1" runat="server" CssClass="labelTurkH" Text="OFFNET MINIPOP RESULTS"></asp:Label>
    <table align="left" width="100%" style="position:absolute; top:220px; left:230px;" >
        <tr>
            <td align="left" style="width: 100px">
                <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="Button1_Click"
                    Text="Export to EXCEL" Width="92px" /></td>
        </tr>
        <tr>
            <td align="right" style="width: 100px; height: 26px">
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
    <asp:GridView ID="gdvShow" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" CellPadding="4" ForeColor="#333333" GridLines="None" Font-Size="9pt" Font-Names="Arial" AllowPaging="True" PageSize="25" OnPageIndexChanging="gdvShow_PageIndexChanging" Width="100%" CssClass="GridView">
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <FooterStyle BackColor="Navy" Font-Bold="True" ForeColor="White" />
        <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px" Font-Size="8pt" />
        <AlternatingRowStyle BackColor="White" />              
        <Columns>
        <asp:BoundField HtmlEncode="False" DataField="BillingDate" HeaderText="BillingDate">
          <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="190px" />
            <HeaderStyle HorizontalAlign="Left" Width="190px" />
        </asp:BoundField>
        <asp:BoundField HtmlEncode="False" DataField="Customer" HeaderText="Customer" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" Width="100px" />
        </asp:BoundField>
        <asp:BoundField  HtmlEncode="False" DataField="Vendor" HeaderText="Vendor" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="150px" />
            <HeaderStyle HorizontalAlign="Left" Width="150px" />
        </asp:BoundField>
        <asp:BoundField DataField="Region" HeaderText="Region">
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="130px" />
            <HeaderStyle HorizontalAlign="Left" Width="130px" />
        </asp:BoundField>
        <asp:BoundField DataField="CustPrefix" HeaderText="CustPrefix" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="Calls" HeaderText="AnsweredCalls" HtmlEncode="False" DataFormatString="{0:0,0}" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="CMinutes" HeaderText="Cminutes" HtmlEncode="False" DataFormatString="{0:0,0.00}" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="Cost" HeaderText="Cost"  HtmlEncode="False" DataFormatString="{0,00:C}">
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="price" HeaderText="Price" HtmlEncode="False" DataFormatString="{0,00:C}" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="100px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="TotalCost" HeaderText="TotalCost" HtmlEncode="False" DataFormatString="{0,00:C}"  >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="150px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="Totalsales" HtmlEncode="False" HeaderText="TotalSales" DataFormatString="{0:C}">
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="160px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        <asp:BoundField DataField="Profit" HeaderText="Profit" HtmlEncode="False" DataFormatString="{0:C}" >
            <ItemStyle Font-Names="Arial" Font-Size="8pt" HorizontalAlign="Left" Width="130px" />
            <HeaderStyle HorizontalAlign="Left" />
        </asp:BoundField>
        
        
        
        </Columns>
        
        
        
        
        
    </asp:GridView></contenttemplate>
                </asp:UpdatePanel><br />
                &nbsp;<asp:Label ID="lblSales" runat="server" CssClass="labelSteelBlue" Text="Nothing found"
                    Visible="False" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td height="30" style="width: 100px">
            </td>
        </tr>
   <tr>
            <td width="100%">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    <br />
    <br />
</asp:Content>

