using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RKLib.ExportData;

public partial class Sales_mpoponnet1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string customer = Session["dpdCustomer"].ToString();
        string vendor = Session["dpdVendor"].ToString();
        string from = Session["from"].ToString();
        string to = Session["to"].ToString();



        if (customer == "** ALL **")
        {
            customer = "%";
        }

        if (vendor == "** ALL **" || vendor == "")
        {
            vendor = "%";
        }

        if (customer == "%" && vendor == "%")
        {

            MyDataSetTableAdaptersTableAdapters.SalesLogMpopOnnetAAdp adpA = new MyDataSetTableAdaptersTableAdapters.SalesLogMpopOnnetAAdp();
            MyDataSetTableAdapters.SalesLogMpopOnnetADataTable TBL = adpA.getDataAll(Convert.ToDateTime(to), customer, Convert.ToDateTime(from));
            Session["TablePop"] = TBL;

            GridView1.DataSource = adpA.getDataAll(Convert.ToDateTime(to), customer, Convert.ToDateTime(from));
            GridView1.DataBind();
            
            for(int j=0; j < TBL.Columns.Count;j++)
            {
            
                this.GridView1.Columns[j].ItemStyle.Width=120;
            
            }



            if (TBL.Rows.Count < 2)
            {

                lblSales.Visible = true;
                GridView1.Visible = false;
            }

        }
        else
        {
            MyDataSetTableAdaptersTableAdapters.SalesLogMpopOnnetAdp adp = new MyDataSetTableAdaptersTableAdapters.SalesLogMpopOnnetAdp();
            MyDataSetTableAdapters.SalesLogMpopOnnetDataTable TBLA = adp.GetData(Convert.ToDateTime(to), customer, vendor, Convert.ToDateTime(from));
            Session["TablePop"] = TBLA;
            GridView1.DataSource = adp.GetData(Convert.ToDateTime(to), customer, vendor, Convert.ToDateTime(from));
            GridView1.DataBind();

            if (TBLA.Rows.Count < 2)
            {

                lblSales.Visible = true;
                GridView1.Visible = false;
            }
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {

            DataTable dtEmployee = ((DataTable)Session["TablePop"]).Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "OnNetMinipop" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);


        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }
}
