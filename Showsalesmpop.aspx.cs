using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Showsalesmpop : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFrom.Text = DateTime.Now.AddDays(-7).ToShortDateString();
            txtTo.Text = DateTime.Now.AddDays(-1).ToShortDateString();

            cdrdbDatasetTableAdapters.ProviderIPTableAdapter adp = new cdrdbDatasetTableAdapters.ProviderIPTableAdapter();
            cdrdbDataset.ProviderIPDataTable tbl = adp.GetProviderName();
            int count = tbl.Rows.Count;

            for (int i = 0; i < count; i++)
            {
                dpdCustomer.Items.Add(tbl.Rows[i]["ProviderName"].ToString());

            }



            cdrdbDatasetTableAdapters.MinipopsTableAdapter adpM = new cdrdbDatasetTableAdapters.MinipopsTableAdapter();
            cdrdbDataset.MinipopsDataTable tblM = adpM.GetData();

            int countM = tblM.Rows.Count;

            for (int j = 0; j < countM; j++)
            {
                dpdVendor.Items.Add(tblM.Rows[j]["ProviderName"].ToString());
            }
            

        }

    }

    protected void cmdShow_Click(object sender, EventArgs e)
    {
        /*Response.Cookies["dpdCustomer"].Value = dpdCustomer.SelectedValue.ToString();
        Response.Cookies["dpdVendor"].Value = dpdVendor.SelectedValue.ToString();
        Response.Cookies["from"].Value = txtFrom.Text;
        Response.Cookies["to"].Value = txtTo.Text;*/

        Session["dpdCustomer"]= dpdCustomer.SelectedValue.ToString();
        Session["dpdVendor"]= dpdVendor.SelectedValue.ToString();
        Session["from"]= txtFrom.Text;
        Session["to"] = txtTo.Text;

        Response.Redirect("Showsalesmpop1.aspx");
    }
}
