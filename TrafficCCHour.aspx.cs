using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RKLib.ExportData;
using System.Data.SqlClient;
using System.Text;

public partial class OutlanderReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string username;
        string agency;
        string minicall;

        string sql = "";

        DataSet ds = null;

        username = Session["DirectoUser"].ToString();
        lblHeader.Text = username.ToUpper() + " Traffic";

        //if (username == "santander" || username == "bento" || username == "contacto" || username == "eci" 
        //    || username == "ergon" || username == "megacal" || username == "mkt911" || username == "sto" 
        //    || username == "targetone" || username == "vcip" || username =="santanadmoncall" 
        //    || username == "santanderdomer" || username == "domercent" || username =="megacall-invex"
        //    || username == "cc-med")
        //{
        //    if (username == "santander")
        //    {
        //        lblAg.Visible = true;
        //        dpdAgen.Visible = true;
        //        lblCC.Visible = true;
        //        dpdCC.Visible = true;
        //        cmdReport.Visible = true;
        //    }
        //    else
        //    {
        //        lblAg.Visible = false;
        //        dpdAgen.Visible = false;
        //        lblCC.Visible = false;
        //        dpdCC.Visible = false;
        //        cmdReport.Visible = false;
        //    }

        //    agency = dpdAgen.SelectedValue.ToString();
        //    minicall = dpdCC.SelectedValue.ToString();

        //    StatsSant(username, agency, minicall);
        //}
        //else
        //{
        Stats(username);
        //}
    }

    private void StatsSant(string username, string agency, string minicall)
    {
        string sql = "";
        DataSet ds = null;

        if (username == "santander")
        {
            if (agency == "**ALL**")
            {
                


                sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
                + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
                + " SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetAllH "
                + " where customer like 'CC-%SAN%' and customer not in ('CC-CORTIZO-SANTANDER','CC-CORTIZO-SANTAN044','PALO-SANTO-TEST') "
                + " group by Customer, ISNULL([Type],'TBD') "
                + " UNION "
                + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
                + " sum(Attempts) as Attempts, "
                + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
                + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetAllH "
                + " where customer like 'CC-%SAN%' and customer not in ('CC-CORTIZO-SANTANDER','CC-CORTIZO-SANTAN044','PALO-SANTO-TEST') "
                + " order by  TotalMinutes DESC ";

                ds = Util.RunQueryByStmnt(sql);
                gdvToday.DataSource = ds.Tables[0];
                gdvToday.DataBind();
                Session["TrafficCCHourToday"] = ds;

                sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
                + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
                + " SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetYH "
                + " where customer like 'CC-%SAN%' and customer not in ('CC-CORTIZO-SANTANDER','CC-CORTIZO-SANTAN044','PALO-SANTO-TEST') "
                + " group by Customer, ISNULL([Type],'TBD') "
                + " UNION "
                + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
                + " sum(Attempts) as Attempts, "
                + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
                + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetYH "
                + " where customer like 'CC-%SAN%' and customer not in ('CC-CORTIZO-SANTANDER','CC-CORTIZO-SANTAN044','PALO-SANTO-TEST') "
                + " order by  TotalMinutes DESC ";


                ds = Util.RunQueryByStmnt(sql);
                gdvYesterday.DataSource = ds.Tables[0];
                gdvYesterday.DataBind();
                Session["TrafficCCHourYesterday"] = ds;
            }
            else if (minicall == "**ALL**")
            {
                


                sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
                + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
                + " SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetAllH "
                + " where customer like 'CC-" + agency + "%SAN%'"
                + " group by Customer, ISNULL([Type],'TBD') "
                + " UNION "
                + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
                + " sum(Attempts) as Attempts, "
                + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
                + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetAllH "
                + " where customer like 'CC-" + agency + "%SAN%'"
                + " order by  TotalMinutes DESC ";

                ds = Util.RunQueryByStmnt(sql);
                gdvToday.DataSource = ds.Tables[0];
                gdvToday.DataBind();
                Session["TrafficCCHourToday"] = ds;

                sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
                + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
                + " SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetYH "
                + " where customer like 'CC-" + agency + "%SAN%'"
                + " group by Customer, ISNULL([Type],'TBD') "
                + " UNION "
                + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
                + " sum(Attempts) as Attempts, "
                + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
                + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetYH "
                + " where customer like 'CC-" + agency + "%SAN%'"
                + " order by  TotalMinutes DESC ";


                ds = Util.RunQueryByStmnt(sql);
                gdvYesterday.DataSource = ds.Tables[0];
                gdvYesterday.DataBind();
                Session["TrafficCCHourYesterday"] = ds;

            }
            else
            {
                


                sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
                + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
                + " SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetAllH "
                + " where customer like '" + minicall + "' "
                + " group by Customer, ISNULL([Type],'TBD') "
                + " UNION "
                + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
                + " sum(Attempts) as Attempts, "
                + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
                + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetAllH "
                + " where customer like '" + minicall + "' "
                + " order by  TotalMinutes DESC ";

                ds = Util.RunQueryByStmnt(sql);
                gdvToday.DataSource = ds.Tables[0];
                gdvToday.DataBind();
                Session["TrafficCCHourToday"] = ds;

                sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
                + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
                + " SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetYH "
                + " where customer like '" + minicall + "' "
                + " group by Customer, ISNULL([Type],'TBD') "
                + " UNION "
                + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
                + " sum(Attempts) as Attempts, "
                + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
                + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
                + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
                + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
                + " from [mecca2].[dbo].OpSheetYH "
                + " where customer like '" + minicall + "' "
                + " order by  TotalMinutes DESC ";


                ds = Util.RunQueryByStmnt(sql);
                gdvYesterday.DataSource = ds.Tables[0];
                gdvYesterday.DataBind();
                Session["TrafficCCHourYesterday"] = ds;

            }
        }
        else if (username == "santanadmoncall" || username == "santanderdomer" || username == "domercent")
        {
            


            sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where customer like 'CC-" + username + "%' "
            + " group by Customer, ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where customer like 'CC-" + username + "%' "
            + " order by  TotalMinutes DESC ";

            ds = Util.RunQueryByStmnt(sql);
            gdvToday.DataSource = ds.Tables[0];
            gdvToday.DataBind();
            Session["TrafficCCHourToday"] = ds;

            sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where customer like 'CC-" + username + "%' "
            + " group by Customer, ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where customer like 'CC-" + username + "%' "
            + " order by  TotalMinutes DESC ";


            ds = Util.RunQueryByStmnt(sql);
            gdvYesterday.DataSource = ds.Tables[0];
            gdvYesterday.DataBind();
            Session["TrafficCCHourYesterday"] = ds;
        }
        else if (username == "vcip")
        {
            


            sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where (customer like 'CC-" + username + "SAN%' or customer like 'CC-" + username + "-SAN%') "
            + " group by Customer, ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where (customer like 'CC-" + username + "SAN%' or customer like 'CC-" + username + "-SAN%') "
            + " order by  TotalMinutes DESC ";

            ds = Util.RunQueryByStmnt(sql);
            gdvToday.DataSource = ds.Tables[0];
            gdvToday.DataBind();
            Session["TrafficCCHourToday"] = ds;

            sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where (customer like 'CC-" + username + "SAN%' or customer like 'CC-" + username + "-SAN%') "
            + " group by Customer, ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where (customer like 'CC-" + username + "SAN%' or customer like 'CC-" + username + "-SAN%') "
            + " order by  TotalMinutes DESC ";


            ds = Util.RunQueryByStmnt(sql);
            gdvYesterday.DataSource = ds.Tables[0];
            gdvYesterday.DataBind();
            Session["TrafficCCHourYesterday"] = ds;
        }
        else if (username == "cc-med")
        {
            


            sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer like 'CC-MED-%' "
            + " group by Customer, ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer like 'CC-MED-%' "
            + " order by  TotalMinutes DESC ";

            ds = Util.RunQueryByStmnt(sql);
            gdvToday.DataSource = ds.Tables[0];
            gdvToday.DataBind();
            Session["TrafficCCHourToday"] = ds;

            sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where Customer like 'CC-MED-%' "
            + " group by Customer, ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where Customer like 'CC-MED-%' "
            + " order by  TotalMinutes DESC ";


            ds = Util.RunQueryByStmnt(sql);
            gdvYesterday.DataSource = ds.Tables[0];
            gdvYesterday.DataBind();
            Session["TrafficCCHourYesterday"] = ds;
        }
        else
        {
           


            sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer like 'CC-" + username + "SAN%' "
            + " group by Customer, ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer like 'CC-" + username + "SAN%' "
            + " order by  TotalMinutes DESC ";

            ds = Util.RunQueryByStmnt(sql);
            gdvToday.DataSource = ds.Tables[0];
            gdvToday.DataBind();
            Session["TrafficCCHourToday"] = ds;

            sql = "Select Customer, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where Customer like 'CC-" + username + "SAN%' "
            + " group by Customer, ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '** ALL **','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where Customer like 'CC-" + username + "SAN%' "
            + " order by  TotalMinutes DESC ";


            ds = Util.RunQueryByStmnt(sql);
            gdvYesterday.DataSource = ds.Tables[0];
            gdvYesterday.DataBind();
            Session["TrafficCCHourYesterday"] = ds;
        }

        RefreshLbls();
    }

    private void Stats(string username)
    {
        string sql = "";
        DataSet ds = null;

        if (username == "santander-demo")
        {
            


            sql = "Select 'Santander-Demo' Customer, OriginationIP,Country, Region, ISNULL([Type],'TBD') as Type,  round(sum(ISNULL(Minutes,0)),0) as TotalMinutes, "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer = '" + username + "' "
            + " group by Customer, OriginationIP,Country, Region,ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '** ALL **','********','********','********','********',round(sum(ISNULL(Minutes,0)),0) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer = '" + username + "' "
            + " order by  TotalMinutes DESC ";

            ds = Util.RunQueryByStmnt(sql);
            gdvToday.DataSource = ds.Tables[0];
            gdvToday.DataBind();
            Session["TrafficCCHourToday"] = ds;

            Label3.Visible = false;
            bexport_yesterday.Visible = false;
        }
        else if (username == "cc-inconcert-cl")
        {
           

            sql = "Select Customer, OriginationIP, Country, Region, LMC, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer = 'CC-INCONCERT-CL' "
            + " group by Customer, OriginationIP, Country, Region, LMC, ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '** ALL **','********','********','********',LMC,'********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer = 'CC-INCONCERT-CL' "
            + " GROUP BY LMC "
            + " UNION "
            + " SELECT '** ALL **','********','********','********','********','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer = 'CC-INCONCERT-CL' "
            + " order by  TotalMinutes DESC ";

            ds = Util.RunQueryByStmnt(sql);
            gdvToday.DataSource = ds.Tables[0];
            gdvToday.DataBind();
            Session["TrafficCCHourToday"] = ds;

            sql = "Select Customer, OriginationIP, Country, Region, LMC, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where Customer = 'CC-INCONCERT-CL' "
            + " group by Customer, OriginationIP, Country, Region, LMC, ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '** ALL **','********','********','********',LMC,'********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where Customer = 'CC-INCONCERT-CL' "
            + " GROUP BY LMC "
            + " UNION "
            + " SELECT '** ALL **','********','********','********','********','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where Customer = 'CC-INCONCERT-CL' "
            + " order by  TotalMinutes DESC " ;


            ds = Util.RunQueryByStmnt(sql);
            gdvYesterday.DataSource = ds.Tables[0];
            gdvYesterday.DataBind();
            Session["TrafficCCHourYesterday"] = ds;
        }
        else if (username == "cc-broxel")
        {
            


            sql = "Select [Hour],Customer, case when Vendor = 'CC-BROXEL-DIGITEX' then 'DIGITEX' when Vendor = 'TLKTMX-BROXEL' then 'BROXEL' else Vendor end as Vendor, OriginationIP, Country, Region, LMC, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer = 'CC-BROXEL' "
            + " group by [Hour], Customer, case when Vendor = 'CC-BROXEL-DIGITEX' then 'DIGITEX' when Vendor = 'TLKTMX-BROXEL' then 'BROXEL' else Vendor end, OriginationIP, Country, Region, LMC, ISNULL([Type],'TBD') "   
            + " UNION "
            + " SELECT 0, '** ALL **','********','********','********','********','********','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer = 'CC-BROXEL' "
            + " order by [Hour] ";

            ds = Util.RunQueryByStmnt(sql);
            gdvToday.DataSource = ds.Tables[0];
            gdvToday.DataBind();
            Session["TrafficCCHourToday"] = ds;

            sql = "Select [Hour], Customer, case when Vendor = 'CC-BROXEL-DIGITEX' then 'DIGITEX' when Vendor = 'TLKTMX-BROXEL' then 'BROXEL' else Vendor end as Vendor, OriginationIP, Country, Region, LMC, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where Customer = 'CC-BROXEL' "
            + " group by [Hour], Customer, case when Vendor = 'CC-BROXEL-DIGITEX' then 'DIGITEX' when Vendor = 'TLKTMX-BROXEL' then 'BROXEL' else Vendor end, OriginationIP, Country, Region, LMC, ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT 0,'** ALL **','********','********','********','********','********','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where Customer = 'CC-BROXEL' "
            + " order by [Hour] ";


            ds = Util.RunQueryByStmnt(sql);
            gdvYesterday.DataSource = ds.Tables[0];
            gdvYesterday.DataBind();
            Session["TrafficCCHourYesterday"] = ds;
        }
        else
        {
            

            sql = "Select [Hour], Customer, OriginationIP,Country, Region, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer = '" + username.ToUpper() + "' "
            + " group by [Hour], Customer, OriginationIP,Country, Region,ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT '0', '** ALL **','********','********','********','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetAllH "
            + " where Customer = '" + username.ToUpper() + "' "
            + " order by [Hour] ";

            ds = Util.RunQueryByStmnt(sql);
            gdvToday.DataSource = ds.Tables[0];
            gdvToday.DataBind();
            Session["TrafficCCHourToday"] = ds;

            sql = "Select [Hour], Customer, OriginationIP, Country, Region, ISNULL([Type],'TBD') as Type,  sum(ISNULL(Minutes,0)) as TotalMinutes, "
            + " sum(Attempts) as Attempts, sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, mecca2.dbo.fGetACD(SUM(Minutes), "
            + " SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where Customer = '" + username.ToUpper() + "' "
            + " group by [Hour], Customer, OriginationIP, Country, Region,ISNULL([Type],'TBD') "
            + " UNION "
            + " SELECT 0,'** ALL **','********','********','********','********',sum(ISNULL(Minutes,0)) as TotalMinutes,  "
            + " sum(Attempts) as Attempts, "
            + " sum(Calls) as AnsweredCalls,sum(RA) as Rejected, "
            + " mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR, "
            + " mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR, "
            + " mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD "
            + " from [mecca2].[dbo].OpSheetYH "
            + " where Customer = '" + username.ToUpper() + "' "
            + " order by  [Hour] ";


            ds = Util.RunQueryByStmnt(sql);
            gdvYesterday.DataSource = ds.Tables[0];
            gdvYesterday.DataBind();
            Session["TrafficCCHourYesterday"] = ds;

            //sql = " Select CAST(datepart(hour,stop)as varchar) as [Hour], ISNULL([Type],'TBD') as Type, "
            //+ "Customer, "
            //+ "sum(ISNULL(CustBillMinutes,0)) as TotalMinutes, count(*) as Attempts, sum(CASE WHEN duration > 0 then 1 else 0 end) as AnsweredCalls, "
            //+ "sum(RA) as Rejected, mecca2.dbo.fGetASR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end), SUM(RA)) as ASR, "
            //+ "mecca2.dbo.fGetABR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ABR, "
            //+ "mecca2.dbo.fGetACD(SUM(CustBillMinutes), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ACD "
            //+ "from [mecca2].[dbo].Repsheet0_ "
            //+ "where Customer like 'ICS%' and datepart(hour,stop) > 18 and [Type] = 'CPP' "
            //+ "group by datepart(hour,stop), ISNULL([Type],'TBD'), "
            //+ "Customer "
            //+ "UNION "
            //+ "Select '******' as [Hour], "
            //+ "'******' as Type, "
            //+ "'******' [Customer Group], "
            //+ "sum(ISNULL(CustBillMinutes,0)) as TotalMinutes, "
            //+ "count(*) as Attempts, "
            //+ "sum(CASE WHEN duration > 0 then 1 else 0 end) as AnsweredCalls, "
            //+ "sum(RA) as Rejected, "
            //+ "mecca2.dbo.fGetASR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end), SUM(RA)) as ASR, "
            //+ "mecca2.dbo.fGetABR(count(*), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ABR, "
            //+ "mecca2.dbo.fGetACD(SUM(CustBillMinutes), SUM(CASE WHEN duration > 0 then 1 else 0 end)) as ACD "
            //+ "from [mecca2].[dbo].Repsheet0_ "
            //+ "where Customer like 'ICS%' "
            //+ "and datepart(hour,stop) > 18 and [Type] = 'CPP' "
            //+ "order by [Hour], TotalMinutes ";

            //ds = Util.RunQueryByStmnt(sql);
            //gdvYesterdayHour.DataSource = ds.Tables[0];
            //gdvYesterdayHour.DataBind();
        }
        RefreshLbls();
    }

    private void RefreshLbls()
    {
       

        //lblRefresh TODAY
        StringBuilder todayupdate = new StringBuilder();
        todayupdate.Append("SELECT ReportCreated AS Modified FROM MECCA2.dbo.OpSheetCreateTables WHERE Report = 'Todays'");
        DataSet ResultSet3;
        ResultSet3 = RunQuery(todayupdate, "MECCA2ConnectionString");
        lblToday.Text = "Last Modified:" + ResultSet3.Tables[0].Rows[0][0].ToString();
        lblToday.Visible = true;

        //lblRefresh YESTERDAY
        StringBuilder query = new StringBuilder();
        query.Append("select crdate as Modified from [mecca2].[dbo].sysobjects  where type = 'U' and name like 'OpSheetYH'");
        DataSet ResultSet1;
        ResultSet1 = RunQuery(query, "MECCA2ConnectionString");
        lblYesterday.Text = "Last Modified:" + ResultSet1.Tables[0].Rows[0][0].ToString();
    }

    

    protected void gdvToday_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvToday.PageIndex = e.NewPageIndex;
        DataSet ds = (DataSet)Session["TrafficCCHourToday"];
        gdvToday.DataSource = ds;
        gdvToday.DataBind();
    }

    protected void gdvYesterday_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvYesterday.PageIndex = e.NewPageIndex;
        DataSet ds = (DataSet)Session["TrafficCCHourYesterday"];
        gdvYesterday.DataSource = ds;
        gdvYesterday.DataBind();
    }

    

    protected void exportTodays(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["TrafficCCHourToday"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "TrafficTodays" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }

    protected void exportYesterdays(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["TrafficCCHourYesterday"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "TrafficYesterdays" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }
    }

    protected void dpdAgen_SelectedIndexChanged(object sender, EventArgs e)
    {
        dpdCC.Enabled = true;

        string agen = dpdAgen.SelectedValue.ToString();

        SqlConnection SqlConn1 = new SqlConnection();
        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand SqlCommand1 = new SqlCommand();

        if (dpdAgen.SelectedValue.ToString() == "VCIP")
        {
            SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE (PROVIDERNAME LIKE 'CC-" + agen + "SAN%' or PROVIDERNAME LIKE 'CC-" + agen + "-SAN%') AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
        }
        else
        {
            SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-" + agen + "SAN%' AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
        }

        SqlCommand1.Connection = SqlConn1;
        SqlConn1.Open();

        dpdCC.Items.Clear();

        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();

        while (myReader1.Read())
        {

            dpdCC.Items.Add(myReader1.GetValue(0).ToString());
        }
        myReader1.Close();
        SqlConn1.Close();
    }

    protected void cmdReport_Click(object sender, EventArgs e)
    {
        string username;
        string agency;
        string minicall;

        username = Session["DirectoUser"].ToString();
        agency = dpdAgen.SelectedValue.ToString();
        minicall = dpdCC.SelectedValue.ToString();

        StatsSant(username, agency, minicall);
    }

    private System.Data.DataSet RunQuery(StringBuilder qry, string StringConnection)
    {

        System.Data.SqlClient.SqlCommand sqlQuery = new System.Data.SqlClient.SqlCommand();
        sqlQuery.CommandText = qry.ToString();


        string connectionString = ConfigurationManager.ConnectionStrings
        [StringConnection].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
