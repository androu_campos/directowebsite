using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class mgmt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MyDataSetTableAdaptersTableAdapters.MinipopReportAllTableAdapter adp = new MyDataSetTableAdaptersTableAdapters.MinipopReportAllTableAdapter();
        GridView1.DataSource = adp.GetData();
        GridView1.DataBind();

        cdrdbDatasetTableAdapters.CustTableTableAdapter adpC = new cdrdbDatasetTableAdapters.CustTableTableAdapter();
        gdvCust.DataSource = adpC.GetCustTable();
        gdvCust.DataBind();

        cdrdbDatasetTableAdapters.VendTableTableAdapter adpV = new cdrdbDatasetTableAdapters.VendTableTableAdapter();
        gdvVend.DataSource = adpV.GetVendTable();
        gdvVend.DataBind();

        


    }
}
