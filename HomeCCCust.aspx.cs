using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;

public partial class HomeCC : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String connectionString;
        SqlConnection DBConnection;
        SqlDataAdapter dbAdapter;
        SqlCommand sQuery;
        DataSet resultSet;
        string coo = "";

        if (Session["DirectoUser"].ToString() == "cc-nbx")
        {
            connectionString = ConfigurationManager.ConnectionStrings["MECCA2ConnectionString"].ConnectionString;
            DBConnection = new SqlConnection(connectionString);
            dbAdapter = new SqlDataAdapter();
            sQuery = new SqlCommand();
            sQuery.Connection = DBConnection;
            sQuery.CommandTimeout = 1800;
            sQuery.CommandType = System.Data.CommandType.Text;
            sQuery.CommandText = "SELECT Billingdate, RemainingBalance from Talktel.dbo.RemainingBalance_CNBX";
            dbAdapter.SelectCommand = sQuery;
            resultSet = new DataSet();
            dbAdapter.Fill(resultSet);

            if (resultSet.Tables.Count > 0)
            {

                if (resultSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in resultSet.Tables[0].Rows)
                    {

                        coo = row["remainingbalance"].ToString();
                        Label1.Text = "Remaining Balance: " + row["RemainingBalance"].ToString() + " MXN";
                        Label2.Text = "Last Update: " + row["Billingdate"].ToString();
                        Label1.Visible = true;
                        Label2.Visible = true;
                    }
                }
            }
        }

        //Implementacion Usuarios Coppel
        if (Session["DirectoUser"].ToString() == "alfonso.felix.coppel" || Session["DirectoUser"].ToString() == "strias.coppel" || Session["DirectoUser"].ToString() == "erika.lopez.coppel")
        {
            Session["DirectoUser"] = "cc-coppel-culiacan";
        }

        if (Session["DirectoUser"].ToString() == "mitzi.rosales.bbva")
        {
            Session["DirectoUser"] = "cc-bancomer";
        }

        



    }

   
}
