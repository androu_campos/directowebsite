using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class TopStatsVendor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string user = "";
        string dt = DateTime.Now.ToString();
        string s = "";
        string sql = "";
        string sqlCustomers = "";
        string sqlCheckPoints = "";
        string sqlSummary = "";
        string customer = "";
        string currentHour = "";
        string range = "";
        string currentDate = "";

        int rows = 0;
        int rowCostumers = 0;
        DataSet dts = null;
        DataSet dtsCustomers = null;
        DataSet dtsSummary = null;
        DataSet dtsCheckP = null;
        int start = 0;
        int stop = 0;
        int id = 0;
        string dateRangeStart = "";
        string dateRangeStop = "";
        string datetime = "";


        if (!Page.IsPostBack)
        {
            user = Session["DirectoUser"].ToString();
            sql = "INSERT INTO mecca2..LoginTopCustVendors VALUES (";
            sql += "'"+user+"','"+dt+"',"+"'Top Vendors')";

            try
            {
                Util.RunQueryByStmnt(sql);
            }
            catch (Exception exception)
            {
                exception.ToString();
            }
        }
        
        currentHour = DateTime.Now.Hour.ToString();
        currentDate = DateTime.Now.ToShortDateString();

        dateRangeStart = currentDate + " ";
        dateRangeStop = currentDate + " ";
        //currentHour=currentHour.Substring(0, 2);


        s = "<table ID=\"Table1\" runat=\"server\" border=\"1\">";

        

        sqlCustomers = "SELECT NAME FROM TopMonitorConfig WHERE TYPE = 'V' ORDER BY NAME";
        dtsCustomers = (DataSet)Util.RunQueryByStmnt(sqlCustomers);

        sqlCheckPoints = "SELECT ID,START,STOP FROM TopMonCheckpoints WHERE ";
        sqlCheckPoints = sqlCheckPoints + currentHour + " BETWEEN START AND STOP ";

        dtsCheckP = (DataSet)Util.RunQueryByStmnt(sqlCheckPoints);

        if (dtsCheckP.Tables[0].Rows.Count > 0)
        {
            range = "'" + dtsCheckP.Tables[0].Rows[0][1] + ":00 - ";
            range = range + dtsCheckP.Tables[0].Rows[0][2] + ":00'";

            dateRangeStart += dtsCheckP.Tables[0].Rows[0][1] + ":00";
            dateRangeStop += dtsCheckP.Tables[0].Rows[0][2] + ":00";

            //id = Convert.ToInt16(+dtsCheckP.Tables[0].Rows[0][0]);

            //dtsCheckP.Dispose();

            //if ((Convert.ToInt16(currentHour) - start) == 1)
            //{
            //    if (id == 1)
            //        sqlCheckPoints = "SELECT START FROM TopMonCheckpoints WHERE id = (SELECT MAX(ID) FROM TopMonCheckpoints)";
            //    else
            //        sqlCheckPoints = "SELECT START FROM TopMonCheckpoints WHERE id = " + (id-1);

            //    dtsCheckP = (DataSet)Util.RunQueryByStmnt(sqlCheckPoints);

            //    start = (int) dtsCheckP.Tables[0].Rows[0][0];               

            //}

        }



        if (dtsCustomers.Tables[0].Rows.Count > 0)
        {
            rowCostumers = dtsCustomers.Tables[0].Rows.Count;
            Label1.Text = "TOP " + rowCostumers + " VENDOR STATS";

            for (int i = 0; i < rowCostumers; i++)
            {
                customer = "";
                customer = "'" + dtsCustomers.Tables[0].Rows[i][0] + "'";
                datetime = "";

                sqlSummary = "SELECT CONVERT(varchar,GETDATE(),101)+' '+" + range + "as Time,";
                sqlSummary = sqlSummary + "VENDOR , TYPE, CLASS,sum(isnull(minutes,0)) as TotalMinutes, sum(Attempts) as Attempts,";
                sqlSummary = sqlSummary + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected,";
                sqlSummary = sqlSummary + "mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,";
                sqlSummary = sqlSummary + "mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes),";
                sqlSummary = sqlSummary + "SUM(Calls)) as ACD ";
                sqlSummary = sqlSummary + "FROM opLiveMonitor ";

                switch (customer)
                {
                    case "'AMERICATEL-C.AMERICA'":
                        sqlSummary = sqlSummary + "WHERE VENDOR = " + customer + " AND TIME BETWEEN '" + dateRangeStart + "' AND '" + dateRangeStop + "' AND REGION='TELGUA' "; break;
                    case "'STELECOM'":
                        sqlSummary = sqlSummary + "WHERE VENDOR = " + customer + " AND TIME BETWEEN '" + dateRangeStart + "' AND '" + dateRangeStop + "' AND COUNTRY = 'BRAZIL' AND TYPE = 'MOBILE' "; break;
                    case "'COTEL'":
                        sqlSummary = sqlSummary + "WHERE VENDOR = " + customer + " AND TIME BETWEEN '" + dateRangeStart + "' AND '" + dateRangeStop + "' AND REGION='LA PAZ' AND TYPE = 'FIXED' "; break;
                    default:
                        sqlSummary = sqlSummary + "WHERE VENDOR = " + customer + " AND TIME BETWEEN '" + dateRangeStart + "' AND '" + dateRangeStop + "' AND (TYPE = 'FIXED' OR TYPE='CPP' OR TYPE='MOBILE') "; break;
                }


                //sqlSummary = sqlSummary + "WHERE VENDOR = " + customer + " AND TIME BETWEEN '" + dateRangeStart + "' AND '" + dateRangeStop + "' AND (TYPE = 'FIXED' OR TYPE='CPP') ";
                sqlSummary = sqlSummary + "GROUP by VENDOR,TYPE, CLASS ";
                sqlSummary = sqlSummary + "Order by VENDOR, TYPE";

                dtsSummary = (DataSet)Util.RunQueryByStmnt(sqlSummary);

                s = s + "<tr><td colspan=\"11\" bgcolor=\"99CCFF\"><b>" + dtsCustomers.Tables[0].Rows[i][0] + "</b></td></tr>";

                s = s + "<tr><td width=\"500px\"><b>DateTime</b></td>";
                s = s + "<td><b>Vendor</b></td>";
                s = s + "<td><b>Type</b></td>";
                s = s + "<td><b>Class</b></td>";
                s = s + "<td><b>TotalMinutes</b></td>";
                s = s + "<td><b>Attempts</b></td>";
                s = s + "<td><b>AnsweredCalls</b></td>";
                s = s + "<td><b>Rejected</b></td>";
                s = s + "<td><b>ASR</b></td>";
                s = s + "<td><b>ABR</b></td>";
                s = s + "<td><b>ACD</td></b></tr>";

                if (dtsSummary.Tables[0].Rows.Count > 0)
                {
                    for (int j = 0; j < dtsSummary.Tables[0].Rows.Count; j++)
                    {
                        s = s + "<tr>";
                        for (int k = 0; k < 11; k++)
                        {
                            s = s + "<td>" + dtsSummary.Tables[0].Rows[j][k].ToString() + "</td>";
                        }
                        s = s + "</tr>";
                    }
                }

                s = s + "<tr><td colspan=\"11\" bgcolor=\"CCCCC\">&nbsp</td></tr>";

                dtsSummary.Dispose();


                sql = "Select convert(varchar,time,101)+' '+left(convert(varchar,time,114),2)+':00' as Time,";
                sql = sql + "VENDOR , TYPE, CLASS,sum(isnull(minutes,0)) as TotalMinutes, sum(Attempts) as Attempts,";
                sql = sql + "sum(Calls) as AnsweredCalls,sum(RA) as Rejected, mecca2.dbo.fGetASR(SUM(Attempts),";
                sql = sql + "SUM(Calls), SUM(RA)) as ASR, mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,";
                sql = sql + "mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) as ACD ";
                sql = sql + "FROM opLiveMonitor ";

                switch (customer)
                {
                    case "'AMERICATEL-C.AMERICA'":
                        sql = sql + "WHERE VENDOR = " + customer + " AND TIME BETWEEN '" + dateRangeStart + "' AND '" + dateRangeStop + "' AND REGION='TELGUA' "; break;
                    case "'STELECOM'":
                        sql = sql + "WHERE VENDOR = " + customer + " AND TIME BETWEEN '" + dateRangeStart + "' AND '" + dateRangeStop + "' AND COUNTRY = 'BRAZIL' AND TYPE = 'MOBILE' "; break;
                    case "'COTEL'":
                        sql = sql + "WHERE VENDOR = " + customer + " AND TIME BETWEEN '" + dateRangeStart + "' AND '" + dateRangeStop + "' AND REGION='LA PAZ' AND TYPE = 'FIXED' "; break;
                    default:
                        sql = sql + "WHERE VENDOR = " + customer + " AND TIME BETWEEN '" + dateRangeStart + "' AND '" + dateRangeStop + "' AND (TYPE = 'FIXED' OR TYPE='CPP') "; break;
                }


                //sql = sql + "WHERE VENDOR = " + customer + " AND TIME BETWEEN '" + dateRangeStart + "' AND '" + dateRangeStop + "' AND (TYPE = 'FIXED' OR TYPE='CPP') ";
                sql = sql + "GROUP by VENDOR,TYPE,CLASS,convert(varchar,time,101)+' '+left(convert(varchar,time,114),2)+':00' ";
                sql = sql + "Order by Time, VENDOR, TYPE";

                dts = (DataSet)Util.RunQueryByStmnt(sql);

                if (dts.Tables[0].Rows.Count > 0)
                {
                    rows = dts.Tables[0].Rows.Count;

                    for (int l = 0; l < rows; l++)
                    {
                        if (dts.Tables[0].Rows[l][0].ToString() != datetime && datetime != "")
                            s = s + "<tr><td colspan=\"11\" bgcolor=\"FFCC00\">&nbsp</td></tr>";

                        s = s + "<tr>";
                        for (int m = 0; m < 11; m++)
                        {
                            s = s + "<td>" + dts.Tables[0].Rows[l][m].ToString() + "</td>";
                        }
                        s = s + "</tr>";

                        datetime = dts.Tables[0].Rows[l][0].ToString();
                    }
                }

                s = s + "<tr><td colspan=\"11\">&nbsp</td></tr>";
                dts.Dispose();
            }

            s = s + "</table>";
        }

        this.stats.Text = s;

    }
}
