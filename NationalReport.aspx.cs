using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class NationalReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TextBox1.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            TextBox2.Text = DateTime.Now.AddDays(-1).ToShortDateString();

            ListBox1.Items.Add("NATIONAL");
            ListBox1.Items.Add("NATIONAL-ISA");
            ListBox1.Items.Add("NATIONAL-OFF");
            ListBox1.Items.Add("NATIONAL-TLL");
            ListBox1.Items.Add("NATIONAL-NEXT");
            ListBox1.Items.Add("NATIONAL-TELULAR"); 
            
        }
    }
    protected void cmdShow_Click(object sender, EventArgs e)
    {

        SqlCommand sqlQry = new SqlCommand();
        sqlQry.CommandType = CommandType.Text;



        string sql = "";
        string name = "";

        foreach (ListItem i in ListBox1.Items)
        {
            name += "'" + i.Text + "',";
        }

        name = name.Substring(0, name.Length - 1);


        sql = "SELECT CONVERT(VARCHAR(11) ,T.Billingdate,101) as BillingDate,T.Customer,T.Vendor,";
        sql += "CustBroker,VendorBroker,T.Country,T.Region,T.RegionPrefix,T.CustPrefix,T.VendorPrefix, ";
        sql += "T.Type,T.LMC,T.Class,T.Cost,T.Price,ROUND(SUM(T.CustMinutes),4) as CMinutes, ";
        sql += "ROUND(SUM(T.VendorMinutes),4) as VMinutes,ROUND(SUM(T.TotalCost),4) as TotalCost, ";
        sql += "ROUND(SUM(T.TotalPrice),4) as TotalSales,ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit, ";
        sql += "SUM(T.tCalls) as Attempts,SUM(T.BillableCalls) as AnsweredCalls,SUM(T.RA) as [Rejected Calls], ";
        sql += "mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR, ";
        sql += "mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR, ";
        sql += "mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD ";
        sql += "FROM MECCA2..trafficlog T LEFT JOIN MECCA2..CBroker B ON T.Customer = B.Customer LEFT JOIN ";
        sql += "MECCA2..VBroker V ON T.Vendor = V.Vendor JOIN CDRDB..ProviderIP p ON ";
        sql += "p.ProviderName = t.Customer  JOIN CDRDB..ProviderIP p2 ON p2.ProviderName = t.Vendor ";
        sql += "WHERE BillingDate BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' AND LEN(p.IP) = 4 AND ";
        sql += "p.Type = 'C' AND LEN(p2.IP) = 4 AND p2.Type = 'V'  AND T.Vendor in (" + name + ")";
        sql += "GROUP BY T.BillingDate,T.Customer,T.Vendor,B.CustBroker,V.VendorBroker,T.Country,T.Region, ";
        sql += "T.RegionPrefix,T.CustPrefix,T.VendorPrefix,T.Type,T.LMC,T.Class,T.Cost,T.Price ";
        sql += "HAVING SUM(CustMinutes) > 0 ";
        sql += "UNION SELECT '** ALL **' AS BillingDate,'****' AS Customer,'****' AS Vendor,'****' aS CustBroker, ";
        sql += "'****' as VendorBroker,'****' as Country,'****' as Region,'****' as RegionPrefix,'****', ";
        sql += "'****' as VendorPrefix,'****' as [Type],'****' as LMC,'****' as Class,NULL as Cost, ";
        sql += "NULL AS Price,ROUND(SUM(T.CustMinutes),4) as CMinutes,ROUND(SUM(T.VendorMinutes),4) as VMinutes, ";
        sql += "ROUND(SUM(T.TotalCost),4) as TotalCost,ROUND(SUM(T.TotalPrice),4) as TotalSales, ";
        sql += "ROUND(SUM(T.TotalPrice-T.TotalCost),4) as Profit,SUM(T.tCalls) as Attempts, ";
        sql += "SUM(T.BillableCalls) as AnsweredCalls,SUM(T.RA) as [Rejected Calls], ";
        sql += "mecca2.dbo.fGetASR(SUM(T.tCalls), SUM(T.BillableCalls), SUM(T.RA)) as ASR, ";
        sql += "mecca2.dbo.fGetABR(SUM(T.tCalls), SUM(T.BillableCalls)) AS ABR, ";
        sql += "mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD ";
        sql += "FROM MECCA2..trafficlog T LEFT JOIN MECCA2..CBroker B ON T.Customer = B.Customer ";
        sql += "LEFT JOIN MECCA2..VBroker V ON T.Vendor = V.Vendor JOIN ";
        sql += "CDRDB..ProviderIP p ON p.ProviderName = t.Customer  JOIN ";
        sql += "CDRDB..ProviderIP p2 ON p2.ProviderName = t.Vendor  ";
        sql += "WHERE BillingDate BETWEEN '" + TextBox1.Text + "' AND '" + TextBox2.Text + "' AND LEN(p.IP) = 4 AND p.Type = 'C' AND ";
        sql += "LEN(p2.IP) = 4 AND p2.Type = 'V' AND T.Vendor in (" + name + ")";
        sql += "HAVING SUM(CustMinutes) > 0  ORDER BY   BillingDate,T.Customer,T.Vendor,CustBroker, ";
        sql += "VendorBroker,T.Country,T.Region,T.RegionPrefix,T.CustPrefix,T.VendorPrefix,T.Type, ";
        sql += "T.LMC,T.Class,T.Cost,T.Price ";



        sqlQry.CommandText = sql;

        sqlQry.CommandTimeout = 180;

        DataSet myDataset = RunQuery(sqlQry);
        Session["NationalReport"] = myDataset;
        if (myDataset.Tables[0].Rows.Count > 0)
        {
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            cmdExport.Visible = true;
            Label4.Visible = false;
        }
        else
        {
            cmdExport.Visible = false;
            Label4.Visible = true;
        }
    }

    protected void cmdAdd(object sender, EventArgs e)
    {
        ListBox1.Items.Add(dpdProvider.Text);
    }

    protected void cmdDel(object sender, EventArgs e)
    {
        ListBox1.Items.Remove(ListBox1.SelectedItem);
    }

    protected void cmdReset(object sender, EventArgs e)
    {
        ListBox1.Items.Clear();
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

        return resultsDataSet;
    }
    protected void cmdExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {
            DataTable dtEmployee = ((DataSet)Session["NationalReport"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            string filename = "NationalReport" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = Session["NationalReport"];
        GridView1.DataBind();
    }



}
