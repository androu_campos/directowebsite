<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PortsGraphSIPGrid.aspx.cs"
    Inherits="PortsGraph" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <table>
        <tr>
            <td style="width: 904px; height: 12px;" valign="top">
                <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Font-Bold="True" Text="Active Ports Graph by SIP Cause"
                    Width="200px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" height="30" style="border-right: 0px solid; border-top: 0px solid;
                border-left: 0px solid; border-bottom: 0px solid; width: 904px;">
                <table style="height: 29px">
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" style="width: 61px">
                            <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Customer:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdCustomer" runat="server" CssClass="dropdown" DataTextField="Customer"
                                DataValueField="Customer">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Vendor:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdVendor" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label7" runat="server" CssClass="labelBlue8" Text="Type:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdType" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label3" runat="server" CssClass="labelBlue8" Text="LMC:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdLMC" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="right" style="width: 108px">
                            <asp:Button ID="cmdReport" runat="server" CssClass="boton" OnClick="cmdReport_Click"
                                Text="View Graph" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label9" runat="server" CssClass="labelBlue8" Text="Country:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdCountry" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="70">
                            <asp:Label ID="Label4" runat="server" CssClass="labelBlue8" Text="Cust SIP:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdCSIPCause" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label8" runat="server" CssClass="labelBlue8" Text="Vend SIP:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdVSIPCause" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td align="center" bgcolor="#d5e3f0" width="61">
                            <asp:Label ID="Label10" runat="server" CssClass="labelBlue8" Text="Report Type:"></asp:Label>
                        </td>
                        <td style="width: 117px">
                            <asp:DropDownList ID="dpdRtype" runat="server" CssClass="dropdown" >
                                <asp:ListItem Selected="True" Text="Recent" Value="OpSheetINC"></asp:ListItem>
                                <asp:ListItem Text="Todays" Value="OpSheetAllH"></asp:ListItem>
                                <asp:ListItem Text="Yesterdays" Value="OpSheetYH"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 321px" colspan="4">
                            <asp:Label ID="lblWarning" runat="server" CssClass="labelBlue8" Font-Bold="True"
                                ForeColor="Red" Text="Nothing found!!" Visible="False" Width="300px"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 904px" align="left" valign="top">
                <DCWC:Chart ID="Chart2" runat="server" Height="400px" Width="750px" ImageUrl="~/TempImages/ChartPic_#SEQ(300,3)" ImageType="Flash" AnimationTheme="GrowingTogether" BackColor="#F3DFC1" BorderLineStyle="Solid" BackGradientType="TopBottom" Palette="Dundas" BorderLineColor="181, 64, 1" borderlinewidth="2">
                    <Legends>
						<dcwc:Legend LegendStyle="Row" AutoFitText="False" Docking="Top" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold" Alignment="Far">
							<Position Y="4" Height="7.653602" Width="49.60493" X="48.65784"></Position>
						</dcwc:Legend>
					</Legends>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <Series>
                        <dcwc:Series YValuesPerPoint="4" Name="Attempts Customer" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"></dcwc:Series>
                        <dcwc:Series YValuesPerPoint="4" ChartArea="GroupedData" Name="Attempts Vendor" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65"></dcwc:Series>                        
					</Series>
                    <ChartAreas>
                        <dcwc:ChartArea Name="Default" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="9" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
						<dcwc:ChartArea Name="GroupedData" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent" AlignWithChartArea="Default">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="50" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
                    </ChartAreas>
                </DCWC:Chart>                 
            </td>
            <td style="width: 100px" align="left" valign="top">
            </td>
        </tr>        
        <tr>
            <td style="width: 904px" align="left" valign="top">
                <table>
                    <tr>
                        <td style="width: 179px">
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 179px" valign="top">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button ID="cmdDetail" runat="server" CssClass="boton" OnClick="cmdDetail_Click"
                                        Text="View detail" Visible="False" /><asp:Button ID="cmdHide" runat="server" CssClass="boton"
                                            OnClick="cmdHide_Click" Text="Hide detail" Visible="False" /><br />
                                    <asp:Label ID="lblGrid" runat="server" Text="Label" Visible="False" CssClass="labelBlue8"></asp:Label>
                                    <br />
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <asp:GridView ID="gdvDetailC" runat="server" Font-Size="8pt" Font-Names="Arial" Visible="False"
                                                    AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="SIPCause" HeaderText="C SIPCause" HeaderStyle-Wrap="false">
                                                            <ItemStyle Wrap="false" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Attempts" HeaderText="Attempts">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Definition" HeaderText="Definition">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="titleOrangegrid"></HeaderStyle>
                                                </asp:GridView>
                                            </td>
                                            <td valign="top">
                                                <asp:GridView ID="gdvDetailV" runat="server" Font-Size="8pt" Font-Names="Arial" Visible="False"
                                                    AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="SIPCause" HeaderText="V SIPCause" HeaderStyle-Wrap="false">
                                                            <ItemStyle Wrap="false" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Attempts" HeaderText="Attempts">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Definition" HeaderText="Definition">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="titleOrangegrid"></HeaderStyle>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="cmdDetail" EventName="Click"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="cmdHide" EventName="Click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td align="left" style="width: 100px" valign="top">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 100px">
                                                <asp:Label ID="Label5" runat="server" Text="Loading...Please wait" ForeColor="LimeGreen"
                                                    Width="105px" CssClass="labelBlue8"></asp:Label>
                                            </td>
                                            <td style="width: 100px">
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Loading.gif"></asp:Image>
                                            </td>
                                        </tr>
                                    </table>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 179px">
                            &nbsp;
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <%--<td style="width: 106px; height: 44px;" valign="top">
                <asp:Timer ID="Timer1" runat="server" Interval="300000">
                </asp:Timer>
            </td>--%>
        </tr>
    </table>
    <br />
</asp:Content>
