using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using RKLib.ExportData;


public partial class qbuilderV1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                string qry = Session["AdQbuilder1"].ToString();


                SqlCommand sqlSpQuery = new SqlCommand();
                sqlSpQuery.CommandType = System.Data.CommandType.Text;
                sqlSpQuery.CommandText = qry;
                DataSet ResultSet;
                ResultSet = RunQuery(sqlSpQuery);
                Session["qbuilder1"] = ResultSet;

                gdvQbuilder.DataSource = ResultSet;
                gdvQbuilder.AllowPaging = true;
                gdvQbuilder.AllowSorting = true;
                gdvQbuilder.EnableSortingAndPagingCallbacks = false;
                gdvQbuilder.PageSize = Convert.ToInt32(Request.QueryString["Pz"]);
                //gdvQbuilder.DataBind();

                for (int z = 0; z < ResultSet.Tables[0].Columns.Count; z++)
                {
                    //    gdvQbuilder.Columns[z].ItemStyle.Width = 50;
                    string columnName = ResultSet.Tables[0].Columns[z].ColumnName.ToString();
                    BoundField columna = new BoundField();
                    columna.DataField = columnName;
                    columna.SortExpression = columnName;
                    columna.HeaderText = columnName;
                    columna.ItemStyle.Width = 120;
                    columna.ItemStyle.Wrap = false;
                    columna.HeaderStyle.Wrap = false;
                    gdvQbuilder.Columns.Add(columna);
                    gdvQbuilder.DataBind();

                }



                /**/
                Button1.Visible = true;

                string table = Request.QueryString[0].ToString();
                string query = "select crdate as Modified from [mecca2].[dbo].sysobjects  where type = 'U' and name like '" + table + "'";
                SqlCommand sqlQuery = new SqlCommand();
                sqlQuery.CommandType = System.Data.CommandType.Text;
                sqlQuery.CommandText = query;
                DataSet ResultSet1;
                ResultSet1 = Util.RunQuery(sqlQuery);
                Session["AdvQB"] = ResultSet1;
                Label2.Text = "Last Modified:" + ResultSet1.Tables[0].Rows[0][0].ToString();

            }


        }
        catch (Exception ex)
        {
            string messageerror = ex.Message.ToString();
            Response.Write(messageerror);
            //Response.Redirect("Qbuilder.aspx", false);

        }


    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

    protected void gdvQbuilder_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gdvQbuilder_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

        gdvQbuilder.PageIndex = e.NewSelectedIndex;

        gdvQbuilder.DataBind();

    }


    protected void Button1_Click(object sender, EventArgs e)//EXPORT TO EXCEL
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["qbuilder1"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "AdvanceQueryBuilder" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string erro = ex.Message.ToString();
        }
    }
    protected void gdvQbuilder_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    {
        gdvQbuilder.PageIndex = e.NewPageIndex;
        DataSet newDts = (DataSet)Session["qbuilder1"];
        gdvQbuilder.DataSource = newDts.Tables[0];
        gdvQbuilder.DataBind();
    }
    protected void gdvQbuilder_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable tbl = ((DataSet)Session["qbuilder1"]).Tables[0];
        DataView dtv = new DataView(tbl);
        dtv.Sort = e.SortExpression;
        
        gdvQbuilder.DataSource = dtv;
        gdvQbuilder.DataBind();

    }
}
