<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AMUnlGuateH.aspx.cs" Inherits="Default7" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td style="width: 200px">
                    <asp:Label ID="lblBilling" runat="server" CssClass="labelBlue8" ForeColor="Red" Text="Label"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 200px">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <asp:Label ID="lblHeader" runat="server" CssClass="labelBlue8" Font-Bold="True" Font-Size="10pt"
                        Text="Label"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px; height: 23px">
                </td>
            </tr>
            <tr>
                <td style="width: 200px">
                    <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Unlimited Guate History" Width="200px"></asp:Label><br />
                    <asp:GridView ID="gdvHistory" runat="server" Font-Names="Arial" Font-Size="8pt" Font-Underline="False" Width="100%" AllowPaging="True" OnPageIndexChanging="gdvHistroy_PageIndexChanging" PageSize="30">
                        <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True"></FooterStyle>
                        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center"></RowStyle>
                        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                        <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center"></PagerStyle>
                        <HeaderStyle Height="40px" CssClass="titleOrangegrid" Font-Size="8pt" Font-Names="Arial"
                            Font-Bold="True" ForeColor="Orange"></HeaderStyle>
                        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                        
                    </asp:GridView>                    
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="bexport_history" runat="server" Text="Excel" CssClass="boton" OnClick="exportHistory"/>
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 24px">
                </td>
            </tr>            
            
        </table>
    
    </div>
    </form>
</body>
</html>
