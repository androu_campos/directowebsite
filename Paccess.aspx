<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="Paccess.aspx.cs" Inherits="Paccess" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="Recent Traffic - Summary of the last 15 mins" Width="263px"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Button ID="cmdExport1" runat="server" CssClass="boton" OnClick="cmdExport1_Click"
                    Text="Export To EXCEL" Width="95px" /></td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:GridView ID="GridView1" runat="server" Font-Names="Arial" Font-Size="8pt" HeaderStyle-Wrap="false" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="25">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" Wrap="False" />
                    <AlternatingRowStyle BackColor="White" />
                    
                    <%--<Columns>
                    <asp:BoundField DataField="Direction" HeaderText="Direction" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Vendor" HeaderText="Vendor" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Region" HeaderText="Region" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Attempts" HeaderText="Attempts" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="TotalCalls" HeaderText="Total Calls" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TotalMinutes" HeaderText="Total Minutes" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ASR" HeaderText="ASR" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ACD" HeaderText="ACD" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="ABR" HeaderText="ABR" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Rejected calls" HeaderText="Rejected calls" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    
                    
                    </Columns>--%>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 22px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" title="Recent traffic">
                <asp:Label ID="Label2" runat="server" Text="Today's Traffic before the last 15 mins" CssClass="labelTurk" Width="246px"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 100px" title="Recent traffic">
                <asp:Button ID="cmdExport2" runat="server" CssClass="boton" OnClick="cmdExport2_Click"
                    Text="Export to EXCEL" Width="95px" /></td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:GridView ID="GridView2" runat="server" Font-Names="Arial" Font-Size="8pt" AllowPaging="True" OnPageIndexChanging="GridView2_PageIndexChanging" PageSize="20">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" />
                    <AlternatingRowStyle BackColor="White" />
                    
                    <%--<Columns>
                    <asp:BoundField DataField="Direction" HeaderText="Direction" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Vendor" HeaderText="Vendor" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Region" HeaderText="Region" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Attempts" HeaderText="Attempts" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="TotalCalls" HeaderText="Total Calls" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TotalMinutes" HeaderText="Total Minutes" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ASR" HeaderText="ASR" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ACD" HeaderText="ACD" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ABR" HeaderText="ABR" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="Rejected calls" HeaderText="Rejected calls" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    
                    
                    
                    </Columns>--%>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

