<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="HistoryCommissions.aspx.cs"
    Inherits="test" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<%--<%@ OutputCache Duration="480" Location="Client" VaryByParam="None" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table id="Commissions">
        <tr>
            <td align="center" bgcolor="#d5e3f0" colspan="1" height="9">
                <asp:Label ID="Label76" runat="server" CssClass="labelBlue8" Text="Month to Date"></asp:Label></td>
            <td colspan="3" height="9" align="center" bgcolor="#d5e3f0">
                <asp:Label ID="Label77" runat="server" CssClass="labelBlue8" Text="Yesterday"></asp:Label></td>
            <td align="center" bgcolor="#d5e3f0" colspan="1" height="9">
                <asp:Label ID="Label78" runat="server" CssClass="labelBlue8" Text="Previous Day"></asp:Label></td>
            <td align="center" colspan="1" height="9">
            </td>
            <td align="center" bgcolor="#d5e3f0" colspan="1" height="9">
                <asp:Label ID="Label17" runat="server" CssClass="labelBlue8" Text="Projected this Month"></asp:Label></td>
            <td align="center" colspan="1" height="9">
            </td>
        </tr>
        <tr>
            <td align="center" colspan="1" height="9">
                <asp:GridView ID="gdvMonth" runat="server" AutoGenerateColumns="False" Font-Names="Arial"
                    Font-Size="8pt" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="Broker" HeaderText="Broker" />
                        <asp:BoundField DataField="Minutes" HeaderText="Minutes" HtmlEncode="false" DataFormatString={0:0,0} />
                        <asp:BoundField DataField="Calls" HeaderText="Calls" HtmlEncode="false" DataFormatString={0:0,0} />
                        <asp:BoundField DataField="Profit" HtmlEncode="false" HeaderText="Profit" DataFormatString=${0:0,0} />
                    </Columns>
                    <HeaderStyle CssClass="titleOrangegrid" />
                </asp:GridView>
            </td>
            <td align="center" colspan="3" height="9">
                <asp:GridView ID="gdvYestr" runat="server" AutoGenerateColumns="False" Font-Names="Arial"
                    Font-Size="8pt" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="Broker" HeaderText="Broker" />
                        <asp:BoundField DataField="Minutes" HeaderText="Minutes" HtmlEncode="false" DataFormatString={0:0,0} />
                        <asp:BoundField DataField="Calls" HeaderText="Calls" HtmlEncode="false" DataFormatString={0:0,0} />
                        <asp:BoundField DataField="Profit" HtmlEncode="false" HeaderText="Profit" DataFormatString=${0:0,0} />
                        <asp:ImageField DataImageUrlField="ImageStatus" HeaderText="Img">
                        </asp:ImageField>
                    </Columns>
                    <HeaderStyle CssClass="titleOrangegrid" />
                </asp:GridView>
            </td>
            <td align="center" colspan="1" height="9">
                <asp:GridView ID="gdvPrev" runat="server" AutoGenerateColumns="false" Font-Names="Arial"
                    Font-Size="8pt" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="Broker" HeaderText="Broker" />
                        <asp:BoundField DataField="Minutes" HeaderText="Minutes" HtmlEncode="false" DataFormatString={0:0,0} />
                        <asp:BoundField DataField="Calls" HeaderText="Calls" HtmlEncode="false" DataFormatString={0:0,0} />
                        <asp:BoundField DataField="Profit" HtmlEncode="false" HeaderText="Profit" DataFormatString=${0:0,0} />                        
                    </Columns>
                    <HeaderStyle CssClass="titleOrangegrid" />
                </asp:GridView>
            </td>
            <td align="center" colspan="1" height="9">
            </td>
            <td align="center" colspan="1" height="9">
                <asp:GridView ID="gdvProj" runat="server" AutoGenerateColumns="False" Font-Names="Arial"
                    Font-Size="8pt" Width="294px">
                    <Columns>
                        <asp:BoundField DataField="Broker" HeaderText="Broker" />
                        <asp:BoundField DataField="Minutes" HeaderText="Minutes" HtmlEncode="False" DataFormatString={0:0,0} />
                        <asp:BoundField DataField="Calls" HeaderText="Calls" HtmlEncode="False" DataFormatString={0:0,0} />
                        <asp:BoundField DataField="Profit" HtmlEncode="False" HeaderText="Profit" DataFormatString=${0:0,0} />
                        <asp:ImageField DataImageUrlField="Flag" HeaderText="Prev Month">
                        </asp:ImageField>
                    </Columns>
                    <HeaderStyle CssClass="titleOrangegrid" />
                </asp:GridView>
            </td>
            <td align="center" colspan="1" height="9">
            </td>
        </tr>
        <tr>
            <td align="center" colspan="1" style="height: 93%" valign="top">
                <asp:Label ID="lblNotMonth" runat="server" ForeColor="Red" Text="Not avalilable"
                    Visible="False"></asp:Label>
                
            </td>
            <td align="center" colspan="3" style="height: 93%" valign="top">
                <asp:Label ID="lblNotYesterday" runat="server" ForeColor="Red" Text="Not available"
                    Visible="False"></asp:Label>
                
            </td>
            <td align="center" colspan="1" style="height: 93%" valign="top">
                <asp:Label ID="lblNotPrevious" runat="server" ForeColor="Red" Text="Not available"
                    Visible="False"></asp:Label>
                
            </td>
            <td align="center" colspan="1" style="height: 93%" valign="top">
            </td>
            <td align="center" colspan="1" style="height: 93%" valign="top">
                <asp:Label ID="lblNotProfit" runat="server" ForeColor="Red" Text="Not available"></asp:Label>
                
            </td>
            <td align="center" colspan="1" style="height: 93%" valign="top">
            </td>
        </tr>
        <tr>
            <td align="center" colspan="1" valign="top" height="30">
            </td>
            <td align="center" colspan="3" style="height: 128%" valign="top">
            </td>
            <td align="center" colspan="1" style="height: 128%" valign="top">
            </td>
            <td align="center" colspan="1" style="height: 128%" valign="top">
            </td>
            <td align="center" colspan="1" style="height: 128%" valign="top">
            </td>
            <td align="center" colspan="1" style="height: 128%" valign="top">
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#d5e3f0" colspan="1" valign="top">
                <asp:Label ID="lblMonth0" runat="server" CssClass="labelBlue8" Text="History"></asp:Label></td>
            <td align="center" colspan="3" style="height: 93%" valign="top" bgcolor="#d5e3f0">
                <asp:Label ID="lblMonth1" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
            <td align="center" colspan="1" style="height: 93%" valign="top" bgcolor="#d5e3f0">
                <asp:Label ID="lblMonth2" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
            <td align="center" colspan="1" style="height: 93%" valign="top">
            </td>
            <td align="center" colspan="1" style="height: 93%" valign="top" bgcolor="#d5e3f0">
                <asp:Label ID="lblMonth3" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
            <td align="center" colspan="1" style="height: 93%" valign="top" bgcolor="#d5e3f0">
                <asp:Label ID="lblMonth4" runat="server" CssClass="labelBlue8" Text="Label"></asp:Label></td>
        </tr>
        <tr>
            <td align="center" colspan="1" valign="top">
                <asp:GridView ID="gdvMonth1" runat="server" AllowPaging="True" Font-Names="Arial"
                    Font-Size="8pt" PageSize="9" Width="100%" AutoGenerateColumns="false" PageIndex="0">
                    <PagerSettings Visible="False" />
                    <HeaderStyle CssClass="titleOrangegrid" />
                    <Columns>
                    <asp:BoundField DataField="CM" HeaderText="Broker" />                    
                    <asp:BoundField DataField="AnsweredCalls" HeaderText="Calls" />
                    <asp:BoundField DataField="Minutes" HeaderText="Minutes" />
                    <asp:BoundField DataField="Profit" HeaderText="Profit" />
                    </Columns>
                </asp:GridView>
            </td>
            <td align="center" colspan="3" style="height: 93%" valign="top">
                <asp:GridView ID="gdvMonth2" runat="server" AllowPaging="True" Font-Names="Arial"
                    Font-Size="8pt" PageSize="9" Width="100%" AutoGenerateColumns="False" PageIndex="1">
                    <PagerSettings Visible="False" />
                    <HeaderStyle CssClass="titleOrangegrid" />
                    <Columns>                    
                    <asp:BoundField DataField="CM" HeaderText="Broker" />
                    <%--<asp:BoundField DataField="MonthN" HeaderText="Month" />--%>
                    <asp:BoundField DataField="AnsweredCalls" HeaderText="Calls" />
                    <asp:BoundField DataField="Minutes" HeaderText="Minutes" />
                    <asp:BoundField DataField="Profit" HeaderText="Profit" />
                    </Columns>
                </asp:GridView>
            </td>
            <td align="center" colspan="1" style="height: 93%" valign="top">
                <asp:GridView ID="gdvMonth3" runat="server" AllowPaging="True" Font-Names="Arial"
                    Font-Size="8pt" PageSize="9" Width="100%" AutoGenerateColumns="False" PageIndex="2">
                    <PagerSettings Visible="False" />
                    <HeaderStyle CssClass="titleOrangegrid" />
                    <Columns>                    
                    <asp:BoundField DataField="CM" HeaderText="Broker" />
                    <%--<asp:BoundField DataField="MonthN" HeaderText="Month" />--%>
                    <asp:BoundField DataField="AnsweredCalls" HeaderText="Calls" />
                    <asp:BoundField DataField="Minutes" HeaderText="Minutes" />
                    <asp:BoundField DataField="Profit" HeaderText="Profit" />
                    </Columns>
                </asp:GridView>
            </td>
            <td align="center" colspan="1" style="height: 93%" valign="top">
            </td>
            <td align="center" colspan="1" style="height: 93%" valign="top">
                <asp:GridView ID="gdvMonth4" runat="server" AllowPaging="True" Font-Names="Arial"
                    Font-Size="8pt" PageSize="9" Width="100%" AutoGenerateColumns="False" PageIndex="3">
                    <PagerSettings Visible="False" />
                    <HeaderStyle CssClass="titleOrangegrid" />
                    <Columns>                    
                    <asp:BoundField DataField="CM" HeaderText="Broker" />
                    <%--<asp:BoundField DataField="MonthN" HeaderText="Month" />--%>
                    <asp:BoundField DataField="AnsweredCalls" HeaderText="Calls" />
                    <asp:BoundField DataField="Minutes" HeaderText="Minutes" />
                    <asp:BoundField DataField="Profit" HeaderText="Profit" />
                    </Columns>
                </asp:GridView>
            </td>
            <td align="center" colspan="1" style="height: 93%" valign="top">
                <asp:GridView ID="gdvMonth5" runat="server" AllowPaging="True" Font-Names="Arial"
                    Font-Size="8pt" PageSize="9" Width="100%" PageIndex="4" AutoGenerateColumns="False">
                    <PagerSettings Visible="False" />
                    <HeaderStyle CssClass="titleOrangegrid" />
                    <Columns>                    
                    <asp:BoundField DataField="CM" HeaderText="Broker" />
                    <%--<asp:BoundField DataField="MonthN" HeaderText="Month" />--%>
                    <asp:BoundField DataField="AnsweredCalls" HeaderText="Calls" />
                    <asp:BoundField DataField="Minutes" HeaderText="Minutes" />
                    <asp:BoundField DataField="Profit" HeaderText="Profit" />
                    </Columns>                    
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="1" valign="top">
                <br />
            </td>
            <td align="center" colspan="3" valign="top">
                </td>
            <td align="center" colspan="1" valign="top">
                
            </td>
            <td align="center" colspan="1" valign="top">
            </td>
            <td align="center" colspan="1" valign="top">                
            </td>
            <td align="center" colspan="1" valign="top">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="5" style="height: 93%" valign="top">
                <asp:HiddenField ID="HiddenField1" runat="server" />
                <asp:HiddenField ID="HiddenField2" runat="server" />
                <asp:HiddenField ID="HiddenField3" runat="server" />
            </td>
            <td align="left" colspan="1" style="height: 93%" valign="top">
            </td>
            <td align="center" colspan="1" style="height: 93%" valign="top">
            </td>
            <td align="center" colspan="1" style="height: 93%" valign="top">
            </td>
        </tr>
    </table>
</asp:Content>
