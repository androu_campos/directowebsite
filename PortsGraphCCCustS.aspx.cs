using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Dundas.Charting.WebControl;


public partial class PortsGraphCCCust : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        DataTable tbl;

        if (!Page.IsPostBack)
        {
            txtFrom.Text = DateTime.Now.ToShortDateString();
            txtTo.Text = DateTime.Now.ToShortDateString();
        }
    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {
        DataTable tbl;
        string username = string.Empty;

        username = Session["DirectoUser"].ToString();

        string from = txtFrom.Text;
        string to = txtTo.Text;

        string agency = dpdAgen.SelectedValue.ToString();
        string minicall = dpdCC.SelectedValue.ToString();

        string query = string.Empty;
        string title = string.Empty;


        if (agency == "**ALL**" && minicall == "**ALL**")
        {
            query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] "
                   + "FROM [CDRDB].[dbo].[CDRN] "
                   + "WHERE [type] like 'C%' "
                   + "and [Cust_Vendor] in (select ip from cdrdb.dbo.providerip where (ProviderName like '%san%' or ProviderName like '%-sant%') AND ProviderName not in ('CC-CORTIZO-SANTANDER','CC-CORTIZO-SANTAN044','PALO-SANTO-TEST','SANTANDER') and [Type] like 'C%') "
                   + "and [Cust_Vendor] not in ('CCSN','CSA4') "
                   + "and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' "
                   + "group by [Time] order by [Time]";

            title = "Customer: " + username.ToUpper();
        }
         else if (agency != "**ALL**")
        {
            if (minicall != "**ALL**")
            {
                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN] WHERE [type] like 'C%' and [Cust_Vendor] in (select ip from cdrdb..providerip where ProviderName like '" + minicall + "' and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000'  group by [Time] order by [Time]";
                title = "Customer: " + minicall;
            }
            else
            {
                query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[CDRN]  WHERE [Cust_Vendor] in (select ip from cdrdb..providerip where (ProviderName like 'CC-" + agency + "SAN%' or ProviderName like 'CC-" + agency + "-SAN%') and [Type] like 'C%') and [Time] between '" + from + " 00:00:00.000' and '" + to + " 23:59:59.000' group by [Time] order by [Time]";
                title = "Customer: " + agency;
            }
        }

        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        if (tbl.Rows.Count <= 0) cmdDetail.Visible = false;
        else cmdDetail.Visible = true;

        makeChart(tbl, title);
        //gdvDetail.Visible = false;
        lblGrid.Visible = false;

    }

    private void makeChart(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart1.Titles.Add(tITLE);
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetail"] = tbl;

            this.Chart1.Visible = true;
            lblWarning.Visible = false;
            this.cmdDetail.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            lblWarning.Visible = true;
            Session["tblDetail"] = tbl;
            this.cmdDetail.Visible = false;

        }

    }
    protected void cmdDetail_Click(object sender, EventArgs e)
    {
        if (((DataTable)Session["tblDetail"]).Rows.Count > 0)
        {

            gdvDetail.DataSource = ((DataTable)Session["tblDetail"]);
            gdvDetail.DataBind();

            gdvDetail.Visible = true;
            lblGrid.Visible = true;
            lblGrid.Text = dpdCC.SelectedValue.ToString();
            cmdHide.Visible = true;
            cmdDetail.Visible = false;
        }
        else
        {
        }
    }
    protected void cmdHide_Click(object sender, EventArgs e)
    {
        gdvDetail.Visible = false;
        lblGrid.Visible = false;
        cmdDetail.Visible = true;
        cmdHide.Visible = false;

    }
    protected void dpdAgen_SelectedIndexChanged(object sender, EventArgs e)
    {

        dpdCC.Enabled = true;

        string agen = dpdAgen.SelectedValue.ToString();

        SqlConnection SqlConn1 = new SqlConnection();
        SqlConn1.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=CDRDB;User ID=steward_of_Gondor;Password=nm@73-mg";
        SqlCommand SqlCommand1 = new SqlCommand();

        if (dpdAgen.SelectedValue.ToString() == "VCIP")
        {
            SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE (PROVIDERNAME LIKE 'CC-" + agen + "SAN%' or PROVIDERNAME LIKE 'CC-" + agen + "-SAN%') AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
        }
        else
        {
            SqlCommand1.CommandText = "SELECT '**ALL**' PROVIDERNAME UNION SELECT ProviderName FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'CC-" + agen + "SAN%' AND TYPE = 'C' GROUP BY PROVIDERNAME ORDER BY PROVIDERNAME";
        }

        SqlCommand1.Connection = SqlConn1;
        SqlConn1.Open();

        dpdCC.Items.Clear();

        SqlDataReader myReader1 = SqlCommand1.ExecuteReader();

        while (myReader1.Read())
        {

            dpdCC.Items.Add(myReader1.GetValue(0).ToString());
        }
        myReader1.Close();
        SqlConn1.Close();
    }
}