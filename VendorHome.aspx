<%@ Page Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="VendorHome.aspx.cs"
    Inherits="VendorHome" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" CssClass="labelTurk" Text="Recent Traffic - Summary of the last 15 mins" Width="263px"></asp:Label>
            </td>            
        </tr>
        <tr>
            <td>
                <asp:Button ID="cmdExport1" runat="server" CssClass="boton" OnClick="cmdExport1_Click"
                    Text="Export To EXCEL" Width="85px" />
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:GridView ID="GridView1" runat="server" Font-Names="Arial" Font-Size="8pt" AutoGenerateColumns="true" HeaderStyle-Wrap="false" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="15">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" Wrap="False" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" Wrap="False" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" Wrap="False" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" Wrap="False" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>                
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 22px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label2" runat="server" Text="Today's Traffic before the last 15 mins" CssClass="labelTurk" Width="246px"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 100px" title="Recent traffic">
                <asp:Button ID="cmdExport2" runat="server" CssClass="boton" OnClick="cmdExport2_Click"
                    Text="Export to EXCEL" Width="82px" /></td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:GridView ID="GridView2" runat="server" Font-Names="Arial" Font-Size="8pt" AutoGenerateColumns="true" HeaderStyle-Wrap="false" AllowPaging="True" OnPageIndexChanging="GridView2_PageIndexChanging" PageSize="15">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" Wrap="False" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="titleOrangegrid" Font-Bold="True" Font-Names="Arial" Height="40px"
                        Font-Size="8pt" Wrap="False" />
                    <AlternatingRowStyle BackColor="White" />
                    
                    
                    <%--<Columns>
                    <asp:BoundField DataField="Customer" HeaderText="Customer" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Region" HeaderText="Region" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>    
                    <asp:BoundField DataField="Type" HeaderText="Type" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Minutes" HeaderText="Minutes" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Attempts" HeaderText="Attempts" >
                        <ItemStyle Wrap="False" />
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>                                   
                    </Columns>--%>
                </asp:GridView>                
            </td>
        </tr>        
    </table>
</asp:Content>    