using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using System.Net.Sockets;

/// <summary>
/// Summary description for Util
/// </summary>
public class Util
{
	public Util()
	{
		//
		// TODO: Add constructor logic here
		//      

	}

    public static string getNumberFormat(string inString,int flagy)
    {

        try
        {
            string number = inString;
            string partDecimal = string.Empty;

            if (flagy == 1)//DECIMAL
            {
                int indxpoint = number.IndexOf(".");
                partDecimal = number.Substring(indxpoint);
                number = number.Substring(0, indxpoint);
            }
            else if (flagy == 2)
            {
                int indxpoint = number.IndexOf(".");
                if ( indxpoint > 0)
                    number = number.Substring(0, indxpoint);
            }
            else
            {
                number = inString;
            }
            
            




            int lon = number.Length;
            int counter = 0;
            int c = lon - 1;
            int comas = 0;
            int residuo = 0;

            residuo = lon % 3;
            if (residuo > 0)
            {
                comas = (lon / 3);
                lon = lon + (lon / 3);
            }
            else
            {
                lon = lon + (lon / 3) - 1;
                comas = (lon / 3) - 1;
            }
            string[] s = new string[lon];
            int flag = 0;
            
            
            for (int i = 0; i < lon; i++)
            {
                counter = counter + 1;
                s[i] = number.Substring(c, 1);
                c = c - 1;


                if (counter == 3 && (flag < comas))
                { //set ,
                    s[i + 1] = ",";
                    i = i + 1;
                    counter = 0;
                    flag = flag + 1;

                }
                else
                {

                }

            }
            string lastString = string.Empty;
            for (int j = s.Length - 1; j >= 0; j--)
            {
                lastString = lastString + s[j].ToString();
            }


            if (flagy == 0 || flagy == 2)
            {
                return lastString;
            }            
            else 
            {
                lastString = lastString + partDecimal;
                return lastString;
            }

        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            return string.Empty;

        }

    
    }

    public static string getLiveTrafficWatch(string start, string end)
    {

        try
        {
            StreamReader FSOqtmon = new StreamReader(@"C:\Inetpub\wwwroot\qtmon.html", System.Text.ASCIIEncoding.Default);
            //
            if (start == "VSGO_D1" )
            {
                string newFile = string.Empty;
                newFile = FSOqtmon.ReadToEnd();
                
                int first = newFile.LastIndexOf(start);
                int last = newFile.IndexOf(end);
                string texto = newFile.Substring(first, last - first);
                int lastI = texto.IndexOf("<TR>");
                texto = texto.Substring(0, lastI - 1);
                //Extract the Ip of first element of list
                string allfirstIp = newFile.Substring(0, first);
                int indFirstIp = allfirstIp.LastIndexOf("<TR><TD><A HREF = telnet:");
                string chida = newFile.Substring(indFirstIp, last - indFirstIp);
                int indStrSeek = chida.IndexOf(start);
                chida = chida.Substring(0, indStrSeek);
                //end extract

                string html = "<table border = 1><TR><TD valign='top'>Node</TD><TD valign='top'>ASR</TD><TD valign='top'>ACD</TD><TD valign='top'>Calls</TD></TR>";
                html = html + chida + texto + "</table>";

                return html;
            }

            else if (start == "NTCQ_1" || start == "CCEN_82" || start == "SFRN_A1" || start == "SNMS_S1" || start == "GNAT_1" || start == "NOFF_1" || start == "VSGO_A2")
            {

                string newFile = string.Empty;
                newFile = FSOqtmon.ReadToEnd();
                int first = newFile.IndexOf(start);
                int last = newFile.LastIndexOf(end);
                int firsl = newFile.LastIndexOf("</R>");
                int lastT = newFile.LastIndexOf("</Table>");
                string texto = newFile.Substring(first, last - first);
                //set header of table
                string html = "<table border = 1><TR><TD valign='top'>Node</TD><TD valign='top'>ASR</TD><TD valign='top'>ACD</TD><TD valign='top'>Calls</TD></TR>";
                //Extract the Ip of first element of list
                string allfirstIp = newFile.Substring(0, first);
                int indFirstIp = allfirstIp.LastIndexOf("<TR><TD><A HREF = telnet:");
                string chida = newFile.Substring(indFirstIp, last - indFirstIp);
                int indStrSeek = chida.IndexOf(start);
                chida = chida.Substring(0, indStrSeek);
                //end extract
                string subsub = newFile.Substring(last, newFile.Length - last);
                int newInLast = subsub.IndexOf("</TR>");



                html = html + chida + texto + subsub.Substring(0,newInLast);
                    //newFile.Substring(last, lastT - firsl);
                html = html.Replace("\r\n", "");
                html = html + "</TR>";
                html = html + "</table>";
                return html;
                
            }
            else
            {
                string newFile = string.Empty;
                newFile = FSOqtmon.ReadToEnd();
                int first = newFile.IndexOf(start);
                int last = newFile.LastIndexOf(end);
                int firsl = newFile.LastIndexOf("</A>");
                int lastT = newFile.LastIndexOf("</Table>");
                string texto = newFile.Substring(first, last - first);
                //set header of table
                string html = "<table border = 1><TR><TD valign='top'>Node</TD><TD valign='top'>ASR</TD><TD valign='top'>ACD</TD><TD valign='top'>Calls</TD></TR>";
                //Extract the Ip of first element of list
                string allfirstIp = newFile.Substring(0, first);
                int indFirstIp = allfirstIp.LastIndexOf("<TR><TD><A HREF = telnet:");
                                                                 
                string chida = newFile.Substring(indFirstIp, last - indFirstIp);
                int indStrSeek = chida.IndexOf(start);
                chida = chida.Substring(0, indStrSeek);
                //end extract
                html = html + chida + texto + newFile.Substring(last, lastT - indFirstIp);
                html = html.Replace("\r\n", "");
                
                
                html = html + "</table>";
                
                html = html.Replace("<table border = 1  style=' font-size: 10pt;border-collapse: collapse'>", "");
                //              
                FSOqtmon.Close();
                return html;
            }
            
            
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            return string.Empty;
        }
    
    }

    public static DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        sqlQuery.CommandTimeout = 0;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch(Exception ex)
        {
            string m = ex.Message.ToString();
        }

        return resultsDataSet;
    }

    public static DataSet RunQueryatCDRDB(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        sqlQuery.CommandTimeout = 600;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

    public static string GetMonth(int numberMonth)
    {
        string monthName = string.Empty;
        try
        {


            if (numberMonth == 1)
            {
                monthName = "January";

            }
            else if (numberMonth == 2)
            {
                monthName = "February";

            }
            else if (numberMonth == 3)
            {
                monthName = "March";

            }
            else if (numberMonth == 4)
            {
                monthName = "April";

            }
            else if (numberMonth == 5)
            {
                monthName = "May";

            }
            else if (numberMonth == 6)
            {
                monthName = "June";

            }
            else if (numberMonth == 7)
            {
                monthName = "July";

            }
            else if (numberMonth == 8)
            {
                monthName = "August";

            }
            else if (numberMonth == 9)
            {
                monthName = "September";

            }
            else if (numberMonth == 10)
            {
                monthName = "October";

            }
            else if (numberMonth == 11)
            {
                monthName = "November";

            }
            else if (numberMonth == 12)
            {
                monthName = "December";
            }

            return monthName;
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
            return string.Empty;                        
        }



    }

    public static DataSet RunQueryByStmnt(string Query)
    {
        SqlCommand qry = new SqlCommand();
        qry.CommandType = CommandType.Text;
        qry.CommandText = Query;
        qry.CommandTimeout = 0;

        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = qry;
        qry.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch(Exception ex)
        {
            string error = ex.Message.ToString();
        }

        return resultsDataSet;
    }
    
    public static void RunQry(string Query)
    {
        SqlCommand qry = new SqlCommand();
        qry.CommandType = CommandType.Text;
        qry.CommandText = Query;


        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = qry;
        qry.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            Query = Query + "";
        }
        catch(Exception ex)
        {
            string error = ex.Message.ToString();
        }

        
    }

    public static void RunQryAtCDRDB_void(string Query)
    {
        SqlCommand qry = new SqlCommand();
	qry.CommandTimeout = 5000;
        qry.CommandType = CommandType.Text;
        qry.CommandText = Query;


        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = qry;
        qry.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }


    }

    public static DataSet RunQueryByStmntatCDRDB(string Query)
    {
        SqlCommand qry = new SqlCommand();
        qry.CommandType = CommandType.Text;
        qry.CommandText = Query;


        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = qry;
        qry.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }

        return resultsDataSet;
    }


    public static DataSet RunQueryByStmntatMECCA(string Query)
    {
        SqlCommand qry = new SqlCommand();
        qry.CommandType = CommandType.Text;
        qry.CommandText = Query;


        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = qry;
        qry.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }

        return resultsDataSet;
    }

    public static DataSet RunQryVoidatDISPUTES(string Query)
    {
        SqlCommand qry = new SqlCommand();
        qry.CommandType = CommandType.Text;
        qry.CommandText = Query;
        qry.CommandTimeout = 0;

        string connectionString = ConfigurationManager.ConnectionStrings
        ["DISPUTESConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = qry;
        qry.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }

        return resultsDataSet;
    }
    public static void RunQuery2(SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["DirectoReportsConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        DBConnection.Open();
        sqlQuery.Connection = DBConnection;
        sqlQuery.CommandTimeout = 666;
        sqlQuery.ExecuteNonQuery();
    }
}
