<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DynamicData.aspx.cs" Inherits="DynamicData" StylesheetTheme="Theme1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title> Dynamic Data </title>
</head>
<body bottommargin="0" rightmargin="0" topmargin="0" leftmargin="0">
    <form id="form1" runat="server">
    <div>
    <table border="1" style="height: 45px" width="177" id="mainTable" runat="server">
            <tr>
                <td align="center" bgcolor="#d5e3f0" width="15%">
                    <asp:Label ID="Label1" runat="server" CssClass="labelBlue8" Text="Calls"></asp:Label></td>
                <td width="85%">
                    <asp:TextBox ID="txtCalls" runat="server" CssClass="dropdown" Width="108px" ForeColor="#004A8F"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" bgcolor="#d5e3f0" width="15%">
                    <asp:Label ID="Label2" runat="server" CssClass="labelBlue8" Text="Datetime"></asp:Label></td>
                <td width="85%">
                    <asp:TextBox ID="txtDate" runat="server" CssClass="dropdown" Width="107px" ForeColor="#004A8F"></asp:TextBox></td>
            </tr>
        </table>
        </div>
        
    </form>
</body>
</html>
