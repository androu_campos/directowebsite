<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CustVendCatalog.aspx.cs"
    Inherits="AssignBroker" StylesheetTheme="Theme1" Title="DIRECTO - Connections Worldwide"
    EnableViewState="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="x">
    </asp:ScriptManager>
    <table align="left">
        <tr>
            <td align="left" valign="top" bgcolor="#006699">
                <asp:Label ID="LabelS1" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                    Text="ID :"></asp:Label>
            </td>
            <td align="left" style="width: 20px" valign="top">
                <asp:TextBox ID="txtID" runat="server"></asp:TextBox>
                <cc1:MaskedEditExtender ID="maskid" runat="server" Mask="LLLL" TargetControlID="txtID">
                </cc1:MaskedEditExtender>
            </td>
            <td>
                <asp:Button ID="btnAcep" runat="server" Text="Add" OnClick="btnAcep_Click" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#006699">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                    Text="Name :"></asp:Label>
            </td>
            <td align="left" style="width: 20px" valign="top">
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#006699">
                <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                    Text="Type :"></asp:Label>
            </td>
            <td align="left" style="width: 20px" valign="top">
                <asp:DropDownList ID="ddlType" runat="server">
                    <asp:ListItem Text="Customer" Value="C"></asp:ListItem>
                    <asp:ListItem Text="Vendor" Value="V"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#006699">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                    Text="Gateway :"></asp:Label>
            </td>
            <td align="left" style="width: 20px" valign="top">
                <asp:TextBox ID="txtGateway" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#006699">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                    Text="VPort :"></asp:Label>
            </td>
            <td align="left" style="width: 20px" valign="top">
                <asp:TextBox ID="txtVport" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#006699">
                <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                    Text="IP :"></asp:Label>
            </td>
            <td align="left" style="width: 20px" valign="top">
                <asp:TextBox ID="txtIP" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#006699">
                <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                    Text="Prefix :"></asp:Label>
            </td>
            <td align="left" style="width: 20px" valign="top">
                <asp:TextBox ID="txtPrefix" runat="server"></asp:TextBox>
                <cc1:MaskedEditExtender ID="maskPrefix" runat="server" Mask="9999" TargetControlID="txtPrefix"
                    ClipboardEnabled="true">
                </cc1:MaskedEditExtender>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#006699">
                <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                    Text="Calling Plan :"></asp:Label>
            </td>
            <td align="left" style="width: 20px" valign="top">
                <asp:TextBox ID="txtCP" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#006699">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                    Text="Nextone ID :"></asp:Label>
            </td>
            <td align="left" style="width: 20px" valign="top">
                <asp:DropDownList ID="ddlNextoneID" runat="server">
                    <asp:ListItem Text="MIA02" Value="MIA02"></asp:ListItem>
                    <asp:ListItem Text="MIA03" Value="MIA03"></asp:ListItem>
                    <asp:ListItem Text="MIA04" Value="MIA04"></asp:ListItem>
                    <asp:ListItem Text="MIA06" Value="MIA06"></asp:ListItem>
                    <asp:ListItem Text="MIA08" Value="MIA08"></asp:ListItem>
                    <asp:ListItem Text="STCB" Value="STCB"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <table>
        <tr>
            <td align="left" style="width: 401px" valign="top">
                <asp:Button ID="btnShowC" runat="server" OnClick="btnShowC_Click" Text="Show Customers"
                    Width="200px" /></td>
            <td align="left" style="width: 401px" valign="top">
                <asp:Button ID="btnShowV" runat="server" OnClick="btnShowV_Click" Text="Show Vendors"
                    Width="200px" /></td>
        </tr>
    </table>
    <table>
        <tr>
            <td align="left" style="width: 401px" valign="top" colspan="2">
                <asp:GridView ID="gvCust" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"
                    Font-Names="Arial" OnRowEditing="gvCust_RowEditing" OnRowDataBound="gvCust_RowDataBound"
                    EnableModelValidation="True" OnRowUpdating="gvCust_RowUpdating" OnRowCancelingEdit="gvCust_RowCancelingEdit"
                    Visible="False" AutoGenerateColumns="False">
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <EditRowStyle BackColor="#999999" ForeColor="White" Font-Bold="False" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:CommandField ShowEditButton="True" ButtonType="Button"
                            ShowDeleteButton="True" CausesValidation="False" />
                        <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" />
                        <asp:TemplateField HeaderText="Name" >
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVName" runat="server" Text='<%#Bind ("Name") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVName" runat="server" Text='<%#Bind ("Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Gateway">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVGateway" runat="server" Text='<%#Bind ("Gateway") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVGateway" runat="server" Text='<%#Bind ("Gateway") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VPort">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVVPort" runat="server" Text='<%#Bind ("VPort") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVVPort" runat="server" Text='<%#Bind ("VPort") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IP">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVIP" runat="server" Text='<%#Bind ("IP") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVIP" runat="server" Text='<%#Bind ("IP") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Prefix">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVPrefix" runat="server" Text='<%#Bind ("Prefix") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVPrefix" runat="server" Text='<%#Bind ("Prefix") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CallingPlan">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVCallingPlan" runat="server" ></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGVCallingPlan" runat="server" Text='<%#Bind ("CallingPlan") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nextone ID">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddledNID" runat="server" EnableViewState="true">
                                    <asp:ListItem Text="MIA02" Value="MIA02"></asp:ListItem>
                                    <asp:ListItem Text="MIA03" Value="MIA03"></asp:ListItem>
                                    <asp:ListItem Text="MIA04" Value="MIA04"></asp:ListItem>
                                    <asp:ListItem Text="MIA06" Value="MIA06"></asp:ListItem>
                                    <asp:ListItem Text="MIA08" Value="MIA08"></asp:ListItem>
                                    <asp:ListItem Text="STCB" Value="STCB"></asp:ListItem>
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblNxtID" runat="server" Text='<%#Bind ("NextoneID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
            <td align="left" style="width: 401px" valign="top">
                <asp:GridView ID="gvVend" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"
                    Font-Names="Arial">
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <EditRowStyle BackColor="#999999" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
