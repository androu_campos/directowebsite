<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GraphGridSipCause2.aspx.cs" Inherits="GraphGrid" Title="DIRECTO - Connections Worldwide"  StylesheetTheme="Theme1"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager id="ScriptManager1" runat="server"></asp:ScriptManager>

    <h2 style="color: #004D88; font-family:Arial; font-weight:bold; font-size:14pt">SIP RESPONSE CODES - TOP CUSTOMERS PANEL</h2>

    
    <div>
        <asp:Label ID="SipList" runat="server" Text="" style="font-size: 14pt"></asp:Label>
        <br /><asp:Label ID="SipList100" runat="server" Text="" style="font-size: 14pt"></asp:Label>
        <br /><asp:Label ID="SipList200" runat="server" Text="" style="font-size: 14pt"></asp:Label>
        <br /><asp:Label ID="SipList300" runat="server" Text="" style="font-size: 14pt"></asp:Label>
        <br /><asp:Label ID="SipList400" runat="server" Text="" style="font-size: 14pt"></asp:Label>
        <br /><asp:Label ID="SipList500" runat="server" Text="" style="font-size: 14pt"></asp:Label>
        <br /><asp:Label ID="SipList600" runat="server" Text="" style="font-size: 14pt"></asp:Label>
    </div>

    <br />
    <table>
        <tr>
            <td>
                <DCWC:Chart ID="Chart1" runat="server" Height="400px" Width="700px" ImageUrl="~/TempImages/ChartPic_#SEQ(300,3)"  BackColor="#F3DFC1" BorderLineStyle="Solid" BackGradientType="TopBottom" Palette="Dundas" BorderLineColor="181, 64, 1" borderlinewidth="2">
                    <Titles>
						<dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Column Chart" Name="Title1" Color="26, 59, 105" Alignment="TopLeft"></dcwc:Title>
					</Titles>
                    <Legends>
						<dcwc:Legend LegendStyle="Row" AutoFitText="False" Docking="Top" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold" Alignment="Far">
							<Position Y="4" Height="7.653602" Width="49.60493" X="48.65784"></Position>
						</dcwc:Legend>
					</Legends>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <Series>
                        <dcwc:Series YValuesPerPoint="4" Name="Attempts Customer" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"></dcwc:Series>
                        <dcwc:Series YValuesPerPoint="4" ChartArea="GroupedData" Name="Attempts Vendor" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65"></dcwc:Series>                        
					</Series>
                    <ChartAreas>
                        <dcwc:ChartArea Name="Default" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="12" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
						<dcwc:ChartArea Name="GroupedData" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent" AlignWithChartArea="Default">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="50" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
                    </ChartAreas>
                </DCWC:Chart>        
            </td>
            <td>
                <DCWC:Chart ID="Chart2" runat="server" Height="400px" Width="750px" ImageUrl="~/TempImages/ChartPic_#SEQ(300,3)" BackColor="#F3DFC1" BorderLineStyle="Solid" BackGradientType="TopBottom" Palette="Dundas" BorderLineColor="181, 64, 1" borderlinewidth="2">
                    <Titles>
						<dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Column Chart" Name="Title1" Color="26, 59, 105" Alignment="TopLeft"></dcwc:Title>
					</Titles>
                    <Legends>
						<dcwc:Legend LegendStyle="Row" AutoFitText="False" Docking="Top" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold" Alignment="Far">
							<Position Y="4" Height="7.653602" Width="49.60493" X="48.65784"></Position>
						</dcwc:Legend>
					</Legends>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <Series>
                        <dcwc:Series YValuesPerPoint="4" Name="Attempts Customer" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"></dcwc:Series>
                        <dcwc:Series YValuesPerPoint="4" ChartArea="GroupedData" Name="Attempts Vendor" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65"></dcwc:Series>                        
					</Series>
                    <ChartAreas>
                        <dcwc:ChartArea Name="Default" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="12" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
						<dcwc:ChartArea Name="GroupedData" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent" AlignWithChartArea="Default">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="50" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
                    </ChartAreas>
                </DCWC:Chart>        
            </td>
            <td>
                <DCWC:Chart ID="Chart3" runat="server" Height="400px" Width="750px" ImageUrl="~/TempImages/ChartPic_#SEQ(300,3)" BackColor="#F3DFC1" BorderLineStyle="Solid" BackGradientType="TopBottom" Palette="Dundas" BorderLineColor="181, 64, 1" borderlinewidth="2">
                    <Titles>
						<dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Column Chart" Name="Title1" Color="26, 59, 105" Alignment="TopLeft"></dcwc:Title>
					</Titles>
                    <Legends>
						<dcwc:Legend LegendStyle="Row" AutoFitText="False" Docking="Top" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold" Alignment="Far">
							<Position Y="4" Height="7.653602" Width="49.60493" X="48.65784"></Position>
						</dcwc:Legend>
					</Legends>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <Series>
                        <dcwc:Series YValuesPerPoint="4" Name="Attempts Customer" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"></dcwc:Series>
                        <dcwc:Series YValuesPerPoint="4" ChartArea="GroupedData" Name="Attempts Vendor" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65"></dcwc:Series>                        
					</Series>
                    <ChartAreas>
                        <dcwc:ChartArea Name="Default" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="12" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
						<dcwc:ChartArea Name="GroupedData" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent" AlignWithChartArea="Default">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="50" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
                    </ChartAreas>
                </DCWC:Chart>        
            </td>
			<%--<td>
                <DCWC:Chart ID="Chart7" runat="server" Height="400px" Width="750px" ImageUrl="~/TempImages/ChartPic_#SEQ(300,3)" BackColor="#F3DFC1" BorderLineStyle="Solid" BackGradientType="TopBottom" Palette="Dundas" BorderLineColor="181, 64, 1" borderlinewidth="2">
                    <Titles>
						<dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Column Chart" Name="Title1" Color="26, 59, 105" Alignment="TopLeft"></dcwc:Title>
					</Titles>
                    <Legends>
						<dcwc:Legend LegendStyle="Row" AutoFitText="False" Docking="Top" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold" Alignment="Far">
							<Position Y="4" Height="7.653602" Width="49.60493" X="48.65784"></Position>
						</dcwc:Legend>
					</Legends>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <Series>
                        <dcwc:Series YValuesPerPoint="4" Name="Attempts Customer" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"></dcwc:Series>
                        <dcwc:Series YValuesPerPoint="4" ChartArea="GroupedData" Name="Attempts Vendor" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65"></dcwc:Series>                        
					</Series>
                    <ChartAreas>
                        <dcwc:ChartArea Name="Default" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="12" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
						<dcwc:ChartArea Name="GroupedData" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent" AlignWithChartArea="Default">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="50" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
                    </ChartAreas>
                </DCWC:Chart>        
            </td>  --%>
        </tr>
        <tr>
            <td>
                <DCWC:Chart ID="Chart4" runat="server" Height="400px" Width="700px" ImageUrl="~/TempImages/ChartPic_#SEQ(300,3)" BackColor="#F3DFC1" BorderLineStyle="Solid" BackGradientType="TopBottom" Palette="Dundas" BorderLineColor="181, 64, 1" borderlinewidth="2">
                    <Titles>
						<dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Column Chart" Name="Title1" Color="26, 59, 105" Alignment="TopLeft"></dcwc:Title>
					</Titles>
                    <Legends>
						<dcwc:Legend LegendStyle="Row" AutoFitText="False" Docking="Top" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold" Alignment="Far">
							<Position Y="4" Height="7.653602" Width="49.60493" X="48.65784"></Position>
						</dcwc:Legend>
					</Legends>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <Series>
                        <dcwc:Series YValuesPerPoint="4" Name="Attempts Customer" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"></dcwc:Series>
                        <dcwc:Series YValuesPerPoint="4" ChartArea="GroupedData" Name="Attempts Vendor" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65"></dcwc:Series>                        
					</Series>
                    <ChartAreas>
                        <dcwc:ChartArea Name="Default" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="12" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
						<dcwc:ChartArea Name="GroupedData" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent" AlignWithChartArea="Default">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="50" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
                    </ChartAreas>
                </DCWC:Chart>        
            </td>
            <td>
                <DCWC:Chart ID="Chart5" runat="server" Height="400px" Width="750px" ImageUrl="~/TempImages/ChartPic_#SEQ(300,3)" BackColor="#F3DFC1" BorderLineStyle="Solid" BackGradientType="TopBottom" Palette="Dundas" BorderLineColor="181, 64, 1" borderlinewidth="2">
                    <Titles>
						<dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Column Chart" Name="Title1" Color="26, 59, 105" Alignment="TopLeft"></dcwc:Title>
					</Titles>
                    <Legends>
						<dcwc:Legend LegendStyle="Row" AutoFitText="False" Docking="Top" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold" Alignment="Far">
							<Position Y="4" Height="7.653602" Width="49.60493" X="48.65784"></Position>
						</dcwc:Legend>
					</Legends>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <Series>
                        <dcwc:Series YValuesPerPoint="4" Name="Attempts Customer" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"></dcwc:Series>
                        <dcwc:Series YValuesPerPoint="4" ChartArea="GroupedData" Name="Attempts Vendor" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65"></dcwc:Series>                        
					</Series>
                    <ChartAreas>
                        <dcwc:ChartArea Name="Default" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="12" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
						<dcwc:ChartArea Name="GroupedData" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent" AlignWithChartArea="Default">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="50" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
                    </ChartAreas>
                </DCWC:Chart>        
            </td>
            <td>
                <DCWC:Chart ID="Chart6" runat="server" Height="400px" Width="750px" ImageUrl="~/TempImages/ChartPic_#SEQ(300,3)" BackColor="#F3DFC1" BorderLineStyle="Solid" BackGradientType="TopBottom" Palette="Dundas" BorderLineColor="181, 64, 1" borderlinewidth="2">
                    <Titles>
						<dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Column Chart" Name="Title1" Color="26, 59, 105" Alignment="TopLeft"></dcwc:Title>
					</Titles>
                    <Legends>
						<dcwc:Legend LegendStyle="Row" AutoFitText="False" Docking="Top" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold" Alignment="Far">
							<Position Y="4" Height="7.653602" Width="49.60493" X="48.65784"></Position>
						</dcwc:Legend>
					</Legends>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <Series>
                        <dcwc:Series YValuesPerPoint="4" Name="Attempts Customer" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"></dcwc:Series>
                        <dcwc:Series YValuesPerPoint="4" ChartArea="GroupedData" Name="Attempts Vendor" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65"></dcwc:Series>                        
					</Series>
                    <ChartAreas>
                        <dcwc:ChartArea Name="Default" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="12" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
						<dcwc:ChartArea Name="GroupedData" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent" AlignWithChartArea="Default">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="50" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
                    </ChartAreas>
                </DCWC:Chart>        
            </td>
			<%--<td>
                <DCWC:Chart ID="Chart8" runat="server" Height="400px" Width="750px" ImageUrl="~/TempImages/ChartPic_#SEQ(300,3)" BackColor="#F3DFC1" BorderLineStyle="Solid" BackGradientType="TopBottom" Palette="Dundas" BorderLineColor="181, 64, 1" borderlinewidth="2">
                    <Titles>
						<dcwc:Title ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Column Chart" Name="Title1" Color="26, 59, 105" Alignment="TopLeft"></dcwc:Title>
					</Titles>
                    <Legends>
						<dcwc:Legend LegendStyle="Row" AutoFitText="False" Docking="Top" Name="Default" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold" Alignment="Far">
							<Position Y="4" Height="7.653602" Width="49.60493" X="48.65784"></Position>
						</dcwc:Legend>
					</Legends>
                    <BorderSkin SkinStyle="Emboss"></BorderSkin>
                    <Series>
                        <dcwc:Series YValuesPerPoint="4" Name="Attempts Customer" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240"></dcwc:Series>
                        <dcwc:Series YValuesPerPoint="4" ChartArea="GroupedData" Name="Attempts Vendor" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65"></dcwc:Series>                        
					</Series>
                    <ChartAreas>
                        <dcwc:ChartArea Name="Default" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="12" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
						<dcwc:ChartArea Name="GroupedData" BorderColor="64, 64, 64, 64" BorderStyle="Solid" BackGradientEndColor="White" BackColor="OldLace" ShadowColor="Transparent" AlignWithChartArea="Default">
							<Area3DStyle YAngle="10" Perspective="10" XAngle="15" RightAngleAxes="False" WallWidth="0" Clustered="True"></Area3DStyle>
							<Position Y="50" Height="39" Width="89.43796" X="2.0"></Position>
							<AxisY LineColor="64, 64, 64, 64" LabelsAutoFit="False" StartFromZero="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisY>
							<AxisX LineColor="64, 64, 64, 64" LabelsAutoFit="False">
								<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Interval="1"></LabelStyle>
								<MajorGrid LineColor="64, 64, 64, 64"></MajorGrid>
							</AxisX>
						</dcwc:ChartArea>
                    </ChartAreas>
                </DCWC:Chart>        
            </td> --%> 
        </tr>        
        <tr>
            <td style="width: 106px; height: 44px;" valign="top">
                <asp:Timer ID="Timer1" runat="server" Interval="300000">
                </asp:Timer>
            </td>
        </tr>
    </table>

</asp:Content>

