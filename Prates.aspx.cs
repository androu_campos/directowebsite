using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;

public partial class Prates : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TextBox1.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            TextBox2.Text = DateTime.Now.AddDays(-1).ToShortDateString();
        }


    }
    protected void cmdShow_Click(object sender, EventArgs e)
    {        
        string customer = Session["DirectoUser"].ToString();
        string vendor = Session["DirectoUser"].ToString();
        SqlCommand sqlQry = new SqlCommand();        
        sqlQry.CommandType = CommandType.Text;

        DateTime dt = Convert.ToDateTime(TextBox1.Text);
        DateTime dtaux = DateTime.Now.AddDays(-16);

        TimeSpan tm = dtaux - dt;

        if (Session["DirectoUser"].ToString() == "ALL-ACCESS-PHIL")
        {
            customer = "ALL-ACCESS-PHIL' OR Customer ='ALLACCESSPH-SCRATCH' OR Customer = 'ALLACCESSPH-PINLESS";
            vendor = "ALL-ACCESS-PHIL' OR Vendor ='ALLACCESSPH-SCRATCH' OR Vendor = 'ALLACCESSPH-PINLESS";
        }

        if(customer == "JollyC")
        {
            customer = "ALL-ACCESS-PHIL' OR Customer ='ALLACCESSPH-SCRATCH' OR Customer = 'ALLACCESSPH-PINLESS";
            vendor = "ALL-ACCESS-PHIL' OR Vendor ='ALLACCESSPH-SCRATCH' OR Vendor = 'ALLACCESSPH-PINLESS";
        }

        if (Session["DirectoUser"].ToString() == "lpicache")
        {
          //provider = "PHIP-CALLCENTER";
            customer = "ALLACCESSPH-SCRATCH' OR Customer = 'ALLACCESSPH-PINLESS";
            vendor = "ALLACCESSPH-SCRATCH' OR Customer = 'ALLACCESSPH-PINLESS";
            
        }
    
        
        if (CheckBox1.Checked == true)
        {
            if (Session["DirectoUser"].ToString() == "ALL-ACCESS-PHIL")
            {
                sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Termination' as Direction, Vendor, Region, sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD  from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Customer = '" + customer + "' group by BillingDate, Vendor, region,  having sum(CustMinutes) > 0 union select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Termination' as Direction, Vendor, Region, sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Vendor = '" + vendor + "' group by BillingDate,Vendor,region, cost  having sum(CustMinutes) > 0 order by Direction";
            }
            else if (Session["Directouser"].ToString() == "csolares")
            {
                sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, Customer, Vendor, Country, Region, Type, Class,round(Sum(CustMinutes),2) as TotalMinutes ,sum(tcalls)AS Attempts,sum(BillableCalls) AS [Answered calls],sum(RA) AS [Rejected calls],mecca2.dbo.fGetASR(SUM(tcalls), SUM(BillableCalls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(CustMinutes), SUM(tcalls)) as ACD,mecca2.dbo.fGetABR(SUM(tcalls),SUM(BillableCalls)) AS ABR from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' AND Country in ('GUATEMALA','EL SALVADOR') group by CONVERT(VARCHAR(11) ,Billingdate,101), Customer, Vendor, Country, Region, Type, Class union select '*****' as BillingDate, '*****' Customer, '*****' Vendor, '*****' Country, '*****' Region, '*****' Type, '*****' Class, sum(tcalls)AS Attempts, sum(tcalls) AS [Answered calls],sum(RA) AS [Rejected calls], round(Sum(CustMinutes),2) as TotalMinutes, mecca2.dbo.fGetASR(SUM(tcalls), SUM(BillableCalls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(CustMinutes), SUM(BillableCalls)) as ACD,mecca2.dbo.fGetABR(SUM(tcalls),SUM(BillableCalls)) AS ABR from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' AND COUNTRY IN ('GUATEMALA','EL SALVADOR') ORDER by CONVERT(VARCHAR(11) ,Billingdate,101), Country, Region";
            }
            else if (Session["DirectoUser"].ToString() == "AFFINITY-VOIP")
            {
                if(tm.TotalDays >= 1)
                    TextBox1.Text = dtaux.ToShortDateString();
                sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, Country, Region, Price, sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Customer = '" + customer + "' group by BillingDate, Country, region,Price  having sum(CustMinutes) > 0 union select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, Country, Region, Price, sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Vendor = '" + vendor + "' group by BillingDate,Country, region,Price  having sum(CustMinutes) > 0 ";
            }
            else if (Session["DirectoUser"].ToString() == "divulge-telecom" || Session["DirectoUser"].ToString() == "adiptel") // Customer
            {
                sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Origination' as Direction, Country, Region, sum(CustMinutes)as TotalMinutes,sum(TotalPrice) as TotalPrice,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD  from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Customer = '" + customer + "' group by Country, region, BillingDate having sum(CustMinutes) > 0 ";
            }
            else if (Session["DirectoUser"].ToString() == "ranaya-minute-serv") // Vendor
            {
                sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Origination' as Direction, Country, Region, sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD  from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Vendor = '" + customer + "' group by Country,region, BillingDate having sum(CustMinutes) > 0 ";
            }
            else
            {
                sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Origination' as Direction, Region, sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD  from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Customer = '" + customer + "' group by region, BillingDate having sum(CustMinutes) > 0 union select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Termination' as Direction, Region, sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Vendor = '" + vendor + "' group by region, cost,BillingDate  having sum(CustMinutes) > 0 order by Direction";
            }
            //sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Origination' as Direction, Region, sum(CustMinutes)as TotalMinutes,sum(BillableCalls) as CompletedCalls from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Customer = '" + customer + "' group by region, BillingDate having sum(CustMinutes) > 0 union select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Termination' as Direction, Cost as Rate, Region, sum(CustMinutes)as TotalMinutes,sum(BillableCalls) as CompletedCalls from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Vendor = '" + vendor + "' group by region, cost,BillingDate  having sum(CustMinutes) > 0 order by Direction";
            // Price as Rate,
            //group by Price,
        }
        else
        {
            if (Session["DirectoUser"].ToString() == "ALL-ACCESS-PHIL")
            {
                sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Termination' as Direction, Vendor, sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Customer = '" + customer + "' group by BillingDate,Vendor having sum(CustMinutes) > 0 union select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate,'Termination' as Direction,Vendor,sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Vendor = '" + vendor + "' group by BillingDate,Vendor   having sum(CustMinutes) > 0 order by Direction";
            }
            else if (Session["Directouser"].ToString() == "csolares")
            {
                sqlQry.CommandText = "SELECT CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, Customer, Vendor, Type, Class,round(Sum(CustMinutes),2) as TotalMinutes ,sum(tcalls)AS Attempts,sum(BillableCalls) AS [Answered calls],sum(RA) AS [Rejected calls],mecca2.dbo.fGetASR(SUM(tcalls), SUM(BillableCalls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(CustMinutes), SUM(tcalls)) as ACD,mecca2.dbo.fGetABR(SUM(tcalls),SUM(BillableCalls)) AS ABR from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' AND Country in ('GUATEMALA','EL SALVADOR') group by CONVERT(VARCHAR(11) ,Billingdate,101), Customer, Vendor, Type, Class union select '*****' as BillingDate, '*****' Customer, '*****' Vendor, '*****' Type, '*****' Class, sum(tcalls)AS Attempts, sum(tcalls) AS [Answered calls],sum(RA) AS [Rejected calls], round(Sum(CustMinutes),2) as TotalMinutes, mecca2.dbo.fGetASR(SUM(tcalls), SUM(BillableCalls), SUM(RA)) as ASR, mecca2.dbo.fGetACD(SUM(CustMinutes), SUM(BillableCalls)) as ACD,mecca2.dbo.fGetABR(SUM(tcalls),SUM(BillableCalls)) AS ABR from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' AND COUNTRY IN ('GUATEMALA','EL SALVADOR') ORDER by CONVERT(VARCHAR(11) ,Billingdate,101)";
            }
            else if (Session["DirectoUser"].ToString() == "divulge-telecom" || Session["DirectoUser"].ToString() == "adiptel") // Customer
            {
                sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Origination' as Direction, Country, Region, sum(CustMinutes)as TotalMinutes,sum(TotalPrice) as TotalPrice,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD  from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Customer = '" + customer + "' group by Country, region, BillingDate having sum(CustMinutes) > 0 ";
            }
            else if (Session["DirectoUser"].ToString() == "ranaya-minute-serv")
            {
                sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Origination' as Direction, sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD  from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Vendor = '" + customer + "' group by Country, region, BillingDate having sum(CustMinutes) > 0 ";
            }
            else
            {
                sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Origination' as Direction, sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Customer = '" + customer + "' group by BillingDate having sum(CustMinutes) > 0 union select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate,'Termination' as Direction,sum(CustMinutes)as TotalMinutes,sum(TotalCost) as TotalCost,sum(BillableCalls) as CompletedCalls,SUM(RA) as [Rejected Calls],mecca2.dbo.fGetASR(SUM(tCalls), SUM(BillableCalls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(tCalls), SUM(BillableCalls)) AS ABR,mecca2.dbo.fGetACD(SUM(VendorMinutes), SUM(BillableCalls)) as ACD from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Vendor = '" + vendor + "' group by BillingDate   having sum(CustMinutes) > 0 order by Direction";
            }
            //sqlQry.CommandText = "select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate, 'Origination' as Direction, sum(CustMinutes)as TotalMinutes,sum(BillableCalls) as CompletedCalls from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Customer = '" + customer + "' group by BillingDate having sum(CustMinutes) > 0 union select CONVERT(VARCHAR(11) ,Billingdate,101) as BillingDate,'Termination' as Direction, Cost as Rate, sum(CustMinutes)as TotalMinutes,sum(BillableCalls) as CompletedCalls from mecca2.dbo.TrafficLog where BillingDate >= '" + TextBox1.Text + "' and BillingDate <= '" + TextBox2.Text + "' and Vendor = '" + vendor + "' group by BillingDate   having sum(CustMinutes) > 0 order by Direction";
            // Price as Rate
            //group by Price,
        }
        //Response.Write(sqlQry.CommandText.ToString());

        DataSet myDataset = RunQuery(sqlQry);
        Session["Prates"] = myDataset;
        if (myDataset.Tables[0].Rows.Count > 0)
        {
            GridView1.DataSource = myDataset.Tables[0];
            GridView1.DataBind();
            cmdExport.Visible = true;
            Label4.Visible = false;
        }
        else
        {
            cmdExport.Visible = false;
            Label4.Visible = true;
        }
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
    protected void cmdExport_Click(object sender, EventArgs e)
    {
        // Export all the details
        try
        {
         DataTable dtEmployee = ((DataSet)Session["Prates"]).Tables[0].Copy();
         // Export all the details to CSV
         RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");                  
         string filename = "OlderTraffic" + ".xls";
         objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception Ex)
        {
            string message = Ex.Message.ToString();
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = Session["Prates"];
        GridView1.DataBind();
    }
}
