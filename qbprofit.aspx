<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="qbprofit.aspx.cs" Inherits="qbprofit" Title="DIRECTO - Connections Worldwide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="left">
        <tr>
            <td style="width: 209px" align="right">
                <asp:Label ID="Label1" runat="server" Text="Profits Trends Analyzer" CssClass="lblHeader"></asp:Label></td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 209px; height: 30px">
            </td>
            <td style="width: 100px; height: 30px">
            </td>
            <td style="width: 100px; height: 30px">
            </td>
        </tr>
        <tr>
            <td style="width: 209px" align="right">
                <asp:Label ID="Label2" runat="server" Text="Customer" CssClass="labelSteelBlue"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdCustomer" runat="server" CssClass="dropdown" Width="126px">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 209px" align="right">
                <asp:Label ID="Label3" runat="server" Text="Vendor" CssClass="labelSteelBlue"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdVendor" runat="server" CssClass="dropdown" Width="126px">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 209px" align="right">
                <asp:Label ID="Label4" runat="server" Text="Country" CssClass="labelSteelBlue"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdCountry" runat="server" CssClass="dropdown" Width="126px">
                    <asp:ListItem>**DONT SHOW**</asp:ListItem>
                    <asp:ListItem>**SHOW ALL**</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 209px" align="right">
                <asp:Label ID="Label5" runat="server" Text="Regions" CssClass="labelSteelBlue"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdRegions" runat="server" CssClass="dropdown" Width="126px">
                    <asp:ListItem>**DONT SHOW**</asp:ListItem>
                    <asp:ListItem>**SHOW ALL**</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 209px" align="right">
                <asp:Label ID="Label6" runat="server" Text="Report Type" CssClass="labelSteelBlue"></asp:Label></td>
            <td style="width: 100px" align="left">
                <asp:DropDownList ID="dpdTypeReport" runat="server" CssClass="dropdown" Width="126px">
                    <asp:ListItem Value="1">Daily</asp:ListItem>
                    <asp:ListItem Value="2">Weekly</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 209px">
            </td>
            <td align="left" style="width: 100px">
                <asp:Button ID="Button1" runat="server" CssClass="boton" Text="Button" OnClick="Button1_Click" /></td>
            <td style="width: 100px">
                <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Button" /></td>
        </tr>
    </table>
</asp:Content>

