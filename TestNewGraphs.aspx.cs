using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;
using System.Data.SqlClient;

public partial class TestNewGraphs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Random random = new Random();

        string yesterdaysCalls = "Select [Time] as Time ,sum(Calls) as Calls from mecca2.dbo.TrafficLogLive Where [Time] >= '20130305 00:00:00' and [Time] <= '20130305 23:59:59' and Calls > 0 and customer = 'ENTEL2' group by [Time] ORDER BY [Time]";
        string yesterdaysLWCalls = "Select dateadd(day,7,[Time]) as Time ,sum(Calls) as Calls from mecca2.dbo.TrafficLogLive Where [Time] >= '20130226 00:00:00' and [Time] <= '20130226 23:59:59' and Calls > 0 and customer = 'ENTEL2' group by dateadd(day,7,[Time]) ORDER BY dateadd(day,7,[Time])";
        string yesterdaysPorts = "select [Time], sum(Live_Calls) as Calls from cdrdb.dbo.cdrn Where [Time] >= '20130305 00:00:00' and [Time] <= '20130305 23:59:59' and cust_vendor = 'EN2C' group by [Time] order by [Time] ";
        string todaysPorts = "select dateadd(day,-1,[Time]) as [Time], sum(Live_Calls) as Calls from cdrdb.dbo.cdrn Where [Time] >= '20130306 00:00:00' and [Time] <= '20130306 23:59:59' and cust_vendor = 'EN2C' group by dateadd(day,-1,[Time]) order by dateadd(day,-1,[Time]) ";

        SqlCommand SqlCommand1 = new SqlCommand();
        SqlCommand1.CommandText = yesterdaysCalls;
        DataSet dataSet = RunQuery(SqlCommand1);
        DataTable dataTable = dataSet.Tables[0];

        SqlCommand1.CommandText = yesterdaysLWCalls;
        dataSet = RunQuery(SqlCommand1);
        DataTable dataTable2 = dataSet.Tables[0];

        SqlCommand1.CommandText = yesterdaysPorts;
        dataSet = RunQuery(SqlCommand1);
        DataTable dataTable3 = dataSet.Tables[0];

        SqlCommand1.CommandText = todaysPorts;
        dataSet = RunQuery(SqlCommand1);
        DataTable dataTable4 = dataSet.Tables[0];

        int totalRows;

        totalRows = dataTable3.Rows.Count;

        if (totalRows > 0)
        {
            for (int pointIndex = 0; pointIndex < totalRows; pointIndex++)
            {
                Chart1.Series["Series1"].Points.AddXY(Convert.ToDateTime(dataTable3.Rows[pointIndex]["Time"]), Convert.ToDouble(dataTable3.Rows[pointIndex]["Calls"]));
                //Chart1.Series["Series2"].Points.AddY(random.Next(5, 75));
            }
        }


        totalRows = dataTable.Rows.Count;


        if (totalRows > 0)
        {
            for (int pointIndex = 0; pointIndex < totalRows; pointIndex++)
            {
                Chart1.Series["Series2"].Points.AddXY(Convert.ToDateTime(dataTable.Rows[pointIndex]["Time"]), Convert.ToDouble(dataTable.Rows[pointIndex]["Calls"]));
                //Chart1.Series["Series2"].Points.AddY(random.Next(5, 75));
            }
        }

        totalRows = dataTable2.Rows.Count;

        if (totalRows > 0)
        {
            for (int pointIndex = 0; pointIndex < totalRows; pointIndex++)
            {
                Chart1.Series["Series3"].Points.AddXY(Convert.ToDateTime(dataTable2.Rows[pointIndex]["Time"]), Convert.ToDouble(dataTable2.Rows[pointIndex]["Calls"]));
                //Chart1.Series["Series2"].Points.AddY(random.Next(5, 75));
            }
        }

        totalRows = dataTable4.Rows.Count;

        if (totalRows > 0)
        {
            for (int pointIndex = 0; pointIndex < totalRows; pointIndex++)
            {
                Chart1.Series["Series4"].Points.AddXY(Convert.ToDateTime(dataTable4.Rows[pointIndex]["Time"]), Convert.ToDouble(dataTable4.Rows[pointIndex]["Calls"]));
                //Chart1.Series["Series2"].Points.AddY(random.Next(5, 75));
            }
        }

       


        //for (int pointIndex = 0; pointIndex < 10; pointIndex++)
        //{
        //    Chart1.Series["Series1"].Points.AddY(random.Next(45, 95));
        //    Chart1.Series["Series2"].Points.AddY(random.Next(5, 75));
        //}       

        // Set series chart type
        
        Chart1.Series["Series2"].Type = SeriesChartType.Area;
        Chart1.Series["Series3"].Type = SeriesChartType.Area;
        Chart1.Series["Series4"].Type = SeriesChartType.StackedColumn;
        

        // Set point labels

        //Chart1.Series["Series1"].ShowLabelAsValue = true;
        //Chart1.Series["Series2"].ShowLabelAsValue = true;

        // Enable X axis margin

        Chart1.ChartAreas["Default"].AxisX.Margin = true;

        // Enable 3D, and show data point marker lines

        Chart1.ChartAreas["Default"].Area3DStyle.Enable3D = false;

        Chart1.Series["Series1"].Name = "Ports";
        Chart1.Series["Series2"].Name = "Yesterdays Calls";
        Chart1.Series["Series3"].Name = "Last Week Calls";

        //Chart1.Series["Series1"]["ShowMarkerLines"] = "True";
        //Chart1.Series["Series2"]["ShowMarkerLines"] = "True";
    }

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {

            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
