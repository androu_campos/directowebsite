<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master"
    AutoEventWireup="true" CodeFile="ManageBroker.aspx.cs" Inherits="ManageSim" Title="DIRECTO - Connections Worldwide" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:Label ID="Label8" runat="server" Text="Manage Broker" CssClass="labelTurkH"></asp:Label>
    <asp:ScriptManager id="ScriptManager1" runat="server">
    </asp:ScriptManager>

<table <%--style="position:absolute; top:220px; left:230px;"--%>>
<tr>
<td>
 <%--<cc1:TabContainer ID="TabContainer1" runat="server" Height="200px" Width="600">
       <cc1:TabPanel ID="TabPanel1" runat="server" Visible="false">
        <ContentTemplate>--%>
        <table align="left" id="ManageSIM">
        <tr>
            <td style="width: 100px" align="left" valign="top">
                <asp:Label ID="lblHeader" runat="server" CssClass="labelTurkH" Text="MANAGE SIM" Visible="false"></asp:Label>
                DELETE</td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" bgcolor="#d5e3f0">
                <asp:Label ID="lbl1" runat="server" CssClass="labelTurk" Text="SIM:"></asp:Label></td>
            <td style="width: 100px">
                <asp:TextBox ID="txtSim" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
            <td style="width: 100px" align="left">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSim"
                    ErrorMessage="Field required" Font-Names="Arial" Font-Size="8pt"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="width: 100px" bgcolor="#d5e3f0">
                <asp:Label ID="lbl2" runat="server" CssClass="labelTurk" Text="IDPlan:"></asp:Label></td>
            <td style="width: 100px">
                <asp:DropDownList ID="dpdPlan" runat="server" CssClass="dropdown" DataTextField="Name">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px" bgcolor="#d5e3f0">
                <asp:Label ID="lbl3" runat="server" CssClass="labelTurk" Text="Broker"></asp:Label></td>
            <td align="left" style="width: 100px">
                <asp:DropDownList ID="dpdBroker" runat="server" CssClass="dropdown" DataTextField="Broker">
                </asp:DropDownList></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td align="left" style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td align="left" style="width: 100px">
                <asp:Button ID="cmdAssign" runat="server" CssClass="boton" Text="Assign" OnClick="cmdAssign_Click" /></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td align="left" style="width: 100px">
                <asp:Label ID="lblAssign" runat="server" Text="Label" CssClass="labelTurk" Visible="False"></asp:Label></td>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 20px">                
            </td>
        </tr>
    </table>
        <%--</ContentTemplate>
        </cc1:TabPanel>
        <cc1:TabPanel ID="Create" runat="server" HeaderText="Create">
            <ContentTemplate>--%>
                <table align="left">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="Label6" runat="server" CssClass="labelTurk" Text="CREATE BROKER"></asp:Label></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 25px">
                        </td>
                        <td style="width: 100px; height: 25px">
                        </td>
                        <td style="width: 100px; height: 25px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px" align="right">
                            <asp:Label ID="Label7" runat="server" CssClass="labelTurk" Text="Name:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtName" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px" align="right" abbr=" ">
                            <asp:Button ID="cmdCreate" runat="server" CssClass="boton" OnClick="cmdCreate_Click"
                                Text="Create" /></td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 23px">
                        </td>
                        <td abbr=" " align="right" style="width: 100px; height: 23px">
                        </td>
                        <td style="width: 100px; height: 23px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 21px;">
                        </td>
                        <td colspan="2" style="height: 21px">
                            <asp:Label ID="lblInsert" runat="server" CssClass="labelTurk" Text="Label" Visible="False"></asp:Label></td>
                    </tr>
                </table>
                
  <%--              </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel ID="Edit1" HeaderText="Edit" runat="server">
                    <ContentTemplate>--%>
                        <table align="left" id="Edit">
                            <tr>
                                <td style="width: 116px" align="left">
                                    <asp:Label ID="Label1" runat="server" Text="EDIT BROKER" CssClass="labelTurk" Width="83px"></asp:Label></td>
                                <td style="width: 100px">
                                </td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 116px; height: 26px;">
                                </td>
                                <td style="width: 100px; height: 26px;">
                                </td>
                                <td style="width: 100px; height: 26px;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 116px" align="right" bgcolor="#d5e3f0">
                                    <asp:Label ID="Label2" runat="server" Text="Select Broker:" CssClass="labelTurk"></asp:Label></td>
                                <td style="width: 100px" align="left">
                                    <asp:DropDownList ID="dpdBrokerName" runat="server" DataTextField="Broker" CssClass="dropdown">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 116px">
                                </td>
                                <td style="width: 100px">
                                </td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 116px" align="right" bgcolor="#d5e3f0">
                                    <asp:Label ID="Label3" runat="server" Text="New Broker name:" CssClass="labelTurk"
                                        Width="110px"></asp:Label></td>
                                <td style="width: 100px" align="left">
                                    <asp:TextBox ID="txtNewName" runat="server" CssClass="labelSteelBlue"></asp:TextBox></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 116px">
                                </td>
                                <td style="width: 100px" align="right">
                                    <asp:Button ID="cmdUpdate" runat="server" OnClick="cmdUpdate_Click" Text="Update Broker"
                                        CssClass="boton" /></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 116px">
                                </td>
                                <td colspan="2">
                                    <asp:Label ID="lblUpdated" runat="server" Text="Label" CssClass="labelTurk" Visible="False"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    
                                </td>
                            </tr>
                        </table>
                    <%--</ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel ID="delete" runat="server" HeaderText="Delete">
                    <ContentTemplate>--%>
                        <table align="left">
                            <tr height="45px">
                                <td style="width: 100px"  valign="top">
                                    <asp:Label ID="Label4" runat="server" Text="DELETE BROKER" CssClass="labelTurk"></asp:Label></td>
                                <td style="width: 100px">
                                </td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#d5e3f0" style="width: 100px">
                                    <asp:Label ID="Label5" runat="server" CssClass="labelTurk" Text="Select Broker:"
                                        Width="82px"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="dpdBrokerDelete" runat="server" CssClass="dropdown" DataTextField="Broker">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                    <asp:Button ID="cmdDelete" runat="server" CssClass="boton" OnClick="cmdDelete_Click"
                                        Text="Delete" /></td>
                            </tr>
                            <tr>
                                <td style="width: 100px">
                                </td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblDelete" runat="server" Text="Label" Visible="false"></asp:Label></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px">
                                </td>
                                <td style="width: 100px">
                                    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:QUESCOMConnectionString %>"
                                        SelectCommand="SELECT DISTINCT Broker FROM Broker&#13;&#10;UNION &#13;&#10;SELECT '**NULL** ' as Broker FROM Broker">
                                    </asp:SqlDataSource>
                                </td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                        </table>
                    <%--</ContentTemplate>
                </cc1:TabPanel>
</cc1:TabContainer> --%>
</td>
</tr>
</table>


    

</asp:Content> 