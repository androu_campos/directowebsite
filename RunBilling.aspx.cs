using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class RunBilling : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string user;

        user = Session["DirectoUser"].ToString();

        if (user == "yurema.villa" || user == "rodrigo.andrade" || user == "jorge.ibarra" || user == "manuel.moreno")
        {
            DateTime inf, sup;
            inf = DateTime.Today.AddHours(5);
            sup = DateTime.Today.AddHours(15);

            if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday || DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
            {
                inf = DateTime.Today.AddHours(5);
                sup = DateTime.Today.AddHours(13);
            }

            if (DateTime.Today.DayOfWeek == DayOfWeek.Monday)
            {
                inf = DateTime.Today.AddHours(5);
                sup = DateTime.Today.AddHours(10);
            }

            if (!Page.IsPostBack)
            {
                SqlCommand sqlSpQuery = new SqlCommand();
                sqlSpQuery.CommandType = CommandType.Text;
                sqlSpQuery.CommandText = "SELECT flag as [Time] FROM billschedule";


                DataSet resultSet;
                resultSet = RunQuery(sqlSpQuery);

                if (resultSet.Tables.Count > 0)
                {

                    gvGood.DataSource = resultSet.Tables[0];
                    gvGood.DataBind();
                }

                if (user == "yurema.villa" || user == "jorge.ibarra" || user == "rodrigo.andrade" || user == "manuel.moreno")
                {
                    if (DateTime.Now >= inf && DateTime.Now <= sup)
                    {
                        btnRunBilling.Enabled = true;
                        btnRunBilling.Text = "Run    Billing";
                    }
                    else
                    {
                        btnRunBilling.Enabled = false;
                        Response.Redirect("Home.aspx", false);
                    }
                }
                //else if (user == "yvilla" || user == "dortiz")
                //{
                //    // Cambiado 14/jun/2008, volver a false 
                //    //btnRunBilling.Enabled = false;
                //    btnRunBilling.Enabled = true;
                //    //btnRunBilling.Text = "out of Schedule  10 AM to 12 PM";
                //    btnRunBilling.Text = "Run    Billing";
                //}
            }
        }
        else
        {
            Response.Redirect("Home.aspx", false);
        }

        

    }
    protected void btnRunBilling_Click(object sender, EventArgs e)
    {

        
        SqlCommand sqlSpQuery = new SqlCommand();
        sqlSpQuery.CommandType = CommandType.StoredProcedure;
        sqlSpQuery.CommandText = "sp_RunBill";
        

        DataSet resultSet;
        resultSet = RunQuery(sqlSpQuery);

        if (resultSet.Tables.Count > 0)
        {
            gvGood.DataSource = resultSet.Tables[0];
            gvGood.DataBind();
        }        

    }

    private DataSet RunQuery(SqlCommand sqlQuery)
    {
        string connectionString =
            ConfigurationManager.ConnectionStrings
            ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection =
            new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;
        sqlQuery.CommandTimeout = 666;
        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
            //labelStatus.Text = "Unable to connect to SQL Server.";
        }
        return resultsDataSet;
    }
}
