using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dundas.Charting.WebControl;


public partial class DirTipData2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String query = null;
        DataTable tbl;

        query = "SELECT sum([Live_Calls]) as Live_Calls,[Time] FROM [CDRDB].[dbo].[cdrn] ";
        query += "WHERE [Time] >=  DATEADD(DAY,-1,LEFT(GETDATE(),11)) and [Time] <= DATEADD(DAY,1,LEFT(GETDATE(),11)) and ";
        query += "[Live_Calls] > 0   AND Cust_Vendor in ('DAT2') and [type] like 'C%'  group by Time ORDER BY Time";
        tbl = Util.RunQueryByStmntatCDRDB(query).Tables[0];

        makeChartData(tbl, "DATA2");

        tbl.Dispose();

        


        
    }

    private void makeChartData(DataTable tbl, string tITLE)
    {
        if (tbl.Rows.Count > 0)
        {
            int pdx;
            string script = string.Empty;

            this.Chart1.ChartAreas[0].AxisX.MajorTickMark.IntervalType = DateTimeIntervalType.Hours;

            foreach (DataRow myRow in tbl.Rows)
            {

                pdx = Chart1.Series["Ports"].Points.AddXY(myRow["Time"], Convert.ToDouble(myRow["Live_Calls"]));
                script = "onclick=dispData('" + myRow["Time"].ToString() + "','" + myRow["Live_Calls"] + "','P')";
                script = script.Replace(" ", "");
                this.Chart1.Series["Ports"].Points[pdx].MapAreaAttributes = script;

            }


            this.Chart1.ChartAreas[0].AxisX.MajorGrid.IntervalType = DateTimeIntervalType.Hours;
            DataPoint maxpointY = this.Chart1.Series["Ports"].Points.FindMaxValue("Y");
            //lblMaxPoint.Text = "MaxPoint: " + maxpointY.ToolTip.ToString();
            this.Chart1.Titles.Add(tITLE);
            this.Chart1.RenderType = RenderType.ImageTag;
            Session["tblDetailDATA"] = tbl;

            this.Chart1.Visible = true;
        }
        else
        {
            this.Chart1.Visible = false;
            Session["tblDetailDATA"] = tbl;
        }

    }

    
}
