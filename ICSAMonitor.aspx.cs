using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

public partial class ICSAMonitor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    //Muestra los datos del Recent,Today y Yesterday segun los parametros de la URL enviados por Monitor.aspx
    {
        try
        {
            string customer = Request.QueryString[0];
            string vendor = Request.QueryString[1];
            string originationIP = Request.QueryString[2];
            string terminationIP = Request.QueryString[3];
            string country = Request.QueryString[4];
            string region = Request.QueryString[5];
            string type = Request.QueryString[6];
            string clas = Request.QueryString[7];
            string lmc = Request.QueryString[8];
            string source = Request.QueryString[9];
            string gateway = Request.QueryString[10];

            //
            //
            StringBuilder select = new StringBuilder();
            select.Append("SELECT ");

            StringBuilder where = new StringBuilder();
            where.Append("WHERE ");

            string whereR = "WHERE ";

            string whereT = "WHERE";

            StringBuilder group = new StringBuilder();
            group.Append("GROUP BY ");

            StringBuilder union = new StringBuilder();
            union.Append(" UNION SELECT ");

            StringBuilder qry = new StringBuilder();
            StringBuilder order = new StringBuilder();
            order.Append(" Order by ");
            int ord = 0;

            //CUSTOMER
            if (customer != "** NONE **")
            {
                if (customer == "ICS-ALL-CARDS")
                {
                    select = select.Append("'ICS-ALLCARDS' as Customer,");
                    where.Append("Customer in ('ICS-800-CARDS','ICS-WEB-DID','ICS-WEB-UNL','ICS-800-WEB','ICS-800-PARAMOUNT','ICS-PARAMOUNT','ICS-800-TSI','ICS','ICS-TSI','ICS-PHONETIME','ICS-800-TITAN') AND ");
                    union.Append("'**ALL**' as Customer,");

                }

                else if (customer == "ICS")
                {
                    select = select.Append("'ICS' as Customer,");
                    where.Append("Customer in (SELECT PROVIDERNAME FROM CDRDB..PROVIDERIP WHERE PROVIDERNAME LIKE 'ICS%') AND ");
                    union.Append("'**ALL**' as Customer,");
                }

                else if (customer == "ICS-RELOADABLE")
                {
                    select = select.Append("'ICS-RELOADABLE' as Customer,");
                    where.Append("Customer in ('ICS-PREMIUM','ICS-PREMIUM-UNL','ICS-PAC-WEST-5','ICS-NUVOX-5') AND ");
                    union.Append("'**ALL**' as Customer,");
                }

                else if (customer == "ICS-RELOADABLE-2")
                {
                    select = select.Append("'ICS-RELOADABLE-2' as Customer,");
                    where.Append("Customer in ('ICS-PAC-WEST-2','ICS-NUVOX-2','ICS-PREMIUM-UNL-2') AND ");
                    union.Append("'**ALL**' as Customer,");
                }
                else if (customer == "ICS-FIRSTU2")
                {
                    select = select.Append("'ICS-FIRSTU2' as Customer,");
                    where.Append("Customer in (SELECT PROVIDERNAME FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'ICS%2' AND TYPE = 'C') AND ");
                    union.Append("'**ALL**' as Customer,");
                }
                else if (customer == "ICS-FIRSTU5")
                {
                    select = select.Append("'ICS-FIRSTU5' as Customer,");
                    where.Append("Customer in (SELECT PROVIDERNAME FROM CDRDB.DBO.PROVIDERIP WHERE PROVIDERNAME LIKE 'ICS%5' AND TYPE = 'C') AND ");
                    union.Append("'**ALL**' as Customer,");
                }
                else
                {
                    select = select.Append("Customer,");
                    where.Append("Customer like '" + customer + "' AND ");
                    group.Append("Customer,");
                    union.Append("'**ALL**' as Customer,");
                    ord = ord + 1;
                }
            }
            //VENDOR
            if (vendor != "** NONE **")
            {
                select = select.Append("Vendor,");

                if (vendor == "NATIONAL ALL")
                    where = where.Append("Vendor in ('NATIONAL','NATIONAL-ISA', 'NATIONAL-OFF','NATIONAL-TLL','NATIONAL-NEXT','NATIONAL-TELULAR') AND ");
                else
                    where = where.Append("Vendor like '" + vendor + "' AND ");
                group.Append("Vendor,");
                union.Append("'**ALL**' as Vendor,");
            }
            //OriginationIP
            if (originationIP != "** NONE **")
            {
                select = select.Append("OriginationIP,");
                where = where.Append("OriginationIP like '" + originationIP + "' AND ");
                group.Append("OriginationIP,");
                union.Append("'' as OriginationIP,");
            }
            //TerminationIP
            if (terminationIP != "** NONE **")
            {
                select = select.Append("TerminationIP,");
                where = where.Append("TerminationIP like '" + terminationIP + "' AND ");
                group.Append("TerminationIP,");
                union.Append("'' as TerminationIP,");

            }
            //Country
            if (country != "** NONE **")
            {
                select = select.Append("Country,");
                where = where.Append("Country like '" + country + "' AND ");
                group.Append("Country,");
                union.Append("'' as Country,");
            }
            //Region
            if (region != "** NONE **")
            {
                select = select.Append("Region,");
                where = where.Append("Region like '" + region + "' AND ");
                group.Append("Region,");
                union.Append("'' as Region,");
            }
            //Type
            if (type != "** NONE **")
            {
                select = select.Append("Type,");
                where = where.Append("Type like '" + type + "' AND ");
                group.Append("Type,");
                union.Append("'' as Type,");
            }
            //Class
            if (clas != "** NONE **")
            {
                select = select.Append("Class,");
                where = where.Append("Class like '" + clas + "' AND ");
                group.Append("Class,");
                union.Append("'' as Class,");

            }
            //LMC
            if (lmc != "** NONE **" && lmc != "**ALL**")
            {
                select = select.Append("LMC,");
                //select = select.Append("'" + lmc + "' as LMC, ");
                //where = where.Append("LMC in (SELECT LMC FROM MECCA2..LMC WHERE ENTERPRISE = '" + lmc + "') AND ");
                where = where.Append("LMC like '" + lmc + "' AND ");
                group.Append("LMC,");
                union.Append("'' as LMC,");

            }



            //Source MSW
            if (source != "** NONE **")
            {
                string tmp = string.Empty;
                tmp = where.ToString();
                select = select.Append("Source,");

                whereR = where.ToString() + "Source like '" + source + "CTMP%' AND ";
                whereT = where.ToString() + "Source like '" + source + "CTMP%' AND ";

                where = where.Append("Source like '" + source + "' AND ");

                group.Append("Source,");
                union.Append("'' as Source,");
            }
            //Gateway
            if (gateway != "** NONE **")
            {
                select = select.Append("VendorID,");
                where = where.Append("VendorID like '" + gateway + "' AND ");
                group.Append("VendorID,");
                union.Append("'' as VendorID,");
            }


            if (lmc == "**ALL**")
            {
                select = select.Append("LMC,");
                group.Append("LMC,");
                union.Append("'' as LMC,");
            }


            select = select.Replace("**ALL**", "%");

            if (customer == "**ALL**")
            {
                where = where.Replace("Customer like '**ALL**'", "Customer like 'ICS%'");
            }
            else if (customer == "** NONE **")
            {
                where = where.Append("Customer like 'ICS%' AND ");
            }
            

            

            where = where.Replace("**ALL**", "%");
            string selectS = select.ToString();
            string whereW = where.ToString();
            string groupG = group.ToString();
            //string orderO = order.ToString();
            string unionU = union.ToString();


            int lst = whereW.LastIndexOf("AND");//where
            whereW = whereW.Remove(lst, whereW.Length - lst);

            if (groupG.Length < 10)
                groupG = " ";
            else
            {
                lst = groupG.LastIndexOf(",");//group
                groupG = groupG.Remove(lst, groupG.Length - lst);
            }

            lst = unionU.LastIndexOf(",");//union
            unionU = unionU.Remove(lst, unionU.Length - lst);

            select.Replace(select.ToString(), selectS);
            where.Replace(where.ToString(), whereW);
            group.Replace(group.ToString(), groupG);
            union.Replace(union.ToString(), unionU);

            select.Append("sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,sum(RA) AS RejectedCalls, mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,CAST(AVG(PDD) as int) AS PDD ");
            union.Append(",sum(Minutes) as TotalMinutes,sum(Attempts) as Attempts,sum(Calls) AS AnsweredCalls,sum(RA) AS RejectedCalls, mecca2.dbo.fGetASR(SUM(Attempts), SUM(Calls), SUM(RA)) as ASR,mecca2.dbo.fGetABR(SUM(Attempts), SUM(Calls)) as ABR,mecca2.dbo.fGetACD(SUM(Minutes), SUM(Calls)) as ACD,CAST(AVG(PDD) as int) AS PDD ");

            //Recent
            whereR = whereR.Replace("**ALL**CTMP%", "%CTMP");
            whereR = whereR.Replace("**ALL**", "%");
            lst = whereR.LastIndexOf("AND");

            if (lst < 0)
                whereR = where.ToString();
            else
                whereR = whereR.Remove(lst, whereR.Length - lst);
            if (Session["hometype"] != null)
            {
                if (Session["hometype"].ToString() == "W")
                {
                    whereR += " AND Routetype = 'W' ";
                }
            }



            qry.Append(select.ToString() + " from OpSheetIncR " + whereR + group.ToString() + union + " from OpSheetIncR " + whereR.ToString() + " ORDER BY TotalMinutes DESC");
            //new
            //Response.Write(qry);
            //Response.End();

            DataSet mydataSet1 = RunQuery(qry, "ICSConnectionString");
            gdvRecent.DataSource = mydataSet1.Tables[0];
            gdvRecent.DataBind();
            //Todays
            whereT = whereT.Replace("**ALL**CTMP%", "%CTMP");
            whereT = whereT.Replace("**ALL**", "%");

            lst = whereT.LastIndexOf("AND");
            if (lst < 0)
                whereT = where.ToString();
            else
                whereT = whereT.Remove(lst, whereT.Length - lst);
            if (Session["hometype"] != null)
            {
                if (Session["hometype"].ToString() == "W")
                {
                    whereT += " AND Routetype = 'W' ";
                }
            }


            qry.Remove(0, qry.Length);
            qry.Append(select.ToString() + " from OpSheetIncR " + whereT + group.ToString() + union + " from OpSheetIncR " + whereT.ToString() + " ORDER BY TotalMinutes DESC");
            qry.Replace("OpSheetIncR", "OpSheetAllR");
            string qry2 = qry.ToString();

            DataSet myDataSet2 = RunQuery(qry, "ICSConnectionString");
            gdvToday.DataSource = myDataSet2.Tables[0];
            gdvToday.DataBind();
            //Yesterday
            qry.Remove(0, qry.Length);

            if (Session["hometype"] != null)
            {
                if (Session["hometype"].ToString() == "W")
                {
                    where.Append(" AND Routetype = 'W' ");
                }
            }



            qry.Append(select.ToString() + " from OpSheetIncR " + where.ToString() + group.ToString() + union + " from OpSheetIncR " + where.ToString() + " ORDER BY TotalMinutes DESC");
            qry.Replace("OpSheetIncR", "OpSheetAllR");
            qry.Replace("OpSheetAllR", "OpSheetYR");
            string qry3 = qry.ToString();

            DataSet myDataSet3 = RunQuery(qry, "ICSConnectionString");
            gdvYesterday.DataSource = myDataSet3.Tables[0];
            gdvYesterday.DataBind();

            //new

            //lblRefresh YESTERDAY
            StringBuilder query = new StringBuilder();
            query.Append("select crdate as Modified from [ICS].[dbo].sysobjects  where type = 'U' and name like 'OpSheetYR'");
            DataSet ResultSet1;
            ResultSet1 = RunQuery(query, "ICSConnectionString");
            lblYesterday.Text = "Last Modified:" + ResultSet1.Tables[0].Rows[0][0].ToString();
            //lblRefresh RECENT
            StringBuilder recentupdate = new StringBuilder();
            recentupdate.Append("select crdate as Modified from [ICS].[dbo].sysobjects  where type = 'U' and name like 'OpSheetIncR'");
            DataSet ResultSet2;
            ResultSet2 = RunQuery(recentupdate, "ICSConnectionString");
            lblRecent.Text = "Last Modified:" + ResultSet2.Tables[0].Rows[0][0].ToString();
            lblRecent.Visible = true;
            //lblRefresh TODAY
            StringBuilder todayupdate = new StringBuilder();
            todayupdate.Append("select crdate as Modified from [ICS].[dbo].sysobjects  where type = 'U' and name like 'OpSheetAllR'");
            DataSet ResultSet3;
            ResultSet3 = RunQuery(todayupdate, "ICSConnectionString");
            lblToday.Text = "Last Modified:" + ResultSet3.Tables[0].Rows[0][0].ToString();
            lblToday.Visible = true;

            lblHeader.Text = customer;
            lblBilling.Visible = false;
        }
        catch (Exception ex)
        {
            lblBilling.Visible = true;
            string error = ex.Message.ToString();

        }
    }


    private System.Data.DataSet RunQuery(StringBuilder qry, string StringConnection)
    {

        System.Data.SqlClient.SqlCommand sqlQuery = new System.Data.SqlClient.SqlCommand();
        sqlQuery.CommandText = qry.ToString();


        string connectionString = ConfigurationManager.ConnectionStrings
        [StringConnection].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }
}
