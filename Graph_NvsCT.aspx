<%@ Page Language="C#" StylesheetTheme="Theme1" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Graph_NvsCT.aspx.cs" Inherits="Graph_NvsCT" Title="Untitled Page" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <table>
            <tr>
                <td align="left" bgcolor="#006699" style="width: 100px" valign="top">
                    <asp:Label ID="LabelS1" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                        Text="Step 1 "></asp:Label></td>
                <td align="left" bgcolor="#006699" style="width: 300px" valign="top">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                        Text="Choose Range Of   Days" Width="282px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" bgcolor="#99ccff" style="width: 100px" valign="top">
                    <asp:Label ID="labelFrom" runat="server" Font-Bold="True" Font-Names="Arial" Text="From:"></asp:Label></td>
                <td align="left" style="width: 300px" valign="top">
                    <asp:TextBox ID="txtFromV" runat="server" Font-Names="Arial" Width="100px" ReadOnly="True"></asp:TextBox>
                    <img id="imaFrom" onclick="openCalendar('txtFrom');" src="calendar.jpg" width="15" />&nbsp;
                    <asp:Label ID="Step1" runat="server" Text="<<  Click here" Width="130px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" bgcolor="#99ccff" style="width: 100px" valign="top">
                    <asp:Label ID="labelTo" runat="server" Font-Bold="True" Font-Names="Arial" Text="Hour"></asp:Label></td>
                <td align="left" style="width: 300px" valign="top">
                    <asp:TextBox ID="txtHf" runat="server" Width="100px">00:00:00</asp:TextBox>
                    To&nbsp;<asp:TextBox ID="txtHt" runat="server" Width="100px">23:59:59</asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" bgcolor="#006699" style="width: 100px" valign="top">
                    <asp:Label ID="LabelS2" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                        Text="Step 2 "></asp:Label></td>
                <td align="center" bgcolor="#006699" style="width: 300px" valign="top">
                    <asp:Button ID="btnNext" runat="server" Font-Names="Arial" OnClick="btnNext_Click"
                        Text="Next" /></td>
            </tr>
            <tr>
                <td align="left" bgcolor="#006699" style="width: 100px" valign="top">
                    <asp:Label ID="LabelS4" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                        Text="Step 3"></asp:Label></td>
                <td align="left" bgcolor="#006699" style="width: 300px" valign="top">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                        Text="Choose    Region" Width="281px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" bgcolor="#99ccff" style="width: 100px" valign="top">
                    <asp:Label ID="labelRegion" runat="server" Font-Bold="True" Font-Names="Arial" Text="Region:"></asp:Label></td>
                <td align="left" style="width: 300px" valign="top">
                    <asp:DropDownList ID="ddlRegion" runat="server" DataSourceID="dsRegion" DataTextField="Region"
                        DataValueField="Region" Enabled="False" Font-Names="Arial" Width="300px">
                    </asp:DropDownList></td>
            </tr>
        </table>
        
        
        <asp:Button ID="btnSubmit"  runat="server" Enabled="False" Font-Names="Arial" OnClick="btnSubmit_Click" Text="Submit" />
        <asp:SqlDataSource ID="dsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>
        <asp:HiddenField ID="txtFrom" runat="server" />
        <asp:HiddenField ID="txtTo" runat="server" />
        <asp:SqlDataSource ID="dsSP" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
        </asp:SqlDataSource>
</asp:Content>

