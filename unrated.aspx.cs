using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using RKLib.ExportData;

public partial class unrated : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter adp = new MyDataSetTableAdaptersTableAdapters.unratedMinutes_TableAdapter();
            MyDataSetTableAdapters.unratedMinutes_DataTable tbl = adp.GetData();
            if (tbl.Rows.Count < 1)
            {
                Session["unrated"] = tbl;
                Button1.Visible = false;
                Label1.Visible = true;
            }
            else
            {
                Session["unrated"] = tbl;
                Button1.Visible = true;
                Label1.Visible = false;
            }

            GridView1.DataSource = adp.GetData();
            GridView1.DataBind();
        }
        catch (Exception ex)

        {

            Label2.Text =  ex.Message.ToString();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
        DataTable dtEmployee = ((DataTable)Session["unrated"]).Copy();
        // Export all the details to CSV
        RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
        Random random = new Random();
        string ran = random.Next(1500).ToString();
        string filename = "UnratedMinutes" + ".xls";
        objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string error = ex.Message.ToString();
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
}
