<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Top20MSL.aspx.cs" Inherits="Top20MSL" Title="DIRECTO - Connections Worldwide" StylesheetTheme="Theme1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label3" runat="server" CssClass="labelTurkH" Text="TOP20 MASTER LOG"
        Font-Bold="True"></asp:Label>
    <asp:SqlDataSource ID="SqlType" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT DISTINCT Type FROM Regions UNION SELECT '** NONE **' AS Type FROM Regions AS Regions_2 UNION SELECT '**ALL**' AS Type FROM Regions AS Regions_1">
    </asp:SqlDataSource>
    <table style="position:absolute; top:220px; left:228px;">
        <tr>
            <td style="width: 430px; height: 306px;" valign="top">
                <table>
                    <tr>
                        <td style="width: 100px" valign="top" align="left" bgcolor="#d5e3f0">
                            <asp:Label ID="LabelS1" runat="server" Font-Bold="False" Text="Step 1 " CssClass="labelBlue8"></asp:Label></td>
                        <td style="width: 300px" valign="top" align="left" bgcolor="#d5e3f0">
                            <asp:Label ID="Label1" runat="server" Font-Bold="False" Font-Names="Arial" ForeColor="#004A8F"
                                Text="Choose Range Of   Days" Width="282px" Font-Size="8pt"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="labelFrom" runat="server" Text="From:" Font-Bold="False" CssClass="labelSteelBlue" /></td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:TextBox ID="txtFromV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                            &nbsp;&nbsp;<asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar1.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="labelTo" runat="server" Text="To:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:TextBox ID="txtToV" runat="server" Width="80px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>
                            &nbsp;&nbsp;<asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar1.gif" /></td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 21px;" valign="top" align="left" bgcolor="#d5e3f0">
                            <asp:Label ID="LabelS3" runat="server" Font-Bold="False" Text="Step 2" CssClass="labelBlue8"></asp:Label></td>
                        <td style="width: 300px; height: 21px;" valign="top" align="left" bgcolor="#d5e3f0">
                            <asp:Label ID="Label4" runat="server" Font-Bold="False" Text="Choose Customer and Vendor"
                                Width="281px" CssClass="labelBlue8"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="labelCustomer" runat="server" Text="Customer:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:DropDownList ID="ddlCust" runat="server" DataSourceID="sqldsCust" DataTextField="Customer"
                                DataValueField="Customer" OnSelectedIndexChanged="ddlCust_SelectedIndexChanged"
                                Width="300px" Font-Names="Arial" CssClass="dropdown">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="labelVendor" runat="server" Text="Vendor:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:DropDownList ID="ddlVendor" runat="server" DataSourceID="sqldsVendor" DataTextField="Vendor"
                                DataValueField="Vendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged"
                                Width="300px" Font-Names="Arial" CssClass="dropdown">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left" bgcolor="#d5e3f0">
                            <asp:Label ID="LabelS4" runat="server" Font-Bold="False" Text="Step 3" CssClass="labelBlue8"></asp:Label></td>
                        <td style="width: 300px" valign="top" align="left" bgcolor="#d5e3f0">
                            <asp:Label ID="Label2" runat="server" Font-Bold="False" Text="Choose Country and Region"
                                Width="281px" CssClass="labelBlue8"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="labelCountry" runat="server" Text="Country:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" DataSourceID="sqldsCountry"
                                DataTextField="Country" DataValueField="Country" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                Width="300px" Font-Names="Arial" CssClass="dropdown">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="labelRegion" runat="server" Text="Region:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:DropDownList ID="ddlRegion" runat="server" Enabled="False" DataSourceID="sqldsRegion"
                                DataTextField="Region" DataValueField="Region" Width="300px" Font-Names="Arial"
                                CssClass="dropdown">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="labelLmc" runat="server" Text="Lmc:" Font-Bold="False" CssClass="labelSteelBlue"></asp:Label></td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:DropDownList ID="ddlLmc" runat="server" Enabled="False" DataSourceID="sqldsLmc"
                                DataTextField="Lmc" DataValueField="Lmc" Width="300px" Font-Names="Arial"
                                CssClass="dropdown">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#d5e3f0" style="width: 100px" valign="top">
                            <asp:Label ID="Label6" runat="server" CssClass="labelBlue8" Text="Step 4"></asp:Label></td>
                        <td align="left" bgcolor="#d5e3f0" style="width: 300px" valign="top">
                            <asp:Label ID="Label9" runat="server" CssClass="labelBlue8" Text="Choose Type and Class"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px" valign="top">
                            <asp:Label ID="Label10" runat="server" CssClass="labelSteelBlue" Text="Type:"></asp:Label></td>
                        <td align="left" style="width: 300px" valign="top">
                            <asp:DropDownList ID="dpdType" runat="server" CssClass="dropdown" DataSourceID="SqlType"
                                DataTextField="Type" DataValueField="Type" Width="300px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px" valign="top">
                            <asp:Label ID="Label11" runat="server" CssClass="labelSteelBlue" Text="Class:"></asp:Label></td>
                        <td align="left" style="width: 300px" valign="top">
                            <asp:DropDownList ID="dpdClass" runat="server" CssClass="dropdown" Width="300px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#d5e3f0" style="width: 100px" valign="top">
                            <asp:Label ID="Label12" runat="server" CssClass="labelBlue8" Text="Step 5"></asp:Label></td>
                        <td align="left" bgcolor="#d5e3f0" style="width: 300px" valign="top">
                            <asp:Label ID="Label13" runat="server" CssClass="labelBlue8" Text="Search Number and ICCID"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="label14" runat="server" Text="Number:" Font-Bold="False" CssClass="labelSteelBlue" /></td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:TextBox ID="TextBox1" runat="server" Width="150px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>                            
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px" valign="top" align="left">
                            <asp:Label ID="label15" runat="server" Text="ICCID:" Font-Bold="False" CssClass="labelSteelBlue" /></td>
                        <td style="width: 300px" valign="top" align="left">
                            <asp:TextBox ID="TextBox2" runat="server" Width="150px" Font-Names="Arial" CssClass="labelSteelBlue"></asp:TextBox>                            
                        </td>
                    </tr>
                </table>
              
                <table>
                    <tr>
                        <td style="width: 100px">
                            <asp:CheckBox ID="ToExcel" runat="server" Text="To Excel" Visible="False" /></td>
                        <td style="width: 100px">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                Font-Names="Arial" CssClass="boton" /></td>
                        <td style="width: 100px">
                            <asp:Button ID="btnReset" runat="server" OnClick="btnReset_Click" Text="Reset" CssClass="boton" /></td>
                    </tr>
                </table>
              
            </td>
            <td style="width: 280px; height: 306px; text-align: left" valign="top" bgcolor="white"
                align="left">
                <asp:RadioButtonList ID="rdbtnlType" runat="server" AutoPostBack="True" CssClass="labelSteelBlue"
                    Font-Bold="False" OnSelectedIndexChanged="rdbtnlType_SelectedIndexChanged" Width="260px"
                    Visible="False">
                    <asp:ListItem Value="0">Sales Log By Region</asp:ListItem>
                    <asp:ListItem Value="1">Sales Log</asp:ListItem>
                    <asp:ListItem Value="2">Sales Log By Prefix</asp:ListItem>
                    <asp:ListItem Value="3">Sales Log Totals</asp:ListItem>
                    <asp:ListItem Value="4" Selected="True">Sales Log With Type</asp:ListItem>
                    <asp:ListItem Value="5">Yesterday's Profit By Customer</asp:ListItem>
                    <asp:ListItem Value="6">Yesterday's Profit By Vendor</asp:ListItem>
                    <asp:ListItem Selected="True" Value="18">Personalized</asp:ListItem>
                </asp:RadioButtonList>
                <br />
                <table width="95%">
                    <tr>
                        <td colspan="2">
                            <asp:CheckBoxList ID="chkBilling" runat="server" CssClass="labelSteelBlue" Visible="False">
                                <asp:ListItem Selected="True">Billing Date</asp:ListItem>
                            </asp:CheckBoxList></td>
                    </tr>
                </table>
            </td>
            <td style="width: 300px; height: 306px; text-align: left;" valign="top" bgcolor="white">
                <asp:CheckBoxList ID="ckboxl" runat="server" CssClass="labelSteelBlue" Font-Bold="False"
                    Width="260px" Visible="False">
                    <asp:ListItem Value="0">Customer</asp:ListItem>
                    <asp:ListItem Value="1">Vendor</asp:ListItem>
                    <asp:ListItem Value="2">CustBroker</asp:ListItem>
                    <asp:ListItem Value="3">VendorBroker</asp:ListItem>
                    <asp:ListItem Value="4">Country</asp:ListItem>
                    <asp:ListItem Value="5">Region</asp:ListItem>
                    <asp:ListItem Value="6">RegionPrefix</asp:ListItem>
                    <asp:ListItem Value="7">CustPrefix</asp:ListItem>
                    <asp:ListItem Value="8">VendorPrefix</asp:ListItem>
                    <asp:ListItem Value="9">Type</asp:ListItem>
                    <asp:ListItem Value="10">LMC</asp:ListItem>
                    <asp:ListItem Value="11">Buy</asp:ListItem>
                    <asp:ListItem Value="12">Sell</asp:ListItem>
                    <asp:ListItem Value="13">Calls</asp:ListItem>
                    <asp:ListItem Value="14">Minutes (Customer)</asp:ListItem>
                    <asp:ListItem Value="15">Minutes (Vendor)</asp:ListItem>
                    <asp:ListItem Value="16">TotalCost</asp:ListItem>
                    <asp:ListItem Value="17">TotalSales</asp:ListItem>
                    <asp:ListItem Selected="True" Value="18">Profit</asp:ListItem>
                    <asp:ListItem Value="19">ASR</asp:ListItem>
                    <asp:ListItem Value="20">VACD</asp:ListItem>
                    <asp:ListItem Value="21">CACD</asp:ListItem>
                    <asp:ListItem Value="22">ATTEMPTS</asp:ListItem>
                    <asp:ListItem>RA</asp:ListItem>
                </asp:CheckBoxList></td>
           
        </tr>
        <tr>
            <td style="width: 430px; height: 306px" valign="top">
                <asp:DropDownList ID="ddlCustBroker" runat="server" DataSourceID="sdldsCustBroker"
                    DataTextField="CustBroker" DataValueField="CustBroker" OnSelectedIndexChanged="ddlCustBroker_SelectedIndexChanged"
                    Width="300px" CssClass="dropdown" Visible="False">
                </asp:DropDownList>
                <asp:Label ID="Label7" runat="server" Font-Bold="False" Text="CustBroker:" CssClass="labelSteelBlue"
                    Visible="False"></asp:Label><br />
                <asp:DropDownList ID="ddlVendorBroker" runat="server" DataSourceID="sqldsVendorBroker"
                    DataTextField="VendorBroker" DataValueField="VendorBroker" OnSelectedIndexChanged="ddlVendorBroker_SelectedIndexChanged"
                    Width="300px" CssClass="dropdown" Visible="False">
                </asp:DropDownList>
                <asp:Label ID="Label8" runat="server" Font-Bold="False" Text="VendorBroker:" CssClass="labelSteelBlue"
                    Visible="False"></asp:Label><br />
                &nbsp;<br />
            </td>
            <td align="left" bgcolor="white" style="width: 280px; height: 306px; text-align: left"
                valign="top">
            </td>
            <td bgcolor="white" style="width: 300px; height: 306px; text-align: left" valign="top">
            </td>
        </tr>
        
        
          <tr>
            <td width="100%" colspan="3">
                <asp:Image ID="Image3" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label5" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="True"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="True" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:ScriptManager id="ScriptManager1" runat="server">
                </asp:ScriptManager></td>
        </tr>
    </table>
    <br />
    <asp:HiddenField ID="hfCustBroker" runat="server" />
    <asp:HiddenField ID="hfVendorBroker" runat="server" />
    <asp:SqlDataSource ID="sqldsFill" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>
    <asp:HiddenField ID="hfRegion" runat="server" />
    <asp:HiddenField ID="hfLmc" runat="server" />
    <asp:HiddenField ID="txtFrom" runat="server" />
    <asp:HiddenField ID="txtTo" runat="server" />
    <asp:HiddenField ID="hfCust" runat="server" />
    <asp:HiddenField ID="hfVendor" runat="server" />
    <asp:HiddenField ID="hfCountry" runat="server" />
    <asp:SqlDataSource ID="sqldsCust" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT '**ALL**' AS Customer FROM CDRDB.dbo.ProviderIP UNION SELECT DISTINCT (CASE WHEN ProviderName LIKE 'ICS%' THEN 'ICS' ELSE ProviderName END) AS Customer FROM CDRDB.dbo.ProviderIP AS ProviderIP_2 UNION SELECT '** NONE **' AS Customer FROM CDRDB.dbo.ProviderIP AS ProviderIP_1"
        ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT '**ALL**' AS Vendor FROM CDRDB.dbo.ProviderIP UNION SELECT DISTINCT ProviderName AS Vendor FROM CDRDB.dbo.ProviderIP AS ProviderIP_2 UNION SELECT '** NONE **' AS Vendor FROM CDRDB.dbo.ProviderIP AS ProviderIP_1">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCountry" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT '**ALL**' as Country &#13;&#10;FROM CDRDB..Regions &#13;&#10;UNION &#13;&#10;SELECT DISTINCT Country &#13;&#10;FROM CDRDB..Regions &#13;&#10;UNION&#13;&#10;SELECT '** NONE **' as Country &#13;&#10;FROM CDRDB..Regions &#13;&#10;ORDER BY Country"
        ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsLmc" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sdldsCustBroker" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT  '**ALL**' as CustBroker UNION SELECT DISTINCT CustBroker FROM MECCA2..CBroker"
        ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsVendorBroker" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT  '**ALL**' as VendorBroker UNION SELECT DISTINCT VendorBroker  FROM MECCA2..VBroker"
        ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlClass" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT DISTINCT Class FROM Regions UNION SELECT '**ALL**' AS Class FROM Regions AS Regions_2 UNION SELECT '** NONE **' AS Class FROM Regions AS Regions_1">
    </asp:SqlDataSource>
    &nbsp;<br />
    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromV"
        PopupButtonID="Image1">
    </cc1:CalendarExtender>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToV"
        PopupButtonID="Image2">
    </cc1:CalendarExtender>
</asp:Content>

