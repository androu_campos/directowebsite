using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Office.Interop;
using System.Data.SqlClient;
using ADODB;
using System.Net.Mail;
using System.Net;
using System.Diagnostics;
using System.Net.Mime;


public partial class OperativeLeverage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFromV.Text = DateTime.Today.AddDays(-1).ToShortDateString();
            txtToV.Text = DateTime.Today.AddDays(-1).ToShortDateString();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string from = txtFromV.Text;
        string to = txtToV.Text;
        createExcelFile(from,to);
    }

    protected void createExcelFile(string from, string to)
    {
        object missValue = System.Reflection.Missing.Value;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

		Microsoft.Office.Interop.Excel.Application xlApp = default(Microsoft.Office.Interop.Excel.Application);
		Microsoft.Office.Interop.Excel.Workbook xlWorkBook = default(Microsoft.Office.Interop.Excel.Workbook);
		Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet = default(Microsoft.Office.Interop.Excel.Worksheet);

        String sToday = DateTime.Now.ToString("MM.dd.yyyy.HH.mm");
        String sPathFiles = "G:\\Reports\\WS\\op-leverage\\";
        String sStandardFile = sPathFiles + "PLApalancamiento.xls";

        String connectionString = ConfigurationManager.ConnectionStrings["MECCA2ConnectionString"].ConnectionString;

        xlApp = new Microsoft.Office.Interop.Excel.ApplicationClass();

        ADODB.Connection DBConnection = new ADODB.Connection();
        ADODB.Recordset rsData = new ADODB.Recordset();
        ADODB.Recordset rsDataB = new ADODB.Recordset();

        


        DBConnection.Open("Provider='SQLOLEDB.1';" + connectionString, "", "", -1);
        DBConnection.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
        rsData.Open("EXEC Mecca2.dbo.sp_getDataApalancamientoC '" + from + "', '" + to + "' WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);
        rsDataB.Open("EXEC Mecca2.dbo.sp_getDataApalancamientoV '" + from + "', '" + to + "' WITH RECOMPILE", DBConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockBatchOptimistic, 1);        

        xlWorkBook = xlApp.Workbooks.Open(sStandardFile, 0, false, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", false, false, 0, false, false, 0);
        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

        Microsoft.Office.Interop.Excel.Range tblRange = xlApp.get_Range("A3", "A3");
        Microsoft.Office.Interop.Excel.Range tblRangeB = xlApp.get_Range("A17", "A17");

        int iData = tblRange.CopyFromRecordset(rsData, missValue, missValue);
        int iDataB = tblRangeB.CopyFromRecordset(rsDataB, missValue, missValue);

        int headersCount = rsData.Fields.Count;
        int headersCountB = rsDataB.Fields.Count;

        int lap = headersCount/28;
        lap++;
        int i = 0;
        int h = 0;
        char j = 'A';
        char k = 'A';
        char endPos;
        bool stop = false;

        int lapcount = 0;

        while (lapcount < lap)
        {
            for (j = 'A'; j <= 'Z'; j++)
            {
                i++;
                if (headersCount == i)
                {
                    stop = true;
                    break;
                }
            }

            if (lap > 1)
            {
                k++;
            }

            lapcount++;
        }

        if (lap > 1)
        {
            sb.Append(k);
            sb.Append(j);
        }
        else
        {
            sb.Append(j);
        }

        xlApp.get_Range("A3",sb.ToString() + "3").Copy(missValue);
        xlApp.get_Range("A17", sb.ToString() + "17").Copy(missValue);
        //xlWorkSheet.get_Range("A2", sb.ToString() + "2").NumberFormat = "0.00";
        Microsoft.Office.Interop.Excel.Range tblData = xlApp.get_Range("A3", sb.ToString() + (2 + iData));
        Microsoft.Office.Interop.Excel.Range tblDataB = xlApp.get_Range("A17", sb.ToString() + (6 + iData +iDataB));
        tblData.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        tblDataB.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, missValue, missValue);
        tblData.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
        tblDataB.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-"; 

        xlWorkSheet.get_Range("A2",sb.ToString()+"2").Font.Bold = true;
        xlWorkSheet.get_Range("A16", sb.ToString() + "16").Font.Bold = true;
		xlWorkSheet.get_Range("A2",sb.ToString()+"2").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
        xlWorkSheet.get_Range("A16",sb.ToString()+"16").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;        
        
        string [] hnames = new string[headersCount];
        string[] hnamesb = new string[headersCountB];
        i = 0;
        h = 0; 

        foreach (ADODB.Field adoField in rsData.Fields)
        {
            hnames[i] = adoField.Name;
            i++;
        }

        foreach (ADODB.Field adoField in rsDataB.Fields)
        {
            hnamesb[h] = adoField.Name;
            h++;
        }

        xlWorkSheet.get_Range("A2", sb.ToString() + "2").Value2 = hnames;
        xlWorkSheet.get_Range("A16", sb.ToString() + "16").Value2 = hnamesb;
        xlWorkSheet.get_Range("A13", "A13").Value2 = "VENTA TOTAL";
        xlWorkSheet.get_Range("A14", "A14").Value2 = "AVANCE META";

        xlWorkSheet.get_Range("A13", "A14").Font.Bold = true;
        xlWorkSheet.get_Range("A25", "A26").Font.Bold = true;

        xlWorkSheet.get_Range("A13", "A14").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
        xlWorkSheet.get_Range("A25", "A26").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

        xlWorkSheet.get_Range("A25", "A25").Value2 = "PROFIT";
        xlWorkSheet.get_Range("A26", "A26").Value2 = "PROFIT %";

        i = 0;
        h = 0;
        char l = 'B';
        char m = 'B';


        while (i < (headersCount - 1))
        {           
            Microsoft.Office.Interop.Excel.Range oRng = xlWorkSheet.get_Range(l.ToString() + "13", l.ToString() + "13");
            Microsoft.Office.Interop.Excel.Range oRngB = xlWorkSheet.get_Range(l.ToString() + "14", l.ToString() + "14");
            oRng.Formula = "=SUM(" + l.ToString()+ "3:" + l.ToString() + "12)";
            oRngB.Formula = "=" + l.ToString() + "13/35000";
            oRng.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";
            oRngB.NumberFormat = "0.0%";
            l++;
            i++;
        }

        while (h < (headersCountB - 1))
        {
            Microsoft.Office.Interop.Excel.Range oRngC = xlWorkSheet.get_Range(m.ToString() + "23", m.ToString() + "23");
            Microsoft.Office.Interop.Excel.Range oRngD = xlWorkSheet.get_Range(m.ToString() + "25", m.ToString() + "25");
            Microsoft.Office.Interop.Excel.Range oRngE = xlWorkSheet.get_Range(m.ToString() + "26", m.ToString() + "26");            

            oRngC.Formula = "=SUM(" + m.ToString() + "17:" + m.ToString() + "22)";            
            oRngC.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";

            oRngD.Formula = "=" + m.ToString() + "13 - " + m.ToString() + "23";
            oRngD.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \" - \"??_-;_-@_-";

            oRngE.Formula = "=" + m.ToString() + "25 / " + m.ToString() + "13";
            oRngE.NumberFormat = "0.0%";
            m++;
            h++;
        }
        

        


                //tblData = xlApp.get_Range("D12", "F" + (11 + iRates));
        //tblData.Formula = tblRates.Value2;

        //tblRange = xlApp.get_Range("A11", missValue);
        //tblRange.Activate();

        rsData.Close();
        rsDataB.Close();
        DBConnection.Close();

        xlApp.DisplayAlerts = false;
        xlApp.AlertBeforeOverwriting = false;

        

        xlWorkBook.SaveAs(sPathFiles+"OperativeLeverage_" + sToday +".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, missValue, missValue, missValue, missValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, missValue, missValue, missValue, missValue);
        xlWorkBook.Close(false, missValue, missValue);

        xlApp.Quit();
        //xlApp.Visible = true;

        releaseObject(xlWorkSheet);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        while (xlApp != null)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
            xlApp = null;
        }

        GC.Collect();
        GC.WaitForPendingFinalizers();

        
        //HyperLink1.NavigateUrl = "http://mecca.directo.com/ws/op-leverage/" + "OperativeLeverage_" + sToday + ".xls";
        //HyperLink1.Visible = true;

        Response.Redirect("http://mecca.directo.com/ws/op-leverage/" + "OperativeLeverage_" + sToday + ".xls");
            
       
    }

    private void releaseObject(object obj)
    {
        if (obj != null)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //lblError.Text = "Exception Occured while releasing object " + ex.ToString();
            }
            finally
            {
                GC.Collect();
            }
        }
    }


    
}
