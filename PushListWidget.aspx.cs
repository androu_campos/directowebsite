using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

public partial class PushListWidget : System.Web.UI.Page
{
    public static int selected = 0;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Page.IsPostBack)
        {
            selected = dpdRegions.SelectedIndex;
            
            dpdRegions.Items.Clear();
            string country = dpdCtry.SelectedValue.ToString();
            
            SqlConnection SqlConn = new SqlConnection();
            SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";
            string commandText = "select distinct Region from Regions where Region is not NULL AND Country like '" + country + "' union select '** NONE **' as Region from Regions union select '**ALL**' as Region from Regions order by Region ASC";
            SqlCommand SqlCommand5 = new SqlCommand(commandText, SqlConn);
            SqlConn.Open();
            SqlDataReader myReader5 = SqlCommand5.ExecuteReader();
            while (myReader5.Read())
            {
                dpdRegions.Items.Add(myReader5.GetValue(0).ToString());
            }
            myReader5.Close();
            SqlConn.Close();
        }
        else
        {
            //COUNTRY
            SqlConnection SqlConn = new SqlConnection();
            SqlConn.ConnectionString = "Data Source=172.27.27.30;Initial Catalog=MECCA2;User ID=steward_of_Gondor;Password=nm@73-mg";

            SqlCommand SqlCommand4 = new SqlCommand("select distinct Country from Regions union  select '** NONE **' as Country from Regions union select '**ALL**' as Country from Regions order by Country ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader4 = SqlCommand4.ExecuteReader();
            while (myReader4.Read())
            {
                dpdCtry.Items.Add(myReader4.GetValue(0).ToString());
            }
            myReader4.Close();
            SqlConn.Close();
            //REGION
            //SqlCommand SqlCommand5 = new SqlCommand("select distinct Region from Regions where Region is not NULL union select '** NONE **' as Region from Regions union select '**ALL**' as Region from Regions order by Region ASC", SqlConn);
            //SqlConn.Open();
            //SqlDataReader myReader5 = SqlCommand5.ExecuteReader();
            //while (myReader5.Read())
            //{
            //    dpdRegions.Items.Add(myReader5.GetValue(0).ToString());
            //}
            //myReader5.Close();
            //SqlConn.Close();

            //TYPE
            SqlCommand SqlCommand6 = new SqlCommand("select distinct Regions.Type from Regions union select '** NONE **' as Type from Regions union select '**ALL**' as Type from Regions order by Type ASC", SqlConn);
            SqlConn.Open();
            SqlDataReader myReader6 = SqlCommand6.ExecuteReader();
            while (myReader6.Read())
            {
                dpdType.Items.Add(myReader6.GetValue(0).ToString());
            }
            myReader6.Close();
            SqlConn.Close();
        }
    }
    protected void cmdReport_Click(object sender, EventArgs e)
    {
        string where = string.Empty;
        
        string WHERE = string.Empty;
        StringBuilder newQuery = new StringBuilder();
        StringBuilder newWhere = new StringBuilder();
        StringBuilder newAll = new StringBuilder();
        StringBuilder newGroup = new StringBuilder();

        newQuery.Append("Select ");
        newWhere.Append(" WHERE ");
        newAll.Append(" UNION Select ");
        newGroup.Append(" Group by ");

        if (dpdCtry.SelectedIndex != 0)
        {
            newQuery.Append("T.Country,");
            newGroup.Append("T.Country,");
            newWhere.Append("T.Country like '" + dpdCtry.SelectedValue.ToString() + "' AND ");
            newAll.Append("'****' as Country,");
        }

        if (selected != 0)  //Regions
        {
            string regionS = dpdRegions.Items[PushListWidget.selected].Value.ToString();
            regionS = regionS.Replace("**ALL**", "%");

            newQuery.Append("T.Region,");
            newGroup.Append("T.Region,");
            newWhere.Append("T.Region like '" + regionS + "' AND ");
            newAll.Append("'****' as Region,");
            Session["SelRegion"] = selected;
        }

        if (dpdType.SelectedIndex != 0)     //Type
        {
            newQuery.Append("R.Type,");
            newGroup.Append("R.Type,");
            newWhere.Append("R.Type like '" + dpdType.SelectedValue.ToString() + "' AND ");
            newAll.Append("'****' as Type,");
        }

        //NEW WHERE
        newWhere.Replace("**ALL**", "%");
        newWhere.Remove(newWhere.Length - 4, 4);
        newWhere.Append(" AND BillingDate BETWEEN '" + DateTime.Now.AddDays(-1).ToShortDateString() + "' AND '" + DateTime.Now.AddDays(-1).ToShortDateString() + "'");
        if (newWhere.Length < 1) newWhere.Replace(newWhere.ToString(), "");

        //Select finalizado
        newQuery.Append("'' as AvailC,'' as serviceL,'' as comments,'' as vendorB,'' as dateU,ROUND( ROUND(SUM(T.TotalCost),4)/ROUND(SUM(T.CustMinutes),4),4) as Average_Cost_Per,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD  FROM MECCA2..trafficlog T LEFT JOIN MECCA2.dbo.Regions R ON T.RegionPrefix = R.Code ");
        newQuery.Append(newWhere.ToString() + " ");
        if (newGroup.Length < 10)
        {
            newGroup.Replace(newGroup.ToString(), " ");
        }
        else
        {
            newGroup.Remove(newGroup.Length - 1, 1);
        }

        newAll.Append("'***' as AvailC,'***' as serviceL,'***' as comments, '***' as vendorB,'***' as dateU,ROUND( ROUND(SUM(T.TotalCost),4)/ROUND(SUM(T.CustMinutes),4),4) as Average_Cost_Per,mecca2.dbo.fGetACD(SUM(T.VendorMinutes), SUM(T.BillableCalls)) as ACD  FROM MECCA2..trafficlog T LEFT JOIN MECCA2.dbo.Regions R ON T.RegionPrefix = R.Code ");
        newAll.Append(newWhere.ToString() + " ");
        
        newQuery.Append(newGroup.ToString() + " HAVING SUM(CustMinutes) > 0 ");


        newQuery.Append(newAll.ToString() + " ORDER BY Average_Cost_Per DESC ");

        gdvWidget.DataSource = Util.RunQueryByStmnt(newQuery.ToString()).Tables[0];
        gdvWidget.DataBind();
        
    }

    protected void gdvWidget_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridView gdvMy = (GridView)(e.CommandSource);        
        int cell = 1;

        if (Convert.ToInt32(Session["SelRegion"]) != 0)
        {
            cell = cell + 1;
        }
        if(dpdType.SelectedIndex !=0)
        {
            cell = cell + 1;
        }
        
        string country = gdvMy.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text;
        
        string region= string.Empty;
        string avg = string.Empty;
        string acd = string.Empty;

        if (Convert.ToInt32(Session["SelRegion"]) != 0 && cell == 2)
            region = gdvMy.Rows[Convert.ToInt32(e.CommandArgument)].Cells[2].Text;

            avg = gdvMy.Rows[Convert.ToInt32(e.CommandArgument)].Cells[6 + cell].Text;
            acd = gdvMy.Rows[Convert.ToInt32(e.CommandArgument)].Cells[7 + cell].Text;        
        
        string url = "c=" + country + "&r=" + region + "&av=" + avg + "&ac=" + acd;
        Response.Redirect("./WidgetEntry.aspx?" + url);
    }
    protected void dpdCtry_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
