<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="AssignBroker.aspx.cs" Inherits="AssignBroker" StylesheetTheme="Theme1" Title="DIRECTO - Connections Worldwide" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table align="center">
        <tr>
            <td align="left" style="width: 400px" valign="top" bgcolor="#006699">
                <asp:Label ID="LabelS1" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                    Text="Customer Broker's"></asp:Label></td>
            <td align="left" style="width: 400px" valign="top" bgcolor="#006699">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="#E0E0E0"
                    Text="Vendor Broker's"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" style="width: 400px" valign="top">
                <asp:DropDownList ID="ddlCBroker" runat="server" DataSourceID="sqldsCBroker"
                    DataTextField="CustBroker" DataValueField="CustBroker" OnSelectedIndexChanged="ddlCBroker_SelectedIndexChanged" Width="100px">
                </asp:DropDownList>
                <asp:Button ID="btnShowC" runat="server" OnClick="btnShowC_Click" Text="Show Customers" Width="200px" /></td>
            <td align="left" style="width: 400px" valign="top">
                <asp:DropDownList ID="ddlVBroker" runat="server" DataSourceID="sqlVBroker"
                    DataTextField="VendorBroker" DataValueField="VendorBroker" OnSelectedIndexChanged="ddlVBroker_SelectedIndexChanged" Width="100px">
                </asp:DropDownList>
                <asp:Button ID="btnShowV" runat="server" OnClick="btnShowV_Click" Text="Show Vendors" Width="200px" /></td>
        </tr>
        <tr>
            <td align="left" style="width: 400px" valign="top">
                <asp:DropDownList ID="ddlNewCust" runat="server" DataSourceID="sqldsProvider" DataTextField="ProviderName"
                    DataValueField="ProviderName" Width="230px" OnSelectedIndexChanged="ddlNewCust_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:Button ID="btnAsignarC" runat="server" OnClick="btnAsignarC_Click" Text="Assign" Width="70px" /></td>
            <td align="left" style="width: 400px" valign="top">
                <asp:DropDownList ID="ddlVendor" runat="server" DataSourceID="sqldsProvider" DataTextField="ProviderName"
                    DataValueField="ProviderName" Width="230px" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:Button ID="btnAsignarV" runat="server" Text="Assign" OnClick="btnAsignarV_Click" Width="70px" /></td>
        </tr>
        <tr>
            <td align="left" style="width: 400px" valign="top">
                <asp:GridView ID="gvCust" runat="server" CellPadding="4" DataSourceID="sqldsCustAs"
                    ForeColor="#333333" GridLines="None" Font-Names="Arial">
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <EditRowStyle BackColor="#999999" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
            </td>
            <td align="left" style="width: 400px" valign="top">                
                <asp:GridView ID="gvVendors" runat="server" CellPadding="4" DataSourceID="sqldsVenAs"
                    ForeColor="#333333" GridLines="None" Font-Names="Arial">
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <EditRowStyle BackColor="#999999" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    
    <asp:SqlDataSource ID="sqldsCBroker" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT DISTINCT CustBroker FROM MECCA2..CBroker" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlVBroker" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT DISTINCT VendorBroker  FROM MECCA2..VBroker" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsCustAs" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
    </asp:SqlDataSource>    
    <asp:SqlDataSource ID="sqldsVenAs" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsProvider" runat="server" ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>"
        SelectCommand="SELECT DISTINCT ProviderName  FROM CDRDB..ProviderIP ORDER BY ProviderName" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>  
</asp:Content>

