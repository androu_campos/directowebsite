<%@ Page Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="MSLH_S.aspx.cs" Inherits="MSLH_S" Title="Untitled Page" StylesheetTheme="Theme1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
    <asp:Label ID="Label2" runat="server" Text="MECCA MASTER SALES LOG RESULTS" CssClass="labelTurkH"></asp:Label>
    <table align="left" width="100%" style="position:absolute; top:220px; left:230px;">
        <tr>
            <td align="left" colspan="2">
                <asp:Button ID="Button1" runat="server" CssClass="boton" OnClick="Button1_Click"
                    Text="Export to EXCEL" /></td>
        </tr>
        <tr>
            <td align="left" colspan="2" style="height: 39px" valign="bottom">
                <asp:Label ID="Label1" runat="server" CssClass="labelSteelBlue" Text="Nothing found"
                    Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:GridView ID="GridView1" runat="server" Font-Names="Arial" Font-Size="9pt" AllowPaging="True"
                    OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="200" >
                    <HeaderStyle CssClass="titleOrangegrid" Height="40px" Width="150px" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <AlternatingRowStyle BackColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                </asp:GridView>
                </td>
        </tr>
        <tr>
            <td width="100%">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/footer.gif" Width="100%" />
            </td>
        </tr>
        <tr>
        <td align="left">
                <table>
                    <tr>
                        <td align="right" style="width: 100px">
                            <asp:Label ID="Label4" runat="server" ForeColor="#666666" Text="Powered by" Font-Italic="False"
                                Font-Names="Arial" Font-Size="8pt" Visible="False"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/computertel.gif" Height="45px"
                                Width="146px" Visible="False" /></td>
                    </tr>
                </table>
               </td>
        </tr>
        
    </table>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
    <br />
</asp:Content>

