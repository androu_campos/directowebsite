using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RKLib.ExportData;
public partial class Profit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            dpdCustomer.Items.Clear();
            dpdVendor.Items.Clear();
            dpdCountry.Items.Clear();
            dpdRegions.Items.Clear();

            cdrdbDatasetTableAdapters.ProviderIP1TableAdapter adp = new cdrdbDatasetTableAdapters.ProviderIP1TableAdapter();
            cdrdbDataset.ProviderIP1DataTable tbl = adp.GetProviderName();

            int c = tbl.Rows.Count;
            

            for (int i = 0; i < c; i++)
            {
                dpdCustomer.Items.Add(tbl.Rows[i]["ProviderName"].ToString());
               dpdVendor.Items.Add(tbl.Rows[i]["ProviderName"].ToString());
            }

            cdrdbDatasetTableAdapters.CountryAdp adpR = new cdrdbDatasetTableAdapters.CountryAdp();
            cdrdbDataset.CountryDataTable tblC = adpR.GetCountry();
            
            

            int cR = tblC.Rows.Count;

            for (int j = 0; j < cR; j++)
            {
                dpdCountry.Items.Add(tblC.Rows[j]["Country"].ToString());                
            }

            MyDataSetTableAdaptersTableAdapters.RegionsTableAdapter regAdp = new MyDataSetTableAdaptersTableAdapters.RegionsTableAdapter();
            MyDataSetTableAdapters.RegionsDataTable tblRg = regAdp.GetRegions();

            int z = tblRg.Rows.Count;

            for (int k = 0; k < z; k++)
            {
               dpdRegions.Items.Add(tblRg.Rows[k]["Region"].ToString());
                
            }     


        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            Label7.Visible = true;
            Label8.Visible = true;
            string qry = string.Empty;
            string select = "SELECT ";
            string where = " WHERE ";
            string groupby = " GROUP BY ";
            string having = string.Empty;
            string table = string.Empty;
            string customer, vendor, country, regions;

            int Counter=0;

            if (dpdCustomer.SelectedValue.ToString() != "**DONT SHOW**")
            {
                select = select + "Customer,";
                customer = dpdCustomer.SelectedValue.ToString();
                Counter = Counter + 1;

                if(dpdCustomer.SelectedValue.ToString() == "**SHOW ALL**")
                {
                    where = where + "Customer like '%' AND ";
                }
                else
                {
                    where = where + "Customer like '" + customer + "' AND ";
                }


                groupby = groupby + "Customer,";
            }
            if (dpdVendor.SelectedValue.ToString() != "**DONT SHOW**")
            {
                select = select + "Vendor,";
                vendor = dpdVendor.SelectedValue.ToString();
                Counter = Counter + 1;

                if (dpdVendor.SelectedValue.ToString() == "**SHOW ALL**")
                {
                    where = where + "Vendor like '%' AND ";
                }
                else
                {
                    where = where + "Vendor like '" + vendor + "' AND ";
                }

                groupby = groupby + "Vendor,";
            }
            if (dpdCountry.SelectedValue.ToString() != "**DONT SHOW**")
            {
                select = select + "Country,";
                country = dpdCountry.SelectedValue.ToString();
                Counter = Counter + 1;

                if (dpdCountry.SelectedValue.ToString() == "**SHOW ALL**")
                {
                    where = where + "Country like '%' AND ";
                }
                else
                {
                    where = where + "Country like '" + country + "' AND ";
                }

                groupby = groupby + "Country,";
            }
            if (dpdRegions.SelectedValue.ToString() != "**DONT SHOW**")
            {
                select = select + "Region,";
                regions = dpdRegions.SelectedValue.ToString();
                Counter = Counter + 1;

                if (dpdRegions.SelectedValue.ToString() == "**SHOW ALL**")
                {
                    where = where + "Region like '%' AND ";
                }
                else
                {
                    where = where + "Region like '" + regions + "' AND ";
                }


                groupby = groupby + "Region,";
            }


            select = select + "sum(TotalMinutes) as  Minutes ,round(sum(Profit),1)  as  [ Profit ], sum(PrevMinutes) as  PreviousMinutes,round(sum(PrevProfit),1) as  PreviousProfit, round(sum(0+Profit)-sum(0+PrevProfit),1) as Delta ";
            //from...Table
            if (dpdReport.SelectedValue.ToString() == "1")
            { table = "ProfitTrendsDaily"; }
            else { table = "ProfitTrendsWeekly"; }

            qry = select + " from " + table;

            //where
            int i = where.LastIndexOf("AND");
            where = where.Substring(0, i);
            qry = qry + where;

            //group by
            int g = groupby.LastIndexOf(",");
            groupby = groupby.Substring(0, g);
            qry = qry + groupby;

            //having
            having = " having (sum(TotalMinutes)+sum(PrevMinutes)) >0  order by Delta ";
            qry = qry + having;


            //end qry
            

            SqlCommand sqlSpQuery = new SqlCommand();
            sqlSpQuery.CommandType = System.Data.CommandType.Text;
            sqlSpQuery.CommandText = qry;
            DataSet ResultSet;            
            ResultSet = RunQuery(sqlSpQuery);
            Session["DataSetProfit1"] = ResultSet;
            gdvProfit.DataSource = ResultSet.Tables[0];
            gdvProfit.Columns.Clear();

            for (int c = 0; c < ResultSet.Tables[0].Columns.Count; c++)
            {

                string columnName = ResultSet.Tables[0].Columns[c].ColumnName.ToString();
                BoundField columna = new BoundField();
                columna.HtmlEncode = false;
                
                if (columnName == " Profit " || columnName == "PreviousProfit")
                {
                    columna.DataFormatString = "{0,00:C}";
                }
                else if (columnName == "PreviousMinutes" || columnName == "Minutes" || columnName == "Delta")
                {
                    columna.DataFormatString = "{0:0,0}";
                }
                
                columna.DataField = columnName;
                columna.HeaderText = columnName;
                gdvProfit.Columns.Add(columna);
                gdvProfit.DataBind();

            }                                                           

            gdvProfit.DataBind();
            gdvProfit.Visible = true;
            if (ResultSet.Tables[0].Rows.Count <= 0)
            {
                lblTotals.Visible = true;
                lblDetails.Visible = true;
                gdvTotals.Visible = false;

            }
            else
            {
                gdvTotals.Visible = true;
                lblDetails.Visible = false;
                lblTotals.Visible = false;
                Button3.Visible = true;
            }

          

            string Ccustomer = "%", Vvendor = "%", Ccountry = "%", Rregions = "%";

            if (dpdCustomer.SelectedValue.ToString() != "**DONT SHOW**")
            {

                if (dpdCustomer.SelectedValue.ToString() == "**SHOW ALL**")
                {
                    Ccustomer = "%";

                }
                else
                {
                    Ccustomer = dpdCustomer.SelectedValue.ToString();
                }
            }
            else if (dpdVendor.SelectedValue.ToString() != "**DONT SHOW**")
            {

                if (dpdVendor.SelectedValue.ToString() == "**SHOW ALL**")
                {

                    Vvendor = "%";

                }
                else
                {
                    Vvendor = dpdVendor.SelectedValue.ToString();
                }
            }

            else  if (dpdCountry.SelectedValue.ToString() != "**DONT SHOW**")
            {

                if (dpdCountry.SelectedValue.ToString() == "**SHOW ALL**")
                {

                    Ccountry = "%";

                }
                else
                {
                    Ccountry = dpdCountry.SelectedValue.ToString();
                }
            }
            else if (dpdRegions.SelectedValue.ToString() != "**DONT SHOW**")
            {

                if (dpdRegions.SelectedValue.ToString() == "**SHOW ALL**")
                {

                    Rregions = "%";

                }
                else
                {
                    Rregions = dpdRegions.SelectedValue.ToString();
                }
            }


            string qryTotals = "select sum(TotalMinutes) as [Total Minutes] ,round(sum(Profit),1)  as  [Total Profit], sum(PrevMinutes) as  [Previous Minutes] ,round(sum(PrevProfit),1) as  [Previous Profit], round(sum(Profit)-sum(PrevProfit),1) as [Total Delta]  from " + table + " where  Customer like '" + Ccustomer  + "' and Vendor like '" + Vvendor + "' and  Country like '" + Ccountry + "' and  Region like '" + Rregions + "' having (sum(TotalMinutes)+sum(PrevMinutes)) >0";
            
            SqlCommand sqlSpQuery2 = new SqlCommand();
            sqlSpQuery2.CommandType = System.Data.CommandType.Text;
            sqlSpQuery2.CommandText = qryTotals;
            DataSet ResultSet2;
            ResultSet2 = RunQuery(sqlSpQuery2);
            Session["DataSetProfit2"] = ResultSet2;
            gdvTotals.DataSource = ResultSet2.Tables[0];
            gdvTotals.DataBind();
            gdvTotals.Visible = true;
            /**/
            
            if (ResultSet2.Tables[0].Rows.Count <= 0)
            {
                lblDetails.Visible = true;
                lblTotals.Visible = true;
                gdvTotals.Visible = false;
                gdvProfit.Visible = false;
            }
            else
            {
                Button2.Visible = true;
                gdvTotals.Visible = true;
                gdvProfit.Visible = true;
                lblTotals.Visible = false;
                
            }


        }

        catch(Exception ex)
        {
            string message = ex.Message.ToString();
            Button2.Visible = false;
            Button3.Visible = false;
            gdvProfit.Visible = false;
            gdvTotals.Visible = false;
            lblDetails.Visible = true;
            lblTotals.Visible = true;
            

        
        }
        
    }


    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["MECCA2ConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEmployee = ((DataSet)Session["DataSetProfit2"]).Tables[0].Copy();
            // Export all the details to CSV
            RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
            Random random = new Random();
            string ran = random.Next(1500).ToString();
            string filename = "ProfitAnalyzerTotals" + ".xls";
            objExport.ExportDetails(dtEmployee, Export.ExportFormat.Excel, filename);
        }
        catch (Exception ex)
        {
            string erro = ex.Message.ToString();
        }
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        try
        {
        DataTable dtEmployee1 = ((DataSet)Session["DataSetProfit1"]).Tables[0].Copy();
        // Export all the details to CSV
        RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Web");
        Random random = new Random();
        string ran = random.Next(1500).ToString();
        string filename = "ProfitAnalyzerDetails" + ".xls";
        objExport.ExportDetails(dtEmployee1, Export.ExportFormat.Excel, filename);
    }
    catch (Exception ex)
    {
        string erro = ex.Message.ToString();
    }
    }
}
