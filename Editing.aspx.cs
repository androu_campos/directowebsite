using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Editing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string table = Session["rdbType"].ToString();
        string prefix = Session["pref"].ToString();
        string name = Session["dpdCustname"].ToString();
        string rate = Session["dpdRate"].ToString();
        lblName.Text = name;
        lblPrefix.Text = prefix;
        lblRate.Text = rate;
        
    }
protected void  Button1_Click(object sender, EventArgs e)
{
    string table = Session["rdbType"].ToString();
    string prefix = Session["pref"].ToString();
    string name = Session["dpdCustname"].ToString();
    string rate = Session["dpdRate"].ToString();
  
    if(table == "C")
    {
    string qry = "UPDATE CustBreakout SET Price=" + txtEdit.Text + ", Modified = GetDate() WHERE Prefix = '"+ prefix + "' and CustName = '" + name + "'";

    SqlCommand sqlSpQuery = new SqlCommand();
    sqlSpQuery.CommandType = System.Data.CommandType.Text;
    sqlSpQuery.CommandText = qry;
    RunQuery(sqlSpQuery);
    lblUpdated.Visible = true;
    lblRate.Text = txtEdit.Text;
    lblRate.ForeColor = System.Drawing.Color.Red;
    
    }
    else if(table == "V")
    {
    string qry = "UPDATE VendorBreakout SET Cost=" + txtEdit.Text + ", Modified = GetDate() WHERE Prefix = '" + prefix + "' and VendorName = '" + name + "'";

    SqlCommand sqlSpQuery = new SqlCommand();
    sqlSpQuery.CommandType = System.Data.CommandType.Text;
    sqlSpQuery.CommandText = qry;
    RunQuery(sqlSpQuery);
    lblUpdated.Visible = true;
    lblRate.Text = txtEdit.Text;
    lblRate.ForeColor = System.Drawing.Color.Red;

    }

}

    private System.Data.DataSet RunQuery(System.Data.SqlClient.SqlCommand sqlQuery)
    {
        string connectionString = ConfigurationManager.ConnectionStrings
        ["CDRDBConnectionString"].ConnectionString;
        SqlConnection DBConnection = new SqlConnection(connectionString);
        SqlDataAdapter dbAdapter = new SqlDataAdapter();
        dbAdapter.SelectCommand = sqlQuery;
        sqlQuery.Connection = DBConnection;

        DataSet resultsDataSet = new DataSet();
        try
        {
            dbAdapter.Fill(resultsDataSet);
        }
        catch
        {
        }

        return resultsDataSet;
    }




}
