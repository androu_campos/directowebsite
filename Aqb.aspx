<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Aqb.aspx.cs" Inherits="Aqb" Title="Untitled Page" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:GridView AutoGenerateColumns="false" ID="GridView1" runat="server"  AllowSorting="true" EnableSortingAndPagingCallbacks="true" AllowPaging="true" >
    <Columns>
    
    <asp:BoundField DataField="Customer" HeaderText="Customer" SortExpression="Customer" />
    <asp:BoundField DataField="Minutes" HeaderText="Minutes" SortExpression="Minutes" />
    <asp:BoundField DataField="Attempts" HeaderText="Attempts"  SortExpression="Attempts"/>
    <%--<asp:BoundField DataField="[Answered calls]" />--%>
    <%--<asp:BoundField DataField="Rejected_calls" />--%>
    <asp:BoundField DataField="ASR" HeaderText="ASR"  SortExpression="ASR"/>
    <asp:BoundField DataField="ABR"  HeaderText="ABR" SortExpression="ABR"/>
    <asp:BoundField DataField="ACD"  HeaderText="ACD" SortExpression="ACD"/>
    
    
    
    
    </Columns>
    </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" SelectCommand="Select Customer,SUM(Minutes)as Minutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls],mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls),SUM(RA)) AS ASR, mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR, mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) AS ACD from OpSheetInc WHERE Customer like '%'   Group by Customer UNION Select '**ALL**' as Customer,SUM(Minutes)as Minutes,sum(Attempts)AS Attempts,sum(Calls) AS [Answered calls],sum(RA) AS [Rejected calls],mecca2.dbo.fGetASR(SUM(Attempts),SUM(Calls),SUM(RA)) AS ASR, mecca2.dbo.fGetABR(SUM(Attempts),SUM(Calls)) AS ABR, mecca2.dbo.fGetACD(SUM(Minutes),SUM(Calls)) AS ACD from OpSheetInc WHERE Customer like 'WAVECREST'   ORDER BY Minutes DESC " ConnectionString="<%$ ConnectionStrings:MECCA2ConnectionString %>" ProviderName="<%$ ConnectionStrings:MECCA2ConnectionString.ProviderName %>"></asp:SqlDataSource>
</div>
</form>
</body>
</html>
